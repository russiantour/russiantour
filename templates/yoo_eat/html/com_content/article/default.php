<?php
/**
* @package   yoo_eat
* @author    YOOtheme http://www.yootheme.com
* @copyright Copyright (C) YOOtheme GmbH
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL

$doc->addCustomTag( '
<meta name="twitter:title" content="'.$this->escape($this->item->title).'">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@">
<meta name="twitter:creator" content="@DARTCreations">
<meta name="twitter:url" content="'.str_replace('" ','&quot;',JURI::current()).'"="">
<meta name="twitter:description" content="'.strip_tags($this->item->introtext).'">
<meta property="og:title" content="'.$this->escape($this->item->title).'"/>
<meta property="og:email" content="info@russiantour.com";/>
<meta property="og:url" content="'.str_replace('" ','&quot;',juri::current()).'"="">
<meta property="og:site_name" content="Russian Tour International"/>
<meta property="fb:admins" content="russiantour"/>
<meta property="og:image" content="'.$timage.'"/>
<meta property="og:description" content="'.strip_tags($this->item->introtext).'"/>
');
*/


// include config and layout






 
$base = dirname(dirname(dirname(__FILE__)));
include($base.'/config.php');
include($warp['path']->path('layouts:'.preg_replace('/'.preg_quote($base, '/').'/', '', __FILE__, 1)));
if (isset($images->image_intro) and !empty($images->image_intro))
{
 $timage= htmlspecialchars(JURI::root().$images->image_intro);
}


$doc =& JFactory::getDocument();
