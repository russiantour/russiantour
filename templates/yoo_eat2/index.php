<?php
$document = &JFactory::getDocument();
$document->setGenerator('');
$document->addScriptDeclaration("var saveID = ''; jQuery(document).ready(function() {jQuery('.modal-to-send').on('click',function(){var tour_id = 0; if (typeof jQuery(this).attr('id') != 'undefined') tour_id = jQuery(this).attr('id').split('tour')[1]; if (jQuery('input[name=tour_id]').length < 1) jQuery('#form-to-send').append('<input type=\"hidden\" name=\"tour_id\">'); jQuery('input[name=tour_id]').val(tour_id); jQuery(this).attr('class').split(' ').forEach(function(item){if (/serial-number/.test(item)) {saveID = [document.location.pathname.split('/')[1],document.location.pathname.split('/')[2],item.split('number')[1]].join('-'); }}); jQuery('.form-data-send').attr('id',saveID); jQuery('#generated-id').val(saveID); var txt = 'From: '+document.location.href+'; ID: '+saveID+'; Data: '+jQuery(this).parents('tr').text(); jQuery('#form-data-to-send').val(txt);  }); jQuery('.form-data-send').on('click',function () {if (jQuery('#form-mail').val()=='') {if (jQuery('#form-mail').next('.error').length < 1) jQuery('#form-mail').after('<div class=\"error\" style=\"color:red;\">".JText::_('COM_RUSSIANTOUR_REQUIRED')."</div>');return false;} else if (!/[^@]+@[^.]+\..+/.test(jQuery('#form-mail').val())) {if (jQuery('#form-mail').next('.error').length < 1) jQuery('#form-mail').after('<div class=\"error\" style=\"color:red;\">wrong email!</div>');return false;} jQuery('#form-mail').next('.error').remove(); jQuery('.form-data-send').after('sending...'); jQuery('.form-data-send').hide(); jQuery.post( '/index.php?option=com_russiantour&task=sendForm', jQuery( '#form-to-send' ).serialize()).done(function( data ) {    jQuery('#form-to-send').next().remove();jQuery('#form-to-send').html(data);  }); }); });");
?>
<?php
/**
 * @package   yoo_eat
 * @author    YOOtheme http://www.yootheme.com
 * @copyright Copyright (C) YOOtheme GmbH
 * @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
 */


// no direct access
defined('_JEXEC') or die('Restricted access');

// get warp
$warp = require(__DIR__.'/warp.php');

// load main theme file, located in /layouts/theme.php
echo $warp['template']->render('theme');