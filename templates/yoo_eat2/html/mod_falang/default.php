<?php
/**
 * @package     Falang for Joomla!
 * @author      Stéphane Bouey <stephane.bouey@faboba.com> - http://www.faboba.com
 * @license     GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 * @copyright   Copyright (C) 2010-2017. Faboba.com All rights reserved.
 */

// No direct access to this file
defined('_JEXEC') or die;

JHtml::_('stylesheet', 'mod_falang/template.css', array(), true);

//add alternate tag
$doc = JFactory::getDocument();
$default_lang = JComponentHelper::getParams('com_languages')->get('site', 'en-GB');
$current_lang = JFactory::getLanguage()->getTag();
$languagesCode = JLanguageHelper::getLanguages('lang_code');

$sef = JFactory::getApplication()->getCfg('sef');

/* Support of alternate tag bassed on language filter parameters  */
$remove_default_prefix = 0;
//set default in case of language filter is not use (ex : joomsef)
$alternate_meta    = 1;
$xdefault          = 1;
$xdefault_language = $default_lang;

$filter_plugin = JPluginHelper::getPlugin('system', 'languagefilter');
if (!empty($filter_plugin)) {
    $filter_plugin_params  = new JRegistry($filter_plugin->params);
    $remove_default_prefix = $filter_plugin_params->get('remove_default_prefix','0');
    $alternate_meta        = $filter_plugin_params->get('alternate_meta', 1);
    $xdefault              = $filter_plugin_params->get('xdefault', 1);
    $xdefault_language     = $filter_plugin_params->get('xdefault_language', $default_lang);
}



// hack to fix the fact that $language->link already contains the rootpath of the joomla site
// ex falang3/en for http//localhost/falang3

$uri_base = substr(JURI::base(false), 0,strlen(JURI::base(false))-strlen(JURI::base(true)));
$db = JFactory::getDbo();
if (isset($_GET['test']))
{
    ini_set('display_errors',1);
    var_dump('qwerty3');
}
foreach($list as $language) {
    if ($alternate_meta && $language->display == '1')
    {
        if ($sef == '1')
        {
            $link = $uri_base . substr($language->link, 1);
            if (($language->lang_code == $default_lang) && $remove_default_prefix == '1')
            {
                $link = preg_replace('|/' . $language->sef . '/|', '/', $link, 1);
                //remove last slash for default language
                $link = rtrim($link, "/");
                $doc->addCustomTag('<link rel="alternate" href="' . $link . '" hreflang="' . $language->sef . '" />');
            }
            else
            {
                //kaktus 15.10.2018 kuku!!!
                $l1 = explode('/',substr($language->link, 1));
                $l2 = explode('/',substr($_SERVER['REQUEST_URI'], 1));
                $l3 = array();
                $lastn = count($l2)-1;
                preg_match('/^([0-9]+)-/',$l2[$lastn],$m);
                foreach ($l2 as $n=>$v)
                {
                    if ((isset($m[0]) && $n != $lastn) || !isset($m[0]))
                        $l3[] = $l1[$n];
                }
                $show = true;
                if (isset($m[0]))
                {
                    $l3[$lastn] = $m[0].$l3[$lastn];
                    $sql = 'SELECT * FROM `wza4w_russiantour_` WHERE reference_id = '.$m[1].' AND language_id = '.$language->lang_id.' LIMIT 1';
                    if (isset($_GET['test3']))
                    {
                        echo $sql;
                        exit;
                    }
                    $db->setQuery($sql);
                    $obj = $db->loadObject();
                    if (isset($obj->alias))
                        $l3[$lastn] .= $obj->alias;
                    else
                        $show = false;
                }
                $txt = implode('/',$l3);

                //$doc->addCustomTag('<link rel="alternate" href="' . $link . '" hreflang="' . substr($language->sef,0,strlen($language->sef)-1) . '" />');
                if ($show)
                    $doc->addCustomTag('<link rel="alternate" href="' . $uri_base.$txt . '" hreflang="' . substr($language->sef,0,strlen($language->sef)-1) . '" />');
            }


        }
        else
        {
            $doc->addCustomTag('<link rel="alternate" href="' . $language->link . '" hreflang="' . $language->sef . '" />');
        }

        if ($show && $xdefault)
        {
            $loc_xdefault_language = ($xdefault_language == 'default') ? $default_lang : $xdefault_language;

            if (($loc_xdefault_language == $language->lang_code) && isset($languagesCode[$loc_xdefault_language]))
            {
                // Use a custom tag because addHeadLink is limited to one URI per tag
                if ($sef == '1')
                {
                    $l = explode('/',substr($_SERVER['REQUEST_URI'], 1));
                    preg_match('/^([0-9]+)-/',$l[count($l)-1],$m);
                    if (isset($m[0]))
                        $doc->addCustomTag('<link rel="alternate" href="' . $uri_base.$txt . '"  hreflang="x-default" />');
                    else
                        $doc->addCustomTag('<link rel="alternate" href="' . $link . '"  hreflang="x-default" />');
                }
                else
                {
                    $doc->addCustomTag('<link rel="alternate" href="' . $language->link . '"  hreflang="x-default" />');
                }
            }

        }
    }

}

?>

<?php
// Support of language domain from yireo
$yireo_plugin = JPluginHelper::getPlugin('system', 'languagedomains');
if (!empty($yireo_plugin)) {
    foreach($list as $language):
        if (empty($language->link) || in_array($language->link, array('/', 'index.php'))) $language->link = '/?lang='.$language->sef;
    endforeach;
}
?>


<div class="mod-languages<?php echo $moduleclass_sfx ?> <?php echo ($params->get('dropdown', 1) && $params->get('advanced_dropdown', 1)) ? ' advanced-dropdown' : '';?>">
<?php if ($headerText) : ?>
	<div class="pretext"><p><?php echo $headerText; ?></p></div>
<?php endif; ?>

<?php if ($params->get('dropdown',1)) : ?>
    <?php require JModuleHelper::getLayoutPath('mod_falang', $params->get('layout', 'default') . '_dropdown'); ?>
<?php else : ?>
    <?php require JModuleHelper::getLayoutPath('mod_falang', $params->get('layout', 'default') . '_list'); ?>
<?php endif; ?>

<?php if ($footerText) : ?>
	<div class="posttext"><p><?php echo $footerText; ?></p></div>
<?php endif; ?>
</div>
