<?php defined('_JEXEC') or die;

$document 						= JFactory::getDocument();
if($params['type_mode'] == 0) $document->addScript('https://vk.com/js/api/openapi.js?160');

// включили суффиксы класса модуля
$class_sfx = htmlspecialchars($params->get('class_sfx'));

require( JModuleHelper::getLayoutPath( 'mod_jt_vk', "default" ) );