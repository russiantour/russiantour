<?php defined( '_JEXEC' ) or die; 

$mode = $params['mode'];
$appid = $params['appid'];
$delay = $params['timeout'];
$id = $params['id'];
$width = $params['width'];
$height = $params['height'];
$color1 = $params['color1'];
$color2 = $params['color2'];
$color3 = $params['color3'];
?>

<?php if($params['type_mode'] == 0){?>

<!-- VK Widget -->
<div id="vk_groups"></div>
<script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: <?php echo $mode ?>, width: "<?php echo $width ?>" <?php if (!empty($height)) echo ', height: "'. $height .'"' ?>, color1: '<?php echo $color1 ?>', 
color2: '<?php echo $color2 ?>', color3: '<?php echo $color3 ?>'}, <?php echo $id ?>);
</script>

<?php } else {?>

<div id="vk_api_transport"></div>
<script type="text/javascript">
  window.vkAsyncInit = function() {
    VK.init({
      apiId: <?php echo $appid;?>
    });
	VK.Widgets.Group("vk_groups", {mode: <?php echo $mode;?>, width: "<?php echo $width;?>" 
	<?php if (!empty($height)) echo ', height: "'. $height .'"' ?>, color1: '<?php echo $color1 ?>', color2: '<?php echo $color2 ?>', 
	color3: '<?php echo $color3 ?>'},  <?php echo $id?>);
  };

  setTimeout(function() {
    var el = document.createElement("script");
    el.type = "text/javascript";
    el.src = "https://vk.com/js/api/openapi.js?160";
    el.async = true;
    document.getElementById("vk_api_transport").appendChild(el);
  }, <?php echo $delay;?>);
</script>


<!-- VK Widget -->
<div id="vk_groups"></div>

<?php } ?>