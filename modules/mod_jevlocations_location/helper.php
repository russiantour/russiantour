<?php
/**
 * @version     3.5.0
 * @package     JEvents_Managed_Locations
 * @subpackage  Module Location
 * @copyright   Copyright (C) 2006-2018 GWE Systems
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 */
use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;

defined('_JEXEC') or die('Restricted access');

/**
 * Helper class
 * @since 3.5.0
 */
class ModJevlocationsLocationHelper
{

	/**
	 * Method to get location data
	 *
	 * @param	object	$params	Module params
	 *
	 * @return	object	Location Object
	 */
	static public function getLocation($params)
	{
		$item = null;
		$input = Factory::getApplication()->input;
		$user = Factory::getUser();

		$locId = $input->get('loclkup_fv', 0);

		if (!$locId)
		{
			$locId = self::getFallbackLocation($params);
		}

		if (!empty($locId))
		{
			// Create query object
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);

			$query->from("#__jev_locations AS loc");
			$query->join("LEFT", "#__jevlocations_catmap AS lcm ON lcm.locid = loc.loc_id");
			$query->join("LEFT", "#__categories AS loccat ON loccat.id=lcm.catid AND loccat.extension='com_jevlocations'");
			$query->join("LEFT", "#__jevlocation_categories AS cc1 ON loc.catid=cc1.id");
			$query->join("LEFT", "#__jevlocation_categories AS cc2 ON cc1.parent_id=cc2.id");
			$query->join("LEFT", "#__jevlocation_categories AS cc3 ON cc2.parent_id=cc3.id");
			$query->where('IF(lcm.locid, loccat.access IN (' . implode(',', $user->getAuthorisedViewLevels()) . '), 1 = 1)');
			$query->where('(loc.global = 1 OR loc.created_by = ' . $db->q((int) $user->id) . ')');
			$query->group("loc.loc_id");
			$query->where("loc.published = 1");

			if ($locId)
			{
				$query->where("loc.loc_id = " . $db->q((int) $locId));
			}

			$query->select("loc.*,cc1.title as c1title,cc2.title as c2title,cc3.title as c3title,GROUP_CONCAT(lcm.catid SEPARATOR ','),GROUP_CONCAT(loccat.title SEPARATOR ',') as categories_list");

			$db->setQuery($query);
			$item = $db->loadObject();

			// New custom fields
			$compparams = JComponentHelper::getParams("com_jevlocations");
			$template = $compparams->get("fieldtemplate", "");

			if ($template != "" && $compparams->get("custinlist"))
			{
				$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;

				if (file_exists($xmlfile))
				{
					$db = JFactory::getDBO();

					if ($item)
					{
						$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=" . intval($item->loc_id) . " AND targettype='com_jevlocations'");
						$customdata = $db->loadObjectList();

						JFactory::getLanguage()->load('plg_jevents_jevcustomfields', JPATH_ADMINISTRATOR);

						JLoader::register('JevCfForm', JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/jevcfform.php");

						$jcfparams = JevCfForm::getInstance("com_jevent.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
						$jcfparams->bind($customdata);
						$jcfparams->setEvent($item);

						$customfields = $jcfparams->renderToBasicArray();
						$item->customfields = $customfields;
					}
				}
			}
		}

		return $item;
	}

	/**
	 * Get fallback location according to module configuration
	 *
	 * @param	Object	$params	Parameters object
	 * @return	$locId
	 */
	public static function getFallbackLocation($params)
	{
		$locId = $params->get('fallback_locid', '');

		if (empty($locId))
		{
			$user = Factory::getUser();
			$groups = $user->getAuthorisedGroups();
			$db = Factory::getDbo();
			$query = $db->getQuery(true);

			$query->select('loc.loc_id');
			$query->from("#__jev_locations AS loc");
			$query->where("loc.published = 1");
			$query->where('(loc.global = 1 OR loc.created_by = ' . $db->q((int) $user->id) . ')');

			$query->join("LEFT", "#__jevlocations_catmap as lcm ON lcm.locid = loc.loc_id");
			$query->join("LEFT", "#__categories AS cat ON lcm.catid = cat.id");

			$query->where('IF(lcm.locid, cat.access IN (' . implode(',', $user->getAuthorisedViewLevels()) . '), 1=1)');

			$db->setQuery($query);

			$locId = $db->loadResult();
		}

		return $locId;
	}

}
