<?php
/**
 * @version     3.5.0
 * @package     JEvents_Managed_Locations
 * @subpackage  Module Location
 * @copyright   Copyright (C) 2006-2018 GWE Systems
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die();
$document = JFactory::getDocument();

$itemId = $params->get('target_itemid', 0);
$mediabase = JURI::root() . $params->get('image_path', 'images/stories');
$folder = "jevents/jevlocations";
?>
<div class="jevlocations_location <?php echo $moduleclassSfx; ?>">
		<div class="mod-jevloc-item">
			<div class="mod-jevloc-image">
			<?php

			$nonSEFLink = 'index.php?option=com_jevlocations&task=locations.detail&loc_id=' . $item->loc_id . "&se=1&title=" . JApplication::stringURLSafe($item->title);

			if ($itemId != 0)
			{
				$nonSEFLink .= "&Itemid=" . $itemId;
			}

			$link = JRoute::_($nonSEFLink);
			$mediabase = JUri::root().$params->get('image_path', 'images');
			$folder = "jevents/jevlocations";

			if (strlen($item->image) >= 2)
			{
				$thimg = '<img class="jevloc-bloglayout-image" width="280px" src="' . $mediabase . '/' . $folder . '/thumbnails/thumb_' . $item->image . '" />';
			}
			else
			{
				$thimg = "";
			}
			?>
				<a href="<?php echo $link; ?>" title="<?php echo $item->title; ?>"><?php echo $thimg; ?></a>
			</div>
			<div class="mod-jevloc-title">
					<h4 class="mod-jevloc-loc-title"><?php echo $item->title; ?></h4>
			</div>
			<?php if ($item->c1title != '') : ?>
			<div class="mod-jevloc-city">
					<strong><?php echo JText::_('MOD_JEVLOCATIONS_CITY');?></strong> <?php echo $item->c1title; ?>
			</div>
			<?php endif ?>
			<?php if ($item->c2title != '') : ?>
			<div class="mod-jevloc-state">
					<strong><?php echo JText::_('MOD_JEVLOCATIONS_STATE');?></strong> <?php echo $item->c2title; ?>
			</div>
			<?php endif ?>
			<?php if ($item->c3title != '') : ?>
			<div class="mod-jevloc-country">
					<strong><?php echo JText::_('MOD_JEVLOCATIONS_COUNTRY');?></strong> <?php echo $item->c3title; ?>
			</div>
			<?php endif ?>
			<div class="mod-jevloc-link">
				<a href="<?php echo $link ?>" ><?php echo JText::_('MOD_JEVLOCATIONS_CHECKTHISPLACE') ?></a>
			</div>
		</div>
</div>
