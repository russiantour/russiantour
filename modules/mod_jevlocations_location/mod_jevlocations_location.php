<?php
/**
 * @version     3.5.0
 * @package     JEvents_Managed_Locations
 * @subpackage  Module Location
 * @copyright   Copyright (C) 2006-2018 GWE Systems
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 */

defined('_JEXEC') or die('Restricted access');

require_once dirname(__FILE__) . '/helper.php';

JHtml::stylesheet('modules/mod_jevlocations_location/assets/css/mod_jevlocations_location.css');

if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
{
	JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
}

$item = modJevlocationsLocationHelper::getLocation($params);

if (!($item))
{
	return;
}

$moduleclassSfx = htmlspecialchars($params->get('moduleclass_sfx'));

require JModuleHelper::getLayoutPath('mod_jevlocations_location', "default");
