<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id: helper.php 1893 2011-04-08 09:01:06Z geraintedwards $
 * @package     JEvents
 * @subpackage  Module JEvents Calendar
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */
// no direct access
defined('_JEXEC' ) or die('Restricted access');

class modJevlocationsHelper
{

	static function getList($params)
	{
		$ordering = $params->get('modloc_ordering','ordering');
		$cats  = $params->get("catid", "");
		$count = $params->get("count", 10);
		// Create query object
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->from("#__jev_locations AS loc");
		$query->join("LEFT", "#__jevlocations_catmap AS loccatmap ON loccatmap.locid = loc.loc_id");
		$query->join("LEFT", "#__categories AS loccat ON loccat.id=loccatmap.catid AND loccat.extension='com_jevlocations'");
		$query->join("LEFT", "#__jevlocation_categories AS cc1 ON loc.catid=cc1.id");
		$query->join("LEFT", "#__jevlocation_categories AS cc2 ON cc1.parent_id=cc2.id");
		$query->join("LEFT", "#__jevlocation_categories AS cc3 ON cc2.parent_id=cc3.id");
		$query->group("loc.loc_id");
		$query->where("loc.published = 1");
		
		

		if (is_array($cats) && count($cats)>0) {
			foreach ($cats as $oldCatId)
			{
				$newCats[] = JevLocationsHelper::getNewCategory($oldcatid);
			}
			$cats = implode(",", $newCats);
			
			$query->where("loccatmap.catid in ($cats)");
		}
		else
		{
			$newCats  = $params->get("multicatid", array());
			$cats = implode(",", $newCats);
			if ($cats)
			{
				$query->where("loccatmap.catid in ($cats)");
			}
		}
		switch($ordering)
		{
			case "random":
				$countQuery = $query;
				$countQuery->select('loc.loc_id');
				$db->setQuery($countQuery);
				$locIds = $db->loadColumn();
				if(!empty($locIds))
				{
					$randomKeys = array_rand($locIds,$count);
					foreach ($randomKeys as $key)
					{
						$randomLocIds [] = $locIds[$key];
					}
					$query->where("loc.loc_id IN (". implode(',',$randomLocIds) .")");
				}
				$query->order("RAND()");
				break;
			case "ordering":
				$query->order("loc.ordering asc");
				break;
			case "priority":
				$query->order("loc.priority desc");
			case "alpha":
			default:
				$query->order("loc.title asc");
				break;
		}
		
		$query->select("loc.*,cc1.title as c1title,cc2.title as c2title,cc3.title as c3title,GROUP_CONCAT(loccatmap.catid SEPARATOR ','),GROUP_CONCAT(loccat.title SEPARATOR ',') as categories_list");

		
		$db->setQuery($query, 0,  $count);
		$items = $db->loadObjectList();

		// New custom fields
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$template = $compparams->get("fieldtemplate", "");
		if ($template != "" && $compparams->get("custinlist"))
		{
			$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
			if (file_exists($xmlfile))
			{
				$db = JFactory::getDBO();
				foreach ($items as &$item)
				{
					$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=" . intval($item->loc_id) . " AND targettype='com_jevlocations'");
					$customdata = $db->loadObjectList();

					JFactory::getLanguage()->load('plg_jevents_jevcustomfields', JPATH_ADMINISTRATOR);
					
					JLoader::register('JevCfForm',JPATH_SITE."/plugins/jevents/jevcustomfields/customfields/jevcfform.php");
					//$jcfparams = new JevCfForm($customdata, $xmlfile, $item);
                                        
                                        $jcfparams = JevCfForm::getInstance("com_jevent.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
                                        $jcfparams->bind($customdata);
					$jcfparams->setEvent($item);

					$customfields = $jcfparams->renderToBasicArray();
					$item->customfields = $customfields;
					unset($item);
				}
			}
		}
		
		return $items;

	}

}
