<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id: mod_jevents_categories.php 1746 2011-03-09 12:13:31Z geraintedwards $
 * @package     JEvents
 * @subpackage  Module JEvents Calendar
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */


defined( '_JEXEC'  ) or die( 'Restricted access' );

require_once (dirname(__FILE__).'/helper.php');

JHtml::stylesheet('modules/mod_jevlocations/assets/css/mod_jevlocations.css');

if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
{
	JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
}

$list = modJevlocationsHelper::getList($params);

if (!count($list)) {
	return;
}

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));

require(JModuleHelper::getLayoutPath('mod_jevlocations', "default"));
