<?php

/**
 * copyright (C) 2011 GWE Systems Ltd - All rights reserved
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();
$document = JFactory::getDocument();

$script = '
    function showHide(elementid) {
var el = document.getElementById(elementid);
	el.style.display = (el.style.display != \'none\' ? \'none\' : \'\' );
};';
$document-> addScriptDeclaration ($script);

$featuredCount = $params->get('featuredcount',3);
$count = $params->get('count',3);
$catid = $params->get('catid',0);
$Itemid = $params->get('target_itemid',0);
$catid = (is_array($catid))?$catid[0]:0;
$mediabase = JURI::root().$params->get('image_path', 'images/stories');
$folder = "jevents/jevlocations";
?>
<?php if(count($list)) : ?>
<div class="jevlocations <?php echo $moduleclass_sfx; ?>">
	<?php foreach ($list as $index => $item) :?>
		<div class="mod-jevloc-item">
                <div class="mod-jevloc-image">
                    <?php
						$nonSEFLink =  'index.php?option=com_jevlocations&task=locations.detail&loc_id='. $item->loc_id ."&se=1"."&title=".JApplication::stringURLSafe($item->title);
						$Itemid = $params->get('target_itemid',0);

						if($Itemid!=0)
						{
							$nonSEFLink .="&Itemid=".$Itemid;
						}

						$link 	= JRoute::_($nonSEFLink);
						$mediabase = JUri::root().$params->get('image_path', 'images');
						$folder = "jevents/jevlocations";
						
						if (strlen($item->image) >= 2)
						{
                            $thimg = '<img class="jevloc-bloglayout-image" width="280px" src="' . $mediabase . '/' . $folder . '/thumbnails/thumb_' . $item->image . '" />';
                        }
						else
						{
							$thimg = "";
						}

                        //TODO add in a category image fallback if a category image exists.                    ?>
                        <a href="<?php echo $link; ?>" title="<?php echo $item->title; ?>"><?php echo $thimg; ?></a>
                </div>
                <div class="mod-jevloc-title">
                        <h4 class="mod-jevloc-loc-title"><?php echo $item->title; ?></h4>
                </div>
                <?php if($item->c1title!='') : ?>
                <div class="mod-jevloc-city">
                        <strong><?php echo JText::_('MOD_JEVLOCATIONS_CITY');?></strong> <?php echo $item->c1title; ?>

                </div>
                <?php endif ?>
                <?php if($item->c2title!='') : ?>
                <div class="mod-jevloc-state">
                        <strong><?php echo JText::_('MOD_JEVLOCATIONS_STATE');?></strong> <?php echo $item->c2title; ?>

                </div>
                <?php endif ?>
				<?php if($item->c3title!='') : ?>
                <div class="mod-jevloc-country">
                        <strong><?php echo JText::_('MOD_JEVLOCATIONS_COUNTRY');?></strong> <?php echo $item->c3title; ?>

                </div>
                <?php endif ?>
                <div class="mod-jevloc-link">
                    <a href="<?php echo $link ?>" ><?php echo JText::_('MOD_JEVLOCATIONS_CHECKTHISPLACE') ?></a>
                </div>
        </div>
	<?php endforeach ?>
</div>
<?php endif ?>
