var hcalbus_mvisaprices = [];
function hcalbus_changestate(t){
    var prevTxt = jQuery(t).parent('div').find('textarea');
    var prevName = prevTxt.attr('name').match(/jform\[params\]\[(.*)\]/)[1];
    var txtVal = prevTxt.val().split("\n");

    var txtSel = jQuery('select#'+prevName);

    if (txtSel.length == 0)
    {
        txtSel = jQuery('<select></select>').attr('id',prevName);
        txtSel.append('<option value="">---</option>');
        prevTxt.hide();
        jQuery(txtVal).each(function(){
            var v = this.split("|")[0];
            txtSel.append('<option value="'+v+'">'+v+'</option>');
        });
        jQuery(t).after(txtSel);
    }
    else
    {
        txtSel.remove();
        prevTxt.show();
    }
    if (jQuery('#hcalbus_addbutton').length && jQuery('select#mvisadurations').length && jQuery('select#mvisatypes').length && jQuery('select#mvisaposttypes').length && jQuery('select#mvisaproductiontimes').length)
        jQuery('#hcalbus_addbutton').show();
    else if (jQuery('#hcalbus_addbutton').length)
        jQuery('#hcalbus_addbutton').hide();
}

function hcalbus_generate_mvisaprices(){
    var mvisadurations = jQuery('select#mvisadurations option:selected').val();
    var mvisatypes = jQuery('select#mvisatypes option:selected').val();
    var mvisaposttypes = jQuery('select#mvisaposttypes option:selected').val();
    var mvisaproductiontimes = jQuery('select#mvisaproductiontimes option:selected').val();
    var hcalbus_cost = jQuery('input#hcalbus_cost').val();
    var hcalbus_insurance = jQuery('input#hcalbus_insurance').val();

    if (mvisadurations && mvisatypes && mvisaposttypes && mvisaproductiontimes && hcalbus_cost && hcalbus_insurance)
    {
        /*var founded = false;

        jQuery(hcalbus_mvisaprices).each(function(e){
            if (this[1]==mvisadurations && this[2]==mvisatypes && this[3]==mvisaposttypes && this[4]==mvisaproductiontimes)
            {
                hcalbus_mvisaprices[e][5] = hcalbus_cost;
                hcalbus_mvisaprices[e][6] = hcalbus_insurance;
                founded = true;
            }
        });*/

        e = jQuery('#jform_params_hcalbus_foridhidden').val();
        if (e!='')
        {
            hcalbus_mvisaprices[e][1] = mvisadurations;
            hcalbus_mvisaprices[e][2] = mvisatypes;
            hcalbus_mvisaprices[e][3] = mvisaposttypes;
            hcalbus_mvisaprices[e][4] = mvisaproductiontimes;
            hcalbus_mvisaprices[e][5] = hcalbus_cost;
            hcalbus_mvisaprices[e][6] = hcalbus_insurance;
        }
        else
        //if (!founded)
            hcalbus_mvisaprices.push([
                hcalbus_mvisaprices.length+1,
                mvisadurations,
                mvisatypes,
                mvisaposttypes,
                mvisaproductiontimes,
                hcalbus_cost,
                hcalbus_insurance
            ]);

        hcalbus_showresults();
    }
    else
        alert('All fields are required');
}

function hcalbus_showresults(){
    jQuery('table#hcalbus_showresults').remove();
    var tab = '<table id="hcalbus_showresults">'+
        '<tr>' +
            '<td>Срок действия</td>' +
            '<td>Тип</td>' +
            '<td>Получение</td>' +
            '<td>Срок изг.</td>' +
            '<td>Стоимость</td>' +
            '<td>Страховка</td>' +
            '<td></td>' +
            '<td></td>' +
        '</tr>';
    jQuery(hcalbus_mvisaprices).each(function(e){
        tab += '<tr>';
        tab += '<td>'+this[1]+'</td>';
        tab += '<td>'+this[2]+'</td>';
        tab += '<td>'+this[3]+'</td>';
        tab += '<td>'+this[4]+'</td>';
        tab += '<td>'+this[5]+'</td>';
        tab += '<td>'+this[6]+'</td>';
        tab += '<td onclick="hcalbus_edit('+e+',this)"><span class="icon-apply"></span></td>';
        tab += '<td onclick="hcalbus_delete('+e+',this)"><span class="icon-cancel"></span></td>';
        tab += '</tr>';
    });
    tab += '</table>';
    jQuery('textarea#jform_params_mvisaprices').parent().after(tab);
    jQuery('textarea[name*=mvisaprices]').val(JSON.stringify(hcalbus_mvisaprices));
    console.log(jQuery('textarea[name*=mvisaprices]').val());
}

function hcalbus_edit(e,t){
    jQuery('select#mvisadurations option:selected').attr('selected',false);
    jQuery('select#mvisadurations option[value="'+hcalbus_mvisaprices[e][1]+'"]').attr('selected',true);

    jQuery('select#mvisatypes option:selected').attr('selected',false);
    jQuery('select#mvisatypes option[value="'+hcalbus_mvisaprices[e][2]+'"]').attr('selected',true);

    jQuery('select#mvisaposttypes option:selected').attr('selected',false);
    jQuery('select#mvisaposttypes option[value="'+hcalbus_mvisaprices[e][3]+'"]').attr('selected',true);

    jQuery('select#mvisaproductiontimes option:selected').attr('selected',false);
    jQuery('select#mvisaproductiontimes option[value="'+hcalbus_mvisaprices[e][4]+'"]').attr('selected',true);

    jQuery('input#hcalbus_cost').val(hcalbus_mvisaprices[e][5]);
    jQuery('input#hcalbus_insurance').val(hcalbus_mvisaprices[e][6]);

    jQuery('table#hcalbus_showresults tr').removeClass('selected');
    jQuery(t).parent().addClass('selected');

    jQuery('#jform_params_hcalbus_foridhidden').val(e);
}

function hcalbus_clear(){
    jQuery('select#mvisadurations option:selected').attr('selected',false);
    jQuery('select#mvisatypes option:selected').attr('selected',false);
    jQuery('select#mvisaposttypes option:selected').attr('selected',false);
    jQuery('select#mvisaproductiontimes option:selected').attr('selected',false);
    jQuery('input#hcalbus_cost').val('');
    jQuery('input#hcalbus_insurance').val('');
    jQuery('table#hcalbus_showresults tr').removeClass('selected');
    jQuery('#jform_params_hcalbus_foridhidden').val('');
}

function hcalbus_delete(e){
    hcalbus_mvisaprices.splice(e,1);
    jQuery(hcalbus_mvisaprices).each(function(i){
        hcalbus_mvisaprices[i][0] = i+1;
    });
    hcalbus_showresults();
}

jQuery(document).ready(function (){
    jQuery('textarea[name*=mvisadurations]').parent('div').prepend('<div style="cursor:pointer;" onclick="hcalbus_changestate(this)">change</div>');
    jQuery('textarea[name*=mvisatypes]').parent('div').prepend('<div style="cursor:pointer;" onclick="hcalbus_changestate(this)">change</div>');
    jQuery('textarea[name*=mvisaposttypes]').parent('div').prepend('<div style="cursor:pointer;" onclick="hcalbus_changestate(this)">change</div>');
    jQuery('textarea[name*=mvisaproductiontimes]').parent('div').prepend('<div style="cursor:pointer;" onclick="hcalbus_changestate(this)">change</div>');
    hcalbus_changestate(jQuery('textarea[name*=mvisadurations]').parent('div').find('div'));
    hcalbus_changestate(jQuery('textarea[name*=mvisatypes]').parent('div').find('div'));
    hcalbus_changestate(jQuery('textarea[name*=mvisaposttypes]').parent('div').find('div'));
    hcalbus_changestate(jQuery('textarea[name*=mvisaproductiontimes]').parent('div').find('div'));

    jQuery('textarea[name*=mvisaprices]').parents('.control-group').prepend('Стоимость: <input type="text" id="hcalbus_cost"><br>'+
        'Страховка: <input type="text" id="hcalbus_insurance"><br>'+
        '<div class="btn btn-small" id="hcalbus_addbutton" onclick="hcalbus_generate_mvisaprices()" style="cursor: pointer;">    <span class="icon-save-new"></span>    Добавить/Изменить</div><br>'+
        '<div class="btn btn-small" id="hcalbus_clearbutton" onclick="hcalbus_clear()" style="cursor: pointer;">    <span class="icon-cancel"></span>    Очистить</div><br>');

    hcalbus_mvisaprices = JSON.parse(jQuery('textarea[name*=mvisaprices]').val());
    if (typeof(hcalbus_mvisaprices)!='undefined')
        hcalbus_showresults();
});
