/////////////////////////////////////////////////////////////////////////////////////////
function $(sId){
    return document.getElementById(sId);
}
var ready_to_submit = true;
/////////////////////////////////////////////////////////////////////////////////////////
/* Расчет */

var
    iCityId = 0,                // Город Италии ( задается полем procedura_completa )
    iFigli = 0,
    iPersons = 1,              // Кол-во человек ( figli + 1 )
    iFast = 0,                            // Срочная/обычная (1/0)
    iPeriod = 0,                //      Срок пребывания
    iPostMail = 0,            // Отправка почты
    iLastMedica = 0,

    iDurationId = 0,
    iTypeId = 0,
    iPostTypeId = 0,
    iVisaProductionTimeId = 0,

    iInsurancePrice = 0,
    iVisaPrice = 0,

    L, LF,
    A, B, C, D, E, F,

    mEls = $('BigForm').elements,
    elVisaDuration = mEls['visa_duration'],
    elVisaType = mEls['visa_type'],
    elVisaPostType = mEls['visa_post_type'],
    elVisaProductionTime = mEls['visa_production_time'],
    sDefSelVal = 'selezionare',

    i, j;


/////////////////////////////////////////////////////////////////////////////////////////
function setVisaDuration(){
    var
        mTypeIds = [],
        mPostTypeIds = [];

    iDurationId = elVisaDuration.value * 1;

    elVisaType.innerHTML = '';
    elVisaPostType.innerHTML = '';
    elVisaProductionTime.innerHTML = '';

    _insertOptionTag(elVisaType, 0, sDefSelVal);
    _insertOptionTag(elVisaPostType,0, sDefSelVal);
    _insertOptionTag(elVisaProductionTime,0, sDefSelVal);

    if(iDurationId){
        for( i=0 ; i < mVisaPrices.length ; i++)
            if( mVisaPrices[i][1] == iDurationId ){
                mTypeIds[mTypeIds.length] = mVisaPrices[i][2];
                mPostTypeIds[mPostTypeIds.length] = mVisaPrices[i][3];
            };

        _makeVisaSelectValues( mVisaTypes , mTypeIds , elVisaType );
        _makeVisaSelectValues( mVisaPostTypes , mPostTypeIds , elVisaPostType );
    }

    recalculate();
}


/////////////////////////////////////////////////////////////////////////////////////////
function setVisaType(){
    var
        mProductionTimeIds = [];

    iTypeId = elVisaType.value * 1;
    iPostTypeId = elVisaPostType.value * 1;

    elVisaProductionTime.innerHTML = '';
    _insertOptionTag(elVisaProductionTime,0, sDefSelVal);

    if(iDurationId && iTypeId && iPostTypeId){
        for( i=0 ; i < mVisaPrices.length ; i++)
            if(
                mVisaPrices[i][1] == iDurationId &&
                    mVisaPrices[i][2] == iTypeId &&
                    mVisaPrices[i][3] == iPostTypeId
                )
                mProductionTimeIds[mProductionTimeIds.length] = mVisaPrices[i][4];

        _makeVisaSelectValues( mVisaProductionTimes , mProductionTimeIds , elVisaProductionTime );
    }

    recalculate();
}


/////////////////////////////////////////////////////////////////////////////////////////
function setVisaTipologiaPrices(){
    iDurationId = elVisaDuration.value * 1;
    iTypeId = elVisaType.value * 1;
    iPostTypeId = elVisaPostType.value * 1;
    iVisaProductionTimeId = elVisaProductionTime.value * 1;
    iInsurancePrice = 0;
    iVisaPrice = 0;

    if(iDurationId && iTypeId && iPostTypeId && iVisaProductionTimeId)
        for( i=0 ; i < mVisaPrices.length ; i++)
            if(
                mVisaPrices[i][1] == iDurationId &&
                    mVisaPrices[i][2] == iTypeId &&
                    mVisaPrices[i][3] == iPostTypeId &&
                    mVisaPrices[i][4] == iVisaProductionTimeId
                ){
                iVisaPrice = mVisaPrices[i][5];
                iInsurancePrice = mVisaPrices[i][6];
            };
}


/////////////////////////////////////////////////////////////////////////////////////////
function _insertOptionTag(elSelect,sValue,sText){
    var
        elOption = document.createElement('OPTION');

    elOption.value = sValue;
    elOption.innerHTML = sText;
    elSelect.appendChild(elOption);
}

/////////////////////////////////////////////////////////////////////////////////////////
function _makeVisaSelectValues( mTable , mAllIds , elSelect ){
    var
        sCode = '',
        sIds = _groupArray(mAllIds);

    elSelect.innerHTML = '';
    _insertOptionTag(elSelect,0, sDefSelVal);

    for( i=0 ; i < mTable.length ; i++)
        if( sIds.indexOf( ','+ mTable[i][0] +',' ) != -1 )
            _insertOptionTag(elSelect, mTable[i][0], mTable[i][1]);
}

/////////////////////////////////////////////////////////////////////////////////////////
function _groupArray(mAllIds){
    var sIds = ',' + mAllIds[0];

    for( i=1 ; i < mAllIds.length ; i++)
        if( sIds.indexOf( mAllIds[i] ) == -1 )
            sIds += ',' + mAllIds[i];

    sIds += ',';
    return sIds;
}






/////////////////////////////////////////////////////////////////////////////////////////
function setFigli(elThis){
    var
        iCount = elThis.value * 1;
    sFigliCode = $('HiddenFigli').innerHTML,
        sRez = '';

    for(var i=1 ; i <= iCount ; i++ ){
        sRez+= sFigliCode.replace( /%#/g , i);
    }
    $('FigliBox').innerHTML = sRez;

    iPersons = iCount + 1;
    recalculate();
}


/////////////////////////////////////////////////////////////////////////////////////////
function setProceduraCompleta(){
    var els = mEls['procedura_completa'];

    iCityId = 0;
    for( i=0 ; i < els.length ; i++){
        if( els[i].checked ){
            iCityId = els[i].value * 1;
            break;
        }
    }

    if( iCityId ){
        mEls['ho_medica'].checked = true;
        mEls['ho_medica'].disabled = true;
    }
    else {
        mEls['ho_medica'].disabled = false;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
function setPostMail(){
    var els = mEls['spese_spedizione'];

    iPostMail = 0;
    for( i=0 ; i < els.length ; i++){
        if( els[i].checked ){
            iPostMail = els[i].value * 1;
            break;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
function setPeriod(elThis){
    var
        iDurationId = elVisaDuration.value * 1,
        iDur = 0,
        dTo,
        dFrom = new Date( mEls['dal_year'].value , mEls['dal_month'].value - 1 , mEls['dal_day'].value);
        //dFrom = new Date( mEls['dal_year'].value , mEls['dal_month'].value , mEls['dal_day'].value);
    for( i=0 ; i < mVisaDurations.length ; i++)
        if(mVisaDurations[i][0] == iDurationId)
            iDur = mVisaDurations[i][2];

    dTo = new Date(Date.parse(dFrom).get('Time') + iDur * 1000 * 60 * 60 * 24);
    iPeriod = iDur;
    mEls['al'].value = printDate(dTo);
    $('alDiv').innerHTML = mEls['al'].value;
}

/////////////////////////////////////////////////////////////////////////////////////////
function printDate(dtDate){
    var iNum, sRez='';
    iNum = dtDate.getDate();
    sRez += (iNum < 10?'0':'')+iNum;
    iNum = dtDate.getMonth()+1;
    sRez += '.'+(iNum < 10?'0':'')+iNum;
    sRez += '.'+dtDate.getFullYear();
    return sRez;
}

/////////////////////////////////////////////////////////////////////////////////////////
function setRegistration(){
    var el, iId;

    for( i=0 ; i < mRussianCities.length ; i++){
        iId = mRussianCities[i][0];
        el = mEls['ho_registrazione_' + iId];
        if(el && el.checked){
            C += mRegistrationPrice[iId][0];
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
function recalculate(){
    var ready_to_submit = true;
    iFast = mEls['ho_urgente'].checked * 1;

    setProceduraCompleta();
    setVisaTipologiaPrices();

// регистрация
    C = 0;
    setRegistration();

// консульский сбор
    D = mConsulPrice[iCityId][iFast];


// услуга
    E = mServicePrice[iCityId][iFast];

// почта
    setPostMail();
    F = mPostPrice[iCityId][iFast] * iPostMail;

// Кол-во персон
    //iFigli = $('figli').value * 1;
    //iPersons = iFigli + 1;

    iPersons = 1;

// бизнес приглашение
    L = iVisaPrice;

// стоимость ребенка
    LF = mFigliPrice[0][0];

// Стоимость отправки оригиналов в Италию
    O = mVisaPostTypePrice[0][ (iPostTypeId != 1 ? 1 : 0) ];
    //O = mVisaPostTypePrice[iCityId][ (iPostTypeId != 1 ? 1 : 0) ];


// бизнес страховка
    setPeriod();
    B = iInsurancePrice * mEls['ho_medica'].checked;

    /////////////////////////////////////////

    if (document.getElementById("compila").checked == true)	// == 1
    {
        //G = 12;
        G = mCompila;
    }
    else	G = 0;


    R = L + LF * iFigli + (B + E) * iPersons + C + O + D + F + G;



///////////////////////////////////////


    $('TotalPrice').innerHTML = R + ' Euro';
    $('total_price').value = R;

    var sInfo =
            'iPeriod=' + iPeriod + ' дней\n' +
                'L=' + L + ' приглашение\n' +
                'LF=' + LF + ' стоимость ребенка\n' +
                'B=' + B + ' страховка\n' +
                'C=' + C + ' регистрация\n' +
                'O=' + O + ' отправка оригиналов\n' +
                'D=' + D + ' консульский сбор\n' +
                'E=' + E + ' услуга\n' +
                'F=' + F + ' почта\n' +
                '\n' +
                'R = L + LF * iFigli + (B + E) * iPersons + C + O + D + F\n' +
                'R=' + R + ' \n' +
                ''
        ;

    if( $('Debug') )        $('Debug').innerHTML = sInfo.replace( /\n/g , '<br />');

    jQuery('#BigForm').on('submit',function(){
        recalculate();
        jQuery('#BigForm input[type=text][required=true],#BigForm select[required=true]').each(function(){
            if (!jQuery(this).val() || jQuery(this).val()=='0')
            {
                jQuery(this).css('color','red');
                ready_to_submit = false;
            }
            else
                jQuery(this).css('color','');
        });
        if (ready_to_submit) {
            jQuery('#BigForm').off('submit');
            jQuery('#BigForm').submit();
        }
        else
        {
            jQuery.scrollTo('#BigForm');
            return false;
        }
    })
}

window.onload = recalculate;
