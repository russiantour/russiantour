/////////////////////////////////////////////////////////////////////////////////////////
function $(sId){
    return document.getElementById(sId);
}


/////////////////////////////////////////////////////////////////////////////////////////
/* Расчет */

var
    iCityId = 0,			// Город Италии ( задается полем procedura_completa )
    iPersons = 1,			// Кол-во человек ( figli + 1 )
    iFast = 0,				// Срочная/обычная (1/0)
    iPeriod = 0,			//	Срок пребывания
    iPostMail = 0,			// Отправка почты
    iLastMedica = 0,

    A, B, C, D, E, F,

    mEls = $('BigForm').elements,
    i;


/////////////////////////////////////////////////////////////////////////////////////////
function setFigli(elThis){
    var
        iCount = elThis.value * 1;
    sFigliCode = $('HiddenFigli').innerHTML,
        sRez = '';

    for(var i=1 ; i <= iCount ; i++ ){
        sRez+= sFigliCode.replace( /%#/g , i);
    }
    $('FigliBox').innerHTML = sRez;

    iPersons = iCount + 1;

    recalculate();
}


/////////////////////////////////////////////////////////////////////////////////////////
function setProceduraCompleta(){
    var els = mEls['procedura_completa'];

    iCityId = 0;
    for( i=0 ; i < els.length ; i++){
        if( els[i].checked ){
            iCityId = els[i].value * 1;
            break;
        }
    }

    if( iCityId ){
        mEls['ho_medica'].checked = true;
        mEls['ho_medica'].disabled = true;
    }
    else {
        mEls['ho_medica'].disabled = false;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
function setPostMail(){
    var els = mEls['spese_spedizione'];

    iPostMail = 0;
    for( i=0 ; i < els.length ; i++){
        if( els[i].checked ){
            iPostMail = els[i].value * 1;
            break;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
function setPeriod(elThis){
    var
        dTo = new Date( mEls['al_year'].value , mEls['al_month'].value - 1 , mEls['al_day'].value),
        dFrom = new Date( mEls['dal_year'].value , mEls['dal_month'].value - 1 , mEls['dal_day'].value);

    iPeriod = 1 + (Date.parse(dTo) - Date.parse(dFrom)) / 1000 / 60 / 60 / 24 ;
}

/////////////////////////////////////////////////////////////////////////////////////////
function setRegistration(){
    var el, iId;

    for( i=0 ; i < mRussianCities.length ; i++){
        iId = mRussianCities[i][0];
        el = mEls['ho_registrazione_' + iId];
        if(el && el.checked){
            C += mRegistrationPrice[iId][0];
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
function recalculate(check){

    iFast = mEls['ho_urgente'].checked * 1;

    setProceduraCompleta();

// приглашение
    A =
        mInvitationPrice[( iCityId > 0 ) * 1] *
            (
                !(mEls['hotel'].checked || mEls['appartamento'].checked)
                )
    ;

// страховка
    B = 0;
    if(mEls['ho_medica'].checked){
        setPeriod();
        for( i=0 ; i < mInsurancePrice.length ; i++){
            B = mInsurancePrice[i][1];
            if( iPeriod <= mInsurancePrice[i][0] ) break;
        }
    }

// регистрация
    C = 0;
    setRegistration();

// консульский сбор
    D = mConsulPrice[iCityId][iFast];


// услуга
    E = mServicePrice[iCityId][iFast];

// почта
    setPostMail();
    F = mPostPrice[iCityId][iFast] * iPostMail;

// Кол-во персон
    //iPersons = $('figli').value * 1 + 1;
    iPersons = 1;

// 
    if (document.getElementById("compila").checked == true)	// == 1
    {
        G = 12;
    }
    else	G = 0;

    R = (A + B + E) * iPersons + C + D + F + G;

    $('TotalPrice').innerHTML = R + ' Euro';
    $('total_price').value = R;

    /*
     var sInfo =
     //		'iPeriod=' + iPeriod + ' дней\n' +
     'A=' + A + ' приглашение\n' +
     'B=' + B + ' страховка\n' +
     'C=' + C + ' регистрация\n' +
     'D=' + D + ' консульский сбор\n' +
     'E=' + E + ' услуга\n' +
     'F=' + F + ' почта\n' +
     '\n' +
     'R = (A + B + E) * (1 + Figli) + C + D + F\n' +
     'R=' + R + ' \n' +
     ''
     ;
     */
//	$('Debug').innerHTML = sInfo.replace( /\n/g , '<br />');
//	$('Debug').style.top = scrollTop() + 100 + 'px';
    /**/

}

window.onload = recalculate;
