<?php

/**
 * @package     Joomla.Tutorials
 * @subpackage  Module
 * @copyright   (C) 2012 http://it-complex.by
 * @license     License GNU General Public License version 2 or later; see LICENSE.txt
 */
   
// No direct access to this file
defined('_JEXEC') or die;

//if ($_GET['test']) {echo '<pre>';var_dump($_SERVER);echo '</pre>';}
$mvisadurations = explode("\n",$params->get('mvisadurations'));
foreach($mvisadurations as $mvdkey=>$mvisaduration){
    $mvisadurations[$mvdkey] = explode('|',$mvisaduration);
    $durationsForPrices[trim($mvisadurations[$mvdkey][0])] = $mvdkey+1;
}

$mvisatypes = explode("\n",$params->get('mvisatypes'));
foreach($mvisatypes as $mvdkey=>$mvisatype){
    $mvisatypes[$mvdkey] = explode('|',$mvisatype);
    $typesForPrices[trim($mvisatypes[$mvdkey][0])] = $mvdkey+1;
}

$mvisaposttypes = explode("\n",$params->get('mvisaposttypes'));
foreach($mvisaposttypes as $mvdkey=>$mvisaposttype){
    $mvisaposttypes[$mvdkey] = explode('|',$mvisaposttype);
    $posttypesForPrices[trim($mvisaposttypes[$mvdkey][0])] = $mvdkey+1;
}
//echo '<pre>';var_dump($mvisaposttypes);echo '</pre>';
$mvisaproductiontimes = explode("\n",$params->get('mvisaproductiontimes'));
foreach($mvisaproductiontimes as $mvdkey=>$mvisaproductiontime){
    $mvisaproductiontimes[$mvdkey] = explode('|',$mvisaproductiontime);
    $productiontimesForPrices[trim($mvisaproductiontimes[$mvdkey][0])] = $mvdkey+1;
}

$mvisaprices = json_decode($params->get('mvisaprices'));

foreach ($mvisaprices as $mvpkey => $mvisaprice)
{
    $mvisaprices[$mvpkey][1] = $durationsForPrices[$mvisaprices[$mvpkey][1]];
    $mvisaprices[$mvpkey][2] = $typesForPrices[$mvisaprices[$mvpkey][2]];
    $mvisaprices[$mvpkey][3] = $posttypesForPrices[$mvisaprices[$mvpkey][3]];
    $mvisaprices[$mvpkey][4] = $productiontimesForPrices[$mvisaprices[$mvpkey][4]];
}
if ($_POST)
{
    switch($_POST['procedura_completa']){
        case 0: $procedura_completa = 'NO'; break;
        case 1: $procedura_completa = 'SI, a Milano'; break;
        case 2: $procedura_completa = 'SI, a Palermo'; break;
        case 3: $procedura_completa = 'SI, a Roma'; break;
        case 6: $procedura_completa = 'SI, a Genova'; break;
        default: $procedura_completa = 'NO';
    }
    switch($_POST['spese_spedizione']){
        case 0: $spese_spedizione = 'NO'; break;
        case 1: $spese_spedizione = 'Solo in ritorno'; break;
        case 2: $spese_spedizione = 'In andata ed in ritorno'; break;
        default: $spese_spedizione = 'NO';
    }

    $Ho_bisogno_anche_di = array();
    if (isset($_POST['hotel'])) $Ho_bisogno_anche_di[]='Hotel';
    if (isset($_POST['appartamento'])) $Ho_bisogno_anche_di[]='Appartamento';
    if (isset($_POST['volo_aereo'])) $Ho_bisogno_anche_di[]='Volo aereo';
    if (isset($_POST['transfers'])) $Ho_bisogno_anche_di[]='Transfers';
    if (isset($_POST['escursioni'])) $Ho_bisogno_anche_di[]='Escursioni';

    switch($_POST['parlo']){
        case 0: $parlo = 'Parlo solo italiano'; break;
        case 1: $parlo = 'Parlo inglese o russo'; break;
        default: $parlo = 'NO';
    }

    $body = 'Visto Business: Visto affari (business)
'.$_SERVER['HTTP_REFERER'].'
Durata del visto: '.($_POST['visa_duration']?(trim($mvisadurations[$_POST['visa_duration']-1][0])):'NO').'
Numero di ingressi: '.($_POST['visa_type']?(trim($mvisatypes[$_POST['visa_type']-1][0])):'NO').'
Telex/Originale: '.($_POST['visa_post_type']?(trim($mvisaposttypes[$_POST['visa_post_type']-1][0])):'NO').'
Tempo di emissione: '.($_POST['visa_production_time']?(trim($mvisaproductiontimes[$_POST['visa_production_time']-1][0])):'NO').'
Nome: '.($_POST['nome']?(trim($_POST['nome'])):'NO').'
Cognome: '.($_POST['cognome']?(trim($_POST['cognome'])):'NO').'
Data di nascita: '.$_POST['nascita_day'].'.'.$_POST['nascita_month'].'.'.$_POST['nascita_year'].'
Sesso: '.($_POST['sesso']?'F':'M').'
Numero di passaporto: '.($_POST['numero_passaporto']?(trim($_POST['numero_passaporto'])):'NO').'
Cittadinanza: '.($_POST['cittadinanza']?(trim($_POST['cittadinanza'])):'NO').'
Rilasciato il: '.$_POST['rilasciato_il_day'].'.'.$_POST['rilasciato_il_month'].'.'.$_POST['rilasciato_il_year'].'
Valido fino al: '.$_POST['valido_fino_al_day'].'.'.$_POST['valido_fino_al_month'].'.'.$_POST['valido_fino_al_year'].'
Nazione di nascita: '.($_POST['nazione_nascita']?(trim($_POST['nazione_nascita'])):'NO').'
Citta di nascita: '.($_POST['citta_nascita']?(trim($_POST['citta_nascita'])):'NO').'
Nazione di residenza: '.($_POST['nazione_residenza']?(trim($_POST['nazione_residenza'])):'NO').'
Indirizzo di residenza: '.($_POST['indirizzo_residenza']?(trim($_POST['indirizzo_residenza'])):'NO').'
E-Mail: '.($_POST['mail']?(trim($_POST['mail'])):'NO').'
Telefono: '.($_POST['telefono']?(trim($_POST['telefono'])):'NO').'
Citta` da visitare: '.($_POST['citta_visitare']?(trim($_POST['citta_visitare'])):'NO').'
Citta di residenza: '.($_POST['citta']?(trim($_POST['citta'])):'NO').'
Dal: '.$_POST['dal_day'].'.'.$_POST['dal_month'].'.'.$_POST['dal_year'].'
Al: '.($_POST['al']?(trim($_POST['al'])):'NO').'
Consolato al quale sara` presentata la domanda di visto: '.($_POST['consolato_quale']?(trim($_POST['consolato_quale'])):'NO').'
Ragione sociale della ditta presso la quale lavora: '.($_POST['ragione_sociale']?(trim($_POST['ragione_sociale'])):'NO').'
Posizione nella ditta: '.($_POST['posizione_nella_ditta']?(trim($_POST['posizione_nella_ditta'])):'NO').'
Telefono lavoro: '.($_POST['telefono_lavoro']?(trim($_POST['telefono_lavoro'])):'NO').'
Indirizzo lavoro: '.($_POST['indirizzo_lavoro']?(trim($_POST['indirizzo_lavoro'])):'NO').'
Ho bisogno dell-assicurazione medica: '.(isset($_POST['ho_medica'])?'SI':'NO').'
Sono interessato alla procedura completa: '.$procedura_completa.'
Ho bisogno della procedura urgente: '.(isset($_POST['ho_urgente'])?'SI':'NO').'
Ho bisogno della registrazione: '.(isset($_POST['ho_registrazione_1'])?'a Mosca':'').
        ((isset($_POST['ho_registrazione_1'])&&isset($_POST['ho_registrazione_2']))?',':'').
        ((!isset($_POST['ho_registrazione_1'])&&!isset($_POST['ho_registrazione_2']))?'NO':'').
        (isset($_POST['ho_registrazione_2'])?'a San Pietroburgo ':'').'
Ho bisogno della compilazione del modulo consolare per mio conto : '.(isset($_POST['ho_compila'])?'SI':'NO').'
Mettete in preventivo le spese di spedizione: '.$spese_spedizione.'
Ho bisogno anche di: '.((count($Ho_bisogno_anche_di)>0)?(implode(',',$Ho_bisogno_anche_di)):'NO').'
Parlo: '.$parlo.'
Note: = '.$_POST['comment'].'

SUM IN PAGE = '.$_POST['total_price'].'
IP: '.$_SERVER["REMOTE_ADDR"].'
Today: '.date('d-m-Y, H:i');
    if (JFactory::getMailer()->sendMail(JFactory::getConfig()->get('mailfrom'), JFactory::getConfig()->get('fromname'), explode(',',$params->get('hcalbus_email')), 'VISTO BUSINESS', $body/*print_r($_POST,true)*/))
    {
        echo '<div data-uk-alert="" class="uk-alert uk-alert-large uk-alert-notice">
<button class="uk-alert-close uk-close" type="button"></button>
<h2>AVVISO</h2>
<p>Grazie per la fiducia! La vostra richiesta e` stata inoltrata, sarete ricontattati via mail al piu` presto da un nostro operatore, che vi indichera` tutti i dettagli della procedura e le modalita` di pagamento.</p>
</div>';
        $notshow = true;
    }

}
if (!isset($notshow))
{
?>
<style>#HiddenFigli {display: none;}</style>
<div id="C">
<script type="text/javascript">

var mVisaDurations = [
<?php
foreach($mvisadurations as $mvdkey=>$mvisaduration){
    echo "    [".($mvdkey+1)." , '".trim($mvisaduration[0])."', ".trim($mvisaduration[1])." ]";
    if (isset($mvisadurations[$mvdkey+1]))
        echo ',
';
}
?>
];

var mVisaTypes = [
<?php
foreach($mvisatypes as $mvdkey=>$mvisatype){
    echo "    [".($mvdkey+1)." , '".trim($mvisatype[0])."' ]";
    if (isset($mvisatypes[$mvdkey+1]))
        echo ',
';
}
?>
];
var mVisaPostTypes = [
<?php
foreach($mvisaposttypes as $mvdkey=>$mvisaposttype){
    echo "    [".($mvdkey+1)." , '".trim($mvisaposttype[0])."' ]";
    if (isset($mvisaposttypes[$mvdkey+1]))
        echo ',
';
}
?>
];
var mVisaProductionTimes = [
<?php
foreach($mvisaproductiontimes as $mvdkey=>$mvisaproductiontime){
    echo "    [".($mvdkey+1)." , '".trim($mvisaproductiontime[0])."' ]";
    if (isset($mvisaproductiontimes[$mvdkey+1]))
        echo ',
';
}
?>
];
var mVisaPrices = [
<?php
foreach($mvisaprices as $mvdkey=>$mvisaprice){
    echo "    [".$mvisaprice[0].", ".$mvisaprice[1].", ".$mvisaprice[2].", ".$mvisaprice[3].", ".$mvisaprice[4].", ".$mvisaprice[5].", ".$mvisaprice[6]." ]";
    if (isset($mvisaprices[$mvdkey+1]))
        echo ',
';
}
?>
];

var mCompila = <?php echo $params->get('compila'); ?>;

 var mFigliPrice = [];
 mFigliPrice[0] = [0];
mFigliPrice[0] = [<?php echo $params->get('figliprice1'); ?>];

 var mVisaPostTypePrice = [];
 mVisaPostTypePrice[0] = [0,0];
mVisaPostTypePrice[0] = [<?php echo $params->get('visaposttypeprice1'); ?>,<?php echo $params->get('visaposttypeprice2'); ?>];

var mItalianCities = [
    [1 , "Milano" ],
    [2 , "Palermo" ],
    [3 , "Roma" ],
    [6 , "Genova" ]
];
var mRussianCities = [
    [1 , "Mosca" ],
    [2 , "St.Petersburg" ]
];

var mServicePrice = [];
mServicePrice[0] = [0,0];
mServicePrice[1] = [<?php echo $params->get('serviceprice1'); ?>,<?php echo $params->get('serviceprice5'); ?>];
mServicePrice[2] = [<?php echo $params->get('serviceprice2'); ?>,<?php echo $params->get('serviceprice6'); ?>];
mServicePrice[3] = [<?php echo $params->get('serviceprice3'); ?>,<?php echo $params->get('serviceprice7'); ?>];
mServicePrice[6] = [<?php echo $params->get('serviceprice4'); ?>,<?php echo $params->get('serviceprice8'); ?>];

var mRegistrationPrice = [];
mRegistrationPrice[0] = [0,0];
mRegistrationPrice[1] = [<?php echo $params->get('registrationprice1'); ?>,0];
mRegistrationPrice[2] = [<?php echo $params->get('registrationprice2'); ?>,0];
var mConsulPrice = [];
mConsulPrice[0] = [0,0];
mConsulPrice[1] = [<?php echo $params->get('consulprice1'); ?>,<?php echo $params->get('consulprice5'); ?>];
mConsulPrice[2] = [<?php echo $params->get('consulprice2'); ?>,<?php echo $params->get('consulprice6'); ?>];
mConsulPrice[3] = [<?php echo $params->get('consulprice3'); ?>,<?php echo $params->get('consulprice7'); ?>];
mConsulPrice[6] = [<?php echo $params->get('consulprice4'); ?>,<?php echo $params->get('consulprice8'); ?>];

var mPostPrice = [];
mPostPrice[0] = [0,0];
mPostPrice[1] = [<?php echo $params->get('postprice1'); ?>,<?php echo $params->get('postprice5'); ?>];
mPostPrice[2] = [<?php echo $params->get('postprice2'); ?>,<?php echo $params->get('postprice6'); ?>];
mPostPrice[3] = [<?php echo $params->get('postprice3'); ?>,<?php echo $params->get('postprice7'); ?>];
mPostPrice[6] = [<?php echo $params->get('postprice4'); ?>,<?php echo $params->get('postprice8'); ?>];



</script>


        <h1><?php echo JText::_('MOD_HCALBUS_H1'); ?></h1>
<p style="text-align: justify;" ><?php echo JText::_('MOD_HCALBUS_INFO'); ?></p>

 
<form id="BigForm" class="BigForm" method="post" action="">

<fieldset class="uk-form">

<div class="leftCol" style="margin-top: 20px;">
<div class="s5">
<div class="s11"><?php echo JText::_('MOD_HCALBUS_DURATO'); ?><span class="red">*</span></div>
                        <div class="s12"> <select required=true class="" onchange="setVisaDuration()" id="visa_duration" name="visa_duration">

                                <option selected="" value="0"><?php echo JText::_('MOD_HCALBUS_SELEZIONARE'); ?> </option>
<?php
foreach($mvisadurations as $mvdkey=>$mvisaduration){
    echo '<option value="'.($mvdkey+1).'">'.trim($mvisaduration[0]).'</option>';
}
?>
                        </select></div><div class="s11">
 <?php echo JText::_('MOD_HCALBUS_INGRESSI'); ?><span class="red">*</span></div>

    <div class="s12"> <select required=true class="" onchange="setVisaType()" id="visa_type" name="visa_type">
                                 <option selected="" value="0"> <?php echo JText::_('MOD_HCALBUS_SELEZIONARE'); ?></option>
                        </select></div>

<div class="s11">
 <?php echo JText::_('MOD_HCALBUS_TELEX'); ?><span class="red">*</span></div><div class="s12">
    <select required=true class="" onchange="setVisaType()" id="visa_post_type" name="visa_post_type">
                               <option selected="" value="0"> <?php echo JText::_('MOD_HCALBUS_SELEZIONARE'); ?></option>
                        </select></div>
<div class="s11"><?php echo JText::_('MOD_HCALBUS_TEMPO'); ?><span class="red">*</span></div>
<div class="s12">
    <select required=true class="" onchange="recalculate()" id="visa_production_time" name="visa_production_time">
                                   <option selected="" value="0"> <?php echo JText::_('MOD_HCALBUS_SELEZIONARE'); ?></option>
</select></div>
                </div></div>
                <div class="clear"></div>
        </div>
</fieldset>

<fieldset class="uk-form">
   <div class="hr"></div>
                        <div class="row1">
               <div class="s5">
<div class="s11"><?php echo JText::_('MOD_HCALBUS_NOME'); ?><span class="red">*</span></div> <div class="s6"><input required="true" type="text" style="width: 96%;" value="" class="txt" id="nome" name="nome"></div>
<div class="s11"><?php echo JText::_('MOD_HCALBUS_COGNOME'); ?><span class="red">*</span></div>  <div class="s6"><input required="true" type="text" value="" style="width: 96%;" class="txt" id="cognome" name="cognome"></div>
                </div></div>
                <div class="clear"></div>
        
<br><br>
                      
                <div class="leftCol ">
<div class="s5"><div class="s11"><?php echo JText::_('MOD_HCALBUS_NASCITA'); ?><span class="red">*</span></div>
                    <div class="s6"><select required=true onchange="recalculate()" class="day" id="nascita_day" name="nascita_day">
                                        <option value="01"> 01</option>
<option value="02"> 02</option>
<option value="03"> 03</option>
<option value="04"> 04</option>
<option value="05"> 05</option>
<option value="06"> 06</option>
<option value="07"> 07</option>
<option value="08"> 08</option>
<option value="09"> 09</option>
<option value="10"> 10</option>
<option value="11"> 11</option>
<option value="12"> 12</option>
<option value="13"> 13</option>
<option value="14"> 14</option>
<option value="15"> 15</option>
<option value="16"> 16</option>
<option value="17"> 17</option>
<option value="18"> 18</option>
<option value="19"> 19</option>
<option value="20"> 20</option>
<option value="21"> 21</option>
<option value="22"> 22</option>
<option value="23"> 23</option>
<option value="24"> 24</option>
<option value="25"> 25</option>
<option value="26"> 26</option>
<option value="27"> 27</option>
<option value="28"> 28</option>
<option value="29"> 29</option>
<option value="30"> 30</option>
<option value="31"> 31</option>
                                        </select>
                    <select required=true onchange="recalculate()" class="month" id="nascita_month" name="nascita_month">
                                <option selected="" value="0"> <?php echo JText::_('MOD_HCALBUS_MOUNT');?> </option>
<option value="01"> <?php echo JText::_('JANUARY');?></option>
<option value="02"> <?php echo JText::_('FEBRUARY');?></option>
<option value="03"> <?php echo JText::_('MARCH');?></option>
<option value="04"> <?php echo JText::_('APRIL');?></option>
<option value="05"> <?php echo JText::_('MAY');?></option>
<option value="06"> <?php echo JText::_('JUNE');?></option>
<option value="07"> <?php echo JText::_('JULY');?></option>
<option value="08"> <?php echo JText::_('AUGUST');?></option>
<option value="09"> <?php echo JText::_('SEPTEMBER');?></option>
<option value="10"> <?php echo JText::_('OCTOBER');?></option>
<option value="11"> <?php echo JText::_('NOVEMBER');?></option>
<option value="12"> <?php echo JText::_('DECEMBER');?></option>
                        </select>
                        <select required=true onchange="recalculate()" class="year" id="nascita_year" name="nascita_year">
                                <option selected="" value="0">  <?php echo JText::_('JYEAR');?></option>
<option value="2014"> 2014</option>
<option value="2013"> 2013</option>
<option value="2012"> 2012</option>
<option value="2011"> 2011</option>
<option value="2010"> 2010</option>
<option value="2009"> 2009</option>
<option value="2008"> 2008</option>
<option value="2007"> 2007</option>
<option value="2006"> 2006</option>
<option value="2005"> 2005</option>
<option value="2004"> 2004</option>
<option value="2003"> 2003</option>
<option value="2002"> 2002</option>
<option value="2001"> 2001</option>
<option value="2000"> 2000</option>
<option value="1999"> 1999</option>
<option value="1998"> 1998</option>
<option value="1997"> 1997</option>
<option value="1996"> 1996</option>
<option value="1995"> 1995</option>
<option value="1994"> 1994</option>
<option value="1993"> 1993</option>
<option value="1992"> 1992</option>
<option value="1991"> 1991</option>
<option value="1990"> 1990</option>
<option value="1989"> 1989</option>
<option value="1988"> 1988</option>
<option value="1987"> 1987</option>
<option value="1986"> 1986</option>
<option value="1985"> 1985</option>
<option value="1984"> 1984</option>
<option value="1983"> 1983</option>
<option value="1982"> 1982</option>
<option value="1981"> 1981</option>
<option value="1980"> 1980</option>
<option value="1979"> 1979</option>
<option value="1978"> 1978</option>
<option value="1977"> 1977</option>
<option value="1976"> 1976</option>
<option value="1975"> 1975</option>
<option value="1974"> 1974</option>
<option value="1973"> 1973</option>
<option value="1972"> 1972</option>
<option value="1971"> 1971</option>
<option value="1970"> 1970</option>
<option value="1969"> 1969</option>
<option value="1968"> 1968</option>
<option value="1967"> 1967</option>
<option value="1966"> 1966</option>
<option value="1965"> 1965</option>
<option value="1964"> 1964</option>
<option value="1963"> 1963</option>
<option value="1962"> 1962</option>
<option value="1961"> 1961</option>
<option value="1960"> 1960</option>
<option value="1959"> 1959</option>
<option value="1958"> 1958</option>
<option value="1957"> 1957</option>
<option value="1956"> 1956</option>
<option value="1955"> 1955</option>
<option value="1954"> 1954</option>
<option value="1953"> 1953</option>
<option value="1952"> 1952</option>
<option value="1951"> 1951</option>
<option value="1950"> 1950</option>
<option value="1949"> 1949</option>
<option value="1948"> 1948</option>
<option value="1947"> 1947</option>
<option value="1946"> 1946</option>
<option value="1945"> 1945</option>
<option value="1944"> 1944</option>
<option value="1943"> 1943</option>
<option value="1942"> 1942</option>
<option value="1941"> 1941</option>
<option value="1940"> 1940</option>
<option value="1939"> 1939</option>
<option value="1938"> 1938</option>
<option value="1937"> 1937</option>
<option value="1936"> 1936</option>
<option value="1935"> 1935</option>
<option value="1934"> 1934</option>
<option value="1933"> 1933</option>
<option value="1932"> 1932</option>
<option value="1931"> 1931</option>
<option value="1930"> 1930</option>
<option value="1929"> 1929</option>
<option value="1928"> 1928</option>
<option value="1927"> 1927</option>
<option value="1926"> 1926</option>
<option value="1925"> 1925</option>
<option value="1924"> 1924</option>
<option value="1923"> 1923</option>
<option value="1922"> 1922</option>
<option value="1921"> 1921</option>
<option value="1920"> 1920</option>
<option value="1919"> 1919</option>
<option value="1918"> 1918</option>
<option value="1917"> 1917</option>
<option value="1916"> 1916</option>
<option value="1915"> 1915</option>
<option value="1914"> 1914</option>
<option value="1913"> 1913</option>
<option value="1912"> 1912</option>
<option value="1911"> 1911</option>
<option value="1910"> 1910</option>
<option value="1909"> 1909</option>
<option value="1908"> 1908</option>
<option value="1907"> 1907</option>
<option value="1906"> 1906</option>
<option value="1905"> 1905</option>
<option value="1904"> 1904</option>
<option value="1903"> 1903</option>
<option value="1902"> 1902</option>
<option value="1901"> 1901</option>
<option value="1900"> 1900</option>
                        </select></div>

                    <div class="s11"><?php echo JText::_('MOD_HCALBUS_CITTADINANZA');?><span class="red">*</span></div>
                   <div class="s6"> <input required="true" type="text" value="" style="width: 96%;" class="txt cittadinanza" id="cittadinanza" name="cittadinanza"></div>

                </div></div>
                <div class="rightCol"> <div class="s5">
 <div class="s11"><?php echo JText::_('MOD_HCALBUS_SESSO');?><span class="red">*</span></div> <div class="s6">

    <input type="radio" checked="" value="0" name="sesso"><?php echo JText::_('MOD_HCALBUS_MASCHILE');?>  
    <input type="radio" value="1" name="sesso"><?php echo JText::_('MOD_HCALBUS_FEMMINILE');?> </div>

<div class="s11"><?php echo JText::_('MOD_HCALBUS_NAZIONE');?><span class="red">*</span> </div><div class="s10" style="padding-right: 20px;"><input required="true" type="text"  value="" class="txt" id="nazione_nascita" name="nazione_nascita"></div>
<div class="sleft"><?php echo JText::_('MOD_HCALBUS_CITTA');?><span class="red">*</span> </div><div class="s11"><input required="true" type="text"  value="" class="txt"  id="citta_nascita" name="citta_nascita"></div> 




               
</div>
       
       

                        
             

       
               
                <div class="leftCol">

                        <div class="date_inputs_name  "  style="margin-top: 20px;">
<div class="s11"><?php echo JText::_('MOD_HCALBUS_NUMEROPASS');?><span class="red">*</span></div> <div class="sleft"><input required="true" type="text" value="" class="txt" style="width: 96%;"  id="numero_passaporto" name="numero_passaporto"></div>

<div class="s11"><?php echo JText::_('MOD_HCALBUS_RILASCIATO');?><span class="red">*</span></div><div class="sleft">
                            <select required=true onchange="recalculate()" class="day" id="rilasciato_il_day" name="rilasciato_il_day">
                                <?php for ($i=1;$i<=31;$i++)
                                {
                                    if ($i<10)
                                        $d = '0'.$i;
                                    else
                                        $d = $i;

                                    if (date('d')==$d)
                                        $sel = 'selected=""';
                                    else
                                        $sel = '';

                                    echo '<option '.$sel.' value="'.$d.'">'.$d.'</option>';
                                }
                                ?>
                        </select>
                            <select required=true onchange="recalculate()" class="month" id="rilasciato_il_month" name="rilasciato_il_month">
                            <?php
                                $month_array = array(
                                "01" => JText::_('JANUARY'),
                                "02" => JText::_('FEBRUARY'),
                                "03" => JText::_('MARCH'),
                                "04" => JText::_('APRIL'),
                                "05" => JText::_('MAY'),
                                "06" => JText::_('JUNE'),
                                "07" => JText::_('JULY'),
                                "08" => JText::_('AUGUST'),
                                "09" => JText::_('SEPTEMBER'),
                                "10" => JText::_('OCTOBER'),
                                "11" => JText::_('NOVEMBER'),
                                "12" => JText::_('DECEMBER')
                                );
                                foreach ($month_array as $k=>$v){
                                    if (date('m')==$k)
                                        $sel = 'selected=""';
                                    else
                                        $sel = '';

                                    echo '<option '.$sel.' value="'.$k.'">'.$v.'</option>';
                                }
                            ?>
                        </select>
                            <select required=true onchange="recalculate()" class="year" id="rilasciato_il_year" name="rilasciato_il_year">
                            <?php for ($i=1990;$i<=intval(date('Y'));$i++)
                            {
                                if (date('Y')==$i)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select></div>
<div class="s11"><?php echo JText::_('MOD_HCALBUS_VALIDOFINO');?><span class="red">*</span></div>
                           <div class="sleft"> <select required=true onchange="recalculate()" class="day" id="valido_fino_al_day" name="valido_fino_al_day">
                            <?php for ($i=1;$i<=31;$i++)
                            {
                                if ($i<10)
                                    $d = '0'.$i;
                                else
                                    $d = $i;

                                if (date('d')==$d)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$d.'">'.$d.'</option>';
                            }
                            ?>
                        </select>
                            <select required=true onchange="recalculate()" class="month" id="valido_fino_al_month" name="valido_fino_al_month">
                            <?php
                            foreach ($month_array as $k=>$v){
                                if (date('m')==$k)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$k.'">'.$v.'</option>';
                            }
                            ?>
                        </select>
                            <select required=true onchange="recalculate()" class="year" id="valido_fino_al_year" name="valido_fino_al_year">
                            <?php for ($i=intval(date('Y'));$i<=intval(date('Y'))+10;$i++)
                            {
                                if (date('Y')==$i)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select>
                        </div>
                </div></div></div>
                <div class="clear" style="clear: both;"></div>
        


<div class="s5">       
<div class="s11"><?php echo JText::_('MOD_HCALBUS_RESIDENZA');?><span class="red">*</span></div>
<div class="s6"><input required="true" type="text" value=""  style="width: 96%;"  class="txt" id="nazione_residenza" name="nazione_residenza"></div>
<div class="s11"><?php echo JText::_('MOD_HCALBUS_DIRESIDENZA');?><span class="red">*</span><br><small><?php echo JText::_('MOD_HCALBUS_DIRESIDENZASMALL');?></small></div>
<div class="s6"><input required="true" type="text" style="width: 96%;"   value="" class="txt" id="indirizzo_residenza" name="indirizzo_residenza"></div>

 </div>
              <div class="clear" style="clear: both;"></div>
              


	

                <div class="s5">
                 <div class="s11"><?php echo JText::_('MOD_HCALBUS_EMAIL');?><span class="red">*</span></div>
                  <div class="s6"> <input required="true"  type="text" value="" style="width: 96%;"class="txt" id="mail" name="mail"></div>
                  <div class="s11"><?php echo JText::_('MOD_HCALBUS_TELEFONO');?><span class="red">*</span></div>
                 <div class="s6">   <input required="true" type="text" style="width: 96%;"   value="" class="txt" id="telefono" name="telefono"></div>
                </div>  
                <div class="clear"  style="clear: both;" ></div>
      

                <div class="hr"></div>
                <div class="s5" >
                        <div class="s11"><?php echo JText::_('MOD_HCALBUS_VISITARE');?>  <span class="red">*</span></div>
                        <div class="s15">   <input required="true" type="text"  style="width: 100%;"  value="" class="txt" id="citta_visitare" name="citta_visitare"></div>
                </div>
				         <div class="clear"  style="clear: both;" ></div>
                        <div class="s5">
                <div class="s11" ">

<?php echo JText::_('MOD_HCALBUS_DAL');?><span class="red">*</span></div><div class="sleft"  style="width: 37%;" >
                    <select required=true onchange="recalculate()" class="day " id="dal_day" name="dal_day">
                            <?php for ($i=1;$i<=31;$i++)
                            {
                                if ($i<10)
                                    $d = '0'.$i;
                                else
                                    $d = $i;

                                if (date('d')==$d)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$d.'">'.$d.'</option>';
                            }
                            ?>
                        </select>
                    <select required=true onchange="recalculate()" class="month " id="dal_month" name="dal_month">
                            <?php
                            foreach ($month_array as $k=>$v){
                                if (date('m')==$k)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$k.'">'.$v.'</option>';
                            }
                            ?>
                        </select>
                    <select required=true onchange="recalculate()" class="year " id="dal_year" name="dal_year">
                            <?php for ($i=intval(date('Y'));$i<=intval(date('Y'))+10;$i++)
                            {
                                if (date('Y')==$i)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select></div>
   <div class="s11" "> <?php echo JText::_('MOD_HCALBUS_AL');?><span class="red">*</span></div> <div class="sleft">
                        <span  id="alDiv"></span>
<input type="hidden" id="al" name="al"></div>
                     

                </div>
                <div class="clear" style="clear: both;"></div>
        

                       
              
<div class="s5">
   <div class="s11"   > 
<?php echo JText::_('MOD_HCALBUS_CONSOLATO');?><span class="red">*</span>  </div>
<div    class="s6" >
 <input required="true" type="text" value="" class="txt"  placeholder="<?php echo JText::_('MOD_HCALBUS_CONSOLATOINFO');?>"   style="width: 96%;" id="consolato_quale" name="consolato_quale"></div>
 

   <div class="s11"  >
  <?php echo JText::_('MOD_HCALBUS_CITTA');?><span class="red">*</span> </div> 

                      
   <div    class="s6" >
<input required="true" type="text" value="" placeholder="<?php echo JText::_('MOD_HCALBUS_CITTAINFO');?>"  class="txt" id="citta"  style="width: 96%;"  name="citta"></div>


</div> 
                        <div class="clear"  style="clear: both;" ></div>
                
<div class="s5">
                       <div class="s11">
					  
                                <?php echo JText::_('MOD_HCALBUS_LAVORO');?><span class="red">*</span> </div>	
							
   <div class="s6"><input required="true" type="text" value="" class="txt" placeholder="<?php echo JText::_('MOD_HCALBUS_LAVOROINFO');?>" id="ragione_sociale" style="width: 96%;" name="ragione_sociale"></div>   
   
   
   <div class="s11">							
                         <?php echo JText::_('MOD_HCALBUS_MANSIONE');?> <span class="red">*</span></div>	
                               
                       
   <div class="s6" >
                        <input required="true" type="text" value="" class="txt"  style="width: 96%;" placeholder="<?php echo JText::_('MOD_HCALBUS_MANSIONEINFO');?>" id="posizione_nella_ditta" name="posizione_nella_ditta"></div> </div>

                        <div class="clear" style="clear: both;" ></div>
               
                       
        <div class="row1">
                <div class="leftCol">
                        <div class="posizione_nella_ditta">
                        </div>
                        <div class="clear"></div>
                </div>
                <div class="s5">
				<div class="s11">
 <?php echo JText::_('MOD_HCALBUS_INDIRIZZO');?><span class="red">*</span></div>
<div class="s6"><input required="true" type="text" value=""  placeholder="<?php echo JText::_('MOD_HCALBUS_INDIRIZZOINFO');?>" class="txt" style="width: 96%;" id="indirizzo_lavoro" name="indirizzo_lavoro"></div>
 <div class="s11">
 <?php echo JText::_('MOD_HCALBUS_TELEFONO');?> <span class="red">*</span></div>
<div class="s6">
<input required="true" type="text" value="" class="txt"  placeholder="<?php echo JText::_('MOD_HCALBUS_TELEFONOINFO');?>" style="width: 96%;" id="telefono_lavoro" name="telefono_lavoro"></div>
			<div class="clear" style="clear: both;" ></div>
		</div>

<div class="hr"></div>


<br>
<div class="uk-form">
   <input type="checkbox" onclick="recalculate()" value="1" name="ho_medica"> Ho bisogno dell’assicurazione medica
</div>

<br>
                <p>
                        Sono interessato alla procedura completa:
                    <label for="radiono"><input type="radio" id="radiono" checked="" value="0" onclick="recalculate()" name="procedura_completa"> NO</label>
                    <label style="white-space:nowrap;" for="radio1"><input type="radio" id="radio1" value="1" onclick="recalculate()" name="procedura_completa">&nbsp;SI,&nbsp;a&nbsp;Milano</label>
                    <label style="white-space:nowrap;" for="radio2"><input type="radio" id="radio2" value="2" onclick="recalculate()" name="procedura_completa">&nbsp;SI,&nbsp;a&nbsp;Palermo</label>
                    <label style="white-space:nowrap;" for="radio3"><input type="radio" id="radio3" value="3" onclick="recalculate()" name="procedura_completa">&nbsp;SI,&nbsp;a&nbsp;Roma</label>
                    <label style="white-space:nowrap;" for="radio6"><input type="radio" id="radio6" value="6" onclick="recalculate()" name="procedura_completa">&nbsp;SI,&nbsp;a&nbsp;Genova</label>
<br></p>
      <div class="hr"></div>


<br>
  <p>
                      <input type="checkbox" id="compila" name="ho_compila" value="1" onclick="recalculate(1)"> Ho bisogno della compilazione del modulo consolare per mio conto
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

    <input type="checkbox" onclick="recalculate()" value="1" name="ho_urgente"> Ho bisogno della procedura urgente
</p>
<br>
      <div class="hr"></div>

                <p>
                        Ho bisogno della registrazione:<span class="red">*</span>

    <input type="checkbox" value="1" onclick="recalculate()" name="ho_registrazione_1"> a Mosca
    <input type="checkbox" value="1" onclick="recalculate()" name="ho_registrazione_2"> a San Pietroburgo
                </p>
      <div class="hr"></div>
      <p>
                        Mettete in preventivo le spese di spedizione:

    <input type="radio" checked="" value="0" onclick="recalculate()" name="spese_spedizione"> NO
    <input type="radio" value="1" onclick="recalculate()" name="spese_spedizione"> Solo in ritorno
    <input type="radio" value="2" onclick="recalculate()" name="spese_spedizione"> In andata ed in ritorno

                </p>
<br>
      <div class="hr"></div>
      <p>
                        Ho bisogno anche di:
                </p>
                <p>

   <input type="checkbox" value="1" onclick="recalculate()" name="hotel"> Hotel
   <input type="checkbox" value="1" onclick="recalculate()" name="appartamento"> Appartamento
   <input type="checkbox" value="1" name="volo_aereo"> Volo aereo
   <input type="checkbox" value="1" name="transfers"> Transfers
   <input type="checkbox" value="1" name="escursioni"> Escursioni

                </p>


      <div class="hr"></div>
                <p>

    <input type="radio" checked="" value="0" name="parlo"> Parlo solo italiano
    <input type="radio" value="1" name="parlo"> Parlo inglese o russo
                                   </p>
<br>
<div class="row1"><div class="s10"><?php echo JText::_('MOD_HCALBUS_COMMENT');?></div>
<TEXTAREA  id="comment" name="comment" WRAP="virtual" COLS="40" style="width: 85%;"  ROWS="3"></TEXTAREA></div><br>

                <h3 class="costo">COSTO DELLA PRATICA VISTO*: <span id="TotalPrice">0 Euro</span></h3>

<br>
         <input type="hidden" value="0" id="total_price" name="total_price">
     <!-- <div id="Debug" style="border: 1px solid rgb(0, 0, 0); padding: 20px; background: rgb(255, 255, 255) none repeat scroll 0%; width: 200px; -moz-background-clip: -moz-initial; -moz-background-origin: -moz-initial; -moz-background-inline-policy: -moz-initial;">L=0 приглашение<br>LF=0 стоимость ребенка<br>B=0 страховка<br>C=0 регистрация<br>O=0 отправка оригиналов<br>D=0 консульский сбор<br>E=0 услуга<br>F=0 почта<br><br>R = L + LF * iFigli + (B + E) * iPersons + C + O + D + F<br>R=0 <br></div>-->
         <p align="justify" class="Arial">
                 * il costo e’ relativo a cittadini italiani, per gli stranieri i costi delle tasse consolari possono variare, e di conseguenza il totale dell’importo. Nel caso in cui abbiate segnalato l’intenzione di prenotare un hotel ed un appartamento, lo sconto automaticamente calcolato sara` effettivo soltanto dopo il pagamento della prenotazione.
         </p>

<br>
            <div class="subm">
                        <div class="submbutton"><input type="submit" class="uk-button uk-button-primary" value="INOLTRA LA RICHIESTA" name="sub"><div style="padding-top: 10px;">
                 PRIMA DI PROCEDERE CONTROLLATE ACCURATAMENTE TUTTE LE INFORMAZIONI INSERITE</div></div>
                   <div class="clear"></div>
</fieldset>

<input type="hidden" name="smail" value="1">
         </div>

      </form>

 <div id="HiddenFigli">
                                <div class="FigliBlock">

                        <h3><span>%#.</span></h3>
                                <div class="row1">
                <div class="leftCol">
Nome:<span class="red">*</span> <input type="text" class="txt nome" id="figli_nome_%#" name="figli_nome_%#">
                </div>
                <div class="rightCol">
Cognome:<span class="red">*</span> <input type="text" class="txt cognome" id="figli_cognome_%#" name="figli_cognome_%#">
                </div>
                <div class="clear"></div>
        </div>

                                <div class="row1">
                <div class="leftCol">
Data di nascita:<span class="red">*</span>
                                                        <select onchange="recalculate()" class="day" id="figli_nascita_%#_day" name="figli_nascita_%#_day">
                          <option value="01"> 01</option>
<option value="02"> 02</option>
<option value="03"> 03</option>
<option value="04"> 04</option>
<option value="05"> 05</option>
<option value="06"> 06</option>
<option value="07"> 07</option>
<option value="08"> 08</option>
<option value="09"> 09</option>
<option value="10"> 10</option>
<option value="11"> 11</option>
<option value="12"> 12</option>
<option value="13"> 13</option>
<option value="14"> 14</option>
<option value="15"> 15</option>
<option value="16"> 16</option>
<option value="17"> 17</option>
<option value="18"> 18</option>
<option value="19"> 19</option>
<option value="20"> 20</option>
<option value="21"> 21</option>
<option value="22"> 22</option>
<option value="23"> 23</option>
<option value="24"> 24</option>
<option value="25"> 25</option>
<option value="26"> 26</option>
<option value="27"> 27</option>
<option value="28"> 28</option>
<option value="29"> 29</option>
<option value="30"> 30</option>
<option value="31"> 31</option>


                        </select>
                        <select onchange="recalculate()" class="month" id="figli_nascita_%#_month" name="figli_nascita_%#_month">
                        <option selected="" value="0"> Mese </option>
<option value="01"> Gennaio</option>
<option value="02"> Febbraio</option>
<option value="03"> Marzo</option>
<option value="04"> Aprile</option>
<option value="05"> Maggio</option>
<option value="06"> Giugno</option>
<option value="07"> Luglio</option>
<option value="08"> Agosto</option>
<option value="09"> Settembre</option>
<option value="10"> Ottobre</option>
<option value="11"> Novembre</option>
<option value="12"> Dicembre</option>
                        </select>
                        <select onchange="recalculate()" class="year" id="figli_nascita_%#_year" name="figli_nascita_%#_year">
                             <option selected="" value="Anno"> Anno</option>
<option value="2014"> 2014</option>
<option value="2013"> 2013</option>
<option value="2012"> 2012</option>
<option value="2011"> 2011</option>
<option value="2010"> 2010</option>
<option value="2009"> 2009</option>
<option value="2008"> 2008</option>
<option value="2007"> 2007</option>
<option value="2006"> 2006</option>
<option value="2005"> 2005</option>
<option value="2004"> 2004</option>
<option value="2003"> 2003</option>
<option value="2002"> 2002</option>
<option value="2001"> 2001</option>
<option value="2000"> 2000</option>
<option value="1999"> 1999</option>
<option value="1998"> 1998</option>
<option value="1997"> 1997</option>
<option value="1996"> 1996</option>
<option value="1995"> 1995</option>
<option value="1994"> 1994</option>
<option value="1993"> 1993</option>
<option value="1992"> 1992</option>
<option value="1991"> 1991</option>
<option value="1990"> 1990</option>
<option value="1989"> 1989</option>
<option value="1988"> 1988</option>
<option value="1987"> 1987</option>
<option value="1986"> 1986</option>
<option value="1985"> 1985</option>
<option value="1984"> 1984</option>
<option value="1983"> 1983</option>
<option value="1982"> 1982</option>
<option value="1981"> 1981</option>
<option value="1980"> 1980</option>
<option value="1979"> 1979</option>
<option value="1978"> 1978</option>
<option value="1977"> 1977</option>
<option value="1976"> 1976</option>
<option value="1975"> 1975</option>
<option value="1974"> 1974</option>
<option value="1973"> 1973</option>
<option value="1972"> 1972</option>
<option value="1971"> 1971</option>
<option value="1970"> 1970</option>
<option value="1969"> 1969</option>
<option value="1968"> 1968</option>
<option value="1967"> 1967</option>
<option value="1966"> 1966</option>
<option value="1965"> 1965</option>
<option value="1964"> 1964</option>
<option value="1963"> 1963</option>
<option value="1962"> 1962</option>
<option value="1961"> 1961</option>
<option value="1960"> 1960</option>
<option value="1959"> 1959</option>
<option value="1958"> 1958</option>
<option value="1957"> 1957</option>
<option value="1956"> 1956</option>
<option value="1955"> 1955</option>
<option value="1954"> 1954</option>
<option value="1953"> 1953</option>
<option value="1952"> 1952</option>
<option value="1951"> 1951</option>
<option value="1950"> 1950</option>
<option value="1949"> 1949</option>
<option value="1948"> 1948</option>
<option value="1947"> 1947</option>
<option value="1946"> 1946</option>
<option value="1945"> 1945</option>
<option value="1944"> 1944</option>
<option value="1943"> 1943</option>
<option value="1942"> 1942</option>
<option value="1941"> 1941</option>
<option value="1940"> 1940</option>
<option value="1939"> 1939</option>
<option value="1938"> 1938</option>
<option value="1937"> 1937</option>
<option value="1936"> 1936</option>
<option value="1935"> 1935</option>
<option value="1934"> 1934</option>
<option value="1933"> 1933</option>
<option value="1932"> 1932</option>
<option value="1931"> 1931</option>
<option value="1930"> 1930</option>
<option value="1929"> 1929</option>
<option value="1928"> 1928</option>
<option value="1927"> 1927</option>
<option value="1926"> 1926</option>
<option value="1925"> 1925</option>
<option value="1924"> 1924</option>
<option value="1923"> 1923</option>
<option value="1922"> 1922</option>
<option value="1921"> 1921</option>
<option value="1920"> 1920</option>
<option value="1919"> 1919</option>
<option value="1918"> 1918</option>
<option value="1917"> 1917</option>
<option value="1916"> 1916</option>
<option value="1915"> 1915</option>
<option value="1914"> 1914</option>
<option value="1913"> 1913</option>
<option value="1912"> 1912</option>
<option value="1911"> 1911</option>
<option value="1910"> 1910</option>
<option value="1909"> 1909</option>
<option value="1908"> 1908</option>
<option value="1907"> 1907</option>
<option value="1906"> 1906</option>
<option value="1905"> 1905</option>
<option value="1904"> 1904</option>
<option value="1903"> 1903</option>
<option value="1902"> 1902</option>
<option value="1901"> 1901</option>
<option value="1900"> 1900</option>
                        </select>
                </div>
                <div class="rightCol">
Sesso:<span class="red">*</span>
                                <input type="radio" checked="checked" value="0" name="figli_sesso_%#"> M
                                <input type="radio" value="1" name="figli_sesso_%#"> F
                </div>
                <div class="clear"></div>
        </div>


                                <div class="row1">
                <div class="leftCol">
Cittadinanza:<span class="red">*</span> <input type="text" class="txt cittadinanza" id="figli_cittadinanza_%#" name="figli_cittadinanza_%#">
                </div>
                <div class="rightCol">
                </div>
                <div class="clear"></div>
        </div>
                </div>
        </div>
<script src="/media/jui/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo JURI::root(true).'/modules/mod_hcalbus/script/visto_business.js?v10'; ?>"></script>

              


                </div>
<?php } ?>