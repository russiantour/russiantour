/////////////////////////////////////////////////////////////////////////////////////////
function $(sId){
    return document.getElementById(sId);
}

var ready_to_submit = true;

/////////////////////////////////////////////////////////////////////////////////////////
/* Расчет */

var
    iCityId = 0,			// Город Италии ( задается полем procedura_completa )
    iPersons = 1,			// Кол-во человек ( figli + 1 )
    iFast = 0,				// Срочная/обычная (1/0)
    iPeriod = 0,			//	Срок пребывания
    iPostMail = 0,			// Отправка почты
    iLastMedica = 0,

    A, B, C, D, E, F,

    mEls = $('BigForm').elements,
    i;


/////////////////////////////////////////////////////////////////////////////////////////
function setFigli(elThis){
    var
        iCount = elThis.value * 1;
    sFigliCode = $('HiddenFigli').innerHTML,
        sRez = '';

    for(var i=1 ; i <= iCount ; i++ ){
        sRez+= sFigliCode.replace( /%#/g , i);
    }
    $('FigliBox').innerHTML = sRez;

    iPersons = iCount + 1;

    recalculate();
}


/////////////////////////////////////////////////////////////////////////////////////////
function setProceduraCompleta(){
    var els = mEls['procedura_completa'];

    iCityId = 0;
    for( i=0 ; i < els.length ; i++){
        if( els[i].checked ){
            iCityId = els[i].value * 1;
            break;
        }
    }

    if( iCityId ){
        jQuery('#ho_medica')[0].checked = true;
        jQuery('#ho_medica')[0].disabled = true;
        jQuery('input[name=ho_medica]')[0].checked = true;
        mEls['ho_medica'].checked = true;
        //mEls['ho_medica'].disabled = true;
    }
    else {
        jQuery('#ho_medica')[0].disabled = false;
        //mEls['ho_medica'].disabled = false;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
function setPostMail(){
    var els = mEls['spese_spedizione'];

    iPostMail = 0;
    for( i=0 ; i < els.length ; i++){
        if( els[i].checked ){
            iPostMail = els[i].value * 1;
            break;
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
function setPeriod(elThis){
    var
        dTo = new Date( mEls['al_year'].value , mEls['al_month'].value - 1 , mEls['al_day'].value),
        dFrom = new Date( mEls['dal_year'].value , mEls['dal_month'].value - 1 , mEls['dal_day'].value);

    iPeriod = 1 + (Date.parse(dTo) - Date.parse(dFrom)) / 1000 / 60 / 60 / 24 ;
}

/////////////////////////////////////////////////////////////////////////////////////////
function setRegistration(){
    var el, iId;

    for( i=0 ; i < mRussianCities.length ; i++){
        iId = mRussianCities[i][0];
        el = mEls['ho_registrazione_' + iId];
        if(el && el.checked){
            C += mRegistrationPrice[iId][0];
        }
    }
}


/////////////////////////////////////////////////////////////////////////////////////////
function recalculate(check){
    var ready_to_submit = true;

    var now = Date.parse(new Date()).get('time');

    jQuery('#nascita_year_error').remove();
    if (jQuery('#nascita_month').val() && jQuery('#nascita_year').val())
    {
        var birth = [jQuery('#nascita_month').val(),jQuery('#nascita_day').val(),jQuery('#nascita_year').val()];
        if (((now-Date.parse(birth.join('-')).get('Time'))/1000/3600/24)<1)
        {
            if (!jQuery('#nascita_year_error').length)
                jQuery('#nascita_year').after('<br><div id="nascita_year_error" style="color:red;">Birthday must be early then today</div>')
            ready_to_submit = false;
        }
    }
    else
        if (!jQuery('#nascita_year_error').length)
            jQuery('#nascita_year').after('<br><div id="nascita_year_error" style="color:red;">Set the birthday!</div>')

    var dal = [jQuery('#dal_month').val(),jQuery('#dal_day').val(),jQuery('#dal_year').val()]
    var al = [jQuery('#al_month').val(),jQuery('#al_day').val(),jQuery('#al_year').val()]
    if(((Date.parse(al.join('-')).get('Time')-Date.parse(dal.join('-')).get('Time'))/1000/3600/24)>30)
    {
        if (!jQuery('#al_year_error').length)
            jQuery('#al_year').after('<br><div id="al_year_error" style="color:red;">Check out not more than 30 days</div>')
        ready_to_submit = false;
    }
    else if (((Date.parse(al.join('-')).get('Time')-Date.parse(dal.join('-')).get('Time'))/1000/3600/24)<1)
    {
        if (rec_flag && !jQuery('#al_year_error').length)
            jQuery('#al_year').after('<br><div id="al_year_error" style="color:red;">La data di partenza deve essere almeno il giorno successivo rispetto alla data di arrivo</div>')
        ready_to_submit = false;
    }
    else
    {
        jQuery('#al_year_error').remove();
    }
    rec_flag = true;
    iFast = mEls['ho_urgente'].checked * 1;

    setProceduraCompleta();

// приглашение
    A =
        mInvitationPrice[( iCityId > 0 ) * 1] *
            (
                !(mEls['hotel'].checked || mEls['appartamento'].checked)
                )
    ;

// страховка
    B = 0;
    if(mEls['ho_medica'].checked){
        setPeriod();
        for( i=0 ; i < mInsurancePrice.length ; i++){
            B = mInsurancePrice[i][1];
            if( iPeriod <= mInsurancePrice[i][0] ) break;
        }
    }

// регистрация
    C = 0;
    setRegistration();

// консульский сбор
    D = mConsulPrice[iCityId][iFast];


// услуга
    E = mServicePrice[iCityId][iFast];

// почта
    setPostMail();
    F = mPostPrice[iCityId][iFast] * iPostMail;

// Кол-во персон
    //iPersons = $('figli').value * 1 + 1;
    iPersons = 1;

// 
    if (document.getElementById("compila").checked == true)	// == 1
    {
        //G = 12;
        G = mCompila;
    }
    else	G = 0;

    R = (A + B + E) * iPersons + C + D + F + G;

    $('TotalPrice').innerHTML = R + ' Euro';
    $('total_price').value = R;

    /*
     var sInfo =
     //		'iPeriod=' + iPeriod + ' дней\n' +
     'A=' + A + ' приглашение\n' +
     'B=' + B + ' страховка\n' +
     'C=' + C + ' регистрация\n' +
     'D=' + D + ' консульский сбор\n' +
     'E=' + E + ' услуга\n' +
     'F=' + F + ' почта\n' +
     '\n' +
     'R = (A + B + E) * (1 + Figli) + C + D + F\n' +
     'R=' + R + ' \n' +
     ''
     ;
     */
//	$('Debug').innerHTML = sInfo.replace( /\n/g , '<br />');
//	$('Debug').style.top = scrollTop() + 100 + 'px';
    /**/

    jQuery('#BigForm').on('submit',function(){
        recalculate();
        jQuery('#BigForm input[type=text][required=true],#BigForm select[required=true]').each(function(){
            if (!jQuery(this).val() || jQuery(this).val()=='0')
            {
                jQuery(this).css('color','red');
                ready_to_submit = false;
            }
            else
                jQuery(this).css('color','');
        });
        if (ready_to_submit) {
            jQuery('#BigForm').off('submit');
            jQuery('#BigForm').submit();
        }
        else
        {
            jQuery.scrollTo('#BigForm');
            return false;
        }
    })
}
var rec_flag = false;
window.onload = recalculate;
