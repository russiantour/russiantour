<?php

/**
 * @package     Joomla.Tutorials
 * @subpackage  Module
 * @copyright   (C) 2012 http://it-complex.by
 * @license     License GNU General Public License version 2 or later; see LICENSE.txt
 */
   
// No direct access to this file
defined('_JEXEC') or die;

if ($_POST)
{
    switch($_POST['procedura_completa']){
        case 0: $procedura_completa = 'NO'; break;
        case 1: $procedura_completa = 'SI, a Milano'; break;
        case 2: $procedura_completa = 'SI, a Palermo'; break;
        case 3: $procedura_completa = 'SI, a Roma'; break;
        case 6: $procedura_completa = 'SI, a Genova'; break;
        default: $procedura_completa = 'NO';
    }
    switch($_POST['spese_spedizione']){
        case 0: $spese_spedizione = 'NO'; break;
        case 1: $spese_spedizione = 'Solo in ritorno'; break;
        case 2: $spese_spedizione = 'In andata ed in ritorno'; break;
        default: $spese_spedizione = 'NO';
    }

    $Ho_bisogno_anche_di = array();
    if (isset($_POST['hotel'])) $Ho_bisogno_anche_di[]='Hotel';
    if (isset($_POST['appartamento'])) $Ho_bisogno_anche_di[]='Appartamento';
    if (isset($_POST['volo_aereo'])) $Ho_bisogno_anche_di[]='Volo aereo';
    if (isset($_POST['transfers'])) $Ho_bisogno_anche_di[]='Transfers';
    if (isset($_POST['escursioni'])) $Ho_bisogno_anche_di[]='Escursioni';

    switch($_POST['parlo']){
        case 0: $parlo = 'Parlo solo italiano'; break;
        case 1: $parlo = 'Parlo inglese o russo'; break;
        default: $parlo = 'NO';
    }

    $body = 'Visto turistico: Visto turistico compilare
'.$_SERVER['HTTP_REFERER'].'
Nome: '.($_POST['nome']?(trim($_POST['nome'])):'NO').'
Cognome: '.($_POST['cognome']?(trim($_POST['cognome'])):'NO').'
Data di nascita: '.$_POST['nascita_day'].'.'.$_POST['nascita_month'].'.'.$_POST['nascita_year'].'
Nazionalita: '.($_POST['nazionalita']?(trim($_POST['nazionalita'])):'NO').'
Numero di passaporto: '.($_POST['numero_passaporto']?(trim($_POST['numero_passaporto'])):'NO').'
Sesso: '.($_POST['sesso']?'F':'M').'
Citta` da visitare: '.($_POST['citta_visitare']?(trim($_POST['citta_visitare'])):'NO').'
Dal: '.$_POST['dal_day'].'.'.$_POST['dal_month'].'.'.$_POST['dal_year'].'
Al: '.$_POST['al_day'].'.'.$_POST['al_month'].'.'.$_POST['al_year'].'
E-Mail: '.($_POST['mail']?(trim($_POST['mail'])):'NO').'
Telefono: '.($_POST['telefono']?(trim($_POST['telefono'])):'NO').'
Citta di residenza: '.($_POST['citta']?(trim($_POST['citta'])):'NO').'
Ho bisogno dell-assicurazione medica: '.(isset($_POST['ho_medica'])?'SI':'NO').'
Sono interessato alla procedura completa: '.$procedura_completa.'
Ho bisogno della procedura urgente: '.(isset($_POST['ho_urgente'])?'SI':'NO').'
Ho bisogno della compilazione del modulo consolare per mio conto : '.(isset($_POST['ho_compila'])?'SI':'NO').'
Ho bisogno della registrazione: '.(isset($_POST['ho_registrazione_1'])?'a Mosca':'').
        ((isset($_POST['ho_registrazione_1'])&&isset($_POST['ho_registrazione_2']))?',':'').
        ((!isset($_POST['ho_registrazione_1'])&&!isset($_POST['ho_registrazione_2']))?'NO':'').
        (isset($_POST['ho_registrazione_2'])?'a San Pietroburgo ':'').'
Mettete in preventivo le spese di spedizione: '.$spese_spedizione.'
Ho bisogno anche di: '.((count($Ho_bisogno_anche_di)>0)?(implode(',',$Ho_bisogno_anche_di)):'NO').'
Parlo: '.$parlo.'
Note = '.$_POST['comment'].'

SUM IN PAGE = '.$_POST['total_price'].'
IP: '.$_SERVER["REMOTE_ADDR"].'
Today: '.date('d-m-Y, H:i');
    if(JFactory::getMailer()->sendMail(JFactory::getConfig()->get('mailfrom'), JFactory::getConfig()->get('fromname'), explode(',',$params->get('hotelcalculator_email')), 'VISTO TURISTICO', $body/*print_r($_POST,true)*/))
    {
echo '<div data-uk-alert="" class="uk-alert uk-alert-large uk-alert-notice">
<button class="uk-alert-close uk-close" type="button"></button>
<h2>';
echo JText::_('MOD_HOTELCALCULATOR_AVVISO');
echo ' </h2><p>';
echo JText::_('MOD_HOTELCALCULATOR_AVVISOINFO');
echo '</p></div>';
        $notshow = true;
    }
}
if (!isset($notshow))
{
?>
<style>#HiddenFigli {display: none;}</style>
<div id="C">
<script type="text/javascript">
var mCompila = <?php echo $params->get('compila'); ?>;

var mItalianCities = [
[1 , "Milano" ],
[2 , "Palermo" ],
[3 , "Roma" ],
[6 , "Genova" ]
];
var mRussianCities = [
[1 , "Mosca" ],
[2 , "St.Petersburg" ]
];
var mInvitationPrice = [
    <?php echo $params->get('invitationprice1'); ?>,
    <?php echo $params->get('invitationprice2'); ?>]
var mInsurancePrice = [
[7,<?php echo $params->get('insuranceprice1'); ?> ],
[14,<?php echo $params->get('insuranceprice2'); ?> ],
[21,<?php echo $params->get('insuranceprice3'); ?> ],
[30,<?php echo $params->get('insuranceprice4'); ?> ]
];
var mServicePrice = [];
 mServicePrice[0] = [0,0];
mServicePrice[1] = [<?php echo $params->get('serviceprice1'); ?>,<?php echo $params->get('serviceprice5'); ?>];
mServicePrice[2] = [<?php echo $params->get('serviceprice2'); ?>,<?php echo $params->get('serviceprice6'); ?>];
mServicePrice[3] = [<?php echo $params->get('serviceprice3'); ?>,<?php echo $params->get('serviceprice7'); ?>];
mServicePrice[6] = [<?php echo $params->get('serviceprice4'); ?>,<?php echo $params->get('serviceprice8'); ?>];

var mRegistrationPrice = [];
 mRegistrationPrice[0] = [0,0];
mRegistrationPrice[1] = [<?php echo $params->get('registrationprice1'); ?>,0];
mRegistrationPrice[2] = [<?php echo $params->get('registrationprice2'); ?>,0];
var mConsulPrice = [];
 mConsulPrice[0] = [0,0];
mConsulPrice[1] = [<?php echo $params->get('consulprice1'); ?>,<?php echo $params->get('consulprice5'); ?>];
mConsulPrice[2] = [<?php echo $params->get('consulprice2'); ?>,<?php echo $params->get('consulprice6'); ?>];
mConsulPrice[3] = [<?php echo $params->get('consulprice3'); ?>,<?php echo $params->get('consulprice7'); ?>];
mConsulPrice[6] = [<?php echo $params->get('consulprice4'); ?>,<?php echo $params->get('consulprice8'); ?>];

var mPostPrice = [];
 mPostPrice[0] = [0,0];
mPostPrice[1] = [<?php echo $params->get('postprice1'); ?>,<?php echo $params->get('postprice5'); ?>];
mPostPrice[2] = [<?php echo $params->get('postprice2'); ?>,<?php echo $params->get('postprice6'); ?>];
mPostPrice[3] = [<?php echo $params->get('postprice3'); ?>,<?php echo $params->get('postprice7'); ?>];
mPostPrice[6] = [<?php echo $params->get('postprice4'); ?>,<?php echo $params->get('postprice8'); ?>];



</script>
<div class="s4">
        <h1><?php echo JText::_('MOD_HOTELCALCULATOR_INFO'); ?></h1>
<p style="text-align: justify;" ><?php echo JText::_('MOD_HOTELCALCULATOR_INFO1'); ?></p>
<br>
<?php echo "<script>var txt='".JText::_('MOD_HOTELCALCULATOR_DEVE')."';</script>"; ?> 
<?php echo "<script>var txz='".JText::_('MOD_HOTELCALCULATOR_30DAY')."';</script>"; ?>     
<?php echo "<script>var txx='".JText::_('MOD_HOTELCALCULATOR_BIRTHDAY')."';</script>"; ?>     
<?php echo "<script>var txx='".JText::_('MOD_HOTELCALCULATOR_BIRTHDAY')."';</script>"; ?>     

<fieldset class="uk-form">

 <form id="BigForm" class="BigForm" method="post" action="">
                        <div class="row1">
                <div class="leftCol"  >
                         <div class="s1"><div class="s10"><?php echo JText::_('MOD_HOTELCALCULATOR_NOME');?><span class="red">*</span> </div><input required=true  type="text" style="width: 74%;"  value="" class="txt" id="nome" name="nome"></div>
                         <div class="s2"><div class="s10"><?php echo JText::_('MOD_HOTELCALCULATOR_COGNOME');?><span class="red">*</span> </div><input required=true type="text" style="width: 74%;"   value="" class="txt" id="cognome" name="cognome"></div>
                </div>
                <div class="clear"></div>
        </div>
              
                        <div class="row1">
                <div class="leftCol ">
                         <div class="s1"><div class="s10">
                        <?php echo JText::_('MOD_HOTELCALCULATOR_DATADI');?><span class="red">*</span></div>
                    <select required=true onchange="recalculate()" class="day" id="nascita_day" name="nascita_day">
<option value="01"> 01</option>
<option value="02"> 02</option>
<option value="03"> 03</option>
<option value="04"> 04</option>
<option value="05"> 05</option>
<option value="06"> 06</option>
<option value="07"> 07</option>
<option value="08"> 08</option>
<option value="09"> 09</option>
<option value="10"> 10</option>
<option value="11"> 11</option>
<option value="12"> 12</option>
<option value="13"> 13</option>
<option value="14"> 14</option>
<option value="15"> 15</option>
<option value="16"> 16</option>
<option value="17"> 17</option>
<option value="18"> 18</option>
<option value="19"> 19</option>
<option value="20"> 20</option>
<option value="21"> 21</option>
<option value="22"> 22</option>
<option value="23"> 23</option>
<option value="24"> 24</option>
<option value="25"> 25</option>
<option value="26"> 26</option>
<option value="27"> 27</option>
<option value="28"> 28</option>
<option value="29"> 29</option>
<option value="30"> 30</option>
<option value="31"> 31</option>
                                        </select>
                    <select required=true onchange="recalculate()" class="month" id="nascita_month" name="nascita_month">
                                <option selected="" value="0"> <?php echo JText::_('MOD_HOTELCALCULATOR_MOUNT');?> </option>
<option value="01"> <?php echo JText::_('JANUARY');?></option>
<option value="02"> <?php echo JText::_('FEBRUARY');?></option>
<option value="03"> <?php echo JText::_('MARCH');?></option>
<option value="04"> <?php echo JText::_('APRIL');?></option>
<option value="05"> <?php echo JText::_('MAY');?></option>
<option value="06"> <?php echo JText::_('JUNE');?></option>
<option value="07"> <?php echo JText::_('JULY');?></option>
<option value="08"> <?php echo JText::_('AUGUST');?></option>
<option value="09"> <?php echo JText::_('SEPTEMBER');?></option>
<option value="10"> <?php echo JText::_('OCTOBER');?></option>
<option value="11"> <?php echo JText::_('NOVEMBER');?></option>
<option value="12"> <?php echo JText::_('DECEMBER');?></option>
                        </select>
                        <select required=true onchange="recalculate()" class="year" id="nascita_year" name="nascita_year">
                                <option selected="" value="0"> <?php echo JText::_('JYEAR');?></option>
<option value="2014"> 2014</option>
<option value="2013"> 2013</option>
<option value="2012"> 2012</option>
<option value="2011"> 2011</option>
<option value="2010"> 2010</option>
<option value="2009"> 2009</option>
<option value="2008"> 2008</option>
<option value="2007"> 2007</option>
<option value="2006"> 2006</option>
<option value="2005"> 2005</option>
<option value="2004"> 2004</option>
<option value="2003"> 2003</option>
<option value="2002"> 2002</option>
<option value="2001"> 2001</option>
<option value="2000"> 2000</option>
<option value="1999"> 1999</option>
<option value="1998"> 1998</option>
<option value="1997"> 1997</option>
<option value="1996"> 1996</option>
<option value="1995"> 1995</option>
<option value="1994"> 1994</option>
<option value="1993"> 1993</option>
<option value="1992"> 1992</option>
<option value="1991"> 1991</option>
<option value="1990"> 1990</option>
<option value="1989"> 1989</option>
<option value="1988"> 1988</option>
<option value="1987"> 1987</option>
<option value="1986"> 1986</option>
<option value="1985"> 1985</option>
<option value="1984"> 1984</option>
<option value="1983"> 1983</option>
<option value="1982"> 1982</option>
<option value="1981"> 1981</option>
<option value="1980"> 1980</option>
<option value="1979"> 1979</option>
<option value="1978"> 1978</option>
<option value="1977"> 1977</option>
<option value="1976"> 1976</option>
<option value="1975"> 1975</option>
<option value="1974"> 1974</option>
<option value="1973"> 1973</option>
<option value="1972"> 1972</option>
<option value="1971"> 1971</option>
<option value="1970"> 1970</option>
<option value="1969"> 1969</option>
<option value="1968"> 1968</option>
<option value="1967"> 1967</option>
<option value="1966"> 1966</option>
<option value="1965"> 1965</option>
<option value="1964"> 1964</option>
<option value="1963"> 1963</option>
<option value="1962"> 1962</option>
<option value="1961"> 1961</option>
<option value="1960"> 1960</option>
<option value="1959"> 1959</option>
<option value="1958"> 1958</option>
<option value="1957"> 1957</option>
<option value="1956"> 1956</option>
<option value="1955"> 1955</option>
<option value="1954"> 1954</option>
<option value="1953"> 1953</option>
<option value="1952"> 1952</option>
<option value="1951"> 1951</option>
<option value="1950"> 1950</option>
<option value="1949"> 1949</option>
<option value="1948"> 1948</option>
<option value="1947"> 1947</option>
<option value="1946"> 1946</option>
<option value="1945"> 1945</option>
<option value="1944"> 1944</option>
<option value="1943"> 1943</option>
<option value="1942"> 1942</option>
<option value="1941"> 1941</option>
<option value="1940"> 1940</option>
<option value="1939"> 1939</option>
<option value="1938"> 1938</option>
<option value="1937"> 1937</option>
<option value="1936"> 1936</option>
<option value="1935"> 1935</option>
<option value="1934"> 1934</option>
<option value="1933"> 1933</option>
<option value="1932"> 1932</option>
<option value="1931"> 1931</option>
<option value="1930"> 1930</option>
<option value="1929"> 1929</option>
<option value="1928"> 1928</option>
<option value="1927"> 1927</option>
<option value="1926"> 1926</option>
<option value="1925"> 1925</option>
<option value="1924"> 1924</option>
<option value="1923"> 1923</option>
<option value="1922"> 1922</option>
<option value="1921"> 1921</option>
<option value="1920"> 1920</option>
<option value="1919"> 1919</option>
<option value="1918"> 1918</option>
<option value="1917"> 1917</option>
<option value="1916"> 1916</option>
<option value="1915"> 1915</option>
<option value="1914"> 1914</option>
<option value="1913"> 1913</option>
<option value="1912"> 1912</option>
<option value="1911"> 1911</option>
<option value="1910"> 1910</option>
<option value="1909"> 1909</option>
<option value="1908"> 1908</option>
<option value="1907"> 1907</option>
<option value="1906"> 1906</option>
<option value="1905"> 1905</option>
<option value="1904"> 1904</option>
<option value="1903"> 1903</option>
<option value="1902"> 1902</option>
<option value="1901"> 1901</option>
<option value="1900"> 1900</option>
                        </select></div>
<div class="s2"><div class="s10"> <?php echo JText::_('MOD_HOTELCALCULATOR_NAZIONAL');?><span class="red">*</span></div> <input required=true type="text" value="" style="width: 74%;" class="txt" id="nazionalita" name="nazionalita"></div>
</div>
                
                     

                <div class="rightCol"><div class="s1">
                        <div class="s10"><?php echo JText::_('MOD_HOTELCALCULATOR_SESSO');?><span class="red">*</span></div><div class="sleft">
    <input type="radio" checked="" value="0" name="sesso"> <?php echo JText::_('MOD_HOTELCALCULATOR_MASCHILE');?> 
    <input type="radio" value="1" name="sesso"> <?php echo JText::_('MOD_HOTELCALCULATOR_FEMMINILE');?> </div>
</div>

<div class="s2"><div class="s10">
 <?php echo JText::_('MOD_HOTELCALCULATOR_PASS');?><span class="red">*</span></div> <input required=true type="text" style="width: 74%;" value="" class="txt" id="numero_passaporto" name="numero_passaporto"></div>

                </div>
<br>
                <div class="clear"></div>
        </div>
              

                <div class="hr"></div>
                <div class="s1">
                        <div class="s10"> <?php echo JText::_('MOD_HOTELCALCULATOR_CITTADA');?><span class="red">*</span></div> <input required=true type="text" value="" class="txt"   id="citta_visitare"   style="width: 74%;" name="citta_visitare">
                </div>
             <div class="s2">

                         <div class="s10"> <?php echo JText::_('MOD_HOTELCALCULATOR_CITTADI');?><span class="red">*</span></div> <input required=true type="text" value="" style="width: 74%;" class="txt" id="citta" name="citta">

                </div><div class="clear"></div>


                        <div class="row1">
                <div class="leftCol"><div class="s1">
                         <div class="s10"><?php echo JText::_('MOD_HOTELCALCULATOR_EMAIL');?><span class="red">*</span></div> <input required=true type="text" value="" style="width: 74%;"   id="mail" name="mail"></div>
<div class="s2">
                         <div class="s10"><?php echo JText::_('MOD_HOTELCALCULATOR_TELEFON');?><span class="red">*</span></div><input required=true type="text" style="width: 74%;"  value="" class="txt" id="telefono" name="telefono"></div>
                </div>
<div class="clear"></div>
        </div>
<br><br><br><br><br><br><br><br>
                        <div class="row1">
                <div class="s5"  >
                         
                        <div class="s1"><div class="s10"> <?php echo JText::_('MOD_HOTELCALCULATOR_PARTENZA');?><span class="red">*</span></div>
                    <select required=true onchange="recalculate()" class="day " id="dal_day" name="dal_day">
                            <?php for ($i=1;$i<=31;$i++)
                            {
                                if ($i<10)
                                    $d = '0'.$i;
                                else
                                    $d = $i;

                                if (date('d')==$d)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$d.'">'.$d.'</option>';
                            }
                            ?>
                        </select>
                    <select required=true onchange="recalculate()" class="month " id="dal_month" name="dal_month">
                            <?php
                            $month_array = array(
                                "01" => JText::_('JANUARY'),
                                "02" => JText::_('FEBRUARY'),
                                "03" => JText::_('MARCH'),
                                "04" => JText::_('APRIL'),
                                "05" => JText::_('MAY'),
                                "06" => JText::_('JUNE'),
                                "07" => JText::_('JULY'),
                                "08" => JText::_('AUGUST'),
                                "09" => JText::_('SEPTEMBER'),
                                "10" => JText::_('OCTOBER'),
                                "11" => JText::_('NOVEMBER'),
                                "12" => JText::_('DECEMBER')
                            );
                            foreach ($month_array as $k=>$v){
                                if (date('m')==$k)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$k.'">'.$v.'</option>';
                            }
                            ?>
                        </select>
                    <select required=true onchange="recalculate()" class="year " id="dal_year" name="dal_year">
                            <?php for ($i=intval(date('Y'));$i<=intval(date('Y'))+2;$i++)
                            {
                                if (date('Y')==$i)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select></div>
                    <div class="s2"><div class="s10"> <?php echo JText::_('MOD_HOTELCALCULATOR_RITORNO');?><span class="red">*</span></div>
                    <select required=true onchange="recalculate()" class="day" id="al_day" name="al_day">
                            <?php for ($i=1;$i<=31;$i++)
                            {
                                if ($i<10)
                                    $d = '0'.$i;
                                else
                                    $d = $i;

                                if (date('d')==$d)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$d.'">'.$d.'</option>';
                            }
                            ?>
                        </select>
                    <select required=true onchange="recalculate()" class="month " id="al_month" name="al_month">
                            <?php
                            foreach ($month_array as $k=>$v){
                                if (date('m')==$k)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$k.'">'.$v.'</option>';
                            }
                            ?>
                        </select>
                    <select required=true onchange="recalculate()" class="year " id="al_year" name="al_year">
                            <?php for ($i=intval(date('Y'));$i<=intval(date('Y'))+2;$i++)
                            {
                                if (date('Y')==$i)
                                    $sel = 'selected=""';
                                else
                                    $sel = '';

                                echo '<option '.$sel.' value="'.$i.'">'.$i.'</option>';
                            }
                            ?>
                        </select></div>
                </div>
<br><br>
            <div class="clear"></div>
        </div>




<div class="uk-form">
   <input type="checkbox" value="1" name="ho_medica" id="ho_medica_hidden" style="display: none;">
   <input type="checkbox" onclick="jQuery('#ho_medica_hidden')[0].click();recalculate()" value="1" id="ho_medica"><?php echo JText::_('MOD_HOTELCALCULATOR_MEDICO');?>
</div>
<br>
                <p>
                        <?php echo JText::_('MOD_HOTELCALCULATOR_SONO');?>
                    <label for="radiono"><input type="radio" id="radiono" checked="" value="0" onclick="recalculate()" name="procedura_completa"><?php echo JText::_('MOD_HOTELCALCULATOR_NO'); ?></label>
                    <label style="white-space:nowrap;" for="radio1"><input type="radio" id="radio1"  value="1" class="data-uk-button-radio" onclick="recalculate()" name="procedura_completa"><?php echo JText::_('MOD_HOTELCALCULATOR_CITY1'); ?></label>
                    <label style="white-space:nowrap;" for="radio2"><input type="radio" id="radio2" value="2" onclick="recalculate()" name="procedura_completa"><?php echo JText::_('MOD_HOTELCALCULATOR_CITY2'); ?></label>
                    <label style="white-space:nowrap;" for="radio3"><input type="radio" id="radio3" value="3" onclick="recalculate()" name="procedura_completa"><?php echo JText::_('MOD_HOTELCALCULATOR_CITY3'); ?></label>
                    <label style="white-space:nowrap;" for="radio6"><input type="radio" id="radio6" value="6" onclick="recalculate()" name="procedura_completa"><?php echo JText::_('MOD_HOTELCALCULATOR_CITY4'); ?></label>


<br><br>



                </p>

      <div class="hr uk-form-controls" >
                       
<label id="ho1">
    <input type="checkbox" onclick="recalculate()"  id="ho1" class="uk-form-controls" value="1" name="ho_urgente"><?php echo JText::_('MOD_HOTELCALCULATOR_DELLA');?>

</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            
            
<label id="ho2">
                      <input type="checkbox" id="compila" id="ho2" name="ho_compila" value="1" onclick="recalculate(1)"><?php echo JText::_('MOD_HOTELCALCULATOR_MODULO');?></label>
</div>
<br>

      <div class="hr"></div>

                <p>
                        <?php echo JText::_('MOD_HOTELCALCULATOR_REGISTR');?><span class="red">*</span>

    <input type="checkbox" value="1" onclick="recalculate()" name="ho_registrazione_1" > <?php echo JText::_('MOD_HOTELCALCULATOR_AMOSCO');?>
    <input type="checkbox" value="1" onclick="recalculate()" name="ho_registrazione_2"><?php echo JText::_('MOD_HOTELCALCULATOR_APITER');?>
                </p>
      <p>
                        <?php echo JText::_('MOD_HOTELCALCULATOR_METTETE');?>

    <input type="radio" checked="" value="0" onclick="recalculate()" name="spese_spedizione"> <?php echo JText::_('MOD_HOTELCALCULATOR_NO');?>
    <input type="radio" value="1" onclick="recalculate()" name="spese_spedizione"> <?php echo JText::_('MOD_HOTELCALCULATOR_SOLO');?>
    <input type="radio" value="2" onclick="recalculate()" name="spese_spedizione"> <?php echo JText::_('MOD_HOTELCALCULATOR_ANDATA');?>
                            
                </p>
<br>
      <p>
                <?php echo JText::_('MOD_HOTELCALCULATOR_ANCHE');?>
                </p>
                <p>
                        
   <input type="checkbox" value="1" onclick="recalculate()" name="hotel"><?php echo JText::_('MOD_HOTELCALCULATOR_HOTEL');?> 
   <input type="checkbox" value="1" onclick="recalculate()" name="appartamento"><?php echo JText::_('MOD_HOTELCALCULATOR_APPARTAMENTO');?> 
   <input type="checkbox" value="1" name="volo_aereo"> <?php echo JText::_('MOD_HOTELCALCULATOR_VOLO');?>
   <input type="checkbox" value="1" name="transfers"><?php echo JText::_('MOD_HOTELCALCULATOR_TRANSFERS');?>
   <input type="checkbox" value="1" name="escursioni"> <?php echo JText::_('MOD_HOTELCALCULATOR_ESCURSIONI');?> 
                </p>
                <p>
                         
    <input type="radio" checked="" value="0" name="parlo"><?php echo JText::_('MOD_HOTELCALCULATOR_PARLOSO');?>  
    <input type="radio" value="1" name="parlo"> <?php echo JText::_('MOD_HOTELCALCULATOR_PARLOIN');?>   
                                   </p>
<br>


<div class="row1"><div class="s10"><?php echo JText::_('MOD_HOTELCALCULATOR_COMMENT');?></div>
<TEXTAREA  id="comment" name="comment" WRAP="virtual" COLS="40" style="width: 75%;"  ROWS="3"></TEXTAREA></div><br>

                <h3 class="costo"><?php echo JText::_('MOD_HOTELCALCULATOR_COSTO');?>   <span id="TotalPrice">Euro</span></h3>
 <input type="hidden" value="" id="total_price" name="total_price">
        <!--       <div id="Debug" style="width:200px;background:#fff;border:1px solid #000;padding:20px;"></div>-->
<p style="text-align: justify;">
                       <?php echo JText::_('MOD_HOTELCALCULATOR_INFOCOSTO');?>  
</p><br>
                <div class="s5" style="vertical-align: top;">
                        <div class="submbutton"><input  type="submit" class="uk-button uk-button-primary button-soggiorno" value="  <?php echo JText::_('MOD_HOTELCALCULATOR_INOLTRA');?> " name="sub"></div><br>
						<div class="s5" style="text-align: justify;" >
                        <?php echo JText::_('MOD_HOTELCALCULATOR_PRIMA');?> </div>
                        <div class="clear"></div>

                </div>
<input type="hidden" name="smail" value="1">

        </form>
 <div id="HiddenFigli">
                                <div class="FigliBlock">

                        <h3><span>%#.</span></h3>
                                <div class="row1">
                <div class="leftCol">
                        <?php echo JText::_('MOD_HOTELCALCULATOR_NOME');?>  <span class="red">*</span> <input type="text" class="txt nome" id="figli_nome_%#" name="figli_nome_%#">
                </div>
                <div class="rightCol">
                        <?php echo JText::_('MOD_HOTELCALCULATOR_COGNOME');?> <span class="red">*</span> <input type="text" class="txt cognome" id="figli_cognome_%#" name="figli_cognome_%#">
                </div>
                <div class="clear"></div>
        </div>

                                <div class="row1">
                <div class="leftCol">
                                Data di nascita:<span class="red">*</span>
                                                        <select onchange="recalculate()" class="day" id="figli_nascita_%#_day" name="figli_nascita_%#_day">
                          <option value="01"> 01</option>
<option value="02"> 02</option>
<option value="03"> 03</option>
<option value="04"> 04</option>
<option value="05"> 05</option>
<option value="06"> 06</option>
<option value="07"> 07</option>
<option value="08"> 08</option>
<option value="09"> 09</option>
<option value="10"> 10</option>
<option value="11"> 11</option>
<option value="12"> 12</option>
<option value="13"> 13</option>
<option value="14"> 14</option>
<option value="15"> 15</option>
<option value="16"> 16</option>
<option value="17"> 17</option>
<option value="18"> 18</option>
<option value="19"> 19</option>
<option value="20"> 20</option>
<option value="21"> 21</option>
<option value="22"> 22</option>
<option value="23"> 23</option>
<option value="24"> 24</option>
<option value="25"> 25</option>
<option value="26"> 26</option>
<option value="27"> 27</option>
<option value="28"> 28</option>
<option value="29"> 29</option>
<option value="30"> 30</option>
<option value="31"> 31</option>


                        </select>
                        <select onchange="recalculate()" class="month" id="figli_nascita_%#_month" name="figli_nascita_%#_month">
                        <option selected="" value="0"> Mese </option>
<option value="01"> Gennaio</option>
<option value="02"> Febbraio</option>
<option value="03"> Marzo</option>
<option value="04"> Aprile</option>
<option value="05"> Maggio</option>
<option value="06"> Giugno</option>
<option value="07"> Luglio</option>
<option value="08"> Agosto</option>
<option value="09"> Settembre</option>
<option value="10"> Ottobre</option>
<option value="11"> Novembre</option>
<option value="12"> Dicembre</option>
                        </select>
                        <select onchange="recalculate()" class="year" id="figli_nascita_%#_year" name="figli_nascita_%#_year">
                             <option selected="" value="Anno"> Anno</option>
<option value="2014"> 2014</option>
<option value="2013"> 2013</option>
<option value="2012"> 2012</option>
<option value="2011"> 2011</option>
<option value="2010"> 2010</option>
<option value="2009"> 2009</option>
<option value="2008"> 2008</option>
<option value="2007"> 2007</option>
<option value="2006"> 2006</option>
<option value="2005"> 2005</option>
<option value="2004"> 2004</option>
<option value="2003"> 2003</option>
<option value="2002"> 2002</option>
<option value="2001"> 2001</option>
<option value="2000"> 2000</option>
<option value="1999"> 1999</option>
<option value="1998"> 1998</option>
<option value="1997"> 1997</option>
<option value="1996"> 1996</option>
<option value="1995"> 1995</option>
<option value="1994"> 1994</option>
<option value="1993"> 1993</option>
<option value="1992"> 1992</option>
<option value="1991"> 1991</option>
<option value="1990"> 1990</option>
<option value="1989"> 1989</option>
<option value="1988"> 1988</option>
<option value="1987"> 1987</option>
<option value="1986"> 1986</option>
<option value="1985"> 1985</option>
<option value="1984"> 1984</option>
<option value="1983"> 1983</option>
<option value="1982"> 1982</option>
<option value="1981"> 1981</option>
<option value="1980"> 1980</option>
<option value="1979"> 1979</option>
<option value="1978"> 1978</option>
<option value="1977"> 1977</option>
<option value="1976"> 1976</option>
<option value="1975"> 1975</option>
<option value="1974"> 1974</option>
<option value="1973"> 1973</option>
<option value="1972"> 1972</option>
<option value="1971"> 1971</option>
<option value="1970"> 1970</option>
<option value="1969"> 1969</option>
<option value="1968"> 1968</option>
<option value="1967"> 1967</option>
<option value="1966"> 1966</option>
<option value="1965"> 1965</option>
<option value="1964"> 1964</option>
<option value="1963"> 1963</option>
<option value="1962"> 1962</option>
<option value="1961"> 1961</option>
<option value="1960"> 1960</option>
<option value="1959"> 1959</option>
<option value="1958"> 1958</option>
<option value="1957"> 1957</option>
<option value="1956"> 1956</option>
<option value="1955"> 1955</option>
<option value="1954"> 1954</option>
<option value="1953"> 1953</option>
<option value="1952"> 1952</option>
<option value="1951"> 1951</option>
<option value="1950"> 1950</option>
<option value="1949"> 1949</option>
<option value="1948"> 1948</option>
<option value="1947"> 1947</option>
<option value="1946"> 1946</option>
<option value="1945"> 1945</option>
<option value="1944"> 1944</option>
<option value="1943"> 1943</option>
<option value="1942"> 1942</option>
<option value="1941"> 1941</option>
<option value="1940"> 1940</option>
<option value="1939"> 1939</option>
<option value="1938"> 1938</option>
<option value="1937"> 1937</option>
<option value="1936"> 1936</option>
<option value="1935"> 1935</option>
<option value="1934"> 1934</option>
<option value="1933"> 1933</option>
<option value="1932"> 1932</option>
<option value="1931"> 1931</option>
<option value="1930"> 1930</option>
<option value="1929"> 1929</option>
<option value="1928"> 1928</option>
<option value="1927"> 1927</option>
<option value="1926"> 1926</option>
<option value="1925"> 1925</option>
<option value="1924"> 1924</option>
<option value="1923"> 1923</option>
<option value="1922"> 1922</option>
<option value="1921"> 1921</option>
<option value="1920"> 1920</option>
<option value="1919"> 1919</option>
<option value="1918"> 1918</option>
<option value="1917"> 1917</option>
<option value="1916"> 1916</option>
<option value="1915"> 1915</option>
<option value="1914"> 1914</option>
<option value="1913"> 1913</option>
<option value="1912"> 1912</option>
<option value="1911"> 1911</option>
<option value="1910"> 1910</option>
<option value="1909"> 1909</option>
<option value="1908"> 1908</option>
<option value="1907"> 1907</option>
<option value="1906"> 1906</option>
<option value="1905"> 1905</option>
<option value="1904"> 1904</option>
<option value="1903"> 1903</option>
<option value="1902"> 1902</option>
<option value="1901"> 1901</option>
<option value="1900"> 1900</option>
                        </select>
                </div>
                <div class="rightCol">
                                Sesso:<span class="red">*</span>
                                <input type="radio" checked="checked" value="0" name="figli_sesso_%#"> M
                                <input type="radio" value="1" name="figli_sesso_%#"> F
                </div>
                <div class="clear"></div>
        </div>


                                <div class="row1">
                <div class="leftCol">
                                Cittadinanza:<span class="red">*</span> <input type="text" class="txt cittadinanza" id="figli_cittadinanza_%#" name="figli_cittadinanza_%#">
                </div>
                <div class="rightCol">
                </div>
                <div class="clear"></div>
        </div>
                </div>
        </div>
 <script src="/media/jui/js/jquery.scrollTo.min.js"></script>
<script src="<?php echo JURI::root(true).'/modules/mod_hotelcalculator/script/visto.js?v22'; ?>"></script>
              

</fieldset>

                </div>
<div>
<?php } ?>