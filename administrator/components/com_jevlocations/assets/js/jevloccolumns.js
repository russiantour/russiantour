jQuery(document).ready(function(){

	jQuery('#sortableLocationsColumns').sortable({
				placeholder: 'ui-state-highlight',
				update: function (event, ui) {getNewOrder();}
	});
	jQuery('.btn.jevloc-toggle-column').click(function(){
		toggleColumn(this);
	})
	jQuery( '#sortable' ).disableSelection();
});

function getNewOrder()
{
	var lis = jQuery('ul#sortableLocationsColumns li.jevloc-col-enabled');
	var newOrder = []
	lis.each(function(i, item){
		var id = jQuery(item).attr('id');
		newOrder[i] = id;
		});
	jQuery('#sortableLocColumns').val(newOrder.join());
}

function toggleColumn(button)
{
	var column = jQuery(button).parent();
	var previousClass = jQuery(column).attr('class');
	if( column.hasClass("jevloc-col-enabled"))
	{
		var newClass = "jevloc-col-disabled";
		var previousIcon = "icon-eye";
		var newIcon = "icon-eye-blocked";
	}
	else
	{
		var newClass = "jevloc-col-enabled"
		var previousIcon = "icon-eye-blocked";
		var newIcon = "icon-eye";
	}
	column.removeClass(previousClass);
	column.addClass(newClass);
	jQuery(button).find("i").removeClass(previousIcon);
	jQuery(button).find("i").addClass(newIcon);
	getNewOrder();
}
