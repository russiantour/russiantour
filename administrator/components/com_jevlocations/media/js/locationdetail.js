var myMap = false;
var myMarker = false;
var myGeocoder = null;
var disableautopan = false;

function myMapload(){

	//We need to be sure there is a map container to put the map in
	var $gmap =  jQuery('#gmap');
	if($gmap.length>0)
	{
		var point =  new google.maps.LatLng(globallat,globallong);

		var myOptions = {
			center: point,
			zoom: globalzoom,
			mapTypeId: google.maps.MapTypeId[maptype]
		};

	
		myMap = new google.maps.Map(document.getElementById("gmap"), myOptions);

		if (maptype == "ROADMAP"){
			mtype="m";
		}
		else if (maptype == "SATELLITE"){
			mtype="k";
		}
		else if (maptype == "TERRAIN"){
			mtype="p";
		}
		else {
			mtype="h";
		}

		if (!markerOptions) {
			//markerOptions = {draggable:true };
			var markerOptions = {
				position:point,
				map:myMap,
				disableAutoPan:disableautopan,
				animation: google.maps.Animation.DROP,
				draggable:false,
				icon: urlroot + mapicon
			};
		}

		//var point = new GLatLng(globallat,globallong);
		//myMap.setCenter(point, globalzoom );

		myMarker = new google.maps.Marker( markerOptions);
		//myMap.addOverlay(myMarker);
		myMarker.setMap(myMap);


		google.maps.event.addListener(myMap, "click", function(e) {
			window.open (googlemapsurl, "map");
		});

		google.maps.event.addListener(myMarker, "click", function(e) {
			window.open (googlemapsurl, "map");
		});

	}

};


jQuery( document ).ready(function() {
	myMapload();
});

