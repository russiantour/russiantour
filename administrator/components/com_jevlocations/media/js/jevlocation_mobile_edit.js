var panel = '<div data-role="panel" id="jevloc-create-location-panel" data-position="right" data-display="overlay"><iframe id="jevloc-create-location-form" src=""></iframe></div>';

jQuery(document).on('pagebeforecreate', function () {

});

jQuery( document ).ready(function() {
  jQuery("body").prepend(panel);
  jQuery("#jevloc-create-location-panel").panel();
});

// We need to load the edit form later to avoid firing chosen twice
jQuery( window ).load(function() {
  jQuery("#jevloc-create-location-form").attr('src',"index.php?option=com_jevlocations&task=locations.edit&tmpl=component&pop=1");
});
