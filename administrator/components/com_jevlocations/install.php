<?php

/**
 * copyright (C) 2012-2015 GWE Systems Ltd - All rights reserved
 * @license GNU/GPLv3 www.gnu.org/licenses/gpl-3.0.html
 * version 3.2.16
 * */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die('Restricted access');

class com_jevlocationsInstallerScript
{

	public function preflight($type, $parent)
	{
		$this->minimum_joomla_release = $parent->get( "manifest" )->attributes()->version;

		if(!$this->joomlaCheck())
		{
			return false;
		}

		if ( $type == 'update' )
		{
			$oldRelease = $this->getParam('version');
			if(version_compare($oldRelease, '3.1.0','le'))
			{
				JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVLOCATIONS_UPDATE_31_WARNING"), 'warning');
			}
		}

		return true;
	}

	public function postflight($type, $parent)
	{
		
	}
	//
	// Joomla installer functions
	//
	
	function install($parent)
	{

		$this->createTables();

		
		$this->cleanupFilters();
		return true;

	}

	/*
	 * get a variable from the manifest file (actually, from the manifest cache).
	 */
	function getParam( $name ) {
		$db = JFactory::getDbo();
		$db->setQuery('SELECT manifest_cache FROM #__extensions WHERE element = "com_jevlocations"');
		$manifest = json_decode( $db->loadResult(), true );
		return $manifest[ $name ];
	}

	function migrateCategories()
	{
		$db = JFactory::getDBO();
            
        $charset = '';
        $rowcharset = '';

        if ($db->hasUTF8mb4Support())
        {
            $charset = ' DEFAULT CHARACTER SET `utf8mb4`';
            $rowcharset = 'CHARACTER SET utf8mb4';
		}
        else if ($db->hasUTFSupport())
        {
            $charset = ' DEFAULT CHARACTER SET `utf8`';
            $rowcharset = 'CHARACTER SET utf8';
		}

		$tables = $db->getTableList();
		if (!in_array(JFactory::getConfig()->get('dbprefix')."jevlocations_oldcatmap", $tables))
		{
			$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jevlocations_oldcatmap(
	oldcatid int(11) NOT NULL,
	newcatid int(11) NOT NULL,
	PRIMARY KEY  (oldcatid, newcatid)
) $charset;
SQL;
			$db->setQuery($sql);
			try
			{
				$db->execute();
			}
			catch (RuntimeException $e)
			{
				JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
				return false;
			}

			$query = $db->getQuery(true);
			$query->select('a.id,a.parent_id,a.title,a.name,a.alias,a.description,a.published,a.ordering,a.checked_out,a.checked_out_time,a.access');
			$query->from("#__jevlocation_categories AS a");
			$query->where('a.section = "com_jevlocations2"');
			$query->order('a.parent_id ASC');
			$db->setQuery($query);
			$categories = $db->loadObjectList('id');

			foreach($categories as $category)
			{
				$categoryData['extension'] = "com_jevlocations";
				$categoryData['language'] = "*";
				$categoryData['parent_id'] = "1";
				$categoryData['level'] = "1";
				$categoryData['title'] = $category->title;
				$categoryData['alias'] = $categoryData['path'] = $category->alias;
				$categoryData['description'] = $category->description;
				$categoryData['published'] = $category->published;
				$categoryData['checked_out'] = $category->checked_out;
				$categoryData['checked_out_time'] = $category->checked_out_time;
				$categoryData['access'] = $category->access;
				if($category->parent_id > 0)
				{
					$categoryData['parent_id'] = $old2NewCatMap[$category->parent_id]["catid"];
				}

				$catobject = (object) $categoryData;
				$db->insertObject("#__categories", $catobject);
				$newCatId = $db->insertid();
				$old2NewCatMap[$category->id] = array("catid"=>$newCatId);
				$old2NewRow[] = $category->id .", ".$newCatId;
			}

			if(!empty($old2NewRow))
			{
				$query = $db->getQuery(true);
				$query->insert('#__jevlocations_oldcatmap');
				$query->columns('oldcatid, newcatid');
				$query->values($old2NewRow);
				$db->setQuery($query);

				try
				{
					$db->execute();
				}
				catch (RuntimeException $e)
				{
					JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
					return false;
				}
			}

			//Fix assets
			$query = $db->getQuery(true);
			$query->select('*');
			$query->from('#__assets AS a');
			$query->where('a.name="com_jevlocations"');
			$query->where('a.parent_id=1');
			$db->setQuery($query);
			$rootasset = $db->loadObject();

			$query = $db->getQuery(true);
			$query->select('*');
			$query->from("#__categories AS a");
			$query->where('a.asset_id=0');
			$query->where('a.extension="com_jevlocations"');
			$query->order('a.level ASC, a.id');
			$db->setQuery($query);
			$missingassets = $db->loadObjectList();

			if (count($missingassets) > 0)
			{
				foreach ($missingassets as $missingasset)
				{
					$this->insertAsset($missingasset,"com_jevlocations",$rootasset);
				}
			}

			// Rebuild the categories table
			$table = JTable::getInstance('Category', 'JTable', array('dbo' => $db));
			try{
				$table->rebuild();
			} catch (Exception $e) {
				JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
				return false;
			}

			if(!empty($old2NewCatMap))
			{
				//Update locations categories links
				foreach($old2NewCatMap as $oldCatId => $newCat)
				{
					$query = $db->getQuery(true);
					$query->select('loc_id');
					$query->from('#__jev_locations');
					$query->where('loccat = ' . (int) $oldCatId);
					$db->setQuery($query);
					$locationsId = $db->loadColumn();
					foreach($locationsId as $locid)
					{
						$catMapRelation[] = "$locid,".$newCat['catid'];
					}
				}
				$query = $db->getQuery(true);
				$query->insert('#__jevlocations_catmap');
				$query->columns('locid,catid');
				$query->values($catMapRelation);
				$db->setQuery($query);
				try
				{
					$db->execute();
				}
				catch (RuntimeException $e)
				{
					JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
					return false;
				}
			}
		}
	}

	private function insertAsset($object,$extension,$rootasset)
	{
		$db = JFactory::getDbo();
		// Getting the asset table
		$table = JTable::getInstance('Asset', 'JTable', array('dbo' => $db));

		// Getting the categories id's
		$db->setQuery("SELECT * FROM #__categories WHERE extension='$extension'");
		$categories = $db->loadObjectList('id');

		$db->setQuery("SELECT * FROM #__assets WHERE name like '$extension.category.%'");
		$assets = $db->loadObjectList('id');

		$assets[$rootasset->id] = $rootasset;

		//
		// Correct extension
		//
		$id = $object->id;
		$table->name = "$extension.category.{$id}";

		// Setting rules values
		$table->rules = '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}';
		$table->title = $db->escape($object->title);

		if ($object->parent_id==1)
		{
			$table->parent_id = $rootasset->id;
		}
		else if (array_key_exists($object->parent_id, $categories) && $categories[$object->parent_id]->asset_id > 0)
		{
			$table->parent_id = $categories[$object->parent_id]->asset_id;
		}

		$table->setLocation($table->parent_id, 'last-child');

		// Make sure this asset doesn't exist already
		$db->setQuery("SELECT * FROM #__assets WHERE name = ".$db->quote($table->name));
		$asset = $db->loadObject();
		if (!$asset){
			// Insert the asset
			$table->store();
		}
		else {
			$table = $asset;
		}

		// updating the category asset_id;
		$updatetable = '#__categories';
		$query = "UPDATE {$updatetable} SET asset_id = {$table->id}"
				. " WHERE id = {$id}";
		$db->setQuery($query);
		$db->query();

		// Check for query error.
		$error = $db->getErrorMsg();

		if ($error)
		{
			throw new Exception($error);
			return false;
		}

		return true;

	}

	function uninstall($parent)
	{
		// No nothing for now

	}

	function update($parent)
	{
		$this->createTables();

		$this->updateTables();
		$this->migrateCategories();
		$this->cleanupFilters();		

		return true;

	}

	private function createTables()
	{

		$db = JFactory::getDBO();


		if (version_compare(JVERSION, "3.3", 'ge')){
			$charset = ($db->hasUTFSupport()) ?  ' DEFAULT CHARACTER SET `utf8`' : '';
			$rowcharset = ($db->hasUTFSupport()) ?  'CHARACTER SET utf8' : '';
		}
		else {
			$charset = ($db->hasUTF()) ?  ' DEFAULT CHARACTER SET `utf8`' : '';
			$rowcharset = ($db->hasUTF()) ?  'CHARACTER SET utf8' : '';
		}
		/**
		 * create tables if it doesn't exit
		 * 
		 */
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jev_locations(
	loc_id int(12) NOT NULL auto_increment,
	title VARCHAR(255) NOT NULL default "",
	rawlocation VARCHAR(500) NOT NULL default "",
	alias VARCHAR(255) NOT NULL default "",
	street varchar(255) NOT NULL default "",
	postcode varchar(255) NOT NULL default "",
	city varchar(255) NOT NULL default "",
	state varchar(255) NOT NULL default "",
	country varchar(255) NOT NULL default "",
	image varchar(255) NOT NULL default "",
	imagetitle varchar(255) NOT NULL default "",
	description text NOT NULL ,
	metatitle VARCHAR(255) NOT NULL default "",
	metadescription text NOT NULL ,
	geolon float(12,8) NOT NULL default 0,
	geolat float(12,8) NOT NULL default 0,
	geozoom int(2) NOT NULL default 10,
	pcode_id int(12) NOT NULL default 0,
	groupusage int(12) NOT NULL default 0,

	targetmenu int(12) NOT NULL default 0,
	overlaps tinyint(3) unsigned NOT NULL DEFAULT '0',

	phone varchar(255) NOT NULL default '',
	url varchar(255) NOT NULL default '',
	
	loccat int(11) NOT NULL default 0,

	anonname varchar(255) NOT NULL default '',
	anonemail varchar(255) NOT NULL default '',
	
	mapicon varchar(255) NOT NULL DEFAULT 'blue-dot.png',
	
	catid int(11) NOT NULL default 0,
	global tinyint(1) unsigned NOT NULL default 0,
	priority tinyint(2) unsigned NOT NULL default 0,
 	 	
	ordering int(11) NOT NULL default '0',
	access int(11) unsigned NOT NULL default 1,
	published tinyint(1) unsigned NOT NULL default 0,
	created datetime  NOT NULL default '0000-00-00 00:00:00',
	created_by int(11) unsigned NOT NULL default '0',
	created_by_alias varchar(100) NOT NULL default '',
	modified datetime  NOT NULL default '0000-00-00 00:00:00',
	modified_by int(11) unsigned NOT NULL default '0',
		
	checked_out int(11) unsigned NOT NULL default '0',
	checked_out_time DATETIME NOT NULL default '0000-00-00 00:00:00',

	params text NOT NULL ,
	
	PRIMARY KEY  (loc_id)
)  $charset;
SQL;
		$db->setQuery($sql);
		try
		{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
			return false;
		}
		

		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jevlocation_categories (
  id int(11) NOT NULL AUTO_INCREMENT,
  parent_id int(11) NOT NULL DEFAULT '0',
  title varchar(255) NOT NULL DEFAULT '',
  name varchar(255) NOT NULL DEFAULT '',
  alias varchar(255) NOT NULL DEFAULT '',
  image varchar(255) NOT NULL DEFAULT '',
  section varchar(50) NOT NULL DEFAULT '',
  image_position varchar(30) NOT NULL DEFAULT '',
  description text NOT NULL,
  published tinyint(1) NOT NULL DEFAULT '0',
  checked_out int(11) unsigned NOT NULL DEFAULT '0',
  checked_out_time datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  editor varchar(50) DEFAULT NULL,
  ordering int(11) NOT NULL DEFAULT '0',
  access tinyint(3) unsigned NOT NULL DEFAULT '1',
  count int(11) NOT NULL DEFAULT '0',
  params text NOT NULL,
  PRIMARY KEY (id),
  KEY cat_idx (section,published,access),
  KEY idx_access (access),
  KEY idx_checkout (checked_out)
)  $charset
SQL;

		$db->setQuery($sql);
		try
		{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
			return false;
		}

		$db = JFactory::getDbo();
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jev_customfields3(
	id int(11) NOT NULL auto_increment,
	target_id int(11) NOT NULL default 0,
	targettype varchar(255) NOT NULL default '',
	name varchar(255) NOT NULL default '',
	value text NOT NULL ,

	PRIMARY KEY  (id),
	INDEX (target_id, targettype(200)),
	INDEX combo (name(200),value(10))
) $charset;	
SQL;
		$db->setQuery($sql);
		try
		{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
			return false;
		}

		$db = JFactory::getDBO();
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jevlocations_catmap(
	locid int(11) NOT NULL,
	catid int(11) NOT NULL,
	PRIMARY KEY  (locid,catid)
) $charset;
SQL;
		$db->setQuery($sql);
		try
		{
			$db->execute();
		}
		catch (RuntimeException $e)
		{
			JFactory::getApplication()->enqueueMessage($e->getMessage(), 'error');
			return false;
		}
		
		// performance changes to event detail table
		$sql = "SHOW COLUMNS FROM `#__jevents_vevdetail`";
		$db->setQuery($sql);
		$cols = @$db->loadObjectList("Field");

		if (!array_key_exists("loc_id", $cols))
		{
			$sql = "ALTER TABLE #__jevents_vevdetail ADD COLUMN loc_id int(11) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();

			$sql = "ALTER TABLE #__jevents_vevdetail ADD INDEX loc_id (loc_id)";
			$db->setQuery($sql);
			@$db->execute();
			
			// move across all the data
			$sql = "UPDATE #__jevents_vevdetail SET loc_id = CAST(location AS UNSIGNED) where location REGEXP '^-?[0-9]+$'";
			$db->setQuery($sql);
			@$db->execute();
			
		}
		
	}

	private function updateTables()
	{

		$db = JFactory::getDBO();
		$db->setDebug(0);

		// Fix any old migrated locations with bad access levels
		$sql = "UPDATE #__jev_locations set access=1 where access=1";
		$db->setQuery($sql);
		@$db->execute();

		$sql = "SHOW COLUMNS FROM `#__jev_locations`";
		$db->setQuery($sql);
		$cols = @$db->loadObjectList("Field");

		if (!array_key_exists("modified", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD  column modified datetime  NOT NULL default '0000-00-00 00:00:00'";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("rawlocation", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD  column  rawlocation VARCHAR(500) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("mapicon", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD  column mapicon varchar(255) NOT NULL DEFAULT 'blue-dot.png'";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("targetmenu", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD  column targetmenu int(12) NOT NULL default 0 ";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("overlaps", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD  column overlaps tinyint(3) unsigned NOT NULL DEFAULT '0' ";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("imagetitle", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column imagetitle varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("image", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column image varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("anonname", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column anonname varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("anonemail", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column anonemail varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("groupusage", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column groupusage int(12) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("geolon", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations MODIFY column geolon float(12,8) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("geolat", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations MODIFY column geolat float(12,8) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("loccat", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column loccat int(11) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("priority", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column priority tinyint(2) unsigned NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("url", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column url varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("phone", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column phone varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("city", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column city varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("state", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column state varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("country", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column country varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("metatitle", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column metatitle varchar(255) NOT NULL default ''";
			$db->setQuery($sql);
			@$db->execute();
		}

		if (!array_key_exists("metadescription", $cols))
		{
			$sql = "ALTER TABLE #__jev_locations ADD column metadescription TEXT NOT NULL";
			$db->setQuery($sql);
			@$db->execute();
		}


		// Make sure DB is up to date
		$db = JFactory::getDBO();
		$db->setQuery("SELECT * FROM #__jev_defaults");
		$defaults = $db->loadObjectList("name");
		if (!isset($defaults['com_jevlocations.locations.detail']))
		{
			$db->setQuery("INSERT INTO  #__jev_defaults set name='com_jevlocations.locations.detail',
						title=" . $db->Quote(JText::_("JEV_LOCATION_DETAIL_PAGE")) . ",
						subject='',
						value='',
                                                params='',
						state=0");
			$db->execute();
		}
		if (!isset($defaults['com_jevlocations.locations.bloglist']))
		{
			$db->setQuery("INSERT INTO  #__jev_defaults set name='com_jevlocations.locations.bloglist',
						title=" . $db->Quote(JText::_("COM_JEVLOCATIONS_LIST_LOCATIONS_BLOG_PAGE")) . ",
						subject='',
						value='',
                                                params='',
						state=0");
			$db->execute();
		}
		/*
		if (!isset($defaults['com_jevlocations.locations.list']))
		{
			$db->setQuery("INSERT INTO  #__jev_defaults set name='com_jevlocations.locations.list',
						title=" . $db->Quote(JText::_("JEV_LOCATION_LIST_PAGE")) . ",
						subject='',
						value='',
						state=0");
			$db->execute();
		}
		*/

		// If upgrading then add new columns - do all the tables at once
		$sql = "SHOW COLUMNS FROM `#__jev_customfields3`";
		$db->setQuery($sql);
		$cols = @$db->loadObjectList("Field");

		if (!array_key_exists("target_id", $cols))
		{
			$sql = "ALTER TABLE #__jev_customfields3 ADD COLUMN target_id int(11) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();
		}

		/* We add default category for new installs */
		$query = $db->getQuery(true);
		$query->select('id');
		$query->from("#__categories AS a");
		$query->where('a.extension="com_jevlocations"');
		$db->setQuery($query);
		$location_Categories = $db->loadObjectList();

		if (count($location_Categories) == 0)
		{
			if (version_compare(JVERSION, '3.0', "<"))
			{
				JLoader::register('JTableCategory', JPATH_PLATFORM . '/joomla/database/table/category.php');
			}
			$catTable = new JTableCategory($db);
			$catTable->extension="com_jevlocations";
			$catTable->bind(array("title"=>JText::_( 'DEFAULT' ), "published"=>1,"alias"=>"default","language"=>"*","path"=>"default","access"=>1,"parent_id"=>1,"level"=>1));			
			$catTable->store();
			//Stupid fix for JTableCategory in J2.5 not saving correct level neither parent_id
			if (version_compare(JVERSION, '3.0', "<"))
			{
				$query = $db->getQuery(true);
				$query->update("#__categories AS a");
				$query->set("parent_id =1");
				$query->set("level=1");
				$query->where('a.extension="com_jevlocations"');
				$query->where('a.title="DEFAULT"');
				$db->setQuery($query);
				$db->execute();
			}
		}
		
		// performance changes to event detail table
		$sql = "SHOW COLUMNS FROM `#__jevents_vevdetail`";
		$db->setQuery($sql);
		$cols = @$db->loadObjectList("Field");

		if (!array_key_exists("loc_id", $cols))
		{
			$sql = "ALTER TABLE #__jevents_vevdetail ADD COLUMN loc_id int(11) NOT NULL default 0";
			$db->setQuery($sql);
			@$db->execute();

			$sql = "ALTER TABLE #__jevents_vevdetail ADD INDEX loc_id (loc_id)";
			$db->setQuery($sql);
			@$db->execute();
			
			// move across all the data
			$sql = "UPDATE #__jevents_vevdetail SET loc_id = CAST(location AS UNSIGNED)";
			$db->setQuery($sql);
			@$db->execute();
			
		}
	}

	protected function joomlaCheck()
	{
		$jversion = new JVersion();

		// abort if the current Joomla release is older
		if( version_compare( $jversion->getShortVersion(), $this->minimum_joomla_release, 'lt' ) )
		{
			JFactory::getApplication()->enqueueMessage(JText::sprintf("COM_JEVLOCATIONS_LOW_JOOMLA_WARNING", $this->minimum_joomla_release), 'error');
			return false;
		}

		return true;
	}

	// removes old redundance filter files
	private function cleanupFilters(){
		jimport('joomla.filesystem.folder');
		jimport('joomla.filesystem.file');		
		$folder = "jevents/jevlocations";
		if (JFolder::exists(JPATH_ROOT.'/plugins/filters') || JFolder::exists(JPATH_ROOT.'/plugins/jevents/filters')) {
			$filters = array("Geofilter","Locationcategory","Locationcities","Locationcity", "Locationcountry", "Locationlookup","Locationsearch","Locationstate");
			foreach ($filters as $filter){
				if (JFile::exists(JPATH_ROOT.'/plugins/filters/'.$filter.".php")) {
					try {
						JFile::delete(JPATH_ROOT.'/plugins/filters/'.$filter.".php");
					}
					catch (Exception $e){
						
					}
				}
				
				if (JFile::exists(JPATH_ROOT.'/plugins/jevents/filters/'.$filter.".php")) {
					try {
						JFile::delete(JPATH_ROOT.'/plugins/jevents/filters/'.$filter.".php");
					}
					catch (Exception $e){
						
					}
				}
				
			}
		}
	}
}
