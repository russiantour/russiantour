<?php
/**
 * JEvents Locations Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C)  2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

defined( 'JPATH_BASE' ) or die( 'Direct Access to this location is not allowed.' );

//error_reporting(E_ALL);

jimport('joomla.filesystem.path');

if (!defined("JEVEX_COM_COMPONENT")){
	define("JEVEX_COM_COMPONENT","com_jevlocations");
	define("JEVEX_COMPONENT",str_replace("com_","",JEVEX_COM_COMPONENT));
}

if (!defined("JEV_COM_COMPONENT")){
	include_once(JPATH_ADMINISTRATOR."/components/com_jevents/jevents.defines.php");
}

JLoader::register('JEVHelper',JPATH_SITE."/components/com_jevents/libraries/helper.php");
JLoader::register('JevLocationsHelper',JPATH_ADMINISTRATOR."/components/com_jevlocations/libraries/helper.php");
JLoader::register('JevCfForm',JPATH_SITE."/plugins/jevents/jevcustomfields/customfields/jevcfform.php");
JLoader::register('JevJoomlaVersion',JPATH_ADMINISTRATOR."/components/com_jevents/libraries/version.php");
JLoader::register('JevHtmlBootstrap' , JEV_PATH."libraries/bootstrap.php");

if (version_compare(JVERSION, "3.0.0", 'le'))
{
	JLoader::register('JLayout',JPATH_LIBRARIES."/jevents/j25legacy/jlayouts/layout.php");
	JHtml::stylesheet('lib_jevj25legacy/icomoon/icomoon.css',array(),true);
}

$option = JEVEX_COM_COMPONENT;
$lang = JFactory::getLanguage();
$lang->load($option, JPATH_COMPONENT_ADMINISTRATOR);
$lang->load($option, JPATH_COMPONENT);
$lang->load("com_jevents", JPATH_SITE);

$cmd = JRequest::getCmd('task', 'cpanel.list');
$view = JRequest::getCmd('view', 'cpanel');
$layout = JRequest::getCmd('layout', '');

if (strpos($cmd, '.') != false) {
	// We have a defined controller/task pair -- lets split them out
	list($controllerName, $task) = explode('.', $cmd);
	
	// Define the controller name and path
	$controllerName	= strtolower($controllerName);
	$controllerPath	= JPATH_COMPONENT.'/controllers/'.$controllerName.'.php';
	$controllerName = "Admin".$controllerName;
	
	// If the controller file path exists, include it ... else lets die with a 500 error
	if (file_exists($controllerPath)) {
		require_once($controllerPath);
	} else {
		JError::raiseError(500, 'Invalid Controller');
	}
} else {
	// Base controller, just set the task 
	$controllerName = $view;
	$task = $layout;
}

// Set the name for the controller and instantiate it
$controllerClass = ucfirst($controllerName).'Controller';
if (class_exists($controllerClass)) {
	$controller = new $controllerClass();
} else {
	JError::raiseError(500, 'Invalid Controller Class - '.$controllerClass );
}

$config	= JFactory::getConfig();

// Perform the Request task
$controller->execute($task);

// Redirect if set by the controller
$controller->redirect();
