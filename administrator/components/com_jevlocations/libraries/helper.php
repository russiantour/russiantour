<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined( '_JEXEC'  ) or die( 'Restricted access' );


class JevLocationsHelper {

	private $params;

	function __construct(){
		$this->params = JComponentHelper::getParams("com_jevlocations");
	}

    static function canCreateOwn()
    {
		$jeventsParams = JComponentHelper::getParams('com_jevents');
		$locationsParams = JComponentHelper::getParams("com_jevlocations");
		$authorisedonly = $jeventsParams->get("authorisedonly",0);

		if(JevLocationsHelper::userReachedMaxOwnLocations())
		{
			$msg = JText::_('COM_JEVLOCATIONS_ERROR_SAVING_LOCATION_MAX_OWN_LOCATIONS');
			JFactory::getApplication()->enqueueMessage($msg, 'error');
			return false;
		}

		if (!$authorisedonly)
		{

			$juser = JFactory::getUser();

			if( $juser->authorise('core.create', 'com_jevlocations'))
			{
					return true;
				}
			}
		else
		{
			$jevuser = JEVHelper::getAuthorisedUser();
			if 	($jevuser && $jevuser->cancreateown)
			{
				// if jevents is not in authorised only mode then switch off this user's permissions
				return true;
			}
		}
		// special case for anonymous users
		$anoncreate = $locationsParams->get("anoncreate",25);
		$user = JFactory::getUser();
		if ($anoncreate && $user->id==0 && ( JRequest::getString("task","")=="locations.save" || JRequest::getString("task","")=="locations.select"))
		{
			return true;
		}

		$isLocationCreator = false;
		JPluginHelper::importPlugin("jevents");
		$dispatcher = JEventDispatcher::getInstance();
		$dispatcher->trigger('isLocationCreator', array(& $isLocationCreator));

		return $isLocationCreator;
	}

	static function canCreateGlobal()
	{
		$params = JComponentHelper::getParams('com_jevents');
		$authorisedonly = $params->get("authorisedonly",0);
		$user = JFactory::getUser();

		if (!$authorisedonly)
		{
			$params = JComponentHelper::getParams("com_jevlocations");
			$loc_global = $params->get("loc_global",25);

			if( $user->authorise('core.createglobal', 'com_jevlocations'))
			{
					return true;
			}
		}
		else
		{
			$jevuser = JEVHelper::getAuthorisedUser();
			if 	($jevuser && $jevuser->cancreateglobal)
			{
				// if jevents is not in authorised only mode then switch off this user's permissions
				return true;
			}
		}
		// special case for anonymous users
		$params = JComponentHelper::getParams("com_jevlocations");
		$anoncreate = $params->get("anoncreate",25);

		if ($anoncreate && $user->id==0 && ( JRequest::getString("task","")=="locations.save" || JRequest::getString("task","")=="locations.select"))
		{
			return true;
		}

		return false;
	}

	static function getUserLocationsCount($idUser)
	{
		$db = JFactory::getDbo();

		$query = $db->getQuery(true);

		$query->select('COUNT(loc_id)')
			->from('#__jev_locations')
			->where('created_by = ' . (int) $idUser);
		$db->setQuery($query);

		return $db->loadResult();
	}

	static function userReachedMaxOwnLocations()
	{
		$jeventsParams = JComponentHelper::getParams('com_jevents');
		$authorisedOnly = $jeventsParams->get("authorisedonly",0);
		$jevlocParams = JComponentHelper::getParams("com_jevlocations");
		$maxOwnLocations = $jevlocParams->get("max_art", "");

		$user = JFactory::getUser();

		if ($maxOwnLocations !== "")
		{
			if (!$authorisedOnly)
			{
				// Super Users can create as many locations as they want
				if (!$user->get('isRoot'))
				{
					if (self::getUserLocationsCount($user->id) >= intval($maxOwnLocations))
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
			else
			{
				$jevuser = JEVHelper::getAuthorisedUser();

				if ($jevuser->extraslimit !== "" && intval($jevuser->extraslimit) > 0) {
					$maxOwnLocations = min(intval($maxOwnLocations), intval($jevuser->extraslimit));
				}


				if (self::getUserLocationsCount($user->id) >= $maxOwnLocations)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		return false;
	}

    static function getApiKey($keytype="browser")
    {
		$compparams = JComponentHelper::getParams("com_jevlocations");
                if ($keytype=="browser") {
                    $googlekey = $compparams->get("googlemapskey","");
                }
                else {
                    $googlekey = $compparams->get("googlemaps_serverkey","");
                }

		$uri = JURI::getInstance();
		$domain = $uri->toString(array('host'));

		$testdomain = "";
		$testkey = "";
		for ($i=1;$i<=10;$i++){
			$testdomain = $compparams->get("googledomain".$i,"");
			if ($testdomain=="") break;
			$testkey = $compparams->get("googledomainkey".$i,"");
			if ($domain == $testdomain){
				return $testkey;
			}
		}
		return $googlekey;
	}

	static function getApiUrl(){
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$googleurl = $compparams->get("googlemaps",'https://maps.googleapis.com');

		$uri = JURI::getInstance();
		$domain = $uri->toString(array('host'));

		$testdomain = "";
		$testurl = "";
		for ($i=1;$i<=10;$i++){
			$testdomain = $compparams->get("googledomain".$i,"");
			if ($testdomain=="") break;
			$testurl = $compparams->get("googledomainurl".$i,"");
			if ($domain == $testdomain && $testurl != ''){
				return $testurl;
			}
		}
		return $googleurl;

	}

	static function loadApiScript(){
		$compparams = JComponentHelper::getParams("com_jevlocations");

		$googlekey = JevLocationsHelper::getApiKey();
		$googleurl = JevLocationsHelper::getApiUrl();
		$gregion = $compparams->get("gregion",false);
		if ($gregion) {
			$gregion = "&region=$gregion";
		}

		$lang = JFactory::getLanguage();
		$hl = substr($lang->getTag(), 0, 2);
		if ($googlekey != "")
		{
			JHTML::script( $googleurl.'/maps/api/js?key=' . $googlekey.'&language='.$hl.$gregion);
		}
		else {
			JHTML::script($googleurl.'/maps/api/js?language='.$hl.$gregion);
		}
	}

	static function getApiScript(){
		$compparams = JComponentHelper::getParams("com_jevlocations");

		$googlekey = JevLocationsHelper::getApiKey();
		$googleurl = JevLocationsHelper::getApiUrl();
		$gregion = $compparams->get("gregion",false);
		if ($gregion) {
			$gregion = "&region=$gregion";
		}

		$lang = JFactory::getLanguage();
		$hl = substr($lang->getTag(), 0, 2);
		if ($googlekey != "")
		{
			return  $googleurl.'/maps/api/js?key=' . $googlekey.'&language='.$hl.$gregion;
		}
		else {
			return  $googleurl.'/maps/api/js?'.'language='.$hl.$gregion;
		}
	}


	static function getMapsUrl()
	{
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$googleurl = $compparams->get("googledirections",'https://maps.google.com');

		$uri = JURI::getInstance();
		$domain = $uri->toString(array('host'));

		$testdomain = "";
		$testurl = "";
		for ($i=1;$i<=10;$i++){
			$testdomain = $compparams->get("googledomain".$i,"");
			if ($testdomain=="") break;
			$testurl = $compparams->get("googledomainurl".$i,"");
			if ($domain == $testdomain){
				return $testurl;
			}
		}

		return $googleurl;
	}

	/**
	 * Method to get the Slug for Google Maps URL
	 * @param	float	$lat		Latitude
	 * @param	float	$long		Longitude
	 * @param	integer	$zoom		Zoom
	 * @param	string	$subtitle	Text for the query
	 *
	 * @return	string
	 */
	public static function getDirectionsSlug($lat, $long, $zoom, $subtitle="")
	{
		$directionsSlug = "";
		$params = JComponentHelper::getParams("com_jevlocations");

		$maptype = $params->get("maptype", "ROADMAP") == "ROADMAP" ? "m" : "h";

		switch ($params->get('directionsmapquery', 'coordinates'))
		{
			case 'name':
				$directionsSlug = "/maps?f=q"
									. "&ie=UTF8"
									. "&t=" . $maptype
									. "&om=1"
									. "&q=" . $subtitle
									. "&@" . $lat . "," . $long
									. "&ll=" . $lat . "," . $long
									. "&z=" . $zoom
									. "&iwloc=addr";
			break;
			case 'coordinates':
				$directionsSlug = "/maps?f=q"
									. "&ie=UTF8"
									. "&t=" . $maptype
									. "&om=1"
									. "&q=" . $lat . "," . $long
									. "&@" . $lat . "," . $long
									. "&ll=" . $lat . "," . $long
									. "&z=" . $zoom
									. "&iwloc=addr";
			default:
			break;
		}

		return $directionsSlug;
	}

	static function getNewCategory($oldcat)
	{
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('newcatid');
		$query->from('#__jevlocations_oldcatmap');
		$query->where('oldcatid = '.(int)$oldcat);
		$db->setQuery($query);
		$newcatid = $db->loadResult();

		return $newcatid;
	}

	static function canUploadImages() {
		$params = JComponentHelper::getParams('com_jevents');
		$authorisedonly = $params->get("authorisedonly",0);
		if (!$authorisedonly){
			$params = JComponentHelper::getParams("com_jevlocations");
			$uploadImages = $params->get("upimageslevel",25);
			$juser = JFactory::getUser();
			if (version_compare(JVERSION, "1.6.0", 'ge'))
			{
				if( $juser->authorise('core.uploadimages', 'com_jevlocations')){
					return true;
				}
			}
			else if ($juser->gid >= intval($uploadImages)){
				return true;
			}
		}
		else {
			$jevuser = JEVHelper::getAuthorisedUser();
			if 	($jevuser && $jevuser->canuploadimages){
				return true;
			}
		}
		return false;
	}

	function processFileUpload($file){
		set_time_limit(1800);

		if (!array_key_exists("HTTP_REFERER",$_SERVER) || (strpos($_SERVER["HTTP_REFERER"],"http://".$_SERVER["HTTP_HOST"] )!==0 && strpos($_SERVER["HTTP_REFERER"],"https://".$_SERVER["HTTP_HOST"] )!==0)){
			die();
		}

		if (!isset($_FILES[$file]) || $_FILES[$file]['error']==UPLOAD_ERR_NO_FILE) {
			$this->error_goback(JText::_( 'MISSING_FILE' ));
		}

		// this should be set in config
		$filesize = $_FILES[$file]["size"];
		$maxsize = $this->params->get("maxuploadfile",2000000);
		if ($_FILES[$file]["size"]>$maxsize){
			$this->error_goback(JText::sprintf("File Too Large",$filesize,$maxsize));
		}

		$this->securityCheck( $_FILES[$file]);

		$suffix = "pdf";
		if (!$this->checkFileType($_FILES[$file]["type"],$_FILES[$file]["name"],$suffix)){
			$this->error_goback(JText::_( 'INVALID_FILE_TYPE' ));
		}

		$ftmp = $_FILES[$file]['tmp_name'];
		$oname = $_FILES[$file]['name'];

		$fileName = $oname;

		$fileName = uniqid(null,true).".".$suffix;

		$folder		= JRequest::getVar( 'folder', '', '', 'path' );

		$filelocation = JEVP_MEDIA_BASE.'/'.$folder;


		$fname = $filelocation.'/'.$fileName;
		jimport("joomla.filesystem.file");
		if(!JFile::copy($ftmp, $fname)){
			if (!rename($ftmp, $fname)){
				$this->error_goback(JText::_( 'COULD_NOT_SAVE' ));
			}
		}
		@chmod($fname,0644);

		return $fileName;
	}


	function processImageUpload($file){
		set_time_limit(1800);

		if (!array_key_exists("HTTP_REFERER",$_SERVER) || (strpos($_SERVER["HTTP_REFERER"],"http://".$_SERVER["HTTP_HOST"] )!==0 && strpos($_SERVER["HTTP_REFERER"],"https://".$_SERVER["HTTP_HOST"] )!==0)){
			die();
		}

		if (!isset($_FILES[$file]) || $_FILES[$file]['error']==UPLOAD_ERR_NO_FILE) {
			$this->error_goback(JText::_( 'MISSING_IMAGE_FILE' ));
		}

		// this should be set in config
		$maxsize = $this->params->get("maxupload",2000000);
		$imagesize = $_FILES[$file]["size"];
		if ($_FILES[$file]["size"]>$maxsize){
			$this->error_goback(JText::sprintf("Image Too Large",$imagesize,$maxsize));
		}

		$this->securityCheck( $_FILES[$file]);

		$suffix = "jpg";
		if (!$this->checkImageType($_FILES[$file]["type"],$suffix)){
			$this->error_goback(JText::sprintf("Invalid TYPE",$_FILES[$file]["type"]));
		}
		$ftmp = $_FILES[$file]['tmp_name'];
		$oname = $_FILES[$file]['name'];

		$fileName = uniqid(null,true).".".$suffix;

		$folder = "jevents/jevlocations";

		$filelocation = JEVP_MEDIA_BASE.'/'.$folder;
		$this->ensureDirExists($filelocation);

		$fname = $filelocation.'/'.$fileName;
		jimport("joomla.filesystem.file");
		if(!JFile::copy($ftmp, $fname)){
			if (!rename($ftmp, $fname)){
				$this->error_goback(JText::_( 'COULD_NOT_SAVE' ));
			}
		}
		@chmod($fname,0644);

		// scale the image
		$imagew = $this->params->get("imagew");
		$imageh = $this->params->get("imageh");
		$nothumbnail = $this->params->get("nothumbnail");
		$test = $this->params->toArray();
		if ($imagew>0 && $imageh>0){
			$this->scaleImage($fname,$imagew,$imageh, $imagesize, $nothumbnail,false);
		}
		// create the thumbnail
		$thumbw = $this->params->get("thumbw");
		$thumbh = $this->params->get("thumbh");
		$this->scaleImage($fname, $thumbw, $thumbh, $imagesize, $nothumbnail,true);

		return $fileName;
	}

	function error_goback($msg){
	?>
	<html>
	<head>
	<script  type="text/javascript">
	alert("<?php echo $msg;?>");
	history.go(-1);
		</script>
	</head>
	<body>
	</body>
	</html>
	<?php
	exit();
	}

	function checkImageType($type,&$suffix){
		static $allowedImageTypes = array("image/png","image/jpeg","image/pjpeg","image/gif");
		if (!in_array($type,$allowedImageTypes)){
			return false;
		}
		$suffix = str_replace("image/","",$type);
		return true;
	}

	function checkFileType($type,$filename,&$suffix){
		static $allowedfileExtensions = array("pdf","xls","xlsx","doc","docx","flv");
		if (strrpos($filename,".")>0){
			$suffix = strtolower(substr($filename,strrpos($filename,".")+1));
			if (in_array($suffix,$allowedfileExtensions)){
				return true;
			}
		}
		return false;
	}


	function securityCheck($file){
		if (!is_uploaded_file($file["tmp_name"])){
			//$this->error_goback("Problems uploading file <b>".$file['name']."</b>");
			// Failed for various reasons
			switch ($file["error"] ) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_INI_SIZE:
					$this->error_goback("The uploaded file exceeds the upload_max_filesize directive (".ini_get("upload_max_filesize").") in php.ini.");
					break;
				case UPLOAD_ERR_FORM_SIZE:
					$this->error_goback("The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.");
					break;
				case UPLOAD_ERR_PARTIAL:
					$this->error_goback( "The uploaded file was only partially uploaded.");
					break;
				case UPLOAD_ERR_NO_FILE:
					$this->error_goback("No file was uploaded.");
					break;
				case UPLOAD_ERR_NO_TMP_DIR:
					$this->error_goback("Missing a temporary folder.");
					break;
				case UPLOAD_ERR_CANT_WRITE:
					$this->error_goback("Failed to write file to disk");
					break;
				default:
					$this->error_goback("Unknown File Error");
			}
		}
	}

	function scaleImage($img, $maxwidth=150,$maxheight=150, $imagesize=10, $nothumbnail=1500000, $thumb=true){
		$info = getimagesize($img);
		if (!$info) {
			$this->error_goback(JText::_( 'PROBLEMS_CREATING_THUMBNAIL' ));
			return;
		}
		$origw = $info[0];
		$origh = $info[1];
		// TODO thumbnail size in config
		// don't make thumbnail bigger than original!!
		if ($origw<=$maxwidth && $origh<=$maxheight){
			$thumbWidth = $origw;
			$thumbHeight = $origh;
		}
		else {
			$thumbWidth = $maxwidth;
			$thumbHeight = intval($origh * $thumbWidth/$origw);
			if ($thumbHeight>$maxheight){
				$thumbHeight = $maxheight;
				$thumbWidth = intval($origw * $thumbHeight/$origh);
			}
		}

		$imgtypes = array( 1 => 'GIF', 2 => 'JPG', 3 => 'PNG', 4 => 'SWF', 5 => 'PSD', 6 => 'BMP', 7 => 'TIFF', 8 => 'TIFF', 9 => 'JPC', 10 => 'JP2', 11 => 'JPX', 12 => 'JB2', 13 => 'SWC', 14 => 'IFF', 15 => 'WBMP', 16 => 'XBM');

		// GD can only handle JPG & PNG images
		if ($info[2] >=4) {
			$this->error_goback(JText::_( 'ERROR_FILE_TYPE' ));
		}

		// Create the thumbnail
		if (!function_exists('imagecreatefromjpeg')) {
			$this->error_goback(JText::_( 'PROBLEMS_CREATING_THUMBNAIL' )." 2");
			return false;
		}
		if (!function_exists('imagecreatetruecolor')) {
			$this->error_goback(JText::_( 'PROBLEMS_CREATING_THUMBNAIL' )." 3");
			return false;
		}

		if ($imagesize>$nothumbnail){
			$src_img = imagecreatefromjpeg(dirname(__FILE__)."/success.jpg");
			$origh=100;
			$origw=149;
			$thumbHeight=100;
			$thumbWidth=150;
		}
		else {
			if ($info[2] == 2) {
				$src_img = imagecreatefromjpeg($img);
			}
			else if ($info[2] == 3){
				$src_img = imagecreatefrompng($img);
			}
			else {
				$src_img = imagecreatefromgif($img);
			}
		}

		if (!$src_img){
			$this->error_goback(JText::_( 'PROBLEMS_CREATING_THUMBNAIL' )." 4");
			return false;
		}

		// TODO set thumbnail directories in config
		if ($thumb){

			$dst_img = imagecreatetruecolor($thumbWidth, $thumbHeight);

			if (strpos(basename($img),".png")>0){

				// Turn off alpha blending and set alpha flag
				imagealphablending($dst_img, false);
				imagesavealpha($dst_img, true);

				$transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
				imagefilledrectangle($dst_img, 0, 0, $thumbWidth, $thumbHeight, $transparent);
			}

			$res = imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origw, $origh);
			//$res = imagecopymerge($dst_img, $src_img, 0, 0, 0, 0, $thumbWidth, $thumbHeight,100);

			$thumbdir = dirname($img)."/thumbnails";
			$dest_file = $thumbdir."/thumb_".basename($img);

			$this->ensureDirExists($thumbdir);

			$tmp = tempnam($thumbdir,"img");

			if (strpos(basename($img),".png")>0){
				imagepng($dst_img, $tmp);
			}
			else {
				// TODO set quality for image save in config
				imagejpeg($dst_img, $tmp,80);
			}

			if (!JFile::copy($tmp,$dest_file)){
				var_dump(JError::getErrors());
				return false;
			}
			unlink($tmp);

		}
		else {
			$thumbdir = dirname($img);
			$dest_file = $thumbdir.'/'.basename($img);

			$dst_img = imagecreatetruecolor($thumbWidth, $thumbHeight);

			if (strpos(basename($img),".png")>0){

				// Turn off alpha blending and set alpha flag
				imagealphablending($dst_img, false);
				imagesavealpha($dst_img, true);

				$transparent = imagecolorallocatealpha($dst_img, 255, 255, 255, 127);
				imagefilledrectangle($dst_img, 0, 0, $thumbWidth, $thumbHeight, $transparent);
			}

			imagecopyresampled($dst_img, $src_img, 0, 0, 0, 0, $thumbWidth, $thumbHeight, $origw, $origh);

			$tmp = tempnam($thumbdir,"img");
			if (strpos(basename($img),".png")>0){
				imagepng($dst_img, $tmp);
			}
			else {
				// TODO set quality for image save in config
				imagejpeg($dst_img, $tmp,80);
			}

			if (!JFile::copy($tmp,$dest_file)){
				var_dump(JError::getErrors());
			}
			unlink($tmp);
		}
		// remove copies in memory
		imagedestroy($src_img);
		imagedestroy($dst_img);

		// We check that the image is valid
		$imginfo = getimagesize($dest_file);
		if ($imginfo == null){
			$this->error_goback(JText::_( 'PROBLEMS_CREATING_THUMBNAIL' )." 5");
			return false;
		} else {
			@chmod($dest_file,0644);
			return true;
		}
	}

	function ensureDirExists($targetDir){
		clearstatcache();
                                    jimport('joomla.filesystem.folder');
		if (!JFolder::exists($targetDir )) {
			if (!JFolder::create($targetDir)) {
				$this->error_goback( "can't create directory $targetDir");
			}
		}
	}

	public static function addSubmenu($vName = "")
	{
		$task = JRequest::getCmd("task", "cpanel.cpanel");
		$option = JRequest::getCmd("option", "com_categories");

		//Use a bit of CSS to hide the Options button on category page.
		//Added if statement, incase in future we extend the helper, this way it will only ever apply to the categories component.
		if ($option == 'com_categories')
		{
			$stylelink = '<style type="text/css">' . "\n";
			//Check if Joomla 3.0 or 2.5? 3.0 has bootstrap so different id.
			if (!version_compare(JVERSION, "3.0.0", 'ge'))
			{
				$stylelink .= '#toolbar-popup-options {display:none;}' . "\n";
			}
			elseif (version_compare(JVERSION, '3.0.0', ">="))
			{
				$stylelink .= '#toolbar-options {display:none;}' . "\n";
			}
			$stylelink .= '</style>' . "\n";

			$document = JFactory::getDocument();
			$document->addCustomTag($stylelink);
		}

		if ($vName == "")
		{
			$vName = $task;
		}
		if (version_compare(JVERSION, "3.0.0", 'ge'))
		{
			JHtmlSidebar::addEntry(
					JText::_('CONTROL_PANEL'), 'index.php?option=com_jevlocations', $vName == 'cpanel.cpanel'
			);

			JHtmlSidebar::addEntry(
					JText::_('MANAGE_LOCATIONS'), 'index.php?option=com_jevlocations&task=locations.overview', $vName == 'locations.overview'
			);

			$compparams = JComponentHelper::getParams("com_jevlocations");
			$usecats = $compparams->get("usecats", 0);
			if ($usecats)
			{
				JHtmlSidebar::addEntry(JText::_('COM_JEVLOCATIONS_COUNTRY_STATE_CITY'), "index.php?option=com_jevlocations&task=categories.list", $vName == 'categories.list');
			}
			JHtmlSidebar::addEntry(JText::_('COM_JEVLOCATIONS_CATEGORIES'), 'index.php?option=com_categories&extension=com_jevlocations', false);
			if (JEVHelper::isAdminUser())
			{
				JFactory::getLanguage()->load("com_jevents", JPATH_ADMINISTRATOR);
				JHtmlSidebar::addEntry( JText::_('JEV_LAYOUT_DEFAULTS'), "index.php?option=com_jevents&task=defaults.list&filter_layout_type=jevlocations", false);
			}
			JHtmlSidebar::addEntry( JText::_('COM_JEVLOCATIONS_RETURN_TO_JEVENTS'), "index.php?option=com_jevents&task=cpanel.cpanel", false);
		}
		else {
			JSubMenuHelper::addEntry(
					JText::_('CONTROL_PANEL'), 'index.php?option=com_jevlocations', $vName == 'cpanel.cpanel'
			);

			JSubMenuHelper::addEntry(
					JText::_('MANAGE_LOCATIONS'), 'index.php?option=com_jevlocations&task=locations.overview', $vName == 'icalevent.list'
			);

			JSubMenuHelper::addEntry(
					JText::_('COM_JEVLOCATIONS_CATEGORIES'), 'index.php?option=com_categories&extension=com_jevlocations', $vName == 'icalevent.list'
			);
		}
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param	int		The category ID.
	 * @param	int		The article ID.
	 *
	 * @return	JObject
	 */
	public static function getActions($categoryId = 0, $articleId = 0)
	{
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($articleId) && empty($categoryId))
		{
			$assetName = 'com_jevents';
		}
		else if (empty($articleId))
		{
			$assetName = 'com_jevents.category.' . (int) $categoryId;
		}
		else
		{
			$assetName = 'com_jevents.article.' . (int) $articleId;
		}

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action)
		{
			$result->set($action, $user->authorise($action, $assetName));
		}

		return $result;

	}

	public static function setupDetailModal($params,$locurl)
	{
		$document = JFactory::getDocument();
		$pwidth = $params->get("pwidth", "750");
		$pheight = $params->get("pheight", "500");

		$modalSelector = "jevlocationModal";
		$modalCss = "#".$modalSelector.'{width: '.$pwidth.'px;}';
		$document->addStyleDeclaration($modalCss);

		JevModal::modal($modalSelector, array('size' => "$pheight,$pwidth"));

		$modalScript = 'jevModalPopup("' . $modalSelector . '", "' . $locurl . '", "");';

		return $modalScript;
	}

	public static function getLocationsCustomFields(&$items)
	{
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$template = $compparams->get("fieldtemplate", "");

		if ($template != "" && $compparams->get("custinlist") && JPluginHelper::isEnabled("jevents", 'jevcustomfields'))
		{
			$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
			if (file_exists($xmlfile))
			{
				$db = JFactory::getDBO();
				foreach ($items as &$item)
				{
					$query = $db->getQuery(true);
					$query->select('*')
							->from($db->quoteName('#__jev_customfields3'))
							->where($db->quoteName('target_id') . " = " . intval($item->loc_id) )
							->where($db->quoteName('targettype') . " = " . $db->quote('com_jevlocations'));
					$db->setQuery($query);
					$cfdata = $db->loadObjectList('name');
					$customdata = array();
					foreach ($cfdata as $dataelem)
					{
						if (strpos($dataelem->name, ".") !== false)
						{
							$dataelem->name = str_replace(".", "_", $dataelem->name);
						}
						$customdata[$dataelem->name] = $dataelem->value;
					}

					$item->customfields = false;
					if (count($customdata))
					{
						$jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
						//$jcfparams = new JevCfForm("com_jevlocations.customfields",array('control' => 'jform', 'load_data' => true));
						$jcfparams->bind($customdata);
						$jcfparams->setEvent($item);
						$customfields = array();
						$groups = $jcfparams->getFieldsets();
						foreach ($groups as $group => $element)
						{
							if ($jcfparams->getFieldCountByFieldSet($group))
							{
								$customfields = array_merge($customfields, $jcfparams->renderToBasicArray('params', $group));
							}
						}

						$item->customfields = $customfields;

						$jcfparams->reset();
					}

					unset($item);


				}
			}
		}

	}

	public static function getLocationsCustomFieldsForEvents($events)
	{
		$locations = array();

		foreach($events as $event)
		{
			$location = new stdClass();

			if(!array_key_exists($event->_loc_id, $locations))
			{
				$location->loc_id = $event->_loc_id;
				$location->name = $event->_loc_name;
				$location->city = $event->_loc_city;
				$location->country = $event->_loc_country;
				$location->desc = $event->_loc_desc;
				$location->image = $event->_loc_image;
				$location->lat = $event->_loc_lat;
				$location->lon = $event->_loc_lon;
				$location->mapicon = $event->_loc_mapicon;
				$location->phone = $event->_loc_phone;
				$location->postcode = $event->_loc_postcode;
				$location->state = $event->_loc_state;
				$location->street = $event->_loc_street;
				$location->title = $event->_loc_title;
				$location->url = $event->_loc_url;
				$location->zoom = $event->_loc_zoom;
				$location->category_list = $event->_location_category_list;
				$location->catid_list = $event->_location_catid_list;

				$locArray[0] = $location;

				self::getLocationsCustomFields($locArray);

				$location = $locArray[0];

				$locations[$event->_loc_id] = $location;
			}
			$event->_location = & $locations[$event->_loc_id];
		}

		//self::getLocationsCustomFields($locations);

		return $events;
	}	

	//New MetaSet Function, to set the meta tags if they exist in the Menu Item

	static public
	function SetMetaTags()
	{
		//Get Document to set the Meta Tags to.
		$document = JFactory::getDocument();

		//Get the Params.
		$params = JComponentHelper::getParams('com_jevlocations');

		if ($params->get('menu-meta_description') && !$document->getDescription())
		{
			$document->setDescription($params->get('menu-meta_description'));
		}

		if ($params->get('menu-meta_keywords') && !$document->getMetaData('keywords'))
		{
			$document->setMetaData('keywords', $params->get('menu-meta_keywords'));
		}
	}
}
