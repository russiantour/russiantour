<?php

// No direct access
defined('_JEXEC') or die;

/**
 * JEvents component helper.
 *
 * @package		Jevents
 * @since		1.6
 */
class JevlocationsHelper
{
	public static $extension = 'com_jevlocations';

	/**
	 * Configure the Linkbar.
	 *
	 * @param	string	The name of the active view.
	 */
	public static function addSubmenu($vName)
	{
		//JSubMenuHelper deprecated in 4.0
		if (version_compare(JVERSION, '3.0', ">=")){
			JHtmlSidebar::addEntry(JText::_( 'CONTROL_PANEL' ),'index.php?option=com_jevlocations', false);
			JHtmlSidebar::addEntry(JText::_('MANAGE_LOCATIONS'), 'index.php?option=com_jevlocations&task=locations.overview', false);
			$compparams = JComponentHelper::getParams("com_jevlocations");
			$usecats = $compparams->get("usecats", 0);
			if ($usecats)
			{
				JHtmlSidebar::addEntry(JText::_('COM_JEVLOCATIONS_COUNTRY_STATE_CITY'), "index.php?option=com_jevlocations&task=categories.list", false);
			}
			JHtmlSidebar::addEntry(JText::_('COM_JEVLOCATIONS_CATEGORIES'), 'index.php?option=com_categories&extension=com_jevlocations', true);
			JLoader::register('JEVHelper',JPATH_SITE."/components/com_jevents/libraries/helper.php");
			if (JEVHelper::isAdminUser())
			{
				JFactory::getLanguage()->load("com_jevents", JPATH_ADMINISTRATOR);
				JHtmlSidebar::addEntry( JText::_('JEV_LAYOUT_DEFAULTS'), "index.php?option=com_jevents&task=defaults.list&filter_layout_type=jevlocations", false);
			}
			JHtmlSidebar::addEntry( JText::_('COM_JEVLOCATIONS_RETURN_TO_JEVENTS'), "index.php?option=com_jevents&task=cpanel.cpanel", false);


		}
		else
		{
			JSubMenuHelper::addEntry(JText::_('CONTROL_PANEL'),'index.php?option=com_jevlocations', false);
			JSubMenuHelper::addEntry(JText::_('MANAGE_LOCATIONS'),'index.php?option=com_jevlocations&task=locations.overview',false);
			JSubMenuHelper::addEntry(JText::_('COM_JEVLOCATIONS_CATEGORIES'),'index.php?option=com_categories&extension=com_jevlocations',true);
		}
		
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param	int		The category ID.
	 * @param	int		The article ID.
	 *
	 * @return	JObject
	 */
	public static function getActions($categoryId = 0, $articleId = 0)
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		if (empty($articleId) && empty($categoryId)) {
			$assetName = 'com_jevlocations';
		}
		else if (empty($articleId)) {
			$assetName = 'com_jevlocations.category.'.(int) $categoryId;
		}
		else {
			$assetName = 'com_jevlocations.article.'.(int) $articleId;
		}

		$actions = array(
			'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.state', 'core.delete'
		);

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;
	}
}