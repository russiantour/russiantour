<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined( '_JEXEC'  ) or die( 'Restricted access' );

jimport('joomla.application.component.controller');

JLoader::register('JevLocationsCategory',JPATH_COMPONENT_ADMINISTRATOR."/libraries/categoryClass.php");

class AdminCategoriesController extends JControllerLegacy {
	var $component = null;
	var $categoryTable = null;
	var $categoryClassname = null;
	var $categoryColumnName = null;
	var $nameTitleColumnName = null;
	var $groupAccessTableName = null;
	var $orderingColumn = null;

	/**
	 * Controler for the Control Panel
	 * @param array		configuration
	 */
	function __construct($config = array())
	{

		parent::__construct($config);
		$this->registerTask( 'list',  'overview' );
		$this->registerDefaultTask("overview");

		$this->component = 	JEVEX_COM_COMPONENT;
		$this->categoryTable = "#__jevlocation_categories";
		$this->categoryClassname = "JevLocationsCategory";
		
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			//$this->categoryColumnName = 'extension';
			//$this->orderingColumn = 'c.lft asc ';
			$this->categoryColumnName = 'section';
			$this->orderingColumn = 'ordering ';
			$this->nameTitleColumnName = 'title';
			$this->groupAccessTableName = '#__viewlevels';
		} else {
			$this->categoryColumnName = 'section';
			$this->nameTitleColumnName = 'name';
			$this->groupAccessTableName = '#__groups';
			$this->orderingColumn = 'ordering ';		
		}		

	}

	/**
	 * Category Management code
	 *
	 * Author: Geraint Edwards
	 */
	/**
	 * Manage categories - show lists
	 *
	 */
	function overview( )
	{
		
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats",0);
		if (!$usecats){
			$this->setRedirect( "index.php?option=$this->component&task=cpanel.cpanel", JText::_("COM_JEVLOCATIONS_CITY_STATE_ERROR") );
			return;
		}

		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - Make J!1.6 compatible
		$section_name  = JRequest::getString("section","com_jevlocations");
		$db	= JFactory::getDBO();
		$user = JFactory::getUser();
		
		if (!(JEVHelper::isAdminUser($user))){
			$this->setRedirect( "index.php?option=$this->component&task=cpanel.cpanel", JText::_("COM_JEVLOCATIONS_UNAUTHORIZED_USER") );
			return;
		}

		$limit		= intval( $mainframe->getUserStateFromRequest( "cat_listlimit", 'limit', 10 ));
		$limitstart = intval( $mainframe->getUserStateFromRequest( "cat_{$this->component}limitstart", 'limitstart', 0 ));
		$catfilter	= intval( $mainframe->getUserStateFromRequest( "cat_catid", 'catid', 0 ));
		$catfilter2	= intval( $mainframe->getUserStateFromRequest( "cat_catid2", 'catid2', 0 ));

		$search	= $mainframe->getUserStateFromRequest( JEVEX_COM_COMPONENT .'loccats_search',			'search',			'',				'string' );
		$search	= JString::strtolower( $search );

		$where = "";
		if (trim($search)!="") {
			$where = ' AND (LOWER(c.title) LIKE '.$db->Quote( '%'.$db->escape( $search, true ).'%', false ). ')';
		}

		// get the total number of records
		if ($catfilter>0){
			$query = "SELECT COUNT(*) FROM $this->categoryTable AS c "
			. " LEFT JOIN $this->categoryTable AS pc on c.parent_id = pc.id"
			. " LEFT JOIN $this->categoryTable AS gpc on pc.parent_id = gpc.id"
			. " WHERE c.$this->categoryColumnName = '$section_name' "
			. " AND (c.id=$catfilter OR pc.id=$catfilter OR gpc.id=$catfilter)";
			if ($catfilter2>0){
				$query .= " AND (c.id=$catfilter2 OR pc.id=$catfilter2 OR gpc.id=$catfilter2)";
			}
			$query .= $where;
		}
		else {
			$query = "SELECT COUNT(*) FROM $this->categoryTable AS c WHERE c.$this->categoryColumnName = '$section_name' ";
			if ($catfilter2>0){
				$query .= " AND c.id=$catfilter2";
			}
			$query .= $where;
		}

		$db->setQuery( $query);
		$total = $db->loadResult();
		echo $db->getErrorMsg();

		if( $limit > $total ) {
			$limitstart = 0;
		}

		$db	= JFactory::getDBO();

		// get the total number of records

		if ($catfilter>0){
			$sql = "SELECT c.* , g.{$this->nameTitleColumnName} AS _groupname, pc.title AS parenttitle FROM $this->categoryTable AS c"
			. " LEFT JOIN $this->categoryTable AS pc on c.parent_id = pc.id"
			. " LEFT JOIN $this->categoryTable AS gpc on pc.parent_id = gpc.id"
			. " LEFT JOIN $this->groupAccessTableName AS g ON g.id = c.access"
			. " WHERE c.$this->categoryColumnName = '$section_name'"
			. " AND (c.id=$catfilter OR pc.id=$catfilter OR gpc.id=$catfilter)"
			. (($catfilter2>0)?" AND (c.id=$catfilter2 OR pc.id=$catfilter2 OR gpc.id=$catfilter2)":"")
			. $where
			. " ORDER BY $this->orderingColumn, parenttitle, c.title ";
		}
		else {
			$sql = "SELECT c.* , g.{$this->nameTitleColumnName} AS _groupname, pc.title AS parenttitle FROM $this->categoryTable AS c"
			. " LEFT JOIN $this->categoryTable AS pc on c.parent_id = pc.id"
			. " LEFT JOIN $this->groupAccessTableName AS g ON g.id = c.access"
			. " WHERE c.$this->categoryColumnName = '$section_name' "
			. (($catfilter2>0)?" AND (c.id=$catfilter2 OR pc.id=$catfilter2)":"")
			. $where
			. " ORDER BY $this->orderingColumn, parenttitle, c.title ";

		}
		if ($limit>0){
			$sql .= "\n LIMIT $limitstart, $limit";
		}

		$db->setQuery($sql);
		$rows = $db->loadObjectList();

		$cats = array();
		if ($rows){
			foreach ($rows AS $row) {
				$cat = new $this->categoryClassname($db,$this->categoryTable);
				$column = $this->categoryColumnName;
				$cat->bind(get_object_vars($row), $row->$column);
				// extra fields
				$cat->_groupname = $row->_groupname;
				$cat->parenttitle = $row->parenttitle;
				$cats[$cat->id]=$cat;
			}
		}

		jimport('joomla.html.pagination');
		$pageNav = new JPagination( $total, $limitstart, $limit  );

		// get the view
		$this->view = $this->getView("categories", "html");

		// Set the layout
		$this->view->setLayout('overview');
		$this->view->assign('title', JText::_("COM_JEVLOCATIONS_CATEGORIES"));
		$this->view->assign('cats', $cats);
		$this->view->assign('catfilter', $catfilter);
		$this->view->assign('catfilter2', $catfilter2);
		$this->view->assign('pageNav', $pageNav);

		$this->view->display();

	}

	/**
	 * Category Editing code
	 *
	 * Author: Geraint Edwards
	 * 
	 */
	function edit(){
		$cid = JRequest::getVar(	'cid',	array(0) );
		JArrayHelper::toInteger($cid);

		$section_name  = JRequest::getString("section","com_jevlocations");

		$user = JFactory::getUser();

		if (!(JEVHelper::isAdminUser($user))){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=categories", JText::_("COM_JEVLOCATIONS_UNAUTHORIZED_USER") );
			return;
		}

		$db	= JFactory::getDBO();

		if (count($cid)<=0){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=categories", "Invalid Category Selection" );
			return;
		}
		else {
			$cid=$cid[0];
		}
		$cat = new $this->categoryClassname($db,$this->categoryTable);
		$cat->load($cid);

		/*
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			$column = 'extension';
			$parent_id = 1;
		} else {
		 * 
		 */
			// We use our own location category table which still has section and not extension
			$column = 'section';
			$parent_id = 0;
		//}

		$sql = "SELECT c.* FROM $this->categoryTable AS c "
		."\n WHERE $column = '$section_name' AND c.id<>$cid"
		."\n ORDER BY $this->orderingColumn"
		;
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$cats = array();
		// empty row
		$emptycat = new $this->categoryClassname($db,$this->categoryTable);
		$emptycat->title=JText::_("COM_JEVLOCATIONS_CATEGORY_PARENT_NONE");
		$cats[0]=$emptycat;

		if ($rows){
			foreach ($rows AS $row) {
				$tempcat = new $this->categoryClassname($db,$this->categoryTable);
				$tempcat->bind(get_object_vars($row), $row->$column);
				$cats[]=$tempcat;

			}
		}
		JLoader::register('JevLocationsHTML',JPATH_ADMINISTRATOR."/components/com_jevlocations/libraries/jevlocationshtml.php");
		$plist = JevLocationsHTML::buildCategorySelect(intval( $cat->parent_id ),'',"",false,true,0,'parent_id','com_jevlocations');

		// get list of groups
		$query = "SELECT id AS value, " . $this->nameTitleColumnName . " AS text"
		. "\n FROM " . $this->groupAccessTableName
		. "\n ORDER BY id"
		;
		$db->setQuery( $query );
		$groups = $db->loadObjectList();

		// build the html select list
		$glist = JHTML::_('select.genericlist', $groups, 'access', 'class="inputbox" size="1"',
		'value', 'text', intval( $cat->access ) );

		// get the view
		$this->view = $this->getView("categories","html");

		// Set the layout
		$this->view->setLayout('edit');
		$this->view->assign('title'   , JText::_("COM_JEVLOCATIONS_CATEGORIES"));
		$this->view->assign('cat',$cat);
		$this->view->assign('plist',$plist);
		$this->view->assign('glist',$glist);

		$this->view->display();
	}

	/**
	 * Category Saving code
	 *
	 * Author: Geraint Edwards
	 * 
	 */
	function save(){
		$db	= JFactory::getDBO();
		$user = JFactory::getUser();

		$cid = JRequest::getVar(	'cid',	array(0) );
		JArrayHelper::toInteger($cid);
		
		if (!(JEVHelper::isAdminUser($user))){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=cpanel.cpanel", JText::_("COM_JEVLOCATIONS_UNAUTHORIZED_USER") );
			return;
		}

		$cat = new $this->categoryClassname($db, $this->categoryTable);
		
		$category_vars = JRequest::get('request', JREQUEST_ALLOWHTML);
		
		// RSH 10/19/10 - Added  this logic to get and set the parent id
		// make sure there is a parent
		/*
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			if (!$category_vars['parent_id']){
				$cat->setLocation(1, 'last-child');  // RSH Note - in J!1.6 the parent is set to '1'm which is different from J!1.5 where it was set to '0'!  This is also changed in administrator\components\com_jevlocations\views\locations\view.html.php!
				$category_vars['parent_id'] = 1;
			} else {
				$cat->setLocation($category_vars['parent_id'], 'last-child');
			}
		}
		 */
				

		$section = JRequest::getString("section","com_jevlocations");
		if (!$cat->bind($category_vars, $section)) {
			echo "<script> alert('".$cat->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}

		if (!$cat->check()) {
			echo "<script> alert('".$cat->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}

		if (!$cat->store()) {
			echo "<script> alert('".$cat->getError()."'); window.history.go(-1); </script>\n";
			exit();
		}
		
		// RSH 10/19/10 - Added  this logic to get and rebuild the path 
	 	// Rebuild the tree path.
		/*
		if (version_compare(JVERSION, "1.6.0", 'ge')) {
			if (!$cat->rebuildPath($cat->id)) {
				$this->setError($cat->getError());
				echo "<script> alert('".$cat->getError()."'); window.history.go(-1); </script>\n";
				exit();
			}
		}
		 */
		
		$cat->checkin();
		$column = $this->categoryColumnName;
		$cat->reorder( "section='".$cat->$column."'" );

		$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=categories.list&section=$section", JText::_('COM_JEVLOCATIONS_ADMIN_CATSUPDATED'));

	}

	/**
	 * Category Ordering code
	 *
	 * Author: Geraint Edwards
	 * Copyright: 2007 Geraint Edwards
	 * 
	 */
	function saveorder(){
		$user = JFactory::getUser();
		
		if (!(JEVHelper::isAdminUser($user))){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=cpanel.cpanel", JText::_("COM_JEVLOCATIONS_UNAUTHORIZED_USER") );
			return;
		}
		
		$cid = JRequest::getVar(	'cid',	array(0) );
		JArrayHelper::toInteger($cid);

		$db	= JFactory::getDBO();
		$order	= JRequest::getVar(		'order', 		array(0) );
		if (count($order)!=count($cid)){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=cpanel.cpanel", "Category order problems" );
			return;
		}
		for ($k=0;$k<count($cid);$k++){
			$cat = new $this->categoryClassname($db,$this->categoryTable);
			$cat->load($cid[$k]);
			$cat->ordering = $order[$k];
			$cat->store();
		}
		$section = JRequest::getString("section","com_jevlocations");
		$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=categories.list&section=$section", JText::_('COM_JEVLOCATIONS_ADMIN_CATSUPDATED'));
		return;
	}

	/*
	function saveorder($cid = array(), $order)
	{
	$row = $this->getTable();
	$groupings = array();

	// update ordering values
	for( $i=0; $i < count($cid); $i++ )
	{
	$row->load( (int) $cid[$i] );
	// track categories
	$groupings[] = $row->catid;

	if ($row->ordering != $order[$i])
	{
	$row->ordering = $order[$i];
	if (!$row->store()) {
	$this->setError($this->_db->getErrorMsg());
	return false;
	}
	}
	}

	// execute updateOrder for each parent group
	$groupings = array_unique( $groupings );
	foreach ($groupings AS $group){
	$row->reorder('catid = '.(int) $group);
	}

	return true;
	}

	*/

	/**
	 * Category Deletion code
	 *
	 * Author: Geraint Edwards
	 * 
	 */	
	function delete(){
		$user = JFactory::getUser();
		
		if (!(JEVHelper::isAdminUser($user))){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=cpanel.cpanel", JText::_("COM_JEVLOCATIONS_UNAUTHORIZED_USER") );
			return;
		}
		
		$cid = JRequest::getVar(	'cid',	array(0) );
		JArrayHelper::toInteger($cid);
		$catids = implode(",",$cid);

		// REMEMBER TO CLEAN OUT THE MAPPING TOO!!
		$db	= JFactory::getDBO();

		if (strlen($catids)==""){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=cpanel.cpanel", "Bad categories" );
			return;
		}

		$query = "DELETE FROM $this->categoryTable WHERE id in ($catids)";
		$db->setQuery( $query );
		$db->query();

		$section = JRequest::getString("section","com_jevlocations");
		$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=categories.list&section=$section", "Category(s) deleted" );
		return;
	}


	function publish(){
		$cid = JRequest::getVar(	'cid',	array(0) );
		JArrayHelper::toInteger($cid);
		$this->toggleCatPublish($cid,1);
	}

	function unpublish(){
		$cid = JRequest::getVar(	'cid',	array(0) );
		JArrayHelper::toInteger($cid);
		$this->toggleCatPublish($cid,0);
	}

	function toggleCatPublish($cid,$newstate){
		$user = JFactory::getUser();
		
		if (!(JEVHelper::isAdminUser($user))){
			$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=cpanel.cpanel", JText::_("COM_JEVLOCATIONS_UNAUTHORIZED_USER") );
			return;
		}

		foreach ($cid AS $kid) {
			if ($kid>0){
				//$cat = JTable::getInstance("category");
				$db = JFactory::getDBO();
				$cat = new $this->categoryClassname($db,$this->categoryTable);
				$cat->load($kid);
				$cat->published = $newstate;
				$cat->store();
			}
		}
		$section = JRequest::getString("section","com_jevlocations");
		$this->setRedirect( "index.php?option=".JEVEX_COM_COMPONENT."&task=categories.list&section=$section", JText::plural('COM_JEVLOCATIONS_ADMIN_CATSUPDATED',count($cid)));

	}

}
