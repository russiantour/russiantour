<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 */

defined( 'JPATH_BASE' ) or die( 'Direct Access to this location is not allowed.' );

jimport('joomla.application.component.controller');
jimport('joomla.filesystem.folder');
class AdminCpanelController extends JControllerLegacy {
	/**
	 * Controler for the Control Panel
	 * @param array		configuration
	 */
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->registerTask( 'show',  'cpanel' );
		$this->registerDefaultTask("cpanel");

		
		// setup filesystem for images
		$this->_checkFilesystem();
		
	}

	function cpanel( )
	{
		// get the view
		$this->view = $this->getView("cpanel","html");

		if (file_exists(JPATH_ADMINISTRATOR."/components/com_jevlocations/install.php")){
			include_once(JPATH_ADMINISTRATOR."/components/com_jevlocations/install.php");
			$installer = new com_jevlocationsInstallerScript();
			$installer->update(false);
		}

		// Detect old Falang and JoomFish translations
		if (file_exists(JPATH_ADMINISTRATOR."/components/com_falang/contentelements/jevlocations_categories.xml")){
			$falangMessage = JText::_("COM_JEVLOCATIONS_FALANG_WARNING");
			JFactory::getApplication()->enqueueMessage($falangMessage, 'warning');
		}


		// Set the layout
		$this->view->setLayout('cpanel');
		$this->view->assign('title'   , JText::_("COM_JEVLOCATIONS_ADMIN_CPANEL"));

		$this->view->cpanel();
	}
	
	function _checkFilesystem(){
		// folder relative to media folder
		$folder = "jevents/jevlocations";
		// ensure folder exists
		if (!JFolder::exists(JPATH_ROOT.'/images/stories/'.$folder)) {
			JFolder::create(JPATH_ROOT. '/images/stories/'.$folder);
		}
	}	
	
}
