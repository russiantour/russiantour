<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

jimport('joomla.application.component.view');
jimport('joomla.html.parameter');

/**
 * HTML View class for the component
 *
 * @static
 */
class AdminLocationsViewLocations extends JViewLegacy
{
	public $location;
	public $locations;
	public $locparams;
	public $mediaData;
	public $maxOwnLocationsReached = false;

	function overview($tpl = null)
	{
		JHtml::_('bootstrap.framework');

		$user      = JFactory::getUser();

		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

        $jinput = $mainframe->input;
        $tmpl   = $jinput->getString('tmpl', 0);

		if (version_compare(JVERSION, '3.0', ">="))
		{
			JHtml::stylesheet('com_jevlocations/jevlocations3x.css', array(), true);
		}
		else
		{
			JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/jevlocations.css');
		}

		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');

		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		// Set toolbar items for the page
		JToolBarHelper::title(JText::_('Locations_Manager'), 'jevlocations');
		JToolBarHelper::addNew("locations.edit");
		JToolBarHelper::editList("locations.edit");
		JToolBarHelper::publishList("locations.publish");
		JToolBarHelper::unpublishList("locations.unpublish");
		JToolBarHelper::deleteList("Are you sure you want to delete these locations?", "locations.delete");
        JToolbarHelper::checkin('locations.checkin');

		if ($tmpl === "component")
		{
			JToolBarHelper::custom("locations.select", "back", "back", "JEV_BACK_TO_SELECT", false);
		}
		//JToolBarHelper::help( 'screen.locations' );
		if ($mainframe->isAdmin() && $tmpl !== "component")
		{
			JToolBarHelper::cancel('cpanel.cpanel', 'Control Panel');
		}

		$this->showToolBar();
		JevlocationsHelper::addSubmenu();

		$document = JFactory::getDocument();

		if ($mainframe->isClient('site') && $tmpl !== "component")
		{
			$menu = JFactory::getApplication('site')->getMenu()->getActive();
			$params = $menu->params;
			if(!is_null($params))
			{
				$pageTitle = $params->get('page_title', JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') . ' :: ' . JText::_('LOCATIONS'));
			} else {
				$pageTitle = JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') . ' :: ' . JText::_('LOCATIONS');
			}
		}
		else
		{
			$pageTitle = JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') . ' :: ' . JText::_('LOCATIONS');
		}

		$document = JFactory::getDocument();
		$document->setTitle($pageTitle);

		$db = JFactory::getDBO();
		$uri = JFactory::getURI();

		$filter_state = $mainframe->getUserStateFromRequest($option . 'loc_filter_state', 'filter_state', '', 'word');
		$filter_creator = $mainframe->getUserStateFromRequest($option . 'loc_filter_created_by', 'filter_created_by', '', 'email');
		$filter_catid = $mainframe->getUserStateFromRequest($option . 'loc_filter_catid', 'filter_catid', 0, 'int');
		$filter_loccat = $mainframe->getUserStateFromRequest($option . 'loc_filter_loccat', 'filter_loccat', 0, 'int');
		$filter_priority = $mainframe->getUserStateFromRequest($option . 'loc_filter_priority', 'filter_priority', 0, 'int');

		$filter_order = $mainframe->getUserStateFromRequest($option . 'loc_filter_order', 'filter_order', 'loc.ordering', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'loc_filter_order_Dir', 'filter_order_Dir', '', 'word');
		$search = $mainframe->getUserStateFromRequest($option . 'loc_search', 'search', '', 'string');
		$search = JString::strtolower($search);

		// Get data from the model
		$params = JComponentHelper::getParams('com_jevents');
		$authorisedonly = $params->get("authorisedonly", 0);
		$juser = JFactory::getUser();

		if ($authorisedonly)
		{
			JRequest::setVar("showglobal", $this->jevuser && $this->jevuser->cancreateglobal);
		}
		else
		{
			$compparams = JComponentHelper::getParams('com_jevlocations');
			$loc_global = $compparams->get("loc_global", 24);

			if (version_compare(JVERSION, "1.6.0", 'ge'))
			{
				if ($juser->authorise('core.createglobal', 'com_jevlocations') && $juser->authorise('core.edit', 'com_jevlocations'))
				{
					JRequest::setVar("showglobal", 1);
				}
				else
				{
					JRequest::setVar("showglobal", 0);
				}
			}
			else
			{
				if ($juser->gid >= intval($loc_global))
				{
					JRequest::setVar("showglobal", 1);
				}
				else
				{
					JRequest::setVar("showglobal", 0);
				}
			}
		}
		if (JEVHelper::isAdminUser($juser))
		{
			JRequest::setVar("showall", 1);
		}
		$model =  $this->getModel();
		$total =  $this->get('Total');
		$items =  $this->get('Data');
		$pagination =  $this->get('Pagination');

		// New custom fields
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$template = $compparams->get("fieldtemplate", "");
		if ($template != "" && $compparams->get("custinlist"))
		{
			$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
			if (file_exists($xmlfile))
			{
				$db = JFactory::getDBO();
				foreach ($items as &$item)
				{
					$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=" . intval($item->loc_id) . " AND targettype='com_jevlocations'");
					$cfdata  = $db->loadObjectList('name');
					$customdata = array();
					foreach ($cfdata as $dataelem)
					{
						if (strpos($dataelem->name, ".") !== false)
						{
							$dataelem->name = str_replace(".", "_", $dataelem->name);
						}
						$customdata[$dataelem->name] = $dataelem->value;
					}

					$jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
					$jcfparams->bind($customdata);
					$jcfparams->setEvent($item);

					$customfields = array();
					$groups = $jcfparams->getFieldsets();
					foreach ($groups as $group => $element)
					{
						if ($jcfparams->getFieldCountByFieldSet($group))
						{
							$customfields = array_merge($customfields, $jcfparams->renderToBasicArray('params', $group));
						}
					}
					$item->customfields = $customfields;
					unset($item);
				}
			}
		}


		// build list of categories
		$javascript = 'onchange="document.adminForm.submit();"';
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);
		JLoader::register('JevLocationsHTML', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/jevlocationshtml.php");
		if ($usecats)
		{
			$lists['catid'] = JevLocationsHTML::buildCategorySelect(intval($filter_catid), $javascript, "", false, false, 0, 'filter_catid', 'com_jevlocations');
			$lists['catid'] = str_replace(JText::_('COM_JEVLOCATIONS_ALL_CATEGORIES'), JText::_("COM_JEVLOCATIONS_ALL_CITIES"), $lists['catid']);
		}
		// normal category filter
		$lists['loccat'] = JevLocationsHTML::buildCategorySelect2(intval($filter_loccat), $javascript, "", false, false, 0, 'filter_loccat', 'com_jevlocations',false,"ordering",false);
		// state filter
		$lists['state'] = JHtml::_('grid.state', $filter_state);
		// creator filter
		$lists['creator'] = JevLocationsHTML::buildCreatorSelect($filter_creator);

		// table ordering
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		$priorities = "";
		// only those who can publish globally can set priority field
		if (JEVHelper::isEventPublisher(true))
		{
			$list = array();
			for ($i = 0; $i < 10; $i++)
			{
				$list[] = JHtml::_('select.option', $i, $i, 'val', 'text');
			}
			$priorities = JHtml::_('select.genericlist', $list, 'filter_loccat', "", 'val', 'text', intval($filter_priority));
			$this->assign('setPriority', true);
			$lists["priority"] = $priorities;
		}
		else
		{
			$this->assign('setPriority', false);
			$lists["priority"] = $priorities;
		}

		// search filter
		$lists['search'] = $search;

		$user = JFactory::getUser();
		$this->assignRef('user', $user);
		$this->assignRef('usecats', $usecats);
		$this->assignRef('lists', $lists);
		$this->assignRef('items', $items);
		$this->assignRef('pagination', $pagination);
		$this->assignRef('mainframe', $mainframe);

		parent::display($tpl);

	}

	function select($tpl = null)
	{
		JLoader::register('JevModal',JPATH_LIBRARIES."/jevents/jevmodal/jevmodal.php");

		// Include jQuery framework
		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core'));
		JHtml::_('bootstrap.framework');
		//JHtml::_('bootstrap.loadCss');

		JHtml::script('com_jevlocations/locations.js', false, true);

		JLoader::register('JEventsHTML', JPATH_SITE . "/components/com_jevents/libraries/jeventshtml.php");

		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		if (version_compare(JVERSION, '3.0', ">=")){
			JHtml::stylesheet('com_jevlocations/jevlocations3x.css',array(),true);
		} else {
			JHTML::stylesheet( 'administrator/components/com_jevlocations/assets/css/jevlocations.css');
		}
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		// Set toolbar items for the page
		JToolBarHelper::title(JText::_('COM_JEVLOCATIONS_SELECT_LOCATION'), 'jevlocations');

		// Only offer management buttons if use is authorised
		if (JevLocationsHelper::canCreateOwn() || JevLocationsHelper::canCreateGlobal())
		{
			//JToolBarHelper::addNew("locations.edit","Create Location");
			$this->toolbarButton("locations.edit", "new", "new", "Create_Location", false);
		}
		if (JevLocationsHelper::canCreateOwn())
		{
			$this->toolbarButton("locations.overview", "config", "config", "Manage_Locations", false);
		}

		$this->showToolBar();

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') . ' :: ' . JText::_('LOCATIONS'));

		$db = JFactory::getDBO();
		$uri =  JFactory::getURI();

		$filter_loctype = $mainframe->getUserStateFromRequest($option . 'loc_filter_loctype', 'filter_loctype', 0, 'int');
		$filter_catid = $mainframe->getUserStateFromRequest($option . 'loc_filter_catid', 'filter_catid', 0, 'int');
		$filter_loccat = $mainframe->getUserStateFromRequest($option . 'loc_filter_loccat', 'filter_loccat', 0, 'int');
		$filter_priority = $mainframe->getUserStateFromRequest($option . 'loc_filter_priority', 'filter_priority', 0, 'int');

		$filter_order = $mainframe->getUserStateFromRequest($option . 'loc_filter_order', 'filter_order', 'loc.ordering', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'loc_filter_order_Dir', 'filter_order_Dir', '', 'word');
		$search = $mainframe->getUserStateFromRequest($option . 'loc_search', 'search', '', 'string');
		$search = JString::strtolower($search);

		// Should we be allowed to select from all locations?
		$compparams = JComponentHelper::getParams("com_jevlocations");
		JRequest::setVar("showall", $compparams->get("selectfromall", 0));
		// JEvents admin users can see them all
		$juser =  JFactory::getUser();
		if (JEVHelper::isAdminUser($juser))
		{
			JRequest::setVar("showall", 1);
		}

		// Get data from the model
		$model =  $this->getModel();
		$model->setState("select", true);
		$model->setState("loctype", $filter_loctype);

		$total =  $this->get('Total');
		$items =  $this->get('Data');
		$pagination =  $this->get('Pagination');

		// build list of categories
		$usecats = $compparams->get("usecats", 0);
		$javascript = 'onchange="document.adminForm.submit();"';
		JLoader::register('JevLocationsHTML', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/jevlocationshtml.php");
		if ($usecats)
		{
			$lists['catid'] = JevLocationsHTML::buildCategorySelect(intval($filter_catid), $javascript, "", false, false, 0, 'filter_catid', 'com_jevlocations');
			$lists['catid'] = str_replace(JText::_('COM_JEVLOCATIONS_ALL_CATEGORIES'), JText::_("COM_JEVLOCATIONS_ALL_CITIES"), $lists['catid']);
		}
		// normal category filter
		$lists['loccat'] = JevLocationsHTML::buildCategorySelect2(intval($filter_loccat), $javascript, "", false, false, 0, 'filter_loccat', 'com_jevlocations',false,"ordering",false);

		$options = array();
		$options[] = JHtml::_('select.option', 0, JText::_('COM_JEVLOCATIONS_ANY_LOCATION'));
		$options[] = JHtml::_('select.option', 1, JText::_('COM_JEVLOCATIONS_MY_LOCATIONS'));
		$options[] = JHtml::_('select.option', 2, JText::_('COM_JEVLOCATIONS_COMMON_LOCATIONS'));
		$lists["loctype"] = JHtml::_('select.genericlist', $options, 'filter_loctype', 'class="inputbox" size="1" onchange="form.submit();"', 'value', 'text', $filter_loctype);

		// only those who can publish globally can set priority field
		if (JEVHelper::isEventPublisher(true))
		{
			$list = array();
			for ($i = 0; $i < 10; $i++)
			{
				$list[] = JHtml::_('select.option', $i, $i, 'val', 'text');
			}
			$priorities = JHtml::_('select.genericlist', $list, 'filter_loccat', "", 'val', 'text', intval($filter_priority));
			$this->assign('setPriority', true);
			$lists["priority"] = $priorities;
		}
		else
		{
			$this->assign('setPriority', false);
			$lists["priority"] = "";
		}

		// table ordering
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		// search filter
		$lists['search'] = $search;

		$user = JFactory::getUser();
		$this->assignRef('user', $user);
		$this->assignRef('usecats', $usecats);
		$this->assignRef('lists', $lists);
		$this->assignRef('items', $items);
		$this->assignRef('pagination', $pagination);

		parent::display($tpl);

	}

	function edit($tpl = null)
	{
		// Include jQuery framework
		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core'));
		JHtml::_('bootstrap.framework');

		JLoader::register('JEventsHTML', JPATH_SITE . "/components/com_jevents/libraries/jeventshtml.php");
		JLoader::register('jevlocationsCategory', JPATH_COMPONENT_ADMINISTRATOR . "/libraries/categoryClass.php");

		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();
		$compparams = JComponentHelper::getParams("com_jevlocations");
		$allowed = true;

		JHtml::stylesheet('com_jevlocations/jevlocations3x.css', array(), true);
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');

		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		// Get the location
		$location = $this->get('data');
		$isNew = ($location->loc_id < 1);

		// Set toolbar items for the page
		$text = $isNew ? JText::_('COM_JEVLOCATIONS_NEW') : JText::_('COM_JEVLOCATIONS_EDIT');
		JToolBarHelper::title(JText::_('COM_JEVLOCATIONS_LOCATION') . ': <small><small>[ ' . $text . ' ]</small></small>', 'jevlocations');

		JToolbarHelper::apply('locations.apply');
		JToolbarHelper::save('locations.save');

		if ($isNew)
		{
			JToolbarHelper::cancel('locations.cancel');

			$ownLocationsLimit = $compparams->get('max_art');

			if ($ownLocationsLimit > 0)
			{
				if (JevLocationsHelper::userReachedMaxOwnLocations())
				{
					$this->maxOwnLocationsReached = true;

					if (JevLocationsHelper::canCreateGlobal())
					{
						$msg = JText::_('COM_JEVLOCATIONS_NEW_LOCATION_MAX_OWN_LOCATIONS_ONLY_GLOBAL_WARNING');
						JFactory::getApplication()->enqueueMessage($msg, 'warning');
					}
					else
					{
						$msg = JText::_('COM_JEVLOCATIONS_NEW_LOCATION_MAX_OWN_LOCATIONS_CANT_CREATE_WARNING');
						JFactory::getApplication()->enqueueMessage($msg, 'error');
						$allowed = false;
					}
				}
			}
		}
		else
		{
			// To Do for Next release: Save as New/Copy button
			JToolbarHelper::save2copy('locations.savecopy', "JTOOLBAR_SAVE_AS_COPY");

			// For existing items the button is renamed `close`
			JToolbarHelper::cancel('locations.cancel', "JTOOLBAR_CLOSE");
		}

		if ($mainframe->isAdmin())
		{
			JToolBarHelper::cancel('cpanel.cpanel', 'Control Panel');
		}

		if ($allowed)
		{
			$this->showToolBar();

			$document = JFactory::getDocument();

			JevLocationsHelper::loadApiScript();
			$googlemapsurl = JevLocationsHelper::getMapsUrl();

			$document->setTitle(JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') . ' :: ' . JText::_('EDIT_LOCATION'));

			$option = JEVEX_COM_COMPONENT;

			$db = JFactory::getDBO();
			$uri = JFactory::getURI();
			$user = JFactory::getUser();
			$model = $this->getModel();

			$lists = array();

			if ($isNew || ($location->geolon == 0 && $location->geolat == 0))
			{
				$long = $compparams->get("long", 30);
				$lat = $compparams->get("lat", 30);
				$zoom = $compparams->get("zoom", 10);
			}
			else
			{
				$long = $location->geolon;
				$lat = $location->geolat;
				$zoom = $location->geozoom;
			}

			$script = <<<SCRIPT
var globallong = $long;
var globallat = $lat;
var globalzoom = $zoom;
SCRIPT;
			$document->addScriptDeclaration($script);

			JHtml::script('com_jevlocations/locations.js', false, true);

			// Fail if checked out not by 'me'
			if ($model->isCheckedOut($user->get('id')))
			{
				$msg = JText::sprintf('DESCBEINGEDITTED', JText::_('THE_LOCATION'), $location->title);
				$mainframe->redirect('index.php?option=' . $option, $msg);
			}

			// Edit or Create?
			if (!$isNew)
			{
				$model->checkout($user->get('id'));
			}
			else
			{
				// Initialise new record
				$location->published = 1;
				$location->approved = 1;
				$location->order = 0;
				$location->overlaps = 0;
				$location->mapicon = "blue-icon.png";
				$location->catid = JRequest::getVar('catid', 0, 'post', 'int');
				$location->loccat = JRequest::getVar('loccat', 0, 'post', 'int');

				if (JevLocationsHelper::canCreateGlobal() && ( $compparams->get("commondefault", 0) || $this->maxOwnLocationsReached ))
				{
					$location->global = 1;
				}

				$location->groupusage = 0;
			}

			// Build the html select list for ordering
			$query = 'SELECT ordering AS value, title AS text'
					. ' FROM #__jev_locations'
					. ' ORDER BY ordering';

			$lists['ordering'] = JHtml::_('list.ordering', 'ordering', $query, "", $location->ordering, $location->loc_id ? 0 : 1);

			if ($compparams->get("groupusage", 0))
			{
				$groups = JHtml::_('user.groups', true);
				$lists['groupusage'] = JHtml::_('select.genericlist', $groups, "groupusage", "", "value", "text", $location->groupusage, "groupusage");
			}

			// List of media files
			jimport("joomla.filesystem.folder");

			$filelist = JFolder::files(JPATH_SITE . "/media/com_jevlocations/images/", "\.png");
			$files = array();

			foreach ($filelist as $file)
			{
				$files[] = array("val" => $file, "text" => $file);
			}

			$lists["mapicon"] = JHtml::_('select.genericlist', $files, 'mapicon', "", 'val', 'text', $location->mapicon);

			$usecats = $compparams->get("usecats", 0);

			JLoader::register('JevLocationsHTML', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/jevlocationshtml.php");

			if ($usecats)
			{
				$javascript = "onblur='addressSelected();' ";
				$lists['catid'] = JevLocationsHTML::buildCategorySelect($location->catid, $javascript, "", false, true, 0, 'catid', 'com_jevlocations');
				$lists['catid'] = str_replace(JText::_('JEV_EVENT_CHOOSE_CATEG'), JText::_('CHOOSE_CITY'), $lists['catid']);
			}

			// Normal category filter
			$lists['loccat'] = JevLocationsHTML::buildCategorySelect2($location->catid_list, "", "", false, true, 0, 'loccat[]', 'com_jevlocations',false,"ordering",true);

			// Build the html select list
			$lists['published'] = '<fieldset class="radio btn-grp btn-group">' . JHtml::_('select.booleanlist', 'published', 'class="radio btn"', $location->published) . '</fieldset>';

			include_once JPATH_COMPONENT_ADMINISTRATOR . "/fields/jevmenu.php";
			$node = null;
			$lists['targetmenu'] = str_replace("targetmenu[targetmenu]", "targetmenu", JFormFieldJEVmenu::getInputStatic('targetmenu', $location->targetmenu, $node, 'targetmenu'));

			// Build the html select list
			$globalProperties = array();
			$globalProperties['class'] = "radio btn";

			if ($this->maxOwnLocationsReached)
			{
				$globalProperties['disabled'] = "disabled";
			}

			$lists['global'] = '<fieldset class="radio btn-grp btn-group">'
				. JHtml::_('select.booleanlist', 'global', $globalProperties, $location->global)
				. '</fieldset>';

			$lists['overlaps'] = '<fieldset class="radio btn-grp btn-group">' . JHtml::_('select.booleanlist', 'overlaps', 'class="radio btn"', $location->overlaps) . '</fieldset>';

			// Only those who can publish globally can set priority field
			if (JEVHelper::isEventPublisher(true))
			{
				$list = array();

				for ($i = 0; $i < 10; $i++)
				{
					$list[] = JHtml::_('select.option', $i, $i, 'val', 'text');
				}

				$priorities = JHtml::_('select.genericlist', $list, 'priority', "", 'val', 'text', $location->priority);
				$this->assign('setPriority', true);
				$this->assign('priority', $priorities);
			}
			else
			{
				$this->assign('setPriority', false);
			}

			$dispatcher = JEventDispatcher::getInstance();
			$recaptcha = array();
			$dispatcher->trigger('onEditLocationCustom', array(&$location, &$recaptcha));

			$this->assign("recaptcha", $recaptcha);

			$this->setCreatorLookup($location);

			// Clean location data
			JFilterOutput::objectHTMLSafe($location, ENT_QUOTES, 'description');

			$file = JPATH_COMPONENT . '/models/location.xml';
			$params = new JRegistry($location->params, $file);

			$this->assignRef('usecats', $usecats);
			$this->assignRef('lists', $lists);
			$this->assignRef('location', $location);
			$this->assignRef('params', $params);
		}

		$this->assignRef('allowed', $allowed);
		parent::display($tpl);

	}

	function detail($tpl = null)
	{
		JHtml::_('jquery.framework');
		JLoader::register('JEventsHTML', JPATH_SITE . "/components/com_jevents/libraries/jeventshtml.php");
		JLoader::register('jevlocationsCategory', JPATH_COMPONENT_ADMINISTRATOR . "/libraries/categoryClass.php");

		$option = JEVEX_COM_COMPONENT;
		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
		// Set toolbar items for the page

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$disableautopan = $compparams->get("autopan", 1)  ? "false":"true";

		//Get Images Sizes
		$mediaSize = new stdClass();
		$mediaSize->thumbnail = array('width' => $compparams->get('thumbw',150), 'height' => $compparams->get('thumbh',150), 'nothumbnail' => $compparams->get('nothumbnail',150));
		$mediaSize->image = array('width' => $compparams->get('imagew',150), 'height' => $compparams->get('thumbh',150));

		$googleurl = JevLocationsHelper::getApiUrl();
		$googlemapsurl = JevLocationsHelper::getMapsUrl();
		JevLocationsHelper::loadApiScript();

		//get the location
		$db = JFactory::getDBO();
		$uri =  JFactory::getURI();
		$user =  JFactory::getUser();
		$model =  $this->getModel();

		$location =  $this->get('data');

		if (!$location->published)
		{
			$mainframe->redirect('index.php', JText::_("COM_JEVLOCATIONS_NOT_AUTHORISED"));
		}

		//Get Category Data
		$location_categories = $this->getCategoryData();

		$location->categoryData = $location_categories;

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('Event_Location') . " :: " . $location->title);

		$subtitle = addslashes(str_replace(" ", "+", urlencode($location->title)));

		$lists = array();

		$long = $location->geolon;
		$lat = $location->geolat;
		$zoom = $location->geozoom;

		$plugin = JPluginHelper::getPlugin("jevents", "jevlocations");
		if ($plugin) {
			$pluginparams = new JRegistry($plugin->params);
		}
		else {
			$pluginparams = new JRegistry(null);
		}

		if ($compparams->get("gwidth",-1) !=-1){
			$pluginparams = $compparams;
		}

		if ($googleurl != "")
		{

			$script = "var urlroot = '" . JURI::root() . "/media/com_jevlocations/images/';\n";
			$maptype = $pluginparams->get("maptype", "ROADMAP");
			$disableautopan = $compparams->get("autopan", 1) ? "false" : "true";

			$directionsUrl = $googlemapsurl . JevLocationsHelper::getDirectionsSlug($lat, $long, $zoom, $subtitle);

			$script .=<<<SCRIPT
var globallong = $long;
var globallat = $lat;
var globalzoom = $zoom;
var globaltitle = "$subtitle";
var googleurl = "$googleurl";
var googlemapsurl = "$directionsUrl";
var maptype = "$maptype";
var disableautopan = $disableautopan;
var mapicon = "$location->mapicon";

SCRIPT;
			$document->addScriptDeclaration($script);
		}

		if ($compparams->get("redirecttodirections"))
		{
			$mainframe->redirect($directionsUrl);
			return;
		}

		JHtml::script('com_jevlocations/locationdetail.js', false, true);


		// New custom fields
		$template = $compparams->get("fieldtemplate", "");
		if ($template != "" )
		{
			$xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
			if (file_exists($xmlfile))
			{
				$db = JFactory::getDBO();
				$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=" . intval($location->loc_id) . " AND targettype='com_jevlocations'");
				$cfdata  = $db->loadObjectList('name');
				$customdata = array();
				
				foreach ($cfdata as $dataelem)
				{
					if (strpos($dataelem->name, ".") !== false)
					{
						$dataelem->name = str_replace(".", "_", $dataelem->name);
					}
					$customdata[$dataelem->name] = $dataelem->value;
				}

				if (count($customdata))
				{
					$jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
					$jcfparams->bind($customdata);
					$jcfparams->setEvent($location);
					$customfields = array();
					$groups = $jcfparams->getFieldsets();

					foreach ($groups as $group => $element)
					{
						if ($jcfparams->getFieldCountByFieldSet($group))
						{
							$customfields = array_merge($customfields, $jcfparams->renderToBasicArray('params', $group));
						}
					}

					$location->customfields = $customfields;
				}
			}
		}

		if ($compparams->get('usemetatags', false))
		{
			if ($location->metatitle)
			{
				$document->setMetaData('title', $location->metatitle);
			}

			if ($location->metadescription)
			{
				$document->setDescription($location->metadescription);
			}
		}

		$this->location = $location;
		$this->locparams = $compparams;
		$this->mediaData = $mediaSize;

		parent::display($tpl);

	}

	function upload($tpl = null)
	{
		parent::display($tpl);

	}

	function toolbarButton($task = '', $icon = '', $iconOver = '', $alt = '', $listSelect = true)
	{
		include_once(JPATH_ADMINISTRATOR . "/components/com_jevents/libraries/jevbuttons.php");
		$bar =  JToolBar::getInstance('toolbar');

		// Add a standard button
		$bar->appendButton('Jev', $icon, $alt, $task, $listSelect);

	}

	function toolbarConfirmButton($task = '', $msg='', $icon = '', $iconOver = '', $alt = '', $listSelect = true)
	{
		include_once(JPATH_ADMINISTRATOR . "/components/com_jevents/libraries/jevbuttons.php");
		$bar =  JToolBar::getInstance('toolbar');

		// Add a standard button
		$bar->appendButton('Jevconfirm', $msg, $icon, $alt, $task, $listSelect);

	}

	function _globalHTML(&$row, $i)
	{
		$img = $row->global ? 'Tick.png' : 'Cross.png';
		$alt = $row->global ? JText::_('GLOBAL') : JText::_('GLOBAL');

		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		$img ='<img src="' .  JURI::Root() . 'components/com_jevlocations/assets/images/'.$img.'" alt="' . $alt . '" style="border:none;" />';

		if (JevLocationsHelper::canCreateGlobal())
		{
			$action = $row->global ? JText::_('Make_Private') : JText::_('Make_Global');
			$task = $row->global ? "locations.privatise" : "locations.globalise";

			$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb' . $i . '\',\'' . $task . '\')" title="' . $action . '">
		' . $img;

			return $href;
		}
		return $img;

	}

	function _publishedHTML(&$row, $i)
	{
		$img = $row->published ? 'Tick.png' : 'Cross.png';
		$alt = $row->published ? JText::_('JLIB_HTML_UNPUBLISH_ITEM') : JText::_('JLIB_HTML_PUBLISH_ITEM');
		$action = $row->published ? JText::_('JLIB_HTML_UNPUBLISH_ITEM') : JText::_('JLIB_HTML_PUBLISH_ITEM');

		$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible

		$img ='<img src="' .  JURI::Root() . 'components/com_jevlocations/assets/images/'.$img.'" alt="' . $alt . '" style="border:none;" />';

		$task = $row->published ? "locations.unpublish" : "locations.publish";

		$href = '
		<a href="javascript:void(0);" onclick="return listItemTask(\'cb' . $i . '\',\'' . $task . '\')" title="' . $action . '">
		' . $img;

		return $href;

	}

	function showToolBar()
	{
		$mainframe = JFactory::getApplication();

		if (JRequest::getVar("tmpl", "") == "component" || !$mainframe->isAdmin())
		{
			?>
			<div class='jevlocations'>
				<div id="toolbar-box" >
					<div class="t">
						<div class="t">
							<div class="t"></div>
						</div>
					</div>
					<div class="m">
						<?php
						$bar =  JToolBar::getInstance('toolbar');
						$barhtml = $bar->render();
						$barhtml = preg_replace('/onclick="(.*)" /', 'onclick="$1;return false;" ', $barhtml);
						//$barhtml = str_replace('href="#"','href="javascript void();"',$barhtml);
						//$barhtml = str_replace('submitbutton','return submitbutton',$barhtml);
						echo $barhtml;

						$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
						$title = $mainframe->JComponentTitle;
						if (!version_compare(JVERSION, "1.6.0", 'ge') || !$mainframe->isAdmin())
						{
							echo $title;
						}
						?>
						<div class="clr"></div>
					</div>
					<div class="b">
						<div class="b">
							<div class="b"></div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		// Kepri doesn't load icons etc. when using tmpl=component - but we want them!
		if (JRequest::getVar("tmpl", "") == "component" && $mainframe->isAdmin())
		{
			JHtml::stylesheet('administrator/templates/' . $mainframe->getTemplate() . '/css/template.css');
			// Lets check if we have editted before! if not... rename the custom file.
			if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
			{
				// It is definitely now created, lets load it!
				JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
			}
		}
	}

	protected function setCreatorLookup($row)
	{
		// If user has backend access then allow them to specify the creator
		$user = JFactory::getUser();

		if ($user->authorise('core.createforothers', 'com_jevlocations'))
		{
			$params =  JComponentHelper::getParams(JEVEX_COM_COMPONENT);
			$jevparams =  JComponentHelper::getParams(JEV_COM_COMPONENT);
			$authusers = $jevparams->get("authorisedonly", 0);
			if ($authusers)
			{
				$sql = "SELECT u.* FROM #__users AS u LEFT JOIN #__jev_users AS ju ON ju.user_id = u.id WHERE ju.cancreateown = 1 OR ju.cancreateglobal = 1 ORDER BY u.name ASC";
				$db = JFactory::getDBO();
				$db->setQuery($sql);
				$users = $db->loadObjectList();
			}
			else
			{
				$db = JFactory::getDBO();

				if (version_compare(JVERSION, "1.6.0", 'ge'))
				{
					$rules = JAccess::getAssetRules("com_jevlocations", true);
					$creatorgroups = $rules->getData();
					// need to merge the arrays because of stupid way Joomla checks super user permissions
					//$creatorgroups = array_merge($creatorgroups["core.admin"]->getData(), $creatorgroups["core.create"]->getData());
					// use union orf arrays sincee getData no longer has string keys in the resultant array
					//$creatorgroups = $creatorgroups["core.admin"]->getData()+ $creatorgroups["core.create"]->getData();
					$creatorgroupsdata = $creatorgroups["core.admin"]->getData();

					// Take the higher permission setting
					foreach ($creatorgroups["core.create"]->getData() as $creatorgroup => $permission)
					{
						if ($permission)
						{
							$creatorgroupsdata[$creatorgroup] = $permission;
						}
					}

					$users = array(0);

					foreach ($creatorgroupsdata as $creatorgroup => $permission)
					{
						if ($permission == 1)
						{
							$users = array_merge(JAccess::getUsersByGroup($creatorgroup, true), $users);
						}
					}

					$sql = "SELECT * FROM #__users where id IN (" . implode(",", array_values($users)) . ") ORDER BY name asc";
					$db->setQuery($sql);
					$users = $db->loadObjectList();
				}
				else
				{
					$minaccess = $params->getValue("loc_own", 20);
					$sql = "SELECT * FROM #__users WHERE gid >= " . $minaccess . " ORDER BY name ASC";

					$db->setQuery($sql);
					$users = $db->loadObjectList();
				}
			}

			$userOptions[] = JHtml::_('select.option', '-1', 'Select User');

			foreach ($users AS $usr)
			{
				$userOptions[] = JHtml::_('select.option', $usr->id, $usr->name . " ( " . $usr->username . " )");
			}

			if ($row->created_by > 0)
			{
				$creator = $row->created_by;
			}
			elseif ($row->anonname != "" && $row->anonemail != "")
			{
				$creator = -1;
			}
			else
			{
				$creator = $user->id;
			}

			$userlist = JHtml::_('select.genericlist', $userOptions, 'created_by', 'class="inputbox" size="1" ', 'value', 'text', $creator);

			$this->assignRef("users", $userlist);
		}

	}

	public function getCategoryData()
	{
		static $location_categories;

		if (!isset($location_categories))
		{
			$db	= JFactory::getDBO();

			$query = $db->getQuery(true);

			$query->select('c.*');
			$query->from('#__categories AS c');
			$query->where('extension = "com_jevlocations"');
			$query->order('c.lft ASC');

			$db->setQuery($query);
			$location_categories = $db->loadObjectList('id');

			foreach ($location_categories as &$cat)
			{
				$cat->name = $cat->title;
				$params = new JRegistry($cat->params);
				$cat->color = $params->get("catcolour", "");
				$cat->overlaps = $params->get("overlaps", 0);
				$cat->image = $params->get("image", "");
			}

			unset($cat);

			$dispatcher	= JEventDispatcher::getInstance();
			$dispatcher->trigger('onGetCategoryData', array (& $cats));
		}

		$dispatcher	= JEventDispatcher::getInstance();
		$dispatcher->trigger('onGetAccessibleCategories', array (& $cats));

		return $location_categories;
	}

}
