<?php defined('_JEXEC' ) or die('Restricted access'); ?>

<?php
if($this->allowed) :
$editor = JFactory::getEditor();
$compparams = JComponentHelper::getParams("com_jevlocations");

// Attach script to document
$document = JFactory::getDocument();

$imgfolder = "jevents/jevlocations";

// debug
$session = JFactory::getSession();
JHtml::_('script', 'system/core.js', false, true);
$mainframe = JFactory::getApplication();  // RSH 10/11/10 - make 1.5/1.6 compatible
//Language strings for Javascript
$messages = array();
$messages['notfound'] = JText::_( 'COM_JEVLOCATIONS_NOT_FOUND_MESSAGE', true );
$messages['introduceaddress'] = JText::_( 'COM_JEVLOCATIONS_PLEASE_INTRODUCE_AN_ADDRESS', true );

if ($mainframe->isAdmin()){
	$targetURL = JURI::root().'administrator/index.php?tmpl=component&folder='.$imgfolder.'&'.$session->getName().'='.$session->getId().'&'.JSession::getFormToken().'=1';
}
else {
	$targetURL = JURI::root().'index.php?tmpl=component&folder='.$imgfolder.'&'.$session->getName().'='.$session->getId().'&'.JSession::getFormToken().'=1';
}

$uploaderInit = "
		var oldAction = '';
		var oldTarget = '';
		var oldTask = '';
		var oldOption = '';
		function uploadFileType(field){
			form = document.adminForm;
			oldAction = form.action;
			oldTarget = form.target;
			oldTask = form.task.value;
			oldOption = form.option.value;
			form.action = '".$targetURL."&field='+field;

			form.target = 'uploadtarget';
			form.task.value = 'locations.upload';
			form.option.value = 'com_jevlocations';
			form.submit();
			form.action = oldAction ;
			form.target = oldTarget ;
			form.task.value = oldTask ;
			form.option.value = oldOption;

			var loading = document.getElementById(field+'_loading');
			loading.style.display='block';
			var loaded = document.getElementById(field+'_loaded');
			loaded.style.display='none';
		}
		function setImageFileName(){
			iframe = frames.uploadtarget;
			if(!iframe.fname) return;
			//elemname = iframe.fname.replace('_file','');
			elemname = iframe.fname.substr(0,iframe.fname.length-5);
			elem = document.getElementById(elemname);
			if (elem) elem.value = iframe.filename;
			elem = document.getElementById(elemname+'title');
			if (elem) elem.value = iframe.oname;
			elem = document.getElementById(iframe.fname);
			if (elem) elem.value = '';
			img = document.getElementById(elemname+'_img');
			img.src = '". JEVP_MEDIA_BASEURL."/$imgfolder/thumbnails/thumb_'+iframe.filename;
			img.style.display='block';
			img.style.marginRight='10px';

			var loading = document.getElementById(elemname+'_loading');
			loading.style.display='none';
			var loaded = document.getElementById(elemname+'_loaded');
			loaded.style.display='block';

		}
		function clearImageFile(elemname){
			img = document.getElementById(elemname+'_img');
			img.src = ''
			img.style.display='none';
			img.style.marginRight='0px';
			elem = document.getElementById(elemname);
			if (elem) elem.value = '';
			elem = document.getElementById(elemname+'title');
			if (elem) elem.value = '';
		}
";
$document->addScriptDeclaration($uploaderInit);


?>

<script  type="text/javascript">
function submitbutton(pressbutton) {
	var form = document.adminForm;
	if (pressbutton == 'locations.overview' || pressbutton == 'locations.cancel') {
		submitform( pressbutton );
		return;
	}

	// do field validation
	<?php echo $editor->save( 'description' ); ?>
	if (form.title.value == "")
	{
		alert( "<?php echo JText::_( 'Location_must_have_a_title', true ); ?>" );
	}
	<?php if ($this->usecats) {?>
	else if (form.catid.value == "0")
	{
		alert( "<?php echo JText::_( 'You_must_select_a_city', true ); ?>" );
	}
	<?php } ?>
	else
	{
		<?php echo $editor->save( 'description' ); ?>

		submitform( pressbutton );
	}
}
</script>
<style type="text/css">
	table.paramlist td.paramlist_key {
		width: 92px;
		text-align: left;
		height: 30px;
	}
</style>

<form action="index.php" method="post" name="adminForm" id="adminForm" enctype='multipart/form-data'>
<div class="col jevbootstrap">
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'COM_JEVLOCATIONS_DETAILS' ); ?></legend>

        <div class="jev_loc_notice"></div>

		<table class="admintable">
		<tr  class="locname">
			<td width="100" align="right" class="key">
				<label for="title">
					<?php echo JText::_( 'JEV_LOCATION_NAME' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="title" id="title" size="60" maxlength="250" value="<?php echo $this->location->title;?>" />
			</td>
		</tr>
		<?php if(isset($this->users)){?>
		<tr class="loccreator">
			<td width="100" align="right" class="key">
				<label for="created_by">
					<?php echo JText::_( 'JEV_LOCATION_CREATOR' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->users;?>
			</td>
		</tr>
		<?php
		}
		if ($mainframe->isAdmin()){
		?>
		<tr class="localias">
			<td width="100" align="right" class="key">
				<label for="alias" title="<?php echo JText::_( 'COM_JEVLOCATIONS_ALIAS_DSC' ); ?>" class="hasTooltip" >
					<i class="icon-info"></i> <?php echo JText::_( 'ALIAS' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="alias" id="alias" size="60" maxlength="250" value="<?php echo $this->location->alias;?>" />
			</td>
		</tr>
		<?php
		}
		?>
		<?php if($this->lists['loccat']):?>
		<tr class="loccategory">
			<td valign="top" align="right" class="key">
				<label for="loccat">
					<?php echo JText::_( 'JEV_LOCATION_CATEGORY' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['loccat']; ?>
			</td>
		</tr>
		<?php endif; ?>
		<?php if(isset($this->lists['groupusage'])):?>
		<tr class="locgroupusage">
			<td valign="top" align="right" class="key">
				<label for="locgroupusage">
					<?php echo JText::_( 'JEV_GROUP_USAGE' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['groupusage']; ?>
			</td>
		</tr>
		<?php endif; ?>
		<tr class="locstreet">
			<td width="100" align="right" class="key">
				<label for="street">
					<?php echo JText::_( 'COM_JEVLOCATIONS_STREET' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="street" id="street" size="60" maxlength="250" value="<?php echo $this->location->street;?>" />
			</td>
		</tr>
		<?php if ($this->usecats) {?>
		<tr class="loccity">
			<td valign="top" align="right" class="key">
				<label for="catid">
					<?php echo JText::_( 'COM_JEVLOCATIONS_CITY' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['catid']; ?>
			</td>
		</tr>
		<?php
		}
		else {
			?>
		<tr class="loccity">
			<td valign="top" align="right" class="key">
				<label for="city">
					<?php echo JText::_( 'COM_JEVLOCATIONS_CITY' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="city" id="city" size="20" maxlength="50" value="<?php echo $this->location->city;?>" />
			</td>
		</tr>
		<tr class="locstate">
			<td valign="top" align="right" class="key">
				<label for="state">
					<?php echo JText::_( 'COM_JEVLOCATIONS_STATE' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="state" id="state" size="20" maxlength="50" onblur="findAddress(false);"  value="<?php echo $this->location->state;?>" />
			</td>
		</tr>
		<tr class="loccountry">
			<td valign="top" align="right" class="key">
				<label for="catid">
					<?php echo JText::_( 'COM_JEVLOCATIONS_COUNTRY' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="country" id="country" size="20" maxlength="50" onblur="findAddress(false);"  value="<?php echo $this->location->country;?>" />
			</td>
		</tr>
			<?php
		}
		?>
		<tr class="locpostcode">
			<td width="100" align="right" class="key">
				<label for="postcode">
					<?php echo JText::_( 'POSTCODE' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="postcode" id="postcode" size="20" maxlength="50" onblur="findAddress(false);"  value="<?php echo $this->location->postcode;?>" />
			</td>
		</tr>
		<tr class="locphone">
			<td width="100" align="right" class="key">
				<label for="phone">
					<?php echo JText::_( 'PHONE' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="phone" id="phone" size="60" maxlength="250" value="<?php echo $this->location->phone;?>" />
			</td>
		</tr>
		<tr class="locurl">
			<td width="100" align="right" class="key">
				<label for="url" title="<?php echo JText::_( 'COM_JEVLOCATIONS_URL_DSC' ); ?>" class="hasTooltip">
					<i class="icon-info"></i> <?php echo JText::_( 'URL' ); ?>:
				</label>
			</td>
			<td>
				<input class="text_area" type="text" name="url" id="url" size="60" maxlength="250" value="<?php echo $this->location->url;?>" />
			</td>
		</tr>
		<tr class="locmapicon">
			<td width="100" align="right" class="key">
				<label for="mapicon">
					<?php echo JText::_( 'JEV_MAP_ICON' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['mapicon']; ?>
			</td>
		</tr>

		<tr class="locmap">
			<td colspan="2">
				<fieldset class="adminform" >
					<legend><?php echo JText::_( 'Google_Map' ); ?></legend>
					<?php if ($this->usecats) {?>
					<div><?php echo JText::_( 'COM_JEVLOCATIONS_GOOGLE_MAP_LOOKUP_EXPLANATION' ); ?></div>
					<?php } ?>
					<table class="admintable">
					<?php if ($this->usecats) {?>

					<tr>
						<td>
							<label for="googleaddress"><?php echo JText::_( 'ADDRESS' );?></label>
						</td>
						<td>
							<label for="googlecountry"><?php echo JText::_( 'COM_JEVLOCATIONS_COUNTRY' );?></label>
						</td>
					</tr>
					<tr>
						<td>
							<input type="text" size="60" name="googleaddress" id="googleaddress" value="" />
						</td>
						<td>
							<input type="text" size="20" name="googlecountry" id="googlecountry" value="" />
							<input class="btn btn-info" type="button" name='findaddress' onclick="findAddress(true);" value="<?php echo JText::_( 'FIND_ADDRESS' );?>" />
						</td>
					</tr>
					<?php }
					else {
					?>
					<tr	>
						<td colspan="2">
							<input class="btn btn-info" type="button" name='findaddress' onclick="findAddress(true);" value="<?php echo JText::_( 'FIND_ADDRESS' );?>" />
						</td>
					</tr>

					<?php } ?>

					<tr>
						<td colspan="2">
							<div id="gmap" style="width: 550px; height: 350px"></div>
							<div style="clear:both;"></div>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<br/>
						</td>
					</tr>
					<tr align="right">
						<td class="key">
							<label for="geolat" title="<?php echo JText::_( 'COM_JEVLOCATIONS_GEO_LAT_DSC' ); ?>" class="hasTooltip">
								<i class="icon-info"></i> <?php echo JText::_( 'Geo_Lat' ); ?>:
							</label>
						</td>
						<td>
							<input class="text_area" type="text" name="geolat" id="geolat" size="32" maxlength="250" value="<?php echo $this->location->geolat;?>" />
						</td>
					</tr>
					<tr align="right">
						<td class="key">
							<label for="geolon" title="<?php echo JText::_( 'COM_JEVLOCATIONS_GEO_LONG_DSC' ); ?>" class="hasTooltip">
								<i class="icon-info"></i> <?php echo JText::_( 'Geo_Long' ); ?>:
							</label>
						</td>
						<td>
							<input class="text_area" type="text" name="geolon" id="geolon" size="32" maxlength="250" value="<?php echo $this->location->geolon;?>" />
						</td>
					</tr>
					<tr align="right">
						<td class="key">
							<label for="v">
								<?php echo JText::_( 'Geo_Zoom' ); ?>:
							</label>
						</td>
						<td>
							<input class="text_area" type="text" name="geozoom" id="geozoom" size="32" maxlength="250" value="<?php echo $this->location->geozoom;?>" />
						</td>
					</tr>
					</table>
				</fieldset>
			</td>
		</tr>

		<?php
		$result = '<iframe src="about:blank" style="display:none" name="uploadtarget" id="uploadtarget"></iframe>';

		$filename = isset($this->location->image)?$this->location->image:"";
		$filetitle = isset($this->location->imagetitle)?$this->location->imagetitle:"";
		$fieldname = "image";
		if ($filename){
			$src = JEVP_MEDIA_BASEURL."/$imgfolder/thumbnails/thumb_$filename";
			$visibility="visibility:visible;";
			$visibility="margin-right:10px;";
		}
		else {
			$src = "about:blank";
			$visibility="margin-right:0px;";
			$visibility="display:none;";
		}
		$result .= '<img id="'.$fieldname.'_img" src="'.$src.'" style="float:left;'.$visibility.'"/>';
		$result .= '<input type="hidden" name="'.$fieldname.'" id="'.$fieldname.'" value="'.$filename.'" size="50"/>';
		$result .= '<input type="hidden" name="'.$fieldname.'title" id="'.$fieldname.'title" value="'.$filetitle.'" size="50"/>';
		$result .= '<br/>';
		$result .= '<input type="file" name="'.$fieldname.'_file" id="'.$fieldname.'_file" size="50"/>';
		$result .= ' <input type="button" onclick="uploadFileType(\''.$fieldname.'\')" value="'.JText::_( 'UPLOAD' ).'"/> ';
		$result .= '<input type="button" onclick="clearImageFile(\''.$fieldname.'\')" value="'.JText::_( 'DELETE' ).'"/>';
		$result .= '<div id="'.$fieldname.'_loading" class="loading" style="display:none">'.JText::_("Image uploading. One Moment ...") .'</div>';
		$result .= '<div id="'.$fieldname.'_loaded" class="loaded" style="display:none">'.JText::_("Upload Complete ...").'</div>';
		$result .= '<br style="clear:both"/>';
		?>
		<?php if (JevLocationsHelper::canUploadImages()) :?>
	            <?php $label = JText::_("JEV_LOC_IMAGE_1"); ?>
			<tr  class="locimage">
				<td width="100" align="right" class="key">
					<label for="image1">
						<?php echo $label; ?>:
					</label>
				</td>
				<td>
					<?php echo $result;?>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<br/>
				</td>
			</tr>
		<?php endif; ?>
		<?php if (isset($this->recaptcha) && !empty($this->recaptcha)): ?>
		<tr class="locrecaptchaname">
			<td width="100" align="right" class="key">
				<?php echo $this->recaptcha['anonusername']['label']; ?>
			</td>
			<td>
				<?php echo $this->recaptcha['anonusername']['input']; ?>
			</td>
		</tr>
		<tr class="locrecaptchaemail">
			<td width="100" align="right" class="key">
				<?php echo $this->recaptcha['anonemail']['label']; ?>
			</td>
			<td>
				<?php echo $this->recaptcha['anonemail']['input']; ?>
			</td>
		</tr>
		<?php if(isset($this->recaptcha['recaptcha']['input'])) : ?>
		<tr  class="locrecaptchainput">
			<td width="100" align="right" class="key">
				<?php echo $this->recaptcha['recaptcha']['label']; ?>
			</td>
			<td>
				<?php echo $this->recaptcha['recaptcha']['input']; ?>
			</td>
		</tr>
		<?php endif; ?>
		<tr>
			<td colspan="2">
				<br/>
			</td>
		</tr>
	<?php endif; ?>
		<tr  class="locoverlaps">
			<td valign="top" align="right" class="key">
				<?php echo JText::_( 'COM_JEVLOCATIONS_CHECKOVERLAPS' ); ?>:
			</td>
			<td>
				<fieldset class="btn-grp"><?php echo $this->lists['overlaps']; ?></fieldset>
			</td>
		</tr>

		<?php 	if (JevLocationsHelper::canCreateGlobal()){ ?>
		<tr class="locglobal">
			<td valign="top" align="right" class="key">
				<?php echo JText::_( 'GLOBAL' ); ?>:
			</td>
			<td>
				<?php echo $this->lists['global']; ?>
			</td>
		</tr>
		<tr class="locpublished">
			<td valign="top" align="right" class="key">
				<?php echo JText::_( 'COM_JEVLOCATIONS_PUBLISHED' ); ?>:
			</td>
			<td>
				<?php echo $this->lists['published']; ?>
			</td>
		</tr>
        <?php
        $params = JComponentHelper::getParams( JEVEX_COM_COMPONENT );
        $showpriority = $params->get("showpriority",0);
        if ($this->setPriority && $showpriority){ ?>
		<tr class="locpriority">
        	<td  valign="top" align="right" class="key"><label for="priority" title="<?php echo JText::_( 'JEV_LOCATION_PRIORITY_DSC' ); ?>" class="hasTooltip">
					<i class="icon-info"></i> <?php echo JText::_( 'JEV_LOCATION_PRIORITY_LBL' ); ?> :</label>
			</td>
            <td >
            	<?php echo $this->priority; ?>
            </td>
		</tr>
        <?php } else { ?>
		<tr style="display:none;">
            <td colspan="2">
            	<input type="hidden" name="priority" value="<?php echo $this->location->priority;?>" />
            </td>
		</tr>
        <?php } ?>
		<tr>
			<td colspan="2">
				<br/>
			</td>
		</tr>
		<tr class="locordering">
			<td valign="top" align="right" class="key">
				<label for="ordering">
					<?php echo JText::_( 'ORDERING' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['ordering']; ?>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<br/>
			</td>
		</tr>
		<tr class="loctargetmenu">
			<td valign="top" align="right" class="key">
				<label for="targetmenu" title="<?php echo JText::_( 'JEV_TARGET_MENU_FOR_LOCATION_EVENTS_DSC' ); ?>" class="hasTooltip">
					<i class="icon-info"></i> <?php echo JText::_( 'TARGET_MENU' ); ?>:
				</label>
			</td>
			<td>
				<?php echo $this->lists['targetmenu']; ?>
			</td>
		</tr>
		<?php }

		// Now include any custom fields
		// New parameterised fields
		$hasparams = false;
		$template = $compparams->get("fieldtemplate","");
		$customfields = array();
		if ($template!=""){
			$xmlfile = JPATH_SITE."/plugins/jevents/jevcustomfields/customfields/templates/".$template;
			if (file_exists($xmlfile)){
                                JEVHelper::script('JevStdRequiredFieldsJQ.js', 'components/' . JEV_COM_COMPONENT . '/assets/js/');
				$jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
				if ($this->location->loc_id){
					$db = JFactory::getDBO();
					$db->setQuery("SELECT * FROM #__jev_customfields3 WHERE target_id=".intval($this->location->loc_id). " AND targettype='com_jevlocations'");
					$cfdata  = $db->loadObjectList('name');
					$data = array();
					foreach ($cfdata as $dataelem)
					{
						if (strpos($dataelem->name, ".") !== false)
						{
							$dataelem->name = str_replace(".", "_", $dataelem->name);
						}
						$data[$dataelem->name] = $dataelem->value;
					}
					$jcfparams->bind($data);
				}
				else {
				}
				$jcfparams->setEvent($this->location);

				$groups = $jcfparams->getFieldsets();
				foreach ($groups as $group => $element)
				{
					if ($jcfparams->getFieldCountByFieldSet($group))
					{
						$jcfparams->setupRender();
						break;
					}
				}
				foreach ($groups as $group => $element)
				{
					$count = $jcfparams->getFieldCountByFieldSet($group);

					if($count){

						$customfields = array();
						$jcfparams->render('custom_', $group, $customfields);
						if (count($customfields) > 0)
						{
							foreach ($customfields as $key => $val)
							{
                                                                $showon = isset($val["showon"]) ? $val["showon"] : "";
								?>
									<tr class="jevplugin_<?php echo $key; ?>" <?php echo $showon?> >
										<td valign="top"  width="130" align="left"><?php echo $customfields[$key]["label"]; ?></td>
										<td colspan="3"><?php echo $customfields[$key]["input"]; ?></td>
									</tr><?php
							}
						}
					}
				}
			}
		}
		?>
	</table>
	</fieldset>

	<?php if (FALSE) { ?>
	<fieldset class="adminform">
		<legend><?php echo JText::_( 'PARAMETERS' ); ?></legend>

		<table class="admintable">
		<tr>
			<td>
				<?php echo $this->params->render();?>
			</td>
		</tr>
		</table>
	</fieldset>
	<?php } ?>

		<fieldset class="adminform">
		<legend><?php echo JText::_( 'DESCRIPTION' ); ?></legend>

		<table class="admintable">
		<tr>
			<td>
				<?php
				$jevcfg = JComponentHelper::getParams("com_jevents");

				if ($jevcfg->get('com_show_editor_buttons')) {
					$t_buttons = explode(',', $jevcfg->get('com_editor_button_exceptions'));
				} else {
					// hide all
					$t_buttons = false;
				}

				// parameters : areaname, content, width, height, cols, rows
				echo $editor->display( 'description',  $this->location->description , '100%', '350', '75', '20' , $t_buttons) ;
				?>
			</td>
		</tr>
		</table>
		</fieldset>
<?php if($compparams->get('usemetatags','0')) : ?>
		<fieldset class="adminform">
		<legend><?php echo JText::_('COM_JEVLOCATIONS_LOCATION_EDIT_METATAGS_TITLE');?></legend>
		<table class="admintable">
			<tr>
				<td>
					<label for="metatitle"><?php echo JText::_('COM_JEVLOCATIONS_LOCATION_EDIT_METATITLE_LABEL');?></label>	
				</td>
				<td>
					<input class="text_area" type="text" name="metatitle" id="metatitle" size="60" maxlength="250" value="<?php echo $this->location->metatitle;?>" />
				</td>
			</tr>
			<tr>
				<td>
					<label for="metadescription"><?php echo JText::_('COM_JEVLOCATIONS_LOCATION_EDIT_METADESCRIPTION_LABEL');?></label>	
				</td>
				<td>
					<textarea class="text_area" name="metadescription" id="metadescription" rows="4" cols="50" maxlength="365"><?php echo $this->location->metadescription;?></textarea>
				</td>
			</tr>
		</table>
<?php endif; ?>
	</div>

	<input type="hidden" name="option" value="com_jevlocations" />
	<input type="hidden" name="cid[]" value="<?php echo $this->location->loc_id; ?>" />
	<input type="hidden" name="returntask" value="<?php echo $this->returntask;?>" />
	<input type="hidden" name="Itemid" value="<?php echo JRequest::getInt("Itemid",0);?>" />
	<input type="hidden" name="task" value="locations.edit" />
	<?php if (JRequest::getString("tmpl","")=="component"){ ?>
	<input type="hidden" name="tmpl" value="component" />
	<?php } ?>
	<?php if (JRequest::getInt("pop",0)==1){ ?>
	<input type="hidden" name="pop" value="1" />
	<?php } ?>
	<?php echo JHTML::_( 'form.token' ); ?>
</form>

<?php echo JevHtmlBootstrap::tooltip();

$messagesScript = array();
foreach($messages as $variable => $message)
{
	$messagesScript[] = "var jev_str_$variable = '$message';";
}

$document->addScriptDeclaration(implode("\n",$messagesScript));


$params = JComponentHelper::getParams("com_jevents");
if ($params->get("bootstrapchosen", 1))
{
    JHtml::_('formbehavior.chosen', '#adminForm select:not(.notchosen)');
    ?>
<script type="text/javascript" >
	window.setTimeout("setupJEventsBootstrap()", 500);

	function setupJEventsBootstrap(){
		(function($){
			// Turn radios into btn-group
			$('.radio.btn-group label').addClass('btn');
			var el = $(".radio.btn-group label");

			// Isis template and others may already have done this so remove these!
			$(".radio.btn-group label").unbind('click');

			$(".radio.btn-group label").click(function() {
				var label = $(this);
				var input = $('#' + label.attr('for'));
				if (!input.prop('checked') && !input.prop('disabled')) {
					label.closest('.btn-group').find("label").removeClass('active btn-success btn-danger btn-primary');
					if (input.prop('value')!=0){
						label.addClass('active btn-success');
					}
					else {
						label.addClass('active btn-danger');
					}
					input.prop('checked', true);
                                        input.trigger('change');
				}
			});

			// Turn checkboxes into btn-group
			$('.checkbox.btn-group label').addClass('btn');

			// Isis template and others may already have done this so remove these!
			$(".checkbox.btn-group label").unbind('click');
			$(".checkbox.btn-group label input[type='checkbox']").unbind('click');

			$(".checkbox.btn-group label").click(function(event) {
				event || (event = window.event);

				// stop the event being triggered twice is click on input AND label outside it!
				if (event.target.tagName.toUpperCase()=="INPUT"){
					//event.preventDefault();
					return;
				}

				var label = $(this);
				var input = $('#' + label.attr('for'));
				//alert(label.val()+ " "+event.target.tagName+" checked? "+input.prop('checked')+ " disabled? "+input.prop('disabled')+ " label disabled? "+label.hasClass('disabled'));
				if (input.prop('disabled')) {
					label.removeClass('active btn-success btn-danger btn-primary');
					input.prop('checked', false);
					event.stopImmediatePropagation();
                                        input.trigger('change');
					return;
				}
				if (!input.prop('checked')) {
					if (input.prop('value')!=0){
						label.addClass('active btn-success');
					}
					else {
						label.addClass('active btn-danger');
					}
				}
				else {
					label.removeClass('active btn-success btn-danger btn-primary');
				}
                                input.trigger('change');
				// bootstrap takes care of the checkboxes themselves!

			});

			$(".btn-group input[type=checkbox]").each(function() {
				var input = $(this);
				input.css('display','none');
			});
		})(jQuery);

		initialiseBootstrapButtons();
	}

	function initialiseBootstrapButtons(){
		(function($){
			// this doesn't seem to find just the checked ones!'
			//$(".btn-group input[checked=checked]").each(function() {
			var clickelems = $(".btn-group input[type=checkbox] , .btn-group input[type=radio]");

			clickelems.each(function(idx, val) {
				if (!$(this).attr('id')){
					return;
				}
				var label = $("label[for=" + $(this).attr('id') + "]");
				var elem = $(this);
				if (elem.prop('disabled')) {
					label.addClass('disabled');
					label.removeClass('active btn-success btn-danger btn-primary');
					return;
				}
				label.removeClass('disabled');
				if (!elem.prop('checked')) {
					label.removeClass('active btn-success btn-danger btn-primary');
					return;
				}
				if (elem.val()!=0){
					label.addClass('active btn-success');
				}
				else {
					label.addClass('active btn-danger');
				}

			});

		})(jQuery);
	}

</script>
    <?php
}
endif;
