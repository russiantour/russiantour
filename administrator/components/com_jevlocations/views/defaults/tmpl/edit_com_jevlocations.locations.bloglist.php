<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id: edit_com_jevlocations.locations.detail.php 2522 2011-09-01 10:04:51Z geraintedwards $
 * @package     JEvents
 * @copyright   Copyright (C)  2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
defined('_JEXEC' ) or die('Restricted access');
?>
<table cellpadding="0" cellspacing="0" border="0">
                  <tr>
                                    <td><?php echo JText::_("JEV_PLUGIN_INSTRUCTIONS", true); ?></td>
                                    <td><select id="jevdefaults" onchange="defaultsEditorPlugin.insert('value','jevdefaults' )" ></select></td>
                  </tr>
                  <tr>
                                    <td colspan="3">
                                        <h3><?php echo JText::_("COM_JEVLOCATIONS_LAYOUT_INSTRUCTIONS_TITLE", true); ?></h3>
                                        <p><?php echo JText::_("COM_JEVLOCATIONS_LAYOUT_INSTRUCTIONS_DESC", true); ?></p>
                                    </td>
                  </tr>
                  <tr>
                                    <td colspan="3">
                                        <dl>
                                            <dt style="font-style: italic;"><?php echo JText::_("COM_JEVLOCATIONS_LAYOUT_IMAGE_CLASS", true); ?></dt>
                                            <dd><?php echo JText::_("COM_JEVLOCATIONS_LAYOUT_IMAGE_CLASS_DESC", true); ?></dd>
                                            <dt style="font-style: italic;"><?php echo JText::_("COM_JEVLOCATIONS_LAYOUT_FIELD_CLASS", true); ?></dt>
                                            <dd><?php echo JText::_("COM_JEVLOCATIONS_LAYOUT_FIELD_CLASS_DESC", true); ?></dd>
                                        </dl>
                                    </td>
                  </tr>
</table>

<script type="text/javascript">
	defaultsEditorPlugin.node($('jevdefaults'),"<?php echo JText::_("JEV_PLUGIN_SELECT", true); ?>","");
	// built in group
	var optgroup = "";

<?php
// get list of enabled plugins
$jevplugin = JPluginHelper::getPlugin("jevents", "jevlocations");
if (JPluginHelper::importPlugin("jevents", $jevplugin->name))
{
	$classname = "plgJevents" . ucfirst($jevplugin->name);
	if (is_callable(array($classname, "fieldNameArray")))
	{
		$lang = JFactory::getLanguage();
		$lang->load("plg_jevents_" . $jevplugin->name, JPATH_ADMINISTRATOR);
		$fieldNameArray = call_user_func(array($classname, "fieldNameArray"),"bloglist");
		if (!isset($fieldNameArray['labels']))
			return;
		?>
					optgroup = defaultsEditorPlugin.optgroup($('jevdefaults') , '<?php echo $fieldNameArray["group"]; ?>');
		<?php
		for ($i = 0; $i < count($fieldNameArray['labels']); $i++)
		{
			?>
							defaultsEditorPlugin.node(optgroup , "<?php echo $fieldNameArray['labels'][$i]; ?>", "<?php echo $fieldNameArray['values'][$i]; ?>");
			<?php
			if (false && strpos($fieldNameArray['values'][$i], "_lbl") === false)
			{
				?>
							defaultsEditorPlugin.node(optgroup , "<?php echo  $fieldNameArray['labels'][$i].' Label'; ?>", "<?php echo str_replace('}}','_lbl}}',$fieldNameArray['values'][$i]); ?>");
				<?php
			}
		}
		?>
					defaultsEditorPlugin.node(optgroup , "<?php echo JText::_('COM_JEVLOCATIONS_UPCOMING_EVENTS', true); ?>", "JEVLOC_UPCOMING");
		<?php
	}
}
?>
</script>
