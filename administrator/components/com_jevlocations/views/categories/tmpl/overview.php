<?php defined('_JEXEC' ) or die('Restricted access'); 

$pathIMG = JURI::root().'/administrator/images/';
?>
<div id="jevents">
<form action="index.php" method="post"  id="adminForm" name="adminForm" >
<input type="hidden" name="section" value="<?php echo $this->section;?>" />
<table width="90%" border="0" cellpadding="2" cellspacing="2" class="adminform">
<tr><td>
<script>			
function saveCategoryOrder( n ) {
	for ( var j = 0; j <= n; j++ ) {
		box = eval( "document.adminForm.cb" + j );
		if ( box ) {
			if ( box.checked == false ) {
				box.checked = true;
			}
		} else {
			alert("You cannot change the order of items, as an item in the list is `Checked Out`");
			return;
		}
	}
	submitform('categories.saveorder');
}
</script>
<table cellpadding="4" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'FILTER' ); ?>:
			<input type="text" name="search" id="search" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
			<button onclick="this.form.submit();"><?php echo JText::_( 'GO' ); ?></button>
			<button onclick="document.getElementById('search').value='';this.form.getElementById('catid').value='-1';this.form.getElementById('catid2').value='-1';this.form.submit();"><?php echo JText::_( 'RESET' ); ?></button>
			<?php
			echo $this->catFilter()."&nbsp;";
			echo $this->catFilter2();
			?>
		</td>
	</tr>
</table>

<table cellpadding="4" cellspacing="0" border="0" width="100%" class="adminlist">
	<tr>
		<th width="20" nowrap="nowrap">
			<input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" />
		</th>
		<th class="title" width="75%" nowrap="nowrap"><?php echo JText::_('COM_JEVLOCATIONS_CATEGORY_TITLE'); ?></th>
		<th class="title" width="25%" nowrap="nowrap"><?php echo JText::_('COM_JEVLOCATIONS_CATEGORY_PARENT'); ?></th>
		<th width="2%">	Order</th>
		<th width="1%">
			<?php
				if (version_compare(JVERSION, "1.6.0", 'ge')) {
					echo JHtml::_('grid.order', $this->cats, 'filesave.png', 'categories.saveorder');
				}
				else {
					$img = 'filesave.png';
					echo  '<a href="javascript: saveCategoryOrder( '. (count( $this->cats)-1) .' )"><img src="'.$pathIMG . $img.'" width="16" height="16" border="0" alt="Save Order" /></a>';
				}
			?>
		</th>

		<th width="10%" nowrap="nowrap"><?php echo JText::_('COM_JEVLOCATIONS_PUBLISHED'); ?></th>
		<th width="10%" nowrap="nowrap"><?php echo JText::_('COM_JEVLOCATIONS_ACCESS'); ?></th>
	</tr>

    <?php
    $k=0;
    $i=0;
    foreach ($this->cats as $cat) {
    	?>
        <tr class="row<?php echo $k; ?>">
        	<td width="20" >
                <?php echo JHtml::_('grid.id', $i, $cat->id) ?>
        	</td>
          	<td >
          		<a href="#edit" class="hasTooltip" onclick="return listItemTask('cb<?php echo $i;?>','categories.edit')" title="<?php echo JText::_('COM_JEVLOCATIONS_COUNTRYCITY_CLICK_TO_EDIT'); ?>">
          		<?php echo $cat->title; ?>
          		</a>
          	</td>
          	<td><?php echo ($cat->parent_id>0) ? $cat->parenttitle : "-"; ?></td>
			<td align="center" colspan="2">
			<input type="text" name="order[]" size="5" value="<?php echo $cat->ordering; ?>" class="text_area" style="text-align: center" />
			</td>
          	<td align="center">
          	<?php                      	
			if (version_compare(JVERSION, "1.6.0", 'ge')) {
				$img =  $cat->published ? JHtml::_('image','admin/tick.png', '',array('title'=>''),true) :JHtml::_('image','admin/publish_x.png', '',array('title'=>''),true);
			}
			else {
				$img = $cat->published ? 'tick.png' : 'publish_x.png';
				$img = '<img src="'.$pathIMG . $img.'" width="16" height="16" border="0" alt="" />';
			}
          	?>
          	<a href="javascript: void(0);" onclick="return listItemTask('cb<?php echo $i; ?>','<?php echo $cat->published ? 'categories.unpublish' : 'categories.publish'; ?>')"><?php echo $img; ?></a>
          	</td>
          	<td align="center"><?php echo $cat->_groupname;?></td>
        </tr>
        <?php
        $i++;
        $k = 1 - $k;
    } ?>
    <tfoot>
        <tr>
    	  <td align="center" colspan="7">
			<?php echo $this->pageNav->getListFooter(); ?>
		  </td>
		</tr>
    </tfoot>
</table>
</td>
</tr>  
</table>
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="task" value="categories.list" />
<input type="hidden" name="act" value="" />
<input type="hidden" name="option" value="<?php echo JEVEX_COM_COMPONENT;?>" />
</form>
</div>
<?php
JHtml::_('bootstrap.tooltip');