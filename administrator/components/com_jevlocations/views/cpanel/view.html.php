<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

jimport( 'joomla.application.component.view');

/**
 * HTML View class for the component
 *
 * @static
 */
class AdminCpanelViewCpanel extends JViewLegacy
{
	/**
	 * Control Panel display function
	 *
	 * @param template $tpl
	 */
	function cpanel($tpl = null)
	{
		jimport('joomla.html.pane');
		JHtml::_('bootstrap.framework');

		JHtml::stylesheet('com_jevlocations/jevlocations3x.css',array(),true);
		JHTML::stylesheet('administrator/components/com_jevlocations/assets/css/eventsadmin.css');
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		//We check Google Maps Key
		$params = JComponentHelper::getParams('com_jevlocations');
		if( !$params->get('googlemapskey','') )
		{
			JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_JEVLOCATIONS_GOOGLEMAPSKEY_WARNING','https://www.jevents.net/blog/changes-in-google-maps-terms-of-service'),'warning');
		}

		if( $params->get('googlestaticmapskey','') )
		{
			if( !$params->get('googlemapskey','') )
			{
				JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_JEVLOCATIONS_GOOGLESTATICMAPSKEY_NOGOOGLEMAPS_WARNING','https://www.jevents.net/blog/changes-in-google-maps-terms-of-service'),'warning');
			}
			else if( $params->get('googlestaticmapskey','') == $params->get('googlemapskey','') )
			{
				JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_JEVLOCATIONS_GOOGLESTATICMAPSKEY_WITH_DIFFERENT_GOOGLEMAPS_WARNING','https://www.jevents.net/blog/changes-in-google-maps-terms-of-service'),'warning');
			}

		}


		JevLocationsHelper::addSubmenu();

		$document = JFactory::getDocument();
		$document->setTitle(JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') . ' :: ' .JText::_( 'CONTROL_PANEL' ));

		// Set toolbar items for the page
		JToolBarHelper::title( JText::_('JEVENTS_JEVENTS_LOCATION_MANAGER') .' :: '. JText::_( 'CONTROL_PANEL' ), 'jevlocations' );
		JToolBarHelper::preferences('com_jevlocations', '580', '750');  // RSH 10/12/10 Use default width of '800' to accomodate permissions

		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}


	 /**
	 * This method creates a standard cpanel button
	 *
	 * @param string $link
	 * @param string $image
	 * @param string $text
	 * @param string $path
	 * @param string $target
	 * @param string $onclick
	 * @access protected
	 */
	function _quickiconButton( $link, $image, $text, $path=null, $target='', $onclick='' ) {
	 	if( $target != '' ) {
	 		$target = 'target="' .$target. '"';
	 	}
	 	if( $onclick != '' ) {
	 		$onclick = 'onclick="' .$onclick. '"';
	 	}
	 	if( $path === null || $path === '' ) {
	 		$option=JRequest::getCmd("option");
	 		$path = 'administrator/components/'.$option.'/assets/images/';
	 	}
		$alttext = str_replace("<br/>", " ", $text);
		?>
		<div style="float:left;">
			<div class="icon">
				<a href="<?php echo $link; ?>" <?php echo $target;?>  <?php echo $onclick;?> title="<?php echo $alttext;?>">
					<?php
					//echo JHTML::_('image.administrator', $image, $path, NULL, NULL, $text );
					if (strpos($path, '/')===0){
						$path = substr($path,1);
					}
					echo JHTML::_('image', $path.$image, $alttext , array('title'=>$alttext), false);
					//JHtml::_('image', 'mod_languages/'.$menuType->image.'.gif', $alt, array('title'=>$menuType->title_native), true)
					?>
					<span><?php echo $text; ?></span>
				</a>
			</div>
		</div>
		<?php
	}

	function _quickiconButtonWHover($link, $image, $image_hover, $text, $path = '/administrator/images/', $target = '', $onclick = '')
	{
		if ($target != '')
		{
			$target = 'target="' . $target . '"';
		}
		if ($onclick != '')
		{
			$onclick = 'onclick="' . $onclick . '"';
		}
		if ($path === null || $path === '')
		{
			$path = '/administrator/images/';
		}
		$alttext = str_replace("<br/>", " ", $text);
		?>
		<div id="cp_icon_container">
			<div class="cp_icon">
				<a href="<?php echo $link; ?>" <?php echo $target; ?>  <?php echo $onclick; ?> title="<?php echo $alttext; ?>">
					<?php
					//echo JHTML::_('image.administrator', $image, $path, NULL, NULL, $text );
					if (strpos($path, '/') === 0)
					{
						$path = substr($path, 1);
					}
					$atributes = array('title' => $alttext, 'onmouseover' => 'this.src=\'../' . $path . $image_hover . '\'', 'onmouseout' => 'this.src=\'../' . $path . $image . '\'' );

					echo JHTML::_('image', $path . $image, $alttext, $atributes, false);
					//JHtml::_('image', 'mod_languages/'.$menuType->image.'.gif', $alt, array('title'=>$menuType->title_native), true)
					?>
					<span><?php echo $text; ?></span>
				</a>
			</div>
		</div>
	<?php

	}

	/**
	 * Routine to hide submenu suing CSS since there are no paramaters for doing so without hiding the main menu
	 *
	 */
	function _hideSubmenu(){
		$option = JEVEX_COM_COMPONENT;
		JHTML::stylesheet(  'administrator/components/'.$option.'/assets/css/hidesubmenu.css' );
	}

}
