<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
jimport("joomla.filesystem.folder");
JFormHelper::loadFieldClass('list');

/**
 * JEVMenu Field class for the JEvents Component
 *
 * @package		JEvents.fields
 * @subpackage	com_banners
 * @since		1.6
 */
class JFormFieldJEVTemplate extends JFormFieldList
{

	/**
	 * The form field type.s
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'JEVTemplate';

	public function getInput()
	{
		JFactory::getLanguage()->load('plg_jevents_jevcustomfields', JPATH_ADMINISTRATOR);

		// While I'm here do the database setup and upgrades
		static $dbchecked = false;
		if (!$dbchecked)
		{
			$dbchecked = true;
			$db = JFactory::getDBO();
			$charset = ($db->hasUTF()) ? 'DEFAULT CHARACTER SET `utf8`' : '';
			$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jev_customfields3(
	id int(11) NOT NULL auto_increment,
	target_id int(11) NOT NULL default 0,
	targettype varchar(255) NOT NULL default '',
	name varchar(255) NOT NULL default '',
	value text NOT NULL ,

	PRIMARY KEY  (id),
	INDEX (target_id, targettype(200)),
	INDEX combo (name(200), value(10))
)  $charset;
SQL;
			$db->setQuery($sql);
			if (!$db->query())
			{
				echo $db->getErrorMsg();
			}

			// If upgrading then add new columns - do all the tables at once
			$sql = "SHOW COLUMNS FROM `#__jev_customfields3`";
			$db->setQuery($sql);
			$cols = $db->loadObjectList();
			$uptodate = false;
			foreach ($cols as $col)
			{
				if ($col->Field == "target_id")
				{
					$uptodate = true;
					break;
				}
			}
			if (!$uptodate)
			{
				$sql = "ALTER TABLE #__jev_customfields3 ADD COLUMN target_id int(11) NOT NULL default 0";
				$db->setQuery($sql);
				@$db->query();
			}
		}

		return parent::getInput();

	}

	public function getOptions()
	{
// Initialize variables.
		$options = array();

		jimport("joomla.utilities.file");
		if (JFolder::exists(JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/")) {
			$templates = JFolder::files(JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/", ".xml");
		}
		else {
			$templates = array();
		}

// only offer extra fields templates if there is more than one available
		if (count($templates) > 0)
		{
			$options = array();
			$options[] = JHTML::_('select.option', "", JText::_("COM_JEVLOCATIONS_SELECT_TEMPLATE"), 'value', 'text');
			foreach ($templates as $template)
			{
				if ($template == "fieldssample.xml" || $template == "fieldssample16.xml"  || $template == "all_fields.xml")
					continue;
				$options[] = JHTML::_('select.option', $template, ucfirst(str_replace(".xml", "", $template)), 'value', 'text');
			}
		}
		return $options;

	}

}
