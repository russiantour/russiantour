<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

class JFormFieldCountryfilter extends JFormFieldList {

    protected $type = 'Countryfilter';

    public function getOptions() {
        $locparams = JComponentHelper::getParams("com_jevlocations");
        $db = JFactory::getDBO();
        $usecats = $locparams->get("usecats", 0);
        if ($usecats) {

            // Make sure there aren't too many
            $sql = 'SELECT count(c.id) FROM #__jevlocation_categories AS c WHERE c.published = 1 AND c.section = "com_jevlocations"';
            $db->setQuery($sql);
            if (intval($db->loadResult()) > 500) {
                $sql = 'SELECT c.id, c.title as ctitle,p.title as ptitle,' .
                        ' CASE WHEN CHAR_LENGTH(p.title) THEN CONCAT_WS(" => ", p.title, c.title) ELSE c.title END as title' .
                        ' FROM #__jevlocation_categories AS c' .
                        ' LEFT JOIN #__jevlocation_categories AS p ON p.id=c.parent_id' .
                        ' LEFT JOIN #__jevlocation_categories AS gp ON gp.id=p.parent_id ' .
                        ' WHERE c.published = 1 ' .
                        ' AND c.section = "com_jevlocations" ' .
                        ' ORDER BY  ptitle ASC, ctitle ASC';
            } else {

                $sql = 'SELECT c.id, c.title as ctitle,p.title as ptitle, gp.title as gptitle, ' .
                        ' CASE WHEN CHAR_LENGTH(p.title) THEN CONCAT_WS(" => ", p.title, c.title) ELSE c.title END as title' .
                        ' FROM #__jevlocation_categories AS c' .
                        ' LEFT JOIN #__jevlocation_categories AS p ON p.id=c.parent_id' .
                        ' LEFT JOIN #__jevlocation_categories AS gp ON gp.id=p.parent_id ' .
                        ' WHERE c.published = 1 ' .
                        ' AND c.section = "com_jevlocations" ' .
                        ' ORDER BY gptitle ASC, ptitle ASC, ctitle ASC';
            }
            $db->setQuery($sql);
            $loccats = @$db->loadObjectList('id');


            $input = "<div style='float: left;text-align:left;'>";
            $input .= "<select multiple='multiple' id='jevloccats2'  size='5'  onchange='updateJevLocCats2();'>";
            $selected = (count($invalue) == 0) ? "selected='selected'" : "";
            //$input .= "<option value='' $selected>---</option>";
            foreach ($loccats as $loccat) {
                $title = $loccat->ctitle;
                if (!is_null($loccat->ptitle)) {
                    $title = $loccat->ptitle . "=>" . $title;
                }
                if (isset($loccat->gptitle) && !is_null($loccat->gptitle)) {
                    $title = $loccat->gptitle . "=>" . $title;
                }
                $catid = $loccat->id;
                $selected = in_array($loccat->id, $invalue) ? "selected='selected'" : "";
                $input .= "<option value='$catid' $selected>$title</option>";
            }
            $input .= "</select>";
            $input .= '<input type="hidden"  name="' . $name . '"   id="jevloccat2" value="' . $value . '" />';
            $input .= "</div>";

            $script = '
			function updateJevLocCats2(){
				var input = document.getElementById("jevloccat2");
				input.value="";
				// jQuery array
				jQuery("select#jevloccats2 option:selected").each(	function(index, item){
					// if select none - reset everything else
					if (item.value=="") {
						select.selectedIndex=0;
						return;
					}
					if (input.value!="") input.value+=",";
					input.value+="jevlcsc:"+item.value;
				});
			}
			';
            $document = JFactory::getDocument();
            $document->addScriptDeclaration($script);

            $data = new stdClass();
            $data->name = "loccsc";
            $data->html = $input;
            $data->label = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_STATE_LBL');
            $data->description = JText::_('PLG_JEVENTS_JEVLOCATIONS_LOCATION_STATE_DSC');
            $data->options = array();
            $menudata[$id] = $data;
            return;
        } else {


            $sql = 'SELECT distinct(loc.country) as value, loc.country as text FROM #__jev_locations as loc ' .
                    ' WHERE loc.published = 1 AND loc.global=1' .
                    ' AND  loc.country !=""  ' .
                    ' ORDER BY  loc.country ASC';
            $db->setQuery($sql);
            $rows = @$db->loadObjectList();

            return $rows;
        }
    }
    
    function getInput() {
        if (is_string($this->value)){
            $this->value = str_replace(", ",",",$this->value);
            $this->value = str_replace(" , ",",",$this->value);
            $this->value = explode(",", $this->value);
        }
        return parent::getInput();
    }

}
