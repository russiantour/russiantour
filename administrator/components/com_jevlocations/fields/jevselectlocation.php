<?php
/**
 * JEvents Locations Component for Joomla 1.5.x
 *
 * @version     $Id: jevselectevent.php 3503 2012-04-10 11:04:26Z geraintedwards $
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

// Check to ensure this file is included in Joomla!

defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
include_once(JPATH_SITE . "/components/com_jevents/jevents.defines.php");
JLoader::register('JevModal',JPATH_LIBRARIES."/jevents/jevmodal/jevmodal.php");

class JFormFieldJEVselectLocation extends JFormField
{

	protected $type = 'JEVselectlocation';

	protected function getInput()
	{
		$sortableLocations = "
			// override script method
			function selectThisLocation(loc_id,elem) {
				var title = elem.innerHTML;
				jQuery('#selectedlocationtitle').val(title);
				jQuery('#selectedlocation').val(loc_id);
				try {
					SqueezeBox.close();
					return false;
				}
				catch (e) {
				}
				try {
					window.parent.SqueezeBox.close();
				}
				catch (e) {
					window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
				}

				return false;
			}
			";

		// Add the script to the document head.
		JFactory::getDocument()->addScriptDeclaration($sortableLocations);

		// Setup variables for display.
		$html	= array();
		$link = 'index.php?option=com_jevlocations&task=locations.select&tmpl=component';

		// get the location id
		$locidfield = $this->form->getField("loc_id", "request");
		$loc_id = $locidfield->value;
		$db	= JFactory::getDBO();
		$db->setQuery(
			'SELECT title' .
			' FROM #__jev_locations as loc ' .
			' WHERE loc_id = '.(int) $loc_id
		);
		$title = $db->loadResult();
		echo $db->getErrorMsg();

		if ($error = $db->getErrorMsg()) {
			JError::raiseWarning(500, $error);
		}

		if (empty($title)) {
			$title = JText::_('COM_JEVLOCATIONS_FIELD_SELECT_LOCATION_LABEL');
		}
		$title = htmlspecialchars($title, ENT_QUOTES, 'UTF-8');

		// The current user display field.
		$html[] = '<div class="fltlft" style="display:inline-block;">';
		$html[] = '  <input type="text" id="selectedlocationtitle" value="'.$title.'" disabled="disabled" size="35" />';
		$html[] = '</div>';

		// The user select button.
		$html[] = '<div class="button2-left" style="display:inline-block;">';
		$html[] = '  <div class="blank">';
//		$html[] = '	<a class="modal" title="'.JText::_('COM_JEVLOCATIONS_CHANGE_LOCATION').'"  href="'.$link.'" rel="{handler: \'iframe\', size: {x: 800, y: 450}}">'.JText::_('COM_JEVLOCATIONS_CHANGE_LOCATION_BUTTON').'</a>';
//
//		javascript:jevModalPopup('jevLocationModal', '/administrator/index.php?option=com_jevlocations&task=locations.select&tmpl=component', 'Select Location');
		JevModal::modal('a.btn.jevmodal');
		$html[] = '	<a class="btn jevmodal btn-primary" title="'.JText::_('COM_JEVLOCATIONS_CHANGE_LOCATION').'"  href="javascript:jevModalPopup(\'jevLocationModal\', \''.$link.'\', \'Select Location\');"><span class="icon-list icon-white"></span>'.JText::_('COM_JEVLOCATIONS_CHANGE_LOCATION_BUTTON').'</a>';
		$html[] = '  </div>';
		$html[] = '</div>';

		// The active event id field.
		if (0 == (int)$this->value) {
			$value = '';
		} else {
			$value = (int)$this->value;
		}

		// class='required' for client side validation
		$class = '';
		if ($this->required) {
			$class = ' class="required modal-value"';
		}

		$html[] = '<input type="hidden" id="selectedlocation"  '.$class.' name="'.$this->name.'" value="'.$value.'" />';

		return implode("\n", $html);
	}
}