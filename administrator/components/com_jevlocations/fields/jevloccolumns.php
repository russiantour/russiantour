<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport('joomla.form.formfield');

class JFormFieldJevloccolumns extends JFormField {

    protected $type = 'Jevloccolumns';

    public function getInput() {
        JHtml::_('jquery.ui', array('core', 'sortable'));

        JHTML::script('administrator/components/com_jevlocations/assets/js/jevloccolumns.js');
        JHtml::stylesheet('com_jevlocations/jevlocations3x.css', array(), true);
        // Lets check if we have editted before! if not... rename the custom file.
        if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css")) {
            // It is definitely now created, lets load it!
            JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
        }

        $columnsArray['location'] = JText::_('COM_JEVLOCATIONS_LOCATION');
        $columnsArray['events'] = JText::_('COM_JEVLOCATIONS_LOCATION_EVENTS');
        $columnsArray['image'] = JText::_('COM_JEVLOCATIONS_LOCATION_IMAGE');
        $columnsArray['country'] = JText::_('COM_JEVLOCATIONS_COUNTRY');
        $columnsArray['state'] = JText::_('COM_JEVLOCATIONS_STATE');
        $columnsArray['city'] = JText::_('COM_JEVLOCATIONS_CITY');
        $columnsArray['phone'] = JText::_('COM_JEVLOCATIONS_PHONE');
        $columnsArray['url'] = JText::_('COM_JEVLOCATIONS_WEBSITE');
        $columnsArray['postcode'] = JText::_('COM_JEVLOCATIONS_POSTCODE');
        $columnsArray['street'] = JText::_('COM_JEVLOCATIONS_STREET');

        $compparams = JComponentHelper::getParams("com_jevlocations");
        $template = $compparams->get("fieldtemplate", "");

        if ($template != "" && $compparams->get("custinlist") && JPluginHelper::isEnabled("jevents", 'jevcustomfields')) {
            $xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
            if (file_exists($xmlfile)) {
                JLoader::register('JevCfForm', JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/jevcfform.php");
                $jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
                $customfields = array();
                $groups = $jcfparams->getFieldsets();
                foreach ($groups as $group => $element) {
                    $fsparams = $jcfparams->getFieldset($group);

                    if ($fsparams) {
                        foreach ($fsparams as $p => $node) {
                            $nodename = $node->attribute('name');
                            $nodelabel = $node->attribute('label');
                            if ($nodelabel && $nodename) {
                                $columnsArray[$nodename] = JText::_($nodelabel);
                            }
                        }
                    }
                }
            }
        }

        $choosenValues = explode(",", $this->value);

        $input[] = '<div id="jevents">';
        $input[] = "<ul id='sortableLocationsColumns' class='jevbootstrap' style='cursor: move;display: inline-block;left: 200px; margin-top: 15px;float:left; list-style: none outside none'>";

        //We add the choosen values in their correct order
        foreach ($choosenValues as $field) {
            $input[] = "<li style='height:30px;' id='" . $field . "' title='" . JText::_('COM_JEVLOCATIONS_MENU_REORDER_COLUMN') . "' class=\"jevloc-col-enabled hasTooltip\"><i class='icon-menu'></i> <div class='btn jevloc-toggle-column hasTooltip' title='" . JText::_('COM_JEVLOCATIONS_MENU_HIDE_COLUMN') . "'><i class='icon-eye'> </i></div> " . $columnsArray[$field] . "</li>";
            //we unset the array element to track the already inserted elements
            unset($columnsArray[$field]);
        }

        //These values are not choosen
        foreach ($columnsArray as $field => $columnName) {
            $input[] = "<li style='height:30px;' id='" . $field . "' title='" . JText::_('COM_JEVLOCATIONS_MENU_REORDER_COLUMN') . "' class=\"jevloc-col-disabled hasTooltip\"><i class='icon-menu'></i> <div class='btn jevloc-toggle-column hasTooltip' title='" . JText::_('COM_JEVLOCATIONS_MENU_HIDE_COLUMN') . "'><i class='icon-eye-blocked'> </i></div> " . $columnName . "</li>";
        }
        $input[] = "</ul>";
        $input[] = '<input style="display:none;" id="sortableLocColumns" type="text" value="' . $this->value . '" name="' . $this->name . '">';
        $input[] = "</div>";
        return implode("\n", $input);
    }

    /**
     * Method to get the field options.
     *
     * @return	array	The field option objects.
     * @since	1.6
     */
    protected function getOptions() {
        // Initialize variables.
        $session = JFactory::getSession();
        $options = array();



        /*
          // Initialize some field attributes.
          $extension = $this->element['extension'] ? (string) $this->element['extension'] : (string) $this->element['scope'];
          $published = (string) $this->element['published'];

          // Load the category options for a given extension.
          if (!empty($extension))
          {

          // Filter over published state or not depending upon if it is present.
          if ($published)
          {
          $options = JHtml::_('category.options', $extension, array('filter.published' => explode(',', $published)));
          }
          else
          {
          $options = JHtml::_('category.options', $extension);
          }

          // Verify permissions.  If the action attribute is set, then we scan the options.
          if ($action = (string) $this->element['action'])
          {

          // Get the current user object.
          $user = JFactory::getUser();

          // TODO: Add a preload method to JAccess so that we can get all the asset rules in one query and cache them.
          // eg JAccess::preload('core.create', 'com_content.category')
          foreach ($options as $i => $option)
          {
          // Unset the option if the user isn't authorised for it.
          if (!$user->authorise($action, $extension . '.category.' . $option->value))
          {
          unset($options[$i]);
          }
          }
          }

          array_unshift($options, JHTML::_('select.option', '0', '- ' . JText::_( 'JEV_SELECT_CATEGORY' ) . ' -'));
          }
          else
          {
          JError::raiseWarning(500, JText::_('JLIB_FORM_ERROR_FIELDS_CATEGORY_ERROR_EXTENSION_EMPTY'));
          }

          // if no value exists, try to load a selected filter category from the list view
          if (!$this->value && ($this->form instanceof JForm))
          {
          $context = $this->form->getName();
          $this->value = $session->get($context . '.filter.category_id', $this->value);
          }

          // Merge any additional options in the XML definition.
          $options = array_merge(parent::getOptions(), $options);
         */
        $options = array(JText::_('COM_JEVLOCATIONS_LOCATION'),
            JText::_('COM_JEVLOCATIONS_LOCATION_EVENTS'),
            JText::_('COM_JEVLOCATIONS_LOCATION_IMAGE'),
            JText::_('COM_JEVLOCATIONS_COUNTRY'),
            JText::_('COM_JEVLOCATIONS_STATE'),
            JText::_('COM_JEVLOCATIONS_CITY'),
            JText::_('COM_JEVLOCATIONS_PHONE'),
            JText::_('COM_JEVLOCATIONS_STREET'),
            JText::_('COM_JEVLOCATIONS_POSTCODE'));
        return $options;
    }

}
