<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

jimport('joomla.html.html');
jimport('joomla.form.formfield');
jimport('joomla.form.helper');
JFormHelper::loadFieldClass('list');

class JFormFieldJevlcategory extends JFormFieldList
{

	protected $type = 'Jevlcategory';

	public function getOptions()
	{
		return "";
	}

	public function getInput()
	{
		return "";
	}

	/*public function getInput()
	{
		JHTML::script('administrator/components/com_jevlocations/assets/js/migrateLocationCategoryFilter.js');
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		$query->select('newcatid');
		$query->from('#__jevlocations_oldcatmap');
		$query->where('oldcatid = '.(int)$this->value);
		$db->setQuery($query);
		$newcatid = $db->loadResult();
		if($newcatid)
		{
			$input = "<script type='text/javascript'>
                      migrateLocationCategoryFilter.setNewValue(".$newcatid.");";
			$input .= "</script>";
		}
		else
		{
			$input = "";
		}
		return "";
	}*/

	public function getLabel()
	{
		return "";
	}
/*
	private function fetchElement($name, $value, &$node, $control_name, $raw=false)  // RSH added raw parameter for J1.6 compatiblity
	{

		// Must load admin language files
		$lang = JFactory::getLanguage();
		$lang->load("com_jevlocations", JPATH_ADMINISTRATOR);

		$db = JFactory::getDBO();

		if (version_compare(JVERSION, "1.6.0", 'ge'))  {
			//$extension	= 'com_jevlocations2';
			//$column = 'extension';
			$section = 'com_jevlocations2';
			$column = 'section';
		}
		else {
			$section = 'com_jevlocations2';
			$column = 'section';
		}
		
		$class		= $node->attributes('class');
		if (!$class) {
			$class = "inputbox";
		}

		$query = 'SELECT c.id, c.title as ctitle,p.title as ptitle, gp.title as gptitle, ggp.title as ggptitle, ' .
				' CASE WHEN CHAR_LENGTH(p.title) THEN CONCAT_WS(" => ", p.title, c.title) ELSE c.title END as title'.
				' FROM #__jevlocation_categories AS c' .
				' LEFT JOIN #__jevlocation_categories AS p ON p.id=c.parent_id' .
				' LEFT JOIN #__jevlocation_categories AS gp ON gp.id=p.parent_id ' .
				' LEFT JOIN #__jevlocation_categories AS ggp ON ggp.id=gp.parent_id ' .
				//' LEFT JOIN #__jevlocation_categories AS gggp ON gggp.id=ggp.parent_id ' .
				' WHERE c.published = 1 ';
				
				if (version_compare(JVERSION, "1.6.0", 'ge'))  {
					//$query .= ' AND c.extension = '.$db->Quote($extension);
					$query .= ' AND c.section = '.$db->Quote($section);
				}
				else {
					$query .= ' AND c.section = '.$db->Quote($section);
				}				
				
				$query .= ' ORDER BY c.' . $column . ', ggptitle, gptitle, ptitle, ctitle ';
				

		$db->setQuery($query);
		$options = $db->loadObjectList();
		echo $db->getErrorMsg();
		foreach ($options as $key=>$option) {
			$title = $option->ctitle;
			if (!is_null($option->ptitle)){
				$title = $option->ptitle."=>".$title;
			}
			if (!is_null($option->gptitle)){
				$title = $option->gptitle."=>".$title;
			}
			if (!is_null($option->ggptitle)){
				$title = $option->ggptitle."=>".$title;
			}
			$options[$key]->title = $title;
			// J16
			$options[$key]->value = $option->id;
			$options[$key]->text = $title;
		}
		JArrayHelper::sortObjects($options,"title");

		if ($raw) {
			if (version_compare(JVERSION, "1.6.0", 'ge'))  {
				array_unshift($options, JHTML::_("select.option","",JText::_("COM_JEVLOCATIONS_NO_CATEGORY"),"value","text"));
			}
			return $options;
		} else {		
			array_unshift($options, JHTML::_("select.option","0",JText::_("COM_JEVLOCATIONS_NO_CATEGORY"),"id","title"));
			return JHTML::_('select.genericlist',  $options, ''.$control_name.'['.$name.'][]', 'class="'.$class.'" multiple="multiple" size="4"', 'id', 'title', $value, $control_name.$name );
		}
	}	
*/
}