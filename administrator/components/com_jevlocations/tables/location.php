<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

/**
* Location Table class
*
*/
class TableLocation extends JTable
{
	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $loc_id = null;

	/**
	 * @var string
	 */
	var $title = null;

	/**
	 * @var string
	 */
	var $alias = null;

	/**
	 * @var string
	 */
	var $catid = null;

	/**
	 * @var string
	 */
	var $loccat = null;

	/**
	 * @var string
	 */
	var $description = null;

	/**
	 * @var string
	 */
	var $street = null;

	/**
	 * @var string
	 */
	var $city = null;

	/**
	 * @var string
	 */
	var $state= null;

	/**
	 * @var string
	 */
	var $country= null;

	/**
	 * @var string
	 */
	var $postcode = null;

	/**
	 * @var int
	 */
	var $geolon = null;

	/**
	 * @var int
	 */
	var $geolat = null;

	/**
	 * @var int
	 */
	var $geozoom = null;

	/**
	 * @var int
	 */
	var $priority = 0;

	/**
	 * @var string
	 */
	var $image = null;

	/**
	 * @var string
	 */
	var $imagetitle = null;

	/**
	 * @var string
	 */
	var $phone = null;

	/**
	 * @var string
	 */
	var $url = null;

	/**
	 * @var int
	 */
	var $ordering = null;

	/**
	 * @var int
	 */
	var $targetmenu = 0;

	/**
	 * @var int
	 */
	var $access = 1;

	/**
	 * @var int
	 */
	var $global = null;

	/**
	 * @var int
	 * published on by default
	 */
	var $published = 1;

	/**
	 * @var datetime
	 */
	var $created = null;

	/**
	 * @var int
	 */
	var $created_by = null;

	/**
	 * @var string
	 */
	var $created_by_alias = null;

	/**
	 * @var int
	 */
	var $modified_by = null;

	/**
	 * @var boolean
	 */
	var $checked_out = 0;

	/**
	 * @var time
	 */
	var $checked_out_time = 0;

	/**
	 * @var string
	 */
	var $anonname = null;

	/**
	 * @var string
	 */
	var $anonemail = null;

	/**
	 * @var string
	 */
	var $mapicon = null;

	/**
	 * @var string
	 */
	var $params = null;

	/**
	 * @var int
	 */
	var $overlaps = 0;

	/**
	 * @var string
	 */
	var $catid_list = null;

	/**
	 * @var string
	 */
	var $groupusage = 0;

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 * @since 1.0
	 */
	function __construct() {
		$db = JFactory::getDBO();
		parent::__construct('#__jev_locations', 'loc_id', $db);

		$params = JComponentHelper::getParams("com_jevlocations");
		$this->geozoom = $params->get("zoom",10);
		$this->geolon = $params->get("long",-3.6);
		$this->geolat = $params->get("lat",-5.3);
	}

	/**
	* Overloaded bind function
	*
	* @acces public
	* @param array $hash named array
	* @return null|string	null is operation was satisfactory, otherwise returns an error
	* @see JTable:bind
	* @since 1.5
	*/
	function bind($array, $ignore = '')
	{
		if (key_exists( 'params', $array ) && is_array( $array['params'] ))
		{
			$registry = new JRegistry();
			$registry->loadArray($array['params']);
			$array['params'] = $registry->toString();
		}
		// convert GPS co-ordinates
		if (key_exists( 'geolat', $array ) && strpos(trim($array['geolat']), " " )>0)
		{
			$array['geolat'] = str_replace("  "," ",trim($array['geolat']));
			$array['geolat'] = str_replace("  "," ",trim($array['geolat']));
			$parts = explode(" ",$array['geolat']);
			// D M S
			if (count ($parts)==3){
				$parts[1] += $parts[2] / 60;
			}
			/*
			else {
				$parts = explode(" ",$array['geolat']);
				// D M.S
				if (count ($parts)==3){
					$parts[1] += $parts[2] / 60;
				}
			}
			 */
			$array['geolat'] = $parts[0] + $parts[1] / 60;
		}
		if (key_exists( 'geolon', $array ) && strpos(trim($array['geolon']), " ")>0)
		{
			$array['geolon'] = str_replace("  "," ",trim($array['geolon']));
			$array['geolon'] = str_replace("  "," ",trim($array['geolon']));
			$parts = explode(" ",$array['geolon']);
			// D M S
			if (count ($parts)==3){
				$parts[1] += $parts[2] / 60;
			}
			$array['geolon'] = $parts[0] + $parts[1] / 60;
		}

		if ((key_exists('custom_anonusername', $array) && $array['custom_anonusername'] != "")
			&& (key_exists('custom_anonemail', $array) && $array['custom_anonemail'] != ""))
		{
			$array['anonname'] = $array['custom_anonusername'];
			$array['anonemail'] = $array['custom_anonemail'];
		}
		return parent::bind($array, $ignore);
	}

	/**
	 * Overloaded check method to ensure data integrity
	 *
	 * @access public
	 * @return boolean True on success
	 * @since 1.0
	 */
	function check()
	{

		/** check for valid name */
		if (trim($this->title) == '') {
			$this->setError(JText::_('YOUR_LOCATION_MUST_CONTAIN_A_TITLE'));
			return false;
		}

		/** check for existing name */
		$query = 'SELECT loc_id FROM #__jev_locations WHERE title = '.$this->_db->Quote($this->title).' AND street = '.$this->_db->Quote($this->street)
		.' AND catid = '.$this->_db->Quote($this->catid);
		$this->_db->setQuery($query);

		$xid = intval($this->_db->loadResult());
		if ($xid && $xid != intval($this->loc_id)) {
			$this->title = $this->title. " (Copy)";
			if(!empty($this->alias))
			{
				$this->alias = $this->alias."-copy";
			}
		}

		if(empty($this->alias)) {
			$this->alias = $this->title;
		}
		$this->alias =  JApplication::stringURLSafe($this->alias);
		if(trim(str_replace('-','',$this->alias)) == '') {
			$datenow = JFactory::getDate();
			$this->alias = $datenow ->format("Y-m-d-H-i-s");
		}

		return true;
	}

	function store( $updateNulls=false ) {

		$user = JFactory::getUser();
		if (is_null($this->created_by)){
			$this->created_by = $user->id;
		}
		else {
			$this->modified_by = $user->id;
		}
		if (is_null($this->created)){
			$datenow = JFactory::getDate();
			$this->created	= $datenow->toSql();
		}
		if ($this->loc_id>0){
			$datenow = JFactory::getDate();
			$this->modified = $datenow->toSql();
		}
		return parent::store($updateNulls);
	}

}
