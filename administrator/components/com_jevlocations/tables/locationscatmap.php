<?php
/**
 * copyright (C) 2014-2015 GWE Systems Ltd - All rights reserved
 */

// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

/**
* Location Table class
*
*/
class TableLocationscatmap extends JTable
{
	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $locid = null;

	/**
	 * Primary Key
	 *
	 * @var int
	 */
	var $catid = null;

	/**
	 * Constructor
	 *
	 * @param object Database connector object
	 * @since 1.0
	 */
	function __construct() {
		$db = JFactory::getDBO();
		parent::__construct('#__jevlocations_catmap', 'locid,catid', $db);

	}

	/**
	 * Method to store a row in the database from the JTable instance properties.
	 * If a primary key value is set the row with that primary key value will be
	 * updated with the instance property values.  If no primary key value is set
	 * a new row will be inserted into the database with the properties from the
	 * JTable instance.
	 *
	 * @param   boolean  $updateNulls  True to update fields even if they are null.
	 *
	 * @return  boolean  True on success.
	 *
	 * @link	http://docs.joomla.org/JTable/store
	 * @since   11.1
	 */
	public function store($updateNulls = false)
	{
		if (version_compare(JVERSION, '3.0', "<"))
		{
			// Initialise variables.
			$k = $this->_tbl_key;
			if (!empty($this->asset_id))
			{
				$currentAssetId = $this->asset_id;
			}

			// The asset id field is managed privately by this class.
			if ($this->_trackAssets)
			{
				unset($this->asset_id);
			}


			//We always clean categories before saving new categories, so we do not need primary key.
			$stored = $this->_db->insertObject($this->_tbl, $this, $this->_tbl_key);
		

			// If the store failed return false.
			if (!$stored)
			{
				$e = new JException(JText::sprintf('JLIB_DATABASE_ERROR_STORE_FAILED', get_class($this), $this->_db->getErrorMsg()));
				$this->setError($e);
				return false;
			}

			// If the table is not set to track assets return true.
			if (!$this->_trackAssets)
			{
				return true;
			}

			if ($this->_locked)
			{
				$this->_unlock();
			}

			//
			// Asset Tracking
			//

			$parentId = $this->_getAssetParentId();
			$name = $this->_getAssetName();
			$title = $this->_getAssetTitle();

			$asset = JTable::getInstance('Asset', 'JTable', array('dbo' => $this->getDbo()));
			$asset->loadByName($name);

			// Re-inject the asset id.
			$this->asset_id = $asset->id;

			// Check for an error.
			if ($error = $asset->getError())
			{
				$this->setError($error);
				return false;
			}

			// Specify how a new or moved node asset is inserted into the tree.
			if (empty($this->asset_id) || $asset->parent_id != $parentId)
			{
				$asset->setLocation($parentId, 'last-child');
			}

			// Prepare the asset to be stored.
			$asset->parent_id = $parentId;
			$asset->name = $name;
			$asset->title = $title;

			if ($this->_rules instanceof JAccessRules)
			{
				$asset->rules = (string) $this->_rules;
			}

			if (!$asset->check() || !$asset->store($updateNulls))
			{
				$this->setError($asset->getError());
				return false;
			}

			// Create an asset_id or heal one that is corrupted.
			if (empty($this->asset_id) || ($currentAssetId != $this->asset_id && !empty($this->asset_id)))
			{
				// Update the asset_id field in this table.
				$this->asset_id = (int) $asset->id;

				$query = $this->_db->getQuery(true);
				$query->update($this->_db->quoteName($this->_tbl));
				$query->set('asset_id = ' . (int) $this->asset_id);
				$query->where($this->_db->quoteName($k) . ' = ' . (int) $this->$k);
				$this->_db->setQuery($query);

				if (!$this->_db->execute())
				{
					$e = new JException(JText::sprintf('JLIB_DATABASE_ERROR_STORE_FAILED_UPDATE_ASSET_ID', $this->_db->getErrorMsg()));
					$this->setError($e);
					return false;
				}
			}

			return true;
		}
		else
		{
			parent::store($updateNulls);
		}
	}
}
