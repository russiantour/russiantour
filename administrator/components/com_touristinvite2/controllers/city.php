<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * City controller class.
 */
class TouristinviteControllerCity extends JControllerForm
{

    function __construct() {
        $this->view_list = 'citys';
        parent::__construct();
    }

    public function save($key,$urlVar) {

        $db = JFactory::getDbo();

        $query = 'SELECT * FROM #__touristinvite_hotels'
            . ' WHERE id = ' . (int) $_POST['jform']['id'];

        $db->setQuery($query);
        $name = $db->loadObject()->hotel_city;

        $query = 'UPDATE #__touristinvite_hotels'
            . ' SET hotel_city = \''.$_POST['jform']['hotel_city'].'\','
            . 'hotel_city_rus = \''.$_POST['jform']['hotel_city_rus'].'\''
            . ' WHERE hotel_city = \'' . $name.'\'';

        $db->setQuery($query);
        $db->execute();

        parent::save($key,$urlVar);
    }
}