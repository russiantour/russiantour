<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Invite controller class.
 */
class TouristinviteControllerInvite extends JControllerForm
{

    function __construct() {
        if ($_POST['forserialize']) {
            $_POST['jform']['invite_data'] = serialize($_POST['forserialize']);
        }

        $this->view_list = 'invites';
        parent::__construct();
    }

    public function showdoc() {
        $model = $this->getModel('Invite', 'TouristinviteModel');
        $model->showdoc($_GET['inviteid']);
    }
}