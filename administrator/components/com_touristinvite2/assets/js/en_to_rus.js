jQuery(document).ready(function (){
    jQuery('#jform_hotel_city, #jform_hotel_name').on('keyup change',function(){
        jQuery('#jform_hotel_city_rus, #jform_hotel_name_rus').val(en_to_rus(jQuery(this).val()));
    });
});

function en_to_rus (txt) {
    txt = txt.toLowerCase();
    var tr_rules1 = {'a':'а','b':'б','v':'в','g':'г','d':'д','e':'е','j':'ж','z':'з','i':'и','y':'й','k':'к','l':'л','m':'м','n':'н','o':'о','p':'п','r':'р','s':'с','t':'т','u':'у','f':'ф','h':'х','c':'ц','w':'в','q':'кв'};
    var tr_rules2 = {'yo':'ё','ch':'ч','sh':'ш','yu':'ю','ya':'я'};
    var tr_rules4 = {'shch':'щ'};

    for (var p in tr_rules4)
    {
        var expr = new RegExp(p,'g');
        txt = txt.replace (expr, tr_rules4[p]);
    }

    for (var p in tr_rules2)
    {
        var expr = new RegExp(p,'g');
        txt = txt.replace (expr, tr_rules2[p]);
    }

    for (var p in tr_rules1)
    {
        var expr = new RegExp(p,'g');
        txt = txt.replace (expr, tr_rules1[p]);
    }

    return txt;
}