<?php
/**
 * Convert HTML to MS Word file for PHP 4.2.x or earlier
 * @author Dale Attree
 * @version 1.0.1
 * @name HTML_TO_DOC
 */
 
/**
 * Convert HTML to MS Word file
 * @author Harish Chauhan
 * @version 1.0.0
 * @name HTML_TO_DOC
 */

class HTML_TO_DOC
{
	var $docFile="";
	var $title="";
	var $htmlHead="";
	var $htmlBody="";
	
	/**
	 * Constructor
	 *
	 * @return void
	 */
	function HTML_TO_DOC()
	{
		$this->title="Untitled Document";
		$this->htmlHead="";
		$this->htmlBody="";
	}
	
	/**
	 * Set the document file name
	 *
	 * @param String $docfile 
	 */
	
	function setDocFileName($docfile)
	{
		//echo 'setDocFileName Entered.<br>';
		$this->docFile=$docfile;
		if(!preg_match("/\.doc$/i",$this->docFile))
			$this->docFile.=".doc";
		return;     
	}
	
	function setTitle($title)
	{
		echo 'setTitle Entered.<br>';
		$this->title=$title;
	}
	
	/**
	 * Return header of MS Doc
	 *
	 * @return String
	 */
	function getHeader()
	{
		//echo 'getHeader Entered.<br>';
		$return  = <<<EOH
		 <html xmlns:v="urn:schemas-microsoft-com:vml"
		xmlns:o="urn:schemas-microsoft-com:office:office"
		xmlns:w="urn:schemas-microsoft-com:office:word"
		xmlns="http://www.w3.org/TR/REC-html40">
		
		<head>
		<meta http-equiv=Content-Type content="text/html; charset=utf-8">
		<meta name=ProgId content=Word.Document>
		<meta name=Generator content="Microsoft Word 9">
		<meta name=Originator content="Microsoft Word 9">
		<!--[if !mso]>
		<style>
		v\:* {behavior:url(#default#VML);}
		o\:* {behavior:url(#default#VML);}
		w\:* {behavior:url(#default#VML);}
		.shape {behavior:url(#default#VML);}
		</style>
		<![endif]-->
		<title>$this->title</title>
		<!--[if gte mso 9]><xml>
		 <w:WordDocument>
		  <w:View>Print</w:View>
		  <w:DoNotHyphenateCaps/>
		  <w:PunctuationKerning/>
		  <w:DrawingGridHorizontalSpacing>9.35 pt</w:DrawingGridHorizontalSpacing>
		  <w:DrawingGridVerticalSpacing>9.35 pt</w:DrawingGridVerticalSpacing>
		 </w:WordDocument>
		</xml><![endif]-->
		<style>
		<!--
		 /* Font Definitions */
		@font-face
			{font-family:Verdana;
			panose-1:2 11 6 4 3 5 4 4 2 4;
			mso-font-charset:0;
			mso-generic-font-family:swiss;
			mso-font-pitch:variable;
			mso-font-signature:536871559 0 0 0 415 0;}
		 /* Style Definitions */
		p.MsoNormal, li.MsoNormal, div.MsoNormal
			{mso-style-parent:"";
			margin:0in;
			margin-bottom:.0001pt;
			mso-pagination:widow-orphan;
			font-size:7.5pt;
				mso-bidi-font-size:8.0pt;
			font-family:"Verdana";
			mso-fareast-font-family:"Verdana";}
		p.small
			{mso-style-parent:"";
			margin:0in;
			margin-bottom:.0001pt;
			mso-pagination:widow-orphan;
			font-size:1.0pt;
				mso-bidi-font-size:1.0pt;
			font-family:"Verdana";
			mso-fareast-font-family:"Verdana";}
		@page Section1
			{size:8.5in 11.0in;
			margin:1.5cm 1.5cm 1.5cm 1.5cm;
			mso-header-margin:.5in;
			mso-footer-margin:.5in;
			mso-paper-source:0;}
		div.Section1
			{page:Section1;}
		-->
		</style>
		<!--[if gte mso 9]><xml>
		 <o:shapedefaults v:ext="edit" spidmax="1032">
		  <o:colormenu v:ext="edit" strokecolor="none"/>
		 </o:shapedefaults></xml><![endif]--><!--[if gte mso 9]><xml>
		 <o:shapelayout v:ext="edit">
		  <o:idmap v:ext="edit" data="1"/>
		 </o:shapelayout></xml><![endif]-->
		 $this->htmlHead
		</head>
		<body>
EOH;
	return $return;
	}
	
	/**
	 * Return Document footer
	 *
	 * @return String
	 */
	function getFotter()
	{
		//echo 'getFotter Entered.<br>';
		return "</body></html>";
	}
	
	/**
	 * Create The MS Word Document from given HTML
	 *
	 * @param String $html :: URL Name like http://www.example.com
	 * @param String $file :: Document File Name
	 * @param Boolean $download :: Wheather to download the file or save the file
	 * @return boolean 
	 */
	
	function createDocFromURL($url,$file,$download=false)
	{
		echo 'createDocFromURL Entered.<br>';
		if(!preg_match("/^http:/",$url))
			$url="http://".$url;
		$f = fopen($url,'rb');
		while(!feof($f)){
			$html= fread($f,8192);
		}
		return $this->createDoc($html,$file,$download); 
	}

	/**
	 * Create The MS Word Document from given HTML
	 *
	 * @param String $html :: HTML Content or HTML File Name like path/to/html/file.html
	 * @param String $file :: Document File Name
	 * @param Boolean $download :: Wheather to download the file or save the file
	 * @return boolean 
	 */
	
	function createDoc($html,$file,$download=false)
	{
		//echo 'createDoc Entered.<br>';
		if(is_file($html))
			$html=@file_get_contents($html);
		
		$this->_parseHtml($html);
		$this->setDocFileName($file);
		$doc=$this->getHeader();
		$doc.=$this->htmlBody;
		$doc.=$this->getFotter();
						
		if($download)
		{
			//$this->write_file($this->docFile,$doc);
			@header("Cache-Control: ");// leave blank to avoid IE errors
			@header("Pragma: ");// leave blank to avoid IE errors
			@header("Content-type: application/octet-stream");
			//@header("Content-type: application/vnd.ms-word");
			@header("Content-Disposition: attachment; filename=\"$this->docFile\"");
			echo $doc;
			return true;
		}
		else 
		{
			return $this->write_file($this->docFile,$doc);
		}
	}
	
	/**
	 * Parse the html and remove <head></head> part if present into html
	 *
	 * @param String $html
	 * @return void
	 * @access Private
	 */
	
	function _parseHtml($html)
	{
		//echo '_parseHtml Entered.<br>';
		$html=preg_replace("/<!DOCTYPE((.|\n)*?)>/ims","",$html);
		$html=preg_replace("/<script((.|\n)*?)>((.|\n)*?)<\/script>/ims","",$html);
		preg_match("/<head>((.|\n)*?)<\/head>/ims",$html,$matches);
		$head=$matches[1];
		preg_match("/<title>((.|\n)*?)<\/title>/ims",$head,$matches);
		$this->title = $matches[1];
		$html=preg_replace("/<head>((.|\n)*?)<\/head>/ims","",$html);
		$head=preg_replace("/<title>((.|\n)*?)<\/title>/ims","",$head);
		$head=preg_replace("/<\/?head>/ims","",$head);
		$html=preg_replace("/<\/?body((.|\n)*?)>/ims","",$html);
		$this->htmlHead=$head;
		$this->htmlBody=$html;
		return;
	}
	
	/**
	 * Write the content int file
	 *
	 * @param String $file :: File name to be save
	 * @param String $content :: Content to be write
	 * @param [Optional] String $mode :: Write Mode
	 * @return void
	 * @access boolean True on success else false
	 */
	
	function write_file($file,$content,$mode="w")
	{
		//echo 'write_file entered!<br>';
		$fp=@fopen($file,$mode);
		if(!is_resource($fp)){
			return false;
		}
		fwrite($fp,$content);
		fclose($fp);
		return true;
	}

}
$id = isset($table->pdf_id)?$table->pdf_id:$this->item->pdf_id;
$created = isset($table->invite_created)?$table->invite_created:$this->item->invite_created;
$created = date('d/m/Y',strtotime($created));
$html='
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body>
<div class=Section1>
<img width=151 height=88 src="http://'.$_SERVER['HTTP_HOST'].'/components/com_touristinvite/assets/php/pdf/logo3.png" />
<div style="font-family:Arial;font-size:11pt;"><b><div style="text-align:center;">ПОДТВЕРЖДЕНИЕ № '.$id.'<br>
о приеме иностранного туриста</div><br></b>
<style>.first td {border:none;text-align: left;}</style>
<table class="first" width="100%">
    <tr>
        <td width="20%"><b>Кратность визы:</b></td>
        <td colspan="3">въездная-выездная однократная</td>
    </tr>
    <tr>
        <td width="20%"><b>Въезд с:</b></td>
        <td width="30%">'.$data['invite_from'].'</td>
        <td width="10%"><b>Выезд:</b></td>
        <td>'.$data['invite_to'].'</td>
    </tr>
</table>
<style>
    table {border-collapse: collapse;width: 100%}
    table td {border:1px solid black;text-align: center}
</style>
<table>
	<tr style="font-size:12pt">
		<td width="17%"><b>Фамилия</b></td>
		<td width="17%"><b>Имена</b></td>
		<td width="16%"><b>Дата<br>рождения</b></td>
		<td width="17%"><b>Пол</b></td>
		<td width="17%"><b>Гражданство</b></td>
		<td width="16%"><b>Номер<br>паспорта</b></td>
	</tr>';
$names = array();
for($i=1;$i<=$data['people_count'];$i++)
{
$html .= '	<tr>
		<td>'.$data['last_name_'.$i].'</td>
		<td>'.$data['first_name_'.$i].'</td>
		<td>'.$data['birthdate_'.$i].'</td>
		<td>'.str_replace(array('m','f'),array('МУЖСКОЙ','ЖЕНСКИЙ'),$data['sex_'.$i]).'</td>
		<td>'.$data['gragd_'.$i].'</td>
		<td>'.$data['passport_'.$i].'</td>
	</tr>';
$names[] = $data['last_name_'.$i].' '.$data['first_name_'.$i];
}
$htls_rus = array();
$htls_en = array();
$cities_rus = array();
$cities_en = array();
foreach ($data['invite_hotels'] as $v)
{
    $expl = explode('|',$v);
    $htls_rus[] = 'Отель «'.$expl[1].'» ('.$expl[3].')';
    $htls_en[] = 'Hotel «'.$expl[0].'» ('.$expl[2].')';
}
foreach ($data['invite_cities'] as $v)
{
    $expl = explode('|',$v);
    $cities_rus[] = $expl[1];
    $cities_en[] = $expl[0];
}
$html .= '
</table>
<br>
<b>Цель поездки:</b> Туризм<br>
<b>Маршрут и место размещения:</b><br>
'.implode(', ',$htls_rus).'<br><br>
<b>Принимающая организация, адрес, No референса:</b><br>
Общество с ограниченной ответственностью Международная Компания «Русский Тур»,
референс номер 012877, Санкт-Петербург, 194044, Финляндский проспект 4, литер А, офис 424
<br><br><br><br>
<b>Подпись:</b><br>
<style>
    .table2 {width: auto}
    .table2 td {border:none;}
    .table-footer {font-family: tahoma;font-size: 8pt;width:100%;}
    .table-footer td {border: none}
</style>
<table class="table2" >
    <tr>
        <td>
            <img width="183" height="185" style="" src="http://'.$_SERVER['HTTP_HOST'].'/components/com_touristinvite/assets/php/pdf/timbro_cropped.png">
        </td>
        <td style="vertical-align: top;"><div style="margin-top: 65pt;font-size: 11pt;font-family: arial;">'.$created.'</div></td>
        <td style="vertical-align: top">
            <img width="121" height="64" src="http://'.$_SERVER['HTTP_HOST'].'/components/com_touristinvite/assets/php/pdf/podpis.png">
        </td>
        <td style="vertical-align: top;"><div style="margin-top: 25pt;font-family: tahoma; font-size:8pt;">/Черемшенко О.Н./<br>Ген.Директор</div></td>
    </tr>
</table>

<br><br>
<hr>
<table class="table-footer">
    <tr>
        <td style="text-align: left">Russian Tour International LTD</td>
        <td style="text-align: right">Tel. +7 812 647 0690</td>
    </tr>
    <tr>
        <td style="text-align: left">194044, St. Petersburg, Russia 4a, Finlyandskiy Prospekt, off. 424</td>
        <td style="text-align: right">Fax +7 812 647 0690</td>
    </tr>
</table>
</div>
</div>
<?php ////////// next page ///////////////////// ?>
<br clear=all style="mso-special-character:line-break; page-break-before:always">
<?php ////////// next page ///////////////////// ?>
<style>.secondPage td {border:none;text-align: left;font-family:Arial;font-size:11pt;margin-top: 10pt;}</style>
<div class="Section1">
<img width=151 height=88 src="http://'.$_SERVER['HTTP_HOST'].'/components/com_touristinvite/assets/php/pdf/logo3.png" />
<div style="font-family:Arial;font-size:11pt;"><b><div style="text-align:center;">Voucher № '.$id.'<br>
Ваучер № '.$id.'<br>
на оказание туристических услуг</div><br>
<table class="secondPage">
    <tr>
        <td width="20%"><b>Date of entry:</b></td>
        <td width="50%">'.$data['invite_from'].'</td>
        <td width="20%"><b>Date of exit:</b></td>
        <td>'.$data['invite_to'].'</td>
    </tr>
    <tr>
        <td width="20%"><b>Въезд с:</b></td>
        <td width="50%">'.$data['invite_from'].'</td>
        <td width="20%"><b>Выезд:</b></td>
        <td>'.$data['invite_to'].'</td>
    </tr>
    <tr>
        <td width="20%"><b>Name:</b></td>
        <td colspan="3">'.implode(', ',$names).'</td>
    </tr>
    <tr>
        <td width="20%"><b>Object of journey:</b></td>
        <td colspan="3">Tourism</td>
    </tr>
    <tr>
        <td width="20%"><b>Цель поездки:</b></td>
        <td colspan="3">Туризм</td>
    </tr>
    <tr>
        <td width="20%"><b>Hotel:</b></td>
        <td colspan="3">'.implode(', ',$htls_en).'</td>
    </tr>
    <tr>
        <td width="20%"><b>Размещение:</b></td>
        <td colspan="3">'.implode(', ',$htls_rus).'</td>
    </tr>
    <tr>
        <td width="20%"><b>Programme:</b></td>
        <td colspan="3">City tour in '.implode(', ',$cities_en).'</td>
    </tr>
    <tr>
        <td width="20%"><b>Программа:</b></td>
        <td colspan="3">Экскурсионное обслуживание в городах: '.implode(', ',$cities_rus).'</td>
    </tr>
</table>
<table class="secondPage" width="100%">
    <tr>
        <td style="vertical-align: middle" width="50%">
            <b>Receiving company, address:</b><br>
            <b>Принимающая организация, адрес:</b>
        </td>
        <td>Общество с ограниченной ответственностью
Международная Компания «Русский Тур», референс
номер 012877, Санкт-Петербург, 194044
Финляндский проспект 4, литер А, офис 424</td>
    </tr>
</table><br><br>
<b>Ministry of Foreign Affairs Index No 012877</b><br><br>
<br>
<br>
<style>
    .table2 {width: auto}
    .table2 td {border:none;}
    .table-footer {font-family: tahoma;font-size: 8pt;width:100%;}
    .table-footer td {border: none}
</style>
<table class="table2" >
    <tr>
        <td>
            <img width="183" height="185" style="" src="http://'.$_SERVER['HTTP_HOST'].'/components/com_touristinvite/assets/php/pdf/timbro_cropped.png">
        </td>
        <td style="vertical-align: top;">
            <div style="margin-top: 25pt;"><b>Signature:</b></div>
            <div style="margin-top: 15pt;font-size: 11pt;font-family: arial;">'.$created.'</div>
        </td>
        <td style="vertical-align: top">
            <img width="121" height="64" src="http://'.$_SERVER['HTTP_HOST'].'/components/com_touristinvite/assets/php/pdf/podpis.png">
        </td>
        <td style="vertical-align: top;">
            <div style="margin-top: 25pt;font-family: tahoma; font-size:8pt;">/Cheremshenko O.N./<br>General Manager</div>
        </td>
    </tr>
</table>

<br><br>
<hr>
<table class="table-footer">
    <tr>
        <td style="text-align: left">Russian Tour International LTD</td>
        <td style="text-align: right">Tel. +7 812 647 06 90</td>
    </tr>
    <tr>
        <td style="text-align: left">194044, St. Petersburg, Russia 4a, Finlyandskiy Prospekt, off. 424</td>
        <td style="text-align: right">Fax +7 812 647 06 90</td>
    </tr>
</table>
</div>
</div>
</body>
</html>
';
if ($_GET['html'])
    exit('<a href="doc.php">word</a>'.$html);
$go = new HTML_TO_DOC;
$go->createDoc($html,'invite',true);
