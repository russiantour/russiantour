CREATE TABLE IF NOT EXISTS `#__touristinvite_hotels` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`hotel_name` VARCHAR(255)  NOT NULL ,
`hotel_name_rus` VARCHAR(255)  NOT NULL ,
`hotel_city` VARCHAR(255)  NOT NULL ,
`hotel_city_rus` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;

CREATE TABLE IF NOT EXISTS `#__touristinvite_invites` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`pdf_id` INT(11)  NOT NULL ,
`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`invite_created` datetime NOT NULL,
`invite_from` datetime NOT NULL,
`invite_to` datetime NOT NULL,
`invite_data` text NOT NULL,
PRIMARY KEY (`id`)
)
DEFAULT COLLATE=utf8_general_ci
AUTO_INCREMENT = 2014;

CREATE TABLE IF NOT EXISTS `#__touristinvite_turfirms` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
`created_by` INT(11)  NOT NULL ,
`turfirm_user_id` int(11) DEFAULT NULL,
`turfirm_cost` float DEFAULT NULL,
`turfirm_balance` float DEFAULT NULL,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8_general_ci;