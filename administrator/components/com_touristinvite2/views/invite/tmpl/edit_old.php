<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_touristinvite/assets/css/touristinvite.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'invite.cancel') {
            Joomla.submitform(task, document.getElementById('invite-form'));
        }
        else {
            
            if (task != 'invite.cancel' && document.formvalidator.isValid(document.id('invite-form'))) {
                
                Joomla.submitform(task, document.getElementById('invite-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_touristinvite&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="invite-form" class="form-validate">
    <div id="testing"></div>
    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_TOURISTINVITE_TITLE_INVITE', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">

                <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>			<div class="control-group">
				<div class="control-label"><?php echo $this->form->getLabel('invite_data'); ?></div>
				<div class="controls"><?php echo $this->form->getInput('invite_data'); ?></div>
                        <?

                        foreach (unserialize($this->item->invite_data) as $k=>$v)
                        {
                            if (gettype($v)=='array')
                            {
                                ?><div class="control-label"><label><?php echo $k; ?></label></div><?
                                ?><div class="controls"><?
                                echo '<select multiple="" name="forserialize[invite_hotels][]" style="display: none;">';

                                foreach ($v as $v1)
                                {
                                    echo '<option value="'.htmlspecialchars($v1).'" selected>'.$v1.'</option>';
                                }

                                foreach ($this->cities as $v1)
                                {

                                    $v1 = JText::_('COM_TOURISTINVITE_TITLE_HOTEL').' "'.$v1->hotel_name.'" ('.$v1->hotel_city.')';
                                    if (!in_array($v1,$v))
                                        echo '<option value="'.htmlspecialchars($v1).'">'.$v1.'</option>';
                                }
                                echo '</select></div>';
                            }
                            else
                            {
                                $readonly = '';
                                $arr_readonly = array('old_balance','new_balance','cost');
                                $arr_date = array('invite_from','invite_to');
                                if (in_array($k,$arr_readonly))
                                    $readonly = 'readonly';

                                ?><div class="control-label"><label><?php echo $k; ?></label></div><?
                                ?><div class="controls">
                                <?php if (in_array($k,$arr_date) || strpos($k,'birthdate')!==false) {
                                    echo JHtml::_('calendar', str_replace('/','.',$v), "forserialize[".$k."]", "forserialize[".$k."]", '%d/%m/%Y', array());
                                } else { ?>
                                <input <?php echo $readonly; ?> name="forserialize[<?php echo $k; ?>]" id="<?php echo $k; ?>" type="text" value="<?php echo $v; ?>">
                                <?php } ?>
                                </div><?
                            }
                        }
                        ?>
			</div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<style>
    input[name="invite_data"] {
        width: 100%;
    }
</style>