<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_touristinvite/assets/css/touristinvite.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'invite.cancel') {
            Joomla.submitform(task, document.getElementById('invite-form'));
        }
        else {
            
            if (task != 'invite.cancel' && document.formvalidator.isValid(document.id('invite-form'))) {
                
                Joomla.submitform(task, document.getElementById('invite-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_touristinvite&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="invite-form" class="form-validate">
    <div id="testing"></div>
    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_TOURISTINVITE_TITLE_INVITE', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">

                <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
				<input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
				<input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
				<input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
				<input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php }
                $invite_data = unserialize($this->item->invite_data)
                ?>
                <?php echo $this->form->getInput('pdf_id'); ?>
                <input type="hidden" name="forserialize[validate_date]" value="<?php echo $invite_data['validate_date']; ?>">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('invite_data'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('invite_data'); ?></div>

                    <div class="control-label"><label>invite_from</label></div>
                    <div class="controls">
                        <?php echo JHtml::_('calendar', str_replace('/','.',$invite_data['invite_from']), "forserialize[invite_from]", "forserialize[invite_from]", '%d/%m/%Y', array()); ?>
                    </div>

                    <div class="control-label"><label>invite_to</label></div>
                    <div class="controls">
                        <?php echo JHtml::_('calendar', str_replace('/','.',$invite_data['invite_to']), "forserialize[invite_to]", "forserialize[invite_to]", '%d/%m/%Y', array()); ?>
                    </div>
<?php //var_dump($invite_data,$this->cities); ?>
                    <div class="control-label"><label>Cities</label></div>
                    <div class="controls">

                        <select id="invite_cities" name="forserialize[invite_cities][]" multiple data-placeholder="Choose cities">
                            <?php foreach ($this->cities as $i => $item) :
                                $invite_cities_option = $item->hotel_city.'|'.$item->hotel_city_rus;
                                if (isset($invite_data) && in_array($invite_cities_option,$invite_data['invite_cities']))
                                    $sel = 'selected';
                                else
                                    $sel = '';
                                ?>
                                <option  value='<?php echo $invite_cities_option; ?>' <?php echo $sel; ?>><?php echo $item->hotel_city; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <br>
                    </div>
                    <div class="control-label"><label><?php echo JText::_('COM_TOURISTINVITE_TITLE_HOTELS').' (maximum 5)'; ?></label></div>
                    <div class="controls">
                        <select id="invite_hotels" name="forserialize[invite_hotels][]" multiple data-placeholder="Choose hotels">
                            <?php foreach ($this->hotels as $i => $item) :
                                $invite_hotels_option = $item->hotel_name.'|'.$item->hotel_name_rus.'|'.$item->hotel_city.'|'.$item->hotel_city_rus;
                                if (isset($invite_data) && in_array($invite_hotels_option,$invite_data['invite_hotels']))
                                    $sel = 'selected';
                                else
                                    $sel = '';
                                ?>
                                <option disabled id="<?php echo $item->hotel_city.'|'.$item->hotel_city_rus; ?>" value='<?php echo $invite_hotels_option; ?>' <?php echo $sel; ?>><?php echo $item->hotel_name.' ('.$item->hotel_city.')'; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <br>


                        <script>
                            function checkhotels(){
                                jQuery("#invite_hotels option").attr('disabled',true);
                                jQuery('#invite_cities').find('option:selected').each(function(){
                                    var city_value = jQuery(this).val();
                                    jQuery('[id="'+city_value+'"]:disabled').attr('disabled',false);
                                });
                                jQuery('#invite_hotels').find('option:selected').each(function(){
                                    var id = jQuery(this).attr('id');
                                    jQuery('#invite_hotels option[id="'+id+'"]:not(:selected)').attr('disabled',true);
                                });
                                jQuery("#invite_hotels").trigger("liszt:updated");
                            }
                            checkhotels();
                            jQuery('#invite_cities').on('change',checkhotels);
                            jQuery('#invite_hotels').on('change',checkhotels);
                        </script>
                        <?php /*<select multiple="" name="forserialize[invite_hotels][]">
                            <?php foreach ($invite_data['invite_hotels'] as $v1)
                            {
                                echo '<option value="'.htmlspecialchars($v1).'" selected>'.$v1.'</option>';
                            }

                            foreach ($this->cities as $v1)
                            {
                                $v1 = JText::_('COM_TOURISTINVITE_TITLE_HOTEL').' "'.$v1->hotel_name.'" ('.$v1->hotel_city.')';
                                if (!in_array($v1,$v))
                                echo '<option value="'.htmlspecialchars($v1).'">'.$v1.'</option>';
                            } ?>
                        </select>*/?>
                    </div>

                    <div class="control-label"><label>invite_to</label></div>
                    <div class="controls">
                        <input name="forserialize[people_count]" id="people_count" type="text" value="<?php echo $invite_data['people_count']; ?>">
                        <a id="apply">Apply</a>
                    </div>

                    <table id="guests">
                        <tr>
                            <td><?php echo JText::_('COM_TOURISTINVITE_GUESTS_FIRST_NAME'); ?></td>
                            <td><?php echo JText::_('COM_TOURISTINVITE_GUESTS_LAST_NAME'); ?></td>
                            <td><?php echo JText::_('COM_TOURISTINVITE_GUESTS_BIRTHDATE'); ?></td>
                            <td><?php echo JText::_('COM_TOURISTINVITE_GUESTS_CITIZENSHIP'); ?></td>
                            <td><?php echo JText::_('COM_TOURISTINVITE_GUESTS_SEX'); ?></td>
                            <td><?php echo JText::_('COM_TOURISTINVITE_GUESTS_PASPORT'); ?></td>
                        </tr>
                        <?php for ($i=1;$i<=$invite_data['people_count'];$i++) {
                            ?><tr>
                                <td><input type="text" name="forserialize[first_name_<?php echo $i; ?>]" value="<?php echo $invite_data['first_name_'.$i]; ?>"></td>
                                <td><input type="text" name="forserialize[last_name_<?php echo $i; ?>]" value="<?php echo $invite_data['last_name_'.$i]; ?>"></td>
                                <td>
                                    <?php echo JHtml::_('calendar', str_replace('/','.',$invite_data['birthdate_'.$i]), "forserialize[birthdate_".$i."]", "forserialize[birthdate_".$i."]", '%d/%m/%Y', array()); ?>
                                </td>
                                <td><input type="text" name="forserialize[gragd_<?php echo $i; ?>]" value="<?php echo $invite_data['gragd_'.$i]; ?>"></td>
                                <td>
                                    <select name="forserialize[sex_<?php echo $i; ?>]">
                                        <option <?php if ($invite_data['sex_'.$i]=='m') echo 'selected'; ?> value="m"><?php echo JText::_('COM_TOURISTINVITE_SEX_MALE'); ?></option>
                                        <option <?php if ($invite_data['sex_'.$i]=='f') echo 'selected'; ?> value="f"><?php echo JText::_('COM_TOURISTINVITE_SEX_FEMALE'); ?></option>
                                    </select>
                                </td>
                                <td><input type="text" name="forserialize[passport_<?php echo $i; ?>]" value="<?php echo $invite_data['passport_'.$i]; ?>"></td>
                            </tr><?php
                        } ?>
                    </table>

                    <div class="control-label"><label>old_balance</label></div>
                    <div class="controls">
                        <input type="text" value="<?php echo $invite_data['old_balance']; ?>" id="old_balance" name="forserialize[old_balance]" readonly="">
                    </div>

                    <div class="control-label"><label>new_balance</label></div>
                    <div class="controls">
                        <input type="text" value="<?php echo $invite_data['new_balance']; ?>" id="new_balance" name="forserialize[new_balance]" readonly="">
                    </div>

                    <div class="control-label"><label>cost</label></div>
                    <div class="controls">
                        <input type="text" value="<?php echo $invite_data['cost']; ?>" id="cost" name="forserialize[cost]" readonly="">
                    </div>

			    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        
        

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<div style="display: none;" id="birthdate_template">
    <?php echo JHtml::_('calendar', date('d.m.Y'), "forserialize[birthdate_[id]]", "forserialize[birthdate_[id]]", '%d/%m/%Y', array()); ?>
</div>
<style>
    input[name="invite_data"] {
        width: 100%;
    }
</style>
<script>
    var tr='<tr><td><input type="text" name="forserialize[first_name_[id]]"></td>';
    tr += '<td><input type="text" name="forserialize[last_name_[id]]"></td>';
    tr += '<td>'+jQuery('#birthdate_template').html()+'</td>';

    tr += '<td><input type="text" name="forserialize[gragd_[id]]"></td>';

    tr += '<td><select name="forserialize[sex_[id]]"><option value="m"><?php echo JText::_('COM_TOURISTINVITE_SEX_MALE'); ?></option>';
    tr += '<option value="f"><?php echo JText::_('COM_TOURISTINVITE_SEX_FEMALE'); ?></option></select></td>';

    tr += '<td><input type="text" name="forserialize[passport_[id]]"></td></tr>';

    jQuery('#apply').on('click',function(){
        jQuery('table#guests').show();
        var length = jQuery('table#guests tr').length;
        var int = parseInt(jQuery('input#people_count').val());
        if (length > (int+1))
            for (i=length-1;i>int;i--)
                jQuery('table#guests tr:eq('+i+')').remove();
        else if (length-1 < int)
            for(i=length;i<int+1;i++)
            {
                var ntr = tr.replace(/\[id\]/g,i);
                jQuery('table#guests').append(ntr);

                Calendar.setup({
                    // Id of the input field
                    inputField: "forserialize[birthdate_"+i+"]",
                    // Format of the input field
                    ifFormat: "%d/%m/%Y",
                    // Trigger for the calendar (button ID)
                    button: "forserialize[birthdate_"+i+"]_img",
                    // Alignment (defaults to "Bl")
                    align: "Tl",
                    singleClick: true,
                    firstDay: 0
                });
            }
    });
</script>
<style>
    .disabled-result {
        display: none !important;
    }

    .active-result {
        text-transform: capitalize;
    }

    #inviteerror {
        color: red;
    }
</style>