<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_touristinvite/assets/css/touristinvite.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_touristinvite');
$saveOrder	= $listOrder == 'a.ordering';

$doc = JFactory::getDocument();
$doc->addScript(JUri::base() . '/components/com_touristinvite/assets/js/jspdf.js');
$doc->addScript(JUri::base() . '/components/com_touristinvite/assets/js/libs/base64.js');
$doc->addScript(JUri::base() . '/components/com_touristinvite/assets/js/libs/sprintf.js');

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_touristinvite&task=invites.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'inviteList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_touristinvite&view=invites'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">
			<div class="filter-search btn-group pull-left">
				<label for="filter_search" class="element-invisible"><?php echo JText::_('JSEARCH_FILTER');?></label>
				<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('JSEARCH_FILTER'); ?>" />
			</div>
			<div class="btn-group pull-left">
				<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
				<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value='';this.form.submit();"><i class="icon-remove"></i></button>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
			<div class="btn-group pull-right hidden-phone">
				<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC');?></label>
				<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC');?></option>
					<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING');?></option>
					<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING');?></option>
				</select>
			</div>
			<div class="btn-group pull-right">
				<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY');?></label>
				<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
					<option value=""><?php echo JText::_('JGLOBAL_SORT_BY');?></option>
					<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder);?>
				</select>
			</div>
		</div>        
		<div class="clearfix"> </div>
		<table class="table table-striped" id="inviteList">
			<thead>
				<tr>
    				<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
					</th>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
					</th>
					<th width="1%" class="nowrap center hidden-phone">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>

                    <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'COM_TOURISTINVITE_INVITE_NUMBER', 'a.pdf_id', $listDirn, $listOrder); ?>
                    </th>
                    <th width="1%" class="nowrap center hidden-phone"><?php echo JText::_('COM_TOURISTINVITE_DATE_OF_ISSUE'); ?></th>
                    <th width="1%" class="nowrap center hidden-phone"><?php echo JText::_('COM_TOURISTINVITE_TURFIRMA'); ?></th>
                    <th width="15%" class="nowrap center hidden-phone"><?php echo JText::_('COM_TOURISTINVITE_CLIENT_INFORMATION'); ?></th>
                    <th width="5%" class="nowrap center hidden-phone"><?php echo JText::_('COM_TOURISTINVITE_CITIES_AND_HOTELS'); ?></th>
                    <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'COM_TOURISTINVITE_FROM', 'a.invite_from', $listDirn, $listOrder); ?>
                    </th>
                    <th width="1%" class="nowrap center hidden-phone">
                        <?php echo JHtml::_('grid.sort', 'COM_TOURISTINVITE_TO', 'a.invite_to', $listDirn, $listOrder); ?>
                    </th>
                    <th width="1%" class="nowrap center hidden-phone"><?php echo JText::_('COM_TOURISTINVITE_PRICE'); ?></th>

					<th width="1%" class="nowrap center hidden-phone"><?php echo JText::_('COM_TOURISTINVITE_STATUS'); ?></th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_touristinvite');
                $canEdit	= $user->authorise('core.edit',			'com_touristinvite');
                $canCheckin	= $user->authorise('core.manage',		'com_touristinvite');
                $canChange	= $user->authorise('core.edit.state',	'com_touristinvite');
				?>
				<tr class="row<?php echo $i % 2; ?>">
                    
                <?php if (isset($this->items[0]->ordering)): ?>
					<td class="order nowrap center hidden-phone">
					<?php if ($canChange) :
						$disableClassName = '';
						$disabledLabel	  = '';
						if (!$saveOrder) :
							$disabledLabel    = JText::_('JORDERINGDISABLED');
							$disableClassName = 'inactive tip-top';
						endif; ?>
						<span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
							<i class="icon-menu"></i>
						</span>
						<input type="text" style="display:none" name="order[]" size="5" value="<?php echo $item->ordering;?>" class="width-20 text-area-order " />
					<?php else : ?>
						<span class="sortable-handler inactive" >
							<i class="icon-menu"></i>
						</span>
					<?php endif; ?>
					</td>
                <?php endif; ?>
					<td class="hidden-phone">
						<?php echo JHtml::_('grid.id', $i, $item->id); ?>
					</td>
                <?php if (isset($this->items[0]->id)): ?>
					<td class="center hidden-phone">
						<?php echo (int) $item->id; ?>
					</td>

                    <?php
                    $data = unserialize($item->invite_data);
                    ?>

                    <td class="center hidden-phone"><?php echo $item->pdf_id; ?></td>
                    <td class="center hidden-phone"><?php echo $item->invite_created; ?></td>
                    <td class="center hidden-phone"><?php echo $item->created_by; ?></td>
                    <td class="center hidden-phone">
                    <?php for ($i=1;$i<=$data['people_count'];$i++) {
                        echo $data['last_name_'.$i].' '.$data['first_name_'.$i].' passport '.$data['passport_'.$i].' Birthday '.$data['birthdate_'.$i].';<br>';
                    } ?>
                    </td>
                    <td class="center hidden-phone"><?php foreach ($data['invite_hotels'] as $h) echo $h.';<br>'; ?></td>
                    <?php
                        $item->invite_from = explode(' ',$item->invite_from);
                        $item->invite_to = explode(' ',$item->invite_to);

                        $item->invite_from = explode('-',$item->invite_from[0]);
                        $item->invite_to = explode('-',$item->invite_to[0]);
                    ?>
                    <td class="center hidden-phone"><?php echo $item->invite_from[2].'/'.$item->invite_from[1].'/'.$item->invite_from[0]; ?></td>
                    <td class="center hidden-phone"><?php echo $item->invite_to[2].'/'.$item->invite_to[1].'/'.$item->invite_to[0]; ?></td>
                    <td class="center hidden-phone"><?php echo $data['cost']; ?></td>

					<td class="center hidden-phone">
                        <a href="?option=com_touristinvite&view=invite&layout=pdf&id=<?php echo (int) $item->id; ?>" target="_blank">PDF</a>
                        <?php if ($item->pdf_id) { ?>
                            <a href="?option=com_touristinvite&view=invite&layout=doc&id=<?php echo (int) $item->id; ?>" target="_blank">DOC</a>
                        <?php } ?>
					</td>
                <?php endif; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>

		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>