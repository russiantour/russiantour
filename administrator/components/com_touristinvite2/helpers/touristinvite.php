<?php

/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Touristinvite helper.
 */
class TouristinviteHelper {

    /**
     * Configure the Linkbar.
     */
    public static function addSubmenu($vName = '') {
        JHtmlSidebar::addEntry(
            JText::_('COM_TOURISTINVITE_TITLE_INVITES'),
            'index.php?option=com_touristinvite&view=invites',
            $vName == 'invites'
        );
        JHtmlSidebar::addEntry(
            JText::_('COM_TOURISTINVITE_TITLE_TURFIRMS'),
            'index.php?option=com_touristinvite&view=turfirms',
            $vName == 'turfirms'
        );
        JHtmlSidebar::addEntry(
			JText::_('COM_TOURISTINVITE_TITLE_HOTELS'),
			'index.php?option=com_touristinvite&view=hotels',
			$vName == 'hotels'
		);
        JHtmlSidebar::addEntry(
            JText::_('COM_TOURISTINVITE_TITLE_CITYS'),
            'index.php?option=com_touristinvite&view=citys',
            $vName == 'citys'
        );
    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @return	JObject
     * @since	1.6
     */
    public static function getActions() {
        $user = JFactory::getUser();
        $result = new JObject;

        $assetName = 'com_touristinvite';

        $actions = array(
            'core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete'
        );

        foreach ($actions as $action) {
            $result->set($action, $user->authorise($action, $assetName));
        }

        return $result;
    }


}
