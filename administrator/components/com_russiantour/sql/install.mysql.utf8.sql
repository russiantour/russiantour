CREATE TABLE IF NOT EXISTS `#__russiantour_` (
`id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,

`ordering` INT(11)  NOT NULL ,
`state` TINYINT(1)  NOT NULL ,
`checked_out` INT(11)  NOT NULL ,
`checked_out_time` DATETIME NOT NULL ,
`created_by` INT(11)  NOT NULL ,
`modified_by` INT(11)  NOT NULL ,
`title` VARCHAR(255)  NOT NULL ,
`alias` VARCHAR(255) COLLATE utf8_bin NOT NULL ,
`introtext` TEXT NOT NULL ,
`fulltext` TEXT NOT NULL ,
`metakey` VARCHAR(255)  NOT NULL ,
`metadesc` VARCHAR(255)  NOT NULL ,
`field` VARCHAR(255)  NOT NULL ,
`field_2` VARCHAR(255)  NOT NULL ,
`field_3` VARCHAR(255)  NOT NULL ,
`field_4` VARCHAR(255)  NOT NULL ,
`field_5` VARCHAR(255)  NOT NULL ,
`field_6` VARCHAR(255)  NOT NULL ,
`field_7` VARCHAR(255)  NOT NULL ,
`field_8` VARCHAR(255)  NOT NULL ,
`field_9` VARCHAR(255)  NOT NULL ,
`field_10` VARCHAR(255)  NOT NULL ,
PRIMARY KEY (`id`)
) DEFAULT COLLATE=utf8mb4_unicode_ci;

