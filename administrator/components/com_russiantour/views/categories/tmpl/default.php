<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_russiantour/assets/css/russiantour.css');

$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_russiantour');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_russiantour&task=categories.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'categoriesList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();

$languages	= JLanguageHelper::getLanguages();

$db    = JFactory::getDbo();
?>

<form action="<?php echo JRoute::_('index.php?option=com_russiantour&view=categories'); ?>" method="post"
	  name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
		<?php else : ?>
		<div id="j-main-container">
			<?php endif; ?>

            <?php //echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>

			<div class="clearfix"></div>
			<table class="table table-striped" id="categoriesList">
				<thead>
				<tr>
					<th width="1%" class="hidden-phone">
						<input type="checkbox" name="checkall-toggle" value=""
							   title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
					</th>

					<?php if (isset($this->items[0]->state)): ?>
                    <th width="1%" class="nowrap center">
                        <?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'c.`state`', $listDirn, $listOrder); ?>
                    </th>
					<?php endif; ?>

                    <th class='left'>
                    <?php echo JHtml::_('searchtools.sort',  'id', 'c.`id`', $listDirn, $listOrder); ?>
                    </th>

                    <th class='left'>
                        <?php echo JHtml::_('searchtools.sort',  'Название', 'c.`title`', $listDirn, $listOrder); ?>
                    </th>

                    <th class='left'>
                        Association
                    </th>

                    <th class='left'>
                        <?php echo JHtml::_('searchtools.sort',  'Language', 'c.`language_id`', $listDirn, $listOrder); ?>
                    </th>
                </tr>
				</thead>
				<tfoot>
				<tr>
					<td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
				</tfoot>
				<tbody>
				<?php foreach ($this->items as $i => $item) {
					$ordering   = ($listOrder == 'c.ordering');
					$canCreate  = $user->authorise('core.create', 'com_russiantour');
					$canEdit    = $user->authorise('core.edit', 'com_russiantour');
					$canCheckin = $user->authorise('core.manage', 'com_russiantour');
					$canChange  = $user->authorise('core.edit.state', 'com_russiantour');
					?>
					<tr class="row<?php echo $i % 2; ?>">
                        <td class="hidden-phone">
                            <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                        </td>

						<?php if (isset($this->items[0]->state)): ?>
                        <td class="center">
                            <?php echo JHtml::_('jgrid.published', $item->state, $i, 'categories.', $canChange, 'cb'); ?>
                        </td>
						<?php endif; ?>

                        <td>
                            <?php echo $item->id; ?>
                        </td>

                        <td>
                            <?php echo $item->title; ?>
                        </td>

                        <td>
                            <ul class="item-associations">
                                <?php
		                        $query = $db->setQuery('SELECT * FROM #__russiantour_categories WHERE category_id = '.$item->category_id);
                                foreach ($db->loadObjectList() as $association) {
                                    foreach ($languages as $language)
                                    {
                                        if ($language->lang_id == $association->language_id)
                                            break;
                                    }
                                    ?>
                                    <li>
                                        <a href="/administrator/index.php?option=com_russiantour&view=category&layout=edit&id=<?php echo $association->id; ?>" title="" class="hasPopover label label-association label-<?php echo $language->sef; ?>" data-content="<?php echo $association->title; ?>" data-placement="top" data-original-title="<?php echo $language->title; ?>"><?php echo strtoupper($language->sef); ?></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </td>

                        <td class="small">
                            <?php
                            $db->setQuery('SELECT * FROM #__languages WHERE lang_id = '.$item->language_id);
                            $language = $db->loadObject();
                            $language->language_image = $language->image;
                            $language->language_title = $language->title;
                            echo JLayoutHelper::render('joomla.content.language', $language);
                            ?>
                        </td>
                    </tr>
				<?php } ?>
				</tbody>
			</table>

			<input type="hidden" name="task" value=""/>
			<input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="list[fullorder]" value="<?php echo $listOrder; ?> <?php echo $listDirn; ?>"/>
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
<script>
    window.toggleField = function (id, task, field) {

        var f = document.adminForm, i = 0, cbx, cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');

        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };
</script>