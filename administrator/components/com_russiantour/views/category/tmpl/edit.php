<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_russiantour/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'category.cancel') {
			Joomla.submitform(task, document.getElementById('category-form'));
		}
		else {
			
			if (task != 'category.cancel' && document.formvalidator.isValid(document.id('category-form'))) {
				
				Joomla.submitform(task, document.getElementById('category-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_russiantour&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="category-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_RUSSIANTOUR_TITLE_HOTEL', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

                    <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
                    <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

                    <?php echo $this->form->renderField('title'); ?>
                    <?php echo $this->form->renderField('language_id'); ?>
                    <?php echo $this->form->renderField('type'); ?>

                    <?php echo $this->form->renderField('category_id'); ?>
                    <?php echo $this->form->renderField('group_id'); ?>
                    <?php echo $this->form->renderField('catid'); ?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'associations', JText::_('JGLOBAL_FIELDSET_ASSOCIATIONS')); ?>
                <?php
                $db = JFactory::getDbo();
                $db->setQuery("select * from #__languages WHERE lang_code <> '".JFactory::getLanguage()->getTag()."'");
                $knownLangs = $db->loadObjectList();
                foreach ($knownLangs as $lang)
                {
                    if ($lang->lang_id == $this->item->language_id)
                        continue;

                    $html = [];
                    $html[] = $lang->title.': <span class="input-append">
        <input type="text" readonly="readonly" id="title_lang_' . $lang->lang_id
                        . '" value="' . (isset($this->item->associations[$lang->lang_id])?$this->item->associations[$lang->lang_id]->title:0) . '" />
                    <input type="hidden" readonly="readonly" id="lang_' . $lang->lang_id
                        . '" value="' . (isset($this->item->associations[$lang->lang_id])?$this->item->associations[$lang->lang_id]->id:0) . '" name="association['.$lang->lang_id.']"/>';
                    $html[] = '<button type="button" data-target="#associationsModal_'.$lang->lang_id.'" class="btn btn-primary" data-toggle="modal" title="' . JText::_('JSELECT') . '">'
                        . '<span class="icon-list icon-white" aria-hidden="true"></span> '
                        . JText::_('JSELECT') . '</button></span>';
                    $html[] = JHtml::_(
                        'bootstrap.renderModal',
                        'associationsModal_'.$lang->lang_id,
                        array(
                            'url'        => '/administrator/index.php?language_id='.$lang->lang_id.'&option=com_russiantour&view=categories&layout=associate',
                            'title'      => 'New association',
                            'width'      => '800px',
                            'height'     => '300px',
                            'modalWidth' => '80',
                            'bodyHeight' => '70',
                            'footer'     => '<button type="button" class="btn" data-dismiss="modal">'
                                . JText::_('JLIB_HTML_BEHAVIOR_CLOSE') . '</button>'
                        )
                    );

                    echo implode("<br/>", $html);
                }
                ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
<script>
    function associate(language_id,id,title) {
        jQuery('#title_lang_'+language_id).val(title);
        jQuery('#lang_'+language_id).val(id);
        jQuery('.close').click();
    }
</script>