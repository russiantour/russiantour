<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_russiantour/css/form.css');

// In case of modal
$app = JFactory::getApplication();
$input = $app->input;

$isModal = $input->get('layout') == 'modal' ? true : false;
$layout  = $isModal ? 'modal' : 'edit';
$tmpl    = $isModal || $input->get('tmpl', '', 'cmd') === 'component' ? '&tmpl=component' : '';
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	});

	Joomla.submitbutton = function (task) {
		if (task == 'hotel.cancel') {
			Joomla.submitform(task, document.getElementById('hotel-form'));
		}
		else {
			
			if (task != 'hotel.cancel' && document.formvalidator.isValid(document.id('hotel-form'))) {
				
				Joomla.submitform(task, document.getElementById('hotel-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_russiantour&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="hotel-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $_GET['tab']??'general')); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_RUSSIANTOUR_TITLE_HOTEL', true)); ?>
            <div class="row-fluid">
                <div class="span10 form-horizontal">
                    <fieldset class="adminform">

                        <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
                        <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
                        <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
                        <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
                        <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />



                        <?php if (isset($_GET['language_id'])) { ?>
                            <style>
                                #language_id_block {
                                    display: none;
                                }
                            </style>
                        <?php } ?>
                        <div id="language_id_block"><?php echo $this->form->renderField('language_id'); ?></div>

                        <?php echo $this->form->renderField('created_by'); ?>
                        <?php echo $this->form->renderField('modified_by'); ?>
                        <?php echo $this->form->renderField('title'); ?>
                        <?php echo $this->form->renderField('alias'); ?>
                        <?php echo $this->form->renderField('introtext'); ?>
                        <?php echo $this->form->renderField('metakey'); ?>
                        <?php echo $this->form->renderField('metadesc'); ?>

                        <?php echo $this->form->renderField('reference_id'); ?>
                        <?php echo $this->form->renderField('catid'); ?>
                        <?php echo $this->form->renderField('group_id'); ?>

                        <?php if ($this->state->params->get('save_history', 1)) : ?>
                        <div class="control-group">
                            <div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
                            <div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
                        </div>
                        <?php endif; ?>
                    </fieldset>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php if (!$isModal) { echo JHtml::_('bootstrap.addTab', 'myTab', 'associations', JText::_('JGLOBAL_FIELDSET_ASSOCIATIONS')); ?>
                <?php
                $db = JFactory::getDbo();
                $db->setQuery("select * from #__languages WHERE lang_id <> '".$this->item->language_id."'");
                $knownLangs = $db->loadObjectList();

                foreach ($knownLangs as $lang)
                {
                    if ($lang->lang_id == $this->item->language_id)
                        continue;

                    $html = [];
                    $html[] = $lang->title.': <span class="input-append">
        <input type="text" readonly="readonly" id="title_lang_' . $lang->lang_id
                        . '" value="' . (isset($this->item->associations[$lang->lang_id])?$this->item->associations[$lang->lang_id]->title:0) . '" />
                    <input type="hidden" readonly="readonly" id="lang_' . $lang->lang_id
                        . '" value="' . (isset($this->item->associations[$lang->lang_id])?$this->item->associations[$lang->lang_id]->id:0) . '" name="association['.$lang->lang_id.']"/>';

                    $html[] = '<button type="button" data-target="#associationsModal_'.$lang->lang_id.'" class="btn btn-primary" data-toggle="modal" title="' . JText::_('JSELECT') . '">'
                        . '<span class="icon-list icon-white" aria-hidden="true"></span> '
                        . JText::_('JSELECT') . '</button><button type="button" data-target="#associationsModalCreate_'.$lang->lang_id.'" class="btn btn-primary" data-toggle="modal" >Create</button></span>';

                    $html[] = JHtml::_(
                        'bootstrap.renderModal',
                        'associationsModal_'.$lang->lang_id,
                        array(
                            'url'        => '/administrator/index.php?language_id='.$lang->lang_id.'&option=com_russiantour&view=hotes&layout=associate',
                            'title'      => 'New association',
                            'width'      => '800px',
                            'height'     => '300px',
                            'modalWidth' => '80',
                            'bodyHeight' => '70',
                            'footer'     => '<button type="button" class="btn" data-dismiss="modal">'
                                . JText::_('JLIB_HTML_BEHAVIOR_CLOSE') . '</button>'
                        )
                    );

                    $html[] = JHtml::_(
                        'bootstrap.renderModal',
                        'associationsModalCreate_'.$lang->lang_id,
                        array(
                            'url'        => '/administrator/index.php?language_id='.$lang->lang_id.'&option=com_russiantour&view=hotel&layout=modal&parent_id='.$this->item->reference_id,
                            'title'      => 'New association',
                            'width'      => '800px',
                            'height'     => '300px',
                            'modalWidth' => '80',
                            'bodyHeight' => '70',
                            'footer'     => '<button type="button" class="btn" data-dismiss="modal">'
                                . JText::_('JLIB_HTML_BEHAVIOR_CLOSE') . '</button>'
                        )
                    );

                    echo implode("<br/>", $html);
                }
                ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'address', 'Адрес'); ?>
                <?php echo $this->form->renderField('address_lat'); ?>
                <?php echo $this->form->renderField('address_lon'); ?>
                <?php echo $this->form->renderField('address_addr'); ?>
                <?php echo $this->form->renderField('address_city'); ?>
                <?php echo $this->form->renderField('address_province'); ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'other', 'Общее'); ?>
                <?php echo $this->form->renderField('category_id'); ?>

                <?php echo $this->form->renderField('room_type'); ?>

                <?php
                echo '<div id="stars_block">'.$this->form->renderField('stars'),'</div>';
                echo '<div id="rooms_block">'.$this->form->renderField('rooms'),'</div>';
                ?>

                <?php echo $this->form->renderField('cost_from'); ?>
                <?php echo $this->form->renderField('cost_to'); ?>

                <?php echo $this->form->renderField('image'); ?>

            <?php echo JHtml::_('bootstrap.endTab'); } else { ?>
                <?php echo $this->form->renderField('parent_id'); ?>
            <?php } ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
<script>
    function associate(language_id,id,title) {
        jQuery('#title_lang_'+language_id).val(title);
        jQuery('#lang_'+language_id).val(id);
        jQuery('.close').click();
    }
    jQuery( document ).ready(function() {
        <?php
        if (intval($this->item->catid)>0)//значит это отель
        {
            echo "document.getElementById('stars_block').setAttribute('style','display:block');";
            echo "document.getElementById('rooms_block').setAttribute('style','display:none');";
        }
        else//иначе это аппартаменты
        {
            echo "document.getElementById('stars_block').setAttribute('style','display:none');";
            echo "document.getElementById('rooms_block').setAttribute('style','display:block');";
        }
        ?>
        jQuery('#jform_category_id').on('change',function () {
            jQuery('#rooms_block').hide();
            jQuery('#stars_block').hide();

            if (jQuery(this).val()==2 || jQuery(this).val()==3 || jQuery(this).val()==4)
                jQuery('#stars_block').show();
            else
                jQuery('#rooms_block').show();
        });
        document.location.search.split('?')[1].split('&').each(function(param){
            if(param.indexOf('hide=') > -1) {
                jQuery('#hotel-form').attr('action',jQuery('#hotel-form').attr('action')+'&'+param);
                param.split('=')[1].split(',').each(function(to_hide){
                    jQuery('a[href="#'+to_hide+'"]').parent().hide();
                });
            }
            if(param.indexOf('tab=') > -1) {
                jQuery('#hotel-form').attr('action',jQuery('#hotel-form').attr('action')+'&'+param);
            }
        });
    });
</script>