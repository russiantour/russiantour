<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     <kaktus.mov@gmail.com>
 * @copyright  2017 kaktus.mov
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Russiantour model.
 *
 * @since  1.6
 */
class RussiantourModelCategory extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_RUSSIANTOUR';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_russiantour.category';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Category', $prefix = 'RussiantourTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_russiantour.category', 'category',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_russiantour.edit.category.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
		    if (isset($item->id))
            {
                // Do any procesing on fields here if needed
                $db = JFactory::getDbo();
                $db->setQuery("SELECT * FROM #__russiantour_categories WHERE category_id = ".$item->category_id." AND id <> ".$item->id);
                $associations = $db->loadObjectList();
                $item->associations = [];
                foreach ($associations as $association)
                {
                    $item->associations[$association->language_id] = $association;
                }
            }
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__russiantour_categories');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

    public function save($data){
        $result = parent::save($data);

        if (!$result)
            return false;

        if (!$data['id'])
        {
            $db = JFactory::getDbo();
            $db->setQuery("select * from #__russiantour_categories ORDER BY id desc");
            $last_hotel = $db->loadObject();
            $data['id'] = $last_hotel->id;
            $data['category_id'] = $last_hotel->id;

            $db->setQuery("UPDATE #__russiantour_categories SET category_id = ".$data['category_id'].' WHERE id = '.$data['id']);
            $db->execute();
        }
        $this->associate($data);
        return true;
    }

    private function associate($data){
        $db = JFactory::getDbo();

	    foreach ($_POST['association'] as $lang=>$association)
        {
            if ($association && intval($association) > 0)
            {
                $group_id = $data['group_id']?$data['group_id']:0;
                $catid = $data['catid']?$data['catid']:0;

                $db->setQuery("UPDATE #__russiantour_categories SET category_id = ".$data['category_id'].', group_id = '.$group_id.', catid = '.$catid.' WHERE id = '.intval($association));

                $db->execute();
            }
        }
    }
}
