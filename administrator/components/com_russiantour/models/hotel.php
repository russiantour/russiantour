<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Russiantour model.
 *
 * @since  1.6
 */
class RussiantourModelHotel extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_RUSSIANTOUR';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_russiantour.hotel';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Hotel', $prefix = 'RussiantourTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_russiantour.hotel', 'hotel',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

        $jinput = JFactory::getApplication()->input;

        /*
         * The front end calls this model and uses a_id to avoid id clashes so we need to check for that first.
         * The back end uses id so we use that the rest of the time and set it to 0 by default.
         */
        $id = $jinput->get('a_id', $jinput->get('id', 0));

		// Prevent messing with article language and category when editing existing article with associations
        $assoc = JLanguageAssociations::isEnabled();

        // Check if article is associated
        if (/*$this->getState('article.id') &&*/ $app->isClient('site') && $assoc)
        {
            $associations = JLanguageAssociations::getAssociations('com_content', '#__content', 'com_content.item', $id);

            // Make fields read only
            if (!empty($associations))
            {
                $form->setFieldAttribute('language', 'readonly', 'true');
                $form->setFieldAttribute('catid', 'readonly', 'true');
                $form->setFieldAttribute('language', 'filter', 'unset');
                $form->setFieldAttribute('catid', 'filter', 'unset');
            }
        }
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_russiantour.edit.hotel.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{
		    if (isset($item->id))
            {
                // Do any procesing on fields here if needed
                $db = JFactory::getDbo();
                //получаем ассоциации
                $db->setQuery("SELECT * FROM #__russiantour_ WHERE reference_id = ".$item->reference_id." AND id <> ".$item->id);
                $associations = $db->loadObjectList();
                $item->associations = [];
                foreach ($associations as $association)
                {
                    $item->associations[$association->language_id] = $association;
                }

                //получаем категорию
                $db->setQuery("SELECT t.*, c.catid FROM #__russiantour_to_category t 
JOIN #__russiantour_categories c ON c.category_id = t.category_id
WHERE t.reference_id = ".$item->reference_id);
                $category = $db->loadObject();
                if ($category && isset($category->category_id))
                {
                    $item->category_id = $category->category_id;
                    $item->view_type = $category->type;
                    $item->catid = $category->catid;
                }

                //получаем адрес
                $db->setQuery("SELECT * FROM #__russiantour_addresses WHERE reference_id = ".$item->reference_id);
                $address = $db->loadObject();
                if ($address && isset($address->id))
                {
                    $item->address_lat = $address->lat;
                    $item->address_lon = $address->lon;
                    $item->address_addr = $address->addr;
                    $item->address_city = $address->city;
                    $item->address_province = $address->province;
                }

                //получаем звёзды или комнаты
                /*if (intval($item->catid) > 0)//значит это отель - сохраняем звёзды
                {
                    $field_id = 17;
                }
                else//иначе это аппартаменты
                {
                    $field_id = 27;
                }*/
                $sql = "SELECT * FROM `#__flexicontent_advsearch_index` where field_id IN (17,19,27) AND item_id = ".$item->reference_id;

                $db->setQuery($sql);
                $adv_datas = $db->loadObjectList();
                if (count($adv_datas))
                {
                    foreach ($adv_datas as $adv_data)
                    {
                        if ($adv_data->field_id == 17)
                            $item->stars = intval($adv_data->search_index);
                        elseif ($adv_data->field_id == 19)
                        {
                            if (!isset($item->room_type))
                                $item->room_type = [];
                            $item->room_type[] = $adv_data->value_id;
                        }
                        elseif ($adv_data->field_id == 27)
                            $item->rooms = $adv_data->search_index;
                    }
                }

                $sql = "SELECT * FROM #__russiantour_hotel_settings WHERE reference_id = ".$item->reference_id;
                $db->setQuery($sql);
                $cost_rub = $db->loadObject();
                $item->cost_from = $cost_rub->cost_from_rub;
                $item->cost_to = $cost_rub->cost_to_rub;

                $sql = "SELECT * FROM #__russiantour_images WHERE reference_id = ".$item->reference_id;
                $db->setQuery($sql);
                $images = $db->loadObjectList();

                foreach ($images as $k => $image)
                {
                    $item->image->{'image'.$k}->image = $image->full_image_path;
                }
            }
		}
        $item->category_id = $item->category_id ?? 0;
        $item->view_type = $item->view_type ?? 0;
        $item->stars = $item->stars ?? 0;
        $item->rooms = $item->rooms ?? 0;
        $item->cost_from = $item->cost_from ?? 0;
        $item->cost_to = $item->cost_to ?? 0;

        $item->address_lat = $item->address_lat ?? '';
        $item->address_lon = $item->address_lon ?? '';
        $item->address_addr = $item->address_addr ?? '';
        $item->address_city = $item->address_city ?? '';
        $item->address_province = $item->address_province ?? '';

        $item->catid = $item->catid ?? '';

        if (isset($_GET['language_id']))
            $item->language_id = $_GET['language_id'];

        if (isset($_GET['parent_id']))
            $item->parent_id = $_GET['parent_id'];

        if (isset($_GET['test']))
            var_dump();

		return $item;
	}

	/**
	 * Method to duplicate an Hotel
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_russiantour'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__russiantour_');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

    public function save($data){
	    if (isset($data['parent_id']))
	        $data['reference_id'] = $data['parent_id'];
        $result = parent::save($data);

        if (!$result)
            return false;

        $db = JFactory::getDbo();

        if (!$data['id'])
        {
            $db->setQuery("select * from #__russiantour_ ORDER BY id desc");
            $last_hotel = $db->loadObject();

            if (isset($data['parent_id']))
            {
                if ($last_hotel)
                {
                    echo "<script>window.parent.associate(".$last_hotel->language_id.",".$last_hotel->id.",'".$last_hotel->title."');</script>";
                    exit;
                }
                return false;
            }

            $data['id'] = $last_hotel->id;
            $data['reference_id'] = $last_hotel->id;
            $db->setQuery("UPDATE #__russiantour_ SET reference_id = ".$data['reference_id'].' WHERE id = '.$data['id']);
            $db->execute();
        }

        $this->set_address($db, $data);
        $this->set_images($db, $data);
        $this->set_category($db, $data);
        $this->set_cost($db, $data);
        $this->save_stars_or_rooms($db, $data);
        $this->save_room_type($db, $data);
        $this->associate($data);
        return true;
    }

    private function set_images($db,$data)
    {
        $set_images = false;

        $new_images = [];
        foreach ($data['image'] as $image)
        {
            if (isset($image['image']) && $image['image'] != '')
                $new_images[] = $image['image'];
        }

        if (!count($new_images))
        {
            $sql = "DELETE FROM `#__russiantour_images` WHERE reference_id = ".$data['reference_id'];
            $db->setQuery($sql);
            $db->execute();
            return;
        }

        $sql = "SELECT * FROM #__russiantour_images WHERE reference_id = ".$data['reference_id'];
        $db->setQuery($sql);
        $db_images = $db->loadObjectList();

        foreach ($db_images as $db_image)
        {
            if (!in_array($db_image->full_image_path,$new_images))
            {
                $set_images = true;
                break;
            }
        }

        if (!$set_images)
        {
            if (count($new_images) == count($db_images))
                return;
        }

        $deleted = false;
        foreach ($data['image'] as $image)
        {
            $can_insert = false;
            if (isset($image['image']) && $image['image'] != '')
            {
                $image_db_path = $image['image'];
                $image_path = $_SERVER["DOCUMENT_ROOT"]."/".$image_db_path;

                $small_image_basename = basename($image['image']);
                if (substr($small_image_basename,0,2) == 'l_')
                    $small_image_basename = substr($small_image_basename,2);
                $small_image_basename = 's_'.$small_image_basename;

                $small_image_db_path = dirname($image['image']).'/'.$small_image_basename;
                $small_image_path = $_SERVER["DOCUMENT_ROOT"]."/".$small_image_db_path;

                if (!file_exists($small_image_path))
                {
                    if (file_exists($image_path))
                    {
                        $expected_width = 250;
                        $expected_height = 180;

                        list ($width, $height) = getimagesize ($image_path);
                        $ratio = $width / $expected_width;
                        $new_width = $expected_width;
                        $new_height = intval($height / $ratio);
                        if ($new_height < $expected_height)
                        {
                            $ratio = $height / $expected_height;
                            $new_width = intval($width / $ratio);
                            $new_height = $expected_height;
                        }

                        $full_image = imagecreatefromjpeg($image_path);
                        $small_image = imagescale($full_image , $new_width , $new_height);
                        $im2 = imagecrop($small_image, ['x' => 0, 'y' => 0, 'width' => $expected_width, 'height' => $expected_height]);
                        if ($im2 !== FALSE) {
                            imagejpeg($im2, $small_image_path);
                            imagedestroy($im2);

                            $can_insert = true;
                        }
                        imagedestroy($full_image);
                    }
                }
                else
                    $can_insert = true;

                if ($can_insert)
                {
                    if (!$deleted)
                    {
                        $sql = "DELETE FROM `#__russiantour_images` WHERE reference_id = ".$data['reference_id'];
                        $db->setQuery($sql);
                        $db->execute();

                        $deleted = true;
                    }

                    $object = new stdClass();
                    $object->reference_id = $data['reference_id'];
                    $object->full_image_path = $image_db_path;
                    $object->small_image_path = $small_image_db_path;
                    JFactory::getDbo()->insertObject('#__russiantour_images', $object, 'id');
                }
            }
        }
    }

    private function set_address($db,$data)
    {
        $sql = "SELECT * FROM `#__russiantour_addresses` where reference_id = ".$data['reference_id'];
        $db->setQuery($sql);
        $adv_data = $db->loadObject();
        if ($adv_data && isset($adv_data->reference_id))
        {
            $adv_data->lat = $data['address_lat'];
            $adv_data->lon = $data['address_lon'];
            $adv_data->addr = $data['address_addr'];
            $adv_data->city = $data['address_city'];
            $adv_data->province = $data['address_province'];
            JFactory::getDbo()->updateObject('#__russiantour_addresses', $adv_data, 'id');
        }
        else
        {
            $object = new stdClass();
            $object->reference_id = $data['reference_id'];
            $object->lat = $data['address_lat'];
            $object->lon = $data['address_lon'];
            $object->addr = $data['address_addr'];
            $object->city = $data['address_city'];
            $object->province = $data['address_province'];
            JFactory::getDbo()->insertObject('#__russiantour_addresses', $object, 'id');
        }
    }

    private function set_category($db,$data)
    {
        $sql = "SELECT * FROM `#__russiantour_to_category` where reference_id = ".$data['reference_id'];
        $db->setQuery($sql);
        $adv_data = $db->loadObject();
        if ($adv_data && isset($adv_data->reference_id))
        {
            $adv_data->category_id = $data['category_id'];
            JFactory::getDbo()->updateObject('#__russiantour_to_category', $adv_data, 'id');
        }
        else
        {
            $object = new stdClass();
            $object->reference_id = $data['reference_id'];
            $object->category_id = $data['category_id'];
            JFactory::getDbo()->insertObject('#__russiantour_to_category', $object, 'id');
        }
    }

    private function set_cost($db,$data)
    {
        //get settings to item
        $params = JComponentHelper::getParams('com_russiantour');
        $rate = $params->get('rate',70);

        //save cost_from
        $sql = "SELECT * FROM `#__flexicontent_advsearch_index` where field_id = 28 AND extraid = 1 AND item_id = ".$data['reference_id'];
        $db->setQuery($sql);
        $adv_data = $db->loadObject();
        if ($adv_data && isset($adv_data->item_id))
        {
            $adv_data->search_index = $data['cost_from']/$rate;
            $adv_data->value_id = 1;
            JFactory::getDbo()->updateObject('#__flexicontent_advsearch_index', $adv_data, 'sid');
        }
        else
        {
            $object = new stdClass();
            $object->field_id = 28;
            $object->item_id = $data['reference_id'];
            $object->extraid = 1;
            $object->search_index = $data['cost_from']/$rate;
            $object->value_id = 1;
            JFactory::getDbo()->insertObject('#__flexicontent_advsearch_index', $object, 'sid');
        }

        //save cost_to
        $sql = "SELECT * FROM `#__flexicontent_advsearch_index` where field_id = 28 AND extraid = 0 AND item_id = ".$data['reference_id'];
        $db->setQuery($sql);
        $adv_data = $db->loadObject();
        if ($adv_data && isset($adv_data->item_id))
        {
            $adv_data->search_index = $data['cost_to']/$rate;
            $adv_data->value_id = 0;
            JFactory::getDbo()->updateObject('#__flexicontent_advsearch_index', $adv_data, 'sid');
        }
        else
        {
            $object = new stdClass();
            $object->field_id = 28;
            $object->item_id = $data['reference_id'];
            $object->extraid = 0;
            $object->search_index = $data['cost_to']/$rate;
            $object->value_id = 0;
            JFactory::getDbo()->insertObject('#__flexicontent_advsearch_index', $object, 'sid');
        }

        //save cost_rub
        $sql = "SELECT * FROM `#__russiantour_hotel_settings` where reference_id = ".$data['reference_id'];
        $db->setQuery($sql);
        $adv_data = $db->loadObject();
        if ($adv_data && isset($adv_data->reference_id))
        {
            $adv_data->cost_from_rub = $data['cost_from'];
            $adv_data->cost_to_rub = $data['cost_to'];
            JFactory::getDbo()->updateObject('#__russiantour_hotel_settings', $adv_data, 'id');
        }
        else
        {
            $object = new stdClass();
            $object->cost_from_rub = $data['cost_from'];
            $object->cost_to_rub = $data['cost_to'];
            $object->reference_id = $data['reference_id'];
            JFactory::getDbo()->insertObject('#__russiantour_hotel_settings', $object, 'id');
        }
    }

    private function save_stars_or_rooms($db,$data)
    {
        //search_index to value_id accordance
        $accordance = [
            'stars' => [
                2 => 5,
                3 => 4,
                4 => 3,
                5 => 2
            ],
            'rooms' => [
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 7,
                6 => 12
            ]
        ];

        $data_type = false;
        if (intval($data['catid']) > 0)//значит это отель - сохраняем звёзды
        {
            if (intval($data['stars']) > 0)
            {
                $data_type = 'stars';
                $field_id = 17;
            }
        }
        else//иначе это аппартаменты
        {
            if (intval($data['rooms']) > 0)
            {
                $data_type = 'rooms';
                $field_id = 27;
            }
        }

        if ($data_type)
        {
            $sql = "SELECT * FROM `#__flexicontent_advsearch_index` where field_id = ".$field_id." AND item_id = ".$data['reference_id'];
            $db->setQuery($sql);
            $adv_data = $db->loadObject();
            if ($adv_data && isset($adv_data->item_id))
            {
                $adv_data->search_index = $data[$data_type];
                $adv_data->value_id = $accordance[$data_type][$data[$data_type]];
                JFactory::getDbo()->updateObject('#__flexicontent_advsearch_index', $adv_data, 'sid');
            }
            else
            {
                $object = new stdClass();
                $object->field_id = $field_id;
                $object->item_id = $data['reference_id'];
                $object->extraid = 0;
                $object->search_index = $data[$data_type];
                $object->value_id = $accordance[$data_type][$data[$data_type]];
                JFactory::getDbo()->insertObject('#__flexicontent_advsearch_index', $object, 'sid');
            }
        }
    }

    private function save_room_type($db,$data)
    {
        $room_types = [
            1 => 'COM_RUSSIANTOUR_ROOM_TYPE_STANDARD',
            12 => 'COM_RUSSIANTOUR_ROOM_TYPE_ECONOMY',
            15 => 'COM_RUSSIANTOUR_ROOM_TYPE_JUNIOR_SUITE',
            19 => 'COM_RUSSIANTOUR_ROOM_TYPE_SUPERIOR',
            2 => 'COM_RUSSIANTOUR_ROOM_TYPE_BUSINESS',
            28 => 'COM_RUSSIANTOUR_ROOM_TYPE_STUDIO',
            29 => 'COM_RUSSIANTOUR_ROOM_TYPE_STUDIO2',
            30 => 'COM_RUSSIANTOUR_ROOM_TYPE_DELUX',
			31 => 'COM_RUSSIANTOUR_ROOM_TYPE_CONFORT',
			32 => 'COM_RUSSIANTOUR_ROOM_TYPE_APAR',
			33 => 'COM_RUSSIANTOUR_ROOM_TYPE_ULU',
			34 => 'COM_RUSSIANTOUR_ROOM_TYPE_OTEL',
			35 => 'COM_RUSSIANTOUR_ROOM_TYPE_KOTETSH',
			36 => 'COM_RUSSIANTOUR_ROOM_TYPE_SHALE',
			37 => 'COM_RUSSIANTOUR_ROOM_TYPE_TAUN',
            3 => 'COM_RUSSIANTOUR_ROOM_TYPE_LUX',
            4 => 'COM_RUSSIANTOUR_ROOM_TYPE_SUITE',
            7 => 'COM_RUSSIANTOUR_ROOM_TYPE_RENOVATED'
        ];

        if (isset($data['room_type']))
        {
            $sql = "DELETE FROM `#__flexicontent_advsearch_index` where field_id = 19 AND item_id = ".$data['reference_id'];
            $db->setQuery($sql);
            $db->execute();

            foreach ($data['room_type'] as $room_type)
            {
                $sql = "INSERT INTO `#__flexicontent_advsearch_index` (field_id,item_id,search_index,value_id,extraid) 
VALUES (19,".$data['reference_id'].",'".JText::_( $room_types[$room_type],true )."',".$room_type.",".$room_type.")";
                $db->setQuery($sql);
                $db->execute();
            }
        }
    }

    private function associate($data){
        $db = JFactory::getDbo();

	    foreach ($_POST['association'] as $lang=>$association)
        {
            if ($association && intval($association) > 0)
            {
                $db->setQuery("UPDATE #__russiantour_ SET 
                reference_id = ".$data['reference_id'].' 
                WHERE id = '.intval($association));

                $db->execute();
            }
        }
    }
}
