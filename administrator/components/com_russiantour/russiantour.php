<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_russiantour'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Russiantour', JPATH_COMPONENT_ADMINISTRATOR);
JLoader::register('RussiantourHelper', JPATH_COMPONENT_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'russiantour.php');

$controller = JControllerLegacy::getInstance('Russiantour');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
