<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Partner controller class.
 *
 * @since  1.6
 */
class ViaggioControllerPartner extends JControllerForm
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'partners';
		parent::__construct();
	}
}
