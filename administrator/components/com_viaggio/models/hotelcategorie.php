<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Class ViaggioModelHotelcategorie
 */
class ViaggioModelHotelcategorie extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_VIAGGIO';
    protected $event_after_save = 'onExtensionAfterSave';

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Hotelcategories', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_viaggio.hotelcategorie', 'hotelcategorie', array('control' => 'jform', 'load_data' => $loadData));
        
        
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_viaggio.edit.hotelcategorie.data', array());

		if (empty($data)) {
			$data = $this->getItem();
            
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {
            //Do any procesing on fields here if needed
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_hotel_categories');//done
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}

    public function save($data){
        parent::save($data);
        if (!$data['id'])
        {
            $db = JFactory::getDbo();
            $db->setQuery("select * from #__viaggio_hotel_categories ORDER BY id desc");
            $last_hotel = $db->loadObject();
            $data['id'] = $last_hotel->id;
        }
        $this->operate_translation('ita','it-IT',$data['id']);
        $this->operate_translation('rus','ru-RU',$data['id']);
        $this->operate_translation('eng','en-GB',$data['id']);
        $this->operate_translation('esp','es-ES',$data['id']);
        return true;
    }

    private function operate_translation($lang_form,$lang_db,$id){
        $db = JFactory::getDbo();

        if (isset($_POST['jform']['hotelcategorie_translation_'.$lang_form]))
        {
            $text = $_POST['jform']['hotelcategorie_translation_'.$lang_form];
            $db->setQuery("select * from #__viaggio_hotel_categories_translations where category_id = ".$id." and language = '".$lang_db."'");
            $hotel_translation = $db->loadObject();
            if ($hotel_translation)
            {
                $hotel_translation->text = $text;
                $db->updateObject('#__viaggio_hotel_categories_translations',$hotel_translation,'id');
            }
            else
            {
                $data = (object) array(
                    'category_id' => $id,
                    'language' => $lang_db,
                    'text' => $text
                );
                $db->insertObject('#__viaggio_hotel_categories_translations', $data);
            }
        }
    }
}