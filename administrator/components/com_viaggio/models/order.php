<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Viaggio model.
 *
 * @since  1.6
 */
class ViaggioModelOrder extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_VIAGGIO';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_viaggio.order';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Order', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array    $data      An optional array of data for the form to interogate.
	 * @param   boolean  $loadData  True if the form is to load its own data (default case), false if not.
	 *
	 * @return  JForm  A JForm object on success, false on failure
	 *
	 * @since    1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm(
			'com_viaggio.order', 'order',
			array('control' => 'jform',
				'load_data' => $loadData
			)
		);

		if (empty($form))
		{
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return   mixed  The data for the form.
	 *
	 * @since    1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_viaggio.edit.order.data', array());

		if (empty($data))
		{
			if ($this->item === null)
			{
				$this->item = $this->getItem();
			}

			$data = $this->item;

			// Support for multiple or not foreign key field: order_id
			$array = array();
			foreach((array)$data->order_id as $value): 
				if(!is_array($value)):
					$array[] = $value;
				endif;
			endforeach;
			$data->order_id = implode(',',$array);

			// Support for multiple or not foreign key field: tour_id
			$array = array();
			foreach((array)$data->tour_id as $value): 
				if(!is_array($value)):
					$array[] = $value;
				endif;
			endforeach;
			$data->tour_id = implode(',',$array);
		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer  $pk  The id of the primary key.
	 *
	 * @return  mixed    Object on success, false on failure.
	 *
	 * @since    1.6
	 */
	public function getItem($pk = null)
	{
	    $changes = array();
        $db = JFactory::getDbo();
		if ($item = parent::getItem($pk))
		{
            $query1 = "SELECT * FROM `#__viaggio_manualpayments` WHERE order_id = '".$item->id."' ORDER BY id ASC LIMIT 1";
            $db->setQuery($query1);
            $manualPayment = $db->loadObject();

            if (isset($manualPayment->id))
            {
                $item->manualpayment = $manualPayment;

                $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
                $db->setQuery($query);
                $manualPaymentDetails = $db->loadObjectList();

                if (count($manualPaymentDetails))
                {
                    $data = array();

                    foreach ($manualPaymentDetails as $manualPaymentDetail)
                    {
                        $data[$manualPaymentDetail->field_group_name][$manualPaymentDetail->field_group][$manualPaymentDetail->field_name] = $manualPaymentDetail->field_value;
                    }

                    if (count($data))
                    {
                        if (isset($data['air-ticket']))
                            $item->airTickets = $data['air-ticket'];
                        if (isset($data['rail-ticket']))
                            $item->railTickets = $data['rail-ticket'];
                        if (isset($data['other']))
                            $item->otherDetails = $data['other'];
                    }
                }

                $query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = ".$item->id." ORDER BY id ASC";
                $db->setQuery($query);
                $clients = $db->loadAssocList();

                $item->clients = array();
                if (count($clients))
                {
                    $item->clients = $clients;
                }
            }

            $query = $db->getQuery(true);
            $query->select('*');
            $query->from('#__viaggio_trips');
            $query->where('id = '.$item->trip_id);
            $db->setQuery($query);
            $item->trip = $db->loadObject();

            $query = "SELECT * FROM #__viaggio_month_statistic WHERE order_id = ".$item->id." AND manualpaymant_id = 0";
            $db->setQuery($query);
            $month_statistic = $db->loadObject();
            if (isset($month_statistic->id))
            {
                $item->month_statistic_costo_parziale = $month_statistic->costo_parziale;
            }
            else
            {
                $item->month_statistic_costo_parziale = '';
            }

            $query = "SELECT * FROM #__viaggio_orders_relations_postchanges WHERE order_id = ".$item->id." ORDER BY id ASC";
            $db->setQuery($query);
            $changes_table_data = $db->loadObjectList();
            foreach ($changes_table_data as $row)
            {
                $changes[$row->partner_id][$row->service_id][] = ['amount_euro' => $row->amount_euro,'amount_rub' => $row->amount_rub];
            }
		}
		else
        {
            $item->trip = false;
            $item->month_statistic_costo_parziale = false;
        }

		$item->changes = $changes;

		$item->nationalityList = $db->setQuery("SELECT * FROM #__viaggio_nationality ORDER BY value ASC")->loadObjectList();

		return $item;
	}

	/**
	 * Method to duplicate an Order
	 *
	 * @param   array  &$pks  An array of primary key IDs.
	 *
	 * @return  boolean  True if successful.
	 *
	 * @throws  Exception
	 */
	public function duplicate(&$pks)
	{
		$user = JFactory::getUser();

		// Access checks.
		if (!$user->authorise('core.create', 'com_viaggio'))
		{
			throw new Exception(JText::_('JERROR_CORE_CREATE_NOT_PERMITTED'));
		}

		$dispatcher = JEventDispatcher::getInstance();
		$context    = $this->option . '.' . $this->name;

		// Include the plugins for the save events.
		JPluginHelper::importPlugin($this->events_map['save']);

		$table = $this->getTable();

		foreach ($pks as $pk)
		{
			if ($table->load($pk, true))
			{
				// Reset the id to create a new record.
				$table->id = 0;

				if (!$table->check())
				{
					throw new Exception($table->getError());
				}
				

				// Trigger the before save event.
				$result = $dispatcher->trigger($this->event_before_save, array($context, &$table, true));

				if (in_array(false, $result, true) || !$table->store())
				{
					throw new Exception($table->getError());
				}

				// Trigger the after save event.
				$dispatcher->trigger($this->event_after_save, array($context, &$table, true));
			}
			else
			{
				throw new Exception($table->getError());
			}
		}

		// Clean cache
		$this->cleanCache();

		return true;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_orders');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

    public function save($data){
        $db = JFactory::getDbo();
        /*if (isset($data['id']) && $data['id'] > 0)
        {
            $query = "SELECT count(*) as count FROM #__viaggio_manualpayments WHERE order_id = ".$data['id']." AND full_payment = 1";
            $db->setQuery($query);
            $MPayment = $db->loadObject();
            if ($MPayment->count > 0)
            {
                return false;
            }
        }*/

        if (strpos($data['date_from'],'/'))
        {
            $data['date_from'] = explode('/',$data['date_from']);
            $data['date_from'] = implode('-',array($data['date_from'][2],$data['date_from'][1],$data['date_from'][0]));
        }
        if (strpos($data['date_to'],'/'))
        {
            $data['date_to'] = explode('/',$data['date_to']);
            $data['date_to'] = implode('-',array($data['date_to'][2],$data['date_to'][1],$data['date_to'][0]));
        }
        if (strpos($data['next_payment_date'],'/'))
        {
            $data['next_payment_date'] = explode('/',$data['next_payment_date']);
            $data['next_payment_date'] = implode('-',array($data['next_payment_date'][2],$data['next_payment_date'][1],$data['next_payment_date'][0]));
        }


        $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = '".$data['id']."' ORDER BY id DESC LIMIT 1");
        $result = $db->loadObject();

        if ($result)
        {
            $db->setQuery("DELETE FROM #__viaggio_manualpayments_details WHERE payment_id = '".$result->paymentID."'");
            $db->execute();

            foreach ($_POST['air-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','air-ticket','".$result->paymentID."')");
                    $db->execute();
                }
            }
            foreach ($_POST['rail-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','rail-ticket','".$result->paymentID."')");
                    $db->execute();
                }
            }
            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-amount','".$_POST['other-amount']."','0','other','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-rus','".$_POST['other-rus']."','0','other','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-ita','".$_POST['other-ita']."','0','other','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-amount','".$_POST['visa-amount']."','0','visa','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-rus','".$_POST['visa-rus']."','0','visa','".$result->paymentID."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-ita','".$_POST['visa-ita']."','0','visa','".$result->paymentID."')");
            $db->execute();

            $clients = $_POST['clients'];
            $data['clientcount'] = count($clients);
            if (count($clients))
            {
                $query = "DELETE FROM `#__viaggio_clients` WHERE order_id = ".$data['id'];
                $db->setQuery($query);
                $db->execute();
                foreach ($clients as $client)
                    self::createClient($client, $data['id'], $data['created_by'], $data['tour_id']);
            }
        }

        $db->setQuery("UPDATE #__viaggio_manualpayments SET full_payment = ".intval(isset($data['full_payment']))." WHERE order_id = '".$data['id']."' ORDER BY id DESC LIMIT 1");
        $db->execute();

        if (!isset($data['full_payment']))
            $data['full_payment'] = 0;

        parent::save($data);
        $this->saveServices($data['id'],count($clients),$result);
        $this->saveCostoParziale($data);
        $this->saveHotels($data['id']);
        return true;
    }

    private static function createClient($clientArray, $order_id, $user_id, $tour_id){
        $query = "INSERT INTO `#__viaggio_clients` (`id`, `asset_id`, `created_by`, `tour_id`, `cognome`, `nome`, `sex`, `birthday`, `nazionalita`, `telephone`, `email`, `order_id`, `numero_di_passaporto`,`indirizzo_di_residenza`,`luogo_di_nascita`,`date_from`,`date_to`,`numero_di_visite`,`last_visit_from`,`last_visit_to`,`nome_dell_organizzazione`,`ufficio_dell_organizzazione`,`indirizzo_dell_organizzazione`,`telefono_dell_organizzazione`) VALUES (NULL, '', '".$user_id."', '$tour_id', '".addslashes($clientArray['cognome'])."', '".addslashes($clientArray['nome'])."', '".$clientArray['sex']."', '".self::dateToSQLformat($clientArray['nascita'])."', '".$clientArray['nazionalita']."', '".$clientArray['telephone']."', '".$clientArray['email']."', '$order_id', '".$clientArray['numero_di_passaporto']."','".$clientArray['indirizzo_di_residenza']."','".$clientArray['luogo_di_nascita']."','".self::dateToSQLformat($clientArray['date_from'])."','".self::dateToSQLformat($clientArray['date_to'])."','".$clientArray['numero_di_visite']."','".self::dateToSQLformat($clientArray['last_visit_from'])."','".self::dateToSQLformat($clientArray['last_visit_to'])."','".$clientArray['nome_dell_organizzazione']."','".$clientArray['ufficio_dell_organizzazione']."','".$clientArray['indirizzo_dell_organizzazione']."','".$clientArray['telefono_dell_organizzazione']."');";// return $query;
        $db = JFactory::getDbo();
        $db->setQuery($query);
        $result = $db->execute();

        return $db->insertid();
    }

    private static function dateToSQLformat($date)
    {
        $date = str_replace('/','-',$date);
        $date_timestamp = strtotime($date);
        return date('Y-m-d', $date_timestamp);
    }

    public function saveServices($order_id,$clientsCount,$manualPayment)
    {
        if (isset($_POST['service']) && is_array($_POST['service']) && count($_POST['service']))
        {
            $db = JFactory::getDbo();

            foreach ($_POST['service'] as $id=>$servicesArray)
            {
                foreach ($servicesArray as $service)
                {
                    if ($service['partner_id'] != '' && intval($service['partner_id']))
                    {
                        $service['amount_rub'] = str_replace(',','.',$service['amount_rub']);
                        $service['amount_euro'] = str_replace(',','.',$service['amount_euro']);

                        if ($service['amount_rub'] != '' || $service['amount_euro'] != '')
                        {
                            $amount_rub = 0;
                            $amount_euro = 0;
                            if (($service['amount_rub'] != '' && floatval($service['amount_rub'])))
                                $amount_rub = intval($service['amount_rub']*100);
                            if ($service['amount_euro'] != '' && floatval($service['amount_euro']))
                                $amount_euro = intval($service['amount_euro']*100);

                            $query = "SELECT r.*, o.full_payment FROM #__viaggio_orders_relations r JOIN #__viaggio_orders o ON o.id = r.order_id ".
                                " WHERE r.order_id = ".$order_id." AND r.partner_id = ".intval($service['partner_id'])." AND r.service_id = ".$id." ORDER BY r.id DESC";

                            $db->setQuery($query);
                            $old_data = $db->loadObject();

                            $not_payed_payments = "SELECT count(*) c FROM #__viaggio_manualpayments WHERE order_id = ".$order_id." AND payTime is null";
                            $db->setQuery($not_payed_payments);
                            $not_payed_payments = $db->loadObject();

                            if (!isset($old_data->id) || $old_data->full_payment == 0 || ($old_data->full_payment == 1 && ($not_payed_payments->c > 0 || date('Y-m',strtotime($manualPayment->payTime)) == date('Y-m'))))
                            {
                                if (isset($old_data->id))
                                {
                                    $query = "DELETE FROM `#__viaggio_orders_relations` WHERE order_id = ".$order_id;
                                    $db->setQuery($query);
                                    $db->execute();
                                }
                                $query = "INSERT INTO `#__viaggio_orders_relations` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`) VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".$amount_rub.",".$amount_euro.")";
                                $db->setQuery($query);
                                $db->execute();

                                $query = $db->getQuery(true);
                                $query->select('*');
                                $query->from('#__viaggio_services');
                                $query->where('id = '.$id);
                                $db->setQuery($query);
                                $current_service = $db->loadObject();

                                $query = $db->getQuery(true);
                                $query->select('*');
                                $query->from('#__viaggio_partners');
                                $query->where('id = '.intval($service['partner_id']));
                                $db->setQuery($query);
                                $current_partner = $db->loadObject();

                                $query = "INSERT INTO `#__viaggio_orders_relations_history` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`,`partner_name_rus`,`partner_name_ita`,`service_name_rus`,`service_name_ita`,`clients_count`) VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".$amount_rub.",".$amount_euro.",'".$current_partner->name_rus."','".$current_partner->name_ita."','".$current_service->name_rus."','".$current_service->name_ita."',$clientsCount)";
                                $db->setQuery($query);
                                $db->execute();
                            }
                            else
                            {
                                $query = "SELECT max(id) id, SUM(amount_rub) amount_rub, SUM(amount_euro) amount_euro FROM #__viaggio_orders_relations_postchanges ".
                                    "WHERE order_id = ".$order_id." AND partner_id = ".intval($service['partner_id'])." AND service_id = ".$id;
                                $db->setQuery($query);

                                $obj = $db->loadObject();

                                /*if (intval($service['partner_id']) == 28 && $id == 6)
                                {
                                    var_dump($query,'<hr>');
                                    var_dump($obj->amount_rub, $obj->amount_euro,'<hr>');
                                    var_dump($old_data->amount_rub, $old_data->amount_euro,'<hr>');
                                    var_dump($amount_rub,$amount_euro,'<hr>');
                                    var_dump(isset($obj->id),$obj->id,'<hr>');
                                    exit;
                                }*/

                                if (isset($obj->id) && $obj->id && (($obj->amount_rub + $old_data->amount_rub) != $amount_rub || ($obj->amount_euro + $old_data->amount_euro) != $amount_euro) )
                                {
                                    $query = "INSERT INTO `#__viaggio_orders_relations_postchanges` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`)".
                                        " VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".($amount_rub-($obj->amount_rub + $old_data->amount_rub)).",".($amount_euro-($obj->amount_euro + $old_data->amount_euro)).")";
                                    $db->setQuery($query);
                                    $db->execute();
                                }
                                elseif ((!isset($obj->id) || !$obj->id) && ($old_data->amount_euro != $amount_euro || $old_data->amount_rub != $amount_rub))
                                {
                                    $query = "INSERT INTO `#__viaggio_orders_relations_postchanges` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`)".
                                        " VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".($amount_rub-$old_data->amount_rub).",".($amount_euro-$old_data->amount_euro).")";
                                    $db->setQuery($query);
                                    $db->execute();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function saveCostoParziale($data)
    {
        if (isset($_POST['costo_parziale']) && intval($_POST['costo_parziale']) > 0)
        {
            $db = JFactory::getDbo();
            $query = "SELECT * FROM #__viaggio_month_statistic WHERE order_id = ".$data['id']." AND manualpaymant_id = 0";
            $db->setQuery($query);
            $month_statistic = $db->loadObject();
            if (isset($month_statistic->id))
            {
                $query = "UPDATE #__viaggio_month_statistic SET costo_parziale = ".intval($_POST['costo_parziale']*100)." WHERE id = ".$month_statistic->id;
                $db->setQuery($query);
                $db->execute();
            }
            else
            {
                $query = "INSERT INTO #__viaggio_month_statistic (order_id,costo_parziale,manualpaymant_id) VALUE (".$data['id'].",".intval($_POST['costo_parziale']*100).",0)";
                $db->setQuery($query);
                $db->execute();
            }
        }
    }

    private function saveHotels($order_id)
    {
        if ($order_id)
        {
            $cities_deleted = false;
            $hotels_deleted = false;
            $db = JFactory::getDbo();

            if (isset($_POST['city_id']) && count($_POST['city_id']))
            {
                foreach ($_POST['city_id'] as $k=>$city_id)
                {
                    if (isset($_POST['hotel_id'][$k]))
                        $hotel_id = $_POST['hotel_id'][$k];
                    else
                        $hotel_id = 0;
                    if ($hotel_id > 0)
                    {
                        if (!$hotels_deleted)
                        {
                            $query = "DELETE FROM #__viaggio_orders_hotels WHERE order_id = ".$order_id;
                            $db->setQuery($query);
                            $db->execute();
                            $hotels_deleted = true;
                        }
                        $query = "INSERT INTO #__viaggio_orders_hotels (`order_id`,`hotel_id`) VALUE (".$order_id.",".$hotel_id.")";
                        $db->setQuery($query);
                        $db->execute();
                    }
                    elseif (intval($city_id) > 0)
                    {
                        if (!$cities_deleted)
                        {
                            $query = "DELETE FROM #__viaggio_orders_cities WHERE order_id = ".$order_id;
                            $db->setQuery($query);
                            $db->execute();
                            $cities_deleted = true;
                        }

                        $query = "INSERT INTO #__viaggio_orders_cities (`order_id`,`city_id`) VALUE (".$order_id.",".intval($city_id).")";
                        $db->setQuery($query);
                        $db->execute();
                    }
                }
            }
        }
    }
}
