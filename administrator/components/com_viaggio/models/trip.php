<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Viaggio model.
 *
 * @since  1.6
 */
class ViaggioModelTrip extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_VIAGGIO';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_viaggio.trip';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Trip', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

    /**
     * Method to get the record form.
     *
     * @param	array	$data		An optional array of data for the form to interogate.
     * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
     * @return	JForm	A JForm object on success, false on failure
     * @since	1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Initialise variables.
        $app	= JFactory::getApplication();

        // Get the form.
        $form = $this->loadForm('com_viaggio.trip', 'trip', array('control' => 'jform', 'load_data' => $loadData));


        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return	mixed	The data for the form.
     * @since	1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_viaggio.edit.trip.data', array());

        if (empty($data)) {
            $data = $this->getItem();

        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param	integer	The id of the primary key.
     *
     * @return	mixed	Object on success, false on failure.
     * @since	1.6
     */
    public function getItem($pk = null)
    {
        if ($item = parent::getItem($pk)) {

            //Do any procesing on fields here if needed

        }

        return $item;
    }

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_trips');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

	public function save($data)
    {
        if (strpos($data['date_from'],'/')!==false)
        {
            $date_from = explode('/',$data['date_from']);
            $data['date_from'] = $date_from[2].'-'.$date_from[1].'-'.$date_from[0];
        }
        if (strpos($data['date_to'],'/')!==false)
        {
            $date_to = explode('/',$data['date_to']);
            $data['date_to'] = $date_to[2].'-'.$date_to[1].'-'.$date_to[0];
        }
        parent::save($data);
        $table      = $this->getTable();
        $key = $table->getKeyName();
        $pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');
        if ($pk)
        {
            $cities_deleted = false;
            $db = JFactory::getDbo();

            $query = "DELETE FROM #__viaggio_trips_hotels WHERE trip_id = ".$pk;
            $db->setQuery($query);
            $db->execute();
            if (isset($_POST['hotel_id']) && count($_POST['hotel_id']))
            {
                foreach ($_POST['hotel_id'] as $k=>$hotel_id)
                {
                    if ($hotel_id > 0)
                    {
                        $query = "INSERT INTO #__viaggio_trips_hotels (`trip_id`,`hotel_id`) VALUE (".$pk.",".$hotel_id.")";
                        $db->setQuery($query);
                        $db->execute();
                    }
                    elseif (intval($_POST['city_id'][$k]) > 0)
                    {
                        if (!$cities_deleted)
                        {
                            $query = "DELETE FROM #__viaggio_trips_cities WHERE trip_id = ".$pk;
                            $db->setQuery($query);
                            $db->execute();
                            $cities_deleted = true;
                        }

                        $query = "INSERT INTO #__viaggio_trips_cities (`trip_id`,`city_id`) VALUE (".$pk.",".intval($_POST['city_id'][$k]).")";
                        $db->setQuery($query);
                        $db->execute();
                    }
                }
            }

            return true;
        }
        return false;
    }
}
