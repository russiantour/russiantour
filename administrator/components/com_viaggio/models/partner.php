<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Viaggio model.
 *
 * @since  1.6
 */
class ViaggioModelPartner extends JModelAdmin
{
	/**
	 * @var      string    The prefix to use with controller messages.
	 * @since    1.6
	 */
	protected $text_prefix = 'COM_VIAGGIO';

	/**
	 * @var   	string  	Alias to manage history control
	 * @since   3.2
	 */
	public $typeAlias = 'com_viaggio.partner';

	/**
	 * @var null  Item data
	 * @since  1.6
	 */
	protected $item = null;

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   string  $type    The table type to instantiate
	 * @param   string  $prefix  A prefix for the table class name. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return    JTable    A database object
	 *
	 * @since    1.6
	 */
	public function getTable($type = 'Partner', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

    /**
     * Method to get the record form.
     *
     * @param	array	$data		An optional array of data for the form to interogate.
     * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
     * @return	JForm	A JForm object on success, false on failure
     * @since	1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Initialise variables.
        $app	= JFactory::getApplication();

        // Get the form.
        $form = $this->loadForm('com_viaggio.partner', 'partner', array('control' => 'jform', 'load_data' => $loadData));


        if (empty($form)) {
            return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return	mixed	The data for the form.
     * @since	1.6
     */
    protected function loadFormData()
    {
        // Check the session for previously entered form data.
        $data = JFactory::getApplication()->getUserState('com_viaggio.edit.partner.data', array());

        if (empty($data)) {
            $data = $this->getItem();

        }

        return $data;
    }

    /**
     * Method to get a single record.
     *
     * @param	integer	The id of the primary key.
     *
     * @return	mixed	Object on success, false on failure.
     * @since	1.6
     */
    public function getItem($pk = null)
    {
        if ($item = parent::getItem($pk)) {

            //Do any procesing on fields here if needed

        }

        return $item;
    }

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @param   JTable  $table  Table Object
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id))
		{
			// Set ordering to the last item if not set
			if (@$table->ordering === '')
			{
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_partners');
				$max             = $db->loadResult();
				$table->ordering = $max + 1;
			}
		}
	}

    public function save($data){
        $db = JFactory::getDbo();
	    if ($data['id'])
        {
            $id = $data['id'];
            parent::save($data);
            $db->setQuery("DELETE FROM #__viaggio_partners_services WHERE partner_id = '".$id."'");
            $db->execute();
        }
        else
        {
            $db->setQuery("INSERT INTO #__viaggio_partners (`name_rus`,`name_ita`) VALUES ('".$data['name_rus']."','".$data['name_ita']."')");
            $db->execute();
            $id = $db->insertid();
        }
        if (isset($_POST['services']) && is_array($_POST['services']) && count($_POST['services']))
        {
            foreach ($_POST['services'] as $service_id)
            {
                if (intval($service_id))
                {
                    $db->setQuery("INSERT INTO #__viaggio_partners_services (`partner_id`,`service_id`) VALUES (".$id.",".intval($service_id).")");
                    $db->execute();
                }
            }
        }
        return true;
    }
}
