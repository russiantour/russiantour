<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Viaggio records.
 *
 * @since  1.6
 */
class ViaggioModelOrders extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
                'ordering', 'a.`ordering`',
				'created_by', 'a.`created_by`',
				'currency', 'a.`currency`',
				'orderstatus', 'a.`orderstatus`',
				'errorcode', 'a.`errorcode`',
				'errormessage', 'a.`errormessage`',
				'amount', 'a.`amount`',
				'tour_id', 'a.`tour_id`',
				'clientcount', 'a.`clientcount`',
				'insurance', 'a.`insurance`',
				'stell', 'a.`stell`',
				'telephone', 'a.`telephone`',
				'description', 'a.`description`',
				'pole1', 'a.`pole1`',
				'email', 'a.`email`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering currency
		$this->setState('filter.currency', $app->getUserStateFromRequest($this->context.'.filter.currency', 'filter_currency', '', 'string'));
		$this->setState('filter.user', $app->getUserStateFromRequest($this->context.'.filter.user', 'filter_user', '', 'string'));

		// Filtering orderstatus
		$this->setState('filter.orderstatus', $app->getUserStateFromRequest($this->context.'.filter.orderstatus', 'filter_orderstatus', '', 'string'));

    // Filtering paymentstatus
        $this->setState('filter.paymentstatus', $app->getUserStateFromRequest($this->context.'.filter.paymentstatus', 'filter_paymentstatus', '', 'string'));

		// Load the parameters.
		$params = JComponentHelper::getParams('com_viaggio');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__viaggio_orders` AS a');


		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`, `a`.created_by AS `created_by_id`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		$user = JFactory::getUser();
        $isAdmin = in_array(8,$user->groups);
        if (!$isAdmin)
        {
            if (isset($_GET['owner']) && $_GET['owner'] == 'all')
                $query->where('a.created_by = 0');
            elseif (!isset($_GET['owner']) || $_GET['owner']=='my')
                $query->where('a.created_by = '.$user->id);
        }

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{

            if (stripos($search, 'id:') === 0)
            {
                $query->where('a.id = ' . (int) substr($search, 3));
            }
            else
            {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                if (isset($_POST['search_type']) && $_POST['search_type'] == 'people')
                {
                    $query->where("a.id IN (SELECT order_id FROM #__viaggio_clients c 
			                WHERE c.cognome LIKE ". $search ."
                            OR c.nome LIKE ". $search ."
			                OR c.numero_di_passaporto LIKE ". $search ."
                        )"
                    );
                }
                elseif (isset($_POST['search_type']) && $_POST['search_type'] == 'id')
                {
                    $query->where("a.id LIKE ".$search);
                }
                else
                {
                    $query->where('( a.errorcode LIKE ' . $search .
                        '  OR  a.description LIKE ' . $search .
                        '  OR  a.errormessage LIKE ' . $search .
                        '  OR  a.amount LIKE ' . $search .
                        '  OR  a.tour_id LIKE ' . $search .
                        '  OR  a.clientcount LIKE ' . $search .
                        '  OR  a.telephone LIKE ' . $search .
                        '  OR  a.email LIKE ' . $search .
                        '  OR  a.nomecgome LIKE ' . $search .
                        ' OR a.id LIKE ' . $search.
                        ' OR (a.id in (SELECT mp.order_id FROM #__viaggio_manualpayments mp WHERE mp.paymentID like '.$search.
                        ' OR mp.fio LIKE ' . $search.
                        ' OR mp.email LIKE ' . $search.
                        '))'.
                        ')'
                    );
                }
            }
		}


        //Filtering user
        $filter_user = $this->state->get("filter.user");
        if ($filter_user)
        {
            $query->where("a.`created_by` = '".$db->escape($filter_user)."'");
        }

		//Filtering currency
		$filter_currency = $this->state->get("filter.currency");
		if ($filter_currency)
		{
			$query->where("a.`currency` = '".$db->escape($filter_currency)."'");
		}

		//Filtering orderstatus
		$filter_orderstatus = $this->state->get("filter.orderstatus");
		if ($filter_orderstatus)
		{
			$query->where("a.`orderstatus` = '".$db->escape($filter_orderstatus)."'");
		}

        //Filtering paymentstatus
        $filter_paymentstatus = $this->state->get("filter.paymentstatus");

        if ($filter_paymentstatus && $filter_paymentstatus != 'a')
        {
            if ($filter_paymentstatus == 'f')
                $query->where("a.`full_payment` = 1");
            elseif ($filter_paymentstatus == 'p')
            {
                $query->join('LEFT', '#__viaggio_manualpayments AS `mp` ON `mp`.order_id = a.`id`');
                $query->where("a.`full_payment` = 0 AND mp.status > 0");
            }
            elseif ($filter_paymentstatus == 'n')
            {
                $query->where("a.payment_exists = 0");
            }

            if (isset($_GET['order_type']) && $_GET['order_type'] == 'requests')
            {
                $query->join('LEFT', '#__viaggio_manualpayments AS `mp` ON `mp`.order_id = a.`id`');
                $query->where("mp.status < 1");
            }
            else
            {
                $query->join('LEFT', '#__viaggio_manualpayments AS `mp` ON `mp`.order_id = a.`id`');
                $query->where("mp.status > 0");
            }
        }

        if (isset($_GET['from']) && intval($_GET['from']) > 0)
        {
            $query->where("a.status_from = ".intval($_GET['from']));
            $query->where("a.status = ".ViaggioHelpersViaggio::ORDER_STATUS_NEW);
        }

		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering','a.id');
		$orderDirn = $this->state->get('list.direction','desc');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
        $db = JFactory::getDbo();

        $query = $db->getQuery(true);
        $query->select('*');
        $query->from('#__viaggio_trips_categorys');
        $query->where('state is null or state <> -2');
        //$query->order('title_ita asc');
        $query->order('ordering ASC');
        $db->setQuery($query);
        $trips_categorys = $db->loadObjectList();
        $trips_categorys_array = [];
        foreach ($trips_categorys as $category)
        {
            $trips_categorys_array[$category->id] = $category;
        }

		foreach ($items as $k=>$oneItem) {
            $oneItem->currency = JText::_('COM_VIAGGIO_ORDERS_CURRENCY_OPTION_' . strtoupper($oneItem->currency));
            $oneItem->orderstatus = JText::_('COM_VIAGGIO_ORDERS_ORDERSTATUS_OPTION_' . strtoupper($oneItem->orderstatus));

			if (isset($oneItem->trip_id)) {
                $query = "SELECT * FROM `#__viaggio_trips` where id = ".$oneItem->trip_id;
                $db->setQuery($query);
                $results = $db->loadObject();

                if ($results) {
                    $items[$k]->tour = $results;
                    $items[$k]->tour->category = $trips_categorys_array[$results->cat_id];
                }
			}

			$query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = ".$oneItem->id;
			$db->setQuery($query);
            $items[$k]->clients = $db->loadObjectList();

            $query = "SELECT * FROM #__viaggio_orders_status WHERE order_id = " . $oneItem->id;
            $db->setQuery($query);
            $items[$k]->accordo = $db->loadObject();
		}
		return $items;
	}
}
