<?php

/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2020. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Oleg <kaktus.mov@gmail.com> -
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class ViaggioModelTourscomments extends JModelList {

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
            $this->getState(
                'list.select', 'a.*'
            )
        );
        $query->from('`#__viaggio_tours_comments` AS a');
        if ($tour_id = $this->state->get('tour_id',false))
            $query->where('a.tour_id = '.$tour_id);

        return $query;
    }
}
