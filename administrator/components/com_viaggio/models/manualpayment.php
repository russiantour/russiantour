<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> -
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modeladmin');

/**
 * Viaggio model.
 */
class ViaggioModelManualpayment extends JModelAdmin
{
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since	1.6
	 */
	protected $text_prefix = 'COM_TOURISTINVITE';
    protected $event_after_save = 'onExtensionAfterSave';

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = 'Manualpayment', $prefix = 'ViaggioTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param	array	$data		An optional array of data for the form to interogate.
	 * @param	boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return	JForm	A JForm object on success, false on failure
	 * @since	1.6
	 */
	public function getForm($data = array(), $loadData = true)
	{
		// Initialise variables.
		$app	= JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_viaggio.Manualpayment', 'Manualpayment', array('control' => 'jform', 'load_data' => $loadData));


		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return	mixed	The data for the form.
	 * @since	1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_viaggio.edit.manualpayment.data', array());

		if (empty($data)) {
			$data = $this->getItem();

		}

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param	integer	The id of the primary key.
	 *
	 * @return	mixed	Object on success, false on failure.
	 * @since	1.6
	 */
	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk)) {

			//Do any procesing on fields here if needed
            $db = JFactory::getDbo();

            if ($item->trip_id && $item->trip_id > 0)
            {
                $query = $db->getQuery(true);
                $query->select('*');
                $query->from('#__viaggio_trips');
                $query->where('id = '.$item->trip_id);
                $db->setQuery($query);
                $item->trip = $db->loadObject();
            }
            else
                $item->trip = false;

            if ($item->order_id)
            {
                $query = "SELECT * FROM #__viaggio_orders WHERE id = ".$item->order_id;
                $db->setQuery($query);
                $order = $db->loadObject();

                $date_from = explode('-',$order->date_from);
                $date_to = explode('-',$order->date_to);

                $item->date_from = $date_from[1].'/'.$date_from[2].'/'.$date_from[0];
                $item->date_to = $date_to[1].'/'.$date_to[2].'/'.$date_to[0];
                $item->order_euro_amount = $order->valute_amount;
                $item->client_type = $order->client_type;
                $item->cancellation = $order->cancellation;
                $item->cancellation_ita = $order->cancellation_ita;
            }
            else
            {
                $item->date_from = '';
                $item->date_to = '';
                $item->order_euro_amount = '';
                $item->client_type = '';
            }
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since	1.6
	 */
	protected function prepareTable($table)
	{
		jimport('joomla.filter.output');

		if (empty($table->id)) {

			// Set ordering to the last item if not set
			if (@$table->ordering === '') {
				$db = JFactory::getDbo();
				$db->setQuery('SELECT MAX(ordering) FROM #__viaggio_manualpayments');
				$max = $db->loadResult();
				$table->ordering = $max+1;
			}

		}
	}


    public function save($data){
        $data['cancellation'] = addslashes($data['cancellation']);
        $data['cancellation_ita'] = addslashes($data['cancellation_ita']);
        $db = JFactory::getDbo();
        if (!$data['id'])
        {
            $data['paymentID'] = 'm'.time();
            $data['timeCreatedLinl'] = date('Y-m-d H:i:s');
        }
        else
        {
            $query = "SELECT * FROM #__viaggio_manualpayments WHERE id = ".$data['id'];
            $db->setQuery($query);
            $MPayment = $db->loadObject();
            if ($MPayment->payment_initiated > 0)
                $data['amountEuro'] = $MPayment->amountEuro;
            /*if ($MPayment->full_payment == 1)
                return false;*/
        }
        $user = JFactory::getUser();
        if (!$data['id'])
            $data['userId'] = $user->get('id');

        $curs = false;
        $cursFile = file_get_contents('curs.txt');
        if ($cursFile)
        {
            $cursFile = explode('|',$cursFile);
            if ((date('H')+3)<14 || date('d.m.Y')==date('d.m.Y',$cursFile[1]))
            {
                $curs = $cursFile[0];
            }
        }
        if (!$curs)
        {
            $xml = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y'));
            if ($xml)
            {
                $movies = new SimpleXMLElement($xml);
                foreach ($movies->Valute as $valute){
                    if ($valute->CharCode == 'EUR')
                    {
                        $curs = floatval(str_replace(',','.',$valute->Value));
                        file_put_contents('curs.txt',$curs.'|'.time().'|'.date('d.m.Y H:i:s'));
                        break;
                    }
                }
            }
            elseif ($cursFile[0])
                $curs = $cursFile[0];
            else
                $curs = 85;
        }
        $data['curs'] = $curs;
        $data['amountRub'] = $data['amountEuro']*100*$curs;
        $isNew = false;

        $date_from = explode('/',$_POST['date_from']);
        $date_from = $date_from[2].'-'.$date_from[1].'-'.$date_from[0];

        $date_to = explode('/',$_POST['date_to']);
        $date_to = $date_to[2].'-'.$date_to[1].'-'.$date_to[0];

        if (!$data['id'] && (!$data['order_id'] || $data['order_id'] == ''))
        {
            $isNew = true;
            $insert_details = true;

            $order_id = time();
            $data['order_id'] = $order_id;

            $total_eur = $_POST['total_amount'];
            $total_rub = round( $curs * 1.00 * $total_eur * 100 );

            $clients = $_POST['clients'];

            $full_payment = ($data['full_payment'] == 1)?1:0;

            $doOrderQuery = "INSERT INTO `#__viaggio_orders` (`id`, `asset_id`, `created_by`, `currency`, `orderstatus`, `errorcode`, `errormessage`, `amount`, `valute_amount`, `valute_rate`, `trip_id`, `clientcount`, `insurance`, `stell`, `nomecgome`, `telephone`, `email`,`date_from`,`date_to`,`full_payment`,`pole1`,`bank`,`check_type`,`client_type`,`cancellation`,`cancellation_ita`) VALUES (".$order_id.", '0', '".$user->get('id')."', '', '-1', '0', '', '".$total_rub."', '".$total_eur."', '".$curs."', '".$data['trip_id']."', ".count($clients).", '".$_POST['insurance']."', '".$_POST['stellcount']."', '".addslashes($data['fio'])."', '".$data['telefon']."', '".$data['email']."','".$date_from."','".$date_to."',".$full_payment.",'".addslashes($data['pole1'])."','".$data['bank']."',".$data['check_type'].",".$data['client_type'].",'".$data['cancellation']."','".$data['cancellation_ita']."');";
            $db->setQuery($doOrderQuery);
            $db->execute();

            $this->saveServices($order_id,count($clients));

            if (count($clients))
            {
                foreach ($clients as $client)
                    self::createClient($client, $order_id, $data['userId'], $data['trip_id']);
            }
        }
        else
        {
            $insert_details = false;

            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM #__viaggio_orders WHERE id = '".$data['order_id']."' LIMIT 1");
            $order = $db->loadObject();
            $data['trip_id'] = $order->trip_id;

            if (!isset($data['bank']) || !$data['bank'] || $data['bank']=='')
                $data['bank'] = $order->bank;

            if (!isset($data['check_type']) || !$data['check_type'] || $data['check_type']=='')
                $data['check_type'] = $order->check_type;

            $cp2018 = floatval(str_replace(',','.',$_POST['cp2018']));
            if ($data['full_payment']==1 && $cp2018)
            {
                $db->setQuery("UPDATE #__viaggio_month_statistic SET costo_parziale = ".($cp2018*100)." WHERE order_id = '".$data['order_id']."' ORDER BY id ASC LIMIT 1");
                $db->execute();

                $db->setQuery("UPDATE #__viaggio_orders SET cancellation = '".$data['cancellation']."', cancellation_ita = '".$data['cancellation_ita']."', cp2018 = 1 WHERE id = '".$data['order_id']."' LIMIT 1");
                $db->execute();
            }
            if ($data['full_payment']==1)
            {
                $db->setQuery("UPDATE #__viaggio_orders SET cancellation = '".$data['cancellation']."', cancellation_ita = '".$data['cancellation_ita']."', full_payment = 1 WHERE id = '".$data['order_id']."' LIMIT 1");
                $db->execute();
            }

            if ($_POST['date_from'] != '' && $_POST['date_to'] != '')
            {
                $db->setQuery("UPDATE #__viaggio_orders SET cancellation = '".$data['cancellation']."', cancellation_ita = '".$data['cancellation_ita']."', date_from = '".$date_from."', date_to = '".$date_to."' WHERE id = '".$data['order_id']."' LIMIT 1");
                $db->execute();
            }
        }

        $result = parent::save($data);
        if ($isNew)
        {
            $this->cp2018($order_id);
        }
        if ($insert_details && $result)
        {
            $db = JFactory::getDbo();
            $db->setQuery("DELETE FROM #__viaggio_manualpayments_details WHERE payment_id = '".$data['paymentID']."'");
            $db->execute();

            foreach ($_POST['air-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','air-ticket','".$data['paymentID']."')");
                    $db->execute();
                }
            }
            foreach ($_POST['rail-ticket'] as $key=>$val)
            {
                foreach ($val as $name => $field)
                {
                    $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('".$name."','".$field."','".$key."','rail-ticket','".$data['paymentID']."')");
                    $db->execute();
                }
            }
            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-amount','".$_POST['other-amount']."','0','other','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-rus','".$_POST['other-rus']."','0','other','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('other-ita','".$_POST['other-ita']."','0','other','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-amount','".$_POST['visa-amount']."','0','visa','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-rus','".$_POST['visa-rus']."','0','visa','".$data['paymentID']."')");
            $db->execute();

            $db->setQuery("INSERT INTO #__viaggio_manualpayments_details (field_name,field_value,field_group,field_group_name,payment_id) VALUES ('visa-ita','".$_POST['visa-ita']."','0','visa','".$data['paymentID']."')");
            $db->execute();
        }
        $this->saveHotels($data['order_id']);
        JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_viaggio&view=orders', false));
    }

    private static function createClient($clientArray, $order_id, $user_id, $trip_id){
        $query = "INSERT INTO `#__viaggio_clients` (`id`, `asset_id`, `created_by`, `trip_id`, `cognome`, `nome`, `sex`, `birthday`, `nazionalita`, `telephone`, `email`, `order_id`, `numero_di_passaporto`,`indirizzo_di_residenza`,`luogo_di_nascita`,`date_from`,`date_to`,`numero_di_visite`,`last_visit_from`,`last_visit_to`,`nome_dell_organizzazione`,`ufficio_dell_organizzazione`,`indirizzo_dell_organizzazione`,`telefono_dell_organizzazione`) VALUES (NULL, '', '".$user_id."', '$trip_id', '".addslashes($clientArray['cognome'])."', '".addslashes($clientArray['nome'])."', '".$clientArray['sex']."', '".self::dateToSQLformat($clientArray['nascita'])."', '".$clientArray['nazionalita']."', '".$clientArray['telephone']."', '".$clientArray['email']."', '$order_id', '".$clientArray['numero_di_passaporto']."','".$clientArray['indirizzo_di_residenza']."','".$clientArray['luogo_di_nascita']."','".$clientArray['date_from']."','".$clientArray['date_to']."','".$clientArray['numero_di_visite']."','".$clientArray['last_visit_from']."','".$clientArray['last_visit_to']."','".$clientArray['nome_dell_organizzazione']."','".$clientArray['ufficio_dell_organizzazione']."','".$clientArray['indirizzo_dell_organizzazione']."','".$clientArray['telefono_dell_organizzazione']."');";// return $query;
        $db = JFactory::getDbo();
        $db->setQuery($query);
        $result = $db->execute();

        return $db->insertid();
    }

    private static function dateToSQLformat($date)
    {
        $date = str_replace('/','-',$date);
        $date_timestamp = strtotime($date);
        return date('Y-m-d', $date_timestamp);
    }

    public function saveServices($order_id,$clientsCount)
    {
        if (isset($_POST['service']) && is_array($_POST['service']) && count($_POST['service']))
        {
            $db = JFactory::getDbo();
            foreach ($_POST['service'] as $id=>$servicesArray)
            {
                foreach ($servicesArray as $service)
                {
                    if ($service['partner_id'] != '' && intval($service['partner_id']))
                    {
                        if (($service['amount_rub'] != '' && intval($service['amount_rub'])) || ($service['amount_euro'] != '' && intval($service['amount_euro'])))
                        {
                            $amount_rub = 0;
                            $amount_euro = 0;
                            if (($service['amount_rub'] != '' && intval($service['amount_rub'])))
                                $amount_rub = intval($service['amount_rub']*100);
                            if ($service['amount_euro'] != '' && intval($service['amount_euro']))
                                $amount_euro = intval($service['amount_euro']*100);

                            $query = "INSERT INTO `#__viaggio_orders_relations` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`) VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".$amount_rub.",".$amount_euro.")";
                            $db->setQuery($query);
                            $db->execute();

                            $query = $db->getQuery(true);
                            $query->select('*');
                            $query->from('#__viaggio_services');
                            $query->where('id = '.$id);
                            $db->setQuery($query);
                            $current_service = $db->loadObject();

                            $query = $db->getQuery(true);
                            $query->select('*');
                            $query->from('#__viaggio_partners');
                            $query->where('id = '.intval($service['partner_id']));
                            $db->setQuery($query);
                            $current_partner = $db->loadObject();

                            $query = "INSERT INTO `#__viaggio_orders_relations_history` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`,`partner_name_rus`,`partner_name_ita`,`service_name_rus`,`service_name_ita`,`clients_count`) VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".$amount_rub.",".$amount_euro.",'".$current_partner->name_rus."','".$current_partner->name_ita."','".$current_service->name_rus."','".$current_service->name_ita."',$clientsCount)";
                            $db->setQuery($query);
                            $db->execute();
                        }
                    }
                }
            }
        }
    }

    public function cp2018($order_id)
    {
        $needInsert = false;
        foreach ($_POST['cp2018_full'] as $key=>$val)
        {
            if ($val!='')
            {
                $needInsert = true;
                break;
            }
        }

        if ($needInsert)
        {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = ".$order_id." ORDER BY id DESC LIMIT 1");
            $manualpayment_id = $db->loadObject()->id;

            $db->setQuery("UPDATE #__viaggio_orders SET cp2018 = 1 WHERE id = ".$order_id);
            $db->execute();

            if (isset($_POST['cp2018_full']['full_payment']))
            {
                $db->setQuery("UPDATE #__viaggio_manualpayments SET full_payment = 1 WHERE id = ".$manualpayment_id);
                $db->execute();

                $db->setQuery("UPDATE #__viaggio_orders SET full_payment = 1 WHERE id = ".$order_id);
                $db->execute();
            }

            $full_summ = floatval(str_replace(',','.',$_POST['cp2018_full']['full_summ']))*100;
            $acconto = floatval(str_replace(',','.',$_POST['cp2018_full']['acconto']))*100;
            $curs = floatval(str_replace(',','.',$_POST['cp2018_full']['curs']))*100;
            $how_much_paid = floatval(str_replace(',','.',$_POST['cp2018_full']['how_much_paid']))*100;
            $payments_for_partners_summ = floatval(str_replace(',','.',$_POST['cp2018_full']['payments_for_partners_summ']))*100;
            $costo_parziale = floatval(str_replace(',','.',$_POST['cp2018_full']['costo_parziale']))*100;
            $total_profit = floatval(str_replace(',','.',$_POST['cp2018_full']['total_profit']))*100;
            $guadagno_parziale = floatval(str_replace(',','.',$_POST['cp2018_full']['guadagno_parziale']))*100;
            $profit = floatval(str_replace(',','.',$_POST['cp2018_full']['profit']))*100;

            $db->setQuery("INSERT INTO #__viaggio_month_statistic (`order_id`,`manualpaymant_id`,`full_summ`,`acconto`,`curs`,`how_much_paid`,`payments_for_partners_summ`,`costo_parziale`,`total_profit`,`guadagno_parziale`,`profit`) VALUES (".$order_id.",0,".$full_summ.",".$acconto.",".$curs.",".$how_much_paid.",".$payments_for_partners_summ.",".$costo_parziale.",".$total_profit.",".$guadagno_parziale.",".$profit.")");
            $db->execute();
        }

        return $needInsert;
    }

    private function saveHotels($order_id)
    {
        if ($order_id)
        {
            $cities_deleted = false;
            $db = JFactory::getDbo();

            $query = "DELETE FROM #__viaggio_orders_hotels WHERE order_id = ".$order_id;
            $db->setQuery($query);
            $db->execute();
            if (isset($_POST['hotel_id']) && count($_POST['hotel_id']))
            {
                foreach ($_POST['hotel_id'] as $k=>$hotel_id)
                {
                    if ($hotel_id > 0)
                    {
                        $query = "INSERT INTO #__viaggio_orders_hotels (`order_id`,`hotel_id`) VALUE (".$order_id.",".$hotel_id.")";
                        $db->setQuery($query);
                        $db->execute();
                    }
                    elseif (intval($_POST['city_id'][$k]) > 0)
                    {
                        if (!$cities_deleted)
                        {
                            $query = "DELETE FROM #__viaggio_orders_cities WHERE order_id = ".$order_id;
                            $db->setQuery($query);
                            $db->execute();
                            $cities_deleted = true;
                        }

                        $query = "INSERT INTO #__viaggio_orders_cities (`order_id`,`city_id`) VALUE (".$order_id.",".intval($_POST['city_id'][$k]).")";
                        $db->setQuery($query);
                        $db->execute();
                    }
                }
            }
        }
    }
}
