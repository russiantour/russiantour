<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Viaggio records.
 *
 * @since  1.6
 */
class ViaggioModelTours extends JModelList
{
/**
	* Constructor.
	*
	* @param   array  $config  An optional associative array of configuration settings.
	*
	* @see        JController
	* @since      1.6
	*/
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.`id`',
				'ordering', 'a.`ordering`',
				'state', 'a.`state`',
				'created_by', 'a.`created_by`',
				'modified_by', 'a.`modified_by`',
				'name', 'a.`name`',
				'alias', 'a.`alias`',
				'desc', 'a.`desc`',
				'category', 'a.`category`',
				'from', 'a.`from`',
				'to', 'a.`to`',
				'inn4count', 'a.`inn4count`',
				'inn3count', 'a.`inn3count`',
				'inn4cost', 'a.`inn4cost`',
				'inn3cost', 'a.`inn3cost`',
				'halfboard4', 'a.`halfboard4`',
				'halfboard3', 'a.`halfboard3`',
				'singola4', 'a.`singola4`',
				'singola3', 'a.`singola3`',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $app->getUserStateFromRequest($this->context . '.filter.state', 'filter_published', '', 'string');
		$this->setState('filter.state', $published);
		// Filtering category
		$this->setState('filter.category', $app->getUserStateFromRequest($this->context.'.filter.category', 'filter_category', '', 'string'));

		// Filtering from
		$this->setState('filter.from.from', $app->getUserStateFromRequest($this->context.'.filter.from.from', 'filter_from_from', '', 'string'));
		$this->setState('filter.from.to', $app->getUserStateFromRequest($this->context.'.filter.from.to', 'filter_to_from', '', 'string'));

		// Filtering to
		$this->setState('filter.to.from', $app->getUserStateFromRequest($this->context.'.filter.to.from', 'filter_from_to', '', 'string'));
		$this->setState('filter.to.to', $app->getUserStateFromRequest($this->context.'.filter.to.to', 'filter_to_to', '', 'string'));


		// Load the parameters.
		$params = JComponentHelper::getParams('com_viaggio');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('a.name', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id  A prefix for the store id.
	 *
	 * @return   string A store id.
	 *
	 * @since    1.6
	 */
	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select', 'DISTINCT a.*'
			)
		);
		$query->from('`#__viaggio_tours` AS a');

		// Join over the users for the checked out user
		$query->select("uc.name AS uEditor");
		$query->join("LEFT", "#__users AS uc ON uc.id=a.checked_out");

		// Join over the user field 'created_by'
		$query->select('`created_by`.name AS `created_by`');
		$query->join('LEFT', '#__users AS `created_by` ON `created_by`.id = a.`created_by`');

		// Join over the user field 'modified_by'
		$query->select('`modified_by`.name AS `modified_by`');
		$query->join('LEFT', '#__users AS `modified_by` ON `modified_by`.id = a.`modified_by`');
		// Join over the category 'category'
		$query->select('`category`.title AS `category`');
		$query->join('LEFT', '#__categories AS `category` ON `category`.id = a.`category`');

		// Filter by published state
		$published = $this->getState('filter.state');

		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state IN (0, 1))');
		}

		// Filter by search in title
		$search = $this->getState('filter.search');

		if (!empty($search))
		{
			if (stripos($search, 'id:') === 0)
			{
				$query->where('a.id = ' . (int) substr($search, 3));
			}
			else
			{
				$search = $db->Quote('%' . $db->escape($search, true) . '%');
				$query->where('( a.name LIKE ' . $search . '  OR  a.category LIKE ' . $search . '  OR  a.inn4count LIKE ' . $search . '  OR  a.inn3count LIKE ' . $search . '  OR  a.inn4cost LIKE ' . $search . '  OR  a.inn3cost LIKE ' . $search . '  OR  a.halfboard4 LIKE ' . $search . '  OR  a.halfboard3 LIKE ' . $search . '  OR  a.singola4 LIKE ' . $search . '  OR  a.singola3 LIKE ' . $search . ' )');
			}
		}


		//Filtering category
		$filter_category = $this->state->get("filter.category");
		if ($filter_category)
		{
			$query->where("a.`category` = '".$db->escape($filter_category)."'");
		}

		//Filtering from
		$filter_from_from = $this->state->get("filter.from.from");
		if ($filter_from_from) {
			$query->where("a.`from` >= '".$db->escape($filter_from_from)."'");
		}
		$filter_from_to = $this->state->get("filter.from.to");
		if ($filter_from_to) {
			$query->where("a.`from` <= '".$db->escape($filter_from_to)."'");
		}

		//Filtering to
		$filter_to_from = $this->state->get("filter.to.from");
		if ($filter_to_from) {
			$query->where("a.`to` >= '".$db->escape($filter_to_from)."'");
		}
		$filter_to_to = $this->state->get("filter.to.to");
		if ($filter_to_to) {
			$query->where("a.`to` <= '".$db->escape($filter_to_to)."'");
		}
		// Add the list ordering clause.
		$orderCol  = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		if ($orderCol && $orderDirn)
		{
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}

		return $query;
	}

	/**
	 * Get an array of data items
	 *
	 * @return mixed Array of data items on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();

		return $items;
	}
}
