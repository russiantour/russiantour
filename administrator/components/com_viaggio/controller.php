<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Class ViaggioController
 *
 * @since  1.6
 */
class ViaggioController extends JControllerLegacy
{
    const CHECK_TYPE_RUSSIANTOUR = 1;
    const CHECK_TYPE_VISTO = 2;

    public $paylerUrl = 'https://secure.payler.com/gapi/';
    public $paylerKey = '562cd71d-5d66-4f10-860a-0fcaaf877da9';
    public $paylerKey2 = '6f140627-6287-43a1-b8a3-363c41366d24';
    public $paymentUrl = '3dsec.paymentgate.ru/ipay';
    /**
     * Method to display a view.
     *
     * @param   boolean  $cachable   If true, the view output will be cached
     * @param   mixed    $urlparams  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return   JController This object to support chaining.
     *
     * @since    1.5
     */
    public function display($cachable = false, $urlparams = false)
    {
        $view = JFactory::getApplication()->input->getCmd('view', 'tours');
        JFactory::getApplication()->input->set('view', $view);

        parent::display($cachable, $urlparams);

        return $this;
    }

    private function manualPayment()
    {
        if (isset($_GET['manualPayment']))
        {
            $curs = false;
            $cursFile = file_get_contents('curs.txt');
            if ($cursFile)
            {
                $cursFile = explode('|',$cursFile);
                if ((date('H')+3)<14 || date('d.m.Y')==date('d.m.Y',$cursFile[1]))
                {
                    $curs = $cursFile[0];
                }
            }
            if (!$curs)
            {
                $xml = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y'));
                if ($xml)
                {
                    $movies = new SimpleXMLElement($xml);
                    foreach ($movies->Valute as $valute){
                        if ($valute->CharCode == 'EUR')
                        {
                            $curs = floatval(str_replace(',','.',$valute->Value));
                            file_put_contents('curs.txt',$curs.'|'.time().'|'.date('d.m.Y H:i:s'));
                            break;
                        }
                    }
                }
                elseif ($cursFile[0])
                    $curs = $cursFile[0];
                else
                    $curs = 85;
            }
            $code = strip_tags(addslashes($_GET['code']));
            $db = JFactory::getDBO();
            $query = 'SELECT *  FROM #__viaggio_manualpayments WHERE status = 0 AND '.
                'paymentID = \''.$code.'\' LIMIT 1' ;
            $db->setQuery($query);
            $payment = $db->loadAssoc();
            if ($payment['id']){
                $payTime = strtotime($payment['timeCreatedLinl']);

                $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
                $db->setQuery($query);
                $curTime = $db->loadAssoc();
                $curTime = strtotime($curTime['curTime']);

//                if ($curTime >= $payTime+3600*24){
                if ($curTime >= $payTime+10){

                    echo '24 hours expired';
                    exit;
                }
                $cost = intval($payment['amountEuro']*100*$curs*1.00);

                $query = 'UPDATE #__viaggio_manualpayments SET'.
                    ' amountRub = '.$cost.','.
                    ' curs = '.$curs.','.
                    ' status = 0 ,'.
                    " payTime = '".date('Y-m-d H:i:s')."'".
                    ' WHERE paymentID = \''.$payment['paymentID'].'\'';
                $db->setQuery($query);
                $db->execute();

                $userName = JFactory::getApplication()->get('REBuserName');
                $password = JFactory::getApplication()->get('REBpassword');

                $req = 'https://'.$this->paymentUrl.'/rest/registerPreAuth.do?'.//dvuhstadiyniy platezh
                    'userName='.$userName.
                    '&password='.$password.
                    '&orderNumber='.$payment['paymentID'].
                    '&amount='.$cost.
                    '&description='.urlencode($payment['description']).
                    '&pole1='.urlencode($payment['pole']).
                    '&language=en'.
                    //'&returnUrl=http://www.visto-russia.com/step2?getResponse=1';
                    '&returnUrl='.JFactory::getApplication()->get('REBreturnUrlDomen').'/modulo-visto-turistico?getResponse=1';
                $resp = file_get_contents($req);
                $resp = json_decode($resp);
                if ($resp->formUrl)
                {
                    $db = JFactory::getDBO();
                    $query = "UPDATE #__viaggio_manualpayments SET outOrderID = '".$resp->orderId."' WHERE  id = ".intval($payment['id']);
                    $db->setQuery($query);
                    if ($db->execute())
                    {
                        header('Location: '.$resp->formUrl);
                        exit;
                    }
                    echo 'error!';
                }
            }
            else{
                echo 'ERROR! Payment not found!';
            }
            echo 'ERROR LAST!';
            exit;
        }
    }

    public function sendLink($cachable = false, $urlparams = false){
        $db = JFactory::getDBO();

        $query = 'SELECT * FROM #__viaggio_manualpayments WHERE id = '.intval($_GET['id']).' limit 1';

        $db->setQuery($query);
        $rez = $db->loadAssocList();
        ViaggioHelpersViaggio::sendMail($rez[0],'../administrator/components/com_viaggio/mailTemplates/sendLink.php');
        $this->display($cachable = false, $urlparams = false);
    }

    public function cancelmanual(){
        if (intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET status = 2 '.
                'WHERE id = '.intval($_GET['id']);
            //echo $query;
            $db->setQuery($query);
            $db->execute();
        }
        $this->display($cachable = false, $urlparams = false);
    }

    public function uncancelmanual(){
        if (intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET status = 0 '.
                'WHERE id = '.intval($_GET['id']);
            //echo $query;
            $db->setQuery($query);
            $db->execute();
        }
        $this->display($cachable = false, $urlparams = false);
    }

    public function deletemanual(){
        if (intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET status = 3 '.
                'WHERE id = '.intval($_GET['id']);
            //echo $query;
            $db->setQuery($query);
            $db->execute();
        }
        $this->display($cachable = false, $urlparams = false);
    }

    public function printpdf()
    {
        //получаем запрос
        $app  = JFactory::getApplication();
        $order_id = $app->input->getInt('order_id', 0);
        $sendmail = $app->input->getString('sendmail', '');
        $template = $app->input->getString('template', '');

        $productNoPrefix = '';
        if (isset($_GET['paymentID']))
        {
            $db = JFactory::getDbo();
            $query1 = "SELECT * FROM `#__viaggio_manualpayments` WHERE paymentID = '".$_GET['paymentID']."' ORDER BY id ASC LIMIT 1";
            $db->setQuery($query1);
            $manualPayment = $db->loadObject();

            if ($manualPayment->status == 1)
                $productNoPrefix = 'С';
            elseif ($manualPayment->status == 2)
                $productNoPrefix = 'В';

            $order_id = $manualPayment->order_id;

            if (!in_array($manualPayment->bank,['payler','tinkoff','СовкомБанк']) || !in_array($manualPayment->check_type,[$this::CHECK_TYPE_RUSSIANTOUR,$this::CHECK_TYPE_VISTO]))
            {
                if ($template == 'printPdfPagareR')
                {
                    $query = "UPDATE #__viaggio_manualpayments SET bank = 'payler', check_type = '".$this::CHECK_TYPE_RUSSIANTOUR."' WHERE paymentID = '".$_GET['paymentID']."' LIMIT 1";
                    $db->setQuery($query);
                    $db->execute();
                }
                elseif ($template == 'printPdfPagareRT')
                {
                    $query = "UPDATE #__viaggio_manualpayments SET bank = 'tinkoff', check_type = '".$this::CHECK_TYPE_RUSSIANTOUR."' WHERE paymentID = '".$_GET['paymentID']."' LIMIT 1";
                    $db->setQuery($query);
                    $db->execute();
                }
                elseif ($template == 'printPdfPagareV')
                {
                    $query = "UPDATE #__viaggio_manualpayments SET bank = 'payler', check_type = '".$this::CHECK_TYPE_VISTO."' WHERE paymentID = '".$_GET['paymentID']."' LIMIT 1";
                    $db->setQuery($query);
                    $db->execute();
                }
                elseif ($template == 'printPdfPagareVT')
                {
                    $query = "UPDATE #__viaggio_manualpayments SET bank = 'tinkoff', check_type = '".$this::CHECK_TYPE_VISTO."' WHERE paymentID = '".$_GET['paymentID']."' LIMIT 1";
                    $db->setQuery($query);
                    $db->execute();
                }
            }
            else
            {
                if ($manualPayment->check_type == $this::CHECK_TYPE_RUSSIANTOUR && ($manualPayment->bank == 'payler' || $manualPayment->bank == 'СовкомБанк'))
                    $template = 'printPdfPagareR';
                elseif ($manualPayment->check_type == $this::CHECK_TYPE_RUSSIANTOUR && $manualPayment->bank == 'tinkoff')
                    $template = 'printPdfPagareRT';
                elseif ($manualPayment->check_type == $this::CHECK_TYPE_VISTO && ($manualPayment->bank == 'payler' || $manualPayment->bank == 'СовкомБанк'))
                    $template = 'printPdfPagareV';
                elseif ($manualPayment->check_type == $this::CHECK_TYPE_VISTO && $manualPayment->bank == 'tinkoff')
                    $template = 'printPdfPagareVT';
            }
        }

        //подключаем хелпер фронта для использования фукнции почты и печати pdf
        include_once JPATH_COMPONENT_SITE.'/helpers/frontend.php';

        //подключаем настройки
        include_once JPATH_COMPONENT_SITE.'/helpers/settings.php';

        //получаем данные заказа
        $order = ViaggioHelpersFrontend::getOrderObject($order_id);
        $order->number = $order_id;

        //echo '<pre>'.print_r($order,1).'</pre>'; die();

        //печать пдф
        $filename = ViaggioHelpersFrontend::printPdf($template, $order, true, $productNoPrefix);
        $pdfPath = JPATH_COMPONENT_SITE.'/pdf/'.$filename;

        if($sendmail)
            ViaggioHelpersFrontend::sendEmailByTemplate($sendmail,$order->email,$settings, $order, $pdfPath.'.pdf');

        die();
    }

    public function downloadpdf()
    {
        $this->printdoc(true);
    }

    public function accordopdf()
    {
        $this->accordo(true);
    }

    public function accordoCreate()
    {
        $date_open = date('Y-m-d H:i:s');

        $data = $this->accordo(true, false, $date_open);

        $db = JFactory::getDbo();

        $query = "INSERT INTO #__viaggio_orders_status (order_id, status, date_open, pdfFilePath) VALUES (" . $data['order_id'] . ", 0, '" . $date_open . "', '" . $data['pdfFilePath'] . "')";
        $db->setQuery($query);
        $db->execute();

        $this->setRedirect('index.php?option=com_viaggio&view=orders', false);
    }

    public function printdoc($downloadpdf = false)
    {
        //получаем запрос
        $app  = JFactory::getApplication();
        $order_id = $app->input->getInt('order_id', 0);

        //подключаем хелпер фронта для использования фукнции почты и печати pdf
        include_once JPATH_COMPONENT_SITE.'/helpers/frontend.php';

        //подключаем настройки
        include_once JPATH_COMPONENT_SITE.'/helpers/settings.php';

        //получаем данные заказа
        $item = ViaggioHelpersFrontend::getOrderObject($order_id);
        $item->number = $order_id;

        $db = JFactory::getDbo();
        $query1 = "SELECT * FROM `#__viaggio_manualpayments` WHERE order_id = '".$order_id."' ORDER BY id ASC LIMIT 1";
        $db->setQuery($query1);
        $manualPayment = $db->loadObject();

        $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
        $db->setQuery($query);
        $manualPaymentDetails = $db->loadObjectList();

        $data = array();

        foreach ($manualPaymentDetails as $manualPaymentDetail)
        {
            if ($manualPaymentDetail->field_value != '')
            {
                $data[$manualPaymentDetail->field_group_name][$manualPaymentDetail->field_group][$manualPaymentDetail->field_name] = $manualPaymentDetail->field_value;
            }
        }

        $manualPayment->check_type = $item->check_type;
        $manualPayment->bank = $item->bank;
        include_once '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';
        include_once JPATH_COMPONENT_ADMINISTRATOR.'/templates/doc.php';
        die();
    }

    public function accordo($accordopdf = false, $returnFile = true, $date_open = false)
    {
        //получаем запрос
        $app  = JFactory::getApplication();
        $order_id = $app->input->getInt('order_id', 0);

        //подключаем хелпер фронта для использования фукнции почты и печати pdf
        include_once JPATH_COMPONENT_SITE.'/helpers/frontend.php';

        //подключаем настройки
        include_once JPATH_COMPONENT_SITE.'/helpers/settings.php';

        //получаем данные заказа
        $item = ViaggioHelpersFrontend::getOrderObject($order_id);
        $item->number = $order_id;

        $db = JFactory::getDbo();
        $query1 = "SELECT * FROM `#__viaggio_manualpayments` WHERE order_id = '".$order_id."' ORDER BY id ASC LIMIT 1";
        $db->setQuery($query1);
        $manualPayment = $db->loadObject();

        $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
        $db->setQuery($query);
        $manualPaymentDetails = $db->loadObjectList();

        if (!$date_open) {
            $query = "SELECT date_open FROM #__viaggio_orders_status WHERE order_id = '" . $order_id . "'";
            $db->setQuery($query);
            $accordo = $db->loadObject();

            if ($accordo && $accordo->date_open)
                $date_open = $accordo->date_open;
        }

        $data = array();

        foreach ($manualPaymentDetails as $manualPaymentDetail)
        {
            if ($manualPaymentDetail->field_value != '')
            {
                $data[$manualPaymentDetail->field_group_name][$manualPaymentDetail->field_group][$manualPaymentDetail->field_name] = $manualPaymentDetail->field_value;
            }
        }

        $manualPayment->check_type = $item->check_type;
        $manualPayment->bank = $item->bank;

        include_once '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';
        include_once JPATH_COMPONENT_ADMINISTRATOR.'/templates/accordo.php';

        if ($returnFile)
            die();

        return [
            'order_id' => $order_id,
            'pdfFilePath' => $filePDF
        ];
    }

    public function printdoc2()
    {
        //получаем запрос
        $app  = JFactory::getApplication();
        $order_id = $app->input->getInt('order_id', 0);

        //подключаем хелпер фронта для использования фукнции почты и печати pdf
        include_once JPATH_COMPONENT_SITE.'/helpers/frontend.php';

        //подключаем настройки
        include_once JPATH_COMPONENT_SITE.'/helpers/settings.php';

        //получаем данные заказа
        $item = ViaggioHelpersFrontend::getOrderObject($order_id);
        $item->number = $order_id;

        $db = JFactory::getDbo();
        $query1 = "SELECT * FROM `#__viaggio_manualpayments` WHERE order_id = '".$order_id."' ORDER BY id ASC LIMIT 1";
        $db->setQuery($query1);
        $manualPayment = $db->loadObject();

        $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualPayment->paymentID."'";
        $db->setQuery($query);
        $manualPaymentDetails = $db->loadObjectList();

        $data = array();

        foreach ($manualPaymentDetails as $manualPaymentDetail)
        {
            if ($manualPaymentDetail->field_value != '')
            {
                $data[$manualPaymentDetail->field_group_name][$manualPaymentDetail->field_group][$manualPaymentDetail->field_name] = $manualPaymentDetail->field_value;
            }
        }

        $payment = $manualPayment;
        include_once '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';
        include_once JPATH_COMPONENT_ADMINISTRATOR.'/templates/agent.php';
        die();
    }

    public function payedPartial()
    {
        $db = JFactory::getDBO();
        $app  = JFactory::getApplication();
        $payment_id = $app->input->getInt('payment_id', 0);
        $order_id = $app->input->getInt('order_id', 0);
        $bank = $app->input->getString('bank', '');
        $check_type = $app->input->getInt('check_type', 1);
        $summ_in_euro = $app->input->getFloat('summ_in_euro', false);

        if ($summ_in_euro)
        {
            $summ_in_euro = round($summ_in_euro,2);
            $query = 'UPDATE #__viaggio_manualpayments SET'.
                ' status = '.ViaggioHelpersViaggio::MANUAL_PAYMENT_PAYED_BY_BANK.' ,'.
                ' bank = \''.$bank.'\' ,'.
                ' check_type = '.$check_type.' ,'.
                " payTime = '".date('Y-m-d H:i:s')."', ".
                " comissionEuro = amountEuro-".$summ_in_euro.
                ' WHERE id = '.$payment_id;
            $db->setQuery($query);
            $db->execute();

            $query = "UPDATE `#__viaggio_orders` SET payment_exists = 1, bank = '".$bank."', check_type = ".$check_type." WHERE id = '".$order_id."'";

            $db->setQuery($query);
            $db->execute();
        }

        $this->setRedirect('index.php?option=com_viaggio&view=orders', false);
    }

    public function payedByCash()
    {
        $db = JFactory::getDBO();
        $app  = JFactory::getApplication();
        $payment_id = $app->input->getInt('payment_id', 0);
        $order_id = $app->input->getInt('order_id', 0);
        $check_type = $app->input->getInt('check_type', 1);
        $query = 'UPDATE #__viaggio_manualpayments SET'.
            ' status = '.ViaggioHelpersViaggio::MANUAL_PAYMENT_PAYED_BY_CASH.' ,'.
            ' check_type = '.$check_type.' ,'.
            " payTime = '".date('Y-m-d H:i:s')."'".
            ' WHERE id = '.$payment_id;
        $db->setQuery($query);
        $db->execute();

        $query = "UPDATE `#__viaggio_orders` SET payment_exists = 1, check_type = ".$check_type." WHERE id = '".$order_id."'";

        $db->setQuery($query);
        $db->execute();

        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }

    public function sendToVisto()
    {
        $order_id = isset($_GET['order_id'])?intval($_GET['order_id']):false;
        if ($order_id)
        {
            $arr = array();
            $toSend = array();

            $db = JFactory::getDbo();
            $query2 = "SELECT * FROM `#__viaggio_orders` where id = ".$order_id;
            $db->setQuery($query2);
            $result = $db->loadObject();
            if($result){
                //получаем списки клиентов
                $query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = ".$order_id;
                $db->setQuery($query);
                $result->clients = $db->loadObjectList();

                $arr['visa']['date_from'] = $result->date_from;
                $arr['visa']['date_to'] = $result->date_to;
                $arr['visa']['total_cost'] = $result->valute_amount;
                $arr['visa']['user_id'] = 0;
                $result_visa_id = [];
                if ($result->visa_id)
                    $result_visa_id = explode(',',$result->visa_id);

                JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php');
                $customFields = FieldsHelper::getFields('com_users.user', JFactory::getUser(), true);
                if (count($customFields))
                {
                    foreach ($customFields as $field)
                    {
                        if ($field->name == 'visto-user-id')
                            $arr['visa']['user_id'] = intval($field->value);
                    }
                }

                $toSendPosition = 0;
                $toSend[$toSendPosition] = $arr;

                if (count($result->clients))
                {
                    foreach ($result->clients as $k => $row)
                    {
                        $insertArray = array(
                            'first_name' => $row->nome,
                            'second_name' => $row->cognome,
                            'gender' => $row->sex,
                            'birthdate' => $row->birthday,
                            'nationality' => 'Italy',
                            'passport' => $row->numero_di_passaporto,
                            'numero_di_telefono' => $row->telephone,
                            'indirizzo_di_residenza' => $row->indirizzo_di_residenza,
                            'luogo_di_nascita' => $row->luogo_di_nascita,
                            'data_di_rilascio_from' => $row->date_from,
                            'data_di_rilascio_to' => $row->date_to,
                            'number_of_visits' => $row->numero_di_visite,
                            'last_visit_from' => $row->last_visit_from,
                            'last_visit_to' => $row->last_visit_to,
                            'organization' => $row->nome_dell_organizzazione,
                            'position' => $row->ufficio_dell_organizzazione,
                            'address' => $row->indirizzo_dell_organizzazione,
                            'phone' => $row->telefono_dell_organizzazione
                        );

                        if (isset($result_visa_id[$toSendPosition]))
                            $toSend[$toSendPosition]['visa']['id'] = $result_visa_id[$toSendPosition];

                        $toSend[$toSendPosition]['participants'][] = $insertArray;
                        if ($k > 0 && $k%14 == 0)
                            $toSendPosition++;
                    }
                }
            }

            $responseFirst = false;

            $url = "https://www.visto-russia.com/index.php?option=com_touristinvite&task=createVisaApi";
            $visa_id = '';

            foreach ($toSend as $clientsBlock)
            {
                $request = curl_init($url);
                curl_setopt($request, CURLOPT_POST, true);
                $query=array('data' => json_encode($clientsBlock));
                curl_setopt($request, CURLOPT_POSTFIELDS, $query);
                curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($request);
                curl_close($request);

                if (intval($response))
                {
                    if (!$responseFirst)
                    {
                        $visa_id = intval($response);
                        $responseFirst = intval($response);
                    }
                    else
                        $visa_id .= ','.intval($response);
                }
            }

            $query = "UPDATE `#__viaggio_orders` SET visa_id = '".$visa_id."' where id = ".$order_id;
            $db->setQuery($query);
            $db->execute();

            $this->setRedirect('index.php?option=com_viaggio&view=orders', false);
        }
    }

    public function test()
    {
        JLoader::register('FieldsHelper', JPATH_ADMINISTRATOR . '/components/com_fields/helpers/fields.php');
        $customFields = FieldsHelper::getFields('com_users.user', JFactory::getUser(), true);
        if (count($customFields))
        {
            foreach ($customFields as $field)
            {
                if ($field->name == 'visto-user-id')
                    echo '<b>'.intval($field->value).'</b>';
            }
        }
    }

    public function hideManualPayment()
    {
        $id = intval($_GET['id']);
        if ($id)
        {
            $db = JFactory::getDBO();
            $query = 'UPDATE #__viaggio_manualpayments '.
                'SET hidden = 1 '.
                'WHERE id = '.$id;
            $db->setQuery($query);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }

    public function courses()
    {
        if (isset($_POST['id']) && isset($_POST['date']) && isset($_POST['amount']))
        {
            $id = intval($_POST['id']);
            $date = $_POST['date'].'-01';
            $amount = floatval($_POST['amount'])*100;

            $db = JFactory::getDbo();
            if ($id < 1)
                $db->setQuery("INSERT INTO #__viaggio_course (`date`,`amount`) VALUES ('".$date."',".$amount.")");
            else
                $db->setQuery("UPDATE #__viaggio_course SET date = '".$date."', amount = ".$amount." WHERE id = ".$id);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=courses', false);
    }

    public function savemonthstatistic()
    {
        $user = JFactory::getUser();
        $user_groups = JAccess::getGroupsByUser($user->get('id'));
        if (in_array(8,$user_groups)) {
            $db = JFactory::getDbo();
            foreach ($_POST['data'] as $row) {
                $db->setQuery('SELECT count(*) count FROM #__viaggio_month_statistic WHERE order_id = ' . $row['order_id'] . ' AND manualpaymant_id = ' . $row['manualpayment_id']);
                $count = $db->loadObject();

                if ($count->count == 0) {
                    $db->setQuery("INSERT INTO #__viaggio_month_statistic (`order_id`,`manualpaymant_id`,`full_summ`,`acconto`,`curs`,`how_much_paid`,`payments_for_partners_summ`,`costo_parziale`,`total_profit`,`guadagno_parziale`,`profit`) VALUES ('" . $row['order_id'] . "','" . $row['manualpayment_id'] . "','" . $row['full_summ'] . "','" . $row['acconto'] . "','" . $row['curs'] . "','" . $row['how_much_paid'] . "','" . $row['payments_for_partners_summ'] . "','" . $row['costo_parziale'] . "','" . $row['total_profit'] . "','" . $row['guadagno_parziale'] . "','" . $row['profit'] . "')");
                    $db->execute();
                }
            }
            $db->setQuery("UPDATE #__viaggio_month_statistic SET skip = null WHERE skip = 1");
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=monthstatistic', false);
    }

    public function changevaluteamount()
    {
        $order_id = intval($_POST['order_id']);
        $valute_amount = floatval($_POST['valute_amount']);
        if ($order_id && $valute_amount)
        {
            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM #__viaggio_course WHERE date = '".date('Y-m-01')."' LIMIT 1");
            $course = $db->loadObject();
            if (isset($course->amount))
                $course = $course->amount/100;
            else
                $course = 1;
            $db->setQuery("UPDATE #__viaggio_orders SET valute_amount = '".$valute_amount."', valute_rate = '".$course."', amount = ".($valute_amount*$course)." WHERE id = ".$order_id);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=statistic', false);
    }

    public function makeCheck()
    {
        $db = JFactory::getDBO();
        $app  = JFactory::getApplication();
        $payment_id = $app->input->getInt('payment_id', 0);
        $amount = round(floatval(str_replace(',','.',$app->input->getString('amount', 0))),2);
        $query = 'SELECT * FROM #__viaggio_manualpayments WHERE id = '.$payment_id;
        $db->setQuery($query);
        $manualPayment = $db->loadObject();

        if (isset($manualPayment->id))
        {
            include ('/home/russiantour/web/russiantour.com/public_html/components/com_viaggio/helpers/frontend.php');
            $helper = new ViaggioHelpersFrontend();
            $helper->makeCheck($manualPayment->paymentID,$manualPayment->email,$manualPayment->telefon,$amount,$manualPayment->check_type,1,$manualPayment->order_id);
            $manualPayment->check_sended = 1;
            $manualPayment->check_amount = intval($amount*100);
            JFactory::getDbo()->updateObject('#__viaggio_manualpayments', $manualPayment, 'id');
        }
        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }

    public function changemanualpaymentamount()
    {
        $id = intval($_POST['manualpayment_id']);
        $eur_amount = round(floatval(str_replace(',','.',$_POST['manualpayment_eur'])),2);
        $comissionEuro = round(floatval(str_replace(',','.',$_POST['comissionEuro'])),2);
        $rub_amount = floatval(str_replace(',','.',$_POST['manualpayment_rub']))*100;
        $curs = floatval(str_replace(',','.',$_POST['manualpayment_curs']));
        if ($id)
        {
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE #__viaggio_manualpayments SET amountEuro = '".$eur_amount."', amountRub = '".$rub_amount."', curs = ".$curs.", comissionEuro = '".$comissionEuro."' WHERE id = ".$id);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }

    public function changerelationamount()
    {
        $id = intval($_POST['id']);
        $amount_rub = round(floatval(trim(str_replace(',','.',$_POST['amount_rub']))),2)*100;
        $amount_euro = round(floatval(trim(str_replace(',','.',$_POST['amount_euro']))),2)*100;
        if ($id)
        {
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE #__viaggio_orders_relations SET amount_rub = '".$amount_rub."', amount_euro = '".$amount_euro."' WHERE id = ".$id);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=orders', false);
    }

    public function getdata()
    {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM #__viaggio_manualpayments WHERE bank = 'payler' AND check_type = 2 AND status = 1";
        var_dump($query);
        $db->setQuery($query);
        $orders = $db->loadAssocList();
        var_dump(count($orders));
        foreach ($orders as $order)
        {
            $OrderId = $order['paymentID'];
            $url = $this->paylerUrl.'GetAdvancedStatus';
            $key=urlencode($this->paylerKey2);
            $data = array(
                'key='.$key,
                'order_id='.$OrderId,
            );
            $postdata = implode('&',$data);
            if ($curl = curl_init()) {
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $postdata);
                curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content­Type: application/x­www­form­urlencoded',
                ));

                $out = curl_exec($curl);
                $out = json_decode($out);

                curl_close($curl);
                var_dump($out);echo '<br/><br/>';
                if ($out->status == 'Charged' || $out->status == 'Authorized')
                {
                    if (strpos($out->order_id,'m')!==false)
                    {
                        $query = "UPDATE `#__viaggio_manualpayments` SET outOrderID = ".$out->processing_order_id." WHERE paymentID = '".$out->order_id."'";
                        $db->setQuery($query);
                        $db->execute();
                    }
                }
            }
        }

    }

    public function getxls()
    {
        $db = JFactory::getDbo();
        $db->setQuery("SELECT * FROM #__viaggio_month_statistic ORDER BY id DESC LIMIT 10");
        $firstRow = '';
        $text = '';
        foreach ($db->loadAssocList() as $num => $row)
        {
            foreach ($row as $key => $val)
            {
                if ($num == 0)
                {
                    $firstRow .= $key.';';
                }
                $text .= $val.';';
            }
            $text .= "\n";
        }
        //$name = time();
        $name = '_test.xls';
        echo $name.'<br/>';
        var_dump(file_put_contents('/home/russiantour/web/russiantour.com/public_html/'.$name,$firstRow."\n".$text));

        echo '<a href="/'.$name.'">check</a>';
    }

    public function fp()
    {
        $id = intval($_POST['id']);
        if (isset($_POST['fp']))
            $fp = 1;
        else
            $fp = 0;

        if ($id)
        {
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE #__viaggio_manualpayments SET full_payment = ".$fp." WHERE id = ".$id);
            $db->execute();

            $db->setQuery('SELECT * FROM #__viaggio_manualpayments WHERE id = '.$id);
            $obj = $db->loadObject();

            $db->setQuery("UPDATE #__viaggio_orders WHERE id = ".$obj->order_id);
            $db->execute();
        }
        $this->setRedirect('index.php?option=com_viaggio&view=manualpayments', false);
    }

    function sendForm()
    {
        $db = JFactory::getDBO();

        $trip_id = 0;
        if (isset($_POST['trip_id']))
            $trip_id = intval($_POST['trip_id']);

        $trip_data = ",null,null";
        if ($trip_id > 0)
        {
            $sql = "SELECT * FROM #__viaggio_trips WHERE id = ".$trip_id." LIMIT 1";
            $db->setQuery($sql);
            $trip = $db->loadObject();
            $trip_data = ",'".$trip->date_from."','".$trip->date_to."'";
        }

        $sql = "INSERT INTO #__viaggio_orders (`clientcount`,`nomecgome`,`telephone`,`email`,`description`,`status`,`status_from`,`trip_id`, `date_from`, `date_to`) VALUES ('".$_POST['form-personas']."','".$_POST['form-cognome']."','".$_POST['form-phone']."','".$_POST['form-mail']."','".$_POST['form-comments']."',0,".ViaggioHelpersViaggio::ORDER_BY_PHONE.",".$_POST['trip_id'].$trip_data.")";
        $db->setQuery($sql);
        $db->execute();

        if ($trip_id > 0)
        {
            $order_id = $db->insertid();

            $sql = "INSERT INTO #__viaggio_orders_hotels (order_id,hotel_id) select ".$order_id.", hotel_id from #__viaggio_trips_hotels where trip_id = ".$trip_id;
            $db->setQuery($sql);
            $db->execute();
        }

        exit;
    }

    public function sendOrderToUser()
    {
        $user_id = intval($_GET['user_id']);
        $order_id = intval($_GET['order_id']);

        if ($user_id && $order_id) {
            $db = JFactory::getDbo();
            $db->setQuery("UPDATE #__viaggio_orders SET created_by = ".$user_id." WHERE id = ".$order_id);
            $db->execute();

            $this->sendEmail($user_id,$order_id);
        }

        $this->setRedirect('index.php?option=com_viaggio&view=orders', false);
    }

    private function sendEmail($user_id,$order_id)
    {
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );
        $mailer->setSender($sender);
        $mailer->isHTML(true);

        $user_from = JFactory::getUser();
        $user_to = JFactory::getUser($user_id);

        $mailer->addRecipient($user_to->email);
        $mailer->addRecipient('info@russiantour.com');
        $mailer->setSubject('Пользователь '.$user_from->username.' ('.$user_from->email.') назначил '.$user_to->username.' ('.$user_to->email.') заявку номер '.$order_id);

        $item = $this->getOrderData($order_id);

        $body = '
        <div class="uk-h3 uk-text-center">
            '.$item->tour_name_rus.'
            <br>
            Категория тура: '.$item->tour_category_title_ita.'
        </div>
        <div class="uk-h4 uk-text-center">
            '.date('d/m/Y',strtotime($item->date_from)).' - '.date('d/m/Y',strtotime($item->date_to)).'
        </div>
        '.($item->description?' <div class="uk-comment-meta">description</div><h3 class="uk-comment-title">'.$item->description.'</h3>':'').'
        '.($item->email?' <div class="uk-comment-meta">e-mail</div><h3 class="uk-comment-title">'.$item->email.'</h3>':'').'
        '.($item->nomecgome?' <div class="uk-comment-meta">FIO</div><h3 class="uk-comment-title">'.$item->nomecgome.'</h3>':'').'
        '.($item->telephone?' <div class="uk-comment-meta">Telephone</div><h3 class="uk-comment-title">'.$item->telephone.'</h3>':'').'
        '.($item->pole1?' <div class="uk-comment-meta">Для юр. договора</div><h3 class="uk-comment-title">'.$item->pole1.'</h3>':'').'

        <h3 class="uk-comment-title"> Persona '.$item->clientcount.' </h3>
        ';

        $mailer->setBody($body);
        $mailer->Send();
    }

    private function getOrderData($order_id)
    {
        $db = JFactory::getDBO();
        $sql = "
        SELECT 
            t.name_rus tour_name_rus,
            tc.title_ita tour_category_title_ita,
            o.date_from,
            o.date_to,
            o.description,
            o.email,
            o.nomecgome,
            o.telephone,
            o.pole1,
            o.clientcount
        FROM #__viaggio_orders o 
        LEFT JOIN #__viaggio_trips t ON t.id = o.trip_id
        LEFT JOIN #__viaggio_trips_categorys tc ON tc.id = t.cat_id 
        WHERE o.id = ".$order_id." LIMIT 1";
        $db->setQuery($sql);
        return $db->loadObject();
    }

    public function createReportByTourFilter()
    {
        if (isset($_GET['data']) && isset($_GET['name']))
        {
            $db = JFactory::getDBO();
            $sql = "INSERT INTO #__viaggio_report_by_tours_filters (`data`,`name`) VALUE ('".$_GET['data']."','".$_GET['name']."')";
            $db->setQuery($sql);
            $db->execute();
        }
        echo json_encode(ViaggioHelpersViaggio::getReportByTourFilters());
    }

    public function deleteReportByTourFilter()
    {
        if (isset($_GET['id']) && intval($_GET['id']))
        {
            $db = JFactory::getDBO();
            $sql = "DELETE FROM #__viaggio_report_by_tours_filters WHERE id = ".intval($_GET['id']);
            $db->setQuery($sql);
            $db->execute();
        }
        echo json_encode(ViaggioHelpersViaggio::getReportByTourFilters());
    }

    public function generateBankPDF()
    {
        //1574329010
        $order_id = $_GET['order_id'];
        exec('convert -density 200 /home/russiantour/web/russiantour.com/public_html/administrator/pdf/'.'doc'.$order_id.'.pdf -quality 100 /home/russiantour/web/russiantour.com/public_html/administrator/pdf/png/'.'doc'.$order_id.'.png');
        exec('convert -background white -alpha remove -alpha off -loop 0 $(ls /home/russiantour/web/russiantour.com/public_html/administrator/pdf/png/*.png | sort -V) -colorspace Gray  /home/russiantour/web/russiantour.com/public_html/administrator/pdf/'.'bank'.$order_id.'.pdf' );
        exec('rm /home/russiantour/web/russiantour.com/public_html/administrator/pdf/png/*.png');
        //echo '<a href="https://www.russiantour.com/administrator/pdf/'.'bank'.$order_id.'.pdf">download</a>';
        header("Content-type:application/pdf");

// It will be called downloaded.pdf
        header('Content-Disposition:attachment;filename=bank'.$order_id.'.pdf');

// The PDF source is in original.pdf
        readfile('/home/russiantour/web/russiantour.com/public_html/administrator/pdf/'.'bank'.$order_id.'.pdf');
        exit;
    }

    public function fillOrdersHotels()
    {
        $db = JFactory::getDBO();
        $sql = "SELECT id, trip_id FROM #__viaggio_orders WHERE trip_id IS NOT NULL AND trip_id > 0";
        $db->setQuery($sql);
        foreach ($db->loadObjectList() as $order)
        {
            $sql = "SELECT count(*) count FROM #__viaggio_orders_hotels WHERE order_id = ".$order->id;
            $db->setQuery($sql);
            if ($db->loadObject()->count == 0)
            {
                $sql = "INSERT INTO #__viaggio_orders_hotels (order_id,hotel_id) select ".$order->id.", hotel_id from #__viaggio_trips_hotels where trip_id = ".$order->trip_id;
                $db->setQuery($sql);
                $db->execute();
            }
        }
    }

    public function getHotelsByTrip()
    {
        $response = array(
            'success' => true,
            'data' => array()
        );
        if (isset($_GET['trip_id']) && intval($_GET['trip_id'])) {
            $db = JFactory::getDbo();

            $query = "SELECT hotel.id, hotel.city_id FROM `#__viaggio_trips_hotels` trip_hotel ".
                " LEFT JOIN #__viaggio_hotels hotel ON trip_hotel.hotel_id = hotel.id ".
                " WHERE trip_hotel.trip_id = ".intval($_GET['trip_id']);
            $db->setQuery($query);
            $hotels = $db->loadObjectList();
            foreach ($hotels as $hotel) {
                $response['data'][] = [
                    'hotel_id' => $hotel->id,
                    'city_id' => $hotel->city_id
                ];
            }

            $query = "SELECT city_id FROM `#__viaggio_trips_cities` ".
                " WHERE trip_id = ".intval($_GET['trip_id']);
            $db->setQuery($query);
            $cities = $db->loadObjectList();
            foreach ($cities as $city) {
                $response['data'][] = [
                    'hotel_id' => 0,
                    'city_id' => $city->city_id
                ];
            }

            echo json_encode($response,JSON_UNESCAPED_UNICODE);
        }
        else
        {
            echo json_encode(['success'=>false,'error'=>4,'errorText'=>'integer trip_id is required']);
        }
        exit;
    }

    public function saveComment()
    {
        $db = JFactory::getDbo();
        if (isset($_POST['id']) && $_POST['id'] > 0)
        {
            $query = "UPDATE #__viaggio_tours_comments SET ".
                "author_name = '".addslashes($_POST['author_name'])."', ".
                "author_email = '".$_POST['author_email']."', ".
                "text = '".addslashes($_POST['text'])."', ".
                "date = '".$_POST['date']."' ".
                " WHERE id = ".intval($_POST['id']);

            $db->setQuery($query);
            $db->execute();
        }
        else
        {
            if ($_POST['author_name']!='' && $_POST['author_email']!='' && $_POST['text']!='')
            {
                $query = "INSERT INTO #__viaggio_tours_comments (tour_id,author_name,author_email,text,state,date) VALUES (".
                    intval($_POST['tour_id']).",".
                    "'".addslashes($_POST['author_name'])."',".
                    "'".$_POST['author_email']."',".
                    "'".addslashes($_POST['text'])."',".
                    "'".$_POST['date']."'".
                    ")";

                $db->setQuery($query);
                $db->execute();
            }
        }
    }

    public function changePartnerSumm()
    {
        $id = intval($_POST['relation_id']);

        $amount_rub = str_replace(',','.',$_POST['amount_rub']);
        if (strpos($amount_rub,'.')===false)
            $amount_rub .= '00';
        else
            $amount_rub = str_replace('.','',$amount_rub);
        $amount_rub = intval($amount_rub);

        $amount_euro = str_replace(',','',$_POST['amount_euro']);
        if (strpos($amount_euro,'.')===false)
            $amount_euro .= '00';
        else
            $amount_euro = str_replace('.','',$amount_euro);
        $amount_euro = intval($amount_euro);

        if ($id > 0)
        {
            $db = JFactory::getDbo();

            $query = "SELECT * FROM #__viaggio_orders_relations WHERE id = ".$id;
            $db->setQuery($query);
            $relation = $db->loadObject();

            if (isset($relation->id))
            {
                $query = "SELECT * FROM #__viaggio_manualpayments WHERE order_id = ".$relation->order_id." AND status > 0 ORDER BY id DESC";
                $db->setQuery($query);
                $payment = $db->loadObject();

                if (isset($payment->id) && ($payment->full_payment == 0 || ($payment->full_payment == 1 && date('Y-m',strtotime($payment->payTime)) == date('Y-m'))))
                {
                    $query = "UPDATE #__viaggio_orders_relations SET amount_rub = ".$amount_rub.", amount_euro = ".$amount_euro." WHERE id = ".$id;
                    $db->setQuery($query);
                    $db->execute();

                    $query = "INSERT INTO `#__viaggio_orders_relations_history` ".
                        "(`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`) ".
                        "VALUES (".$relation->order_id.",".$relation->partner_id.",".$relation->service_id.",".$amount_rub.",".$amount_euro.")";
                    $db->setQuery($query);
                    $db->execute();
                }
            }
        }

        return $this->setRedirect($_SERVER['HTTP_REFERER'], false);
    }
}
