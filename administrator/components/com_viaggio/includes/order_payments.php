<?php
if ($manualpayment->check_type == 1)
    $first = 'ООО Русский Тур';
elseif ($manualpayment->check_type == 2)
    $first = 'ООО Висто руссия';
else
    $first = 'ПАО Путин Тур';

if ($manualpayment->bank == 'payler')
{
    $bank = 'Оплата по банку Payler';
}
elseif ($manualpayment->bank == 'tinkoff')
{
    $bank = 'Оплата по банку Тинькофф';
}
elseif ($manualpayment->bank == 'СовкомБанк')
{
    $bank = 'Оплата по банку СовкомБанк';
}
else
{
    $bank = '';
}


$payTime = '';
if ($manualpayment->payTime)
{
    $payTime = explode(' ',$manualpayment->payTime);
    $payTime = explode('-',$payTime[0]);
    $payTime = $payTime[2].'/'.$payTime[1].'/'.$payTime[0];
}

$productNoPrefix = '';
if ($manualpayment->status == 1)
    $productNoPrefix = 'С';
elseif ($manualpayment->status == 2)
    $productNoPrefix = 'В';

echo '
<div class="uk-h4 uk-text-center">'.$first.'</div>
<article class="uk-comment uk-comment-primary">
    <header class="uk-comment-header '.(($manualpayment->status == 0)?'status0':'').'">
        <h3 class="uk-comment-title"><b>Номер:</b> <a  target="_blank" href="/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&id='.$manualpayment->id.'">'.$manualpayment->paymentID.'</a> от '.$payTime.' </h3>
        <h3 class="uk-comment-title">'.$bank.' </h3>
		
        <h3 class="uk-comment-title"><b>Сумма:</b> '.$manualpayment->amountEuro.' €<br>';
if ($manualpayment->status != 0)
{
echo '      <a target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID='.$manualpayment->paymentID.'">
                <i class="uk-icon-info uk-icon-small"></i>
            </a>
            <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">    
                <div><i class="uk-icon-file-word-o uk-icon-small"></i></div>
                <div class="uk-dropdown">
                    <a target="_blank" href="/administrator/index.php?option=com_viaggio&task=printdoc&order_id='.$manualpayment->order_id.'&paymentID='.$manualpayment->paymentID.'">Word</a>
                    <br>
                    <a  target="_blank"  href="/administrator/index.php?option=com_viaggio&task=generateBankPDF&order_id='.$manualpayment->order_id.'">Generate Bank PDF</a>
                </div>
            </div>	
            <a target="_blank" href="/administrator/index.php?option=com_viaggio&task=downloadpdf&order_id='.$manualpayment->order_id.'&paymentID='.$manualpayment->paymentID.'">
                <i class="uk-icon-file-pdf-o uk-icon-small"></i>
            </a>
            <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                <!-- This is the element toggling the dropdown -->
                <div><i class="uk-icon-bank uk-icon-small"></i></div>
                <div class="uk-dropdown">
                    <a target="_blank" href="https://www.russiantour.com/components/com_viaggio/pdf/pagare'.$productNoPrefix.$manualpayment->order_id.'_bank.pdf">Invoce</a><br>
                                    <a target="_blank" href="https://www.russiantour.com/bonifico?payment_id='.$manualpayment->paymentID.'">ita Ссылка</a><br>
									 <a target="_blank" href="https://www.russiantour.com/rus/oplata?payment_id='.$manualpayment->paymentID.'">rus Ссылка</a><br>
					                <input style=" width: 54px; " value="https://www.russiantour.com/ita/bonifico?payment_id='.$manualpayment->paymentID.'" id="myInput1'.$manualpayment->paymentID.'"> ita 
                <a onclick="myFunction1'.$manualpayment->paymentID.'()"><i class="uk-icon-copy"></i> </a>
<script>function myFunction1'.$manualpayment->paymentID.'(){var copyText=document.getElementById("myInput1'.$manualpayment->paymentID.'");copyText.select();document.execCommand("copy");}</script><br>
					                <input style=" width: 54px; " value="https://www.russiantour.com/rus/oplata?payment_id='.$manualpayment->paymentID.'" id="myInput2'.$manualpayment->paymentID.'"> rus 
                <a onclick="myFunction2'.$manualpayment->paymentID.'()"><i class="uk-icon-copy"></i> </a>
<script>function myFunction2'.$manualpayment->paymentID.'(){var copyText=document.getElementById("myInput2'.$manualpayment->paymentID.'");copyText.select();document.execCommand("copy");}</script>
				
				</div>
            </div>
					 

			';
}

echo ' 

'.(($manualpayment->status == 0)?' 
  <a target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID='.$manualpayment->paymentID.'">
                <i data-uk-tooltip title="Скачать Invoce" class="uk-icon-info uk-icon-small"></i>
            </a> 
 			            <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                <!-- This is the element toggling the dropdown -->
                <div data-uk-tooltip title="Ссылка на оплату по банку. "  ><i  class="uk-icon-bank uk-icon-small"></i></div>
                <div class="uk-dropdown">
                    <a target="_blank" href="https://www.russiantour.com/components/com_viaggio/pdf/pagare'.$productNoPrefix.$manualpayment->order_id.'_bank.pdf">Invoce</a><br>
                                    <a target="_blank" href="https://www.russiantour.com/bonifico?payment_id='.$manualpayment->paymentID.'">ita Ссылка</a><br>
									 <a target="_blank" href="https://www.russiantour.com/rus/oplata?payment_id='.$manualpayment->paymentID.'">rus Ссылка</a><br>
					                <input style=" width: 54px; " value="https://www.russiantour.com/ita/bonifico?payment_id='.$manualpayment->paymentID.'" id="myInput1'.$manualpayment->paymentID.'"> ita 
                <a onclick="myFunction1'.$manualpayment->paymentID.'()"><i class="uk-icon-copy"></i> </a>
<script>function myFunction1'.$manualpayment->paymentID.'(){var copyText=document.getElementById("myInput1'.$manualpayment->paymentID.'");copyText.select();document.execCommand("copy");}</script><br>
					                <input style=" width: 54px; " value="https://www.russiantour.com/rus/oplata?payment_id='.$manualpayment->paymentID.'" id="myInput2'.$manualpayment->paymentID.'"> rus 
                <a onclick="myFunction2'.$manualpayment->paymentID.'()"><i class="uk-icon-copy"></i> </a>
<script>function myFunction2'.$manualpayment->paymentID.'(){var copyText=document.getElementById("myInput2'.$manualpayment->paymentID.'");copyText.select();document.execCommand("copy");}</script>
				
				</div>
            </div>
			
 			            <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                <!-- This is the element toggling the dropdown -->
                <div data-uk-tooltip title="Ссылка на оплату по карточке. "  ><i  class="uk-icon-link uk-icon-small"></i></div>
                <div class="uk-dropdown">
                    <a target="_blank" href="https://www.russiantour.com/pagato?payment_id='.$manualpayment->paymentID.'">Ссылка</a><br>
					                <input style=" width: 54px; " value="https://www.russiantour.com/ita/pagato?payment_id='.$manualpayment->paymentID.'" id="myInput2'.$manualpayment->paymentID.'">
                <a onclick="myFunction2'.$manualpayment->paymentID.'()"><i class="uk-icon-copy"></i> </a>
<script>function myFunction2'.$manualpayment->paymentID.'(){var copyText=document.getElementById("myInput2'.$manualpayment->paymentID.'");copyText.select();document.execCommand("copy");}</script>
                </div>
            </div> 
			  <a target="_blank"  onclick="return confirm(\'ААААААаааааааааааааааааа ненадо.\')" href="/administrator/index.php?option=com_viaggio&task=hideManualPayment&id='.$manualpayment->id.'">
                <i data-uk-tooltip title="Удалить платёж" class="uk-icon-trash uk-icon-small"> </i>
            </a>
':'').'

 </h3> 
    </header>
</article>
																		 
';