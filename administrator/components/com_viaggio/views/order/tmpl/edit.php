<html>
<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author  COM_VIAGGIO_FORM_LBL_ORDER_DATE_FROM
   Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

if (isset($_POST['service2']) && is_array($_POST['service2']) && count($_POST['service2']))
{
    $db = JFactory::getDbo();
    $query = "DELETE FROM `#__viaggio_orders_relations_postchanges` WHERE order_id = ".$order_id;
    $db->setQuery($query);
    $db->execute();
    foreach ($_POST['service2'] as $id=>$servicesArray)
    {
        foreach ($servicesArray as $service)
        {
            if ($service['partner_id'] != '' && intval($service['partner_id']))
            {
                if (($service['amount_rub'] != '' && intval($service['amount_rub'])) || ($service['amount_euro'] != '' && intval($service['amount_euro'])))
                {
                    $amount_rub = 0;
                    $amount_euro = 0;
                    if (($service['amount_rub'] != '' && intval($service['amount_rub'])))
                        $amount_rub = intval($service['amount_rub']*100);
                    if ($service['amount_euro'] != '' && intval($service['amount_euro']))
                        $amount_euro = intval($service['amount_euro']*100);

                    $query = "INSERT INTO `#__viaggio_orders_relations_postchanges` (`order_id`,`partner_id`,`service_id`,`amount_rub`,`amount_euro`) VALUES (".$order_id.",".intval($service['partner_id']).",".$id.",".$amount_rub.",".$amount_euro.")";
                    $db->setQuery($query);
                    $db->execute();

                    $query = $db->getQuery(true);
                    $query->select('*');
                    $query->from('#__viaggio_services');
                    $query->where('id = '.$id);
                    $db->setQuery($query);
                    $current_service = $db->loadObject();

                    $query = $db->getQuery(true);
                    $query->select('*');
                    $query->from('#__viaggio_partners');
                    $query->where('id = '.intval($service['partner_id']));
                    $db->setQuery($query);
                    $current_partner = $db->loadObject();
                }
            }
        }
    }
}

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');

$db    = JFactory::getDbo();

$query = "SELECT * FROM #__viaggio_services ORDER BY name_rus ASC";
$db->setQuery($query);
$servicesList = $db->loadObjectList();

$query = 'select s.*, r.partner_id, r.amount_rub, r.amount_euro from #__viaggio_services s
 join #__viaggio_orders_relations r on r.service_id = s.id and r.order_id = '.$this->item->id;
$user      = JFactory::getUser();
$userId    = $user->get('id');
$db->setQuery($query);
$services = $db->loadObjectList();

$query = 'select p.*, ps.service_id from #__viaggio_partners p
join #__viaggio_partners_services ps on p.id = ps.partner_id';
$db->setQuery($query);
$partners = $db->loadObjectList();
$partners_of_services = array();
foreach ($partners as $partner)
{
    $partners_of_services[$partner->service_id][] = $partner;
}

$query = 'select s.*, r.partner_id, r.amount_rub, r.amount_euro from #__viaggio_services s
 join #__viaggio_orders_relations_postchanges r on r.service_id = s.id and r.order_id = '.$this->item->id;
$db->setQuery($query);
$services2 = $db->loadObjectList();

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_trips_categorys');
$query->where('state is null or state <> -2');
//$query->order('CAST(title_ita AS UNSIGNED) asc, title_ita ASC');
$query->order('ordering ASC');
$db->setQuery($query);
$trips_categorys = $db->loadObjectList();

?>
<head>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {

	js('input:hidden.order_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('order_idhidden')){
			js('#jform_order_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_order_id").trigger("liszt:updated");
	js('input:hidden.tour_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('tour_idhidden')){
			js('#jform_tour_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_tour_id").trigger("liszt:updated");
	
	});

	Joomla.submitbutton = function (task) {
		if (task == 'order.cancel') {
			Joomla.submitform(task, document.getElementById('order-form'));
		}
		else {

			if (task != 'order.cancel' && document.formvalidator.isValid(document.id('order-form'))) {

				Joomla.submitform(task, document.getElementById('order-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
	 
</script>
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<script type="text/javascript"   src="/images/css/2/js/components/tooltip.js"></script>
<script>
	function recalculate() {}
	function init() {
		recalculate();
	}
</script>
</head>
<body onload="init()">
<form
	action="<?php echo JRoute::_('index.php?option=com_viaggio&view=order&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="order-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_VIAGGIO_TITLE_ORDER', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

								 	<?php echo $this->form->renderField('id'); ?> 
				<input type="hidden" name="jform[subid]" value="<?php echo $this->item->subid; ?>" />
			<!--<?php echo $this->form->renderField('order_id'); ?>-->
                    <?php
                    $user_groups = JAccess::getGroupsByUser($user->get('id'));
                    if (in_array(8,$user_groups))
                    echo $this->form->renderField('created_by'), $this->form->renderField('orderstatus'), $this->form->renderField('errorcode'), $this->form->renderField('errormessage'),$this->form->renderField('stell'),$this->form->renderField('insurance'), $this->form->renderField('clientcount') ;

                        else
                    echo '';
                    ?>
                    <?php echo $this->form->renderField('client_type'); ?>
                    <div class="trip_id">
					<?php $aircheck = @$_SESSION['avia_checkbox']; ?>
					<?php $railcheck = @$_SESSION['rail_checkbox']; ?>
					
					
 
				 
				 
 	 
				 
				 
					 
                        <div class="control-group">
                            <label class="control-label">Категория тура (обязательное поле)</label>
                            <div class="controls">
                                <select id="parent-cat">
                                    <option value=0> --- Выберите категорию --- </option>
                                    <?php
                                    $parent_id = false;
                                    foreach ($trips_categorys as $cat)
                                    {
                                        if ($this->item->trip && $this->item->trip->cat_id && $this->item->trip->cat_id == $cat->id)
                                            $parent_id = $cat->parent_id;
                                    }

                                    foreach ($trips_categorys as $cat)
                                    {
                                        if ($cat->parent_id)
                                            continue;
                                        echo '<option value="'.$cat->id.'" '.(($this->item->trip && $parent_id == $cat->id)?'selected':'').'>'.$cat->title_ita.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="controls tour-cat-parent" <?php if (!$this->item->trip || !$this->item->trip->cat_id) {echo 'style="display: none;"';} ?>>
                                <select id="tour-cat">
                                    <option value=0 class="const"> --- Выберите сабкатегорию --- </option>
                                    <?php
                                    foreach ($trips_categorys as $cat)
                                    {
                                        if (!$cat->parent_id)
                                            continue;
                                        echo '<option '.((!$this->item->trip || !$this->item->trip->cat_id || $parent_id != $cat->parent_id)?'style="display: none;"':'').' parent_id = "'.$cat->parent_id.'" value="'.$cat->id.'" '.(($this->item->trip && $this->item->trip->cat_id == $cat->id)?'selected':'').'>'.$cat->title_ita.'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="control-group choose_tour" <?php if (!$this->item->trip) { ?>style="display: none"<?php } ?>>
                            <label class="control-label">Тур (обязательное поле)</label>
                            <div class="controls">
                                <select id="jform_trip_id" name="jform[trip_id]">
                                    <option value="" class="const"> --- Выберите тур --- </option>
                                    <?php
                                    if ($this->item->trip) {
                                        if ($this->item->trip->cat_id)
                                            $cat_id = 'cat_id = '.$this->item->trip->cat_id;
                                        else
                                            $cat_id = 'cat_id is null';
                                        $query = $db->getQuery(true);
                                        $query->select('*');
                                        $query->from('#__viaggio_trips');
                                        $query->where('state is null or state <> -2');
                                        $query->where($cat_id);
                                        $query->order('ordering ASC');
                                        $db->setQuery($query);
                                        $trips = $db->loadObjectList();
                                        foreach ($trips as $trip) {
                                            echo '<option value="'.$trip->id.'" datefrom="'.$trip->date_from.'" dateto="'.$trip->date_to.'" '.(($this->item->trip_id == $trip->id)?'selected':'').'>'.$trip->name_ita.'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

			<?php
				foreach((array)$this->item->order_id as $value):
					if(!is_array($value)):
						echo '<input type="hidden" class="order_id" name="jform[order_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>

                <?php
                    $user_groups = JAccess::getGroupsByUser($user->get('id'));
                    if (in_array(8,$user_groups))
                    echo $this->form->renderField('created_by'), $this->form->renderField('orderstatus'), $this->form->renderField('errorcode'), $this->form->renderField('errormessage'),$this->form->renderField('stell'),$this->form->renderField('insurance'), $this->form->renderField('clientcount') ;

                        else
                    echo '';
                ?>

                <?php echo $this->form->renderField('amount'); ?>
				<?php echo $this->form->renderField('valute_amount'); ?>
				<?php echo $this->form->renderField('valute_rate'); ?>
                <?php
                    foreach((array)$this->item->tour_id as $value):
                        if(!is_array($value)):
                            echo '<input type="hidden" class="tour_id" name="jform[tour_idhidden]['.$value.']" value="'.$value.'" />';
                        endif;
                    endforeach;
                ?>

				<?php echo $this->form->renderField('nomecgome'); ?>
				<?php echo $this->form->renderField('telephone'); ?>
				<?php echo $this->form->renderField('email'); ?>
				<?php echo $this->form->renderField('date_from'); ?>
				<?php echo $this->form->renderField('date_to'); ?>
									<div class="control-group">
			<div class="control-label"><label>Авиа</label>
</div> 
		<div class="controls"> <input id="avia_checkbox" type="checkbox"> </div>

</div>
				 
						<div class="control-group">
			<div class="control-label"><label>РЖД</label>
</div> 
		<div class="controls"> <input id="rail_checkbox" type="checkbox"> </div>

</div>			
				<?php echo $this->form->renderField('description'); ?>
				<?php echo $this->form->renderField('pole1'); ?>
				<?php
                if ($this->item->full_payment == 1)
                {
                    $not_payed_payments = "SELECT count(*) c FROM #__viaggio_manualpayments WHERE order_id = ".$this->item->id." AND payTime is null";
                    $db->setQuery($not_payed_payments);
                    $not_payed_payments = $db->loadObject();
                    var_dump($not_payed_payments->c);
                }
                echo $this->form->renderField('full_payment',null,null,(($this->item->full_payment == 1 && $not_payed_payments->c == 0)?['readonly']:[])); ?>
                    <div class="control-group">
                        <div class="control-label">
                            <label id="costo_parziale-lbl" for="costo_parziale" class="hasTooltip" title="" data-original-title="<strong>Costo parziale</strong>">
                                Costo parziale
                            </label>
                        </div>
                        <div class="controls">
                            <input type="text" name="costo_parziale" value="<?=round($this->item->month_statistic_costo_parziale/100,2); ?>" placeholder="Costo parziale" aria-invalid="false">
                        </div>
                    </div>
                    <select name="jform[check_type]" id="check_type">
                        <option value="0"> select check_type </option>
                        <option value="2" <?php if ($this->item->check_type == 2) echo 'selected' ?> >ООО Висто Руссия</option>
                        <option value="1" <?php if ($this->item->check_type == 1) echo 'selected' ?> >ООО Русский тур</option>
                    </select>

                    <select name="jform[bank]">
                        <option value=""> select bank </option>
                        <option value="payler" <?php if ($this->item->bank == 'payler') echo 'selected' ?> >payler</option>
                        <option value="tinkoff" <?php if ($this->item->bank == 'tinkoff') echo 'selected' ?> >tinkoff</option>
                    </select>
					<?php /*if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif;*/ ?>
                    <!-- HOTELS EDIT BEGIN -->
                    <?php
                    //same in order and trip edit
                    if ($this->item->trip_id && $this->item->trip_id > 0) //this condition is absent in trip edit
                    {
                        ?>
                        <hr/>
                        Отели:
                        <?php
                        $db    = JFactory::getDbo();

                        $query = $db->getQuery(true);
                        $query->select('*');
                        $query->from('#__viaggio_city');
                        $query->order('name_rus asc');
                        $db->setQuery($query);
                        $cities = $db->loadObjectList();

                        if (isset($_GET['id']) && intval($_GET['id']))
                        {
                            $query = $db->getQuery(true);
                            $query->select('a.*, hotel.city_id');
                            $query->from('#__viaggio_orders_hotels a');//difference between trips and orders
                            $query->join('LEFT','#__viaggio_hotels AS `hotel` ON `hotel`.id = a.`hotel_id`');
                            $query->where('order_id = '.intval($_GET['id']));//difference between trips and orders
                            $db->setQuery($query);
                            $hotels = $db->loadObjectList();

                            $query = $db->getQuery(true);
                            $query->select('*');
                            $query->from('#__viaggio_orders_cities');//difference between trips and orders
                            $query->where('order_id = '.intval($_GET['id']));//difference between trips and orders
                            $db->setQuery($query);
                            $order_cities = $db->loadObjectList();
                        }

                        $query = $db->getQuery(true);
                        $query->select('*');
                        $query->from('#__viaggio_hotels');
                        $db->setQuery($query);
                        $all_hotels = $db->loadObjectList();

                        $counter = 1;

                        foreach ($hotels as $hotel)
                        {
                            ?>
                            <div class='hotel-row' counter="<?php echo $counter; ?>">
                                Город:
                                <select class='hotel-city-id' name="city_id[<?php echo $counter; ?>]" onchange="filterHotels();">
                                    <option value='0'> -- Выберите город -- </option>
                                    <?php
                                    foreach ($cities as $city) {
                                        echo "<option value='" . $city->id . "' " . (($city->id == $hotel->city_id) ? 'selected' : '') . ">" . $city->name_rus . "</option>";
                                    }
                                    ?>
                                </select>

                                Отель:
                                <select class='hotel-id' name="hotel_id[<?php echo $counter; ?>]" hotel_id="<?php echo $hotel->hotel_id; ?>">
                                    <option value="0"> -- Выберите отель -- </option>
                                </select>
                                <div style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()">Удалить отель</div>
                            </div>
                            <?php $counter++; ?>
                        <?php
                        }

                        foreach ($order_cities as $order_city)
                        {
                            ?>
                            <div class='hotel-row' counter="<?php echo $counter; ?>">
                                Город:
                                <select class='hotel-city-id' name="city_id[<?php echo $counter; ?>]" onchange="filterHotels();">
                                    <option value='0'> -- Выберите город -- </option>
                                    <?php
                                    foreach ($cities as $city) {
                                        echo "<option value='" . $city->id . "' " . (($city->id == $order_city->city_id) ? 'selected' : '') . ">" . $city->name_rus . "</option>";
                                    }
                                    ?>
                                </select>

                                Отель:
                                <select class='hotel-id' name="hotel_id[<?php echo $counter; ?>]" hotel_id="0">
                                    <option value="0"> -- Выберите отель -- </option>
                                </select>
                                <div class="uk-icon-trash uk-icon-medium" style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()"></div>
                            </div>
                            <?php $counter++; ?>
                        <?php
                        }

                        ?>
                        <div class="add-hotel" style="cursor: pointer" onclick="addHotel()">Добавить отель</div>
                        <div class='hotel-row-empty' counter="<?php echo $counter; ?>" style="display: none">
                            Город:
                            <select class='hotel-city-id' name="city_id[<?php echo $counter; ?>]"  onchange="filterHotels();">
                                <option value='0'> -- Выберите город -- </option>
                                <?php
                                foreach ($cities as $city) {
                                    echo "<option value='" . $city->id . "'>" . $city->name_rus . "</option>";
                                }
                                ?>
                            </select>

                            Отель:
                            <select class='hotel-id' name="hotel_id[<?php echo $counter; ?>]" hotel_id="0">
                                <option value="0"> -- Выберите отель -- </option>
                                <?php
                                foreach ($all_hotels as $all_hotel)
                                {
                                    echo "<option city='".$all_hotel->city_id."' value='".$all_hotel->id."'>".$all_hotel->hotel_name_rus."</option>";
                                }
                                ?>
                            </select>
                            <div style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()">Удалить отель</div>
                        </div>
                        <script>
                            function addHotel()
                            {
                                let counter = jQuery('.hotel-row:last').attr('counter')+1;
                                jQuery('.add-hotel').before(jQuery('.hotel-row-empty').clone());//clone template hotel block
                                jQuery('.hotel-row-empty:first').addClass('hotel-row').removeClass('hotel-row-empty');//change class of cloned block
                                jQuery('.hotel-row:last select.hotel-id option').remove();//clear hotels list
                                jQuery('.hotel-row:last').show();//show cloned block
                                //remove old chosen block and generate new
                                jQuery('.hotel-row:last select').each(function () {
                                    jQuery(this).next().remove();
                                    jQuery(this).chosen();
                                });
                                jQuery('.hotel-row:last').attr('counter',counter);
                                jQuery('.hotel-row:last .hotel-id').attr('name','hotel_id['+counter+']');
                                jQuery('.hotel-row:last .hotel-city-id').attr('name','city_id['+counter+']');
                            }
                            function filterHotels()
                            {
                                //fill hotels list according to chosen city and stored in DB hotel
                                jQuery('.hotel-row .hotel-id').each(function () {
                                    var current_select = jQuery(this);
                                    let city_id = current_select.parent().find('.hotel-city-id').val();

                                    //clear option list
                                    current_select.find('option').remove();

                                    //fill hotels list according to chosen city
                                    jQuery('.hotel-row-empty option[city='+city_id+']').each(function () {
                                        let current_option = jQuery(this).clone();
                                        current_select.append(current_option);
                                    });

                                    //select stored in DB hotel
                                    current_select.val(current_select.attr('hotel_id'));

                                    //update chosen
                                    current_select.trigger("liszt:updated");
                                });
                            }
                            jQuery(document).ready(function() {
                                filterHotels();
                            });
                        </script>
                    <?php } ?>
                    <!-- HOTELS EDIT END -->
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php if (isset($this->item->manualpayment)) { ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'paymentDetails', JText::_('COM_VIAGGIO_TITLE_DETAILS', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <!-- АВИА -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>Авиа</h1>
						
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
							
                            <?php $n = 0; include ('blocks/air-ticket-block.php'); ?>
                            <?php
                            if (isset($this->item->airTickets) && count($this->item->airTickets) > 1)
                                {
                                    foreach ($this->item->airTickets as $n => $v)
                                    {
                                        if ($n > 0)
                                            include ('blocks/air-ticket-block.php');
                                    }
                                }
                            ?>
                            
                            <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('air-ticket')">Добавить билет</button>
                            <button class="uk-button uk-button-primary air-ticket-remove" type="button" onclick="removeBlock('air-ticket')" <?php if ($n==0) echo 'hidden'; ?>>Удалить последний билет</button>
							
							<div class="uk-margin">
							<input type="hidden" class="uk-input uk-form-width-small" id="airdate1" placeholder="Дата начало тура" name="air-ticket[<?php echo $n; ?>][begin]" value="<?php echo isset($this->item->airTickets)?str_replace('/','-',$this->item->airTickets[$n]['begin']):''; ?>">
							</div>

							<div class="uk-margin">
							<input type="hidden" class="uk-input uk-form-width-small" id="airdate2" placeholder="Дата завершение тура" name="air-ticket[<?php echo $n; ?>][end]" value="<?php echo isset($this->item->airTickets)?str_replace('/','-',$this->item->airTickets[$n]['end']):''; ?>">
							</div>
							
							
							<script type="text/javascript">
								document.getElementById('avia_checkbox').checked = document.getElementById('airdate1').value;
								document.getElementById('avia_checkbox').checked = document.getElementById('airdate2').value;
								document.getElementById('avia_checkbox').onclick = function(){
								if(this.checked){
									document.getElementById('airdate1').value = "01/01/1970";
									document.getElementById('airdate2').value = "01/01/1970";
									document.getElementById('air-ticket[0][begin]').value = "01/01/1970";
									document.getElementById('air-ticket[0][end]').value = "01/01/1970";
									}
								else {
									document.getElementById('airdate1').value = "";  
									document.getElementById('airdate2').value = "";
									document.getElementById('air-ticket[0][begin]').value = "";
									document.getElementById('air-ticket[0][end]').value = "";
									}
								}
							</script>

						</div>
						
                    </div>
                    <!-- ЖД -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>ЖД</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <?php $n = 0; include ('blocks/rail-ticket-block.php'); ?>
                            <?php
                            if (isset($this->item->railTickets) && count($this->item->railTickets) > 1)
                            {
                                foreach ($this->item->railTickets as $n => $v)
                                {
                                    if ($n > 0)
                                        include ('blocks/rail-ticket-block.php');
                                }
                            }
                            ?>
                            <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('rail-ticket')">Добавить билет</button>
                            <button class="uk-button uk-button-primary rail-ticket-remove" type="button" onclick="removeBlock('rail-ticket')" <?php if ($n==0) echo 'hidden'; ?>>Удалить последний билет</button>
							
							<div class="uk-margin">
							<input type="hidden" class="uk-input uk-form-width-small" id="raildate1" placeholder="Дата начало тура" name="rail-ticket[<?php echo $n; ?>][begin]" value="<?php echo isset($this->item->railTickets)?str_replace('/','-',$this->item->railTickets[$n]['begin']):''; ?>">
							</div>

							<div class="uk-margin">
							<input type="hidden" class="uk-input uk-form-width-small" id="raildate2" placeholder="Дата завершение тура" name="rail-ticket[<?php echo $n; ?>][end]" value="<?php echo isset($this->item->railTickets)?str_replace('/','-',$this->item->railTickets[$n]['end']):''; ?>">
							</div>
							
							<script type="text/javascript">
								document.getElementById('rail_checkbox').checked = document.getElementById('raildate1').value;
								document.getElementById('rail_checkbox').checked = document.getElementById('raildate2').value;
								document.getElementById('rail_checkbox').onclick = function(){
								if(this.checked){
									document.getElementById('raildate1').value = "01/01/1970";
									document.getElementById('raildate2').value = "01/01/1970";
									document.getElementById('rail-ticket[0][begin]').value = "01/01/1970";
									document.getElementById('rail-ticket[0][end]').value = "01/01/1970";
									}
								else {
									document.getElementById('raildate1').value = "";  
									document.getElementById('raildate2').value = "";
									document.getElementById('rail-ticket[0][begin]').value = "";
									document.getElementById('rail-ticket[0][end]').value = "";
									}
								}
							</script>
						</div>
                    </div>
                    <!-- ПРОЧЕЕ -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>Прочее</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="other-amount" value="<?php echo isset($this->item->otherDetails)?$this->item->otherDetails[0]['other-amount']:''; ?>"><br>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Русский текст" name="other-rus"><?php echo isset($this->item->otherDetails)?$this->item->otherDetails[0]['other-rus']:''; ?></textarea>
                            </div>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Итальянский текст" name="other-ita"><?php echo isset($this->item->otherDetails)?$this->item->otherDetails[0]['other-ita']:''; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- Виза -->
                    <div class="uk-inline uk-width-1-1">
                        <h1>Виза</h1>
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="visa-amount" value="<?php echo (isset($this->item->otherDetails) && isset($this->item->otherDetails[0]['visa-amount']))?$this->item->otherDetails[0]['visa-amount']:''; ?>"><br>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Русский текст" name="visa-rus"><?php echo (isset($this->item->otherDetails) && isset($this->item->otherDetails[0]['visa-rus']))?$this->item->otherDetails[0]['visa-rus']:''; ?></textarea>
                            </div>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="5" placeholder="Итальянский текст" name="visa-ita"><?php echo (isset($this->item->otherDetails) && isset($this->item->otherDetails[0]['visa-ita']))?$this->item->otherDetails[0]['visa-ita']:''; ?></textarea>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php } ?>
        <?php if (isset($this->item->clients) && count($this->item->clients)) { ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'clients', JText::_('COM_VIAGGIO_TITLE_CLIENTS', true)); ?>
            <div class="row-fluid">
                <div class="span10 form-horizontal">
                    <fieldset class="adminform">
                        <!-- КЛИЕНТЫ -->
                        <div class="uk-inline uk-width-1-1">
                            <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                                <?php $n = 0; include ('blocks/clients-block.php'); ?>
                                <?php
                                if (count($this->item->clients) > 1)
                                {
                                    foreach ($this->item->clients as $n => $v)
                                    {
                                        if ($n > 0)
                                            include ('blocks/clients-block.php');
                                    }
                                }
                                ?>
                                <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('client')">Добавить клиента</button>
                                Сколько добавить: <input id="number_of_blocks" value="1">
                                <button class="uk-button uk-button-primary client-remove" type="button" onclick="removeBlock('client')" <?php if ($n==0) echo 'hidden'; ?>>Удалить последнего клиента</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
        <?php } ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'services', JText::_('COM_VIAGGIO_TITLE_SERVICES', true)); ?>
        <div class="row-fluid">
            <div class="span12 form-horizontal">
                <fieldset class="adminform">
                    <!-- УСЛУГИ -->

                    <div class="uk-inline uk-width-1-1">
                        <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                            <?php
                            $num = 0;
                            $summRub = 0;
                            $summEur = 0;
                            foreach ($services as $key=>$service)
                            {
                                $num = $key+1;
                                $summRub += $service->amount_rub/100;
                                $summEur += $service->amount_euro/100;
                                ?>
                                <div class="control-group service" blockid="<?php echo $key; ?>">
                                    <div class="control-label"><?php echo $service->name_rus; ?></div>
                                    <div class="controls">
                                        Поставщик:
                                        <select name="service[<?php echo $service->id; ?>][<?php echo $key; ?>][partner_id]">
                                            <?php
                                            foreach ($partners_of_services[$service->id] as $partner)
                                            {
                                                echo "<option value='".$partner->id."' ".(($service->partner_id == $partner->id)?'selected':'').">".$partner->name_rus."</option>";
                                            }
                                            ?>
                                        </select>
                                        <?php if (count($this->item->changes) > 0) {

                                            if (isset($this->item->changes[$service->partner_id]) && isset($this->item->changes[$service->partner_id][$service->id]))
                                            {
                                                $old_amount_euro = $service->amount_euro;
                                                $old_amount_rub = $service->amount_rub;
                                                foreach ($this->item->changes[$service->partner_id][$service->id] as $change)
                                                {
                                                    $service->amount_euro += $change['amount_euro'];
                                                    $service->amount_rub += $change['amount_rub'];
                                                }
                                            }
                                        } ?>
                                        Цена в евро: <input class="amount_euro" name="service[<?php echo $service->id; ?>][<?php echo $key; ?>][amount_euro]" value="<?php echo $service->amount_euro/100; ?>">
                                        Цена в рублях: <input class="amount_rub" name="service[<?php echo $service->id; ?>][<?php echo $key; ?>][amount_rub]" value="<?php echo $service->amount_rub/100; ?>">
                                        <?php if (count($this->item->changes) > 0) {
                                            $db->setQuery("SELECT * FROM #__viaggio_course WHERE date = '".date('Y-m-01')."' LIMIT 1");
                                            $course = $db->loadObject();
                                            if (isset($course->amount))
                                                $course = $course->amount/100;
                                            else
                                                $course = 1;

                                            if (isset($this->item->changes[$service->partner_id]) && isset($this->item->changes[$service->partner_id][$service->id]))
                                            {
                                                if ($service->amount_euro != $old_amount_euro)
                                                {
                                                    echo ($service->amount_euro/100).' евро = '.($old_amount_euro/100).' евро ';
                                                    foreach ($this->item->changes[$service->partner_id][$service->id] as $change)
                                                    {
                                                        if ($change['amount_euro'] > 0)
                                                            echo '+ '.($change['amount_euro']/100).' евро';
                                                        elseif ($change['amount_euro'] < 0)
                                                            echo ($change['amount_euro']/100).' евро';
                                                    }
                                                    echo ' | ';
                                                }
                                                if ($service->amount_rub != $old_amount_rub)
                                                {
                                                    echo ($service->amount_rub/100).' руб = '.($old_amount_rub/100).' руб ';
                                                    foreach ($this->item->changes[$service->partner_id][$service->id] as $change)
                                                    {
                                                        if ($change['amount_rub'] > 0)
                                                            echo '+ '.($change['amount_rub']/100).' руб';
                                                        elseif ($change['amount_rub'] < 0)
                                                            echo ($change['amount_rub']/100).' руб';
                                                    }
                                                    if ($service->amount_euro != $old_amount_euro)
                                                        echo " <b>(".(round($service->amount_rub/$course/100-$old_amount_euro/100-$old_amount_rub/$course/100,2))." евро)</b>";
                                                }
                                            }
                                        } ?>
                                    </div>
                                    <button onclick="jQuery(this).parent().remove()">Удалить строку</button>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="service" blockid=<?php echo $num; ?>></div>
                                Добавить сервис:
                                <select id="servicesList">
                                    <?php
                                    foreach ($servicesList as $oneService)
                                    {
                                        echo '<option value="'.$oneService->id.'">'.$oneService->name_rus.'</option>';
                                    }
                                    ?>
                                </select>
                                <button onclick="addServiceRow();return false;">Добавить</button>
                                <div class="control-group servicetoclone" blockid="" style="display: none">
                                    <div class="control-label"></div>
                                    <div class="controls">
                                        Поставщик:
                                        <?php
                                        foreach ($partners_of_services as $key=>$val)
                                        {
                                            ?>
                                            <select nametemplate="service[<?php echo $key; ?>][blockid][partner_id]">
                                                <?php
                                                foreach ($val as $partner)
                                                {
                                                    echo "<option value='".$partner->id."'>".$partner->name_rus."</option>";
                                                }
                                                ?>
                                            </select>
                                            <?php
                                        }
                                        ?>
										  Цена в евро: <input nametemplate="service[serviceid][blockid][amount_euro]" class="amount_euro">
                                        Цена в рублях: <input nametemplate="service[serviceid][blockid][amount_rub]" class="amount_rub">
                                      
                                    </div>
                                    <button onclick="jQuery(this).parent().remove()">Удалить строку</button>
                                </div>

                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>
            <script>
                function addServiceRow()
                {
                    if (jQuery('.service').length > 0)
                        var lastBlockId = parseInt(jQuery('.service:last').attr('blockid'));
                    else
                        var lastBlockId = 0;
                    var serviceToAddId = jQuery('#servicesList').val();
                    var serviceToAddName = jQuery('#servicesList option[value="'+serviceToAddId+'"]').text();
                    jQuery('.service:last').after(jQuery('.servicetoclone').clone());
                    jQuery('.servicetoclone:first').addClass('service');
                    jQuery('.servicetoclone:first').removeClass('servicetoclone');
                    jQuery('.service:last .control-label').html(serviceToAddName);
                    jQuery('.service:last [nametemplate*="service['+serviceToAddId+']"]').attr('name','service['+serviceToAddId+']['+(lastBlockId+1)+'][partner_id]');
                    jQuery('.service:last [nametemplate*="service['+serviceToAddId+']"]').removeAttr('nametemplate');
                    jQuery('.service:last select[nametemplate]').next().remove();
                    jQuery('.service:last select[nametemplate]').remove();
                    jQuery('.service:last select[name]').next().remove();
                    jQuery('.service:last select[name]').chosen();
                    jQuery('.service:last [nametemplate*=serviceid]').each(function(){
                        var nametemplate = jQuery(this).attr('nametemplate').replace('serviceid',serviceToAddId).replace('blockid',lastBlockId+1);
                        jQuery(this).attr('name',nametemplate);
                    });
                    jQuery('.service:last').attr('blockid',lastBlockId+1);
                    jQuery('.service:last').show();

                    calcSummSet();
					
                }
                function calcSummSet() {
                    jQuery('.amount_rub').off('change');
                    jQuery('.amount_rub').on('change',function () {
                        var summRub = 0;
                        jQuery('.amount_rub').each(function () {
                            var amount = parseFloat(jQuery(this).val().replace(',','.'));
                            if (amount)
                                summRub += amount;
                        });
                        jQuery('#summRub').html(summRub);
                    });

                    jQuery('.amount_euro').off('change');
                    jQuery('.amount_euro').on('change',function () {
                        var summEur = 0;
                        jQuery('.amount_euro').each(function () {
                            var amount = parseFloat(jQuery(this).val().replace(',','.'));
                            if (amount)
                                summEur += amount;
                        });
                        jQuery('#summEur').html(summEur);
                    });
                }
                calcSummSet();
            </script>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'cancellation', 'АННУЛЯЦИЯ'); ?>
                <div class="row-fluid">
                    <div class="span10 form-horizontal">
                        <?php echo $this->form->renderField('cancellation'); ?>
                        <?php echo $this->form->renderField('cancellation_ita'); ?>
                    </div>
                </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php
            $db = JFactory::getDbo();
            $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = '".$this->item->id."' AND status <> 0 AND hidden <> 1 ORDER BY id DESC");
            $manualpayments = $db->loadObjectList();
            if (count($manualpayments) > 0) {
                echo JHtml::_('bootstrap.addTab', 'myTab', 'payments', 'Платежи');
                foreach ($manualpayments as $manualpayment)
                {
                    include ('/home/russiantour/web/russiantour.com/public_html/administrator/components/com_viaggio/includes/order_payments.php');
                }
                echo JHtml::_('bootstrap.endTab');
            }
            ?>
            <?php /*if ($this->item->full_payment == 1) { ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'services2', 'Изменение'); ?>
                <div class="row-fluid">
                    <div class="span10 form-horizontal">
                        <fieldset class="adminform">
                            <!-- УСЛУГИ2 -->

                            <div class="uk-inline uk-width-1-1">
                                <div class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small">
                                    <?php
                                    $num2 = 0;
                                    $summRub2 = 0;
                                    $summEur2 = 0;
                                    foreach ($services2 as $key2=>$service2)
                                    {
                                        $num2 = $key2+1;
                                        $summRub2 += $service2->amount_rub/100;
                                        $summEur2 += $service2->amount_euro/100;
                                        ?>
                                        <div class="control-group service" blockid2="<?php echo $key2; ?>">
                                            <div class="control-label"><?php echo $service2->name_rus; ?></div>
                                            <div class="controls">
                                                Поставщик:
                                                <select name="service2[<?php echo $service2->id; ?>][<?php echo $key2; ?>][partner_id]">
                                                    <?php
                                                    foreach ($partners_of_services[$service2->id] as $partner2)
                                                    {
                                                        echo "<option value='".$partner2->id."' ".(($service2->partner_id == $partner2->id)?'selected':'').">".$partner2->name_rus."</option>";
                                                    }
                                                    ?>
                                                </select>

                                                Цена в евро: <input class="amount_euro2" name="service2[<?php echo $service2->id; ?>][<?php echo $key2; ?>][amount_euro]" value="<?php echo $service2->amount_euro/100; ?>">
                                                Цена в рублях: <input class="amount_rub2" name="service2[<?php echo $service2->id; ?>][<?php echo $key2; ?>][amount_rub]" value="<?php echo $service2->amount_rub/100; ?>">
                                            </div>
                                            <button onclick="jQuery(this).parent().remove()">Удалить строку</button>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <div class="service2" blockid2=<?php echo $num2; ?>></div>
                                    Добавить сервис:
                                    <select id="servicesList2">
                                        <?php
                                        foreach ($servicesList as $oneService)
                                        {
                                            echo '<option value="'.$oneService->id.'">'.$oneService->name_rus.'</option>';
                                        }
                                        ?>
                                    </select>
                                    <button onclick="addServiceRow2();return false;">Добавить</button>
                                    <div class="control-group servicetoclone2" blockid2="" style="display: none">
                                        <div class="control-label"></div>
                                        <div class="controls">
                                            Поставщик:
                                            <?php
                                            foreach ($partners_of_services as $key=>$val)
                                            {
                                                ?>
                                                <select nametemplate2="service2[<?php echo $key; ?>][blockid][partner_id]">
                                                    <?php
                                                    foreach ($val as $partner)
                                                    {
                                                        echo "<option value='".$partner->id."'>".$partner->name_rus."</option>";
                                                    }
                                                    ?>
                                                </select>
                                                <?php
                                            }
                                            ?>
                                            Цена в евро: <input nametemplate2="service2[serviceid][blockid][amount_euro]" class="amount_euro2">
                                            Цена в рублях: <input nametemplate2="service2[serviceid][blockid][amount_rub]" class="amount_rub2">

                                        </div>
                                        <button onclick="jQuery(this).parent().remove()">Удалить строку</button>
                                    </div>

                                </div>
                            </div>
                            <input type="submit">
                        </fieldset>
                    </div>
                </div>
                <script>
                    function addServiceRow2()
                    {
                        if (jQuery('.service2').length > 0)
                            var lastBlockId2 = parseInt(jQuery('.service2:last').attr('blockid2'));
                        else
                            var lastBlockId2 = 0;
                        var serviceToAddId2 = jQuery('#servicesList2').val();
                        var serviceToAddName2 = jQuery('#servicesList2 option[value="'+serviceToAddId2+'"]').text();
                        jQuery('.service2:last').after(jQuery('.servicetoclone2').clone());
                        jQuery('.servicetoclone2:first').addClass('service2');
                        jQuery('.servicetoclone2:first').removeClass('servicetoclone2');
                        jQuery('.service2:last .control-label').html(serviceToAddName2);
                        jQuery('.service2:last [nametemplate2*="service2['+serviceToAddId2+']"]').attr('name','service2['+serviceToAddId2+']['+(lastBlockId2+1)+'][partner_id]');
                        jQuery('.service2:last [nametemplate2*="service2['+serviceToAddId2+']"]').removeAttr('nametemplate2');
                        jQuery('.service2:last select[nametemplate2]').next().remove();
                        jQuery('.service2:last select[nametemplate2]').remove();
                        jQuery('.service2:last select[name]').next().remove();
                        jQuery('.service2:last select[name]').chosen();
                        jQuery('.service2:last [nametemplate2*=serviceid]').each(function(){
                            var nametemplate2 = jQuery(this).attr('nametemplate2').replace('serviceid',serviceToAddId2).replace('blockid',lastBlockId2+1);
                            jQuery(this).attr('name',nametemplate2);
                        });
                        jQuery('.service2:last').attr('blockid2',lastBlockId2+1);
                        jQuery('.service2:last').show();

                        calcSummSet2();
                    }
                    function calcSummSet2() {
                        jQuery('.amount_rub2').off('change');
                        jQuery('.amount_rub2').on('change',function () {
                            var summRub2 = 0;
                            jQuery('.amount_rub2').each(function () {
                                var amount2 = parseFloat(jQuery(this).val().replace(',','.'));
                                if (amount2)
                                    summRub2 += amount2;
                            });
                            jQuery('#summRub2').html(summRub2);
                        });

                        jQuery('.amount_euro2').off('change');
                        jQuery('.amount_euro2').on('change',function () {
                            var summEur2 = 0;
                            jQuery('.amount_euro2').each(function () {
                                var amount2 = parseFloat(jQuery(this).val().replace(',','.'));
                                if (amount2)
                                    summEur2 += amount2;
                            });
                            jQuery('#summEur2').html(summEur2);
                        });
                    }
                    calcSummSet2();
                </script>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php }*/ ?>
		<?php /* if (JFactory::getUser()->authorise('core.admin','viaggio')) : ?>
	<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php echo $this->form->getInput('rules'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); ?>
<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); */?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>
    </div>
	</div>
</form>
</body>
<script>
    function cloneBlock(name,num)
    {
        if (typeof num == 'undefined')
            num = 0;
        var lastBlockId = parseInt(jQuery('.'+name+':last').attr('block-id'));
        jQuery('.'+name+':last').after(jQuery('.'+name+':first').clone());
        jQuery('.'+name+':last').attr('block-id',lastBlockId+1);
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] input').each(function(){
            var inputName = jQuery(this).attr('name');
            if (inputName)
            {
                if (inputName.indexOf('amount') > 0)
                    jQuery(this).remove();
                else
                {
                    jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
                    jQuery(this).val('');
                }
            } 
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] select').each(function(){
            jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
            jQuery(this).next().remove();
            jQuery(this).chosen();
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] .field-calendar').each(function(){
            JoomlaCalendar.init(this);
			
        });
        jQuery('.'+name+'-remove').show();

        if (jQuery('#number_of_blocks').length > 0)
        {
            var number_of_blocks = parseInt(jQuery('#number_of_blocks').val());
            if (number_of_blocks > 1 && num < number_of_blocks-1)
            {
                num++;
                cloneBlock(name,num);
            }
        }
	
    }

    function removeBlock(name) {
        jQuery('.'+name+':last').remove();
        var lastBlockId = parseInt(jQuery('.'+name+':last').attr('block-id'));
        if (lastBlockId < 1)
            jQuery('.'+name+'-remove').hide();
			
    }
</script>
<script>
    jQuery(document).ready(function() {
        jQuery('#tour-cat').on('change',function () {
            jQuery('input[name=trip_id]').val(0);
            jQuery('#jform_trip_id').find('option[class!=const]').remove();
            let id = jQuery(this).val();
            if (id > 0)
            {
                jQuery('.choose_tour').show();
                jQuery.getJSON('/eng/?option=com_viaggio&task=tripsByCategory&key=2VKAFw6iw8ebVH2Rr1eE&cat_id='+id,function(data){
                    jQuery.each(data.data, function(id, name_rus) {
                        jQuery('#jform_trip_id').append('<option value="'+id+'" datefrom="'+data.full_data[id].date_from+'" dateto="'+data.full_data[id].date_to+'">'+name_rus+'</option>');
                    });
                    jQuery('#jform_trip_id').trigger("liszt:updated");
                });
            }
            else
                jQuery('.choose_tour').hide();
        });
        jQuery('#jform_trip_id').on('change',function(){
            jQuery('input[name=trip_id]').val(jQuery(this).val());
        });
        jQuery('#parent-cat').on('change',function(){
            jQuery('#tour-cat').val(0);
            jQuery('#tour-cat').find('option[class!=const]').remove();
            let id = jQuery(this).val();

            if (id > 0)
            {
                jQuery('.tour-cat-parent').show();
                jQuery.getJSON('/eng/?option=com_viaggio&task=tripsSubcategories&key=2VKAFw6iw8ebVH2Rr1eE&id='+id,function(data){
                    jQuery.each(data.data, function(id, title_ita) {
                        jQuery('#tour-cat').append('<option value="'+id+'">'+title_ita+'</option>');
                    });
                    jQuery('#tour-cat').trigger("liszt:updated");
                });
            }
            else
                jQuery('.tour-cat-parent').hide();
        });
    });
</script>
</html>