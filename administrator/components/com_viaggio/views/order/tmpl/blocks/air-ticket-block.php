<hr/>

<div class="air-ticket" block-id="<?php echo $n; ?>">
    <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="air-ticket[<?php echo $n; ?>][amount]" value="<?php echo (isset($this->item->airTickets) && isset($this->item->airTickets[$n]['amount']))?$this->item->airTickets[$n]['amount']:''; ?>"><br>

    <?php echo JHtml::_('calendar', isset($this->item->airTickets)?str_replace('/','-',$this->item->airTickets[$n]['begin']):'', "air-ticket[".$n."][begin]", "air-ticket[".$n."][begin]", '%d/%m/%Y', array('placeholder'=>"Дата начало тура")); ?>

    <?php echo JHtml::_('calendar', isset($this->item->airTickets)?str_replace('/','-',$this->item->airTickets[$n]['end']):'', "air-ticket[".$n."][end]", "air-ticket[".$n."][end]", '%d/%m/%Y', array('placeholder'=>"Дата завершение тура")); ?>

    Откуда?<br/>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[<?php echo $n; ?>][from-city-rus]" value="<?php echo isset($this->item->airTickets)?$this->item->airTickets[$n]['from-city-rus']:''; ?>">
    </div>

    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[<?php echo $n; ?>][from-city-ita]" value="<?php echo (isset($this->item->airTickets) && isset($this->item->airTickets[$n]['from-city-ita']))?$this->item->airTickets[$n]['from-city-ita']:''; ?>">
    </div>

    Куда?<br>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[<?php echo $n; ?>][to-city-rus]" value="<?php echo isset($this->item->airTickets)?$this->item->airTickets[$n]['to-city-rus']:''; ?>">
    </div>

    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[<?php echo $n; ?>][to-city-ita]" value="<?php echo isset($this->item->airTickets)?$this->item->airTickets[$n]['to-city-ita']:''; ?>">
    </div>

    Обратно<br>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[<?php echo $n; ?>][back-city-rus]" value="<?php echo isset($this->item->airTickets)?$this->item->airTickets[$n]['back-city-rus']:''; ?>">
    </div>

    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[<?php echo $n; ?>][back-city-ita]" value="<?php echo isset($this->item->airTickets)?$this->item->airTickets[$n]['back-city-ita']:''; ?>">
    </div>
</div>