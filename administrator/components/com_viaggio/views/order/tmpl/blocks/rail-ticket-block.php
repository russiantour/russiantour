<hr/>
<div class="rail-ticket" block-id="<?php echo $n; ?>">
    <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="rail-ticket[<?php echo $n; ?>]][amount]" value="<?php echo (isset($this->item->railTickets) && isset($this->item->railTickets[$n]['amount']))?$this->item->railTickets[$n]['amount']:''; ?>"><br>
    <?php echo JHtml::_('calendar', isset($this->item->railTickets)?str_replace('/','-',$this->item->railTickets[$n]['begin']):'', "rail-ticket[".$n."][begin]", "rail-ticket[".$n."][begin]", '%d/%m/%Y', array('placeholder'=>"Дата начало тура")); ?>
    <?php echo JHtml::_('calendar', isset($this->item->railTickets)?str_replace('/','-',$this->item->railTickets[$n]['end']):'', "rail-ticket[".$n."][end]", "rail-ticket[".$n."][end]", '%d/%m/%Y', array('placeholder'=>"Дата завершение тура")); ?>
    <br>Откуда?
    <br>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="rail-ticket[<?php echo $n; ?>][from-city-rus]" value="<?php echo isset($this->item->railTickets)?$this->item->railTickets[$n]['from-city-rus']:''; ?>">
    </div>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="rail-ticket[<?php echo $n; ?>][from-city-ita]" value="<?php echo isset($this->item->railTickets)?$this->item->railTickets[$n]['from-city-ita']:''; ?>">
    </div>
    Куда?<br>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="rail-ticket[<?php echo $n; ?>][to-city-rus]" value="<?php echo isset($this->item->railTickets)?$this->item->railTickets[$n]['to-city-rus']:''; ?>">
    </div>
    <div class="uk-margin">
        <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="rail-ticket[<?php echo $n; ?>][to-city-ita]" value="<?php echo isset($this->item->railTickets)?$this->item->railTickets[$n]['to-city-ita']:''; ?>">
    </div>
</div>