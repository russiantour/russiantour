<div class="client" block-id="<?php echo $n; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][cognome]" placeholder="cognome" value="<?php echo isset($this->item->clients[$n]['cognome'])?$this->item->clients[$n]['cognome']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][nome]" placeholder="nome" value="<?php echo isset($this->item->clients[$n]['nome'])?$this->item->clients[$n]['nome']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][numero_di_passaporto]" placeholder="numero_di_passaporto" value="<?php echo isset($this->item->clients[$n]['numero_di_passaporto'])?$this->item->clients[$n]['numero_di_passaporto']:''; ?>">
    <select name="clients[<?php echo $n; ?>][sex]"><option value="m" <?php if ($this->item->clients[$n]['sex'] == 'm') echo 'selected'; ?>>Мужик</option><option value="f" <?php if ($this->item->clients[$n]['sex'] == 'f') echo 'selected'; ?>>Леди</option></select>
    <?php echo JHtml::_('calendar', isset($this->item->clients)?str_replace('/','-',$this->item->clients[$n]['birthday']):'', "clients[".$n."][nascita]", "clients[".$n."][nascita]", '%d/%m/%Y', array('placeholder'=>"nascita")); ?>
    <input type="text" name="clients[<?php echo $n; ?>][telephone]" placeholder="Telephone" value="<?php echo isset($this->item->clients[$n]['telephone'])?$this->item->clients[$n]['telephone']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][indirizzo_di_residenza]" placeholder="indirizzo_di_residenza" value="<?php echo isset($this->item->clients[$n]['indirizzo_di_residenza'])?$this->item->clients[$n]['indirizzo_di_residenza']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][luogo_di_nascita]" placeholder="luogo_di_nascita" value="<?php echo isset($this->item->clients[$n]['luogo_di_nascita'])?$this->item->clients[$n]['luogo_di_nascita']:''; ?>">
    <?php echo JHtml::_('calendar', isset($this->item->clients)?str_replace('/','-',$this->item->clients[$n]['date_from']):'', "clients[".$n."][date_from]", "clients[".$n."][date_from]", '%d/%m/%Y', array('placeholder'=>"data_di_rilascio_from")); ?>
    <?php echo JHtml::_('calendar', isset($this->item->clients)?str_replace('/','-',$this->item->clients[$n]['date_to']):'', "clients[".$n."][date_to]", "clients[".$n."][date_to]", '%d/%m/%Y', array('placeholder'=>"data_di_rilascio_to")); ?>
    <input type="text" name="clients[<?php echo $n; ?>][numero_di_visite]" placeholder="numero_di_visite" value="<?php echo $n; ?>" value="<?php echo isset($this->item->clients[$n]['numero_di_visite'])?$this->item->clients[$n]['numero_di_visite']:''; ?>">
    <?php echo JHtml::_('calendar', isset($this->item->clients)?str_replace('/','-',$this->item->clients[$n]['last_visit_from']):'', "clients[".$n."][last_visit_from]", "clients[".$n."][last_visit_from]", '%d/%m/%Y', array('placeholder'=>"last_visit_from")); ?>
    <?php echo JHtml::_('calendar', isset($this->item->clients)?str_replace('/','-',$this->item->clients[$n]['last_visit_to']):'', "clients[".$n."][last_visit_to]", "clients[".$n."][last_visit_to]", '%d/%m/%Y', array('placeholder'=>"last_visit_to")); ?>
    <input type="text" name="clients[<?php echo $n; ?>][nome_dell_organizzazione]" placeholder="organization" value="<?php echo isset($this->item->clients[$n]['nome_dell_organizzazione'])?$this->item->clients[$n]['nome_dell_organizzazione']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][ufficio_dell_organizzazione]" placeholder="position" value="<?php echo isset($this->item->clients[$n]['ufficio_dell_organizzazione'])?$this->item->clients[$n]['ufficio_dell_organizzazione']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][indirizzo_dell_organizzazione]" placeholder="address" value="<?php echo isset($this->item->clients[$n]['indirizzo_dell_organizzazione'])?$this->item->clients[$n]['indirizzo_dell_organizzazione']:''; ?>">
    <input type="text" name="clients[<?php echo $n; ?>][telefono_dell_organizzazione]" placeholder="telefono_dell_organizzazione" value="<?php echo isset($this->item->clients[$n]['telefono_dell_organizzazione'])?$this->item->clients[$n]['telefono_dell_organizzazione']:''; ?>">
    <input type="email" name="clients[<?php echo $n; ?>][email]" class="validate-email" placeholder="Email" value="<?php echo isset($this->item->clients[$n]['email'])?$this->item->clients[$n]['email']:''; ?>">
    <br/>
    Nationality:
    <select name="clients[<?php echo $n; ?>][nazionalita]">
        <?php foreach ($this->item->nationalityList as $nationality) { ?>
            <option value="<?=$nationality->value?>" <?php echo (isset($this->item->clients[$n]['nazionalita']) && $nationality->value==$this->item->clients[$n]['nazionalita']) ?> >
                <?=JText::_($nationality->translation, true)?>
            </option>
        <?php } ?>
    </select>
    <hr/>
</div>