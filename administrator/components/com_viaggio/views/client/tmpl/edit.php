<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');
?>
<script type="text/javascript">
	js = jQuery.noConflict();
	js(document).ready(function () {
		
	js('input:hidden.tour_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('tour_idhidden')){
			js('#jform_tour_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_tour_id").trigger("liszt:updated");
	js('input:hidden.order_id').each(function(){
		var name = js(this).attr('name');
		if(name.indexOf('order_idhidden')){
			js('#jform_order_id option[value="'+js(this).val()+'"]').attr('selected',true);
		}
	});
	js("#jform_order_id").trigger("liszt:updated");
	});

	Joomla.submitbutton = function (task) {
		if (task == 'client.cancel') {
			Joomla.submitform(task, document.getElementById('client-form'));
		}
		else {
			
			if (task != 'client.cancel' && document.formvalidator.isValid(document.id('client-form'))) {
				
				Joomla.submitform(task, document.getElementById('client-form'));
			}
			else {
				alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
			}
		}
	}
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="client-form" class="form-validate">

	<div class="form-horizontal">
		<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

		<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_VIAGGIO_TITLE_CLIENT', true)); ?>
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">

									<?php echo $this->form->renderField('id'); ?>
				<?php echo $this->form->renderField('created_by'); ?>
				<?php echo $this->form->renderField('tour_id'); ?>
				<?php echo $this->form->renderField('order_id'); ?>
				<?php echo $this->form->renderField('edited'); ?>
				<hr/>

			<?php
				foreach((array)$this->item->tour_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="tour_id" name="jform[tour_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>				<?php echo $this->form->renderField('cognome'); ?>
    <?php echo $this->form->renderField('nome'); ?>
		<?php echo $this->form->renderField('sex'); ?>
		<?php echo $this->form->renderField('birthday'); ?>
		<?php echo $this->form->renderField('nazionalita'); ?>
		<?php echo $this->form->renderField('numero_di_passaporto'); ?>
		<?php echo $this->form->renderField('telephone'); ?>
		<?php echo $this->form->renderField('indirizzo_di_residenza'); ?>
		<?php echo $this->form->renderField('luogo_di_nascita'); ?>
		<?php //echo $this->form->renderField('serie_passaporto'); ?> 
		data_di_rilascio_from<?php echo $this->form->renderField('date_from'); ?>
		data_di_rilascio_to<?php echo $this->form->renderField('date_to'); ?>
		<?php echo $this->form->renderField('numero_di_visite'); ?>
		<?php echo $this->form->renderField('last_visit_from'); ?>
		<?php echo $this->form->renderField('last_visit_to'); ?>
		organization<?php echo $this->form->renderField('nome_dell_organizzazione'); ?>
		position<?php echo $this->form->renderField('ufficio_dell_organizzazione'); ?>
		address<?php echo $this->form->renderField('indirizzo_dell_organizzazione'); ?>
		phone<?php echo $this->form->renderField('telefono_dell_organizzazione'); ?>
		<?php echo $this->form->renderField('email'); ?>

			<?php
				foreach((array)$this->item->order_id as $value): 
					if(!is_array($value)):
						echo '<input type="hidden" class="order_id" name="jform[order_idhidden]['.$value.']" value="'.$value.'" />';
					endif;
				endforeach;
			?>

					<?php if ($this->state->params->get('save_history', 1)) : ?>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('version_note'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('version_note'); ?></div>
					</div>
					<?php endif; ?>
				</fieldset>
			</div>
		</div>
		<?php echo JHtml::_('bootstrap.endTab'); ?>

		<?php if (JFactory::getUser()->authorise('core.admin','viaggio')) : ?>
	<?php /* echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('JGLOBAL_ACTION_PERMISSIONS_LABEL', true)); ?>
		<?php echo $this->form->getInput('rules'); ?>
	<?php echo JHtml::_('bootstrap.endTab'); */ ?>
<?php endif; ?>

		<?php echo JHtml::_('bootstrap.endTabSet'); ?>

		<input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>

	</div>
</form>
