<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');

$db    = JFactory::getDbo();

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_services');
$query->order('name_rus asc');
$db->setQuery($query);
$services = $db->loadObjectList();

if (isset($_GET['id']))
{
    $query = $db->getQuery(true);
    $query->select('*');
    $query->from('#__viaggio_partners_services');
    $query->where('partner_id = '.$_GET['id']);
    $db->setQuery($query);
    $partners_services = $db->loadObjectList();
}
$partners_services_array = array();
if (count($partners_services))
{
    foreach ($partners_services as $partners_service)
        $partners_services_array[] = $partners_service->service_id;
}
?>

<script type="text/javascript">
    Joomla.submitbutton = function (task) {
        if (task == 'partner.cancel') {
            Joomla.submitform(task, document.getElementById('partner-form'));
        }
        else {

            if (task != 'partner.cancel' && document.formvalidator.isValid(document.id('partner-form'))) {

                Joomla.submitform(task, document.getElementById('partner-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form
	action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>"
	method="post" enctype="multipart/form-data" name="adminForm" id="partner-form" class="form-validate">

	<div class="form-horizontal">
		<div class="row-fluid">
			<div class="span10 form-horizontal">
				<fieldset class="adminform">
                    <?php echo $this->form->renderField('id'); ?>
                    <?php echo $this->form->renderField('name_rus'); ?>
                    <?php echo $this->form->renderField('name_ita'); ?>
                    <br/>
                    Предоставляемые услуги:
                    <select name="services[]" multiple>
                        <?php
                        foreach ($services as $service)
                        {
                            echo "<option value='".$service->id."' ".(in_array($service->id,$partners_services_array)?'selected':'').">".$service->name_rus."</option>";
                        }
                        ?>
                    </select>
                </fieldset>
            </div>
        </div>
        <input type="hidden" name="task" value=""/>
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>