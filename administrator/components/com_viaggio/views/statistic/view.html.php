<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class ViaggioViewStatistic extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
    public function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            throw new Exception(implode("\n", $errors));
        }

        ViaggioHelpersViaggio::addSubmenu('statistic');

        if ($this->getLayout() == 'default')
            $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function addToolbar()
	{
		JToolBarHelper::title(JText::_('COM_VIAGGIO_TITLE_STATISTIC'));
        //Filter by user
        $select_label = 'By user';
        $options = array('Все'=>'Все');
        $db = JFactory::getDbo();
        $db->setQuery("SELECT u.id value, u.name text FROM #__users u JOIN #__user_usergroup_map g ON g.user_id = u.id WHERE g.group_id = 10 ORDER BY u.name ASC");
        $users = $db->loadObjectList();
        foreach ($users as $k=>$user)
        {
            $options[$k] = $user;
        }
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_user',
            JHtml::_('select.options', $options , "value", "text", isset($_POST['filter_user'])?$_POST['filter_user']:'', true)
        );

        JHtmlSidebar::addFilter(
            'By pay time',
            'filter_paytime',
            JHtml::_('select.options', ['a'=>'Все','c'=>'Этот месяц'] , "value", "text", isset($_POST['filter_paytime'])?$_POST['filter_paytime']:'a', true)
        );
	}
}
