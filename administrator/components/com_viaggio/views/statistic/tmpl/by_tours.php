<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.js"></script>

<script type="text/javascript"   src="/images/css/2/js/components/tooltip.js"></script>
<?php
JHtml::_('formbehavior.chosen', 'select');
$db = JFactory::getDbo();

/*$db->setQuery("SELECT amount FROM #__viaggio_course ORDER BY date DESC");
$rate = $db->loadObject();
$rate = $rate->amount/100;*/

if (isset($_POST['filter_trip']) && is_array($_POST['filter_trip']) && isset($_POST['filter_trip'][0]) && $_POST['filter_trip'][0] == '')
   unset($_POST['filter_trip'][0]);

$query = "
select
       t.name_ita tour_name,
       t.date_from,
       t.date_to,
       mp.order_id,
       p.name_ita partner_name,
       r.amount_rub,
       r.amount_euro,
       u.name
from #__viaggio_orders o
                left join #__viaggio_manualpayments mp on mp.order_id = o.id
                left join #__viaggio_orders_relations r on r.order_id = o.id
                left join #__viaggio_partners p on p.id = r.partner_id
                left join #__viaggio_trips t on t.id = o.trip_id
                left join #__users u on u.id = o.created_by
where mp.status > 0
  and partner_id is not null
  and o.trip_id > 0
  ".((isset($_POST['filter_trip']) && is_array($_POST['filter_trip']) && count($_POST['filter_trip']) > 0)?" AND t.id IN (".implode(',',$_POST['filter_trip']).") ":"")."
  group by mp.order_id, partner_name
order by o.id desc
";

$db->setQuery($query);
$rows = $db->loadObjectList();

$users = [];
$users_total = [];
$partner_total = [];
$total_amount_euro = 0;
$total_amount_rub = 0;

foreach ($rows as $row)
{
    $row->tour_name = $row->tour_name.' ('.$row->date_from.' - '.$row->date_to.')<br/><a href="/administrator/index.php?option=com_viaggio&view=order&layout=edit&id='.$row->order_id.'">'.$row->order_id.'</a>';
    if (!isset($users[$row->name]))
        $users[$row->name] = [];
    if (!isset($users[$row->name][$row->tour_name]))
        $users[$row->name][$row->tour_name] = [];
    if (!isset($users[$row->name][$row->tour_name][$row->partner_name]))
        $users[$row->name][$row->tour_name][$row->partner_name] = ['euro'=>0,'rub'=>0];

    if (!isset($users_total[$row->name]))
        $users_total[$row->name] = [];
    if (!isset($users_total[$row->name][$row->partner_name]))
        $users_total[$row->name][$row->partner_name] = ['euro'=>0,'rub'=>0];

    if (!isset($partner_total[$row->partner_name]))
        $partner_total[$row->partner_name] = ['euro'=>0,'rub'=>0];

    if ($row->amount_euro > 0)
    {
        $users[$row->name][$row->tour_name][$row->partner_name]['euro'] += $row->amount_euro/100;
        $users_total[$row->name][$row->partner_name]['euro'] += $row->amount_euro/100;
        $partner_total[$row->partner_name]['euro'] += $row->amount_euro/100;
        $total_amount_euro += $row->amount_euro/100;
    }

    if ($row->amount_rub > 0)
    {
        $users[$row->name][$row->tour_name][$row->partner_name]['rub'] += $row->amount_rub/100;
        $users_total[$row->name][$row->partner_name]['rub'] += $row->amount_rub/100;
        $partner_total[$row->partner_name]['rub'] += $row->amount_rub/100;
        $total_amount_rub += $row->amount_rub/100;
    }
}

?>
<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=statistic&layout=by_tours'); ?>" method="post" id="j-sidebar-container" class="span2">
    <?php echo $this->sidebar; ?>
    <div class="filter-select">
        <hr/>
        Выберите туры:
        <br/>
        <select id="filter_trip" name="filter_trip[]" multiple="multiple">
            <?php
            $db->setQuery("SELECT id value, name_ita text, date_from, date_to FROM #__viaggio_trips WHERE state is null OR state > -2 ORDER BY ordering ASC");
            $trips = $db->loadObjectList();
            foreach ($trips as $trip)
            {
                echo "<option value='".$trip->value."' ".((isset($_POST['filter_trip']) && in_array($trip->value,$_POST['filter_trip'])?'selected':'')).">".$trip->text." (".$trip->date_from." - ".$trip->date_to.")</option>";
                $options[$k] = $trip;
            }
            ?>
        </select>
        <br/>
        <br/>
        <input type="submit" value="Фильтровать">
        <br/>
        <hr/>
        <input id="save_filter_name">
        <br/>
        <br/>
        <input type="button" value="Сохранить фильтр" onclick="saveFilterByTours()">
        <br/>
        <br/>
        <div>
            Сохранённые фильтры:
            <br/>
            <?php foreach (ViaggioHelpersViaggio::getReportByTourFilters() as $filter) { ?>
                <div onclick="fillByToursFilter('<?= $filter["data"] ?>')">
                    <span class="choose_filter"><?=$filter['name'] ?></span>
                    <span title="Удалить" class="delete_filter" onclick="jQuery.get('/administrator/index.php?option=com_viaggio&task=deleteReportByTourFilter&id=<?= $filter['id'] ?>'); jQuery(this).remove();">-</span>
                </div>
            <?php } ?>
        </div>
    </div>
</form>
<div id="j-main-container" class="span10">
    <table>
        <?php $lastUser = ''; foreach ($users as $user=>$tours)  { ?>
            <tr>
                <td class="by_tours_tours">
                    <?php if ($lastUser != $user) echo $user; $lastUser = $user; ?>
                    <table>
                        <?php $lastTour= ''; foreach ($tours as $tour=>$partners) { ?>
                            <?php foreach ($partners as $partner=>$amount) { ?>
                                <tr>
                                    <td><?php if ($lastTour!=$tour) echo $tour; $lastTour = $tour; ?></td>
                                    <td><?php echo $partner; ?></td>
                                    <td><?php echo $amount['rub'].' руб.'; ?></td>
                                    <td><?php echo $amount['euro'].'€'; ?></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
                <td class="by_tours_middle"></td>
                <td class="by_tours_total">
                    <table>
                        <?php foreach ($users_total[$user] as $partner=>$amount) { ?>
                            <tr>
                                <td><?php echo $partner; ?></td>
                                <td><?php echo $amount['rub'].' руб.'; ?></td>
                                <td><?php echo $amount['euro'].'€'; ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td><hr/></td>
                <td><hr/></td>
            </tr>
        <?php } ?>
    </table>
    <br/>
    <br/>
    Всего по партнёрам:
    <table>
        <?php foreach ($partner_total as $partner=>$amount) { ?>
            <tr>
                <td><?php echo $partner; ?></td>
                <td class="by_tours_total_partners_middle"></td>
                <td><?php echo $amount['rub'].' руб.'; ?></td>
                <td><?php echo $amount['euro'].'€'; ?></td>
            </tr>
        <?php } ?>
        <tr class="by_tours_total_partners_last">
            <td>Общий результат: </td>
            <td class="by_tours_total_partners_middle"></td>
            <td><?php echo $total_amount_rub.' руб.'; ?></td>
            <td><?php echo $total_amount_euro.'€'; ?></td>
        </tr>
    </table>
</div>
<style>
    .by_tours_tours {width: 550px}
    .by_tours_tours table {width: 100%}

    .by_tours_middle {width: 100px;}

    .by_tours_total {width: 250px;}
    .by_tours_total table {width: 100%}

    .by_tours_total_partners_middle {width: 50px;}
    .by_tours_total_partners_last td {font-weight: bold}

    .delete_filter {
        margin-left: 20px;
        font-weight: bold;
        cursor: pointer;
        width: 25px;
        height: 25px;
        display: inline-block;
    }

    .delete_filter:hover {
        color: red;
    }

    .choose_filter {
        cursor: pointer;
    }

    .choose_filter:hover {
        text-decoration: underline;
    }
</style>
<script>
    jQuery(document).ready(function(){
        jQuery('#filter_trip').chosen();
    });
    function fillByToursFilter(data) {
        jQuery('#filter_trip').chosen('destroy');
        jQuery('#filter_trip').val([]);
        jQuery('#filter_trip').val(data.split(','));
        jQuery('#filter_trip').chosen();
        jQuery('#j-sidebar-container').submit();
    }
    function saveFilterByTours() {
        jQuery.get(
            '/administrator/index.php?option=com_viaggio&task=createReportByTourFilter&name='+encodeURI(jQuery('#save_filter_name').val()).replace(/([\'])/g, "\\'")+'$&data='+jQuery('#filter_trip').val().join(),
            jQuery('#j-sidebar-container').submit()
        );
    }
</script>