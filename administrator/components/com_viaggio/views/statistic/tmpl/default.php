<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.js"></script>
<script type="text/javascript"   src="/images/css/2/js/components/tooltip.js"></script>
<script type="text/javascript"   src="/images/css/2/js/uikit.js"></script>

<?php
//$filterByMinutes = 70;
$db = JFactory::getDbo();
$created_by = '';
$db->setQuery("SELECT u.id value, u.name text FROM #__users u JOIN #__user_usergroup_map g ON g.user_id = u.id WHERE g.group_id = 10 ORDER BY u.name ASC");
$users = $db->loadObjectList();
$users_array = [];
foreach ($users as $user)
{
    $users_array[] = $user->value;
}
$created_by = " WHERE created_by IN (".implode(',',$users_array).") ";
$user = JFactory::getUser();
$user_groups = JAccess::getGroupsByUser($user->get('id'));
if (in_array(8,$user_groups))
{
    if (isset($_POST['filter_user']) && intval($_POST['filter_user']))
        $created_by = " WHERE created_by = ".intval($_POST['filter_user']);
}
else
    $created_by = " WHERE created_by = ".$user->get('id');
$db->setQuery("SELECT o.*, t.name_rus, t.name_ita 
FROM #__viaggio_orders o 
JOIN #__viaggio_manualpayments mp on mp.order_id = o.id
JOIN #__viaggio_trips t ON o.trip_id = t.id ".$created_by." 
 and mp.payTime is not null
group by o.id
ORDER BY mp.payTime asc");
if (isset($_GET['showdata']) || (is_array($_POST) && count($_POST)>0))
    $orders = $db->loadObjectList();
else
{
    $orders = array();
}

if (in_array(8,$user_groups)) {
?>


<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=statistic'); ?>" method="post" id="j-sidebar-container" class="span2">
     <?php echo $this->sidebar; ?>
    <div class="filter-select">
        Не раньше чем<br/>
        <input name="filter_byminutes" value="<?php echo (isset($_POST['filter_byminutes'])?$_POST['filter_byminutes']:''); ?>">
        <br/>минут назад
        <br/>
        <input type="submit" value="Фильтровать">
    </div>
</form>
<?php } ?>
<div id="j-main-container" class="span10">
	<form method="post" action="index.php?option=com_viaggio&task=savemonthstatistic">
        <?php if (count($orders) < 1) {
            echo '<button onclick="jQuery(\'#loading_text\').show(); jQuery(this).hide(); window.location.href = \'/administrator/index.php?option=com_viaggio&view=statistic&showdata=1\'; return false; ">Получить данные</button><br/>';
        } ?>
        <div id="loading_text" style="display: none">
            <b>Загрузка данных</b>
            <br/>
            <b>Подождите несколько секунд...</b>
        </div>
        Сумма всех сумм<div id="summ_of_summs"></div>
		<span class="uk-num"></span>
        Сумма costo paraliziale: <div id="total_costo_paraliziale"></div>
		<span class="uk-num"></span>
        TOTALE GUADAGNI: <div id="totale_guadagni"></div>
        <span class="uk-num"></span>
        % PROFITTO <div id="profito"></div>
        <span class="uk-num"></span>
        ЗП <div id="zp"></div>
        ЗП2 <div id="zp2"></div>
        <br/>
        <select id="files" onchange="getInfoFromFile(jQuery(this).val())">
            <option value=""> ---- </option>
            <?php
            $files = scandir('/home/russiantour/web/russiantour.com/public_html/images/nalog/user/');
            foreach ($files as $file)
            {
                if (strpos($file,'_'.$user->get('id').'.html') !== false)
                {
                    echo '<option value="'.explode('_',$file)[0].'">'.explode('_',$file)[0].'</option>';
                }
            }
            ?>
        </select>
        <script>
            function getInfoFromFile(date) {
                if (date != '')
                {
                    jQuery.get('/images/nalog/user/'+date+'_<?php echo $user->get('id'); ?>.html',function(data){
                        jQuery('#data-block').html(data);
                        jQuery('#download-excel').attr('href','/images/nalog/user/'+date+'_<?php echo $user->get('id'); ?>.xlsx').show();
                    });
                }
            }
        </script>
        <a href="#" style="display: none" id="download-excel">Скачать excel</a>
			<style>
			.sticky{
				
			}
			.sticky th{
			  position: sticky;
			  top: 0;
			  top: 6em;
			  min-height: 3em;
			  background: lightblue;
			}
			</style>


            <table class="sticky uk-table" border="1">
                <thead>
                    <tr>
                        <th>название тура</th>
                        <th>парнтеры</th>
                        <th>оплата партнёрам</th>
                        <th>в рублях</th>
                        <th>курс</th>
                        <th>полная сумма</th>
                        <th>% ACCONTO</th>
                        <th>Сколько оплатили</th>
                        <th>tmp</th>
                        <th>Сумма оплат парнерам</th>
                        <th>COSTO PARZIALE</th>
                        <th>Full COSTO PARZIALE</th>
                        <th>сколько заработали всего</th>
                        <th>GUADAGNO PARZIALE</th>
                        <th>Profit</th>
                    </tr>
                </thead>
				
                <tbody>
                    <?php
                    $total_costo_paraliziale = 0;
                    $totale_guadagni = 0;
                    $db->setQuery("SELECT * FROM #__viaggio_course WHERE date = '".date('Y-m-01')."' LIMIT 1");
                    $course = $db->loadObject();
                    if (isset($course->amount))
                        $course = $course->amount/100;
                    else
                        $course = 1;
                    $total_sum_paid_this_month = 0;
                    foreach ($orders as $order)
                    {
                        $db->setQuery("SELECT * FROM #__viaggio_clients WHERE order_id = ".$order->id);
                        $clients = $db->loadObjectList();
                        $clients_str = '';
                        foreach ($clients as $client)
                        {
                            $clients_str .= $client->nome.' '.$client->cognome.'<br/>';
                        }

                        $db->setQuery("SELECT * FROM #__viaggio_month_statistic WHERE order_id = ".$order->id." ORDER BY id ASC");
                        $month_statistic = $db->loadObjectList();
                        $month_statistic_array = array();
                        $lastMonthStatisticDate = false;
                        foreach ($month_statistic as $row){
                            $month_statistic_array[] = $row->manualpaymant_id;
                            $lastMonthStatisticDate = $row->timeCreated;
                        }
                        if (isset($_GET['kaktus'])) {echo 'исключения: ';var_dump($month_statistic_array);}
                        $where = '';

                        if (isset($_POST['filter_byminutes']) && intval($_POST['filter_byminutes']))
                        {
                            $where .= " AND payTime IS NOT NULL";
                            $where .= " AND payTime >= '".date('Y-m-d H:i:s',(time()-60*intval($_POST['filter_byminutes'])))."' ";
                            $where .= " AND payTime <= '".date('Y-m-d H:i:s')."' ";
                        }
                        elseif (isset($_POST['filter_paytime']) && $_POST['filter_paytime']=='c')
                        {
                            $where .= " AND payTime IS NOT NULL";
                            $where .= " AND payTime >= '".date('Y-m-01 00:00:00')."' ";
                            $where .= " AND payTime <= '".date('Y-m-d H:i:s')."' ";
                        }
                        /*else
                        {
                            $divider = 60*intval($filterByMinutes);
                            $time = intval(time()/$divider)*$divider;
                            $where .= " AND payTime IS NOT NULL";
                            $where .= " AND payTime >= '".date('Y-m-d H:i:s',$time)."' ";
                            $where .= " AND payTime <= '".date('Y-m-d H:i:s')."' ";
                        }*/
                        //if (isset($_GET['kaktus'])) echo $where;//////////////DEBUG
                        $db->setQuery("SELECT count(*) count FROM #__viaggio_manualpayments WHERE order_id = ".$order->id." and status > 0 and hidden <> 1 ".$where." ORDER BY id ASC");
                        $paymentsCount = $db->loadObject();
                        //////////////////////////DEBUG
                        /*if (isset($_GET['kaktus'])) {
                            echo '<br/>';
                            var_dump($paymentsCount->count);
                            echo '<br/>';
                            echo "SELECT count(*) count FROM #__viaggio_manualpayments WHERE order_id = ".$order->id." and status > 0 and hidden <> 1 ".$where." ORDER BY id ASC";
                            echo '<br/>';
                        }*/
                        //////////////////////////DEBUG
                        if ($paymentsCount->count > 0)
                        {
                            $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = ".$order->id." and status > 0 and hidden <> 1 ORDER BY id ASC");
                            $payments = $db->loadObjectList();

                            $db->setQuery("SELECT count(*) count, sum(payments_count) pcs FROM #__viaggio_month_statistic WHERE skip is null AND manualpaymant_id <> 0 AND order_id = ".$order->id);
                            $monthStatisticCount = $db->loadObject();
                        }

                        $query = "SELECT p.name_rus pname, s.name_rus sname, c.amount_euro, c.amount_rub FROM #__viaggio_orders_relations_postchanges c
                            LEFT JOIN #__viaggio_partners p on p.id = c.partner_id
                            LEFT JOIN #__viaggio_services s ON s.id = c.service_id
                            WHERE c.order_id = ".$order->id.
                            ($lastMonthStatisticDate?" AND c.created_at > '".$lastMonthStatisticDate."' ":"").
                            " ORDER BY c.id ASC";
                        $db->setQuery($query);
                        $changes_table_data = $db->loadObjectList();

                        if (($paymentsCount->count > 0 && $paymentsCount->count > $monthStatisticCount->pcs/*$monthStatisticCount->count*/) || count($changes_table_data) > 0)
                        {
                            $total_euro = 0;
                            $total_rub = 0;
                            $partially_skip = false;
                            if (count($changes_table_data) == 0)
                            {
                                $db->setQuery("SELECT r.id, p.name_rus pname, s.name_rus sname, r.amount_euro, r.amount_rub FROM #__viaggio_orders_relations r
                        JOIN #__viaggio_services s ON s.id = r.service_id
                        JOIN #__viaggio_partners p ON p.id = r.partner_id
                        WHERE r.order_id = ".$order->id." ORDER BY p.name_rus ASC, s.name_rus ASC");
                                $partners = $db->loadObjectList();
                                $pcount = count($partners)+1;
                            }
                            else
                            {
                                $partially_skip = true;
                                $month_statistic = array();
                                $month_statistic_array = array();

                                $order->valute_amount = 0;

                                $partners = $changes_table_data;
                                $pcount = count($changes_table_data)+1;
                            }

                            foreach ($partners as $k=>$partner)
                            {
                                if ($partially_skip && $partner->amount_euro != 0 && $partner->amount_rub != 0)
                                {
                                    $partners[$k]->amount_euro += ($partner->amount_rub/($course*100))*100;
                                    $partners[$k]->amount_rub = 0;
                                }
                                $total_euro += $partner->amount_euro/100;
                                $total_rub += $partner->amount_rub/100;
                            }

                            echo '<tr>';

                            echo '<td rowspan="'.$pcount.'">';
                                                    if ($payments[count($payments)-1]->full_payment==1)
                            {
                                echo '<br/><br/><b>Полный платёж</b><br> ';
                            }
                            echo '<a target="_blank"  href="/administrator/index.php?option=com_viaggio&view=order&layout=edit&id='.$order->id.'">'.$order->id.'</a><br/>'.$order->name_rus.'<br/>'.$order->nomecgome.'<br/>'.$order->email.'<br/>'.$order->created_at.'<br/>'.$order->clientcount.' PAX<br/>'.$clients_str;
                                        /*
                                    foreach ($order->clients as $client)
                                        echo '<a href="?option=com_viaggio&view=client&layout=edit&id='.$client->id.'">'.$client->nome.' '.$client->cognome.'</a>  </br>  '.date('d/m/Y',strtotime($client->birthday)).'   '.$client->numero_di_passaporto.'<br>';
                                 */
                           echo '
                           </td>';

                            $howMuchPaidText =  '<td rowspan="'.$pcount.'">';
                            $euroAmount = 0;
                            $summEur = 0;
                            $sss = '';

                            $sum_paid_this_month = 0;
                            foreach ($payments as $payment)
                            {
                                if ($payment->full_payment == 1)
                                {
                                    $howMuchPaidText .= ' ';
                                }
                                else
                                    $summEur += $payment->amountEuro;
                                if (!in_array($payment->id,$month_statistic_array)
                                    && (
                                            count($month_statistic_array) < 1 ||
                                            $payment->id > $month_statistic_array[count($month_statistic_array)-1]
                                    )
                                )
                                {
                                    if (!$partially_skip)
                                    {
                                        $total_sum_paid_this_month += $payment->amountEuro;
                                        $sss .= $payment->amountEuro.'<br/>';
                                    }
                                    $sum_paid_this_month += $payment->amountEuro;
                                    $howMuchPaidText .= '<!-- <b>ПОСЧИТАЛ mpID: '.$payment->id.'</b> -->';
                                }
                                if (!$partially_skip)
                                {
                                    $howMuchPaidText .=''.$payment->paymentID.'<div  data-uk-tooltip title="'.$payment->paymentID.'<br/>';

                                    $howMuchPaidText .= convertTimeCreated($payment->payTime).'<br/>';

                                    if ($payment->status ==1)
                                    {
                                        $howMuchPaidText .= ' ' .($payment->amountRub/100).' ₽ </span><br/>';
                                        $howMuchPaidText .= 'Курс: '.$payment->curs.'<br/>';
                                    }
                                    $howMuchPaidText .= '<span class="uk-num">'.$payment->amountEuro   .'</span> €</div> ';
                                    $howMuchPaidText .= '<hr/>';
                                }
                                $euroAmount += $payment->amountEuro;
                            }
                            if (!$partially_skip)
                                $howMuchPaidText .= '<br/>Всего: '.$euroAmount;
							
                            $howMuchPaidText .= '</td>';
							if ($payment->check_type == 1){
								$howMuchPaidText .= '<td rowspan="'.$pcount.'"><b>'.$sss.' «RT» '.'</b></td>';
							} else {$howMuchPaidText .= '<td rowspan="'.$pcount.'"><b>'.$sss.' «VR» '.'</b></td>';}
                            
							
                            if (isset($partners[0]))
                            {
                                $a1 = '';
                                $a2 = '';

                                if (!$partially_skip)
                                {
                                    $a1 = '<a href="#change_partner_summ" data-uk-modal onclick="'.
                                        'jQuery(\'#change_partner_summ [name=amount_rub]\').val('.round($partners[0]->amount_rub/100,2).');'.
                                        'jQuery(\'#change_partner_summ [name=relation_id]\').val('.$partners[0]->id.');'.
                                        'jQuery(\'#change_partner_summ [name=amount_euro]\').val('.round($partners[0]->amount_euro/100,2).');'.
                                        'jQuery(\'#change_partner_summ .partner\').html(\''.$partners[0]->pname.'\');'.
                                        'return false;">';
                                    $a2 = '</a>';
                                }

                                echo '<td><div  data-uk-tooltip title="'.$partners[0]->sname.'"  "> '.$partners[0]->pname.' </div></td>';
                                echo '<td>'.$a1.'<span class="uk-num">'.round($partners[0]->amount_euro/100,2).' € </span>'.$a2.'</td>';
                                echo '<td>'.$a1.'<span class="uk-num">'.round($partners[0]->amount_rub/100,2).' ₽ </span>'.$a2.'</td>';

                            }
                            else
                            {
                                echo '<td></td>';
                                echo '<td></td>';
                                echo '<td></td>';
                            }

                            echo '<td rowspan="'.$pcount.'">';
                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                {
                                    echo ($row->curs/100).'<hr/>';
                                }
                            echo $course;
                            echo '</td>';

                            echo '<td rowspan="'.$pcount.'">';
                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                    echo '<span class="uk-num">'.($row->full_summ/100).' €</span><hr/>';
                            echo ' '.$order->valute_amount.' € ';
                            echo '/ Изменить: <a href="#order_valute_amount" data-uk-modal onclick="jQuery(\'#changevaluteamount_order_id\').val('.$order->id.');jQuery(\'#changevaluteamount_valute_amount\').val('.$order->valute_amount.');"><span class="uk-num">'.$order->valute_amount.' €</span></a>';
                            echo '</td>';

                            //$acconto = ($order->valute_amount?round(($payments[count($payments)-1]->amountEuro/$order->valute_amount*100),2):0);
                            //$acconto = ($order->valute_amount?round(($euroAmount/$order->valute_amount*100),2):0);
                            $acconto = ($order->valute_amount?round(($sum_paid_this_month/$order->valute_amount*100),2):0);
                            echo '<td rowspan="'.$pcount.'">';
                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                    echo ''.($row->acconto/100).'%<hr/>';
                            echo $acconto.'%';
                            echo '</td>';
                            echo $howMuchPaidText;
							
                            echo '<td rowspan="'.$pcount.'">';

                            $paymentsForPartnersSumm = round($total_euro+$total_rub/$course,2);
                            $howMuchPaidNotThisMonth = 0;
                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                {
                                    if (!$partially_skip)
                                        echo '<span class="uk-num">'.($row->payments_for_partners_summ/100).' €</span><hr/>';
                                    $howMuchPaidNotThisMonth += $row->how_much_paid/100;
                                }

                            if (!$partially_skip)
                                echo '<span class="uk-num"> '.$paymentsForPartnersSumm.' € </span>';

                            echo '</td>';

                            echo '<td rowspan="'.$pcount.'">';

                            $full_payment_costoParziale = 0;
                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                {
                                    if (!$partially_skip)
                                        echo '<span class="uk-num">  '.($row->costo_parziale/100).' € </span><hr/>';
                                    $full_payment_costoParziale += $row->costo_parziale/100;
                                }


                            //var_dump($paymentsForPartnersSumm,$full_payment_costoParziale,$payments[count($payments)-1]->full_payment,$row->costo_parziale,count($month_statistic));
                            if ($payments[count($payments)-1]->full_payment==1 && count($month_statistic) > 0)
                            {
                                $full_payment_costoParziale = $row->costo_parziale/100;
                                $costoParziale = $paymentsForPartnersSumm-$full_payment_costoParziale;
                            }
                            else
                            {
                                $costoParziale = ($order->valute_amount?round($acconto*($total_euro+$total_rub/$course)/100,2):0);

                            }
                            $total_costo_paraliziale += $costoParziale;
                            if (!$partially_skip)
                            {
                                echo ' <span class="uk-num">'.$costoParziale.' € </span> ';
                                if ($payments[count($payments)-1]->full_payment==1)
                                {
                                    echo ' <br/><b> <span class="uk-num"> '.$full_payment_costoParziale.' € </span></b> ';
                                }
                            }

                            echo ' </td>';

                            echo '<td rowspan="'.$pcount.'">';
                            if (!$partially_skip)
                                echo ' <span class="uk-num">'.$costoParziale.' € </span> ';
                            echo ' </td>';

                            echo '<td rowspan="'.$pcount.'">';

                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                    echo '<span data-uk-tooltip title="'.($row->total_profit/100).' € "><hr></span><br>';
                            $total_profit = ($order->valute_amount-round($total_euro+$total_rub/$course,2));
                            if ($partially_skip)
                                $totale_guadagni += $total_profit;
                            echo '<span class="uk-num">'.$total_profit. ' € </span> ' ;

                            echo '</td>';

                            echo '<td rowspan="'.$pcount.'">';

                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                    echo '<span data-uk-tooltip title="'.($row->guadagno_parziale/100).' € "><hr></span><br>';
                            $guadagno_parziale = ($sum_paid_this_month-$costoParziale);
                            if (isset($_GET['1563863340']))
                                var_dump($euroAmount,$costoParziale,$howMuchPaidNotThisMonth,$sum_paid_this_month);
                            if (!$partially_skip)
                                echo ' <span class="uk-num">  '.$guadagno_parziale. ' € </span> ';

                            echo '</td>';

                            echo '<td rowspan="'.$pcount.'">';
                            if (count($month_statistic))
                                foreach ($month_statistic as $row)
                                    echo '<span data-uk-tooltip title="'.($row->profit).' % "><hr></span>
    ';
                            /*$paidThisMonth = $euroAmount-$howMuchPaidNotThisMonth;
                            if ($paidThisMonth != 0)
                                $profit = $guadagno_parziale/$paidThisMonth;
                            else
                                $profit = 0;*/
                            if ($sum_paid_this_month != 0)
                                $profit = $guadagno_parziale/$sum_paid_this_month;
                            else
                                $profit = 0;
                            if (!$partially_skip)
                                echo round(($profit*100),2).' %';
                            echo '</td>';

                            $payment = $payments[count($payments)-1];

                            $total_profit = ($order->valute_amount-round($total_euro+$total_rub/$course,2));
                            echo '<input type="hidden" name="data['.$payment->id.'][order_id]" value="'.$order->id.'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][manualpayment_id]" value="'.$payment->id.'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][full_summ]" value="'.($order->valute_amount*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][acconto]" value="'.($acconto*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][curs]" value="'.($course*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][how_much_paid]" value="'.($euroAmount*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][payments_for_partners_summ]" value="'.($paymentsForPartnersSumm*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][costo_parziale]" value="'.($costoParziale*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][total_profit]" value="'.($total_profit*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][guadagno_parziale]" value="'.($guadagno_parziale*100).'" />';
                            echo '<input type="hidden" name="data['.$payment->id.'][profit]" value="'.($profit*100).'" />';

                            echo '</tr>';
                            if ($pcount > 1)
                            {
                                foreach ($partners as $k=>$partner)
                                {
                                    if ($k>0)
                                    {
                                        $a1 = '';
                                        $a2 = '';

                                        if (!$partially_skip)
                                        {
                                            $a1 = '<a href="#change_partner_summ" data-uk-modal onclick="'.
                                                'jQuery(\'#change_partner_summ [name=amount_rub]\').val('.round($partner->amount_rub/100,2).');'.
                                                'jQuery(\'#change_partner_summ [name=relation_id]\').val('.$partner->id.');'.
                                                'jQuery(\'#change_partner_summ [name=amount_euro]\').val('.round($partner->amount_euro/100,2).');'.
                                                'jQuery(\'#change_partner_summ .partner\').html(\''.$partner->pname.'\');'.
                                                'return false;">';
                                            $a2 = '</a>';
                                        }

                                        echo '<tr>';

                                        echo '<td><div  data-uk-tooltip title="'.$partner->sname.'"  > '.$partner->pname.' </div></td>';
                                        echo '<td>'.$a1.'<span class="uk-num">'.round($partner->amount_euro/100,2).' €</span>'.$a2.'</td>';
                                        echo '<td>'.$a1.'<span class="uk-num"> '.round($partner->amount_rub/100,2).' ₽</span>'.$a2.'</td>';

                                        echo '</tr>';
                                    }
                                }
                                echo '<tr class="uk-panel-box">';
                                echo '<td>общее сумма</td>';
                                echo '<td><span class="uk-num"> '.round($total_euro,2).' €  </span></td>';
                                echo '<td> <span class="uk-num"> '.round($total_rub,2).' ₽ </span></td>';
                                echo '</tr>';
                            }
                        }
                    }
                    ?>
                </tbody>
            </table>

        <?php if (in_array(8,$user_groups)) { ?>
        <input type="submit" value="КНОПКА" />;
        <?php } ?>
    </form>;
</div>

<!-- This is the modal -->
<div id="order_valute_amount" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        Изменить полную сумму<br/>
        <form method="post" action="index.php?option=com_viaggio&task=changevaluteamount">
            <input type="hidden" name="order_id" id="changevaluteamount_order_id" value="">
            Полная сумма: <input name="valute_amount" id="changevaluteamount_valute_amount" value=""><br/>
            <input type="submit" value="Изменить">
        </form>
    </div>
</div>

<!-- This is the modal -->
<div id="change_partner_summ" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        Партнёр: <b class="partner"></b><br/>
        <form method="post" action="index.php?option=com_viaggio&task=changePartnerSumm">
            <input type="hidden" name="relation_id" value="">
            <input name="amount_rub" value=""> ₽<br/>
            <input name="amount_euro" value=""> €<br/>
            <input type="submit" value="Изменить">
        </form>
    </div>
</div>

<?php
function convertTimeCreated($timeCreated)
{
    $timeCreated = explode(' ',$timeCreated);
    $timeCreatedDate = explode('-',$timeCreated[0]);
    return $timeCreated[1].' '.$timeCreatedDate[2].'/'.$timeCreatedDate[1].'/'.$timeCreatedDate[0];
}
$forZP = ($total_sum_paid_this_month-$total_costo_paraliziale+$totale_guadagni)/10*$course+10000;
if ($forZP < 50000)
    $forZP = 50000;

$forZP2 = ($total_sum_paid_this_month-$total_costo_paraliziale+$totale_guadagni)/10*$course+10000;
if ($forZP2 < 50000)
    $forZP2 = 50000;
else
    $forZP2 = 50000 + 0.87*($forZP2-50000);
?>
<script>jQuery(document).ready(function(){
    jQuery('#summ_of_summs').html('<?php echo $total_sum_paid_this_month; ?>');
    jQuery('#total_costo_paraliziale').html('<?php echo $total_costo_paraliziale; ?>');
    jQuery('#totale_guadagni').html('<?php echo ($total_sum_paid_this_month-$total_costo_paraliziale+$totale_guadagni); ?>');
    jQuery('#profito').html('<?php echo (($total_sum_paid_this_month!=0)?(round(($total_sum_paid_this_month-$total_costo_paraliziale+$totale_guadagni)/$total_sum_paid_this_month*100,2)):0); ?>');
    jQuery('#zp').html('<?php echo $forZP; ?>');
    jQuery('#zp2').html('<?php echo $forZP2; ?>');
});</script>
<script type="text/javascript"   src="/images/css/2/js/uk-nu.js"></script>