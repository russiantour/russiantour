 
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>

 <script type="text/javascript"   src="/images/css/2/js/components/tooltip.js"></script>
<?php
$db = JFactory::getDbo();
$db->setQuery("SELECT * FROM #__viaggio_month_statistic ORDER BY id DESC");
$data = $db->loadObjectList();
?>
<div id="j-main-container" class="span10">
    <table border="1">
        <thead>
            <tr>
                <td>order_id</td>
                <td>manualpaymant_id</td>
                <td>full_summ</td>
                <td>acconto</td>
                <td>curs</td>
                <td>how_much_paid</td>
                <td>payments_for_partners_summ</td>
                <td>costo_parziale</td>
                <td>total_profit</td>
                <td>guadagno_parziale</td>
                <td>profit</td>
                <td>timeCreated</td>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($data as $row) {
                echo '<tr>';
                echo '<td>'.$row->order_id.'</td>';
                echo '<td>'.$row->manualpaymant_id.'</td>';
                echo '<td>'.($row->full_summ/100).'</td>';
                echo '<td>'.($row->acconto/100).'</td>';
                echo '<td>'.($row->curs/100).'</td>';
                echo '<td>'.($row->how_much_paid/100).'</td>';
                echo '<td>'.($row->payments_for_partners_summ/100).'</td>';
                echo '<td>'.($row->costo_parziale/100).'</td>';
                echo '<td>'.($row->total_profit/100).'</td>';
                echo '<td>'.($row->guadagno_parziale/100).'</td>';
                echo '<td>'.($row->profit/100).'</td>';
                echo '<td>'.($row->timeCreated).'</td>';
                echo '</tr>';
            } ?>
        </tbody>
    </table>
</div>