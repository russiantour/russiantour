<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');
?>

<script type="text/javascript">
    Joomla.submitbutton = function (task) {
        if (task == 'tripscategory.cancel') {
            Joomla.submitform(task, document.getElementById('tripscategory-form'));
        }
        else {

            if (task != 'tripscategory.cancel' && document.formvalidator.isValid(document.id('tripscategory-form'))) {

                Joomla.submitform(task, document.getElementById('tripscategory-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form
    action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>"
    method="post" enctype="multipart/form-data" name="adminForm" id="tripscategory-form" class="form-validate">

    <div class="form-horizontal">
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <?php echo $this->form->renderField('id'); ?>
                    <?php echo $this->form->renderField('title_rus'); ?>
                    <?php echo $this->form->renderField('title_eng'); ?>
                    <?php echo $this->form->renderField('title_ita'); ?>
                    <?php echo $this->form->renderField('title_esp'); ?>
                </fieldset>
            </div>
        </div>
        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
