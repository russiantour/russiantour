<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access
defined('_JEXEC') or die;

if ($tour_id = $_GET['tour_id'])
{
    if (count($this->items))
        echo '<b>Редактировать:</b><br/>';
    foreach ($this->items as $comment)
    {
        echo '<div id="comment_'.$comment->id.'">';
        echo '<input type=hidden name="comment_id" value="'.$comment->id.'" />';
        echo '<input type=hidden name="comment_tour_id" value="'.$comment->tour_id.'" />';
        echo 'author_name <input name="comment_author_name" value="'.$comment->author_name.'" />';
        echo '<br/>';
        echo 'author_email <input name="comment_author_email" value="'.$comment->author_email.'" />';
        echo '<br/>';
        echo JHtml::_('calendar', $comment->date, 'comment_date', 'comment_date', '%Y-%m-%d');
        echo '<br/>';
        echo 'text <textarea name="comment_text">'.$comment->text.'</textarea>';
        echo '<br/>';
        echo '<div onclick="send_comment(\'comment_'.$comment->id.'\')">Отправить</div>';
        echo '<hr/>';
        echo '</div>';
    }
    echo '<b>Добавить:</b><br/>';
    echo '<div id="comment_0">';
    echo '<input type=hidden name="comment_id" value="0" />';
    echo '<input type=hidden name="comment_tour_id" value="'.$tour_id.'" />';
    echo '<input name="comment_author_name" value="" />';
    echo '<br/>';
    echo '<input name="comment_author_email" value="" />';
    echo '<br/>';
    echo JHtml::_('calendar', date('Y-m-d'), 'comment_date', 'comment_date', '%Y-%m-%d');
    echo '<br/>';
    echo '<textarea name="comment_text"></textarea>';
    echo '<br/>';
    echo '<div onclick="send_comment(\'comment_0\')">Отправить</div>';
    echo '</div>';
}
else
    echo 'Error: tour_id is required';
exit;