<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/viaggio.css');

$user	= JFactory::getUser();
$userId	= $user->get('id');
$listOrder	= $this->state->get('list.ordering');
$listDirn	= $this->state->get('list.direction');
$canOrder	= $user->authorise('core.edit.state', 'com_viaggio');
$saveOrder	= $listOrder == 'a.ordering';
if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_viaggio&task=manualpayments.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'hotelList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
 
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>

 <script type="text/javascript"   src="/images/css/2/js/components/tooltip.js"></script>
<script type="text/javascript">
	Joomla.orderTable = function() {
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>') {
			dirn = 'asc';
		} else {
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<?php
//Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar)) {
    $this->sidebar .= $this->extra_sidebar;
}
?>

<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=manualpayments'); ?>" method="post" name="adminForm" id="adminForm">
<?php if(!empty($this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
        <div class="filter-select">
            Payment ID:<br/>
            <input name="filter_bypaymentid" value="<?php echo (isset($_POST['filter_bypaymentid'])?$_POST['filter_bypaymentid']:''); ?>">
        </div>
        <div class="filter-select">
            email:<br/>
            <input name="filter_byemail" value="<?php echo (isset($_POST['filter_byemail'])?$_POST['filter_byemail']:''); ?>">
        </div>
        <div class="filter-select">
            FIO:<br/>
            <input name="filter_byfio" value="<?php echo (isset($_POST['filter_byfio'])?$_POST['filter_byfio']:''); ?>">
        </div>
        <div class="filter-select">
            <input type="submit" value="Фильтровать">
        </div>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
    
		<div id="filter-bar" class="btn-toolbar">
			<div class="btn-group pull-right hidden-phone">
				<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC');?></label>
				<?php echo $this->pagination->getLimitBox(); ?>
			</div>
		</div>         
 

		 	 
		<table class="uk-table table-striped" id="hotelList">
			<thead>
				<tr>
					<th width="1%" class="nowrap center">
						<?php echo JHtml::_('grid.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
					</th>
					<th width="1%" class="nowrap center">
						<?php //echo JText::_('COM_TOURISTINVITE_PAYMENTS_STATUS');?>
                        data\id\user
                    </th>
					<th width="1%" class="nowrap center">
                        FIO telefon
					</th>
                    <th width="1%" class="nowrap center">
                        описание
                    </th>
                    <th width="1%" class="nowrap center">
                        email
                    </th>
                    <th width="1%" class="nowrap center">
                        деньги
<?php echo JText::_('COM_TOURISTINVITE_LINK');?>
                    </th>
                    <th width="1%" class="nowrap center">
                        status
                    </th>
				</tr>
			</thead>
			<tfoot>
                <?php 
                if(isset($this->items[0])){
                    $colspan = count(get_object_vars($this->items[0]));
                }
                else{
                    $colspan = 10;
                }
            ?>
			<tr>
				<td colspan="<?php echo $colspan ?>">
					<?php echo $this->pagination->getListFooter(); ?>
				</td>
			</tr>
			</tfoot>
			<tbody>
			<?php foreach ($this->items as $i => $item) :
                $date = explode(' ',$item->timeCreated);
                $date = explode('-',$date[0]);
                $str = array(
                    'paymentID='.$item->paymentID,
                    'mp_d='.$date[2],
                    'mp_m='.$date[1],
                    'mp_y='.$date[0],
                    'order_id='.$item->order_id,
                    'amountEur='.$item->amountEuro
                );

                $db = JFactory::getDbo();
                $db->setQuery("SELECT * FROM #__viaggio_manualpayments_details WHERE payment_id = '".$item->paymentID."'");
                $details = $db->loadObjectList();
                $payTime = strtotime($item->timeCreatedLinl);

                $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
                $db=JFactory::getDBO();
                $db->setQuery($query);
                $curTime = $db->loadAssoc();
                $curTime = strtotime($curTime['curTime']);
                if ($item->userId)
                    $rowUser = JFactory::getUser($item->userId);
				$ordering   = ($listOrder == 'a.ordering');
                $canCreate	= $user->authorise('core.create',		'com_viaggio');
                $canEdit	= $user->authorise('core.edit',			'com_viaggio');
                $canCheckin	= $user->authorise('core.manage',		'com_viaggio');
                $canChange	= $user->authorise('core.edit.state',	'com_viaggio');
				?>
				<tr class="row<?php echo $i % 2; 
				
				?>">
 
                <?php if (isset($this->items[0]->id)): ?>
					<td class="center">
						<a href="/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&id=<?php echo (int) $item->id; ?>"><?php echo (int) $item->id; ?></a>
                        <br/><br/> 
						 <a href="?option=com_viaggio&task=hideManualPayment&id=<?php echo (int) $item->id; ?>">Удалить</a>
                        <br/><br/>
						<?php
						                            $user_groups = JAccess::getGroupsByUser($user->get('id'));
                            if (in_array(8,$user_groups))
                            {
                               
                                    
echo '<a href="/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&id='.$item->id.'"> edit
                        </a> </b> <br><a href="?option=com_viaggio&task=hideManualPayment&id='.$item->id.'">
                            Удалить
                        </a> </b><br><b>
						<a href="#manualpayment_summ" data-uk-modal onclick="jQuery(\'#changemanualpaymentamount_manualpayment_id\').val('.$item->id.');jQuery(\'#changemanualpaymentamount_manualpayment_eur\').val('.$item->amountEuro.');jQuery(\'#changemanualpaymentamount_comissionEuro\').val('.$item->comissionEuro.');jQuery(\'#changemanualpaymentamount_manualpayment_rub\').val('.($item->amountRub/100).');jQuery(\'#changemanualpaymentamount_manualpayment_curs\').val('.$item->curs.');"><span class="uk-num">Изменить</span></a></b>
						
						';
                             
                            }
						echo ' <a href="?option=com_viaggio&task=hideManualPayment&id='.$item->id.'">
                          .
                        </a> </b>  '
						?>
 
					</td>
					<td class="center">
 <a  target="_blank" href="index.php?option=com_viaggio&view=order&layout=edit&id=<?php echo $item->order_id; ?>"><?php echo $item->order_id; ?></a><br>
 
                        Дата создания <br/>
						<?php echo $item->timeCreated; ?><br/><br/>
                        <?php if ($item->userId) : ?>
                        Создал <br/>
                        <?php echo $rowUser->get('username'); ?><br/><br/>
                        id <?php echo $item->paymentID.'/'.$rowUser->get('id'); ?>
                        <?php endif; ?>
					</td>
					<td class="center">
						<?php echo $item->fio; ?><br/>
                        <?php echo $item->telefon; ?>
					</td>
                    <td class="center">
                        <?php echo $item->description; ?>		 
                        <?php echo $item->pole1; ?>		 

                        <?php
                        foreach ($details as $row) {
                            if ($row->field_value != '')
                                echo /*$row->field_group_name.': '.$row->field_name.'['.$row->field_group.'] = '.*/$row->field_value.'<br/>';
                        }
                        ?>
                    </td>
                    <td class="center">
                        <?php echo $item->email; ?>
                        <?php if ($item->status == 0 && $payTime>0 && $curTime < $payTime+3600*24) : ?>
                        <a onclick="return confirm('Вы хотите отправить ссылку?');" href="?option=com_viaggio&view=manualpayments&task=sendLink&id=<?php echo $item->id; ?>">
                            (Отправить ссылку)
                        </a>

                        <?php endif; ?>
                    </td>


                    <td class="center">
                        Amount <b><?php echo $item->amountEuro; ?> €</b>  Tax <b><?php echo $item->comissionEuro; ?> €</b><br>
					   <?php
                        if ($item->curs) {
                          echo ($item->amountRub/100).'<br/>';
							
                            echo 'курс '.$item->curs.' от '.$item->payTime.'<br/>';
                        }
                        ?>
                        <?php
                        if ($item->status == 0 && $item->payment_initiated != 1)
                                  echo '<a href="#manualpayment_summ" data-uk-modal onclick="jQuery(\'#changemanualpaymentamount_manualpayment_id\').val('.$item->id.');jQuery(\'#changemanualpaymentamount_manualpayment_eur\').val('.$item->amountEuro.');jQuery(\'#changemanualpaymentamount_manualpayment_rub\').val('.($item->amountRub/100).');jQuery(\'#changemanualpaymentamount_manualpayment_curs\').val('.$item->curs.');"><span class="uk-num">Изменить</span></a>';
                        
                        ?>
                    </td>
				
                    <td class="center">
                        <?php
                        if ($item->status == 0 && $payTime>0  ){
                            echo ' '; ?>
 
 
<?php echo '                        
							<div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button uk-button-primary">Скачать Invoce <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown">';
?>
                                        <?php if (in_array($item->bank,['payler','tinkoff']) && in_array($item->check_type,[1,2])) { ?>
                                            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID=<?php echo $item->paymentID; ?>">Скачать</a></li>
                                        <?php } else { ?>
                                            <?php if (!$item->check_type || $item->check_type == 1) { ?>
                                            <li class="uk-nav-header">ООО "Русский тур"</li>
                                            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID=<?php echo $item->paymentID; ?>">Софкомбанк</a></li>
                                            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID=<?php echo $item->paymentID; ?>">Тинькофф</a></li>
                                            <li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare<?php echo $item->order_id; ?>.pdf">Edit pdf</a></li>
                                            <?php } ?>
                                            <?php if (!$item->check_type || $item->check_type == 2) { ?>
                                            <li class="uk-nav-header">ООО "Висто Руссиа"</li>
                                            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID=<?php echo $item->paymentID; ?>">СовкомБанк</a></li>
                                            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID=<?php echo $item->paymentID; ?>">Тинькофф</a></li>
                                            <li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare<?php echo $item->order_id; ?>.pdf">Edit pdf</a></li>
                                            <?php } ?>
                                        <?php } ?> 
<?php echo '                        </ul>
                                </div>
                            </div>
							
							
							<br>
</div>'.

                                '<!--<a href="?option=com_viaggio&view=manualpayments&task=sendLink&id='.$item->id.'">Пересоздать Заказ</a><br/>-->'.
                                '<!-- <a href="?option=com_viaggio&view=manualpayments&task=cancelmanual&id='.$item->id.'">Отменить Заказ</a><br/>-->'.
                                'от '.$item->timeCreatedLinl.'<br/>'.
                                'до '.date('Y-m-d H:i',strtotime($item->timeCreatedLinl)+3600*24);

                            echo ' <br>
                                    <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                    <button class="uk-button uk-button-primary">
                                        Оплатили по банку 
                                        <i class="uk-icon-caret-down"></i>
                                    </button>
                                    <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                        <ul class="uk-nav uk-nav-dropdown">'; ?>
                                            <?php if (!$item->check_type || $item->check_type == 1) { ?>
                                                <li class="uk-nav-header">ООО "Русский тур"</li>
                                                    <?php /* if (!$item->bank || $item->bank == 'payler') {*/  ?>
                                                <li>
                                                    <button data-uk-modal="{target:'#payedPartial-modal'}" onclick="jQuery('#payedPartial_order_id').val(<?php echo $item->order_id; ?>); jQuery('#payedPartial_bank').val('СовкомБанк'); jQuery('#payedPartial_check_type').val(1); jQuery('#payedPartial_payment_id').val(<?php echo $item->id; ?>); jQuery('#payedPartial_summ-in-euro').val(<?php echo $item->amountEuro; ?>);return false;">СовкомБанк</button>
                                                </li>
                                                    <?php /* } */ ?>
                                                    <?php /* if (!$item->bank || $item->bank == 'tinkoff') {*/ ?>
                                                <li>
                                                    <button data-uk-modal="{target:'#payedPartial-modal'}" onclick="jQuery('#payedPartial_order_id').val(<?php echo $item->order_id; ?>); jQuery('#payedPartial_bank').val('tinkoff'); jQuery('#payedPartial_check_type').val(1); jQuery('#payedPartial_payment_id').val(<?php echo $item->id; ?>); jQuery('#payedPartial_summ-in-euro').val(<?php echo $item->amountEuro; ?>);return false;">Тинькофф</button>
                                                </li>
                                                    <?php /* }*/ ?>

                                                <?php if (!$item->bank || $item->bank == 'best2pay') { ?>
                                                <!--<li><a onclick="return confirm(\'Точно вы хотите оплатить?\')" href="?option=com_viaggio&task=payedPartial&order_id=<?php echo $item->order_id; ?>&bank=best2pay&check_type=1&payment_id=<?php echo $item->id; ?>">СовкомБанк</a></li>-->
                                                <?php } ?>
                                            <?php } ?>
                                            <?php if (!$item->check_type || $item->check_type == 2) { ?>
                                                <li class="uk-nav-header">ООО "Висто Руссиа"</li>
                                                 <?php /* if (!$item->bank || $item->bank == 'payler') {*/ ?>
                                                <li>
                                                    <button data-uk-modal="{target:'#payedPartial-modal'}" onclick="jQuery('#payedPartial_order_id').val(<?php echo $item->order_id; ?>); jQuery('#payedPartial_bank').val('СовкомБанк'); jQuery('#payedPartial_check_type').val(2); jQuery('#payedPartial_payment_id').val(<?php echo $item->id; ?>); jQuery('#payedPartial_summ-in-euro').val(<?php echo $item->amountEuro; ?>);return false;">СовкомБанк</button>
                                                </li>
                                                    <?php /* }*/ ?>

                                                    <?php /* if (!$item->bank || $item->bank == 'tinkoff') {*/ ?>
                                                <li>
                                                    <button data-uk-modal="{target:'#payedPartial-modal'}" onclick="jQuery('#payedPartial_order_id').val(<?php echo $item->order_id; ?>); jQuery('#payedPartial_bank').val('tinkoff'); jQuery('#payedPartial_check_type').val(2); jQuery('#payedPartial_payment_id').val(<?php echo $item->id; ?>); jQuery('#payedPartial_summ-in-euro').val(<?php echo $item->amountEuro; ?>);return false;">Тинькофф</button>
                                                </li>
                                                    <?php /*} */?>
                                                <?php if (!$item->bank || $item->bank == 'best2pay') { ?>
                                               <!-- <li><a onclick="return confirm(\'Точно вы хотите оплатить?\')" href="?option=com_viaggio&task=payedPartial&order_id=<?php echo $item->order_id; ?>&bank=best2pay&check_type=2&payment_id=<?php echo $item->id; ?>">СовкомБанк</a></li>-->
                                                <?php } ?>
                                            <?php } ?>
											
<?php echo                              '</ul>
                                    </div>
                                </div>
</div>

   
                                             ';
                        }
                        elseif ($item->status == 0 && $payTime>0 && $curTime >= $payTime+360000*24){
                            echo '<a href="?option=com_viaggio&view=manualpayments&task=sendLink&id='.$item->id.'">Пересоздать ссылку</a><br/>'.
                                '<a href="?option=com_viaggio&view=manualpayments&task=deletemanual&id='.$item->id.'">Удалить заявку.</a><br/>'.
                                'закончился<br/>'.
                                date('Y-m-d H:i',strtotime($item->timeCreatedLinl)+360000*24);
                        }
                        elseif ($item->status == 1){
                            echo 'Оплатил по ссылке!!!<br>';
                          switch ($item->check_type) {
                                case 1:
                                    echo 'ООО Русский тур<br>
									
							 
									<div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button uk-button-primary">Скачать Invoce <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown"> 
                                         
                                        <li class="uk-nav-header">ООО "Русский тур"</li>
            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID='.$item->paymentID.'">Софкомбанк</a></li>
                    <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID='.$item->paymentID.'">Тинькофф</a></li>
										<li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare'.$item->order_id.'.pdf">Edit pdf</a></li>						
                                       
                                         
                           
                         </ul>
                                </div>
</div>	<br>
									
									
									';
									break;
                                case 2:
									 echo 'ООО Висто Руссиа<br>
									 <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button uk-button-primary">Скачать Invoce <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown"> 
                                         
                                        <li class="uk-nav-header"ООО Висто Руссиа</li>
            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID='.$item->paymentID.'">Софкомбанк</a></li>
                    <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID='.$item->paymentID.'">Тинькофф</a></li>										
                                       
                          								<li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare'.$item->order_id.'.pdf">Edit pdf</a></li>	               
                           
                         </ul>
                                </div>
</div>
									 
									 
									 ';
									 break;
                                default:
                                    echo $row->status;
                            }
						}
                        elseif ($item->status == 2){
 
 
 
                            switch ($item->check_type) {
                                case 1:
                                    echo 'ООО Русский тур<br>
									
							 
									<div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button uk-button-primary">Скачать Invoce <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown"> 
                                         
                                        <li class="uk-nav-header">ООО "Русский тур"</li>
            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareR&'. implode('&',$str).'">Софкомбанк</a></li>
                    <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareRT&'. implode('&',$str).' ">Тинькофф</a></li>										
                                       
                           	<li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare'.$item->order_id.'.pdf">Edit pdf</a></li>	                   
                           
                         </ul>
                                </div>
</div>	<br>
									';
									break;
                                case 2:
									 echo 'ООО Висто Руссиа<br>
									 <div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button uk-button-primary">Скачать Invoce <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown"> 
                                         
                                        <li class="uk-nav-header"ООО Висто Руссиа</li>
            <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareV&'. implode('&',$str).'">Софкомбанк</a></li>
                    <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareVT&'. implode('&',$str).' ">Тинькофф</a></li>										
                                   	<li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare'.$item->order_id.'.pdf">Edit pdf</a></li>	         
                                         
                           
                         </ul>
                                </div>
</div>	<br>
									 
									 ';
									 break;
                                default:
                                    echo $row->status;
                            }
 					 
							echo 'Оплатил по банку							 
							<br/>';
                            $user_groups = JAccess::getGroupsByUser($user->get('id'));
                            if (in_array(8,$user_groups))
                            {
                                if ($item->check_sended)
                                    echo '<b>Чек отправлен.<br>Сумма: '.($item->check_amount/100).'</b>';
                                else
                                    echo 'Сумма: <input id="amount'.$item->id.'" type="number"   name="amount"><br/><a href="#" onclick="makeCheck(\''.$item->id.'\')">Создать чек</a>';
                            }
                        }

                        ?>
				 
 
							 <?php /*  <div class="uk-inline">
	<div uk-dropdown="mode: click">
    <ul class="uk-nav uk-dropdown-nav">
        <li class="uk-active"> <li class="uk-nav-header">СовкомБанк</li>
        <li><a  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM&<?php echo implode('&',$str); ?>">Скачать Invoce</a></li>
        <li><a  onclick="return confirm('Вы хотите отправить ссылку?');"  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM&sendmail=sendEmailPagare&<?php echo implode('&',$str); ?>">Отправить Invoce</a></li>
	 <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM2&<?php echo implode('&',$str); ?>">Скачать Invoce Client</a></li>
        <li><a  onclick="return confirm('Вы хотите отправить ссылку?');"  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareM2&sendmail=sendEmailPagare&<?php echo implode('&',$str); ?>">Отправить Invoce Client</a></li>
       	
		<li class="uk-nav-header">Тинькофф</li>
         <li><a  target="_blank"href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareT&<?php echo implode('&',$str); ?>">Скачать Invoce</a></li>
       <li><a  target="_blank" href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareT&sendmail=sendEmailPagare&<?php echo implode('&',$str); ?>">Оправить Invoce</a></li>  

	   </ul>
</div>
    
</div>*/?>

 

 
 <?php if (in_array($item->payment_initiated,[0,1])) { ?>
     <br>
     <?php if (in_array($item->bank,['tinkoff','payler']) && in_array($item->check_type,[1,2])) { ?>
         <a  target="_blank"   href="https://www.russiantour.com/ita/?option=com_viaggio&view=step3bonifico&payment_id=<?=$item->paymentID;?>">Accetto bonifico</a>
     <?php }
 } ?>
					 <?php if ($item->payment_initiated == 2) { ?>

	<br>
 <b>Ссылка нажата 
 </b><br>
 <a  target="_blank"   href="https://www.russiantour.com/ita/?option=com_viaggio&view=step3bonifico&payment_id=<?=$item->paymentID;?>">Accetto bonifico</a>                          
                                       
      
												<?php } ?>
																	 <?php if ($item->status == 3) { ?>

	<br>
 <b>оплачено по ALTRO  </b>
                          
                                       
      
												<?php } ?>
 
<br>

					
			
                    </td>
                <?php endif; ?>
				</tr>
				<?php endforeach; ?>
			</tbody>
		</table>



		<input type="hidden" name="task" value="" />
		<input type="hidden" name="boxchecked" value="0" />
		<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
		<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
</form>
<!-- This is the modal -->
<div id="manualpayment_summ" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        Изменить сумму<br/>
        <form method="post" action="index.php?option=com_viaggio&task=changemanualpaymentamount">
            <input type="hidden" name="manualpayment_id" id="changemanualpaymentamount_manualpayment_id" value="">
            Сумма в евро: <input name="manualpayment_eur" id="changemanualpaymentamount_manualpayment_eur" value=""><br/>
            Комиссия: <input name="comissionEuro" id="changemanualpaymentamount_comissionEuro" value=""><br/>
            <input type="hidden" name="manualpayment_rub" id="changemanualpaymentamount_manualpayment_rub" value=""><br/>
            <input type="hidden" name="manualpayment_curs" id="changemanualpaymentamount_manualpayment_curs" value=""><br/>
            <input type="submit" value="Подтвердить">
        </form>
    </div>
</div>
<script>
    function makeCheck(id){
        window.location.href='?option=com_viaggio&task=makeCheck&payment_id='+id+'&amount='+jQuery('#amount'+id).val();
    }
</script>
<div id="payedPartial-modal" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <form method="get" action="">
            Сумма в евро: <input id="payedPartial_summ-in-euro" name="summ_in_euro">
            <br/>
            <input type="hidden" name="payment_id" id="payedPartial_payment_id">
            <input type="hidden" name="order_id" id="payedPartial_order_id">
            <input type="hidden" name="bank" id="payedPartial_bank">
            <input type="hidden" name="check_type" id="payedPartial_check_type">
            <input type="hidden" name="option" value="com_viaggio">
            <input type="hidden" name="task" value="payedPartial">
            <input type="submit" value="Изменить">
        </form>
    </div>
</div>