<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Viaggio.
 *
 * @since  1.6
 */
class ViaggioViewTripscategorys extends JViewLegacy
{
    protected $items;

    protected $pagination;

    protected $state;

    /**
     * Display the view
     *
     * @param   string  $tpl  Template name
     *
     * @return void
     *
     * @throws Exception
     */
    public function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            throw new Exception(implode("\n", $errors));
        }

        ViaggioHelpersViaggio::addSubmenu('tripscategorys');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @return void
     *
     * @since    1.6
     */
    protected function addToolbar()
    {
        $state = $this->get('State');
        $canDo = ViaggioHelpersViaggio::getActions();

        JToolBarHelper::title(JText::_('COM_VIAGGIO_TITLE_TRIPSCATEGORYS'));

        // Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/tripscategory';

        if (file_exists($formPath))
        {
            if ($canDo->get('core.create'))
            {
                JToolBarHelper::addNew('tripscategory.add', 'JTOOLBAR_NEW');
            }

            if ($canDo->get('core.edit') && isset($this->items[0]))
            {
                JToolBarHelper::editList('tripscategory.edit', 'JTOOLBAR_EDIT');
            }
        }

        if ($canDo->get('core.admin'))
        {
            JToolBarHelper::preferences('com_viaggio');
        }

        if ($canDo->get('core.delete'))
        {
            JToolBarHelper::deleteList('', 'tripscategorys.trash', 'JTOOLBAR_TRASH');
            JToolBarHelper::divider();
        }

        // Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_viaggio&view=tripscategorys');

        $this->extra_sidebar = '';
    }

    protected function getSortFields()
    {
        return array(
            'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
        );
    }
}
