<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Viaggio.
 *
 * @since  1.6
 */
class ViaggioViewClients extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		ViaggioHelpersViaggio::addSubmenu('clients');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = ViaggioHelpersViaggio::getActions();

		JToolBarHelper::title(JText::_('COM_VIAGGIO_TITLE_CLIENTS'), 'clients.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/client';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('client.add', 'JTOOLBAR_NEW');
				/*
				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('clients.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
				*/
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('client.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('clients.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('clients.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'clients.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('clients.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('clients.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'clients.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('clients.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_viaggio');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_viaggio&view=clients');

		$this->extra_sidebar = '';
		//Filter for the field sex
		$select_label = JText::sprintf('COM_VIAGGIO_FILTER_SELECT_LABEL', 'Sex');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "m";
		$options[0]->text = "Maschile";
		$options[1] = new stdClass();
		$options[1]->value = "f";
		$options[1]->text = "Femminile";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_sex',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.sex'), true)
		);

			//Filter for the field birthday
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_birthday">'. JText::sprintf('COM_VIAGGIO_FROM_FILTER', 'Birthday') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.birthday.from'), 'filter_from_birthday', 'filter_from_birthday', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_birthday">'. JText::sprintf('COM_VIAGGIO_TO_FILTER', 'Birthday') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.birthday.to'), 'filter_to_birthday', 'filter_to_birthday', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

		//Filter for the field nazionalita
		$select_label = JText::sprintf('COM_VIAGGIO_FILTER_SELECT_LABEL', 'Nazionalita');
		$options = array();
		$options[0] = new stdClass();
		$options[0]->value = "italiani";
		$options[0]->text = "italiani";
		JHtmlSidebar::addFilter(
			$select_label,
			'filter_nazionalita',
			JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.nazionalita'), true)
		);

	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`created_by`' => JText::_('COM_VIAGGIO_CLIENTS_CREATED_BY'),
			'a.`tour_id`' => JText::_('COM_VIAGGIO_CLIENTS_TOUR_ID'),
			'a.`cognome`' => JText::_('COM_VIAGGIO_CLIENTS_COGNOME'),
			'a.`nome`' => JText::_('COM_VIAGGIO_CLIENTS_NOME'),
			'a.`sex`' => JText::_('COM_VIAGGIO_CLIENTS_SEX'),
			'a.`birthday`' => JText::_('COM_VIAGGIO_CLIENTS_BIRTHDAY'),
			'a.`nazionalita`' => JText::_('COM_VIAGGIO_CLIENTS_NAZIONALITA'),
			'a.`telephone`' => JText::_('COM_VIAGGIO_CLIENTS_TELEPHONE'),
			'a.`email`' => JText::_('COM_VIAGGIO_CLIENTS_EMAIL'),
			'a.`order_id`' => JText::_('COM_VIAGGIO_CLIENTS_ORDER_ID'),
		);
	}
}
