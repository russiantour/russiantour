<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'services', JText::_('COM_VIAGGIO_TITLE_SERVICES', true)); ?>
    <div class="row-fluid ">
        <div class="span10 form-horizontal">
            <fieldset class="adminform">
                <!-- УСЛУГИ -->

                    <div class="uk-margin-small">
                        <div class="service" blockid=0></div>
                        Добавить сервис:
                        <select id="servicesList">
                            <?php
                            foreach ($servicesList as $oneService)
                            {
                                echo '<option value="'.$oneService->id.'">'.$oneService->name_rus.'</option>';
                            }
                            ?>
                        </select>
                        <button onclick="addServiceRow();return false;">Добавить</button>
                        <div class="control-group servicetoclone" blockid="" style="display: none">
                            <div class="control-label"></div>
                            <div class="controls">
                                Поставщик:
                                <?php
                                foreach ($partners_of_services as $key=>$val)
                                {
                                    ?>
                                    <select nametemplate="service[<?php echo $key; ?>][blockid][partner_id]">
                                        <?php
                                        foreach ($val as $partner)
                                        {
                                            echo "<option value='".$partner->id."'>".$partner->name_rus."</option>";
                                        }
                                        ?>
                                    </select>
                                    <?php
                                }
                                ?>
                                
                                Цена в евро: <input nametemplate="service[serviceid][blockid][amount_euro]" class="amount_euro">
								Цена в рублях: <input nametemplate="service[serviceid][blockid][amount_rub]" class="amount_rub">
								
                            </div>
                            <button onclick="jQuery(this).parent().remove()">Удалить строку</button>
                        </div>
 
                </div>
            </fieldset>
        </div>
    </div>
    <script>
        function addServiceRow()
        {
            if (jQuery('.service').length > 0)
                var lastBlockId = parseInt(jQuery('.service:last').attr('blockid'));
            else
                var lastBlockId = 0;
            var serviceToAddId = jQuery('#servicesList').val();
            var serviceToAddName = jQuery('#servicesList option[value="'+serviceToAddId+'"]').text();
            jQuery('.service:last').after(jQuery('.servicetoclone').clone());
            jQuery('.servicetoclone:first').addClass('service');
            jQuery('.servicetoclone:first').removeClass('servicetoclone');
            jQuery('.service:last .control-label').html(serviceToAddName);
            jQuery('.service:last [nametemplate*="service['+serviceToAddId+']"]').attr('name','service['+serviceToAddId+']['+(lastBlockId+1)+'][partner_id]');
            jQuery('.service:last [nametemplate*="service['+serviceToAddId+']"]').removeAttr('nametemplate');
            jQuery('.service:last select[nametemplate]').next().remove();
            jQuery('.service:last select[nametemplate]').remove();
            jQuery('.service:last select[name]').next().remove();
            jQuery('.service:last select[name]').chosen();
            jQuery('.service:last [nametemplate*=serviceid]').each(function(){
                var nametemplate = jQuery(this).attr('nametemplate').replace('serviceid',serviceToAddId).replace('blockid',lastBlockId+1);
                jQuery(this).attr('name',nametemplate);
            });
            jQuery('.service:last').attr('blockid',lastBlockId+1);
            jQuery('.service:last').show();

            jQuery('.amount_rub').off('change');
            jQuery('.amount_rub').on('change',function () {
                var summRub = 0;
                jQuery('.amount_rub').each(function () {
                    var amount = parseFloat(jQuery(this).val().replace(',','.'));
                    if (amount)
                        summRub += amount;
                });
                jQuery('#summRub').html(summRub);
            });

            jQuery('.amount_euro').off('change');
            jQuery('.amount_euro').on('change',function () {
                var summEur = 0;
                jQuery('.amount_euro').each(function () {
                    var amount = parseFloat(jQuery(this).val().replace(',','.'));
                    if (amount)
                        summEur += amount;
                });
                jQuery('#summEur').html(summEur);
            });
        }
    </script>
<?php echo JHtml::_('bootstrap.endTab'); ?>