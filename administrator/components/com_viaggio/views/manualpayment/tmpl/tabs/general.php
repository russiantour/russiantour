<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<?php
$user_groups = JAccess::getGroupsByUser($user->get('id'));
if (in_array(8,$user_groups))
    echo  'aaaaa1' ;

else
    echo 'aaaa2';
?>
<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', 'Partial payment'); ?>
<div class="row-fluid">
    <div class="span10 form-horizontal">
        <div class="span5  ">
            <fieldset class="adminform">
                <input type="hidden" name="jform[id]" value="<?php echo $itemId; ?>" />
                <div class="trip_id">
                    <div class="control-group">
						<label class="uk-label uk-width-medium-1-2">Авиа<br><input id="order_avia_checkbox" type="checkbox"></label>
						<label class="uk-label uk-width-medium-1-2">РЖД<br><input id="order_rail_checkbox" type="checkbox"></label>
                        <label class="control-label">Категория тура</label>
                        <div class="controls">
                            <select id="parent-cat">
                                <option value=0> --- Выберите категорию --- </option>
                                <?php
                                $parent_id = false;
                                foreach ($trips_categorys as $cat)
                                {
                                    if ($this->item->trip && $this->item->trip->cat_id && $this->item->trip->cat_id == $cat->id)
                                        $parent_id = $cat->parent_id;
                                }
                                foreach ($trips_categorys as $cat)
                                {
                                    if ($cat->parent_id)
                                        continue;
                                    echo '<option value="'.$cat->id.'" '.(($this->item->trip && $parent_id == $cat->id)?'selected':'').'>'.$cat->title_ita.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="controls tour-cat-parent" <?php if (!$this->item->trip || !$this->item->trip->cat_id) {echo 'style="display: none;"';} ?>>
                            <select id="tour-cat">
                                <option value=0 class="const"> --- Выберите сабкатегорию --- </option>
                                <?php
                                foreach ($trips_categorys as $cat)
                                {
                                    if (!$cat->parent_id)
                                        continue;
                                    echo '<option '.((!$this->item->trip || !$this->item->trip->cat_id || $parent_id != $cat->parent_id)?'style="display: none;"':'').' parent_id = "'.$cat->parent_id.'" value="'.$cat->id.'" '.(($this->item->trip && $this->item->trip->cat_id == $cat->id)?'selected':'').'>'.$cat->title_ita.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group choose_tour" <?php if (!$this->item->trip) { ?>style="display: none"<?php } ?>>
                        <label class="control-label">Тур</label>
                        <div class="controls">
                            <select id="jform_trip_id" name="jform[trip_id]">
                                <option value="" class="const"> --- Выберите тур --- </option>
                                <?php
                                if (isset($_GET['test']))
                                {
                                    var_dump($this->item->trip,$this->item->trip->cat_id);
                                    exit;
                                }
                                if ($this->item->trip) {
                                    if ($this->item->trip->cat_id)
                                        $cat_id = 'cat_id = '.$this->item->trip->cat_id;
                                    else
                                        $cat_id = 'cat_id is null';
                                    $query = $db->getQuery(true);
                                    $query->select('*');
                                    $query->from('#__viaggio_trips');
                                    $query->where('state is null or state <> -2');
                                    $query->where($cat_id);
                                    $query->order('ordering ASC');
                                    $db->setQuery($query);
                                    $trips = $db->loadObjectList();
                                    foreach ($trips as $trip) {
                                        echo '<option value="'.$trip->id.'" datefrom="'.$trip->date_from.'" dateto="'.$trip->date_to.'" '.(($this->item->trip_id == $trip->id)?'selected':'').'>'.$trip->name_ita.'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- HOTELS EDIT BEGIN -->
                <?php
                //same in order and trip edit
                $db    = JFactory::getDbo();

                $query = $db->getQuery(true);
                $query->select('*');
                $query->from('#__viaggio_city');
                $query->order('name_rus asc');
                $db->setQuery($query);
                $cities = $db->loadObjectList();

                $query = $db->getQuery(true);
                $query->select('*');
                $query->from('#__viaggio_hotels');
                $db->setQuery($query);
                $all_hotels = $db->loadObjectList();

                if ($this->item->trip->id && $this->item->trip->id > 0) //this condition is absent in trip edit
                {
                    ?>
                    <hr/>
                    Отели:
                    <?php

                    if (isset($_GET['id']) && intval($_GET['id']))
                    {
                        $query = $db->getQuery(true);
                        $query->select('a.*, hotel.city_id');
                        $query->from('#__viaggio_orders_hotels a');//difference between trips and orders
                        $query->join('LEFT','#__viaggio_hotels AS `hotel` ON `hotel`.id = a.`hotel_id`');
                        $query->where('order_id = '.$this->item->order_id);//difference between trips and orders
                        $db->setQuery($query);
                        $hotels = $db->loadObjectList();

                        $query = $db->getQuery(true);
                        $query->select('*');
                        $query->from('#__viaggio_orders_cities');//difference between trips and orders
                        $query->where('order_id = '.$this->item->order_id);//difference between trips and orders
                        $db->setQuery($query);
                        $order_cities = $db->loadObjectList();
                    }

                    foreach ($hotels as $hotel)
                    {
                        ?>
                        <div class='hotel-row'>
                            Город:
                            <select class='hotel-city-id' name="city_id[]" onchange="filterHotels();">
                                <option value='0'> -- Выберите город -- </option>
                                <?php
                                foreach ($cities as $city) {
                                    echo "<option value='" . $city->id . "' " . (($city->id == $hotel->city_id) ? 'selected' : '') . ">" . $city->name_rus . "</option>";
                                }
                                ?>
                            </select>

                            Отель:
                            <select class='hotel-id' name="hotel_id[]" hotel_id="<?php echo $hotel->hotel_id; ?>">
                                <option value="0"> -- Выберите отель -- </option>
                            </select>
                            <div class="uk-icon-trash uk-icon-medium" style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()"></div>
                        </div>
                        <?php
                    }

                    foreach ($order_cities as $order_city)
                    {
                        ?>
                        <div class='hotel-row'>
                            Город:
                            <select class='hotel-city-id' name="city_id[]" onchange="filterHotels();">
                                <option value='0'> -- Выберите город -- </option>
                                <?php
                                foreach ($cities as $city) {
                                    echo "<option value='" . $city->id . "' " . (($city->id == $order_city->city_id) ? 'selected' : '') . ">" . $city->name_rus . "</option>";
                                }
                                ?>
                            </select>

                            Отель:
                            <select class='hotel-id' name="hotel_id[]" hotel_id="0">
                                <option value="0"> -- Выберите отель -- </option>
                            </select>
                            <div class="uk-icon-trash uk-icon-medium" style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()"></div>
                        </div>
                        <?php
                    }

                    ?>
                <?php } ?>
                <div class="add-hotel" style="cursor: pointer" onclick="addHotel()">Добавить запись</div>
                <div class='hotel-row-empty' style="display: none">
                    Город:
                    <select class='hotel-city-id' name="city_id[]" onchange="filterHotels();">
                        <option value='0'> -- Выберите город -- </option>
                        <?php
                        foreach ($cities as $city) {
                            echo "<option value='" . $city->id . "'>" . $city->name_rus . "</option>";
                        }
                        ?>
                    </select>

                    Отель:
                    <select class='hotel-id' name="hotel_id[]" hotel_id="0">
                        <option value="0"> -- Выберите отель -- </option>
                        <?php
                        foreach ($all_hotels as $all_hotel)
                        {
                            echo "<option city='".$all_hotel->city_id."' value='".$all_hotel->id."'>".$all_hotel->hotel_name_rus."</option>";
                        }
                        ?>
                    </select>
                    <div class="uk-icon-trash uk-icon-medium"  style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()"></div>
                </div>
                <script>
                    function getTripData() {
                        jQuery('.hotel-row').remove();
                        jQuery.getJSON('/administrator/?option=com_viaggio&task=getHotelsByTrip&trip_id='+jQuery('#jform_trip_id').val(),function(data){
                            jQuery.each(data.data, function(id, row) {
                                addHotel();
                                jQuery('.hotel-row:last .hotel-city-id').val(row.city_id);
                                jQuery('.hotel-row:last .hotel-city-id').trigger("liszt:updated");
                                jQuery('.hotel-row:last select.hotel-id').attr('hotel_id',row.hotel_id);
                            });
                            filterHotels();
                        });
                    }
                    function addHotel()
                    {
                        jQuery('.add-hotel').before(jQuery('.hotel-row-empty').clone());//clone template hotel block
                        jQuery('.hotel-row-empty:first').addClass('hotel-row').removeClass('hotel-row-empty');//change class of cloned block
                        jQuery('.hotel-row:last select.hotel-id option').remove();//clear hotels list
                        jQuery('.hotel-row:last').show();//show cloned block
                        //remove old chosen block and generate new
                        jQuery('.hotel-row:last select').each(function () {
                            jQuery(this).next().remove();
                            jQuery(this).chosen();
                        });
                    }
                    function filterHotels()
                    {
                        //fill hotels list according to chosen city and stored in DB hotel
                        jQuery('.hotel-row .hotel-id').each(function () {
                            var current_select = jQuery(this);
                            let city_id = current_select.parent().find('.hotel-city-id').val();

                            //clear option list
                            current_select.find('option').remove();

                            //fill hotels list according to chosen city
                            jQuery('.hotel-row-empty option[city='+city_id+']').each(function () {
                                let current_option = jQuery(this).clone();
                                current_select.append(current_option);
                            });

                            //select stored in DB hotel
                            current_select.val(current_select.attr('hotel_id'));

                            //update chosen
                            current_select.trigger("liszt:updated");
                        });
                    }
                    jQuery(document).ready(function() {
                        filterHotels();
                    });
                </script>
                <!-- HOTELS EDIT END -->
                <div class="control-group">
                    <div class="control-label">Date from</div>
                    <div class="controls">
                        <?php echo JHtml::_('calendar', $this->item->date_from, "date_from", "date_from", '%d/%m/%Y', array('placeholder'=>"Date from")); ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label">Date to</div>
                    <div class="controls">
                        <?php echo JHtml::_('calendar', $this->item->date_to, "date_to", "date_to", '%d/%m/%Y', array('placeholder'=>"Date to")); ?>
                    </div>
                </div>
                <div class="control-group order_id">
                    <div class="control-label">order_id</div>
                    <div class="controls">
                        <?php
                        if (isset($this->item->order_id) && $this->item->order_id)
                            echo '<input type=hidden name="jform[order_id]" value="'.$this->item->order_id.'">'.$this->item->order_id;
                        else
                        {
                            ?>
                            <script>var orders = [];</script>
                            <select name="jform[order_id]" id="order_id">
                                <option value=""> --- select order id --- </option>
                                <?php
                                $cancellation = false;
                                $cancellation_ita = false;
                                foreach ($orders as $order)
                                {
                                    if (isset($_GET['data']) && intval($_GET['data']) > 0 && $order->id == intval($_GET['data']))
                                    {
                                        $cancellation = $order->cancellation;
                                        $cancellation_ita = $order->cancellation_ita;
                                    }
                                    //$query = "SELECT sum(amountEuro) + sum(comissionEuro) sum FROM wza4w_viaggio_manualpayments WHERE hidden = 0 AND status IN (1,2,3) AND order_id = ".$order->id;
                                    $query = "SELECT sum(amountEuro) sum FROM wza4w_viaggio_manualpayments WHERE hidden = 0 AND status IN (1,2,3) AND order_id = ".$order->id;
                                    $db->setQuery($query);
                                    $sum = $db->loadObject();
                                    echo '<option value="'.$order->id.'" remaining="'.($order->valute_amount - $sum->sum).'">'.$order->id.' (email: '.$order->email.', amount: '.$order->valute_amount.'€)</option>';
                                    $query = $db->getQuery(true);
                                    $query->select('*');
                                    $query->from('#__viaggio_clients');
                                    $query->where('order_id = '.$order->id);
                                    $query->order('id ASC');
                                    $db->setQuery($query);
                                    $clients = $db->loadObjectList();
                                    $arr=array();
                                    foreach ($clients as $client)
                                    {
                                        $arr[] = '{"cognome":"'.$client->cognome.'","nome":"'.$client->nome.'","numero_di_passaporto":"'.$client->numero_di_passaporto.'","sex":"'.$client->sex.'","nascita":"'.$client->birthday.'"}';
                                    }
                                    echo '<script>orders['.$order->id.'] = {"check_type":"'.$order->check_type.'","total_amount":"'.$order->valute_amount.'","stellcount":"'.$order->stell.'","insurance":"'.$order->insurance.'","fio":"'.$order->nomecgome.'","telefon":"'.$order->telephone.'","email":"'.$order->email.'","clients":['.implode(',',$arr).']};</script>';
                                }
                                ?>
                            </select>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label">Цена за тур</div>
                    <div class="controls">
                        <input type="text" id="tour_price" need-to-calc value="<?php echo $this->item->order_euro_amount; ?>">
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-label">Платёж Invoce</div>
                    <div class="controls"><?php echo $this->form->getInput('amountEuro'); ?></div>
                </div>

                <div class="control-group">
                    <div class="control-label">Общая сумма</div>
                    <div class="controls"><input type="text" name="total_amount" id ="total_amount" readonly></div>
                </div>

                <?php echo $this->form->renderField('client_type'); ?>
                <?php echo $this->form->renderField('bank'); ?>
                <?php echo $this->form->renderField('check_type'); ?>

                <?php /*<div class="control-group not-read-only" id="check_type" style="display: none">
                        <div class="control-label">Фирма</div>
                        <div class="controls">
                            <input type="radio" name="jform[check_type]" value="" id="check_type_empty" style="display: none">
                            <input type="radio" name="jform[check_type]" value="1" id="check_type_1">
                            Русский Турн
                            <br/>
                            <input type="radio" name="jform[check_type]" value="2" id="check_type_2">
                            Висто Руссиа
                        </div>
                    </div> */ ?>

                <div class="control-group">
                    <div class="control-label">fio</div>
                    <div class="controls"><?php echo $this->form->getInput('fio'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label">telefon</div>
                    <div class="controls"><?php echo $this->form->getInput('telefon'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label">email</div>
                    <div class="controls"><?php echo $this->form->getInput('email'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label">description</div>
                    <div class="controls"><?php echo $this->form->getInput('description'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label">description (rus)</div>
                    <div class="controls"><?php echo $this->form->getInput('description_rus'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label">pole1</div>
                    <div class="controls"><?php echo $this->form->getInput('pole1'); ?></div>
                </div>
                <div class="control-group" style="display: none" id="cp2018">
                    <div class="control-label">COSTO PARZIALE 2018</div>
                    <div class="controls"><input name="cp2018"></div>
                </div>

                <div class="control-group" id="full-payment">
                    <div class="control-label"></div>
                    <div class="controls">
                        <button onclick="fullPayment();return false;">Полная оплата</button>
                    </div>
                </div>
                <?php if (isset($this->item->id)) { ?>
                    <div class="control-group">
                        <div class="control-label">payButton</div>
                        <div class="controls"><?php echo $this->form->getInput('payButton'); ?></div>
                    </div>

                    <div class="control-group">
                        <div class="control-label">timeCreated</div>
                        <div class="controls"><?php echo $this->form->getInput('timeCreated'); ?></div>
                    </div>

                    <div class="control-group">
                        <div class="control-label">timeCreatedLinl</div>
                        <div class="controls"><?php echo $this->form->getInput('timeCreatedLinl'); ?></div>
                    </div>
                <?php } ?>
            </fieldset>
        </div>
        <div class="span4 not-show-on-order-set">
            <div class="uk-inline uk-width-1-1">
                <button class="uk-button uk-button-primary client-button" type="button" onclick="cloneBlock('client')">Добавить клиента</button>
                Сколько добавить: <input id="number_of_blocks" value="1"><br><br>

                <b>Клиенты</b><br/>
                <div class="client" block-id="0">
                    <input type="text" name="clients[0][cognome]" placeholder="cognome">
                    <input type="text" name="clients[0][nome]" placeholder="nome">
                    <input type="text" name="clients[0][numero_di_passaporto]" placeholder="numero_di_passaporto">
                    <select name="clients[0][sex]"><option value="m" selected>Мужик</option><option value="f">Леди</option></select>
                    <?php echo JHtml::_('calendar', '', "clients[0][nascita]", "clients[0][nascita]", '%d/%m/%Y', array('placeholder'=>"nascita")); ?>
                    <input type="text" name="clients[0][telephone]" placeholder="Telephone"/>
                    <input type="text" name="clients[0][indirizzo_di_residenza]" placeholder="indirizzo_di_residenza"/>
                    <input type="text" name="clients[0][luogo_di_nascita]" placeholder="luogo_di_nascita"/>
                    <?php echo JHtml::_('calendar', '', "clients[0][date_from]", "clients[0][date_from]", '%d/%m/%Y', array('placeholder'=>"data_di_rilascio_from")); ?>
                    <?php echo JHtml::_('calendar', '', "clients[0][date_to]", "clients[0][date_to]", '%d/%m/%Y', array('placeholder'=>"data_di_rilascio_to")); ?>
                    <input type="text" name="clients[0][numero_di_visite]" placeholder="numero_di_visite" value="0"/>
                    <?php echo JHtml::_('calendar', '', "clients[0][last_visit_from]", "clients[0][last_visit_from]", '%d/%m/%Y', array('placeholder'=>"last_visit_from")); ?>
                    <?php echo JHtml::_('calendar', '', "clients[0][last_visit_to]", "clients[0][last_visit_to]", '%d/%m/%Y', array('placeholder'=>"last_visit_to")); ?>
                    <input type="text" name="clients[0][nome_dell_organizzazione]" placeholder="organization"/>
                    <input type="text" name="clients[0][ufficio_dell_organizzazione]" placeholder="position"/>
                    <input type="text" name="clients[0][indirizzo_dell_organizzazione]" placeholder="address"/>
                    <input type="text" name="clients[0][telefono_dell_organizzazione]" placeholder="telefono_dell_organizzazione"/>
                    <input type="email" name="clients[0][email]" class="validate-email" placeholder="Email"/>
                    <hr/>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo JHtml::_('bootstrap.endTab'); ?>
<script>
    jQuery(document).ready(function() {
        jQuery('#tour-cat').on('change',function () {
            jQuery('input[name=trip_id]').val(0);
            jQuery('#jform_trip_id').find('option[class!=const]').remove();
            let id = jQuery(this).val();
            if (id > 0)
            {
                jQuery('.choose_tour').show();
                jQuery.getJSON('/eng/?option=com_viaggio&task=tripsByCategory&key=2VKAFw6iw8ebVH2Rr1eE&cat_id='+id,function(data){
                    jQuery.each(data.data, function(id, name_rus) {
                        jQuery('#jform_trip_id').append('<option value="'+id+'" datefrom="'+data.full_data[id].date_from+'" dateto="'+data.full_data[id].date_to+'">'+name_rus+'</option>');
                    });
                    jQuery('#jform_trip_id').trigger("liszt:updated");
                });
            }
            else
                jQuery('.choose_tour').hide();
        });
        jQuery('#jform_trip_id').on('change',function(){
            jQuery('input[name=trip_id]').val(jQuery(this).val());
            getTripData();
        });
        jQuery('#parent-cat').on('change',function(){
            jQuery('#tour-cat').val(0);
            jQuery('#tour-cat').find('option[class!=const]').remove();
            let id = jQuery(this).val();//

            if (id > 0)
            {
                jQuery('.tour-cat-parent').show();
                jQuery.getJSON('/eng/?option=com_viaggio&task=tripsSubcategories&key=2VKAFw6iw8ebVH2Rr1eE&id='+id,function(data){
                    jQuery.each(data.data, function(id, title_ita) {
                        jQuery('#tour-cat').append('<option value="'+id+'">'+title_ita+'</option>');
                    });
                    jQuery('#tour-cat').trigger("liszt:updated");
                });
            }
            else
                jQuery('.tour-cat-parent').hide();
        });
    });
</script>
