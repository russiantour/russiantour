<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<?php
$user_groups = JAccess::getGroupsByUser($user->get('id'));
if (in_array(8,$user_groups))
    echo  'aaaaa1' ;

else
    echo 'aaaa2';
?>
<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'avia', 'Авиа РЖД'); ?>
<div class="row-fluid">
    <div class="span10 form-horizontal">
        <div class="span4 not-show-on-order-set">
            <div class="uk-inline uk-width-1-1">
                <div id="toggle-animation1" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
                    <div class="air-ticket  uk-grid " block-id="0">
                        <div class="uk-grid  uk-margin-remove-top uk-text-center" >
                            <div class="uk-width-1-1 uk-text-center">
                                <span class="uk-h3 uk-text-center">Авиабилет</span>
                            </div>
                        </div>
                        <div class="uk-margin-remove  uk-grid">
                            <div style=" padding-left: 3px; " class="uk-margin-remove uk-width-1-4 ">
                                <input  style=" width: 100px; "class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="air-ticket[0][amount]">
                            </div>
                            <div class="uk-margin-remove">
                                <?php echo JHtml::_('calendar', '', "air-ticket[0][begin]", "air-ticket[0][begin]", '%d/%m/%Y', array('placeholder'=>"Дата начало тура")); ?>
                                <?php echo JHtml::_('calendar', '', "air-ticket[0][end]", "air-ticket[0][end]", '%d/%m/%Y',  array('placeholder'=>"Дата завершение тура")); ?>
                            </div>
                        </div>
                        <div class="uk-grid  uk-margin-remove-top" >
                            <div class="uk-width-1-5">
                                Откуда?
                            </div>
                            <div class="uk-width-2-5">
                                <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[0][to-city-rus]">
                            </div>
                            <div class="uk-width-2-5" >
                                <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[0][from-city-rus]">
                            </div>
                        </div>
                        <div class="uk-grid uk-margin-remove-top" >
                            <div class="uk-width-1-5">
                                Куда?
                            </div>
                            <div class="uk-width-2-5">
                                <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[0][to-city-ita]">
                            </div>
                            <div class="uk-width-2-5">
                                <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[0][from-city-rus]">
                            </div>
                        </div>
                        <div class="uk-grid  uk-margin-remove-top" >
                            <div class="uk-width-1-5">
                                Обратно
                            </div>
                            <div class="uk-width-2-5">
                                <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="air-ticket[0][back-city-ita]">
                            </div>
                            <div class="uk-width-2-5">
                                <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="air-ticket[0][back-city-rus]">
                            </div>
                        </div>
                        <div class="uk-grid  uk-margin-remove-top" >
                            <div class="uk-width-1-1">
                                <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('air-ticket')">Добавить билет</button>
                            </div>
                        </div>
						
						<!-- ORDER AIR SCRIPT -->
						<div class="uk-margin">
						<input type="hidden" class="uk-input uk-form-width-small" id="airdate1" placeholder="Дата начало тура" name="air-ticket[0][begin]" value="<?php echo isset($this->item->airTickets)?str_replace('/','-',$this->item->airTickets['0']['begin']):''; ?>">
						</div>

						<div class="uk-margin">
						<input type="hidden" class="uk-input uk-form-width-small" id="airdate2" placeholder="Дата завершение тура" name="air-ticket[0][end]" value="<?php echo isset($this->item->airTickets)?str_replace('/','-',$this->item->airTickets['0']['end']):''; ?>">
						</div>


						<script type="text/javascript">
							document.getElementById('order_avia_checkbox').checked = document.getElementById('airdate1').value;
							document.getElementById('order_avia_checkbox').checked = document.getElementById('airdate2').value;
							document.getElementById('order_avia_checkbox').onclick = function(){
							if(this.checked){
								document.getElementById('airdate1').value = "01/01/1970";
								document.getElementById('airdate2').value = "01/01/1970";
								document.getElementById('air-ticket[0][begin]').value = "01/01/1970";
								document.getElementById('air-ticket[0][end]').value = "01/01/1970";
								}
							else {
								document.getElementById('airdate1').value = "";  
								document.getElementById('airdate2').value = "";
								document.getElementById('air-ticket[0][begin]').value = "";
								document.getElementById('air-ticket[0][end]').value = "";
								}
							}
						</script>
                    </div>
                </div>

                <div class="uk-inline uk-width-1-1">
                    <div id="toggle-animation2" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
                        <div class="rail-ticket" block-id="0">
                            <div class="uk-grid  uk-margin-remove-top uk-text-center" >
                                <div class="uk-width-1-1 uk-text-center">
                                    <span class="uk-h3 uk-text-center">РЖД</span>
                                </div>
                            </div>
                            <div class="uk-margin-remove  uk-grid">
                                <div style=" padding-left: 3px; " class="uk-margin-remove uk-width-1-4 ">
                                    <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="rail-ticket[0][amount]">
                                </div>
                                <div class="uk-margin-remove">
                                    <?php echo JHtml::_('calendar', '', "rail-ticket[0][begin]", "rail-ticket[0][begin]", '%d/%m/%Y', array('placeholder'=>"Дата начало тура")); ?>
                                    <?php echo JHtml::_('calendar', '', "rail-ticket[0][end]", "rail-ticket[0][end]", '%d/%m/%Y', array('placeholder'=>"Дата завершение тура")); ?>
                                </div>
                            </div>
                            <div class="uk-grid  uk-margin-remove-top" >
                                <div class="uk-width-1-5">Откуда?</div>
                                <div class="uk-width-2-5">
                                    <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="rail-ticket[0][from-city-ita]">
                                </div>
                                <div class="uk-width-2-5">
                                    <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="rail-ticket[0][from-city-rus]">
                                </div>
                            </div>
                            <div class="uk-grid  uk-margin-remove-top" >
                                <div class="uk-width-1-5">
                                    Куда?
                                </div>
                                <div class="uk-width-2-5">
                                    <input class="uk-input uk-form-width-small" placeholder="Город по итальянски" name="rail-ticket[0][to-city-ita]">
                                </div>
                                <div class="uk-width-2-5">
                                    <input class="uk-input uk-form-width-small" placeholder="Город по русски" name="rail-ticket[0][to-city-rus]">
                                </div>
                            </div>
                            <button class="uk-button uk-button-primary" type="button" onclick="cloneBlock('rail-ticket')">Добавить билет</button>
                        
						<!-- ORDER RAIL SCRIPT -->
						
						<div class="uk-margin">
						<input type="hidden" class="uk-input uk-form-width-small" id="raildate1" placeholder="Дата начало тура" name="rail-ticket[0][begin]" value="<?php echo isset($this->item->railTickets)?str_replace('/','-',$this->item->railTickets['0']['begin']):''; ?>">
						</div>

						<div class="uk-margin">
						<input type="hidden" class="uk-input uk-form-width-small" id="raildate2" placeholder="Дата завершение тура" name="rail-ticket[0][end]" value="<?php echo isset($this->item->railTickets)?str_replace('/','-',$this->item->railTickets['0']['end']):''; ?>">
						</div>

						<script type="text/javascript">
							document.getElementById('order_rail_checkbox').checked = document.getElementById('raildate1').value;
							document.getElementById('order_rail_checkbox').checked = document.getElementById('raildate2').value;
							document.getElementById('order_rail_checkbox').onclick = function(){
							if(this.checked){
								document.getElementById('raildate1').value = "01/01/1970";
								document.getElementById('raildate2').value = "01/01/1970";
								document.getElementById('rail-ticket[0][begin]').value = "01/01/1970";
								document.getElementById('rail-ticket[0][end]').value = "01/01/1970";
								}
							else {
								document.getElementById('raildate1').value = "";  
								document.getElementById('raildate2').value = "";
								document.getElementById('rail-ticket[0][begin]').value = "";
								document.getElementById('rail-ticket[0][end]').value = "";
								}
							}
						</script>
						
						
						</div>
                    </div>
                    <div class="uk-inline uk-width-1-1">
                        <div id="toggle-animation-visa" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
                            <div class="uk-grid  uk-margin-remove-top uk-text-center" >
                                <div class="uk-width-1-1 uk-text-center">
                                    <span class="uk-h3 uk-text-center">Визы</span>
                                </div>
                            </div>
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="visa-amount"><br>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="2" placeholder="Русский текст" name="visa-rus"></textarea>
                            </div>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="2" placeholder="Итальянский текст" name="visa-ita"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="uk-inline uk-width-1-1">
                        <div id="toggle-animation3" class="uk-card uk-card-default uk-card-body uk-card-small uk-margin-small"  >
                            <div class="uk-grid  uk-margin-remove-top uk-text-center" >
                                <div class="uk-width-1-1 uk-text-center">
                                    <span class="uk-h3 uk-text-center">Прочее</span>
                                </div>
                            </div>
                            <input class="uk-input uk-form-width-medium" type="text" placeholder="Стоимость" need-to-calc name="other-amount"><br>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="2" placeholder="Русский текст" name="other-rus"></textarea>
                            </div>
                            <div class="uk-margin">
                                <textarea class="uk-textarea" rows="2" placeholder="Итальянский текст" name="other-ita"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php echo JHtml::_('bootstrap.endTab'); ?>
