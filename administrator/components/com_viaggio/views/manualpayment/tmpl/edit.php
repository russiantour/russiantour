<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/viaggio.css');
 
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');
$db    = JFactory::getDbo();

$user   = JFactory::getUser();

$query = $db->getQuery(true);
$user_groups = JAccess::getGroupsByUser($user->get('id'));

$year_ago = date('Y-m-d H:i:s', (time() - 3600 * 24 * 365));

if (!in_array(8,$user_groups))
    $query = "SELECT * FROM #__viaggio_orders WHERE (full_payment is NULL or full_payment = 0) AND created_at > '" . $year_ago . "' AND created_by = ".$user->id." ORDER BY id DESC";
else
    $query = "SELECT * FROM #__viaggio_orders WHERE (full_payment is NULL or full_payment = 0) AND created_at > '" . $year_ago . "' ORDER BY id DESC";
$db->setQuery($query);
$orders = $db->loadObjectList();

$query = 'select p.*, ps.service_id from #__viaggio_partners p
join #__viaggio_partners_services ps on p.id = ps.partner_id';
$db->setQuery($query);
$partners = $db->loadObjectList();
$partners_of_services = array();
foreach ($partners as $partner)
{
    $partners_of_services[$partner->service_id][] = $partner;
}

$query = "SELECT * FROM #__viaggio_services ORDER BY name_rus ASC";
$db->setQuery($query);
$servicesList = $db->loadObjectList();

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_trips_categorys');
$query->where('state is null or state <> -2');
//$query->order('CAST(title_ita AS UNSIGNED) asc, title_ita ASC');
$query->order('ordering ASC');
$db->setQuery($query);
$trips_categorys = $db->loadObjectList();
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'manualpayment.cancel') {
            Joomla.submitform(task, document.getElementById('manualpayment-form'));
        }
        else {
            
            if (task != 'manualpayment.cancel' && document.formvalidator.isValid(document.id('manualpayment-form'))) {
                
                Joomla.submitform(task, document.getElementById('manualpayment-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<?php $itemId = (isset($this->item->id)?$this->item->id:0); ?>
  
 <link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<form action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . $itemId); ?>" method="post" name="adminForm" id="manualpayment-form" class="form-validate">
    <?php echo $this->form->getInput('full_payment'); ?>
    <div class="form-horizontal to-set-read-only">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
        <?php include  ('tabs/general.php'); ?>
        <?php include  ('tabs/services.php'); ?>
        <?php include  ('tabs/2018.php'); ?>
        <?php include  ('tabs/avia.php'); ?>
        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'cancellation', 'АННУЛЯЦИЯ'); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <?php echo $this->form->renderField('cancellation',null,($cancellation?$cancellation:null)); ?>
                <?php echo $this->form->renderField('cancellation_ita',null,($cancellation_ita?$cancellation_ita:null)); ?>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>
    </div>
    <input type="hidden" name="task" value="" />
    <?php echo JHtml::_('form.token'); ?>
</form>
<script>
    jQuery( document ).ready(function() {
        jQuery('#order_id').on('change',function () {
            jQuery('#jform_full_payment').val(0);
            jQuery('#jform_amountEuro').val('');
            jQuery('#jform_amountEuro').removeAttr('readonly');
            if (jQuery(this).val()!='')
            {
                jQuery('#check_type').show();

                //jQuery('#full-payment').show();
                jQuery('a[href="#services"]').hide();
                jQuery('.trip_id').hide();
                jQuery('.to-set-read-only input').attr('readonly','readonly');
                jQuery('#cp2018').show();
                jQuery('#cp2018 input').removeAttr('readonly');
                jQuery('.to-set-read-only select').attr('readonly','readonly');
                jQuery('.not-read-only input').removeAttr('readonly');
                jQuery('.to-set-read-only select').trigger("chosen:updated");
                jQuery('.to-set-read-only [name="jform[trip_id]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[order_id]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[description]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[description_rus]"]').removeAttr('readonly');
                jQuery('.to-set-read-only [name="jform[amountEuro]"]').removeAttr('readonly');
                jQuery('.field-calendar button').hide();
                jQuery('.client-button').hide();

                var order = orders[jQuery(this).val()];
                jQuery('#check_type_'+order.check_type).click();
                jQuery('#total_amount').val(order.total_amount);
                jQuery('#stellcount').val(order.stellcount);
                jQuery('#insurance').attr('checked',(order.insurance=='on'));
                jQuery('#jform_fio').val(order.fio);
                jQuery('#jform_telefon').val(order.telefon);
                jQuery('#jform_email').val(order.email);

                jQuery('.client[block-id!=0]').remove();
                var needCreate = false;

                order.clients.forEach(function (row) {
                    if (needCreate)
                        cloneBlock('client');
                    else
                        needCreate = true;
                    var block = jQuery('.client:last');
                    var block_num = jQuery('.client:last').attr('block-id');

                    block.find('[name="clients['+block_num+'][cognome]"]').val(row.cognome);
                    block.find('[name="clients['+block_num+'][nome]"]').val(row.nome);
                    block.find('[name="clients['+block_num+'][numero_di_passaporto]"]').val(row.numero_di_passaporto);
                    block.find('[name="clients['+block_num+'][sex]"]').val(row.sex);
                    block.find('[name="clients['+block_num+'][nascita]"]').val(row.nascita);
                });

                if (document.location.search.indexOf('data=') > 0)
                {
                    jQuery('#tour_price').hide();
                    jQuery('#order_id').next().after(data[0]);
                    jQuery('#order_id').next().remove();
                }

                jQuery('.not-show-on-order-set').hide();
            }
            else
            {
                jQuery('#check_type_empty').click();
                jQuery('#check_type').hide();
                jQuery('#cp2018').hide();
                //jQuery('#full-payment').hide();
                jQuery('a[href="#services"]').show();
                jQuery('.trip_id').show();
                jQuery('.to-set-read-only input').removeAttr('readonly');
                jQuery('.to-set-read-only select').removeAttr('readonly');
                jQuery('.to-set-read-only select').trigger("chosen:updated");
                jQuery('.client-button').show();

                jQuery('.not-show-on-order-set').show();
            }
        });
        if (document.location.search.indexOf('data=') > 0)
        {
            var data = document.location.search.split('&data=')[1].split('|');
            jQuery('#order_id').val(data[0]);
            jQuery('#order_id').change();
            jQuery('#jform_amountEuro').val(data[1]);
        }

        jQuery('[need-to-calc]').on('change',function(){
            var total_amount = 0;
            jQuery('[need-to-calc]').each(function(){
                var amount = parseInt(jQuery(this).val());
                if (amount)
                    total_amount += amount;
            });
            jQuery('#total_amount').val(total_amount);
        });

        jQuery('#jform_trip_id').on('change',function(){
            var value = jQuery(this).val();
            var datefrom = jQuery(this).find('option[value='+value+']').attr('datefrom');
            var dateto = jQuery(this).find('option[value='+value+']').attr('dateto');

            jQuery('input[name=date_from]').val(datefrom);
            jQuery('input[name=date_from]').attr('data-alt-value',datefrom);
            jQuery('input[name=date_from]').attr('data-local-value',datefrom);
            jQuery('input[name=date_to]').val(dateto);
            jQuery('input[name=date_to]').attr('data-alt-value',dateto);
            jQuery('input[name=date_to]').attr('data-local-value',dateto);
        });
    });
    function cloneBlock(name,num)
    {
        if (typeof num == 'undefined')
            num = 0;
        var lastBlockId = parseInt(jQuery('.'+name+':last').attr('block-id'));
        jQuery('.'+name+':last').after(jQuery('.'+name+':first').clone());
        jQuery('.'+name+':last').attr('block-id',lastBlockId+1);
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] input').each(function(){
            var inputName = jQuery(this).attr('name');
            if (inputName)
            {
                if (inputName.indexOf('amount') > 0)
                    jQuery(this).remove();
                else
                {
                    jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
                    jQuery(this).val('');
                }
            }
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] select').each(function(){
            jQuery(this).attr('name',jQuery(this).attr('name').replace('[0]','['+(lastBlockId+1)+']'));
            jQuery(this).next().remove();
            jQuery(this).chosen();
        });
        jQuery('.'+name+'[block-id='+(lastBlockId+1)+'] .field-calendar').each(function(){
            JoomlaCalendar.init(this);
        });

        if (jQuery('#number_of_blocks').length > 0)
        {
            var number_of_blocks = parseInt(jQuery('#number_of_blocks').val());
            if (number_of_blocks > 1 && num < number_of_blocks-1)
            {
                num++;
                cloneBlock(name,num);
            }
        }
    }
    function fullPayment() {
        if (jQuery('#order_id option:selected').val() == '')
            var remaining = jQuery('#tour_price').val();
        else
            var remaining = jQuery('#order_id option:selected').attr('remaining');
        jQuery('#jform_amountEuro').val(remaining);
        jQuery('#jform_amountEuro').attr('readonly','readonly');
        jQuery('#jform_full_payment').val(1);
    }
</script>