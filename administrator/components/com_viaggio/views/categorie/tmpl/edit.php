<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/touristinvite.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'hotelcategorie.cancel') {
            Joomla.submitform(task, document.getElementById('hotelcategorie-form'));
        }
        else {
            
            if (task != 'hotelcategorie.cancel' && document.formvalidator.isValid(document.id('hotelcategorie-form'))) {
                
                Joomla.submitform(task, document.getElementById('hotelcategorie-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="hotelcategorie-form" class="form-validate">

    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_TOURISTINVITE_TITLE_HOTEL_CATEGORY', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">

                    <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
                    <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />

                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hotelcategorie_translation_ita'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hotelcategorie_translation_ita'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hotelcategorie_translation_rus'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hotelcategorie_translation_rus'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hotelcategorie_translation_eng'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hotelcategorie_translation_eng'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hotelcategorie_translation_esp'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hotelcategorie_translation_esp'); ?></div>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<?php
$document->addScript(JUri::base() . '/components/com_viaggio/assets/js/en_to_rus.js');
?>