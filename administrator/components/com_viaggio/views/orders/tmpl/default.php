<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */

// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'administrator/components/com_viaggio/assets/css/viaggio.css');


$user      = JFactory::getUser();
$userId    = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn  = $this->state->get('list.direction');
$canOrder  = $user->authorise('core.edit.state', 'com_viaggio');
$saveOrder = $listOrder == 'a.`ordering`';

if ($saveOrder)
{
    $saveOrderingUrl = 'index.php?option=com_viaggio&task=orders.saveOrderAjax&tmpl=component';
    JHtml::_('sortablelist.sortable', 'orderList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}

$sortFields = $this->getSortFields();

$query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
$db=JFactory::getDBO();
$db->setQuery($query);
$curTime = $db->loadAssoc();
$curTime = strtotime($curTime['curTime']);

$db    = JFactory::getDbo();

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_services');
$query->order('name_rus asc');
$db->setQuery($query);
$pre_services = $db->loadObjectList();

$services = array();
foreach ($pre_services as $pre_service)
{
    $services[$pre_service->id] = $pre_service;
}

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_partners');
$query->order('name_rus asc');
$db->setQuery($query);
$pre_partners = $db->loadObjectList();

$partners = array();
foreach ($pre_partners as $pre_partner)
{
    $partners[$pre_partner->id] = $pre_partner;
}

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_trips_categorys');
$query->where('state is null or state <> -2');
//$query->order('title_ita asc');
$query->order('ordering ASC');
$db->setQuery($query);
$trips_categorys = $db->loadObjectList();

?>
<script type="text/javascript">
    Joomla.orderTable = function () {
        table = document.getElementById("sortTable");
        direction = document.getElementById("directionTable");
        order = table.options[table.selectedIndex].value;
        if (order != '<?php echo $listOrder; ?>') {
            dirn = 'asc';
        } else {
            dirn = direction.options[direction.selectedIndex].value;
        }
        Joomla.tableOrdering(order, dirn, '');
    };

    jQuery(document).ready(function () {
        jQuery('#clear-search-button').on('click', function () {
            jQuery('#filter_search').val('');
            jQuery('#adminForm').submit();
        });
        jQuery('#toolbar- button').off('click');
        jQuery('#toolbar- button').on('click',function () {
            $('#modal111').show();
            e.preventDefault();
            return false;
        });
    });

    window.toggleField = function (id, task, field) {

        var f = document.adminForm,
            i = 0, cbx,
            cb = f[ id ];

        if (!cb) return false;

        while (true) {
            cbx = f[ 'cb' + i ];

            if (!cbx) break;

            cbx.checked = false;
            i++;
        }

        var inputField   = document.createElement('input');
        inputField.type  = 'hidden';
        inputField.name  = 'field';
        inputField.value = field;
        f.appendChild(inputField);

        cb.checked = true;
        f.boxchecked.value = 1;
        window.submitform(task);

        return false;
    };

</script>

<?php

// Joomla Component Creator code to allow adding non select list filters
if (!empty($this->extra_sidebar))
{
    $this->sidebar .= $this->extra_sidebar;
}

?>
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<form action="<?php echo JRoute::_('index.php?option=com_viaggio&view=orders'); ?>" method="post"
      name="adminForm" id="adminForm">
    <?php if (!empty($this->sidebar)): ?>
    <div id="j-sidebar-container" class="span2">
        <?php echo $this->sidebar; ?>
    </div>
    <div id="j-main-container" class="span10">
        <?php else : ?>
        <div id="j-main-container">
            <?php endif; ?>

            <div id="filter-bar" class="btn-toolbar">
                <div class="btn-group pull-left">
                    <select name="search_type">
                        <option value="id">поиск по id</option>
                        <option value="">поиск по order</option>
                        <option value="people" <?php if (isset($_POST['search_type']) && $_POST['search_type']=='people') echo 'selected'; ?>>Поиск по людям</option>
                    </select>
                </div>
                <div class="filter-search btn-group pull-left">
                    <label for="filter_search"
                           class="element-invisible">
                        <?php echo JText::_('JSEARCH_FILTER'); ?>
                    </label>
                    <input type="text" name="filter_search" id="filter_search"
                           placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>"
                           value="<?php echo $this->escape($this->state->get('filter.search')); ?>"
                           title="<?php echo JText::_('JSEARCH_FILTER'); ?>"/>
                </div>
                <div class="btn-group pull-left">
                    <button class="btn hasTooltip" type="submit"
                            title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>">
                        <i class="icon-search"></i></button>
                    <button class="btn hasTooltip" id="clear-search-button" type="button"
                            title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>">
                        <i class="icon-remove"></i></button>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&owner=my" class="btn">Свои</a>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&owner=all" class="btn">Все</a>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&from=<?php echo ViaggioHelpersViaggio::ORDER_BY_EMAIL; ?>" class="btn">Почта</a>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&from=<?php echo ViaggioHelpersViaggio::ORDER_BY_CHAT; ?>" class="btn">Чат</a>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&from=<?php echo ViaggioHelpersViaggio::ORDER_BY_SITE; ?>" class="btn">Сайт</a>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&from=<?php echo ViaggioHelpersViaggio::ORDER_BY_PHONE; ?>" class="btn">Телефон</a>
                    <a href="/administrator/index.php?option=com_viaggio&view=orders&from=<?php echo ViaggioHelpersViaggio::ORDER_BY_VIAGGIO; ?>" class="btn">Viaggio</a>
                </div>
                <div class="btn-group pull-right hidden-phone">
                    <label for="limit"
                           class="element-invisible">
                        <?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?>
                    </label>
                    <?php echo $this->pagination->getLimitBox(); ?>
                </div>
                <div class="btn-group pull-right hidden-phone">
                    <label for="directionTable"
                           class="element-invisible">
                        <?php echo JText::_('JFIELD_ORDERING_DESC'); ?>
                    </label>
                    <select name="directionTable" id="directionTable" class="input-medium"
                            onchange="Joomla.orderTable()">
                        <option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
                        <option value="asc" <?php echo $listDirn == 'asc' ? 'selected="selected"' : ''; ?>>
                            <?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?>
                        </option>
                        <option value="desc" <?php echo $listDirn == 'desc' ? 'selected="selected"' : ''; ?>>
                            <?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?>
                        </option>
                    </select>
                </div>
                <div class="btn-group pull-right">
                    <label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
                    <select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
                        <option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
                        <?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
                    </select>
                </div>
            </div>
            <div class="clearfix"></div>
            <table class="table table-striped" id="orderList">
                <thead>
                <tr>
                    <?php if (isset($this->items[0]->ordering)): ?>
                        <th width="1%" class="nowrap center hidden-phone">
                            <?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'a.`ordering`', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
                        </th>
                    <?php endif; ?>
                    <th width="1%" class="hidden-phone">
                        <input type="checkbox" name="checkall-toggle" value=""
                               title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)"/>
                    </th>
                    <?php if (isset($this->items[0]->state)): ?>

                    <?php endif; ?>

                    <th class='uk-width-1-4'>
                        <?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_ID', 'a.`id`', $listDirn, $listOrder); ?>
                    </th>

                    <th class='uk-width-1-4'>
                        <?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_CREATED_BY', 'a.`created_by`', $listDirn, $listOrder); ?>
                    </th>
                    <th class='left'>
                        <?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_CURRENCY', 'a.`currency`', $listDirn, $listOrder); ?>
                    </th>
                    <th class='left'>
                        <?php echo JHtml::_('grid.sort',  'COM_VIAGGIO_ORDERS_ORDERSTATUS', 'a.`orderstatus`', $listDirn, $listOrder); ?>
                    </th>


                </tr>
                </thead>
                <tfoot>
                <tr>
                    <td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
                        <?php echo $this->pagination->getListFooter(); ?>
                    </td>
                </tr>
                </tfoot>
                <tbody>
                <?php foreach ($this->items as $i => $item) :
                $date = explode(' ',$manualpayment->timeCreated);
                $date = explode('-',$date[0]);
                $str = array(
                    'paymentID='.$manualpayment->paymentID,
                    'mp_d='.$date[2],
                    'mp_m='.$date[1],
                    'mp_y='.$date[0],
                    'order_id='.$manualpayment->order_id,
                    'amountEur='.$manualpayment->amountEuro
                );
                $db = JFactory::getDbo();
                $db->setQuery("SELECT * FROM #__viaggio_manualpayments_details WHERE payment_id = '".$item->paymentID."'");
                $details = $db->loadObjectList();
                $payTime = strtotime($item->timeCreatedLinl);
                $ordering   = ($listOrder == 'a.ordering');
                $canCreate  = $user->authorise('core.create', 'com_viaggio');
                $canEdit    = $user->authorise('core.edit', 'com_viaggio');
                $canCheckin = $user->authorise('core.manage', 'com_viaggio');
                $canChange  = $user->authorise('core.edit.state', 'com_viaggio');

                $db = JFactory::getDbo();
                $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = '".$item->id."' AND status <> 0 AND hidden <> 1 ORDER BY id DESC");
                $manualpayments = $db->loadObjectList();
                ?>
                <tr class="row<?php echo $i % 2; ?>">

                    <?php if (isset($this->items[0]->ordering)) : ?>
                        <td class="order nowrap center hidden-phone">
                            <?php if ($canChange) :
                                $disableClassName = '';
                                $disabledLabel    = '';

                                if (!$saveOrder) :
                                    $disabledLabel    = JText::_('JORDERINGDISABLED');
                                    $disableClassName = 'inactive tip-top';
                                endif; ?>
                                <span class="sortable-handler hasTooltip <?php echo $disableClassName ?>"
                                      title="<?php echo $disabledLabel ?>">
							<i class="icon-menu"></i>
						</span>
                                <input type="text" style="display:none" name="order[]" size="5"
                                       value="<?php echo $item->ordering; ?>" class="width-20 text-area-order "/>
                            <?php else : ?>
                                <span class="sortable-handler inactive">
							<i class="icon-menu"></i>
						</span>
                            <?php endif; ?>
                        </td>
                    <?php endif; ?>
                    <td class="hidden-phone">
                        <?php echo JHtml::_('grid.id', $i, $item->id); ?>
                    </td>
                    <?php if (isset($this->items[0]->state)): ?>

                    <?php endif; ?>

                    <td>

                        <?php /*switch ($item['userId']) {
    case 725:
    $name = 'Paolo Carlucci';
        break;
    case 510:
    $name = 'Nadia Efremova ';
        break;
    case 663:
        $name = 'Anastasia Dimitri';
                break;
    case 673:
        $name = 'Nata';
                break;
    case 594 :
    $name = 'Ekaterina Alekseeva';
        break;
    case 725 :
    $name = 'Paolo Carlucci';
        break;
        default:
    $name = 'Аноним';
        break;
}
*/ ?>
                        <!--<?php echo $item->tour_id; ?>
						                        <?php if ($item->userId) : ?>
                        Создал <br/>
                        <?php echo $rowUser->get('username'); ?><br/><br/>
                        id <?php echo $item->paymentID.'/'.$rowUser->get('id'); ?>
                        <?php endif; ?>-->
                        <div class="uk-h3 uk-text-center">
                            <a href="/administrator/index.php?option=com_viaggio&view=order&layout=edit&id=<?php echo $item->id; ?>"> <?php echo $item->id; ?> </a>
                            <br>
                            <?php echo $item->tour->name_rus; ?>
                            <br>
                            Категория тура: <?php echo $item->tour->category->title_ita; ?>
                        </div>
                        <div class="uk-h4 uk-text-center"><?php echo date('d/m/Y',strtotime($item->date_from)); ?> - <?php echo date('d/m/Y',strtotime($item->date_to)); ?>   </div>
                        <?php echo  $item->description?' <div class="uk-comment-meta">description</div><h3 class="uk-comment-title">'.$item->description.'</h3>':'' ;?>
  <?php echo (($manualPayment['order']['cancellation_ita']=='')?'':'<br>CONDIZIONI DI CANCELLAZIONE:<br> '.$manualPayment['order']['cancellation_ita'].'<br/>') ;?>

                        <?php echo  $item->nomecgome?' <div class="uk-comment-meta">FIO</div><h3 class="uk-comment-title">'.$item->nomecgome.'</h3>':'' ;?>
                        <?php echo  $item->telephone?' <div class="uk-comment-meta">Telephone</div><h3 class="uk-comment-title">'.$item->telephone.'</h3>':'' ;?>
                        <?php echo  $item->pole1?' <div class="uk-comment-meta">Для юр. договора</div><h3 class="uk-comment-title">'.$item->pole1.'</h3>':'' ;?>

                        <h3 class="uk-comment-title"> Persona <a href="/administrator/index.php?option=com_viaggio&view=clients&filter_search=<?php echo $item->id; ?>"><?php echo $item->clientcount; ?></a> </h3>
                              <?php echo (($item->cancellation_ita=='')?'':'CONDIZIONI DI CANCELLAZIONE - <b> Si</b>') ;?><br>

                        <?php if (!$item->accordo) { ?>
                            1.  <button class="uk-button uk-button-primary" type="button" >
                                <a href="/administrator/index.php?option=com_viaggio&task=accordoCreate&order_id=<?php echo $item->id; ?>" style="color: white">Создать accordo</a>
                            </button><br>
                        <?php } elseif ($item->accordo->status == 0) { ?>
                            2. Accordo создано: <?=$item->accordo->date_open?>
                            <a href="https://www.russiantour.com/ita/accordo?payment_id=<?php echo $manualpayments[0]->paymentID; ?>">ссылка  </a>
                            <a href="/administrator/index.php?option=com_viaggio&task=accordopdf&order_id=<?php echo $item->id; ?>">пересоздать  </a>
                            <br>
                        <?php } elseif ($item->accordo->status == 1) { ?>
                            3. Accordo подтверждено <?=$item->accordo->date_changed?>
                            ip:<?=$item->accordo->ip?> <br>
                        <?php } ?>
                    </td>
                    <!--вывод ссылок на скачивание pdf и отправку мыла-->
                    <td>

                        <div class="uk-text-center">  
                            <span class="uk-h3">
                                Договор <b><?php echo $item->id; ?></b>  от  <b><?php echo date('d/m/Y',$item->id); ?></b>  </span>
                            <!--    <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown">

                                         <li class="uk-nav-header">ООО "Русский тур"</li>
                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoR">Для физ. лиц Совком</a></li>
                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoRU">Для юри. лиц Совком</a></li>
                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoRT">Для физ. лиц Тинькофф</a></li>
                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoRTU">Для юри. лиц Тинькофф</a></li>
                                        <li class="uk-nav-header">ООО "Висто Руссиа"</li>
                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoV">Для физ. лиц Совком</a></li>

                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoVU">Для юри. лиц Совком</a></li>
                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoVT">Для физ. лиц Тинькофф</a></li>

                                        <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&order_id=<?=$item->id?>&template=printPdfContrattoVTU">Для юри. лиц Тинькофф</a></li>
                                        <li class="uk-nav-header">Edit</li> 
                                        <li><a target="_blank"  href="/components/com_viaggio/pdf/14/contratto<?=$item->id?>.pdf">Edit PDF</a></li>
										<li><a href="/administrator/index.php?option=com_viaggio&task=printdoc&order_id=<?php echo $item->id; ?>">Download DOC</a>
										<li><a href="/administrator/index.php?option=com_viaggio&task=generateBankPDF&order_id=<?php echo $item->id; ?>">Generate Bank PDF</a>
										<li><a href="/administrator/index.php?option=com_viaggio&task=downloadpdf&order_id=<?php echo $item->id; ?>">Download PDF</a>

                                    </ul>
                                </div> 
                            </div>-->		<?php
                            if (count($manualpayments))
                            {
                                echo '<div class="uk-h4 uk-text-center"> Сумма <b>';

                                echo $item->valute_amount.' €</b> оплачено ';
                                $left_amount = $item->valute_amount;
                                foreach ($manualpayments as $k=>$manualpayment)
                                {
                                    $left_amount -= $manualpayment->amountEuro;
                                    ?>
                                    <b><!-- <a type="button" uk-toggle="target: #offcanvas-flip status"> --><?php echo $manualpayment->amountEuro; ?> €   </b>
                                    ( Commissione  <?php echo $manualpayment->comissionEuro; ?> €)
                                    <?php
                                    if (count($manualpayments)-1 != $k)
                                        echo '  + ';
                                }
                                ?>

                                остаток <!-- <a target="_blank" href="https://www.russiantour.com/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&data=<?php echo $item->id.'|'.$left_amount; ?>">--><b><?php echo round($left_amount,2); ?> € </b>
                                <?php
                            }
                            ?></div>



                        <?php

                        foreach ($manualpayments as $manualpayment)
                        {
                            include ('/home/russiantour/web/russiantour.com/public_html/administrator/components/com_viaggio/includes/order_payments.php');
                        }
                        $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = '".$item->id."' AND status = 0 AND hidden <> 1 ORDER BY id DESC");
                        $notPayedManualpayments = $db->loadObjectList();
                        foreach ($notPayedManualpayments as $manualpayment)
                        {
                            include ('/home/russiantour/web/russiantour.com/public_html/administrator/components/com_viaggio/includes/order_payments.php');
                        }
                        ?>

 

                    </td>
                    <td>


 

                        <?php
                        foreach ($item->clients as $client)
                            echo '<a href="?option=com_viaggio&view=client&layout=edit&id='.$client->id.'">'.$client->nome.' '.$client->cognome.'</a>  </br>  '.date('d/m/Y',strtotime($client->birthday)).'   '.$client->numero_di_passaporto.'<br>';
                        ?>

                    </td>



                    <td> <?php
                        $user_groups = JAccess::getGroupsByUser($user->get('id'));
                        if (in_array(8,$user_groups))
                            echo $item->created_by;
                        else
                            echo '                    ';

                        if (!$item->created_by && (in_array(13,$user_groups) || in_array(8,$user_groups))){
                            echo '<select id="send_order" onchange="window.location.href = \'/administrator/index.php?option=com_viaggio&task=sendOrderToUser&order_id='.$item->id.'&user_id=\'+jQuery(this).val();">';
                            echo '<option value="0">отправить order</option>';
                            foreach (JAccess::getUsersByGroup(10) as $user_id)
                            {
                                $local_user = JFactory::getUser($user_id);
                                echo '<option value="'.$user_id.'">'.$local_user->name.'</option>';
                            }
                            echo '</select>';
                            ?>
                            <?php
                        }

                        ?>
                        <div id="offcanvas-flip<?php echo $item->id; ?>status" uk-offcanvas="flip: true; overlay: true">
                            <div class="uk-offcanvas-bar">

                                <button class="uk-offcanvas-close" type="button" uk-close></button>

                                <h3>Статус платежа</h3>

                                <?php
                                foreach ($manualpayments as $k=>$manualpayment)
                                {
                                    echo 'PaymentID: '.$manualpayment->paymentID.'; ';
                                    echo '<br>Curs: '.$manualpayment->curs.'; ';
                                    echo '<br>Date: '.$manualpayment->timeCreated.'; ';
                                    echo '<br>Amount: '.$manualpayment->amountEuro.'; ';
                                    echo '<br>Status: '.$manualpayment->status.'.<br/>';
                                    echo 'Сумма в рублях: '.$manualpayment->amountRub.'. руб<br/><br/>';
                                }
                                ?>
                            </div>
                        </div>
                        <div id="offcanvas-flip2" uk-offcanvas="flip: true; overlay: true">
                            <div class="uk-offcanvas-bar">
                                <button class="uk-offcanvas-close" type="button" uk-close></button>
                                Тут редактируеться клиенты которые поедут по туры.
                                <h3>Клиенты</h3>
                                <button class="uk-button uk-button-default" type="button" uk-toggle="target: #toggle-usage5">Фамииля Имя Клиента</button>
                                <div id="toggle-usage5" hidden >
                                    <fieldset class="adminform">
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_cognome-lbl" for="jform_cognome">Cognome</label>
                                            </div>
                                            <div class="controls"><input type="text" name="jform[cognome]" id="jform_cognome" value="" placeholder="Cognome"></div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_nome-lbl" for="jform_nome">Nome</label>
                                            </div>
                                            <div class="controls">
                                                <input type="text" name="jform[nome]" id="jform_nome" value="" placeholder="Nome">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_sex-lbl" for="jform_sex">Sex</label>
                                            </div>
                                            <div class="controls">
                                                <select id="jform_sex" name="jform[sex]" style="display: none;">
                                                    <option value="m">Maschile</option>
                                                    <option value="f">Femminile</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_birthday-lbl" for="jform_birthday">Birthday</label>
                                            </div>
                                            <div class="controls">
                                                <input type="text" id="jform_birthday" name="jform[birthday]" value="" class="inputbox" placeholder="Birthday" data-alt-value="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_nazionalita-lbl" for="jform_nazionalita">Nazionalita</label>
                                            </div>
                                            <div class="controls">
                                                <select id="jform_nazionalita" name="jform[nazionalita]" style="display: none;">
                                                    <option value="italiani">italiani</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_luogo_di_nascita-lbl" for="jform_luogo_di_nascita">citta_di_residenza</label>
                                            </div>
                                            <div class="controls">
                                                <input type="text" name="jform[luogo_di_nascita]" id="jform_luogo_di_nascita" value="">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_numero_di_passaporto-lbl" for="jform_numero_di_passaporto">numero_di_passaporto</label>
                                            </div>
                                            <div class="controls">
                                                <input type="text" name="jform[numero_di_passaporto]" id="jform_numero_di_passaporto" value="">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_date_from-lbl" for="jform_date_from" class="hasPopover" title="" data-content="=" data-original-title="date_from">passaporto_date_from</label>
                                            </div>
                                            <div class="controls">
                                                <input type="text" id="jform_date_from" name="jform[date_from]" value="" class="inputbox" data-alt-value="" autocomplete="off">
                                            </div>
                                        </div>
                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_date_to-lbl" for="jform_date_to" class="hasPopover" title="" data-content="=" data-original-title="date_to">passaporto_date_to</label>
                                            </div>
                                            <div class="controls">
                                                <input type="text" id="jform_date_to" name="jform[date_to]" value="" class="inputbox" data-alt-value="" autocomplete="off">
                                            </div>
                                        </div>
                                    </fieldset>
                                </div>
                            </div>
                        </div>
        </div>

        <?php
        $query = "SELECT * FROM `#__viaggio_manualpayments_details` WHERE payment_id = '".$manualpayments[0]->paymentID."'";
        $db->setQuery($query);
        $manualPaymentDetails = $db->loadObjectList();
        $details = '';
        foreach ($manualPaymentDetails as $manualPaymentDetail)
        {
            if ($manualPaymentDetail->field_value != '')
            {
                $prefix = '';
                $postfix = '';
                switch ($manualPaymentDetail->field_group_name) {
                    case 'air-ticket':
                        $prefix = 'Авиа - ';
                        break;
                    case 'rail-ticket':
                        $prefix = 'ЖД - ';
                        break;
                }
                switch ($manualPaymentDetail->field_name) {
                    case 'visa-ita':
                        $prefix .= 'Виза (ita): ';
                        break;
                    case 'visa-rus':
                        $prefix .= 'Виза (rus): ';
                        break;
                    case 'visa-amount':
                        $prefix .= 'Стоимость визы: ';
                        $postfix = '€';
                        break;
                    case 'other-ita':
                        $prefix .= 'Другое (ita): ';
                        break;
                    case 'other-rus':
                        $prefix .= 'Другое (rus): ';
                        break;
                    case 'other-amount':
                        $prefix .= 'Другое - стоимость: ';
                        $postfix = '€';
                        break;
                    case 'to-city-ita':
                        $prefix .= 'в город (ita): ';
                        break;
                    case 'from-city-ita':
                        $prefix .= 'из города (ita): ';
                        break;
                    case 'to-city-rus':
                        $prefix .= 'в город (rus): ';
                        break;
                    case 'from-city-rus':
                        $prefix .= 'из города (rus): ';
                        break;
                    case 'begin':
                        $prefix .= 'с: ';
                        break;
                    case 'end':
                        $prefix .= 'по: ';
                        break;
                    case 'amount':
                        $prefix .= 'стоимость: ';
                        $postfix = '€';
                        break;
                }

                $details .= $prefix.$manualPaymentDetail->field_value.$postfix.'<br/>';
            }
        }
        echo $details;
        ?>

        <?php
        if (count($manualpayments))
        {
            echo $item->valute_amount.' € = ';
            $left_amount = $item->valute_amount;
            foreach ($manualpayments as $k=>$manualpayment)
            {
                $left_amount -= $manualpayment->amountEuro;
                ?>
                <a type="button" uk-toggle="target: #offcanvas-flip status"> <?php echo $manualpayment->amountEuro; ?> €   </a>
                <?php
                if (count($manualpayments)-1 != $k)
                    echo ' + ';
            }
            ?>
            <br>
          
            <?php
        }
		
        ?>  <a   class="uk-button uk-button-primary"  target="_blank" href="https://www.russiantour.com/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&data=<?php echo $item->id; ?>">Создать платёж</a><br>

            <?php

            if (!$item->visa_id) { ?>
                <a class="uk-button uk-button-primary"   href="/administrator/index.php?option=com_viaggio&task=sendToVisto&order_id=<?php echo $item->id; ?>">Отправить на visto</a>
            <?php } else { ?>
                <?php foreach (explode(',',$item->visa_id) as $visa_id)  { ?>
                <a class="uk-button uk-button-success"  target="_blank" href="https://www.visto-russia.com/administrator/index.php?option=com_touristinvite&view=visaandinivte&layout=new&id=<?php echo $visa_id; ?>">Открыть visto-russia </a>
                <br/>
                <?php } ?>
                <a class="uk-button uk-button-primary"   href="/administrator/index.php?option=com_viaggio&task=sendToVisto&order_id=<?php echo $item->id; ?>">Обновить</a>
                <?php


            }

            ?>


            <!-- Сумма в рублях <?php echo ($item->amount/100); ?>₽</b> -->
            <br>
            <?php
            $date = explode(' ',$manualpayment->timeCreated);
            $date = explode('-',$date[0]);
            $str = array(
                'paymentID='.$manualpayment->paymentID,
                'mp_d='.$date[2],
                'mp_m='.$date[1],
                'mp_y='.$date[0],
                'order_id='.$manualpayment->order_id,
                'amountEur='.$manualpayment->amountEuro
            );

            echo '     <!--                  
							<div class="uk-button-dropdown" data-uk-dropdown="{mode:\'click\'}" aria-haspopup="true" aria-expanded="false">
                                <button class="uk-button uk-button-primary">Скачать Invoce <i class="uk-icon-caret-down"></i></button>
                                <div class="uk-dropdown uk-dropdown-bottom" aria-hidden="true" style="top: 30px; left: 0px;" tabindex="">
                                    <ul class="uk-nav uk-nav-dropdown">'; ?>
            <?php if (!$item->check_type || $item->check_type == 1) { ?>
                <li class="uk-nav-header">ООО "Русский тур"</li>
				 <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&paymentID=m<?php echo $item->id; ?>">new Софкомбанк</a></li>
                <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareR&<?php echo implode('&',$str); ?>">Софкомбанк</a></li>
                <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareRT&<?php echo implode('&',$str); ?>">Тинькофф</a></li>
                <li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare<?php echo $item->id; ?>.pdf">Edit pdf</a></li>
            <?php } ?>
            <?php if (!$item->check_type || $item->check_type == 2) { ?>
                <li class="uk-nav-header">ООО "Висто Руссиа"</li>
                <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareV&<?php echo implode('&',$str); ?>">СовкомБанк</a></li>
                <li><a target="_blank"  href="/administrator/index.php?option=com_viaggio&task=printpdf&template=printPdfPagareVT&<?php echo implode('&',$str); ?>">Тинькофф</a></li>
                <li><a target="_blank"  href="https://www.russiantour.com/components/com_viaggio/pdf/14/pagare<?php echo $item->id; ?>.pdf">Edit pdf</a></li>
            <?php } ?>
            </ul>
    </div>
    </div>-->

<?php
						                            $user_groups = JAccess::getGroupsByUser($user->get('id'));
                            if (in_array(8,$user_groups))
                            {
                                if ($item->status == 0)
                                    
echo '<a href="/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit&id='.$item->id.'"> edit
                        </a> </b> <br><a href="?option=com_viaggio&task=hideManualPayment&id='.$item->id.'">
                            Удалить
                        </a> </b><br>';
                                else
                                    echo '';
                            }
						echo ' <a href="?option=com_viaggio&task=hideManualPayment&id='.$item->id.'">
                          ,
                        </a> </b>  '
						?>






    </td>


    <td>
        <?php

        $query = "SELECT * FROM `#__viaggio_orders_relations` WHERE order_id = '".$item->id."'";

        $db->setQuery($query);
        $relations = $db->loadObjectList();
        $summRub = 0;
        $summEuro = 0;
        foreach ($relations as $relation)
        {
            $summRub += $relation->amount_rub/100;
            $summEuro += $relation->amount_euro/100;

            echo ' <div id="toggle'.$item->id.'" class="uk-panel uk-panel-box uk-hidden" aria-hidden="false">
								 
								 Услуга: <b> '.$services[$relation->service_id]->name_rus.'</b> <br> ';
            echo ' Партнёр: <b> '.$partners[$relation->partner_id]->name_rus.'</b><br>  ';
            echo '<b> '.(($relation->amount_rub/100!='')?(''.$relation->amount_rub/100):' € ').'</b>     	  ';
            echo '<b> '.(($relation->amount_euro/100!='')?(''.$relation->amount_euro/100):' ₽  ').'   </b>  	 ';
            echo ' <a href="#relation_amount" data-uk-modal onclick="change_relation_amount('.$relation->id.','.$relation->amount_rub.','.$relation->amount_euro.')"><span class="uk-num">Изменить</span></a> </div>';
        }
        if ($summEuro + $summRub)
        {
            echo '<hr>';
            echo '
<a class="uk-button uk-button-primary" data-uk-toggle="{target:\'#toggle'.$item->id.' \'}">Сколько должны партнером <br>Всего: <b>'.$summEuro.' €</b>, <b>'.$summRub.' ₽</b></a></p>
								';
        }
        ?>



    </td>
    <?php echo '<td>'; include 'includes/last_payment.php'; echo '</td>'; ?>
    </tr>
<?php endforeach; ?>
    </tbody>
    </table>

    </div>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="boxchecked" value="0"/>
    <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
    <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
    <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
<!-- This is the modal -->
<div id="relation_amount" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        Изменить сумму<br/>
        <form method="post" action="index.php?option=com_viaggio&task=changerelationamount">
            <input type="hidden" name="id" id="changerelationamount_id" value="">
            <div style="display: none"><input type="text" name="amount_rub" id="changerelationamount_amount_rub" value=""> ₽<br/></div>
            <div style="display: none"><input type="text" name="amount_euro" id="changerelationamount_amount_euro" value=""> €<br/></div>
            <input type="submit" value="Изменить">
        </form>
    </div>
</div>
<script>
    function change_relation_amount(id,amount_rub,amount_euro) {
        jQuery('#changerelationamount_id').val(id);
        jQuery('#changerelationamount_amount_rub').val(amount_rub/100);
        jQuery('#changerelationamount_amount_euro').val(amount_euro/100);
        if (amount_rub > 0)
        {
            jQuery('#changerelationamount_amount_rub').parent().show();
            jQuery('#changerelationamount_amount_euro').parent().hide();
        }
        else if (amount_euro > 0)
        {
            jQuery('#changerelationamount_amount_rub').parent().hide();
            jQuery('#changerelationamount_amount_euro').parent().show();
        }
    }
    var saveID = '';
    jQuery(document).ready(function() {
        jQuery('.form-data-send').on('click',function () {
            if (jQuery('#form-mail').val()=='') {
                if (jQuery('#form-mail').next('.error').length < 1)
                    jQuery('#form-mail').after('<div class="error" style="color:red;"><?php echo JText::_('COM_RUSSIANTOUR_REQUIRED'); ?></div>');
                return false;
            }
            else if (!/[^@]+@[^.]+\..+/.test(jQuery('#form-mail').val())) {
                if (jQuery('#form-mail').next('.error').length < 1)
                    jQuery('#form-mail').after('<div class="error" style="color:red;">wrong email!</div>');
                return false;
            }
            jQuery('#form-mail').next('.error').remove();
            jQuery('.form-data-send').after('sending...');
            jQuery('.form-data-send').hide();
            jQuery.post( 'index.php?option=com_viaggio&task=sendForm', jQuery( '#form-to-send' ).serialize()).done(function( data ) {
                document.location.reload();
            });
        });
    });
</script>
<div id="modal" class="uk-modal uk-open" style="overflow-y: scroll; display: none; padding-left: 0px;">
    <div class="uk-modal-dialog">
        <button class="uk-modal-close uk-close" type="button" onclick="jQuery('#modal').modal('hide')"></button>
        <div class="uk-modal-header">
            <h2>Richiesta di maggiori informazioni</h2>
        </div>
        <form id="form-to-send" class="uk-form uk-form-horizontal">
            <input id="generated-id" name="generated-id" type="hidden" value="ita-mosca-sanpietroburgo-4">
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-it">Maggiori informazioni</label>
                <div class="uk-form-controls">
                    <input id="form-mail" name="form-mail" required="" type="text" autofocus="" placeholder="Indirizzo e-mail">
                    <br>
                    <input id="form-phone" name="form-phone" type="text" placeholder="Telefono">
                </div>
            </div>
            <div class="uk-form-row"><label class="uk-form-label" for="form-h-it">Nome e cognome</label>
                <div class="uk-form-controls">
                    <input id="form-cognome" name="form-cognome" type="text" placeholder="Nome e cognome ">
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-s">Numero di persone</label>
                <div class="uk-form-controls">
                    <select id="form-personas" name="form-personas">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label">Категория тура</label>
                <div class="uk-form-controls">
                    <select id="tour-cat">
                        <option value=0> --- Выберите категорию --- </option>
                    <?php
                    foreach ($trips_categorys as $cat)
                    {
                        echo '<option value="'.$cat->id.'">'.$cat->title_ita.'</option>';
                    }
                    ?>
                    </select>
                </div>
            </div>
            <div class="uk-form-row choose_tour">
                <label class="uk-form-label">Тур</label>
                <div class="uk-form-controls">
                    <select id="tour">
                        <option value="" class="const"> --- Выберите тур --- </option>
                    </select>
                </div>
            </div>
            <div class="uk-form-row">
                <label class="uk-form-label" for="form-h-t">Commenti</label>
                <div class="uk-form-controls">&nbsp;
                    <textarea id="form-comments" name="form-comments"></textarea>
                </div>
            </div>
            <input type="hidden" name="trip_id" value="0">
        </form>
        <div class="uk-modal-footer uk-text-right">
            <button id="ita-mosca-sanpietroburgo-4" class="uk-button uk-button-primary form-data-send" type="button">Invia</button>
        </div>
    </div>
</div>
<div id="payedPartial-modal" class="uk-modal">
    <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <form method="get" action="">
            Сумма в евро: <input id="payedPartial_summ-in-euro" name="summ_in_euro">
            <br/>
            <input type="hidden" name="payment_id" id="payedPartial_payment_id">
            <input type="hidden" name="order_id" id="payedPartial_order_id">
            <input type="hidden" name="bank" id="payedPartial_bank">
            <input type="hidden" name="check_type" id="payedPartial_check_type">
            <input type="hidden" name="option" value="com_viaggio">
            <input type="hidden" name="task" value="payedPartial">
            <input type="submit" value="Изменить">
        </form>
    </div>
</div>
<script>
    function makeCheck(id){
        window.location.href='?option=com_viaggio&task=makeCheck&payment_id='+id+'&amount='+jQuery('#amount'+id).val();
    }
</script>
<style>
    .modal-backdrop {
        opacity: 0;
        z-index: 1;
    }
    .choose_tour {
        display: none;
    }
    .uk-comment-header.status0 {
        background-color: #efefef !important;
		text-align: center;
    }
</style>
<script>
    jQuery(document).ready(function() {
        jQuery('#tour-cat').on('change',function () {
            jQuery('input[name=trip_id]').val(0);
            jQuery('#tour').find('option[class!=const]').remove();
            let id = jQuery(this).val();
            if (id > 0)
            {
                jQuery('.choose_tour').show();
                jQuery.getJSON('https://www.russiantour.com/eng/?option=com_viaggio&task=tripsByCategory&key=2VKAFw6iw8ebVH2Rr1eE&cat_id='+id,function(data){
                    jQuery.each(data.data, function(id, name_rus) {
                        jQuery('#tour').append('<option value="'+id+'">'+name_rus+'</option>');
                    });
                    jQuery('#tour').trigger("liszt:updated");
                });
            }
            else
                jQuery('.choose_tour').hide();
        });
        jQuery('#tour').on('change',function(){
            jQuery('input[name=trip_id]').val(jQuery(this).val());
        });
    });
</script>