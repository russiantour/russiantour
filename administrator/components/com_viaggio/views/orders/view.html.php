<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Viaggio.
 *
 * @since  1.6
 */
class ViaggioViewOrders extends JViewLegacy
{
    protected $items;

    protected $pagination;

    protected $state;

    /**
     * Display the view
     *
     * @param   string  $tpl  Template name
     *
     * @return void
     *
     * @throws Exception
     */
    public function display($tpl = null)
    {
        $this->state = $this->get('State');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            throw new Exception(implode("\n", $errors));
        }

        ViaggioHelpersViaggio::addSubmenu('orders');

        $this->addToolbar();

        $this->sidebar = JHtmlSidebar::render();
        parent::display($tpl);
    }

    /**
     * Add the page title and toolbar.
     *
     * @return void
     *
     * @since    1.6
     */
    protected function addToolbar()
    {
        //JToolBarHelper::editList('order.edit', 'JTOOLBAR_EDIT');

        $state = $this->get('State');
        $canDo = ViaggioHelpersViaggio::getActions();

        JToolBarHelper::title(JText::_('COM_VIAGGIO_TITLE_ORDERS'), 'orders.png');

        // Check if the form exists before showing the add/edit buttons
        $formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/order';

        if (file_exists($formPath))
        {
            /*if ($canDo->get('core.create'))
            {
                JToolBarHelper::addNew('order.add', 'JTOOLBAR_NEW');

                if (isset($this->items[0]))
                {
                    JToolbarHelper::custom('orders.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
                }
            }*/

            if ($canDo->get('core.edit') && isset($this->items[0]))
            {
                JToolBarHelper::editList('order.edit', 'JTOOLBAR_EDIT');
            }
        }

        if ($canDo->get('core.edit.state'))
        {
            if (isset($this->items[0]->state))
            {
                JToolBarHelper::divider();
                JToolBarHelper::custom('orders.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
                JToolBarHelper::custom('orders.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
            }
            elseif (isset($this->items[0]))
            {
                // If this component does not use state then show a direct delete button as we can not trash
                JToolBarHelper::deleteList('', 'orders.delete', 'JTOOLBAR_DELETE');
            }

            if (isset($this->items[0]->state))
            {
                JToolBarHelper::divider();
                JToolBarHelper::archiveList('orders.archive', 'JTOOLBAR_ARCHIVE');
            }

            if (isset($this->items[0]->checked_out))
            {
                JToolBarHelper::custom('orders.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
            }
        }
        JToolBarHelper::modal('modal','','Создать заявку');
        JToolBarHelper::link('/administrator/index.php?option=com_viaggio&view=manualpayment&layout=edit','Создать manualpayment','manualpayment_create');

        // Show trash and delete for components that uses the state field
        if (isset($this->items[0]->state))
        {
            if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
            {
                JToolBarHelper::deleteList('', 'orders.delete', 'JTOOLBAR_EMPTY_TRASH');
                JToolBarHelper::divider();
            }
            elseif ($canDo->get('core.edit.state'))
            {
                JToolBarHelper::trash('orders.trash', 'JTOOLBAR_TRASH');
                JToolBarHelper::divider();
            }
        }

        if ($canDo->get('core.admin'))
        {
            JToolBarHelper::preferences('com_viaggio');
        }

        // Set sidebar action - New in 3.0
        JHtmlSidebar::setAction('index.php?option=com_viaggio&view=orders');

        $this->extra_sidebar = '';

        //Filter by user
        $select_label = 'By user';
        $options = array();
        $db = JFactory::getDbo();
        $db->setQuery("SELECT u.id value, u.name text FROM #__users u JOIN #__user_usergroup_map g ON g.user_id = u.id WHERE g.group_id = 10 ORDER BY u.name ASC");
        $users = $db->loadObjectList();
        foreach ($users as $k=>$user)
        {
            $options[$k] = $user;
        }
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_user',
            JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.user'), true)
        );
        //Filter for the field currency
        $select_label = JText::sprintf('COM_VIAGGIO_FILTER_SELECT_LABEL', 'Currency');
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "643";
        $options[0]->text = "rub";
        $options[1] = new stdClass();
        $options[1]->value = "840";
        $options[1]->text = "usd";
        $options[2] = new stdClass();
        $options[2]->value = "978";
        $options[2]->text = "eur";
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_currency',
            JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.currency'), true)
        );

        //Filter for the field orderstatus
        $select_label = JText::sprintf('COM_VIAGGIO_FILTER_SELECT_LABEL', 'Orderstatus');
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "0";
        $options[0]->text = "Заказ зарегистрирован, но не оплачен";
        $options[1] = new stdClass();
        $options[1]->value = "1";
        $options[1]->text = "Предавторизованная сумма захолдирована (для двухстадийных платежей)";
        $options[2] = new stdClass();
        $options[2]->value = "2";
        $options[2]->text = "Проведена полная авторизация суммы заказа";
        $options[3] = new stdClass();
        $options[3]->value = "3";
        $options[3]->text = "Авторизация отменена";
        $options[4] = new stdClass();
        $options[4]->value = "4";
        $options[4]->text = "По транзакции была проведена операция возврата";
        $options[5] = new stdClass();
        $options[5]->value = "5";
        $options[5]->text = "Инициирована авторизация через ACS банка-эмитента";
        $options[6] = new stdClass();
        $options[6]->value = "6";
        $options[6]->text = "Авторизация отклонена";
        JHtmlSidebar::addFilter(
            $select_label,
            'filter_orderstatus',
            JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.orderstatus'), true)
        );

        //Filter for payment status
        $select_label = 'Статус платежа';
        $options = array();
        $options[0] = new stdClass();
        $options[0]->value = "a";
        $options[0]->text = "Все";
        $options[1] = new stdClass();
        $options[1]->value = "p";
        $options[1]->text = "Частично оплаченные";
        $options[2] = new stdClass();
        $options[2]->value = "f";
        $options[2]->text = "Полностью оплаченные";
        $options[3] = new stdClass();
        $options[3]->value = "n";
        $options[3]->text = "Не оплаченные";

        JHtmlSidebar::addFilter(
            $select_label,
            'filter_paymentstatus',
            JHtml::_('select.options', $options , "value", "text", $this->state->get('filter.paymentstatus'), true)
        );

    }

    /**
     * Method to order fields
     *
     * @return void
     */
    protected function getSortFields()
    {
        return array(
            'a.`id`' => JText::_('JGRID_HEADING_ID'),
            'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
            'a.`created_by`' => JText::_('COM_VIAGGIO_ORDERS_CREATED_BY'),
            'a.`currency`' => JText::_('COM_VIAGGIO_ORDERS_CURRENCY'),
            'a.`orderstatus`' => JText::_('COM_VIAGGIO_ORDERS_ORDERSTATUS'),
            'a.`errorcode`' => JText::_('COM_VIAGGIO_ORDERS_ERRORCODE'),
            'a.`errormessage`' => JText::_('COM_VIAGGIO_ORDERS_ERRORMESSAGE'),
            'a.`amount`' => JText::_('COM_VIAGGIO_ORDERS_AMOUNT'),
            'a.`tour_id`' => JText::_('COM_VIAGGIO_ORDERS_TOUR_ID'),
            'a.`clientcount`' => JText::_('COM_VIAGGIO_ORDERS_CLIENTCOUNT'),
            'a.`insurance`' => JText::_('COM_VIAGGIO_ORDERS_INSURANCE'),
            'a.`stell`' => JText::_('COM_VIAGGIO_ORDERS_STELL'),
            'a.`telephone`' => JText::_('COM_VIAGGIO_ORDERS_TELEPHONE'),
            'a.`description`' => JText::_('COM_VIAGGIO_ORDERS_DESCRIPTION'),
            'a.`pole1`' => JText::_('COM_VIAGGIO_ORDERS_POLE1'),

            'a.`email`' => JText::_('COM_VIAGGIO_ORDERS_EMAIL'),
        );
    }
}
