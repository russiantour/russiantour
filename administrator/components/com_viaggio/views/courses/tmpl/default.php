 
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">

<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>

<?php
$db = JFactory::getDbo();
$db->setQuery("SELECT * FROM #__viaggio_course ORDER BY date DESC");
$courses = $db->loadObjectList();
?>
<form method="post" action="?option=com_viaggio&task=courses">
    <input name="id" value="0" type="hidden">
    Дата: <input name="date">
    Курс: <input name="amount">
    <input type="submit" value="Добавить" onclick="return validate(this)">
    Дата должна быть в формате ГГГГ-ММ (Например: 2019-01)
    <span style="color: red;" class="error"></span>
</form><hr/>
<?php foreach ($courses as $course) {
    $date = explode('-',$course->date);
    ?>
    <form method="post" action="?option=com_viaggio&task=courses">
        <input name="id" value="<?php echo $course->id; ?>" type="hidden">
        Дата: <input name="date" value="<?php echo $date[0].'-'.$date[1]; ?>">
        Курс: <input name="amount" value="<?php echo $course->amount/100; ?>">
        <input type="submit" value="Изменить"  onclick="return validate(this)">
        <span style="color: red;" class="error"></span>
    </form><br/>
    <?php
}
?>
<script>
    function validate(t) {
        var form = jQuery(t).parent();
        jQuery(form).find('.error').html('');
        var date = jQuery(form).find('input[name=date]').val();
        if (!/^\d+-\d+$/.test(jQuery(form).find('input[name=date]').val()))
        {
            jQuery(form).find('.error').html('Неверный формат даты!');
            return false;
        }
        var count = 0;
        jQuery('input[name=date]').each(function(){
            if (jQuery(this).val()==date)
                count++;
        });
        if (count > 1)
        {
            jQuery(form).find('.error').html('Даты не могут повторяться!');
            return false;
        }
        var amount = parseFloat(jQuery(form).find('input[name=amount]').val().replace(',','.'));
        if (!amount)
        {
            jQuery(form).find('.error').html('Неверная сумма!');
            return false;
        }
        jQuery(form).find('input[name=amount]').val(amount);
        return true;
    }
</script>