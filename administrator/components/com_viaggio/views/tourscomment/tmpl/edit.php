<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');
?>

<script type="text/javascript">
    Joomla.submitbutton = function (task) {
        if (task == 'tourscomment.cancel') {
            Joomla.submitform(task, document.getElementById('tourscomment-form'));
        }
        else {

            if (task != 'tourscomment.cancel' && document.formvalidator.isValid(document.id('tourscomment-form'))) {

                Joomla.submitform(task, document.getElementById('tourscomment-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<form
        action="<?php echo JRoute::_('index.php?option=com_viaggio&view=tourscomment&layout=edit&id=' . (int) $this->item->id); ?>"
        method="post" enctype="multipart/form-data" name="adminForm" id="tourscomment-form" class="form-validate">

    <div class="form-horizontal">
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <?php echo $this->form->renderField('id'); ?>
                    <?php echo $this->form->renderField('tour_id'); ?>
                    <?php echo $this->form->renderField('author_name'); ?>
                    <?php echo $this->form->renderField('author_email'); ?>
                    <?php echo $this->form->renderField('text'); ?>
                    <?php echo $this->form->renderField('date'); ?>
                </fieldset>
            </div>
        </div>
        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>