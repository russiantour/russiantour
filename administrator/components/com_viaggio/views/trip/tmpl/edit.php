<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright
 * @license
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet(JUri::root() . 'media/com_viaggio/css/form.css');

$db    = JFactory::getDbo();

$query = $db->getQuery(true);
$query->select('*');
$query->from('#__viaggio_trips_categorys');
$query->where('state is null or state <> -2');
//$query->order('CAST(title_ita AS UNSIGNED) asc, title_ita ASC');
$query->order('ordering ASC');
$db->setQuery($query);
$trips_categorys = $db->loadObjectList();
?>

<script type="text/javascript">
    Joomla.submitbutton = function (task) {
        if (task == 'trip.cancel') {
            Joomla.submitform(task, document.getElementById('trip-form'));
        }
        else {

            if (task != 'trip.cancel' && document.formvalidator.isValid(document.id('trip-form'))) {

                Joomla.submitform(task, document.getElementById('trip-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>
<link rel="stylesheet" href="/images/css/2/css/uikit.almost-flat.css">
<link rel="stylesheet" href="/images/css/2/css/components/tooltip.css">
<script type="text/javascript"   src="/images/css/2/js/uikit.min.js"></script>
<form
        action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>"
        method="post" enctype="multipart/form-data" name="adminForm" id="trip-form" class="form-validate">

    <div class="form-horizontal">
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">
                    <?php echo $this->form->renderField('id'); ?>
                    <?php echo $this->form->renderField('name_rus'); ?>
                    <?php echo $this->form->renderField('name_ita'); ?>
                    <?php echo $this->form->renderField('date_from'); ?>
                    <?php echo $this->form->renderField('date_to'); ?>
                    <?php //echo $this->form->renderField('cat_id'); ?>

                    <div class="control-group">
                        <label class="control-label">Категория тура</label>
                        <div class="controls">
                            <select id="parent-cat">
                                <option value=0> --- Выберите категорию --- </option>
                                <?php
                                $parent_id = false;
                                foreach ($trips_categorys as $cat)
                                {
                                    if ($this->item->cat_id && $this->item->cat_id == $cat->id)
                                        $parent_id = $cat->parent_id;
                                }
                                foreach ($trips_categorys as $cat)
                                {
                                    if ($cat->parent_id)
                                        continue;
                                    echo '<option value="'.$cat->id.'" '.(($this->item->cat_id && $parent_id == $cat->id)?'selected':'').'>'.$cat->title_ita.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="controls tour-cat-parent" <?php if (!$this->item->cat_id) {echo 'style="display: none;"';} ?>>
                            <select id="tour-cat" name="jform[cat_id]">
                                <option value=0 class="const"> --- Выберите сабкатегорию --- </option>
                                <?php
                                foreach ($trips_categorys as $cat)
                                {
                                    if (!$cat->parent_id)
                                        continue;
                                    echo '<option '.((!$this->item->cat_id || $parent_id != $cat->parent_id)?'style="display: none;"':'').' parent_id = "'.$cat->parent_id.'" value="'.$cat->id.'" '.(($this->item->cat_id == $cat->id)?'selected':'').'>'.$cat->title_ita.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <a href="/administrator/index.php?option=com_viaggio&view=tripscategorys">Категории</a><br/>
                    <!-- HOTELS EDIT BEGIN -->
                    <hr/>
                    Отели и города:
                    <?php
                    //same in order and trip edit
                    $db    = JFactory::getDbo();

                    $query = $db->getQuery(true);
                    $query->select('*');
                    $query->from('#__viaggio_city');
                    $query->order('name_rus asc');
                    $db->setQuery($query);
                    $cities = $db->loadObjectList();

                    if (isset($_GET['id']) && intval($_GET['id']))
                    {
                        $query = $db->getQuery(true);
                        $query->select('a.*, hotel.city_id');
                        $query->from('#__viaggio_trips_hotels a');//difference between trips and orders
                        $query->join('LEFT','#__viaggio_hotels AS `hotel` ON `hotel`.id = a.`hotel_id`');
                        $query->where('trip_id = '.intval($_GET['id']));
                        $db->setQuery($query);
                        $hotels = $db->loadObjectList();

                        $query = $db->getQuery(true);
                        $query->select('*');
                        $query->from('#__viaggio_trips_cities');//difference between trips and orders
                        $query->where('trip_id = '.intval($_GET['id']));//difference between trips and orders
                        $db->setQuery($query);
                        $trip_cities = $db->loadObjectList();
                    }

                    $query = $db->getQuery(true);
                    $query->select('*');
                    $query->from('#__viaggio_hotels');
                    $db->setQuery($query);
                    $all_hotels = $db->loadObjectList();

                    foreach ($hotels as $hotel)
                    {
                        ?>
                        <div class='hotel-row'>
                            Город:
                            <select class='hotel-city-id' name="city_id[]" onchange="filterHotels();">
                                <option value='0'> -- Выберите город -- </option>
                                <?php
                                foreach ($cities as $city) {
                                    echo "<option value='" . $city->id . "' " . (($city->id == $hotel->city_id) ? 'selected' : '') . ">" . $city->name_rus . "</option>";
                                }
                                ?>
                            </select>

                            Отель:
                            <select class='hotel-id' name="hotel_id[]" hotel_id="<?php echo $hotel->hotel_id; ?>">
                                <option value="0"> -- Выберите отель -- </option>
                            </select>
                            <div style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()">Удалить запись</div>
                        </div>
                        <?php
                    }

                    foreach ($trip_cities as $trip_city)
                    {
                        ?>
                        <div class='hotel-row'>
                            Город:
                            <select class='hotel-city-id' name="city_id[]" onchange="filterHotels();">
                                <option value='0'> -- Выберите город -- </option>
                                <?php
                                foreach ($cities as $city) {
                                    echo "<option value='" . $city->id . "' " . (($city->id == $trip_city->city_id) ? 'selected' : '') . ">" . $city->name_rus . "</option>";
                                }
                                ?>
                            </select>

                            Отель:
                            <select class='hotel-id' name="hotel_id[]" hotel_id="0">
                                <option value="0"> -- Выберите отель -- </option>
                            </select>
                            <div class="uk-icon-trash uk-icon-medium" style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()"></div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="add-hotel" style="cursor: pointer" onclick="addHotel()">Добавить запись</div>
                    <div class='hotel-row-empty' style="display: none">
                        Город:
                        <select class='hotel-city-id' name="city_id[]" onchange="filterHotels();">
                            <option value='0'> -- Выберите город -- </option>
                            <?php
                            foreach ($cities as $city) {
                                echo "<option value='" . $city->id . "'>" . $city->name_rus . "</option>";
                            }
                            ?>
                        </select>

                        Отель:
                        <select class='hotel-id' name="hotel_id[]" hotel_id="0">
                            <option value="0"> -- Выберите отель -- </option>
                            <?php
                            foreach ($all_hotels as $all_hotel)
                            {
                                echo "<option city='".$all_hotel->city_id."' value='".$all_hotel->id."'>".$all_hotel->hotel_name_rus."</option>";
                            }
                            ?>
                        </select>
                        <div style="cursor: pointer;display: inline-block;" onclick="jQuery(this).parent().remove()">Удалить запись</div>
                    </div>
                    <script>
                        function addHotel()
                        {
                            jQuery('.add-hotel').before(jQuery('.hotel-row-empty').clone());//clone template hotel block
                            jQuery('.hotel-row-empty:first').addClass('hotel-row').removeClass('hotel-row-empty');//change class of cloned block
                            jQuery('.hotel-row:last select.hotel-id option').remove();//clear hotels list
                            jQuery('.hotel-row:last').show();//show cloned block
                            //remove old chosen block and generate new
                            jQuery('.hotel-row:last select').each(function () {
                                jQuery(this).next().remove();
                                jQuery(this).chosen();
                            });
                        }
                        function filterHotels()
                        {
                            //fill hotels list according to chosen city and stored in DB hotel
                            jQuery('.hotel-row .hotel-id').each(function () {
                                var current_select = jQuery(this);
                                let city_id = current_select.parent().find('.hotel-city-id').val();

                                //clear option list
                                current_select.find('option').remove();

                                //fill hotels list according to chosen city
                                jQuery('.hotel-row-empty option[city='+city_id+']').each(function () {
                                    let current_option = jQuery(this).clone();
                                    current_select.append(current_option);
                                });

                                //select stored in DB hotel
                                current_select.val(current_select.attr('hotel_id'));

                                //update chosen
                                current_select.trigger("liszt:updated");
                            });
                        }
                        jQuery(document).ready(function() {
                            filterHotels();
                        });
                    </script>
                    <!-- HOTELS EDIT END -->
                </fieldset>
            </div>
        </div>
        <input type="hidden" name="task" value=""/>
        <?php echo JHtml::_('form.token'); ?>
    </div>
</form>
<script>
    jQuery(document).ready(function() {
        jQuery('#parent-cat').on('change',function(){
            jQuery('#tour-cat').val(0);
            jQuery('#tour-cat').find('option[class!=const]').remove();
            let id = jQuery(this).val();

            if (id > 0)
            {
                jQuery('.tour-cat-parent').show();
                jQuery.getJSON('/eng/?option=com_viaggio&task=tripsSubcategories&key=2VKAFw6iw8ebVH2Rr1eE&id='+id,function(data){
                    jQuery.each(data.data, function(id, title_ita) {
                        jQuery('#tour-cat').append('<option value="'+id+'">'+title_ita+'</option>');
                    });
                    jQuery('#tour-cat').trigger("liszt:updated");
                });
            }
            else
                jQuery('.tour-cat-parent').hide();
        });
    });
</script>