<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Viaggio.
 *
 * @since  1.6
 */
class ViaggioViewTours extends JViewLegacy
{
	protected $items;

	protected $pagination;

	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		ViaggioHelpersViaggio::addSubmenu('tours');

		$this->addToolbar();

		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return void
	 *
	 * @since    1.6
	 */
	protected function addToolbar()
	{
		$state = $this->get('State');
		$canDo = ViaggioHelpersViaggio::getActions();

		JToolBarHelper::title(JText::_('COM_VIAGGIO_TITLE_TOURS'), 'tours.png');

		// Check if the form exists before showing the add/edit buttons
		$formPath = JPATH_COMPONENT_ADMINISTRATOR . '/views/tour';

		if (file_exists($formPath))
		{
			if ($canDo->get('core.create'))
			{
				JToolBarHelper::addNew('tour.add', 'JTOOLBAR_NEW');

				if (isset($this->items[0]))
				{
					JToolbarHelper::custom('tours.duplicate', 'copy.png', 'copy_f2.png', 'JTOOLBAR_DUPLICATE', true);
				}
			}

			if ($canDo->get('core.edit') && isset($this->items[0]))
			{
				JToolBarHelper::editList('tour.edit', 'JTOOLBAR_EDIT');
			}
		}

		if ($canDo->get('core.edit.state'))
		{
			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::custom('tours.publish', 'publish.png', 'publish_f2.png', 'JTOOLBAR_PUBLISH', true);
				JToolBarHelper::custom('tours.unpublish', 'unpublish.png', 'unpublish_f2.png', 'JTOOLBAR_UNPUBLISH', true);
			}
			elseif (isset($this->items[0]))
			{
				// If this component does not use state then show a direct delete button as we can not trash
				JToolBarHelper::deleteList('', 'tours.delete', 'JTOOLBAR_DELETE');
			}

			if (isset($this->items[0]->state))
			{
				JToolBarHelper::divider();
				JToolBarHelper::archiveList('tours.archive', 'JTOOLBAR_ARCHIVE');
			}

			if (isset($this->items[0]->checked_out))
			{
				JToolBarHelper::custom('tours.checkin', 'checkin.png', 'checkin_f2.png', 'JTOOLBAR_CHECKIN', true);
			}
		}

		// Show trash and delete for components that uses the state field
		if (isset($this->items[0]->state))
		{
			if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
			{
				JToolBarHelper::deleteList('', 'tours.delete', 'JTOOLBAR_EMPTY_TRASH');
				JToolBarHelper::divider();
			}
			elseif ($canDo->get('core.edit.state'))
			{
				JToolBarHelper::trash('tours.trash', 'JTOOLBAR_TRASH');
				JToolBarHelper::divider();
			}
		}

		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::preferences('com_viaggio');
		}

		// Set sidebar action - New in 3.0
		JHtmlSidebar::setAction('index.php?option=com_viaggio&view=tours');

		$this->extra_sidebar = '';
		JHtmlSidebar::addFilter(

			JText::_('JOPTION_SELECT_PUBLISHED'),

			'filter_published',

			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), "value", "text", $this->state->get('filter.state'), true)

		);
		JHtmlSidebar::addFilter(
			JText::_("JOPTION_SELECT_CATEGORY"),
			'filter_category',
			JHtml::_('select.options', JHtml::_('category.options', 'com_viaggio'), "value", "text", $this->state->get('filter.category'))

		);

			//Filter for the field from
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_from">'. JText::sprintf('COM_VIAGGIO_FROM_FILTER', 'Дата поездки: от') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.from.from'), 'filter_from_from', 'filter_from_from', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_from">'. JText::sprintf('COM_VIAGGIO_TO_FILTER', 'Дата поездки: от') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.from.to'), 'filter_to_from', 'filter_to_from', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

			//Filter for the field to
		$this->extra_sidebar .= '<div class="other-filters">';
			$this->extra_sidebar .= '<small><label for="filter_from_to">'. JText::sprintf('COM_VIAGGIO_FROM_FILTER', 'ToДата поездки: до') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.to.from'), 'filter_from_to', 'filter_from_to', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange' => 'this.form.submit();'));
			$this->extra_sidebar .= '<small><label for="filter_to_to">'. JText::sprintf('COM_VIAGGIO_TO_FILTER', 'ToДата поездки: до') .'</label></small>';
			$this->extra_sidebar .= JHtml::_('calendar', $this->state->get('filter.to.to'), 'filter_to_to', 'filter_to_to', '%Y-%m-%d', array('style' => 'width:142px;', 'onchange'=> 'this.form.submit();'));
		$this->extra_sidebar .= '</div>';
			$this->extra_sidebar .= '<hr class="hr-condensed">';

	}

	/**
	 * Method to order fields 
	 *
	 * @return void 
	 */
	protected function getSortFields()
	{
		return array(
			'a.`id`' => JText::_('JGRID_HEADING_ID'),
			'a.`ordering`' => JText::_('JGRID_HEADING_ORDERING'),
			'a.`state`' => JText::_('JSTATUS'),
			'a.`name`' => JText::_('COM_VIAGGIO_TOURS_NAME'),
			'a.`category`' => JText::_('COM_VIAGGIO_TOURS_CATEGORY'),
			'a.`category`' => JText::_('COM_VIAGGIO_TOURS_MANUALPAYMENTS'),
			'a.`from`' => JText::_('COM_VIAGGIO_TOURS_FROM'),
			'a.`to`' => JText::_('COM_VIAGGIO_TOURS_TO'),
			'a.`inn4count`' => JText::_('COM_VIAGGIO_TOURS_INN4COUNT'),
			'a.`inn3count`' => JText::_('COM_VIAGGIO_TOURS_INN3COUNT'),
			'a.`inn4cost`' => JText::_('COM_VIAGGIO_TOURS_INN4COST'),
			'a.`inn3cost`' => JText::_('COM_VIAGGIO_TOURS_INN3COST'),
			'a.`halfboard4`' => JText::_('COM_VIAGGIO_TOURS_HALFBOARD4'),
			'a.`halfboard3`' => JText::_('COM_VIAGGIO_TOURS_HALFBOARD3'),
			'a.`singola4`' => JText::_('COM_VIAGGIO_TOURS_SINGOLA4'),
			'a.`singola3`' => JText::_('COM_VIAGGIO_TOURS_SINGOLA3'),
		);
	}
}
