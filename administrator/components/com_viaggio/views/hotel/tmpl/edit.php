<?php
/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.keepalive');

// Import CSS
$document = JFactory::getDocument();
$document->addStyleSheet('components/com_viaggio/assets/css/touristinvite.css');
?>
<script type="text/javascript">
    js = jQuery.noConflict();
    js(document).ready(function() {
        
    });

    Joomla.submitbutton = function(task)
    {
        if (task == 'hotel.cancel') {
            Joomla.submitform(task, document.getElementById('hotel-form'));
        }
        else {
            
            if (task != 'hotel.cancel' && document.formvalidator.isValid(document.id('hotel-form'))) {
                
                Joomla.submitform(task, document.getElementById('hotel-form'));
            }
            else {
                alert('<?php echo $this->escape(JText::_('JGLOBAL_VALIDATION_FORM_FAILED')); ?>');
            }
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_viaggio&layout=edit&id=' . (int) $this->item->id); ?>" method="post" enctype="multipart/form-data" name="adminForm" id="hotel-form" class="form-validate">

    <div class="form-horizontal">
        <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_TOURISTINVITE_TITLE_HOTEL', true)); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <fieldset class="adminform">

                    <input type="hidden" name="jform[id]" value="<?php echo $this->item->id; ?>" />
                    <input type="hidden" name="jform[ordering]" value="<?php echo $this->item->ordering; ?>" />
                    <input type="hidden" name="jform[state]" value="<?php echo $this->item->state; ?>" />
                    <input type="hidden" name="jform[checked_out]" value="<?php echo $this->item->checked_out; ?>" />
                    <input type="hidden" name="jform[checked_out_time]" value="<?php echo $this->item->checked_out_time; ?>" />

				<?php if(empty($this->item->created_by)){ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo JFactory::getUser()->id; ?>" />

				<?php } 
				else{ ?>
					<input type="hidden" name="jform[created_by]" value="<?php echo $this->item->created_by; ?>" />

				<?php } ?>
                    <div class="control-group">
                        <div class="control-label"><?php echo JText::_('COM_TOURISTINVITE_FORM_LBL_HOTEL_HOTEL_CITY'); ?></div>
                        <div class="controls">
                            <select name="jform[city_id]">
                                <?php
                                $db    = JFactory::getDbo();

                                $query = $db->getQuery(true);
                                $query->select('*');
                                $query->from('#__viaggio_city');
                                $query->order('name_rus asc');
                                $db->setQuery($query);
                                $cities = $db->loadObjectList();

                                foreach ($cities as $t) {
                                    if ($t->id == $this->item->city_id)
                                        $sel = 'selected';
                                    else
                                        $sel = '';
                                    ?>
                                <option <?php echo $sel; ?> value="<?php echo $t->id; ?>"><?php echo $t->name_eng; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hotel_name'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hotel_name'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('hotel_name_rus'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('hotel_name_rus'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('stars'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('stars'); ?></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label"><?php echo $this->form->getLabel('cost'); ?></div>
                        <div class="controls"><?php echo $this->form->getInput('cost'); ?></div>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'hotel_translations', 'Переводы'); ?>
        <div class="row-fluid">
            <div class="span10 form-horizontal">
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('hotel_translation_ita'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('hotel_translation_ita'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('hotel_translation_rus'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('hotel_translation_rus'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('hotel_translation_eng'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('hotel_translation_eng'); ?></div>
                </div>
                <div class="control-group">
                    <div class="control-label"><?php echo $this->form->getLabel('hotel_translation_esp'); ?></div>
                    <div class="controls"><?php echo $this->form->getInput('hotel_translation_esp'); ?></div>
                </div>
            </div>
        </div>
        <?php echo JHtml::_('bootstrap.endTab'); ?>

        <?php echo JHtml::_('bootstrap.endTabSet'); ?>

        <input type="hidden" name="task" value="" />
        <?php echo JHtml::_('form.token'); ?>

    </div>
</form>
<?php
$document->addScript(JUri::base() . '/components/com_viaggio/assets/js/en_to_rus.js');
?>