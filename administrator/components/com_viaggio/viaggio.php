<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access
defined('_JEXEC') or die;

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_viaggio'))
{
	throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
}

// Include dependancies
jimport('joomla.application.component.controller');

JLoader::registerPrefix('Viaggio', JPATH_COMPONENT_ADMINISTRATOR);

$controller = JControllerLegacy::getInstance('Viaggio');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
