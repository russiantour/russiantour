<div style="background-color:#EEEEEE;" width="100%"><meta charset="utf-8"><!-- utf-8 works for most cases --><meta name="viewport" content="width=device-width"><!-- Forcing initial-scale shouldn't be necessary --><meta http-equiv="X-UA-Compatible" content="IE=edge"><!-- Use the latest (edge) version of IE rendering engine --><meta name="x-apple-disable-message-reformatting"><!-- Disable auto-scale in iOS 10 Mail entirely --></div>
<title></title>
<!-- The title tag shows in email notifications, like Android 4.4. --><!-- Web Font / @font-face : BEGIN --><!-- NOTE: If web fonts are not required, lines 10 - 27 can be safely removed. --><!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. --><!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]--><!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ --><!--[if !mso]><!--><!-- insert web font reference, eg: <link href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> --><!--<![endif]--><!-- Web Font / @font-face : END --><!-- CSS Reset -->
<style type="text/css">/* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        /* What it does: A work-around for iOS meddling in triggered links. */
        *[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
        }
        /* What it does: A work-around for Gmail meddling in triggered links. */
        .x-gmail-data-detectors,
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
        }
        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
	        display: none !important;
	        opacity: 0.01 !important;
        }
        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
	        display:none !important;
	   	}
        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }
        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */
        /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }
</style>
<!-- What it does: Makes background images in 72ppi Outlook render at correct size. --><!--[if gte mso 9]>
	
	<![endif]--><!-- Progressive Enhancements -->
<style type="text/css">/* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }
</style>
<center style="width: 100%; background: #eeeeee; text-align: left;"><!-- Visually Hidden Preheader Text : BEGIN --><!-- Visually Hidden Preheader Text : END --><!--
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
            2. MSO tags for Desktop Windows Outlook enforce a 600px width.
        -->
<div class="email-container" style="max-width: 600px; margin: auto;"><!--[if mso]>
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
            <td>
            <![endif]--><!-- Email Header : BEGIN -->
<div style="display:none;font-size:1px;color:#222222;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide: all;">Russian Tour International</div>

<table align="center" aria-hidden="true" border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 600px;" width="100%">
	<tbody class="acyeditor_sortable">
		<tr class="acyeditor_delete" style="line-height: 0px;" title="Deletable">
			<td bgcolor="#ffffff" style="padding: 20px 0; text-align: center"><a href="https://www.russiantour.com"><img alt="Logo Russia Tour " height="165" src="http://www.russiantour.com/media/com_acymailing/upload/rti289_1_.png" style=" height:165px;  width:289px; " width="289" /></a></td>
		</tr>
	</tbody>
</table>
<!-- Email Header : END --><!-- Email Body : BEGIN -->

<table align="center" aria-hidden="true" border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 600px;" width="100%"><!-- Hero Image, Flush : BEGIN -->
	<tbody>
	 
		<tr>
			<td bgcolor="#ffffff">
			<table aria-hidden="true" border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
				<tbody>
					<tr>
						<td>

						sadfsadf
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<!-- Two Even Columns : END --><!-- Clear Spacer : BEGIN --><!-- Clear Spacer : END --><!-- 1 Column Text : BEGIN --><!-- 1 Column Text : END -->
	</tbody>
</table>


<table align="center" aria-hidden="true" border="0" cellpadding="0" cellspacing="0" role="presentation" style="max-width: 680px;" width="100%">
	<tbody class="acyeditor_sortable">
		<tr class="acyeditor_delete" title="Deletable">
			<td class="x-gmail-data-detectors" style="padding: 20px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align: center; color: #888888;">Russian Tour International Ltd. info@russiantour.com<br />
			Finlyandskiy Pr. 4a, off.717, 194044 San Pietroburgo<br />
			Tel/fax +78126470690 - Numero Verde: 800404000<br />
			Roma +390690289660 - Palermo +390917481654<br />
			Milano +390245074837 - Genova +390108935089<br />
			<a href="https://www.facebook.com/russiantour"><img alt="Facebook" height="25" src="http://www.russiantour.com/media/com_acymailing/upload/soc/face.png" style=" height:25px;  width:25px; " width="25" /></a>&nbsp;<a href="https://plus.google.com/u/1/+Russiantourcom/posts"><img alt="Google Plus" height="25" src="http://www.russiantour.com/media/com_acymailing/upload/soc/g.png" style=" height:25px;  width:25px; " width="25" /></a>&nbsp;<a href="http://instagram.com/russiantour"><img alt="Instagram" height="25" src="http://www.russiantour.com/media/com_acymailing/upload/soc/in.png" style=" height:25px;  width:25px; " width="25" /></a>&nbsp;<a href="https://www.vk.com/russiantour"><img alt="Vkontakte " height="25" src="http://www.russiantour.com/media/com_acymailing/upload/soc/vk.png" style=" height:25px;  width:25px; " width="25" /></a>&nbsp;<a href="https://www.youtube.com/channel/UCUwaVG2fjz07ox9W3V3Nv9A"><img alt="You Tube" height="28" src="http://www.russiantour.com/media/com_acymailing/upload/soc/video_youtube_icon_1_.png" style=" height:28px;  width:28px; " width="28" /></a>

			<div class="acyeditor_delete acyeditor_text" style="text-align:center" title="Editable (Text)
Deletable">{unsubscribe}Unsubscribe{/unsubscribe}</div>
			</td>
		</tr>
	</tbody>
</table>
<!-- Email Footer : END --><!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]--></div>
</center>
