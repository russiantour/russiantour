<?php

/**
 * copyright (C) 2012-2015 GWE Systems Ltd - All rights reserved
 * @license GNU/GPLv3 www.gnu.org/licenses/gpl-3.0.html
 * version 3.2.16
 * */
// no direct access
defined('_JEXEC' ) or die('Restricted access');

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

class pkg_jevlocationsInstallerScript
{

	public function preflight($type, $parent)
	{
		$this->minimum_joomla_release = $parent->get( "manifest" )->attributes()->version;

		if(!$this->phpCheck())
		{
			return false;
		}

		if(!$this->joomlaCheck())
		{
			return false;
		}

		// Joomla! broke the update call, so we have to create a workaround check.
		$db = JFactory::getDbo ();
		$db->setQuery ( "SELECT enabled FROM #__extensions WHERE element = 'com_jevents' AND type='component' " );
		$is_enabled = $db->loadResult ();

		if ($is_enabled == 1) {
			$manifest  =  JPATH_SITE . "/administrator/components/com_jevents/manifest.xml";
			if (!JFile::exists($manifest) || ! $manifestdata = $this->getValidManifestFile ( $manifest )) {
				$manifest  =  JPATH_SITE . "/administrator/manifests/packages/pkg_jevents.xml";
				if (!JFile::exists($manifest) ||  ! $manifestdata = $this->getValidManifestFile ( $manifest )) {
					JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVENTS_UNINSTALLED") . ". " . $rel, 'error');
					return false;
				}
			}

			$app = new stdClass ();
			$app->name = $manifestdata ["name"];
			//We clean up the version tag
			$app->version = str_replace("RC", "", $manifestdata ["version"]);

			if (version_compare( $app->version , '3.4.0', "lt"))
			{
				JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVENTS_OLDER_3_4_0") . ". " . $rel, 'error');
				return false;
			} else {
				$this->hasJEventsInst = 1;
				return;
			}
		}
		else
		{
            $this->hasJEventsInst = 0;
            if ($is_enabled == 0)
			{
				JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVENTS_DISABLED") . ". " . $rel, 'error');
                return false;
            } elseif(!$is_enabled)
			{
				JFactory::getApplication()->enqueueMessage(JText::_("COM_JEVENTS_UNINSTALLED") . ". " . $rel, 'error');
                return false;
            }
		}
	}
	// TODO enable plugins
	public function update()
	{

		return true;
	}

	public function install($adapter)
	{
		return true;
	}


	public function uninstall($adapter)
	{

	}

	protected function joomlaCheck()
	{
		$jversion = new JVersion();

		// abort if the current Joomla release is older
		if( version_compare( $jversion->getShortVersion(), $this->minimum_joomla_release, 'lt' ) )
		{
			JFactory::getApplication()->enqueueMessage(JText::sprintf("COM_JEVLOCATIONS_LOW_JOOMLA_WARNING", $this->minimum_joomla_release), 'error');
			return false;
		}

		return true;
	}

	function phpCheck()
	{
		// Only allow to install on PHP 5.3.1 or later
		if (defined('PHP_VERSION'))
		{
			$version = PHP_VERSION;
		}
		elseif (function_exists('phpversion'))
		{
			$version = phpversion();
		}
		else
		{
			$version = '5.0.0'; // We set this version as reference
		}

		if (!version_compare($version, '5.3.1', 'ge'))
		{
			JFactory::getApplication()->enqueueMessage(JText::sprintf("COM_JEVENTS_LOW_PHP_WARNING",JText::_("COM_JEVLOCATIONS")), 'error');

			return false;
		}
		else
		{
			return true;
		}
	}

	/*
	 * enable the plugins
	 */
	function postflight($type, $parent)
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)

		if ($type == 'install') {
			// enable plugin
			$db = JFactory::getDbo();
			$query = "UPDATE #__extensions SET enabled=1 WHERE folder='jevents' and type='plugin' and element='jevlocations'";
			$db->setQuery($query);
			$db->query();
		}

	}
	// Manifest validation
	function getValidManifestFile($manifest)
	{
		$manifestdata = JApplicationHelper::parseXMLInstallFile($manifest);
		if (!$manifestdata)
			return false;
		return $manifestdata;

	}

}
