# If the Joomla site is installed within a folder such as at
# e.g. www.example.com/joomla/ the robots.txt file MUST be
# moved to the site root at e.g. www.example.com/robots.txt
# AND the joomla folder name MUST be prefixed to the disallowed
# path, e.g. the Disallow rule for the /administrator/ folder
# MUST be changed to read Disallow: /joomla/administrator/
#
# For more information about the robots.txt standard, see:
# http://www.robotstxt.org/orig.html
#
# For syntax checking, see:
# http://tool.motoricerca.info/robots-checker.phtml

User-agent: *
Allow: /*.js*
Allow: /*.css*
Allow: /*.png*
Allow: /*.jpg*
Allow: /*.gif*

Allow: /ita/mosca-sanpietroburgo


Disallow: /rus/modulo-visto-turistico
Disallow: /ita/modulo-visto-turistico
Disallow: /esp/modulo-visto-turistico
Disallow: /eng/modulo-visto-turistico

Disallow: /rus/bonifico
Disallow: /ita/bonifico
Disallow: /esp/bonifico
Disallow: /eng/bonifico

Disallow: /rus/pagato
Disallow: /ita/pagato
Disallow: /esp/pagato
Disallow: /eng/pagato


Disallow: /ita/info/calendar/eventidellanno/
Disallow: /ita/info/calendar/calendariomensile/
Disallow: /ita/info/calendar/eventidellasettimana/
Disallow: /bin/
#Disallow: /cache/
Disallow: /cli/
Disallow: /esp/info/calendar2/
Disallow: /component/jevents/
Disallow: /esp/info/calendar/
Disallow: /ita/component/flexicontent/
Disallow: /esp/component/flexicontent/
Disallow: /ita/component/search/
Disallow: /esp/component/search/
Disallow: /eng/component/search/
Disallow: /esp/recensioni
#
Disallow: /includes/
Disallow: /installation/
Disallow: /language/
Disallow: /layouts/
Disallow: /libraries/
Disallow: /logs/
#


#
Disallow: /tmp/

Disallow: /ita/tour3/
Disallow: /ita/10-tour/
Disallow: /ita/224-biglietti/
Disallow: /ita/itaita/site-content/
Disallow: /eng/11-russia/


Disallow: /ita/component/flexicontent/
Disallow: /ita/component/flexicontent
Disallow: /esp/component/flexicontent/
Disallow: /eng/component/flexicontent/
Disallow: /ita/component/jevents/
Disallow: /esp/component/jevents/
Disallow: /eng/component/jevents/
Disallow: /eng/site_content/
Disallow: /ita/site_content/
Disallow: /eng/soggiorno/hotel-russia/
Disallow: /esp/site_content/
Disallow: /eng/hotels-rents
Disallow: /esp/hoteles/
Disallow: /ita/soggiorno/
Disallow: /rus/hotel/
