<html>
<head>
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://demo1.imbachat.com/imbachat/v1/app-widget"></script>
    <script>
        function imbachatWidget(opt){
            if(!window.ImbaChat){
                return setTimeout(imbachatWidget, 50, opt)
            }

            window.ImbaChat.load(opt)
        }

        imbachatWidget({
            user_id: 1
        });
    </script>
</head>
<body>

</body>
</html>
