<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:25 */

// Don't redefine the functions if included multiple times.
if (!function_exists('XTP_BUILD\League\Uri\resolve_domain')) {
    require __DIR__.'/functions.php';
}
