<?php /* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:02 */

namespace XTP_BUILD\Psr\SimpleCache;

/**
 * Interface used for all types of exceptions thrown by the implementing library.
 */
interface CacheException
{
}
