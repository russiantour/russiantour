<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-08 16:33:53 */

/*
 * @package     Extly Infrastructure Support
 *
 * @author      Extly, CB. <team@extly.com>
 * @copyright   Copyright (c)2007-2019 Extly, CB. All rights reserved.
 * @license     http://www.opensource.org/licenses/mit-license.html  MIT License
 *
 * @see         https://www.extly.com
 */

namespace XTP_BUILD\Extly\Infrastructure\Service\Cms;

class CmsSettingsRegistry
{
    const CONFIG_CMS_NAME = 'CMS_NAME';
    const CONFIG_CMS_NAME_OVERRIDE = 'CMS_NAME_OVERRIDE';
    const CONFIG_CMS_PATH_ROOT = 'CMS_PATH_ROOT';
    const CONFIG_CMS_BASE_URL = 'CMS_BASE_URL';
    const CONFIG_CMS_LOG_FOLDER = 'CMS_LOG_FOLDER';
    const CONFIG_CMS_LOG_FILE = 'CMS_LOG_FILE';
    const CONFIG_CMS_LOG_LEVEL = 'CMS_LOG_LEVEL';
    const CONFIG_CMS_CDN_URL = 'CMS_CDN_URL';

    const CONFIG_EXTENSION_ALIAS = 'EXTENSION_ALIAS';
    const CONFIG_EXTENSION_UPDATE_DLID = 'EXTENSION_UPDATE_DLID';
    const CONFIG_EXTENSION_FRONTEND_SECRET_WORD = 'EXTENSION_FRONTEND_SECRET_WORD';

    const BASE_URL = 'base_url';

    const WEBSERVICE_API_KEY = 'rest_api_key';
}
