<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:20 */

namespace XTP_BUILD\Http\Discovery\Exception;

/**
 * Thrown when we can't use Puli for discovery.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
final class PuliUnavailableException extends StrategyUnavailableException
{
}
