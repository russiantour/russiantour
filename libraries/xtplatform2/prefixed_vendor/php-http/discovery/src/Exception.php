<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:21 */

namespace XTP_BUILD\Http\Discovery;

/**
 * An interface implemented by all discovery related exceptions.
 *
 * @author Tobias Nyholm <tobias.nyholm@gmail.com>
 */
interface Exception
{
}
