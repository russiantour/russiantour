<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:09 */

namespace XTP_BUILD\Http\Message;

/**
 * An interface implemented by all HTTP message related exceptions.
 */
interface Exception
{
}
