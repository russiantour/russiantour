<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:06 */

namespace XTP_BUILD\Http\Message\Exception;

use XTP_BUILD\Http\Message\Exception;

final class UnexpectedValueException extends \UnexpectedValueException implements Exception
{
}
