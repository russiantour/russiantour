<?php
/* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:57:03 */

namespace XTP_BUILD\Http\Client\Exception;

use XTP_BUILD\Http\Client\Exception;

/**
 * Base exception for transfer related exceptions.
 *
 * @author Márk Sági-Kazár <mark.sagikazar@gmail.com>
 */
class TransferException extends \RuntimeException implements Exception
{
}
