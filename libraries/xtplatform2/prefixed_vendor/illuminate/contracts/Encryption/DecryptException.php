<?php /* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:55:13 */

namespace XTP_BUILD\Illuminate\Contracts\Encryption;

use RuntimeException;

class DecryptException extends RuntimeException
{
    //
}
