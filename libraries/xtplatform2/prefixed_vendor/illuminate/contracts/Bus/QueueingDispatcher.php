<?php /* This file has been prefixed by <PHP-Prefixer> for "XT Platform" on 2019-10-03 16:55:15 */

namespace XTP_BUILD\Illuminate\Contracts\Bus;

interface QueueingDispatcher extends Dispatcher
{
    /**
     * Dispatch a command to its appropriate handler behind a queue.
     *
     * @param  mixed  $command
     * @return mixed
     */
    public function dispatchToQueue($command);
}
