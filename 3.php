<?php
//BuyerId='RT-INTER' UserId='XML' Password='43ks71wx9'
$url = "http://test-www.acase.ru/xml/form.jsp";

// параметры логина
$companyId = "RT-INTER";
$userId = "XML";
$userPass = "43ks71wx9";

// язык пользователя
$language = "ru";

$xml = "<HotelProductRequest BuyerId=\"".$companyId."\" UserId=\"".$userId."\" Password=\"".$userPass."\" Language=\"ru\" Hotel=\"9800032\" Currency=\"2\" WhereToPay=\"3\" ArrivalDate=\"11.11.2020\" DepartureDate=\"12.11.2020\" Id=\"\" AccommodationId=\"\" NumberOfGuests=\"1\" NumberOfExtraBedsAdult=\"0\"/>";

// метод XML-шлюза
$RequestName = "HotelProductRequest";

// формируем запрос, параметры пропускаем через urlencode
$request = "RequestName=".urlencode($RequestName).
    "&XML=".urlencode($xml);


// опции post-запроса
$options = array(
    'method' => 'POST',
    'header' => 'Content-Type: application/x-www-form-urlencoded' . PHP_EOL,
    'content' => $request);

// выполняем запрос, ответ считываем в строку $xml_response
$context = stream_context_create( array('http' => $options) );
$xml_response = file_get_contents ($url, false, $context) or die('Ошибка');

// тот самый simplexml
$xml = simplexml_load_string($xml_response);

// выводим полученный объект
echo '<pre>';
var_dump($xml);
echo '</pre>';