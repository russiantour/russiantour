<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  System.russiantour
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Language Code plugin class.
 *
 * @since  2.5
 */
class PlgSystemRussiantour extends JPlugin
{
	/**
	 * Plugin that removes unnecessary alternate language tags
	 *
	 * @return  void
	 *
	 * @since   2.5
	 */
	public function onAfterRender()
	{
		$app = JFactory::getApplication();

		// Use this plugin only in site application.
		if ($app->isSite())
		{
			// Get the response body.
			$body = $app->getBody();
            $patterns = array();

			if (isset($app->russiantour_langsToHide))
            {
                foreach ($app->russiantour_langsToHide as $lang)
                {
                    $patterns[] = '/.*hreflang="'.$lang.'".*\/>/';
                }
                $app->setBody(preg_replace($patterns, '', $body));
            }
            if (/*isset($_GET['kaktus']) && */isset($app->russiantour_links)){
                preg_match_all('/<a .*"(.*\/hotel\/.*)".*>/',$body,$m);
                /*var_dump($m);
                var_dump($app->russiantour_links);*/
                foreach ($m[1] as $link)
                {
                    $hide = true;
                    foreach ($app->russiantour_links as $l2){
                        if (strpos($link,$l2)!==false)
                        {
                            $hide = false;
                            break;
                        }
                    }
                    if ($hide)
                    {
                        $str = str_replace('/','\/',$link);
                        $str = str_replace('?','\?',$str);
                        $patterns[] = '/<a[^>]*'.$str.'[^>]*>[^<]*(<img[^>]*>)[^<]*<\/a>/sU';
                    }
                }
                /*$p = array(
                    '/<a[^>]*\/esp\/hoteles\/hoteles-moscu\/hotel\/hotel-academicheskaya-mosca\?kaktus=1[^>]*>[^<]*(<img[^>]*>)[^<]*<\/a>/sU',
                    //'/<a.*\/eng\/hotels-rents\/hotels-moscow\/hotel\/hotel-academicheskaya-mosca?kaktus=1.*>.*(<img.*>).*<\/a>/sU'
                );*/
                $app->setBody(preg_replace($patterns, '$1', $body));
                //exit;
            }
		}
	}
}
