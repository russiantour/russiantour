<?php

/**
 * @package   FL Yandex Turbo Plugin
 * @author    Дмитрий Васюков https://fictionlabs.ru
 * @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
 */

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class plgAjaxFLYandexTurboInstallerScript
{	
	// Pre Install Function

	function preflight ($type, $parent)
	{	
		// Check PHP version

		if (!(version_compare(PHP_VERSION, '5.6.0') >= 0)) {
			JFactory::getApplication()->enqueueMessage(JText::_('PLG_FLYANDEXTURBO_ERROR_PHP'), 'error');
			
			return false;
		}

		// Check Joomla Version

		jimport('joomla.version');
		$jversion = new JVersion();

		if (!$jversion->isCompatible('3.6'))
		{
			JFactory::getApplication()->enqueueMessage(JText::_('PLG_FLYANDEXTURBO_ERROR_JOOMLA'), 'error');

			return false;
		}
	}

	// Post Install Function

	function postflight( $type, $parent )
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery( true );
		$query->update('#__extensions')->set('enabled=1')->where('type='.$db->q('plugin'))->where('element='.$db->q( 'flyandexturbo'));
		$db->setQuery( $query )->execute();
	}
}
