<?php

/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2008-2009 GWE Systems Ltd, 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://www.jevents.net
 */

// no direct access
defined('_JEXEC' ) or die('Restricted access');

jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');

class plgjeventsjevlocationsInstallerScript
{

	var $pluginsDir;
	var $filterpath;
	var $pluginDir;
	var $plugin_filterpath;
	var $filters;	
	
	public function __construct()
	{
		$this->pluginsDir = 'plugins/jevents';
		$this->filterpath = JFolder::makeSafe($this->pluginsDir."/filters");
		$this->pluginDir = $this->pluginsDir . '/jevlocations';
		$this->plugin_filterpath = JFolder::makeSafe($this->pluginDir."/filters");
		$this->filters = array('Locationcategory.php', 'Locationcity.php', 'Locationlookup.php', 'Locationsearch.php', 'Locationstate.php');  // RSH 11/11/10 - Need a better way to do this!		
	}	

	public function preflight($action, $adapter)
	{
		return true;
	}

	public function update()
	{

		return true;
	}

	public function install($adapter)
	{
		return true;
	}

	public function postflight($action, $adapter)
	{
		/*
		// This should be done as part of the postflight - after the rest of the installation has occurred
		// Create, if necessary, the main jevents folder for the filters, copy the files from plugin's filter folder
		if (!(JFolder::exists(JPATH_ROOT . "/" . $this->plugin_filterpath))) {
			JError::raiseNotice(0, JText::_('PLG_JEVENTS_JEVLOCATIONS_FILTERS_NOT_FOUND'));
			return false;
		} else {
			// The 'force' parameter on the copy() will create the destination directory if it doesn't already exist
			if (!(JFolder::copy($this->plugin_filterpath, $this->filterpath, JPATH_ROOT, $force = true))) {
				JError::raiseNotice(0, JText::_('PLG_JEVENTS_JEVLOCATIONS_FILTERS_MOVE_ERROR'));
				return false;
			}
		}
		
		return true;  
		*/
	}

	public function uninstall($adapter)
	{
		$files = array();
		// need a better way to get files.  When this method is invoked by the uninstallation framework, the initially installed filter folder for 
		// this plugin is already deleted!
		foreach ($this->filters AS $file)
		{
			$files[] = JPATH_ROOT . "/" . $this->filterpath . "/" . $file;
		}
		
		try {
			JFile::delete($files);  // delete all of the files
		} catch (Exception $e) {
			echo $e->getMessage();
		}
		
		// Delete the main filters folder if it is empty
		$files = JFolder::files(JPATH_ROOT . "/" . $this->filterpath, $filter = '.', $recurse = true, $fullpath = true, $exclude = array());
		
		if ( (is_array($files)) && (count($files) == 0) ) {
			try {
				JFolder::delete(JPATH_ROOT . "/" . $this->filterpath);  // delete main folder
			} catch (Exception $e) {
				echo $e->getMessage();
			}		
		}
		
	}

}
