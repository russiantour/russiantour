/*
function selectThisLocation(event){
	var oldid = this.id;
	var newid = this.id.replace("locn_pot_","");
	jQuery("#locn").val(newid);
	jQuery("#evlocation").val(this.rel);
	locnClearMatches();
}
*/

function findEventNearby(autogeolocate) {
	// Touch devices only
	if (autogeolocate==2){
		try{
			document.createEvent("TouchEvent");
		}
		catch(e){
			return false;
		}
	}
	if ("geolocation" in navigator) {
		/* geolocation is available */
		navigator.geolocation.getCurrentPosition(function(position) {
		var googleaddress = document.getElementById("googleaddress");
		if (!googleaddress || googleaddress.value !="") {
			return;
		}
		
		var gvelem = document.getElementById("geosearch_fv");
		if (!gvelem) {
			return;
		}
		gvelem.value = position.coords.latitude+"," + position.coords.longitude;

		if (myGeocoder) {
			var myTimer = trapSlowGoogle.delay(30000);

			var input = document.getElementById("geosearch_fv").value;
			var latlngStr = input.split(",",2);
			var lat = parseFloat(latlngStr[0]);
			var lng = parseFloat(latlngStr[1]);

			var latlng = new google.maps.LatLng(lat, lng);

			myGeocoder.geocode(
				{
					'latLng': latlng
				},
				function(results, status) {
					var gaddressmatches = $("gaddressmatches");
					gaddressmatches.innerHTML = "";
					gaddressmatches.style.display="none";
					if (status == google.maps.GeocoderStatus.OK) {
						if (results.length>0){
							$("googleaddress").value = results[0].formatted_address;
							var gvelem = document.getElementById("geosearch_fv");
							var point = results[0].geometry.location;
							//gvelem.value = point.lng()+","+point.lat();
							gvelem.value = point.lat()+","+point.lng();
						}

						clearTimeout(myTimer);
						myform = $("jeventspost");
						myform.submit();
					} else {
						 alert("Geocoder failed due to: " + status);
						myform = $("jeventspost");
						myform.submit();
					}
				}
			);
		}
		//myform = $("jeventspost");
		//myform.submit();
	   });
	}
}

function trapSlowGoogle(){
//	alert("The address lookup is taking too long and so the address filter has been ignored");
//	$myform = jQuery("#jeventspost");
//	$myform.submit();
}
