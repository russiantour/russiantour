<?php

/**
 * copyright (C) 2012 GWE Systems Ltd - All rights reserved
 * @license GNU/GPLv3 www.gnu.org/licenses/gpl-3.0.html
 * */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

class plgjeventsjevmetatagsInstallerScript
{

	//
	// Joomla installer functions
	//
	
	function install($parent)
	{

		$this->createTables();
		return true;

	}

	function uninstall($parent)
	{
		// No nothing for now, we want to keep the tables just incase they remove the plugin by accident. 

	}

	function update($parent)
	{
		// Nothing to do for now, tables should be created on install.

	}

	function createTables()
	{
		$db = JFactory::getDBO();
		$charset = ($db->hasUTF()) ? 'DEFAULT CHARACTER SET `utf8`' : '';
		$sql = <<<SQL
CREATE TABLE IF NOT EXISTS #__jev_metatags(
	id int(11) NOT NULL auto_increment,
	ev_id int(11) NOT NULL default 0,
	metadesc text NOT NULL ,
	metakey text NOT NULL ,
	PRIMARY KEY  (id),
	INDEX (ev_id)
) $charset;
SQL;
		$db->setQuery($sql);
		if (!$db->query()){
			echo $db->getErrorMsg();
		}

	}

	function postflight($type, $parent)
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		
		// New install, lets enable the plugin! 
		$db = JFactory::getDBO();
		$db->setDebug(0);
		$sql = "UPDATE #__extensions SET enabled=1 WHERE element='jevmetatags'";
		$db->setQuery($sql);
		$db->query();
		echo $db->getErrorMsg();
		
		// New we can echo out the nice Completed install page.
		
		echo '<h2>' . JText::_('JEV_METATAG') . ' ' . $parent->get('manifest')->version . ' </h2>';
		echo '<strong>';

		if ($type == "update")
		{
			echo JText::_('JEV_METATAG_UPDATE') . '<br/>';
			echo JText::_('JEV_METATAG_DESC');
		}
		else
		{
			echo JText::_('JEV_METATAG_INSTALL') . '<br/>';
			echo JText::_('JEV_METATAG_DESC');
		}
		echo '</strong><br/><br/>';

	}

}
