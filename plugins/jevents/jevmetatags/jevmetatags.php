<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );

class plgJEventsJevmetatags extends JPlugin
{
	var $_dbvalid = 0;

	function __construct(& $subject, $config)
	{
		parent::__construct($subject, $config);

		JFactory::getLanguage()->load( 'plg_jevents_jevmetatags',JPATH_ADMINISTRATOR );
	}

	function onEditCustom( &$row, &$customfields )
	{
		// Only setup when editing an event (not a repeat)
		if (JRequest::getString("jevtask","")!="icalevent.edit" && JRequest::getString("jevtask","")!="icalevent.editcopy" ) return;

		$sql = "SELECT * FROM #__jev_metatags WHERE ev_id=".$row->ev_id();
		$db = JFactory::getDBO();
		$db->setQuery($sql);
		$metadata = $db->loadObject();

		$label = JText::_("JEV_METADESC");
		$value = (!is_null($metadata) && isset($metadata->metadesc))?$metadata->metadesc:"";
		$input	= '<textarea cols="50" rows="3" name="custom_metadesc" id="custom_metadesc" >'.$value.'</textarea>';
		$customfield = array("label"=>$label,"input"=>$input,"default_value"=>"","id_to_check"=>"custom_metadesc");
		$customfields["metadesc"]=$customfield;

		$label = JText::_("JEV_METAKEY");
		$value = (!is_null($metadata) && isset($metadata->metakey))?$metadata->metakey:"";
		$input	= '<textarea cols="50" rows="3" name="custom_metakey" id="custom_metakey" >'.$value.'</textarea>';
		$customfield = array("label"=>$label,"input"=>$input,"default_value"=>"","id_to_check"=>"custom_metakey");
		$customfields["metakey"]=$customfield;

		return true;
	}

	/**
	 * Clean out custom fields for event details not matching global event detail
	 *
	 * @param unknown_type $idlist
	 */
	function onCleanCustomDetails($idlist){
		// TODO
		return true;
	}

	/**
	 * Store custom fields
	 *
	 * @param iCalEventDetail $evdetail
	 */
	function onStoreCustomDetails($evdetail){

	}

	/**
	 * Store custom fields
	 *
	 * @param iCalEventDetail $evdetail
	 */
	// TODO update reminder timestamps when event times have changed
	function onStoreCustomEvent($event){

		$evdetail = $event->_detail;

		if (!array_key_exists("metadesc",$evdetail->_customFields)) return;

		$noHtmlFilter = JFilterInput::getInstance();
		$metadesc =$noHtmlFilter->clean($evdetail->_customFields["metadesc"]);
		$metakeys = $noHtmlFilter->clean($evdetail->_customFields["metakey"]);

		$db = JFactory::getDBO();

		$eventid = intval($event->ev_id);

		$sql = "SELECT * FROM #__jev_metatags WHERE ev_id=".$eventid;
		$db->setQuery($sql);
		$metadata = $db->loadObject();

		if ($metadata && $metadata->id>0){

			$sql = "UPDATE #__jev_metatags SET metadesc=".$db->Quote($metadesc)
			.",   metakey =".$db->Quote($metakeys)
			.", ev_id=".intval($eventid)
			." WHERE id=".intval($metadata->id)
			;
		}
		else {
			$sql = "INSERT INTO  #__jev_metatags SET metadesc=".$db->Quote($metadesc)
			.",   metakey =".$db->Quote($metakeys)
			.", ev_id=".intval($eventid)
			;
		}
		$db->setQuery($sql);
		$success =  $db->query();

		return $success;
	}

	/**
	 * Clean out custom details for deleted event details
	 *
	 * @param comma separated list of event detail ids $idlist
	 */
	function onDeleteCustomEvent($idlist){
		$ids = explode(",",$idlist);
		JArrayHelper::toInteger($ids);
		$idlist = implode(",",$ids);
		// delete the metatags too
		$db = JFactory::getDBO();
		$sql = "DELETE FROM #__jev_metatags WHERE ev_id IN (".$idlist.")";
		$db->setQuery($sql);
		$db->query();
		return true;
	}

	function onDisplayCustomFields(&$row){

		$sql = "SELECT * FROM #__jev_metatags WHERE ev_id=".$row->ev_id();
		$db = JFactory::getDBO();
		$db->setQuery($sql);
		$metadata = $db->loadObject();

		$document = JFactory::getDocument();
		if(!is_null($metadata) && isset($metadata->metadesc) && $metadata->metadesc!=""){
			$document->setDescription($metadata->metadesc);

		}
		if(!is_null($metadata) && isset($metadata->metakey) && $metadata->metakey!=""){
			$document->setMetaData('keywords', $metadata->metakey);
		}
	}

	static function fieldNameArray($layout='detail')
	{
		if ($layout == "edit")
		{
			$return = array();
			$labels = array();
			$values = array();

			//JFactory::getLanguage()->load( 'plg_jevents_jevmetatags',JPATH_ADMINISTRATOR );

			$return['group'] = JText::_("JEV_METAKEY", true);

			$labels[] =  JText::_("JEV_METADESC");
			$values[] = "metadesc";

			$labels[] =  JText::_("JEV_METAKEY");
			$values[] = "metakey";

			$return['values'] = $values;
			$return['labels'] = $labels;

			return $return;
		}

		return array();
	}
}
