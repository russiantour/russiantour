<?php
/**------------------------------------------------------------------------
# plg_socbuttons - Social Share Buttons Plugin Content for Joomla 2.5 and Joomla 3.x
# author    tallib
# copyright - Copyright (C) 2014 Socext.com. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://socext.com
# Technical Support:  http://socext.com
-------------------------------------------------------------------------**/
/**------------------------------------------------------------------------
* file: socbuttons.php 1.6.0, September 2014 20:00:00 SocExt $
* package:	SocButtons Plugin Content
------------------------------------------------------------------------**/
//No direct access!
defined( '_JEXEC' ) or die;

class plgContentSocButtons extends JPlugin 
{
	public function onContentAfterDisplay($context, &$row, &$params, $page = 0)
	{	
		if($context == 'com_content.article'){
			//hide plugin on category
			$exclusion_categories = $this->params->get('fb_categories', 0);
			if(!empty($exclusion_categories)){
				if(strlen(array_search($row->catid, $exclusion_categories))){
					return false;
				}
			}
						
			//hide plugin on article 
			$exclusion_articles = $this->params->get('fb_articles', '');
			$articlesArray = explode(",",$exclusion_articles);
			if(!empty($exclusion_articles)){
				if(strlen(array_search($row->id, $articlesArray))){
					return false;
				}
			}
			//plugin
			require_once(JPATH_BASE.'/components/com_content/helpers/route.php');			
			$Itemid = JRequest::getVar("Itemid","1");
    
					if($row->id){
	    $link = JURI::getInstance();
        $root= $link->getScheme() ."://" . $link->getHost();
        
        $link = JRoute::_(ContentHelperRoute::getArticleRoute($row->slug, $row->catslug), false);
        $link = $root.$link;
			}else{
				$jURI =& JURI::getInstance();
				$link = $jURI->toString();
			}
			
			//setting css 
			$fblike_type = $this->params->get('fblike_type');
			if($fblike_type == 'button_count'){
				$widthtype= 75;
				$heighttype = 20;
			}else{
				$widthtype= 47;
				$heighttype = 60;
			}
			
			$twitter_type = $this->params->get('twitter_type');
			if($twitter_type == 'horizontal'){
				$widthweet= 90;
			}else{
				$widthweet= 60;
			}
			
			$twitter_lang = $this->params->get('twitter_lang');
			if($twitter_lang == 'en'){
				$widthlang= 0;
			}else{
				$widthlang= 0;
			}
			
			$Google_type = $this->params->get('Google_type');
			if($Google_type == 'medium'){
				$Google_top = 60;
			}else{
				$Google_top=60;
			}
			
			$linkedin_type = $this->params->get('linkedin_type');
			if($linkedin_type == 'top'){
				$margin_top = 3;
			}else{
				$margin_top='';
			}
			
			$Vk_id = $this->params->get('vk_id', '');
			$Vk_type = $this->params->get('Vk_type');
			if($Vk_type == 'vertical'){
				$vk_height = 40;
				$vk_margin = 5;
			}
			if($Vk_type == 'button'){
				$vk_height = 140;
				$vk_margin = 10;
			}
            if($Vk_type == 'mini'){
				$vk_height = 75;
				$vk_margin = 10;
			}
			
            $Odnomm_type = $this->params->get('Odnomm_type');
			if($Odnomm_type == '1'){
				$Odnw = 100;
				$mmt = ', \'vt\' : \''.$this->params->get('Odnomm_type').'\'';
			}else{
				$Odnw= 125;
				$mmt = '';
			}			
			$soc_copr = $this->params->get('soc_copr');	

			
			//code plugin
			$html  = '';
			$html .= '<div style="clear:both;"></div>';
			if($this->params->get('soc_text_on')==1){
			$html  = '<div><span style="font-size:'.$this->params->get('soc_fs').'px;font-weight:bold;">'.$this->params->get('soc_text').'</span></div>';
			}else{
				$html .='';
			}
			$html .= '<div class="socbuttons" style="padding-top: 10px; overflow:visible; float: '.$this->params->get('fb_align', 'left').';">';
			$document = JFactory::getDocument();
		    $config = JFactory::getConfig();
		    $pattern = "/<img[^>]*src\=['\"]?(([^>]*)(jpg|gif|png|jpeg))['\"]?/";
			preg_match($pattern, $row->text, $matches);
			if(!empty($matches)){
				$document->addCustomTag('<meta property="og:image" content="'.$root.'/'.$matches[1].'"/>');
			}
			
			if($this->params->get('fblike')==1){
			if($this->params->get('fb_show_og_tag')==1){
			$document->addCustomTag('<meta property="og:site_name" content="'.$config->get('sitename').'"/>');
			$document->addCustomTag('<meta property="og:title" content="'.$row->title.'"/>');
			$document->addCustomTag('<meta property="og:type" content="article"/>');
			$document->addCustomTag('<meta property="og:url" content="'.$link.'"/>');
			}else{
				$html .='';
			}
			$html .='<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/'.$this->params->get('fblike_lang').'/sdk.js#xfbml=1&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>';

				$html .= '<div style="margin-left: 10px; width: '.$this->params->get('fblike_width').'px; height :'.$heighttype.'px; float: left;">';
				$html .='<div class="fb-like padbo5" data-href="'.$link.'" data-colorscheme="'.$this->params->get('fblike_color').'" data-layout="'.$fblike_type.'" data-action="'.$this->params->get('fblike_action').'" data-show-faces="true" data-send="false" data-share="'.$this->params->get('fblike_share').'" data-width="'.$this->params->get('fblike_width').'" data-height="'.$heighttype.'"></div>';
				$html .= '</div>';
			}else{
				$html .='';
			}
			
			if($this->params->get('twitter')==1){
				$html .='<div style="width: '.$this->params->get('widthweet').'px; float: left; margin-left: 10px; padding-top:0px; margin-right:0px;">';
				$html .= '<a rel="nofollow" href="http://twitter.com/share" class="twitter-share-button" data-url="'.$link.'" data-count="'.$twitter_type.'" data-lang="'.$this->params->get('twitter_lang').'">Twitter</a><script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></script>'; 
				$html .= '</div>';
			}else{
				$html .='';
			}
			
			if($this->params->get('Google')==1){
			$doc =& JFactory::getDocument();
				$html .='<div style="width: '.$Google_top.'px !important; float: left; margin-left: 10px;     margin-top: -3px; border: none;">';
				$html .= '<g:plusone size="'.$Google_type.'"></g:plusone>';
	if($this->params->get('Google_code')==1){
				$document->addCustomTag('<script type="text/javascript" src="https://apis.google.com/js/plusone.js">{lang: \''.$this->params->get('plusLang').'\'}</script>');
				} else {
				$html .='<script type="text/javascript">
				window.___gcfg = {lang:\''.$this->params->get('plusLang').'\'};
  (function() {
    var po = document.createElement(\'script\'); po.type = \'text/javascript\'; po.async = true;
    po.src = \'https://apis.google.com/js/plusone.js\';
    var s = document.getElementsByTagName(\'script\')[0]; s.parentNode.insertBefore(po, s);  })();</script>';}
				$html .= '</div>';
			}else{
				$html .='';
			}
			
			if($this->params->get('linkedin')==1){
				$html .='<div style="float: left; margin-left: 10px; border: none;">';
				$html .= '<script type="text/javascript" src="http://platform.linkedin.com/in.js">lang: '.$this->params->get('LinkedinLang').'</script><script type="IN/share" data-url="'.$link.'" data-counter="'.$this->params->get('linkedin_type').'"></script>'; 
				$html .= '</div>';
			}else{
				$html .='';
			}
						
			if($this->params->get('Vk')==1){
				$html .='<div style="width: '.$vk_height.'px !important; float: left; border: none; margin-left: '.$vk_margin.'px;">';
				$html .= '<script type="text/javascript" src="//vk.com/js/api/openapi.js?101"></script><script type="text/javascript">VK.init({apiId:'.$Vk_id.', onlyWidgets: true});</script>';
                $html .='<div id="vk_like'.$row->id.'"></div>';
                $html .='<script type="text/javascript">VK.Widgets.Like("vk_like'.$row->id.'", {type: "'.$Vk_type.'", height: '.$this->params->get('Vk_h').', pageUrl:\''.$link.'\'});</script>'; 
				$html .= '</div>';
			}else{
				$html .='';
			}
							
			if($this->params->get('Odnomm')==1){
				$html .='<div style="width:'.$Odnw.'px;float: left; margin-left: 10px; border: none;" >';
				$html .= '<a target="_blank" class="mrc__plugin_uber_like_button" href="http://connect.mail.ru/share?url='.$link.'" data-mrc-config="{\'cm\' : \''.$this->params->get('mm_txt').'\', \'sz\' : \'20\', \'st\' : \'2\', \'tp\' : \'mm\''.$mmt.'}">Нравится</a>
				<script src="http://cdn.connect.mail.ru/js/loader.js" type="text/javascript" charset="UTF-8"></script>';
				$html .='</div>';
				$html .='<div style="width:'.$Odnw.'px;float: left; margin-left: 10px; border: none;" >';
				$html .= '<div id="ok_shareWidget"></div>
<script>
!function (d, id, did, st) {
  var js = d.createElement("script");
  js.src = "http://connect.ok.ru/connect.js";
  js.onload = js.onreadystatechange = function () {
  if (!this.readyState || this.readyState == "loaded" || this.readyState == "complete") {
    if (!this.executed) {
      this.executed = true;
      setTimeout(function () {
        OK.CONNECT.insertShareWidget(id,did,st);
      }, 0);
    }
  }};
  d.documentElement.appendChild(js);
}(document,"ok_shareWidget","'.$link.'","{st:\'rounded\',sz:20,ck:'.$this->params->get('odn_txt').',vt:\''.$this->params->get('Odnomm_type').'\'}");
</script>';
				$html .='</div>';
			}else{
				$html .='';
			}
			
			$html .= '<div style="clear:both;"></div>';
			$html .= '<style>.soc_no a{color:#d6d6d6; font-size:8px;} .soc_yes a{color:#d6d6d6; font-size:8px;display:none;}</style>';
			$html .= '</div>';
			$html .= '<div style="clear:both;"></div>';
			//end plugin
			
			//var_dump($row->text);
			//var_dump($row->fulltext);
			//var_dump($row->introtext);
			
            $position = $this->params->get('fb_position', 'above');
			if($position == 'above'){
				if($row->fulltext == ''){
					$row->text = $html . $row->text;
					}else{
					$row->text = $html . $row->text;	
				}
			} else {
				$row->text .= $html;
				}
		}
		return; 
	}
}
?>