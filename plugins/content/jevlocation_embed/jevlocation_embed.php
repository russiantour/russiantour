<?php

defined('_JEXEC') or die('Direct Access to this location is not allowed.');

jimport('joomla.plugin.plugin');
include_once(JPATH_SITE . "/components/com_jevents/jevents.defines.php");
JLoader::register('JevModal',JPATH_LIBRARIES."/jevents/jevmodal/jevmodal.php");

class plgContentJevlocation_embed extends JPlugin
{

	// Joomla 1.6!!
	public function onContentPrepare($context, &$row, &$params, $page = 0)
	{
		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
		// do not process the same article more than once (i.e. avoid recursion!)
		if (isset($row->jevloc_processed)){
			return true;
		}
		$row->jevloc_processed = true;

		static $locationsloaded;
		if (!isset($locationsloaded)) {
			$locationsloaded = array();
		}

		JFactory::getLanguage()->load('plg_content_jevlocation_embed', JPATH_ADMINISTRATOR);

// expression to search for
		$regex = "#{jevlocation[\=|\s]?(.+)}#sU";

// find all instances of mambot and put in $matches
		preg_match_all($regex, $row->text, $matches);
// Number of mambots
		$replace = array();
		if ($matches && count($matches) > 0)
		{

			$count = count($matches[0]);

			for ($i = 0; $i < $count; $i++)
			{
				$r = str_replace('{jevlocation=', '', $matches[0][$i]);
				$r = str_replace('{jevlocation ', '', $r);
				$r = str_replace('}', '', trim($r));

				$ploc = intval($r);

				// top it being loaded in component and or multiple modules
				if (in_array($ploc, $locationsloaded)){
					continue;
				}
				 $locationsloaded[] = $ploc;

				$replace[] = $this->plg_jevlocationembed_replacer($ploc);

			}
			$row->text = str_replace($matches[0], $replace, $row->text);
		}

	}

	private function plg_jevlocationembed_replacer($locid)
	{

		jimport('joomla.filesystem.path');

		if (!defined("JEVEX_COM_COMPONENT"))
		{
			define("JEVEX_COM_COMPONENT", "com_jevlocations");
			define("JEVEX_COMPONENT", str_replace("com_", "", JEVEX_COM_COMPONENT));
		}
		if (!defined("JEV_COM_COMPONENT"))
		{
			define("JEV_COM_COMPONENT", "com_jevents");
			define("JEV_COMPONENT", str_replace("com_", "", JEV_COM_COMPONENT));
		}
		JLoader::register('JEVHelper', JPATH_SITE . "/components/com_jevents/libraries/helper.php");
		JLoader::register('JevLocationsHelper', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/helper.php");
		JLoader::register('LocationsModelLocation', JPATH_ADMINISTRATOR . "/components/com_jevlocations/models/location.php");

		JLoader::register('JEventsHTML', JPATH_SITE . "/components/com_jevents/libraries/jeventshtml.php");
		JLoader::register('jevlocationsCategory', JPATH_COMPONENT_ADMINISTRATOR . "/libraries/categoryClass.php");


		//get the location
		$db = JFactory::getDbo();
		$uri =  JFactory::getUri();
		$user =  JFactory::getUser();
		$doc = JFactory::getDocument();
		$model = new LocationsModelLocation();
		$model->setId($locid);

		$location = $model->getData();
		if (!$location)
		{
			return "";
		}

		$long = $location->geolon;
		$lat = $location->geolat;
		$zoom = $location->geozoom;

		$compparams = JComponentHelper::getParams("com_jevlocations");
		JevLocationsHelper::loadApiScript();
		$googleurl = JevLocationsHelper::getApiUrl();
		$googlemapsurl = JevLocationsHelper::getMapsUrl();

		$redirecttodirections = $compparams->get("redirecttodirections", 0);
		$disableautopan = $compparams->get("autopan", 1)  ? "false":"true";
		$plugin = JPluginHelper::getPlugin("jevents", "jevlocations");
		if ($plugin)
		{
			$pluginparams = new JRegistry($plugin->params);
		}
		else
		{
			$pluginparams = new JRegistry(null);
		}

		$detailpopup = $this->params->get("detailpopup", 1);
		$pwidth = $this->params->get("pwidth", "750");
		$pheight = $this->params->get("pheight", "500");
		$menuItem = $this->params->get("target_location_itemid", 0);

		if ($detailpopup)
		{
			$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&tmpl=component&loc_id=$locid&title=" . JApplication::stringURLSafe($location->title);
			if ($menuItem != 0)
			{
				$baseUrl .= "&Itemid=" . $menuItem;
			}
			$locurl = JRoute::_($baseUrl);			
		}
		else
		{
			$baseUrl = "index.php?option=com_jevlocations&task=locations.detail&se=1&loc_id=$locid&title=" . JApplication::stringURLSafe($location->title);
			if ($menuItem != 0)
			{
				$baseUrl .= "&Itemid=" . $menuItem;
			}
			$locurl = JRoute::_($baseUrl);
		}

		$modalScript = JevLocationsHelper::setupDetailModal($this->params, $locurl);
		
		if ($googleurl != "")
		{
			if ($location->mapicon == "")
			{
				$location->mapicon = "blue-dot.png";
			}
			
			$maptype = $pluginparams->get("maptype", "ROADMAP");

			static $mapscript = false;
			if (!$mapscript){
				$root =  JURI::root();
				$mapscript = <<<SCRIPT
var urlrootloc = '$root'+'media/com_jevlocations/images/';
var googleurl = "$googleurl";
var googlemapsurl = "$googlemapsurl";
var maptype = "$maptype";
SCRIPT;
				$doc->addScriptDeclaration($mapscript);
			}
			$script = <<<SCRIPT
var myMap$locid = false;
var myMarker$locid = false;

function myMapload$locid(){
	if (!document.getElementById("gmap$locid")) return;

	var myOptions = {
		center: new google.maps.LatLng($lat,$long),
		zoom: $zoom,
		mapTypeId: google.maps.MapTypeId.$maptype
	};
	myMap$locid = new google.maps.Map(document.getElementById("gmap$locid"), myOptions);
					
	// Create our "tiny" marker icon
	var blueIcon = new google.maps.MarkerImage(urlrootloc + '$location->mapicon',
	// This marker is 32 pixels wide by 32 pixels tall.
	new google.maps.Size(32, 32),
	// The origin for this image is 0,0 within a sprite
	new google.maps.Point(0,0),
	// The anchor for this image is the base of the flagpole at 0,32.
	new google.maps.Point(16, 32));
							
	var markerOptions = {
		position:new google.maps.LatLng($lat,$long),
		map:myMap$locid,
		icon:blueIcon,
		disableAutoPan:$disableautopan,
		animation: google.maps.Animation.DROP,
		draggable:false
	};

	myMarker$locid = new google.maps.Marker( markerOptions);
	
	google.maps.event.addListener(myMarker$locid, "click", function(e) {
		if ($detailpopup){
			if ($redirecttodirections){
				window.open("$locurl");
			}
			else {
				$modalScript
			}
		}
		else {
			document.location = "$locurl";
		}
	});

	google.maps.event.addListener(myMap$locid, "click", function(e) {
		if ($detailpopup){
			if ($redirecttodirections){
				window.open("$locurl");
			}
			else {
				$modalScript
			}
		}
		else {
			document.location = "$locurl";
		}
	});
								
};
// delay by 1 second so that the page is properly rendered before we get the map
window.setTimeout("myMapload$locid()",1000);
SCRIPT;
			$location->mapscript = $script;
		}


		if ($detailpopup)
		{
			JHTML::_('behavior.modal');
			// google no longer allows this
			if ($compparams->get("redirecttodirections"))
			{
				$location->linkstart = "<a href='$locurl' target='_blank'>";
			}
			else
			{
				$location->linkstart = "<a href='$locurl' class='modal' rel='{handler:\"iframe\",\"size\": {\"x\": $pwidth, \"y\": $pheight}}'>";
			}
		}
		else
		{
			$location->linkstart = "<a href='$locurl'>";
		}

		$loadmapscript = false;
		$template = $this->params->get("template", "");
		if ($template != "")
		{
			$text = $template;
			$text = str_replace("{TITLE}", $location->title == "" ? "" : $location->title, $text);
			$text = str_replace("{STREET}", $location->street == "" ? "" : $location->street, $text);
			$text = str_replace("{CITY}", $location->city == "" ? "" : $location->city, $text);
			$text = str_replace("{STATE}", $location->state == "" ? "" : $location->state, $text);
			$text = str_replace("{POSTCODE}", $location->postcode == "" ? "" : $location->postcode, $text);
			$text = str_replace("{COUNTRY}", $location->country == "" ? "" : $location->country, $text);
			$text = str_replace("{PHONE}", $location->phone == "" ? "" : $location->phone, $text);

			if ($location->image != "")
			{
				$mediaparams = JComponentHelper::getParams('com_media');
				$mediabase = JURI::root() . $mediaparams->get('image_path', 'images/stories');
				// folder relative to media folder
				$locparams = JComponentHelper::getParams("com_jevlocations");
				$folder = "jevents/jevlocations";
				$thimg = '<img src="' . $mediabase . '/' . $folder . '/thumbnails/thumb_' . $location->image . '" />';
				$img = '<img src="' . $mediabase . '/' . $folder . '/' . $location->image . '" />';
				$text = str_replace("{IMAGE}", $img, $text);
				$text = str_replace("{THUMBNAIL}", $thimg, $text);
			}
			else
			{
				$text = str_replace("{IMAGE}", "", $text);
				$text = str_replace("{THUMBNAIL}", "", $text);
			}

			if (strlen($location->url) > 0)
			{
				$pattern = '[a-zA-Z0-9&?_.,=%\-\/]';
				if (strpos($location->url, "http://") === false)
					$location->url = "http://" . trim($location->url);
				$location->url = preg_replace('#(http://)(' . $pattern . '*)#i', '<a href="\\1\\2"  target="_blank">\\1\\2</a>', $location->url);

				$text = str_replace("{URL}", $location->url, $text);
			}
			else
			{
				$text = str_replace("{URL}", "", $text);
			}

			$text = str_replace("{LINK}", $location->linkstart, $text);
			$text = str_replace("{/LINK}", "</a>", $text);

			$text = str_replace("{DESCRIPTION}", $location->description == "" ? "" : $location->description, $text);
			$map = '<div id="gmap'.$locid.'"></div>';
			$location->map = $map;
			$width = $this->params->get('gwidth', 200);
			$height = $this->params->get('gheight', 150);
			$doc->addStyleDeclaration("#gmap$locid img {max-width: none !important;} #gmap$locid {width:" . $width . "px; height:" . $height . "px;overflow:hidden !important;}");

			if (strpos($text, "{MAP}") !== false)
			{
				$loadmapscript = true;
			}
			$text = str_replace("{MAP}", $map, $text);
		}
		else
		{
			$text = $location->title;
			if (strlen($location->street) > 0)
				$text .= '<br/>' . $location->street;
			if (strlen($location->city) > 0)
				$text .= '<br/>' . $location->city;
			if (strlen($location->state) > 0)
				$text .= "<br/>" . $location->state;
			if (strlen($location->postcode) > 0)
				$text .= "<br/>" . $location->postcode;
			if (strlen($location->country) > 0)
				$text .= "<br/>" . $location->country;
			if (strlen($location->phone) > 0)
				$text .= "<br/>" . $location->phone;

			if (strlen($location->url) > 0)
			{
				$pattern = '[a-zA-Z0-9&?_.,=%\-\/]';
				if (strpos($location->url, "http://") === false)
					$location->url = "http://" . trim($location->url);
				$location->url = preg_replace('#(http://)(' . $pattern . '*)#i', '<a href="\\1\\2"  target="_blank">\\1\\2</a>', $location->url);
				$text          .= "<br/>" . $location->url;
			}

			$map           = '<div id="gmap' . $locid . '"></div>';
			$location->map = $map;
			$width         = $this->params->get('gwidth', 200);
			$height        = $this->params->get('gheight', 150);
			$doc->addStyleDeclaration("#gmap$locid img {max-width: none !important;} #gmap$locid {width:" . $width . "px; height:" . $height . "px;overflow:hidden !important;}");

			$loadmapscript = true;
			$text          .= $map;
			if ($this->params->get("showdesc", 0) && strlen($location->description) > 0)
			{
				$text .= "<br/>" . $location->description;
			}
		}

		if ($loadmapscript)
		{
			$doc->addScriptDeclaration($location->mapscript);
			$location->mapscriptloaded = true;
		}
		return $text;

	}

}
