<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;
if (isset($_GET['test'])) ini_set('display_errors',1);
$monthArr = array(
    '01' => JText::_('JANUARY'),//'Gennaio',
    '02' => JText::_('FEBRUARY'),//'Febbraio',
    '03' => JText::_('MARCH'),//'Marzo',
    '04' => JText::_('APRIL'),//'Aprile',
    '05' => JText::_('MAY'),//'Maggio',
    '06' => JText::_('JUNE'),//'Giugno',
    '07' => JText::_('JULY'),//'Luglio',
    '08' => JText::_('AUGUST'),//'Agosto',
    '09' => JText::_('SEPTEMBER'),//'Settembre',
    '10' => JText::_('OCTOBER'),//'Ottobre',
    '11' => JText::_('NOVEMBER'),//'Novembre',
    '12' => JText::_('DECEMBER')//'Dicembre'
);

$room_types = [
    1 => 'COM_RUSSIANTOUR_ROOM_TYPE_STANDARD',
    12 => 'COM_RUSSIANTOUR_ROOM_TYPE_ECONOMY',
    15 => 'COM_RUSSIANTOUR_ROOM_TYPE_JUNIOR_SUITE',
    19 => 'COM_RUSSIANTOUR_ROOM_TYPE_SUPERIOR',
    2 => 'COM_RUSSIANTOUR_ROOM_TYPE_BUSINESS',
    28 => 'COM_RUSSIANTOUR_ROOM_TYPE_STUDIO',
    3 => 'COM_RUSSIANTOUR_ROOM_TYPE_LUX',
    4 => 'COM_RUSSIANTOUR_ROOM_TYPE_SUITE',
    7 => 'COM_RUSSIANTOUR_ROOM_TYPE_RENOVATED'
];
//get settings to item
$app = JFactory::getApplication();
$params = $app->getParams();
$rate = $params->get('rate',70);
$language_id = RussiantourHelpersRussiantour::get_language_id();
if (isset($_GET['test3']))
{
    var_dump(JFactory::getLanguage());
    exit;
}
if (count($_POST))
{
    //var_dump($_POST);
    $errors = array();
    $body = $_SERVER['HTTP_REFERER'].'
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PRENOTAZIONE_ALBERGO').': '.$item->title.'.

';
    $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_DAL).': '.$_POST['dal_day'].'  
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_AL').': '.$_POST['al_day'].'  
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDISTANZE').': '.$_POST['numero_stanze'].'
';
    $n1 = 1;
    while (isset($_POST['hotel_numero_persone_'.$n1])) {
        $n = $_POST['hotel_numero_persone_'.$n1];
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TIPOLOGIADISTANZA).' '.$n1.': '.$_POST['hotel_tipologia_stanza_'.$n1].'
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDIPERSONE').' '.$n1.': '.$n.'
';
        for ($i=1;$i<=$n;$i++)
        {
            if ($_POST['hotel_persona_nome_'.$n1.'_'.$i])
                $body .=  JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA).' '.$i.' '.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NOME').' : '.$_POST['hotel_persona_nome_'.$n1.'_'.$i].'
';
            else
                $errors[] = 'hotel_persona_nome_'.$n1.'_'.$i;

            if ($_POST['hotel_persona_cognome_'.$n1.'_'.$i])
                $body .=  JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA).' '.$i.' '.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_COGNOME').': '.$_POST['hotel_persona_cognome_'.$n1.'_'.$i].'
';
            else
                $errors[] = 'hotel_persona_cognome'.$n1.'_'.$i;

            if ($_POST['hotel_persona_sesso_'.$n1.'_'.$i]=='0')
                $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_MALE).'
';
            elseif ($_POST['hotel_persona_sesso_'.$n1.'_'.$i]=='1')
                $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_FEMALE).'
';
            elseif ($_POST['hotel_persona_sesso_'.$n1.'_'.$i]=='2')
                $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_BAMBINO).'
';
        }
        $n1++;
    }
    if (!$_POST['mail'] || count(preg_match('/.+@.+\..+/', $_POST['mail'])) < 1)
        $errors[] = 'mail';
    else
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL).': '.$_POST['mail'].'
';

    if (!$_POST['telefono'] || count(preg_match('/.+@.+\..+/', $_POST['telefono'])) < 1)
        $errors[] = 'telefono';
    else
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TELEFONO).': '.$_POST['telefono'].'
';

    if (isset($_POST['hotel_invito']) && $_POST['hotel_invito'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX1).'
';

    if (isset($_POST['hotel_procedura_completa']) && $_POST['hotel_procedura_completa'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX2).'
';

    if (isset($_POST['hotel_volo_aereo']) && $_POST['hotel_volo_aereo'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX3).'
';

    if (isset($_POST['hotel_transfers']) && $_POST['hotel_transfers'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX4).'
';

    if (isset($_POST['hotel_escursioni']) && $_POST['hotel_escursioni'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX5).'
';

    if ($_POST['parlo']=='0')
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_RADIO1).'
';
    elseif ($_POST['parlo']=='1')
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_RADIO2).'
';
    $mainframe= JFactory::getApplication();
    $MailFrom	= $mainframe->getCfg('mailfrom');
    $SiteName	= $mainframe->getCfg('sitename');

    if(JFactory::getMailer()->sendMail($MailFrom, $SiteName, ['info@russiantour.com','support@russiantour.com'], JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL_SUBJ'), $body))
    {
        echo '


<div data-uk-alert="" class="uk-alert uk-alert-large uk-alert-notice">
<button class="uk-alert-close uk-close" type="button"></button>
<h2>'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL_AVVISO').'</h2>
<p>'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL_SENDED').'</p>
</div>';
        $notshow = true;
    }
}
$mopt = '';
foreach ($monthArr as $k=>$v){
    if ($k == $tomonth)
        $sel = 'selected="" ';
    else
        $sel = '';
    $mopt .= '<option '.$sel.'value="'.$k.'"> '.$v.'</option>';
}
?>
<script src="/templates/yoo_eat/warp/vendor/uikit/js/components/lightbox.js"></script>
<article id="flexicontent" class="flexicontent fcitems fcitem469 fctype2 fcmaincat55 menuitem502 group">

    <header class="group">

        <!-- BOF item title -->
        <h1 class="contentheading"><span class="fc_item_title"><?php echo $this->item->title; ?></span></h1>
        <!-- EOF item title -->


        <?php
        $stanza = array();
        $stanza_arr = [];
        $cat = false;
        foreach($this->item->index as $field)
        {
            if($field->field_id == 19 && $field->search_index) {
                $title = JText::_( 'COM_RUSSIANTOUR_STANZA',true );
                //$mode = 's';
                $stanza_arr[] = JText::_( $room_types[$field->value_id],true );
            }
            elseif ($field->field_id == 28 && $field->search_index) {
                $title = JText::_( 'COM_RUSSIANTOUR_PREZZO',true );
                //$mode = 'p';
                $stanza[] = round($field->search_index);
            }
            elseif ($field->field_id == 17 && $field->search_index) {
                $cat = $field->search_index;
            }
        }
        /*if ($mode == 's')
            $text = implode(", ", $stanza);*/
        //elseif ($mode == 'p')
        {
            if ($language_id == 5)
                $text = $this->item->cost_from_rub.' - '.$this->item->cost_to_rub.' руб.';
            else
                $text = round($this->item->cost_from_rub/$rate).' - '.round($this->item->cost_to_rub/$rate).'€.';
            if (count($stanza)==1)
            {
                /*$stanza[] = round($stanza[0]);
                $text = implode(" - ",  $stanza).'€';*/
                $text = round($this->item->cost_from_rub/$rate).' - '.round($this->item->cost_to_rub/$rate).'€.';
            }
        }
        ?>
        <!-- EOF subtitle1 block -->
        <?php if ($cat) { $cat = explode(' ',$cat); ?>
        <div class="uk-margin-small-top uk-margin-left">
            <span class="uk-badge"><?php echo JText::_( 'COM_RUSSIANTOUR_CATEGORIA',true ); ?></span>
            <?php echo $cat[0].' '.JText::_( 'COM_RUSSIANTOUR_STELLE',true ); ?>
        </div>
        <?php } ?>
        <!-- BOF subtitle1 block -->
        <div class="flexi lineinfo subtitle1 group">
            <div class="uk-margin-small-top uk-margin-left">
                <span class="uk-badge"><?php echo JText::_( 'COM_RUSSIANTOUR_PREZZO',true ) ?></span>
                <?php echo $text ?>
            </div>
        </div>

        <?php if (count($stanza_arr)) { ?>
        <div class="flexi lineinfo subtitle1 group">
            <div class="uk-margin-small-top uk-margin-left">
                <span class="uk-badge"><?php echo JText::_( 'COM_RUSSIANTOUR_STANZA',true );; ?></span>
                <?php echo implode(",",  $stanza_arr); ?>
            </div>
        </div>
        <?php } ?>
    </header>


    <!-- BOF description -->
    <div class="uk-margin-top uk-text-justify">
        <?php echo nl2br($this->item->introtext); ?>
    </div>
    <!-- EOF description -->


    <div class="fcclear"></div>

    <section>
        <!-- tabber start -->

        <div class="uk-width-medium-1-1 uk-row-first">

            <ul class="uk-tab" data-uk-switcher="{connect:'#tab-content'}" onclick="initMap()">
                <li class="uk-active" aria-expanded="true"><a href="#"> <?php echo JText::_( 'COM_RUSSIANTOUR_FOTO',true ); ?></a></li>
                <li aria-expanded="false" class=""><a href="#"><?php echo JText::_( 'COM_RUSSIANTOUR_MAPPA',true ); ?></a></a></li>
                <li aria-expanded="false" class=""><a href="#"><?php echo JText::_( 'COM_RUSSIANTOUR_PRENOTAZIONE',true ); ?></a></li>
            </ul>

            <ul id="tab-content" class="uk-switcher uk-margin">
                <li class="uk-active" aria-hidden="false">
                    <p class="gallery">
                        <?php
                        foreach($this->item->images as $image)
                        {
                            echo '
                        <a  data-uk-lightbox="{group:\'group1\'}"  href="'.$image->full_image_path.'" class="fancybox" data-fancybox-group="fcview_item_fcitem_'.$this->item->reference_id.'_fcfield_15" title="'.$this->item->title.'">
                            <img src="'.$image->small_image_path.'" alt="'.$this->item->title.'" class="fc_field_image"/>
                        </a>';
                            if (isset($_GET['tets']))
                            {
                                echo '
                        <a  data-uk-lightbox="{group:\'group1\'}"  href="'.$image->full_image_path.'" class="fancybox" data-fancybox-group="fcview_item_fcitem_'.$this->item->reference_id.'_fcfield_15" title="'.$this->item->title.'">
                            <img src="'.$image->small_image_path.'" alt="'.$this->item->title.'" class="fc_field_image"/>
                        </a>';
                            }
                        }
                        ?>

                    </p>
                </li>
                <li aria-hidden="true" class="">
                    <h3 class="tabberheading"><?php echo JText::_( 'COM_RUSSIANTOUR_MAPPA',true ); ?></h3><!-- tab title -->
                    <div id="fc_bottom_tab3" class="tabbertab" title="">
                        <div class="flexi lineinfo">
                            <div class="flexi element">
                                <div class="flexi value field_field16">
                                    <div class="map">
                                        <style>
                                            #map {
                                                height: 400px;
                                                width: 100%;
                                            }
                                        </style>
                                        <div id="map"></div>
                                    </div>
                                    <div class="addr1"><?=$this->item->address->addr?></div>
                                    <div class="city-state-zip">
                                        <span class="city"><?=$this->item->address->city?></span>,
                                        <span class="province"><?=$this->item->address->province?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
                <li aria-hidden="true" class="">
                    <div  >
                        <h3 class="tabberheading"><?php echo JText::_( 'COM_RUSSIANTOUR_PRENOTAZIONE',true ); ?></h3><!-- tab title -->
                        <div class="flexi lineinfo">
                            <div class="flexi element">
                                <?php if (!isset($notshow)) { ?>
                                <form class="HotelForm uk-form" method="post" action="">
                                    <div >
                                        <div class="uk-text-justify">
                                            <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_INFO'); ?>
                                        </div> </div>
                                    <div class="uk-grid">
                                        <div class="uk-width-medium-1-1">
                                            <div class="uk-width-medium-1-1">
                            
                                                 <div class="day uk-width-medium-1-10">
												 
												 <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_DAL'); ?><span class="uk-text-danger">*</span>
												  </div>
												 <div class="day uk-width-medium-1-20">
<?php echo JHtml::_('calendar', '', "dal_day", "hotel_dal_day", '%d/%m/%Y', array('placeholder'=>"DAL")); ?>
                                                 
   </div>
                                            </div>
                                            <div class="uk-width-medium-1-1">

                                                 <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_AL'); ?>  &nbsp;&nbsp;<span class="uk-text-danger">*</span>
<?php echo JHtml::_('calendar', '', "al_day", "hotel_al_day", '%d/%m/%Y', array('placeholder'=>"AL")); ?>
                                               
                                            </div>
                                            <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDISTANZE'); ?>:<span class="red">*</span>
                                            <select onchange="setStanze(this)" id="hotel_numero_stanze" style="
    width: 198px;
"  name="numero_stanze">
                                                <option selected="" value="1"> 1</option>
                                                <option value="2"> 2</option>
                                                <option value="3"> 3</option>
                                                <option value="4"> 4</option>
                                                <option value="5"> 5</option>
                                                <option value="6"> 6</option>
                                                <option value="7"> 7</option>
                                                <option value="8"> 8</option>
                                                <option value="9"> 9</option>
                                                <option value="10"> 10</option>
                                            </select>
                                            </p></div> </div>
                                    <div id="StanzeBox">
                                        <div class="StanzeBlock">
                                            <h4> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_STANZA'); ?> 1 </h4>
                                            <div >
                                                <div class="HotelFormStanze s10">
                                                     <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TIPOLOGIADISTANZA'); ?> :
                                                </div><div>
                                                    <input type="text" value="" class="txt hotel_tipologia_stanza" id="hotel_tipologia_stanza_1"  style="width: 192px;" name="hotel_tipologia_stanza_1"></div>
                                            </div>
                                            <div >
                                                <div class="HotelFormStanze s10">
                                                     <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDIPERSONE'); ?>:<span class="red">*</span>
                                                </div>
                                                <div><select onchange="setPersona(this,1)"  style="width: 203px;" id="hotel_numero_persone_1" name="hotel_numero_persone_1">
                                                        <option value="1"> 1</option>
                                                        <option value="2"> 2</option>
                                                        <option value="3"> 3</option>
                                                        <option value="4"> 4</option>
                                                        <option value="5"> 5</option>
                                                    </select></div>
                                            </div>
                                            <div class="PersonaBox" id="PersonaBox_1">
                                                <div class="StanzeBlock">
                                                    <h4> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA'); ?> </h4>
                                                    <div >
                                                        <div class="s10"><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NOME'); ?>:<span class="red">*</span></div>
                                                        <div><input req="req" type="text" value="" class="txt hotel_persona_nome" style=" width: 192px;"  id="hotel_persona_nome_1_1" name="hotel_persona_nome_1_1"></div>
                                                    </div>
                                                    <div >
                                                        <div class="s10"><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_COGNOME'); ?>:<span class="red">*</span></div>
                                                        <div><input req="req" type="text" value="" class="txt hotel_persona_cognome" id="hotel_persona_cognome_1_1" style=" width: 192px;"  name="hotel_persona_cognome_1_1"></div>
                                                    </div>
                                                    <div >
                                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" checked="" value="0" name="hotel_persona_sesso_1_1"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_MALE'); ?>
                                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" value="1" name="hotel_persona_sesso_1_1"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_FEMALE'); ?> 
                                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" value="2" name="hotel_persona_sesso_1_1"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_BAMBINO'); ?>  
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hr"></div>
                                    <div >
                                        <div class="s10"><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL'); ?>:<span class="red">*</span></div>
                                        <div><input req="req" type="text" value="" class="txt"  style=" width: 192px;"  id="hotel_indirizzo_mail" name="mail"></div>
                                    </div>
                                    <div >
                                        <div class="s10"><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TELEFONO'); ?>:</div>
                                        <div><input  type="text" value="" class="txt"  style=" width: 192px;"  id="hotel_telefono" name="telefono"></div>
                                    </div>
                                    <div class="hr"></div>
                                    <p><strong><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TEXT1'); ?>:</strong></p>
																					    
                                    <p> </p>
                                    <p><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TEXT2'); ?>:</p>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_invito"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX1'); ?>
																			     
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_procedura_completa"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX2'); ?>  
																			     
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_volo_aereo"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX3'); ?> 
																			     
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_transfers"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX4'); ?> 
																			    
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_escursioni"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX5'); ?>  
                                    </div>
                                    <div class="hr"></div>
                                    <div >
                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" checked="" value="0" name="parlo"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_RADIO1'); ?>
																			
                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" value="1" name="parlo"> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_RADIO2'); ?>
																			
                                    </div>
                                    <table  style="width: 100%;" class="subm">
                                        <tbody>
                                        <tr>
                                            <td class="submbutton">
                                                <input type="submit" class="uk-button uk-button-primary button-soggiorno" value="<?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_SUBBUTTON'); ?>"  "" name="sub">
                                                <div style="padding-top: 10px; text-align: left;">
                                                    <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_SUBTEXT'); ?> 
                                                </div>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="smail" value="1">
                                </form>
                                <div id="HiddenStanze">
                                    <div class="StanzeBlock">
                                        <h3><span><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_STANZA'); ?> ёъ.</span></h3>
                                        <div >
                                            <div class="HotelFormStanze">
                                                 <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TIPOLOGIADISTANZA'); ?>:
                                            </div>
                                            <input type="text" name="hotel_tipologia_stanza_ёъ" id="hotel_tipologia_stanza_ёъ" class="txt hotel_tipologia_stanza" />
                                        </div>
                                        <div >
                                            <div class="HotelFormStanze">
                                                <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDIPERSONE'); ?>:<span class="red">*</span>
                                            </div>
                                            <select name="hotel_numero_persone_ёъ" id="hotel_numero_persone_ёъ" onchange="setPersona(this,ёъ)">
                                                <option value="1"> 1</option>
                                                <option value="2"> 2</option>
                                                <option value="3"> 3</option>
                                                <option value="4"> 4</option>
                                                <option value="5"> 5</option>
                                            </select>
                                        </div>
                                        <div id="PersonaBox_ёъ" class="PersonaBox">
                                            <div class="StanzeBlock">
                                                <h3><span><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA'); ?> 1.</span></h3>
                                                <div >
                                                    <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NOME'); ?>:<span class="red">*</span>
                                                    <input req="req" type="text" name="hotel_persona_nome_ёъ_1" style="margin-left: 70px; width: 193px;" id="hotel_persona_nome_ёъ_1" class="txt hotel_persona_nome" />
                                                </div>
                                                <div >
                                                    <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_COGNOME'); ?>:<span class="red">*</span>
                                                    <input req="req" type="text" name="hotel_persona_cognome_ёъ_1" id="hotel_persona_cognome_ёъ_1" class="txt hotel_persona_cognome" />
                                                </div>
                                                <div >
                                                    <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_1" value="0" /> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_MALE'); ?>
                                                    <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_1" value="1" /> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_FEMALE'); ?> 
                                                    <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_1" value="2" /> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_BAMBINO'); ?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="HiddenPersona">
                                    <div class="StanzeBlock">
                                        <h3><span><?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA'); ?> </span></h3>
                                        <div >
                                            <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NOME'); ?>:<span class="red">*</span>
                                            <input req="req" type="text" name="hotel_persona_nome_ёъ_ъь" id="hotel_persona_nome_ёъ_ъь" class="txt hotel_persona_nome" />
                                        </div>
                                        <div >
                                            <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_COGNOME'); ?>:<span class="red">*</span>
                                            <input req="req" type="text" name="hotel_persona_cognome_ёъ_ъь" id="hotel_persona_cognome_ёъ_ъь" class="txt hotel_persona_cognome" />
                                        </div>
                                        <div >
                                            <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_ъь" value="0" /> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_MALE'); ?>
                                            <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_ъь" value="1" />  <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_FEMALE'); ?>
                                            <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_ъь" value="2" /> <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_BAMBINO'); ?>
                                        </div>
                                    </div>
                                </div>
                                <!--script src="https://russiantour.com/plugins/flexicontent_fields/hotelreservation/js/script.js"></script-->
                                <script src="/components/com_russiantour/js/script.js?v1"></script>
                                <?php } ?>

                            </div>
                        </div>

                    </div>




                </li>
            </ul>

        </div>





        <div class="fcclear"></div>


    </section>

</article>
<script>
    function initMap() {
        if (jQuery('#map:visible').length)
        {
            var uluru = {lat: <?=$this->item->address->lat?>, lng: <?=$this->item->address->lon?>};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
        else
            setTimeout(function(){initMap()},500);
    }
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9kZ7fBYgmyciBRzmUBAzXjcdFGRMJceo&callback=initMap">
</script>
<script>
    window.onresize = function(){
        initMap();
    };
</script>
<style>
    #HiddenStanze, #HiddenPersona {
        display: none;
    }
</style>