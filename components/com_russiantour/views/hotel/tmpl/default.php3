<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

$monthArr = array(
    '01' => JText::_('JANUARY'),//'Gennaio',
    '02' => JText::_('FEBRUARY'),//'Febbraio',
    '03' => JText::_('MARCH'),//'Marzo',
    '04' => JText::_('APRIL'),//'Aprile',
    '05' => JText::_('MAY'),//'Maggio',
    '06' => JText::_('JUNE'),//'Giugno',
    '07' => JText::_('JULY'),//'Luglio',
    '08' => JText::_('AUGUST'),//'Agosto',
    '09' => JText::_('SEPTEMBER'),//'Settembre',
    '10' => JText::_('OCTOBER'),//'Ottobre',
    '11' => JText::_('NOVEMBER'),//'Novembre',
    '12' => JText::_('DECEMBER')//'Dicembre'
);
if (count($_POST))
{
    //var_dump($_POST);
    $errors = array();
    $body = $_SERVER['HTTP_REFERER'].'
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PRENOTAZIONE_ALBERGO').': '.$item->title.'.

';
    $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_DAL).': '.$_POST['dal_day'].' '.$monthArr[$_POST['dal_month']].' '.$_POST['dal_year'].'
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_AL').': '.$_POST['al_day'].' '.$monthArr[$_POST['al_month']].' '.$_POST['al_year'].'
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDISTANZE').': '.$_POST['numero_stanze'].'
';
    $n1 = 1;
    while (isset($_POST['hotel_numero_persone_'.$n1])) {
        $n = $_POST['hotel_numero_persone_'.$n1];
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TIPOLOGIADISTANZA).' '.$n1.': '.$_POST['hotel_tipologia_stanza_'.$n1].'
'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NDIPERSONE').' '.$n1.': '.$n.'
';
        for ($i=1;$i<=$n;$i++)
        {
            if ($_POST['hotel_persona_nome_'.$n1.'_'.$i])
                $body .=  JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA).' '.$i.' '.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_NOME').' : '.$_POST['hotel_persona_nome_'.$n1.'_'.$i].'
';
            else
                $errors[] = 'hotel_persona_nome_'.$n1.'_'.$i;

            if ($_POST['hotel_persona_cognome_'.$n1.'_'.$i])
                $body .=  JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_PERSONA).' '.$i.' '.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_COGNOME').': '.$_POST['hotel_persona_cognome_'.$n1.'_'.$i].'
';
            else
                $errors[] = 'hotel_persona_cognome'.$n1.'_'.$i;

            if ($_POST['hotel_persona_sesso_'.$n1.'_'.$i]=='0')
                $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_MALE).'
';
            elseif ($_POST['hotel_persona_sesso_'.$n1.'_'.$i]=='1')
                $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_FEMALE).'
';
            elseif ($_POST['hotel_persona_sesso_'.$n1.'_'.$i]=='2')
                $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_BAMBINO).'
';
        }
        $n1++;
    }
    if (!$_POST['mail'] || count(preg_match('/.+@.+\..+/', $_POST['mail'])) < 1)
        $errors[] = 'mail';
    else
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL).': '.$_POST['mail'].'
';

    if (!$_POST['telefono'] || count(preg_match('/.+@.+\..+/', $_POST['telefono'])) < 1)
        $errors[] = 'telefono';
    else
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_TELEFONO).': '.$_POST['telefono'].'
';

    if (isset($_POST['hotel_invito']) && $_POST['hotel_invito'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX1).'
';

    if (isset($_POST['hotel_procedura_completa']) && $_POST['hotel_procedura_completa'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX2).'
';

    if (isset($_POST['hotel_volo_aereo']) && $_POST['hotel_volo_aereo'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX3).'
';

    if (isset($_POST['hotel_transfers']) && $_POST['hotel_transfers'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX4).'
';

    if (isset($_POST['hotel_escursioni']) && $_POST['hotel_escursioni'])
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_CHECKBOX5).'
';

    if ($_POST['parlo']=='0')
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_RADIO1).'
';
    elseif ($_POST['parlo']=='1')
        $body .= JText::_(PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_RADIO2).'
';
    $mainframe= JFactory::getApplication();
    $MailFrom	= $mainframe->getCfg('mailfrom');
    $SiteName	= $mainframe->getCfg('sitename');

    if(JFactory::getMailer()->sendMail($MailFrom, $SiteName, ['info@russiantour.com','support@russiantour.com'], JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL_SUBJ'), $body))
    {
        echo '<div data-uk-alert="" class="uk-alert uk-alert-large uk-alert-notice">
<button class="uk-alert-close uk-close" type="button"></button>
<h2>AVVISO</h2>
<p>'.JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_EMAIL_SENDED').'</p>
</div>';
        $notshow = true;
    }
}
$mopt = '';
foreach ($monthArr as $k=>$v){
    if ($k == $tomonth)
        $sel = 'selected="" ';
    else
        $sel = '';
    $mopt .= '<option '.$sel.'value="'.$k.'"> '.$v.'</option>';
}
?>
<article id="flexicontent" class="flexicontent fcitems fcitem469 fctype2 fcmaincat55 menuitem502 group">

    <header class="group">

        <!-- BOF item title -->
        <h1 class="contentheading"><span class="fc_item_title"><?php echo $this->item->title; ?></span></h1>
        <!-- EOF item title -->




        <!-- BOF subtitle1 block -->
        <div class="flexi lineinfo subtitle1 group">
            <div class="uk-margin-small-top uk-margin-left">
                <?php
                $stanza = array();
                $cat = false;
                foreach($this->item->index as $field)
                {
                    if($field->field_id == 19 && $field->search_index) {
                        $title = JText::_( 'COM_RUSSIANTOUR_STANZA',true );
                        $mode = 's';
                        $stanza[] = $field->search_index;
                    }
                    elseif ($field->field_id == 28 && $field->search_index) {
                        $title = JText::_( 'COM_RUSSIANTOUR_PREZZO',true );
                        $mode = 'p';
                        $stanza[] = $field->search_index;
                    }
                    elseif ($field->field_id == 17 && $field->search_index) {
                        $cat = $field->search_index;
                    }
                }
                if ($mode == 's')
                    $text = implode(", ", $stanza);
                elseif ($mode == 'p')
                {
                    if (count($stanza)==1)
                        $stanza[] = $stanza[0];
                    $text = implode(" - ",  $stanza).'€';
                }
                ?>
                <span class="uk-badge"><?php echo $title; ?></span>
                <?php echo $text; ?>
            </div>
        </div>
        <!-- EOF subtitle1 block -->
        <?php if ($cat) { $cat = explode(' ',$cat); ?>
        <div class="uk-margin-small-top uk-margin-left">
            <span class="uk-badge"><?php echo JText::_( 'COM_RUSSIANTOUR_CATEGORIA',true ); ?></span>
            <?php echo $cat[0].' '.JText::_( 'COM_RUSSIANTOUR_STELLE',true ); ?>
        </div>
        <?php } ?>

    </header>


    <!-- BOF description -->
    <div class="uk-margin-top uk-text-justify">
        <?php echo nl2br($this->item->introtext); ?>
    </div>
    <!-- EOF description -->


    <div class="fcclear"></div>

    <section>
        <!-- tabber start -->

        <div class="uk-width-medium-1-1 uk-row-first">

            <ul class="uk-tab" data-uk-switcher="{connect:'#tab-content'}" onclick="initMap()">
                <li class="uk-active" aria-expanded="true"><a href="#">Foto</a></li>
                <li aria-expanded="false" class=""><a href="#">Mappa</a></li>
                <li aria-expanded="false" class=""><a href="#">Prenotazione</a></li>
            </ul>

            <ul id="tab-content" class="uk-switcher uk-margin">
                <li class="uk-active" aria-hidden="false">


                    <p class="gallery">


                        <?php
                        foreach($this->item->fields as $field)
                        {
                            if($field->field_id == 15)
                            {
                                $fields15 = unserialize( $field->value );

                                $img_name = $fields15['originalname'];
                                $img_alt = $fields15['alt'];
                                $img_title = $fields15['title'];
                                echo '
									 <a data-lightbox="group:mygroup1;titlePosition:float" href="/images/stories/flexicontent/item_'.$this->item->reference_id.'_field_15/l_'.$img_name.'" class="fancybox" data-fancybox-group="fcview_item_fcitem_'.$this->item->reference_id.'_fcfield_15" title="'.$img_title.'">
									<img src="/images/stories/flexicontent/item_'.$this->item->reference_id.'_field_15/s_'.$img_name.'" alt="'.$this->item->title.'" class="fc_field_image"/>
									</a>';
                                //echo '<pre>'; print_r(  ); echo '</pre>';
                            }
                        }
                        ?>

                    </p>
                </li>
                <li aria-hidden="true" class="">
                    <?php
                    foreach($this->item->fields as $field)
                    {
                        if($field->field_id == 16)
                        {
                            $field_16 = unserialize( $field->value );
                        }
                    }
                    ?>
                    <h3 class="tabberheading">Mappa</h3><!-- tab title --> <div id="fc_bottom_tab3" class="tabbertab" title="">

                        <div class="flexi lineinfo">
                            <div class="flexi element">
                                <div class="flexi value field_field16"><div class="map">
                                        <?php /*<a href="https://maps.google.com/maps?q=<?=$field_16['lat']?>,<?=$field_16['lon']?>" target="_blank">
                                        <img src="https://maps.google.com/maps/api/staticmap?center=<?=$field_16['lat']?>,<?=$field_16['lon']?>&amp;zoom=15&amp;size=1000x300&amp;maptype=roadmap&amp;markers=size:normal%7Ccolor:orange%7C|<?=$field_16['lat']?>,<?=$field_16['lon']?>&amp;sensor=false" width="1000" height="300">
                                        <br>Click Map for Directions
                                    </a> */?>
                                        <style>
                                            #map {
                                                height: 400px;
                                                width: 100%;
                                            }
                                        </style>
                                        <div id="map"></div>
                                    </div><div class="addr1"><?=$field_16['addr1']?></div><div class="city-state-zip"><span class="city"><?=$field_16['city']?></span>, <span class="province"><?=$field_16['province']?></span> </div></div>
                            </div>
                        </div>
                    </div>
                </li>
                <li aria-hidden="true" class="">
                    <div  >
                        <h3 class="tabberheading">Prenotazione</h3><!-- tab title -->
                        <div class="flexi lineinfo">
                            <div class="flexi element">
                                <?php if (!isset($notshow)) { ?>
                                <form class="HotelForm uk-form" method="post" action="">
                                    <div >
                                        <div class="uk-text-justify">
                                            <?php echo JText::_('PLG_FLEXICONTENT_FIELDS_HOTELRESERVATION_INFO'); ?>
                                        </div> </div>
                                    <div class="uk-grid">
                                        <div class="uk-width-medium-1-1">
                                            <div class="uk-width-medium-1-2">

                                                Dal:<span class="uk-text-danger">*</span>

                                                <select class="day uk-width-medium-1-10"  id="hotel_dal_day" name="dal_day">
                                                    <option value="01"> 01</option><option value="02"> 02</option><option value="03"> 03</option><option value="04"> 04</option><option value="05"> 05</option><option value="06"> 06</option><option value="07"> 07</option><option value="08"> 08</option><option value="09"> 09</option><option value="10"> 10</option><option value="11"> 11</option><option value="12"> 12</option><option value="13"> 13</option><option value="14"> 14</option><option value="15"> 15</option><option value="16"> 16</option><option value="17"> 17</option><option value="18"> 18</option><option value="19"> 19</option><option value="20"> 20</option><option value="21"> 21</option><option value="22"> 22</option><option value="23"> 23</option><option value="24"> 24</option><option selected="" value="25"> 25</option><option value="26"> 26</option><option value="27"> 27</option><option value="28"> 28</option><option value="29"> 29</option><option value="30"> 30</option><option value="31"> 31</option>
                                                </select>
                                                <select class="month uk-width-medium-2-10" id="hotel_dal_month" name="dal_month">
                                                    <?php echo $mopt; ?>
                                                </select>
                                                <select class="year uk-width-medium-1-10" id="hotel_dal_year" name="dal_year">
                                                    <option selected="" value="2017"> 2017</option>
                                                    <option value="2018"> 2018</option>
                                                    <option value="2019"> 2019</option>
                                                </select>

                                            </div>
                                            <div class="uk-width-medium-1-2">

                                                Al:<span class="uk-text-danger">*</span>

                                                <select class="day" id="hotel_al_day" name="al_day">
                                                    <option value="01"> 01</option><option value="02"> 02</option><option value="03"> 03</option><option value="04"> 04</option><option value="05"> 05</option><option value="06"> 06</option><option value="07"> 07</option><option value="08"> 08</option><option value="09"> 09</option><option value="10"> 10</option><option value="11"> 11</option><option value="12"> 12</option><option value="13"> 13</option><option value="14"> 14</option><option value="15"> 15</option><option value="16"> 16</option><option value="17"> 17</option><option value="18"> 18</option><option value="19"> 19</option><option value="20"> 20</option><option value="21"> 21</option><option value="22"> 22</option><option value="23"> 23</option><option value="24"> 24</option><option selected="" value="25"> 25</option><option value="26"> 26</option><option value="27"> 27</option><option value="28"> 28</option><option value="29"> 29</option><option value="30"> 30</option><option value="31"> 31</option>
                                                </select>
                                                <select class="month" id="hotel_al_month" name="al_month">
                                                    <option value="01"> Gennaio</option><option value="02"> Febbraio</option><option value="03"> Marzo</option><option value="04"> Aprile</option><option value="05"> Maggio</option><option value="06"> Giugno</option><option value="07"> Luglio</option><option value="08"> Agosto</option><option value="09"> Settembre</option><option selected="" value="10"> Ottobre</option><option value="11"> Novembre</option><option value="12"> Dicembre</option>
                                                </select>
                                                <select class="year" id="hotel_al_year" name="al_year">
                                                    <option selected="" value="2017"> 2017</option>
                                                    <option value="2018"> 2018</option>
                                                    <option value="2019"> 2019</option>
                                                </select>
                                            </div>
                                            Numero di stanze:<span class="red">*</span>
                                            <select onchange="setStanze(this)" id="hotel_numero_stanze" style="
    width: 198px;
"  name="numero_stanze">
                                                <option selected="" value="1"> 1</option>
                                                <option value="2"> 2</option>
                                                <option value="3"> 3</option>
                                                <option value="4"> 4</option>
                                                <option value="5"> 5</option>
                                                <option value="6"> 6</option>
                                                <option value="7"> 7</option>
                                                <option value="8"> 8</option>
                                                <option value="9"> 9</option>
                                                <option value="10"> 10</option>
                                            </select>
                                            </p></div> </div>
                                    <div id="StanzeBox">
                                        <div class="StanzeBlock">
                                            <h4> Stanza 1 </h4>
                                            <div >
                                                <div class="HotelFormStanze s10">
                                                    Tipologia di stanza:
                                                </div><div>
                                                    <input type="text" value="" class="txt hotel_tipologia_stanza" id="hotel_tipologia_stanza_1"  style="width: 192px;" name="hotel_tipologia_stanza_1"></div>
                                            </div>
                                            <div >
                                                <div class="HotelFormStanze s10">
                                                    Numero di persone:<span class="red">*</span>
                                                </div>
                                                <div><select onchange="setPersona(this,1)"  style="width: 203px;" id="hotel_numero_persone_1" name="hotel_numero_persone_1">
                                                        <option value="1"> 1</option>
                                                        <option value="2"> 2</option>
                                                        <option value="3"> 3</option>
                                                        <option value="4"> 4</option>
                                                        <option value="5"> 5</option>
                                                    </select></div>
                                            </div>
                                            <div class="PersonaBox" id="PersonaBox_1">
                                                <div class="StanzeBlock">
                                                    <h4> Persona </h4>
                                                    <div >
                                                        <div class="s10">Nome:<span class="red">*</span></div>
                                                        <div><input req="req" type="text" value="" class="txt hotel_persona_nome" style=" width: 192px;"  id="hotel_persona_nome_1_1" name="hotel_persona_nome_1_1"></div>
                                                    </div>
                                                    <div >
                                                        <div class="s10">Cognome:<span class="red">*</span></div>
                                                        <div><input req="req" type="text" value="" class="txt hotel_persona_cognome" id="hotel_persona_cognome_1_1" style=" width: 192px;"  name="hotel_persona_cognome_1_1"></div>
                                                    </div>
                                                    <div >
                                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" checked="" value="0" name="hotel_persona_sesso_1_1"> M
                                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" value="1" name="hotel_persona_sesso_1_1"> F
                                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" value="2" name="hotel_persona_sesso_1_1"> Bambino
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="hr"></div>
                                    <div >
                                        <div class="s10">Indirizzo e-mail:<span class="red">*</span></div>
                                        <div><input req="req" type="text" value="" class="txt"  style=" width: 192px;"  id="hotel_indirizzo_mail" name="mail"></div>
                                    </div>
                                    <div >
                                        <div class="s10">Telefono:</div>
                                        <div><input  type="text" value="" class="txt"  style=" width: 192px;"  id="hotel_telefono" name="telefono"></div>
                                    </div>
                                    <div class="hr"></div>
                                    <p><strong>Per un riscontro piu` tempestivo ed efficiente, cortesemente ci fornisca le seguenti informazioni:</strong></p>
                                    <p> </p>
                                    <p>Ho bisogno anche di:</p>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_invito"> Invito + voucher per il visto (gratuito)
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_procedura_completa"> Procedura completa visto
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_volo_aereo"> Volo aereo
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_transfers"> Transfers
                                    </div>
                                    <div >
                                        <input type="checkbox" value="1" name="hotel_escursioni"> Escursioni
                                    </div>
                                    <div class="hr"></div>
                                    <div >
                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" checked="" value="0" name="parlo"> Parlo solo italiano
                                        <input type="radio" style="display: inline-block !important;opacity: 1 !important;" value="1" name="parlo"> Parlo inglese o russo
                                    </div>
                                    <table  style="width: 100%;" class="subm">
                                        <tbody>
                                        <tr>
                                            <td class="submbutton">
                                                <input type="submit" class="uk-button uk-button-primary" value="INOLTRA LA RICHIESTA" name="sub">
                                                <div style="padding-top: 10px; text-align: left;">
                                                    PRIMA DI PROCEDERE CONTROLLATE ACCURATAMENTE TUTTE LE INFORMAZIONI INSERITE
                                                </div>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <input type="hidden" name="smail" value="1">
                                </form>
                                <div id="HiddenStanze">
                                    <div class="StanzeBlock">
                                        <h3><span>Stanza ёъ.</span></h3>
                                        <div >
                                            <div class="HotelFormStanze">
                                                Tipologia di stanza:
                                            </div>
                                            <input type="text" name="hotel_tipologia_stanza_ёъ" id="hotel_tipologia_stanza_ёъ" class="txt hotel_tipologia_stanza" />
                                        </div>
                                        <div >
                                            <div class="HotelFormStanze">
                                                Numero di persone:<span class="red">*</span>
                                            </div>
                                            <select name="hotel_numero_persone_ёъ" id="hotel_numero_persone_ёъ" onchange="setPersona(this,ёъ)">
                                                <option value="1"> 1</option>
                                                <option value="2"> 2</option>
                                                <option value="3"> 3</option>
                                                <option value="4"> 4</option>
                                                <option value="5"> 5</option>
                                            </select>
                                        </div>
                                        <div id="PersonaBox_ёъ" class="PersonaBox">
                                            <div class="StanzeBlock">
                                                <h3><span>Persona 1.</span></h3>
                                                <div >
                                                    Nome:<span class="red">*</span>
                                                    <input req="req" type="text" name="hotel_persona_nome_ёъ_1" style="margin-left: 70px; width: 193px;" id="hotel_persona_nome_ёъ_1" class="txt hotel_persona_nome" />
                                                </div>
                                                <div >
                                                    Cognome:<span class="red">*</span>
                                                    <input req="req" type="text" name="hotel_persona_cognome_ёъ_1" id="hotel_persona_cognome_ёъ_1" class="txt hotel_persona_cognome" />
                                                </div>
                                                <div >
                                                    <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_1" value="0" /> M
                                                    <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_1" value="1" /> F
                                                    <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_1" value="2" /> Bambino
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="HiddenPersona">
                                    <div class="StanzeBlock">
                                        <h3><span>Persona </span></h3>
                                        <div >
                                            Nome:<span class="red">*</span>
                                            <input req="req" type="text" name="hotel_persona_nome_ёъ_ъь" id="hotel_persona_nome_ёъ_ъь" class="txt hotel_persona_nome" />
                                        </div>
                                        <div >
                                            Cognome:<span class="red">*</span>
                                            <input req="req" type="text" name="hotel_persona_cognome_ёъ_ъь" id="hotel_persona_cognome_ёъ_ъь" class="txt hotel_persona_cognome" />
                                        </div>
                                        <div >
                                            <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_ъь" value="0" /> M
                                            <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_ъь" value="1" /> F
                                            <input type="radio" style="display: inline-block !important;opacity: 1 !important;" name="hotel_persona_sesso_ёъ_ъь" value="2" /> Bambino
                                        </div>
                                    </div>
                                </div>
                                <!--script src="https://images.russiantour.com/plugins/flexicontent_fields/hotelreservation/js/script.js"></script-->
                                <script src="/components/com_russiantour/js/script.js?v1"></script>
                                <?php } ?>

                            </div>
                        </div>

                    </div>




                </li>
            </ul>

        </div>





        <div class="fcclear"></div>


    </section>

</article>
<script>
    function initMap() {
        if (jQuery('#map:visible').length)
        {
            var uluru = {lat: <?=$field_16['lat']?>, lng: <?=$field_16['lon']?>};
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 17,
                center: uluru
            });
            var marker = new google.maps.Marker({
                position: uluru,
                map: map
            });
        }
        else
            setTimeout(function(){initMap()},500);
    }
</script>
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD9kZ7fBYgmyciBRzmUBAzXjcdFGRMJceo&callback=initMap">
</script>
<script>
    window.onresize = function(){
        initMap();
    };
</script>
<script>
    jQuery(document).ready(function(){
        jQuery('.fancybox').fancybox();
    });
</script>
<style>
    #HiddenStanze, #HiddenPersona {
        display: none;
    }
</style>