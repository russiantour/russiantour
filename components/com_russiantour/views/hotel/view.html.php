<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 *
 * @since  1.6
 */
class RussiantourViewHotel extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	protected $params;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  Template name
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	public function display($tpl = null)
	{
		$app  = JFactory::getApplication();
		$user = JFactory::getUser();

		$this->state  = $this->get('State');//var_dump($this->state);exit;
		$this->item   = $this->get('Data');
		$this->params = $app->getParams('com_russiantour');

		if (!empty($this->item))
		{

		}

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			throw new Exception(implode("\n", $errors));
		}

		

		if ($this->_layout == 'edit')
		{
			$authorised = $user->authorise('core.create', 'com_russiantour');

			if ($authorised !== true)
			{
				throw new Exception(JText::_('JERROR_ALERTNOAUTHOR'));
			}
		}

		$this->_prepareDocument();

		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 *
	 * @return void
	 *
	 * @throws Exception
	 */
	protected function _prepareDocument()
	{
		$app   = JFactory::getApplication();
		$menus = $app->getMenu();

		$title = null;

		// Because the application sets a default page title,
		// We need to get it from the menu item itself
		$menu = $menus->getActive();

		if ($menu)
		{
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		}
		else
		{
			$this->params->def('page_heading', JText::_('COM_RUSSIANTOUR_DEFAULT_PAGE_TITLE'));
		}

		$title = $this->params->get('page_title', '');

        if (isset($this->item->title))
            $title = $this->item->title;
		if (empty($title))
		{
			$title = $app->get('sitename');
		}
		elseif ($app->get('sitename_pagetitles', 0) == 1)
		{
			$title = JText::sprintf('JPAGETITLE', $app->get('sitename'), $title);
		}
		elseif ($app->get('sitename_pagetitles', 0) == 2)
		{
			$title = JText::sprintf('JPAGETITLE', $title, $app->get('sitename'));
		}

		$this->document->setTitle($title);

        if (isset($this->item->metadesc))
            $this->document->setDescription($this->item->metadesc);
        if (isset($this->item->metakey))
            $this->document->setMetadata('keywords',$this->item->metakey);
		if ($this->params->get('menu-meta_description'))
		{
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords'))
		{
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots'))
		{
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		$languagesCode = JLanguageHelper::getLanguages('lang_code');
        foreach ($languagesCode as $lang){
            $db = JFactory::getDBO();
            $sql = "SELECT * FROM `#__russiantour_` where reference_id = ".$this->item->reference_id.' and language_id = '.$lang->lang_id;
            if (isset($_GET['test2']))
            {
                var_dump($this->item->id,$this->item);
                echo $sql;
                exit;
            }
            $db->setQuery($sql);
            if ($db->loadObject()==null)
            {
                $app->russiantour_langsToHide[] = $lang->lang_code;
            }
        }
        /*if (isset($app->russiantour_langsToHide))
        {
            foreach ($menus->getItems() as $menu){
                if ($menu->link == 'index.php?option=com_russiantour&view=hotes')
                {
                    $app->russiantour_links[] = $menu->route;
                }
            }
        }*/
	}
}
