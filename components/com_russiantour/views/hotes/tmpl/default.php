<?php 
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');//

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_russiantour') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'hotelform.xml');
$canEdit    = $user->authorise('core.edit', 'com_russiantour') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'hotelform.xml');
$canCheckin = $user->authorise('core.manage', 'com_russiantour');
$canChange  = $user->authorise('core.edit.state', 'com_russiantour');
$canDelete  = $user->authorise('core.delete', 'com_russiantour');

$reference_id = $this->params->get('reference_id');
$filePath = JPATH_COMPONENT.'/include/'.$reference_id.'.php';
if (is_file($filePath))
    include ($filePath);
$room_types = [
            1 => 'COM_RUSSIANTOUR_ROOM_TYPE_STANDARD',
            12 => 'COM_RUSSIANTOUR_ROOM_TYPE_ECONOMY',
            15 => 'COM_RUSSIANTOUR_ROOM_TYPE_JUNIOR_SUITE',
            19 => 'COM_RUSSIANTOUR_ROOM_TYPE_SUPERIOR',
            2 => 'COM_RUSSIANTOUR_ROOM_TYPE_BUSINESS',
            28 => 'COM_RUSSIANTOUR_ROOM_TYPE_STUDIO',
            29 => 'COM_RUSSIANTOUR_ROOM_TYPE_STUDIO2',
            30 => 'COM_RUSSIANTOUR_ROOM_TYPE_DELUX',
	    31 => 'COM_RUSSIANTOUR_ROOM_TYPE_CONFORT',
	    32 => 'COM_RUSSIANTOUR_ROOM_TYPE_APAR',
	    33 => 'COM_RUSSIANTOUR_ROOM_TYPE_ULU',
	    34 => 'COM_RUSSIANTOUR_ROOM_TYPE_OTEL',
	    35 => 'COM_RUSSIANTOUR_ROOM_TYPE_KOTETSH',
	    36 => 'COM_RUSSIANTOUR_ROOM_TYPE_SHALE',
	    37 => 'COM_RUSSIANTOUR_ROOM_TYPE_TAUN',
            3 => 'COM_RUSSIANTOUR_ROOM_TYPE_LUX',
            4 => 'COM_RUSSIANTOUR_ROOM_TYPE_SUITE',
            7 => 'COM_RUSSIANTOUR_ROOM_TYPE_RENOVATED'
];
//get settings to item
$app = JFactory::getApplication();
$params = $app->getParams();
$rate = $params->get('rate',70);
$language_id = RussiantourHelpersRussiantour::get_language_id();
?>
<?php /*echo $this->pagination->total; ?><br/>
<?php echo $this->pagination->limit; ?><br/>
<?php echo $this->pagination->pagesCurrent;*/ ?><br/>
<div id="flexicontent">
    <div class="uk-grid">
        <form class="uk-form"  style="width: 100%;" action="<?php echo $this->document->base; ?>" method="post"
              name="adminForm" id="adminForm">
            <?php $cat = $this->getState('filter.cat',''); ?>
            <?php $cats = $this->getState('filter.cats',''); ?>
            <div class="uk-width-medium-1-2" style=" display: inline-block; ">
                <?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
            </div>
            <div  style=" display: inline-block;width: 49%; ">
                <div  class="uk-form-select uk-text-left" data-uk-form-select>
                    <select class="uk-button" name="filter[cat]">
                        <option value="">- Metro.... -</option>
                        <?php foreach ($this->metro as $v) { ?>
                        <option value="<?php echo $v->id; ?>" <?php if ($v->id==$cat) echo 'selected'; ?>>.&nbsp;&nbsp;&nbsp;'-&nbsp;<?php echo $v->title; ?></option>
                        <?php } ?>
                    </select>
                    <select name="filter[cats]">
                        <option value="">- Categoria -</option>
                        <?php foreach ($this->cats as $v) { ?>
                        <option value="<?php echo $v->value_id; ?>" <?php if ($v->value_id==$cats) echo 'selected'; ?>>.&nbsp;&nbsp;&nbsp;'-&nbsp;<?php echo $v->search_index; ?></option>
                        <?php } ?>
                    </select>
                    <input class="uk-button-primary uk-button "  style=" color: #fff; "  value=" <?php echo JText::_( 'COM_RUSSIANTOUR_SEND',true ); ?>" type="submit">
                </div>
            </div>
            <table class="uk-table" id="flexitable" style="width:97%;">
                <thead>
                    <tr>
                        <th style="  word-break: break-word; ">
                            <?php echo JText::_( 'COM_RUSSIANTOUR_NOME',true ); ?>
                        </th>
                        <th>
                            <?php echo JText::_( 'COM_RUSSIANTOUR_CATEGORIA',true ); ?>
							
                        </th>
                        <th>
                            <?php echo JText::_( 'COM_RUSSIANTOUR_STANZA',true ); ?>
                        </th>
                        <th>
                            <?php echo JText::_( 'COM_RUSSIANTOUR_PREZZO',true ); ?>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr >
                        <td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>

                    </tr>
                </tfoot>
                <tbody>
                    <?php if (isset($_GET['test'])) echo count($this->items); foreach ($this->items as $i => $item) : ?>
                    <?php if (!$item->max_price && !$item->min_price) continue; ?>
                    <tr>
                        <td style="  word-break: break-word; ">
                            <?php print '<a href="'.$_SERVER["REQUEST_URI"].'/'.$item->reference_id.'-'.$item->alias.'">'.$item->title.'</a>'; ?>
                        </td>
                        <td>
                            <?php
                                //print_r($item->index);
                                $stanza = array();
                                foreach($item->index as $i)
                                {
                                    if($i->field_id == 19) $stanza[] = JText::_( $room_types[$i->value_id],true );//STANZA
                                    if($i->field_id == 17) {
                                        $cat = explode(' ',$i->search_index);
                                        echo $cat[0].' '.JText::_( 'COM_RUSSIANTOUR_STELLE',true );
                                    }//CATEGORIA
                                }
                            ?>
                        </td>
                        <td class="uk-text-left">
                            <?php echo implode(", ",  $stanza); ?>
                        </td>
                        <td>
                            <?php
                            if ($language_id == 5)
                                echo $item->cost_from_rub.' - '.$item->cost_to_rub . ' руб.';
                            else
                                echo round($item->cost_from_rub/$rate).' - '.round($item->cost_to_rub/$rate) . ' €';
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php if ($canCreate) : ?>
                <a href="<?php echo JRoute::_('index.php?option=com_russiantour&task=hotelform.edit&id=0', false, 0); ?>"
                   class="btn btn-success btn-small"><i
                        class="icon-plus"></i>
                    <?php echo JText::_('COM_RUSSIANTOUR_ADD_ITEM'); ?></a>
            <?php endif; ?>

            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
            <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
            <?php echo JHtml::_('form.token'); ?>

        </form>
    </div>
</div>
<?php if($canDelete) : ?>
<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {

		if (!confirm("<?php echo JText::_('COM_RUSSIANTOUR_DELETE_MESSAGE'); ?>")) {
			return false;
		}
	}
</script>
<?php endif; ?>
<style>
    .pagination, .pagination ul {
        text-align: center;
    }
    .pagination ul>li {
        display: inline;
    }
    .pagination ul > li > a, .pagination ul>li>span {
        float: left;
        padding: 4px 12px;
        line-height: 24px;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #eaeaea;
        border-left-width: 0;
    }
</style>
<script>
    var Joomla = {};
    Joomla.submitform = function(){document.adminForm.submit()}
</script>