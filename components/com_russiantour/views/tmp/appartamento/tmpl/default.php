<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');//

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_russiantour') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'hotelform.xml');
$canEdit    = $user->authorise('core.edit', 'com_russiantour') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'hotelform.xml');
$canCheckin = $user->authorise('core.manage', 'com_russiantour');
$canChange  = $user->authorise('core.edit.state', 'com_russiantour');
$canDelete  = $user->authorise('core.delete', 'com_russiantour');

$group_id = $this->params->get('group_id');
$filePath = JPATH_COMPONENT.'/include/appartamento/'.$group_id.'.php';
if (is_file($filePath))
    include ($filePath);

?>
<?php $stanze = $this->getState('filter.stanze',''); ?>
<?php $metro = $this->getState('filter.metro',''); ?>
<div id="flexicontent">
    <div class="uk-grid">
        <form class="uk-form"  style="width: 100%;" action="<?php echo $this->document->base; ?>" method="post" name="adminForm" id="adminForm">
            <div class="uk-width-medium-1-2" style=" display: inline-block; ">
                <?php echo JLayoutHelper::render('default_filter', array('view' => $this), dirname(__FILE__)); ?>
            </div>
            <?php if ($this->two_rows==0) { ?>
            <div  style=" display: inline-block;width: 49%; ">
                <select class="uk-button" name="filter[metro]">
                    <option value="">- Metro -</option>
                    <?php foreach ($this->metro as $v)  { ?>
                        <option value="<?php echo $v->value_id; ?>" <?php if ($v->value_id==$metro) echo 'selected'; ?>>.&nbsp;&nbsp;&nbsp;'-&nbsp;<?php echo $v->search_index; ?></option>
                    <?php } ?>
                </select>
                <select name="filter[stanze]">
                    <option value="">- Stanze -</option>
                    <?php foreach ($this->stanza as $v)  { ?>
                        <option value="<?php echo $v->value_id; ?>" <?php if ($v->value_id==$stanze) echo 'selected'; ?>>.&nbsp;&nbsp;&nbsp;'-&nbsp;<?php echo $v->search_index; ?></option>
                    <?php } ?>
                </select>
                <select name="filter[prezzo]">
                    <option value="">- Prezzo -</option>
                </select>
                <div  class="uk-form-select uk-text-left" data-uk-form-select style="display: inline-block">
                    <input class="uk-button-primary uk-button "  style=" color: #fff; "  value="<?php echo JText::_('COM_RUSSIANTOUR_SEND'); ?>" type="submit">
                </div>
            </div>
            <?php } ?>
            <!--table class="table table-striped" id="hotelList"-->
            <table class="uk-table" style=" width: 97%;" id="flexitable">
                <thead>
                    <tr>
                        <th style="  word-break: break-word; ">
                            <?php echo JText::_( 'COM_RUSSIANTOUR_NOME',true ); ?>
                        </th>
                        <?php if ($this->two_rows==0) { ?>
                        <th>
                            <?php echo JText::_( 'COM_RUSSIANTOUR_STANZE',true ); ?>:
                        </th>
                        <th  style="word-break: break-word; ">
                            <?php echo JText::_( 'COM_RUSSIANTOUR_METRO',true ); ?>
                        </th>
                        <?php } ?>
                        <th>
                            <?php echo JText::_( 'COM_RUSSIANTOUR_PREZZO',true ); ?>
                        </th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <td colspan="<?php echo isset($this->items[0]) ? count(get_object_vars($this->items[0])) : 10; ?>">
                            <?php echo $this->pagination->getListFooter(); ?>
                        </td>
                    </tr>
                </tfoot>
                <tbody>
                    <?php foreach ($this->items as $i => $item) : ?>
                    <tr>
                        <td style="word-break: break-word; "><!-- Nome -->
                            <?php print '<a href="'.$_SERVER["REQUEST_URI"].'/'.$item->reference_id.'-'.$item->alias.'">'.$item->title.'</a>'; ?>
                        </td>
                        <?php if ($this->two_rows==0) { ?>
                        <td><!-- Stanza -->
                            <?php echo $item->stanze; ?>
                        </td>
                        <td><!-- Metro -->
                            <?php echo $item->metro; ?>
                        </td>
                        <?php }// ?>
                        <td><!-- Prezzo -->
                            <?php echo $item->min_price.' - '.$item->max_price.' €'; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

            <?php if ($canCreate) : ?>
                <a href="<?php echo JRoute::_('index.php?option=com_russiantour&task=hotelform.edit&id=0', false, 0); ?>"
                   class="btn btn-success btn-small"><i
                        class="icon-plus"></i>
                    <?php echo JText::_('COM_RUSSIANTOUR_ADD_ITEM'); ?></a>
            <?php endif; ?>

            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
            <input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>"/>
            <input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>"/>
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</div>
<?php if($canDelete) : ?>
<script type="text/javascript">

	jQuery(document).ready(function () {
		jQuery('.delete-button').click(deleteItem);
	});

	function deleteItem() {

		if (!confirm("<?php echo JText::_('COM_RUSSIANTOUR_DELETE_MESSAGE'); ?>")) {
			return false;
		}
	}
</script>
<?php endif; ?>
<style>
    .pagination, .pagination ul {
        text-align: center;
    }
    .pagination ul>li {
        display: inline;
    }
    .pagination ul > li > a, .pagination ul>li>span {
        float: left;
        padding: 4px 12px;
        line-height: 24px;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #eaeaea;
        border-left-width: 0;
    }
</style>
<script>
    var Joomla = {};
    Joomla.submitform = function(){document.adminForm.submit()}
</script>