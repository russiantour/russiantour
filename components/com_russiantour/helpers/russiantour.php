<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('RussiantourHelper', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_russiantour' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'russiantour.php');
//test
/**
 * Class RussiantourFrontendHelper
 *
 * @since  1.6
 */
class RussiantourHelpersRussiantour
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_russiantour/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_russiantour/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'RussiantourModel');
		}

		return $model;
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item)
    {
        $permission = false;
        $user       = JFactory::getUser();

        if ($user->authorise('core.edit', 'com_russiantour'))
        {
            $permission = true;
        }
        else
        {
            if (isset($item->created_by))
            {
                if ($user->authorise('core.edit.own', 'com_russiantour') && $item->created_by == $user->id)
                {
                    $permission = true;
                }
            }
            else
            {
                $permission = true;
            }
        }

        return $permission;
    }
	
	public static function get_language_id()
	{
		/*$app = JFactory::getApplication();

		// Определяем язык из cookies
		$langCode = $app->input->cookie->getString(JApplication::getHash('language'));
		 
		// Если cookies не установлены, используем язык обозревателя
		if (!$langCode)
		{
			$langCode = JLanguageHelper::detectLanguage();
		}*/

		$langCode = JFactory::getLanguage()->getTag();

		$language_id = 0;

        $db    = JFactory::getDbo();
        $sql = "SELECT * FROM `#__languages` where lang_code = '$langCode'";
        $db->setQuery($sql);
        $language = $db->loadObject();
        if ($language)
            $language_id = $language->lang_id;
		
		return $language_id;
	}
}
