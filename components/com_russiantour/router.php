<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JLoader::registerPrefix('Russiantour', JPATH_SITE . '/components/com_russiantour/');

/**
 * Class RussiantourRouter
 *
 * @since  3.3
 */
class RussiantourRouter extends JComponentRouterBase
{
	/**
	 * Build method for URLs
	 * This method is meant to transform the query parameters into a more human
	 * readable form. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  array  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @since   3.3
	 */
	public function build(&$query)
	{if (isset($_GET['kaktus'])) {echo 1;exit;}
		$segments = array();
		$view     = null;

		if (isset($query['task']))
		{
			$taskParts  = explode('.', $query['task']);
			$segments[] = implode('/', $taskParts);
			$view       = $taskParts[0];
			unset($query['task']);
		}

		if (isset($query['view']))
		{
			$segments[] = $query['view'];
			$view = $query['view'];
			
			unset($query['view']);
		}

		if (isset($query['id']))
		{
			if ($view !== null)
			{
				$model      = RussiantourHelpersRussiantour::getModel($view);
				if($model !== null){
					$item       = $model->getData($query['id']);
					$alias      = $model->getAliasFieldNameByView($view);
					$segments[] = (isset($alias)) ? $item->alias : $query['id'];
				}
			}
			else
			{
				$segments[] = $query['id'];
			}

			unset($query['id']);
		}

		return $segments;
	}

	/**
	 * Parse method for URLs
	 * This method is meant to transform the human readable URL back into
	 * query parameters. It is only executed when SEF mode is switched on.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 *
	 * @return  array  The URL attributes to be used by the application.
	 *
	 * @since   3.3
	 */
	public function parse(&$segments)
	{
		$vars = array();
        $segment = explode('-',$segments[count($segments)-1]);
        if (isset($_GET['test2']))
        {
            var_dump($segment);
        }
        //var_dump($segments);
        if (is_numeric($segment[0]))
        {
            $menu	= JFactory::getApplication()->getMenu();
            $item    = $menu->getActive();
            $params	= $menu->getParams($item->id);
            $where = '';

            /*$group_id = $params->get('group_id', false);
            if ($group_id)
                $where = " group_id = ".$group_id." AND ";*/

            $vars['view'] = 'hotel';
            //$vars['reference_id'] = $segment[0];
            $sql = "SELECT * FROM `#__russiantour_` where ".$where." reference_id = ".$segment[0]." AND language_id = ".RussiantourHelpersRussiantour::get_language_id()." LIMIT 1";
            if (isset($_GET['test2']))
            {
                var_dump($sql);
            }
            $db = $db = JFactory::getDbo();
            $db->setQuery($sql);
            $vars['id'] = $db->loadObject()->id;
        }
        else
        {
            // View is always the first element of the array
            $vars['view'] = array_shift($segments);
            $model        = RussiantourHelpersRussiantour::getModel($vars['view']);

            while (!empty($segments))
            {
                $segment = array_pop($segments);

                // If it's the ID, let's put on the request
                if (is_numeric($segment))
                {
                    $vars['id'] = $segment;
                }
                else
                {
                    $id = $model->getItemIdByAlias(str_replace(':', '-', $segment));
                    if (!empty($id))
                    {
                        $vars['id'] = $id;
                    }
                    else
                    {
                        $vars['task'] = $vars['view'] . '.' . $segment;
                    }
                }
            }
        }
        //var_dump($vars);exit;
		return $vars;
	}
}
