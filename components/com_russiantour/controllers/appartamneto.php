<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

/**
 * Hotel controller class.
 *
 * @since  1.6
 */
class RussiantourControllerAppartamento extends JControllerLegacy
{
	
}
