
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>ULTIMA NEWSLETTER DELLA STAGIONE, ULTIME DISPONIBILITA</title>
<style type="text/css">
.ReadMsgBody{width: 100%;}
.ExternalClass{width: 100%;}
div, p, a, li, td { -webkit-text-size-adjust:none; }
a[x-apple-data-detectors]{
	    color: inherit !important;
	    text-decoration: inherit !important;
	    font-size: inherit !important;
	    font-family: inherit !important;
	    font-weight: inherit !important;
	    line-height: inherit !important;
	    }
a:visited{ color:#003399;}
</style>
</head>
<body yahoo="fix" bgcolor="#EEEEEE" style="background-color:#EEEEEE;">
<title></title>
<!-- The title tag shows in email notifications, like Android 4.4. --><!-- Web Font / @font-face : BEGIN --><!-- NOTE: If web fonts are not required,
lines 10 - 27 can be safely removed. --><!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe
fallback font. --><!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]--><!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on
that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ --><!--[if !mso]><!--><!-- insert web font reference, eg: <link
href='https://fonts.googleapis.com/css?family=Roboto:400,700' rel='stylesheet' type='text/css'> --><!--<![endif]--><!-- Web Font / @font-face : END
--><!-- CSS Reset --><style type="text/css">/* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        }
        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }
        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }
        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }
        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes
for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }
        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }
        /* What it does: A work-around for iOS meddling in triggered links. */
        *[x-apple-data-detectors] {
            color: inherit !important;
            text-decoration: none !important;
        }
        /* What it does: A work-around for Gmail meddling in triggered links. */
        .x-gmail-data-detectors,
        .x-gmail-data-detectors *,
        .aBn {
            border-bottom: 0 !important;
            cursor: default !important;
        }
        /* What it does: Prevents Gmail from displaying an download button on large, non-linked images. */
        .a6S {
            display: none !important;
            opacity: 0.01 !important;
        }
        /* If the above doesn't work, add a .g-img class to any image in question. */
        img.g-img + div {
            display:none !important;
        }
        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }
        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */
        /* Thanks to Eric Lepetit @ericlepetitsf) for help troubleshooting */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) { /* iPhone 6 and 6+ */
            .email-container {
                min-width: 375px !important;
            }
        }
</style>
<!-- What it does: Makes background images in 72ppi Outlook render at correct size. --><!--[if gte mso 9]>
    
    <![endif]--><!-- Progressive Enhancements --><style type="text/css">/* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }
</style>
<center style="width: 100%; background: #eeeeee; text-align: left;">
<!-- Visually Hidden Preheader Text : BEGIN --><!-- Visually Hidden Preheader Text : END --><!--
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 600px.
            2. MSO tags for Desktop Windows Outlook enforce a 600px width.
        -->
<div class="email-container" style="max-width: 600px; margin: auto;">
<!--[if mso]>
            <table role="presentation" aria-hidden="true" cellspacing="0" cellpadding="0" border="0" width="600" align="center">
            <tr>
            <td>
            <![endif]--><!-- Email Header : BEGIN -->
<div style="display:none;font-size:1px;color:#222222;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:
all;">Transiberiana, Crociera sul Volga, Tour Mosca San Pietroburgo</div>

<table aria-hidden="true" role="presentation" style="max-width: 600px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center"><tr 
style="line-height: 0px;"  >
<td style="padding: 20px 0; text-align: center"   bgcolor="#ffffff"><a href="https://www.russiantour.com?acm=786_61" style="color:#003399;"><img 
alt="Logo Russia Tour " src="http://www.russiantour.com/media/com_acymailing/upload/rti289_1_.png"
style="border:0px;text-decoration:none;height:165px;width:289px;" height="165" width="289" /></a></td>
	</tr></table>
<!-- Email Header : END --><!-- Email Body : BEGIN -->
<table aria-hidden="true" role="presentation" style="max-width: 600px;" width="100%"
cellspacing="0" cellpadding="0" border="0" align="center">
<!-- Hero Image, Flush : BEGIN -->
<tr  style="line-height: 0px;"  >
<td  style="line-height: 0px;"   bgcolor="#ffffff"><a href="https://www.russiantour.com/ita/mosca-sanpietroburgo?acm=786_61&idU=1"
style="color:#003399;"><img width="600" height="200" src="http://www.russiantour.com/media/com_acymailing/upload/asdas.jpg" alt="alt_text" 
style="border:0px; text-decoration:none;" /></a></td>
	</tr>
<!-- Hero Image, Flush : END --><!-- 1 Column Text + Button : BEGIN --><tr>
<td bgcolor="#ffffff">
	    <table aria-hidden="true" role="presentation" width="100%" cellspacing="0" cellpadding="0" border="0"><tr>
<td  style="padding: 40px; font-family: sans-serif; font-size: 15px; line-height: 20px; color: #555555;"  >
<h2 style="font-size:16px;font-weight:bold;text-align:center;"><a href="http://www.russiantour.com/ita/mosca-sanpietroburgo?acm=786_61"
style="color:#003399;">TOUR MOSCA SAN PIETROBURGO</a></h2> 

aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa

</td>
		     </table>
</td>
	</tr>
<!-- Two Even Columns : END --><!-- Clear Spacer : BEGIN --><tr   >
<td style="font-size: 0; line-height: 0;"   height="40"></td>
	</tr>
<!-- Clear Spacer : END --><!-- 1 Column Text : BEGIN --><!-- 1 Column Text : END -->

</table> 
 
 
<!-- Email Body : END --><!-- Email Footer : BEGIN --><table aria-hidden="true" role="presentation" style="max-width: 600px;" width="100%"
cellspacing="0" cellpadding="0" border="0" align="center">
<!-- Hero Image, Flush : BEGIN -->
<!-- Hero Image, Flush : END --><!-- 1 Column Text + Button : BEGIN --><tr>
<td style="text-align: center;">
<a href="https://www.russiantour.com/ita/mosca-sanpietroburgo?acm=786_61" style="color:#003399;">Tour Mosca San Pietroburgo</a>, <a
href="https://www.russiantour.com/ita/anellodoro?acm=786_61" style="color:#003399;">Tour Anello d'oro,</a><a
href="https://www.russiantour.com/ita/crocieravolga?acm=786_61" style="color:#003399;">Crociera sul Volga</a>, <a
href="https://www.russiantour.com/ita/transiberiana?acm=786_61" style="color:#003399;">Transiberiana</a>,&nbsp;<a
href="https://www.russiantour.com/ita/treno-retro?acm=786_61" style="color:#003399;">Treno Retro</a>, <a
href="https://www.russiantour.com/ita/marnero?acm=786_61" style="color:#003399;">Tour Crimea e Mar Nero,</a><a
href="https://www.russiantour.com/ita/kizhi-valaam-solovki?acm=786_61" style="color:#003399;">Tour Carelia</a>, <a
href="https://www.russiantour.com/ita/baltici?acm=786_61" style="color:#003399;">Tour Paesi Baltici</a>, <a
href="https://www.russiantour.com/ita/altritour?acm=786_61" style="color:#003399;">Altri tour in Russia</a><br /><a
href="https://www.russiantour.com/ita/visto-turistico-russia?acm=786_61" style="color:#003399;">Visto Turistico Russia</a>, <a
href="https://www.russiantour.com/ita/visto-business-russia?acm=786_61" style="color:#003399;">Visto Business Russia</a>, <a
href="https://www.russiantour.com/ita/visto-altri-paesi?acm=786_61" style="color:#003399;">Visto altri paesi</a>, <a
href="https://www.russiantour.com/ita/visto-italia?acm=786_61" style="color:#003399;">Visto Schengen</a><br /><a
href="https://www.russiantour.com/ita/soggiorno/hotel-mosca?acm=786_61" style="color:#003399;">Hotel Mosca</a>,<a
href="https://www.russiantour.com/ita/soggiorno/hotel-sanpietroburgo?acm=786_61" style="color:#003399;"> Hotel San Pietroburgo</a>, <a
href="https://www.russiantour.com/ita/soggiorno/appartamento-mosca?acm=786_61" style="color:#003399;">Appartamenti Mosca</a>, <a
href="https://www.russiantour.com/ita/soggiorno/appartamento-sanpietroburgo?acm=786_61" style="color:#003399;">Appartamenti San Pietroburgo</a>.<br
/><br /><a href="https://www.russiantour.com/index.php?option=com_acymailing&ctrl=url&subid=786&urlid=40&mailid=61" style="color:#003399;"><img 
alt="Facebook" src="http://www.russiantour.com/media/com_acymailing/upload/soc/face.png"
style="border:0px;text-decoration:none;height:25px;width:25px;" height="25" width="25" /></a>&nbsp;<a
href="https://www.russiantour.com/index.php?option=com_acymailing&ctrl=url&subid=786&urlid=41&mailid=61" style="color:#003399;"><img  alt="Google
Plus" src="http://www.russiantour.com/media/com_acymailing/upload/soc/g.png" style="border:0px;text-decoration:none;height:25px;width:25px;"
height="25" width="25" /></a>&nbsp;<a href="https://www.russiantour.com/index.php?option=com_acymailing&ctrl=url&subid=786&urlid=42&mailid=61"
style="color:#003399;"><img  alt="Instagram" src="http://www.russiantour.com/media/com_acymailing/upload/soc/in.png"
style="border:0px;text-decoration:none;height:25px;width:25px;" height="25" width="25" /></a>&nbsp;<a
href="https://www.russiantour.com/index.php?option=com_acymailing&ctrl=url&subid=786&urlid=43&mailid=61" style="color:#003399;"><img  alt="Vkontakte "
src="http://www.russiantour.com/media/com_acymailing/upload/soc/vk.png" style="border:0px;text-decoration:none;height:25px;width:25px;" height="25"
width="25" /></a>&nbsp;<a href="https://www.russiantour.com/index.php?option=com_acymailing&ctrl=url&subid=786&urlid=44&mailid=61"
style="color:#003399;"><img  alt="You Tube" src="http://www.russiantour.com/media/com_acymailing/upload/soc/video_youtube_icon_1_.png"
style="border:0px;text-decoration:none;height:28px;width:28px;" height="28" width="28" /></a>
</td>
	</tr>
<!-- Two Even Columns : END --><!-- Clear Spacer : BEGIN --><tr>
<td style="font-size: 0; line-height: 0;" height="40"></td>
	</tr>
<!-- Clear Spacer : END --><!-- 1 Column Text : BEGIN --><!-- 1 Column Text : END -->

</table>
<table aria-hidden="true" role="presentation" style="max-width: 680px;" width="100%" cellspacing="0" cellpadding="0" border="0" align="center"><tr   >
<td class="x-gmail-data-detectors" style="padding: 40px 10px;width: 100%;font-size: 12px; font-family: sans-serif; line-height:18px; text-align:
center; color: #888888;" >
<webversion style="color:#cccccc; text-decoration:underline; font-weight: bold;"><a href="https://www.russiantour.com/ita/?acm=786_61"
style="color:#003399;">View as a Web Page</a></webversion><br /><br />
	    Russian Tour International Ltd. info@russiantour.com<br />
	    Finlyandskiy Pr. 4a, off.717, 194044 San Pietroburgo<br />
	    Tel/fax +78126470690 - Numero Verde: 800404000<br />
	    Roma +390690289660 - Palermo +390917481654<br />
	    Milano +390245074837 - Genova +390108935089<br /> 
</td>
	</tr></table>
<!-- Email Footer : END --><!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
</div>
</center>
 
</body>
</html>