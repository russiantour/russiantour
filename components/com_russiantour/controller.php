<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

/**
 * Class RussiantourController
 *
 * @since  1.6
 */
class RussiantourController extends JControllerLegacy
{
	/**
	 * Method to display a view.
	 *
	 * @param   boolean $cachable  If true, the view output will be cached
	 * @param   mixed   $urlparams An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController   This object to support chaining.
	 *
	 * @since    1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{
        $app  = JFactory::getApplication();
        $view = $app->input->getCmd('view', 'hotes');
		$app->input->set('view', $view);

		parent::display($cachable, $urlparams);

		return $this;
	}
	
	public function update_cid()
	{
		$this->l('hello world');
		$limit = 0;
		if(isset($_GET['l'])) $limit = $_GET['l'];
		//$sql = 'SELECT a.id, a.reference_id, c.catid, a.catid as old_catid FROM `wza4w_russiantour_` as a LEFT JOIN wza4w_content as c ON a.reference_id = c.id LIMIT '.$limit.',1';
		$sql = 'SELECT a.id, a.reference_id, c.sid, c.field_id, c.search_index, c.extraid, c.value_id FROM `wza4w_russiantour_` as a LEFT JOIN wza4w_flexicontent_advsearch_index as c ON a.reference_id = c.item_id LIMIT '.$limit.',1';
		$this->l($sql);
		$db = JFactory::getDBO();
		$db->setQuery($sql);
		$row = $db->loadObject();
		$this->l($row);
		
		if(0){
			$update_sql = "UPDATE `wza4w_russiantour_` SET `catid` = '$row->catid' WHERE `wza4w_russiantour_`.`id` = $row->id";
			$db->setQuery($update_sql);
			if($db->execute())
			{
				echo '<meta http-equiv="refresh" content="1; url=http://test2.viaggio-russia.com/ita/?option=com_russiantour&task=update_cid&l='.($limit+1).'">';
			}else{
				echo 'не удалось обновить';
			}
			$this->l($update_sql);
			echo $db->stderr();
			
		}else{
			echo 'кончились';
		}
		die();
	}
	
	
	public function copy2()
	{
		echo '<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">';
		$offset = 0;
		if(isset($_GET['o'])) $offset = $_GET['o'];
		$sql = "SELECT reference_id FROM `wza4w_russiantour_` LIMIT $offset, 1";
		$db = JFactory::getDBO();
		$db->setQuery($sql);
		$reference_id = $db->loadResult();
		if($reference_id){
			echo '<meta http-equiv="refresh" content="1; url=http://test2.viaggio-russia.com/ita/?option=com_russiantour&task=copy2&o='.($offset+1).'">';
		}else{
			$this->l('Строка для определения не найдена!'); echo '<style>body{background:red;}</style></body>
</html>';  die();
		}
		$sql = "SELECT field_id, item_id, valueorder, value
FROM (
    SELECT field_id, item_id, valueorder, value
    FROM wza4w_flexicontent_fields_item_relations
    ORDER BY valueorder desc) 
as t WHERE item_id = $reference_id 
GROUP BY field_id, item_id
ORDER BY NULL";
		echo '</head>
 <body> ';
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$set = "";//`field_16` = '1111', `field_22` = '222'
		foreach($rows as $row)
		{
			$set .= ", `field_$row->field_id` = '$row->value'";
		}
		$sql = "UPDATE `wza4w_russiantour_` SET `ordering` = '0' $set WHERE `reference_id` = $reference_id";
		$this->l($sql);
		
		if($rows && $set)
		{
			echo '<a href="http://test2.viaggio-russia.com/ita/?option=com_russiantour&task=copy2&o='.($offset+1).'">click click click click click click click click click click click </a>';
		//}
			//$this->l($sql_to_insert); die();
			$db->setQuery($sql);
			$db->execute();
			echo $db->stderr();
		
			echo '<style>body{background:green;}</style>';
		}
		echo '</body></html>';
		die();
	}
	
	public function copy()
	{
		echo '<!DOCTYPE html>
<html>
 <head>
  <meta charset="utf-8">';
		
		$language_id = 4;//1 english, 3 italiano, 4 espanol
		if(isset($_GET['l'])) $language_id = $_GET['l'];
		$inserts = array(
			/*0=> array(),
			1=> array(),
			2=> array(),
			3=> array(),
			4=> array()*/
		);
		
		$offset = 0;
		$reference_id = 0;
		//
		if(isset($_GET['o'])) $offset = $_GET['o'];
		
		$sql1 = "SELECT f.reference_id FROM `wza4w_content` as c LEFT JOIN `wza4w_falang_content` as f ON c.id = f.reference_id WHERE c.catid IN( 55, 12, 228, 223, 1) AND f.reference_table = 'content' AND f.language_id='$language_id' AND  c.state > 0 GROUP BY c.id LIMIT $offset, 1";
		
		//echo $sql1; die();
		
		$db = JFactory::getDBO();
		$db->setQuery($sql1);
		$reference_id = $db->loadResult();
		//print_r($reference_id);
		//die();
		if($reference_id){
			echo '<meta http-equiv="refresh" content="1; url=http://test4.russiantour.com/ita/?option=com_russiantour&task=copy&o='.($offset+1).'">';
		}else{
			$this->l('Строка для определения не найдена!'); echo '<style>body{background:red;}</style></body>
</html>';  die();
		}
		$db->setQuery("SELECT * FROM `wza4w_russiantour_` where reference_id = $reference_id AND language_id = $language_id");
		if( $db->loadObject() ){
			$this->l('Строка существует!'); echo '<style>body{background:gray;}</style></body>
</html>';  die();
		}
		echo '</head>
 <body> ';
		
		//$this->l($row);
		$sql2 = "SELECT c.*, f.* FROM `wza4w_content` as c LEFT JOIN `wza4w_falang_content` as f ON c.id = f.reference_id WHERE c.catid IN( 55, 12, 228, 223, 1) AND f.language_id='$language_id' AND f.reference_table = 'content' AND c.state > 0 AND c.id = $reference_id  ORDER BY `f`.`modified` ASC";
		
		//echo $sql2; die();
		
		$db->setQuery($sql2);
		$rows = $db->loadObjectList();
		
		//$this->l($rows);
		
		if($rows)
		{
			$inserts['alias'] = $rows[0]->alias;
			$inserts['title'] = $rows[0]->title;
			$inserts['catid'] = $rows[0]->catid;
			$inserts['created_by'] = $rows[0]->created_by;
			$inserts['modified_by'] = $rows[0]->modified_by;
			foreach($rows as $k=>$v)
			{
				//$inserts[$v->language_id][$v->reference_field] = $v->value;
				$inserts[$v->reference_field] = $v->value;
				/*
	[id] => 21185
    [language_id] => 1
    [reference_id] => 5587
    [reference_table] => content
    [reference_field] => title
    [value] => Yakutia: in the heart of Siberia
    [original_value] => 1f1d14fc8fecc52de38e8ddf62646d20
    [original_text] => Tour Yakutia
    [modified] => 2016-11-10 16:03:03
    [modified_by] => 419
    [published] => 1
	*/
			}
		}
		
		//$this->l($inserts);
		$sql_to_insert = '';
		//foreach($inserts as $language_id=>$array)
		//{
			$sql_to_insert .= $this->create_insert_sql($inserts, $reference_id, $language_id);
			
			//$this->l($offset); 
			echo '<a href="http://test4.russiantour/ita/?option=com_russiantour&task=copy&o='.($offset+1).'">click click click click click click click click click click click </a>';
		//}
			//$this->l($sql_to_insert); die();
			$db->setQuery($sql_to_insert);
			$db->execute();
			echo $db->stderr();
		
		echo '<style>body{background:green;}</style></body>
</html>'; die();
	}
	
	function create_insert_sql($array, $reference_id, $language_id)
	{
		$sql = '';
		
		if(count($array))
		{
			$modified_by = ''; if(isset($array['modified_by'])) $modified_by = addslashes( $array['modified_by'] );
			$created_by = ''; if(isset($array['created_by'])) $created_by = addslashes( $array['created_by'] );
			$catid = ''; if(isset($array['catid'])) $catid = addslashes( $array['catid'] );
			
			$title = ''; if(isset($array['title'])) $title = addslashes( $array['title'] );
			$alias = ''; if(isset($array['alias'])) $alias = addslashes( $array['alias'] );
			$introtext = ''; if(isset($array['introtext'])) $introtext = addslashes( $array['introtext'] );
			$fulltext = ''; if(isset($array['fulltext'])) $fulltext = addslashes( $array['fulltext'] );
			
			$metakey = ''; if(isset($array['metakey'])) $metakey = addslashes( $array['metakey'] );
			$metadesc = ''; if(isset($array['metadesc'])) $metadesc = addslashes( $array['metadesc'] );
			
			$sql = "INSERT INTO `wza4w_russiantour_` (`id`, `reference_id`, `catid`, `language_id`, `ordering`, `state`, `checked_out`, `checked_out_time`, `created_by`, `modified_by`, `title`, `alias`, `introtext`, `fulltext`, `metakey`, `metadesc`, `field_13`, `field_17`, `field_19`, `field_27`, `field_28`, `field_31`, `field_35`, `field_13_search_index`, `field_17_search_index`, `field_19_search_index`, `field_27_search_index`, `field_28_search_index`, `field_31_search_index`, `field_35_search_index`) VALUES (NULL,
			'$reference_id',
			'$catid',
			'$language_id', 
			'0',
			'0',
			'0',
			'0000-00-00 00:00:00',
			'$created_by',
			'$modified_by',
			'$title',
			'$alias',
			'$introtext',
			'$fulltext',
			'$metakey',
			'$metadesc', '', '', '', '', '', '', '', '', '', '', '', '', '', ''); ";
		}
		
		return $sql;
	}
	
	function l($l)
	{
		echo '<pre>';
		print_r($l);
		echo '</pre>';
	}
	
	function sendForm()
    {
        $lang = JFactory::getLanguage();
//var_dump($lang->getTag());//it-IT,es-ES,en-GB

        //$mail = 'isvetu@gmail.com';
        //$mail = 'info@russiantour.com';
        //$mail = 'kaktus.mov@gmail.com';
        $mail = 'info@russiantour.com';
        //$result = JFactory::getMailer()->sendMail('admin@host.russiantour.com', 'Zayvka RTI', $mail, 'Zayvka RTI', var_export($_POST,1));
        include 'mailTemplates/'.$lang->getTag().'_invia_richiesta.php';
        $result = JFactory::getMailer()->sendMail('noreply@mail.russiantour.com', $fromName, $mail, $subject, $body, true);

        $user_ip = $_SERVER['HTTP_X_REAL_IP'] ?? ($_SERVER['REMOTE_ADDR'] ?? '');

        $sql = "INSERT INTO #__requests (`form-data-to-send`,`form-mail`,`form-phone`,`form-cognome`,`form-personas`,`form-comments`,`generated-id`,`user_ip`) VALUES ('".addslashes($_POST['form-data-to-send'])."','".$_POST['form-mail']."','".$_POST['form-phone']."','".$_POST['form-cognome']."','".$_POST['form-personas']."','".$_POST['form-comments']."','".$_POST['generated-id']."','".$user_ip."')";
        $db = JFactory::getDBO();
        $db->setQuery($sql);
        $db->execute();

        $trip_id = 0;
        if (isset($_POST['tour_id']))
            $trip_id = intval($_POST['tour_id']);

        $trip_data = ",null,null";
        if ($trip_id > 0)
        {
            $sql = "SELECT * FROM #__viaggio_trips WHERE id = ".$trip_id." LIMIT 1";
            $db->setQuery($sql);
            $trip = $db->loadObject();
            $trip_data = ",'".$trip->date_from."','".$trip->date_to."'";
        }

        $sql = "INSERT INTO #__viaggio_orders (`clientcount`,`nomecgome`,`telephone`,`email`,`description`,`status`,`status_from`,`trip_id`, `date_from`, `date_to`) VALUES ('".$_POST['form-personas']."','".$_POST['form-cognome']."','".$_POST['form-phone']."','".$_POST['form-mail']."','".$_POST['form-comments']."',0,3,".$trip_id.$trip_data.")";
        $db->setQuery($sql);
        $db->execute();

        if ($trip_id > 0)
        {
            $order_id = $db->insertid();

            $sql = "INSERT INTO #__viaggio_orders_hotels (order_id,hotel_id) select ".$order_id.", hotel_id from #__viaggio_trips_hotels where trip_id = ".$trip_id;
            $db->setQuery($sql);
            $db->execute();
        }

        echo JText::_( 'COM_RUSSIANTOUR_GRAZIE',true );
        echo "<script> fbq('track', 'Purchase');</script> 

";


        exit;
    }
}
