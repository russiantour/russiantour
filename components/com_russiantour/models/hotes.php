<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Russiantour records.
 *
 * @since  1.6
 */
class RussiantourModelHotes extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'ordering', 'a.ordering',
				'state', 'a.state',
				'created_by', 'a.created_by',
				'modified_by', 'a.modified_by',
				'title', 'a.title',
				'alias', 'a.alias',
				'introtext', 'a.introtext',
				'fulltext', 'a.fulltext',
				'metakey', 'a.metakey',
				'metadesc', 'a.metadesc',
				'field', 'a.field',
				'field_2', 'a.field_2',
				'field_3', 'a.field_3',
				'field_4', 'a.field_4',
				'field_5', 'a.field_5',
				'field_6', 'a.field_6',
				'field_7', 'a.field_7',
				'field_8', 'a.field_8',
				'field_9', 'a.field_9',
				'field_10', 'a.field_10',
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app  = JFactory::getApplication();
		$list = $app->getUserState($this->context . '.list');

		$ordering  = isset($list['filter_order'])     ? $list['filter_order']     : null;
		$direction = isset($list['filter_order_Dir']) ? $list['filter_order_Dir'] : null;

		$list['limit']     = (int) JFactory::getConfig()->get('list_limit', 20);
		$list['start']     = $app->input->getInt('start', 0);
		$list['ordering']  = $ordering;
		$list['direction'] = $direction;

		$app->setUserState($this->context . '.list', $list);
		$app->input->set('list', null);

		// List state information.
		parent::populateState($ordering, $direction);

        $app = JFactory::getApplication();

        $ordering  = $app->getUserStateFromRequest($this->context . '.ordercol', 'filter_order', $ordering);
        $direction = $app->getUserStateFromRequest($this->context . '.orderdirn', 'filter_order_Dir', $ordering);

        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', $direction);

        $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
        //$start = $app->getUserStateFromRequest($this->context . '.start', 'start', 0, 'int');
        $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 0, 'int');

        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		//получаем ID языка
		$language_id = (RussiantourHelpersRussiantour::get_language_id());
		$where = "r.language_id = '$language_id ' AND ";
        // Filter by search in title

        $search = $this->getState('filter.search');
        if (!empty($search))
        {
            $where .= " r.title LIKE '%".$search."%' AND ";
        }

        $join = '';

        $cats = $this->getState('filter.cats');
        if (!empty($cats))
        {
            $where .= ' f.value_id = '.$cats." AND f.field_id = 17 AND ";
            $join = " JOIN `wza4w_flexicontent_advsearch_index` as f ON f.item_id = r.reference_id ";
        }
		//получаем ID категории
		$menu	= JFactory::getApplication()->getMenu();
		$item    = $menu->getActive();
		$params	=  $menu->getParams($item->id);
		$reference_id = $params->get('reference_id', 0);
        $catFilter = $this->getState('filter.cat',0);
        if ($catFilter > 0 && $catFilter != $reference_id){
            $where .= " (r.catid = ".$catFilter.") AND ";
        }

        $inner_where = '';
		if($reference_id) $inner_where .= " AND t.category_id = $reference_id ";

        //field_22 - best, field_17 - stars
		$query = "SELECT (SELECT search_index FROM #__flexicontent_advsearch_index WHERE item_id = r.reference_id AND field_id = 28 AND extraid = 0) max_price, 
(SELECT search_index FROM #__flexicontent_advsearch_index WHERE item_id = r.reference_id AND field_id = 28 AND extraid = 1) min_price, 
hs.cost_from_rub,
hs.cost_to_rub,
r.*
FROM `#__russiantour_` as r 
".$join."
JOIN #__russiantour_to_category t on t.reference_id = r.reference_id
JOIN #__russiantour_hotel_settings hs on hs.reference_id = r.reference_id
where $where r.state >= 0 ".$inner_where."
ORDER BY r.field_22 DESC, r.field_17 DESC, r.title ASC";
        if (isset($_GET['test'])) echo $query;
		return $query;
	}

	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		$db    = $this->getDbo();

		if($items){
			for($i = 0; $i<count($items); $i++)
			{
				$sql = "SELECT * FROM `#__flexicontent_advsearch_index` where field_id IN (17,19) AND item_id = ".$items[$i]->reference_id." ORDER BY `extraid` ASC"; 
				$db->setQuery($sql);
				$items[$i]->index = $db->loadObjectList();
			}
		}
		return $items;
	}

    public function getCats()
    {
        $db    = $this->getDbo();
        $menu	=  JFactory::getApplication()->getMenu();
        $item    = $menu->getActive();
        $params	=  $menu->getParams($item->id);
        $reference_id = $params->get('reference_id', 0);

        //var_dump($reference_id);exit;
        if ($reference_id)
        {
            $language_id = (RussiantourHelpersRussiantour::get_language_id());
            //field_22 - best
            $sql = "SELECT f.search_index, f.value_id
FROM `#__russiantour_` as r 
JOIN `#__flexicontent_advsearch_index` as f ON f.item_id = r.reference_id
JOIN #__russiantour_to_category t on t.reference_id = r.reference_id
WHERE r.state >= 0 AND f.field_id = 17 AND t.category_id = ".$reference_id." AND r.language_id = ".$language_id."
GROUP BY f.search_index ORDER BY f.search_index ASC";

            $db->setQuery($sql);
            return $db->loadObjectList();
        }
        return false;
    }

    public function getMetro()
    {
        $db    = $this->getDbo();
        $menu	=  JFactory::getApplication()->getMenu();
        $item    = $menu->getActive();
        $params	=  $menu->getParams($item->id);
        $reference_id = $params->get('reference_id', 0);
        //var_dump($reference_id);exit;
        if ($reference_id)
        {
            $sql = "SELECT * FROM `#__metro` where city_id = ".$reference_id." order by title asc";
            $db->setQuery($sql);
            return $db->loadObjectList();
        }
        return false;
    }

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 *
	 * @return void
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;

		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null)
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}

		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_RUSSIANTOUR_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
	 *
	 * @param   string  $date  Date to be checked
	 *
	 * @return bool
	 */
	private function isValidDate($date)
    {
        $date = str_replace('/', '-', $date);
        return (date_create($date)) ? JFactory::getDate($date)->format("Y-m-d") : null;
    }
}
