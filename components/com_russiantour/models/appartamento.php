<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Russiantour records.
 *
 * @since  1.6
 */
class RussiantourModelAppartamento extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   Elements order
	 * @param   string  $direction  Order direction
	 *
	 * @return void
	 *
	 * @throws Exception
	 *
	 * @since    1.6
	 */
	protected function populateState($ordering = null, $direction = null)
	{
		$app  = JFactory::getApplication();
		$list = $app->getUserState($this->context . '.list');

		$ordering  = isset($list['filter_order'])     ? $list['filter_order']     : null;
		$direction = isset($list['filter_order_Dir']) ? $list['filter_order_Dir'] : null;

		$list['limit']     = (int) JFactory::getConfig()->get('list_limit', 20);
		$list['start']     = $app->input->getInt('start', 0);
		$list['ordering']  = $ordering;
		$list['direction'] = $direction;

		$app->setUserState($this->context . '.list', $list);
		$app->input->set('list', null);

		// List state information.
		parent::populateState($ordering, $direction);

        $app = JFactory::getApplication();

        $ordering  = $app->getUserStateFromRequest($this->context . '.ordercol', 'filter_order', $ordering);
        $direction = $app->getUserStateFromRequest($this->context . '.orderdirn', 'filter_order_Dir', $ordering);

        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', $direction);

        $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
        $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 0, 'int');

        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return   JDatabaseQuery
	 *
	 * @since    1.6
	 */
	protected function getListQuery()
	{
		//получаем ID языка
		$language_id = (RussiantourHelpersRussiantour::get_language_id());
        $where = "r.language_id = '$language_id ' AND ";
        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search))
        {
            $where .= " r.title LIKE '%".$search."%' AND ";
        }
		//получаем ID категории
		$menu	= JFactory::getApplication()->getMenu();
		$item    = $menu->getActive();
		$params	=  $menu->getParams($item->id);
		$group_id = $params->get('group_id', 0);

        $field_27 = $this->getState('filter.stanze');
        $field_13 = $this->getState('filter.metro');
        $price_from = $this->getState('filter.prezzo');

        if ($field_13)
            $where .= " r.field_13 = ".$field_13." AND ";

        if ($field_27)
            $where .= " r.field_27 = ".$field_27." AND ";

        if ($price_from)
        {
            $price_to = $price_from+50;
            $where .= " r.price_from >= ".$price_from." AND r.price_to < ".$price_to." AND " ;
        }

		$query = "SELECT r.*, t.*, hs.cost_from_rub, hs.cost_to_rub FROM `#__russiantour_` as r 
JOIN #__russiantour_to_category t on t.reference_id = r.reference_id
JOIN #__russiantour_hotel_settings hs on hs.reference_id = r.reference_id
where ".$where." r.state >= 0 AND t.category_id = ".$group_id." ORDER BY r.title ASC";
        if (isset($_GET['kaktus']))////
        {
            echo '-1-'.$query;
        }
		return $query;
	}

	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
		$items = parent::getItems();
		$db    = $this->getDbo();

		if($items){//
			for($i = 0; $i<count($items); $i++)
			{
				$sql = "SELECT * FROM `#__flexicontent_advsearch_index` where field_id IN (13,27,28) AND item_id = ".$items[$i]->reference_id." ORDER BY `extraid` ASC";
                if (isset($_GET['kaktus']))
                {
                    echo '-2-'.$sql;
                }
				$db->setQuery($sql);
				//$items[$i]->index = $db->loadObjectList();
                foreach ($db->loadObjectList() as $row){
                    if ($row->field_id == 13 && $row->value_id != $items[$i]->id)
                        $items[$i]->metro = $row->search_index;
                    elseif ($row->field_id == 27)
                        $items[$i]->stanze = $row->search_index;
                    elseif ($row->field_id == 28 && $row->extraid == 1)
                        $items[$i]->min_price = $row->search_index;
                    elseif ($row->field_id == 28 && $row->extraid == 0)
                        $items[$i]->max_price = $row->search_index;
                }
                if (!isset($items[$i]->min_price))
                    $items[$i]->min_price = $items[$i]->max_price;
			}
		}
		return $items;
	}

	/**
	 * Overrides the default function to check Date fields format, identified by
	 * "_dateformat" suffix, and erases the field if it's not correct.
	 *
	 * @return void
	 */
	protected function loadFormData()
	{
		$app              = JFactory::getApplication();
		$filters          = $app->getUserState($this->context . '.filter', array());
		$error_dateformat = false;

		foreach ($filters as $key => $value)
		{
			if (strpos($key, '_dateformat') && !empty($value) && $this->isValidDate($value) == null)
			{
				$filters[$key]    = '';
				$error_dateformat = true;
			}
		}

		if ($error_dateformat)
		{
			$app->enqueueMessage(JText::_("COM_RUSSIANTOUR_SEARCH_FILTER_DATE_FORMAT"), "warning");
			$app->setUserState($this->context . '.filter', $filters);
		}

		return parent::loadFormData();
	}

	/**
	 * Checks if a given date is valid and in a specified format (YYYY-MM-DD)
	 *
	 * @param   string  $date  Date to be checked
	 *
	 * @return bool
	 */
	private function isValidDate($date)
	{
		$date = str_replace('/', '-', $date);
		return (date_create($date)) ? JFactory::getDate($date)->format("Y-m-d") : null;
	}

    public function getStanza()
    {
        $db    = $this->getDbo();
        $menu	= JFactory::getApplication()->getMenu();
        $item    = $menu->getActive();
        $params	=  $menu->getParams($item->id);
        $group_id = $params->get('group_id', 0);

        if ($group_id)
        {
            $language_id = (RussiantourHelpersRussiantour::get_language_id());
            $sql = "SELECT f.search_index, f.value_id
FROM `wza4w_russiantour_` as r 
JOIN `wza4w_flexicontent_advsearch_index` as f ON f.item_id = r.reference_id
JOIN #__russiantour_to_category t on t.reference_id = r.reference_id
WHERE r.state >= 0 AND f.field_id = 27 AND t.category_id = ".$group_id." AND r.language_id = ".$language_id."
GROUP BY f.search_index ORDER BY f.search_index ASC";
            if (isset($_GET['kaktus']))
            {
                echo '-3-'.$sql;
            }
            $db->setQuery($sql);
            return $db->loadObjectList();
        }
        return false;
    }

    public function getMetro()
    {
        $db    = $this->getDbo();
        $menu	= JFactory::getApplication()->getMenu();
        $item    = $menu->getActive();
        $params	= $menu->getParams($item->id);
        $group_id = $params->get('group_id', 0);

        if ($group_id)
        {
            $language_id = (RussiantourHelpersRussiantour::get_language_id());
            $sql = "SELECT f.search_index, f.value_id
FROM `wza4w_russiantour_` as r 
JOIN `wza4w_flexicontent_advsearch_index` as f ON f.item_id = r.reference_id
JOIN #__russiantour_to_category t on t.reference_id = r.reference_id
WHERE r.state >= 0 AND f.field_id = 13 AND t.category_id = ".$group_id." AND r.language_id = ".$language_id."
GROUP BY f.search_index ORDER BY f.search_index ASC";
            if (isset($_GET['kaktus']))
                echo '-4-'.$sql;
            $db->setQuery($sql);
            return $db->loadObjectList();
        }
        return false;
    }
}
