<?php
$fromName = 'Отправить запрос';

$subject = "Отправить запрос";

$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8"> <!-- utf-8 works for most cases -->
    <meta name="viewport" content="width=device-width"> <!-- Forcing initial-scale shouldn/t be necessary -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- Use the latest (edge) version of IE rendering engine -->
    <meta name="x-apple-disable-message-reformatting">  <!-- Disable auto-scale in iOS 10 Mail entirely -->
    <title>Russian Tour </title> <!-- The title tag shows in email notifications, like Android 4.4. -->

    <!-- Web Font / @font-face : BEGIN -->
    <!-- NOTE: If web fonts are not required, lines 9 - 26 can be safely removed. -->

    <!-- Desktop Outlook chokes on web font references and defaults to Times New Roman, so we force a safe fallback font. -->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->

    <!-- All other clients get the webfont reference; some will render the font and others will silently fail to the fallbacks. More on that here: http://stylecampaign.com/blog/2015/02/webfont-support-in-email/ -->
    <!--[if !mso]><!-->

    <!--<![endif]-->

    <!-- Web Font / @font-face : END -->

    <!-- CSS Reset -->
    <style>

        /* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
        html,
        body {
            margin: 0 auto !important;
            padding: 0 !important;
            height: 100% !important;
            width: 100% !important;
        background-color: #f1f1f1;
        }

        /* What it does: Stops email clients resizing small text. */
        * {
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%;
        }

        /* What it does: Centers email on Android 4.4 */
        div[style*="margin: 16px 0"] {
            margin:0 !important;
        }

        /* What it does: Stops Outlook from adding extra spacing to tables. */
        table,
        td {
            mso-table-lspace: 0pt !important;
            mso-table-rspace: 0pt !important;
        }

        /* What it does: Fixes webkit padding issue. Fix for Yahoo mail table alignment bug. Applies table-layout to the first 2 tables then removes for anything nested deeper. */
        table {
            border-spacing: 0 !important;
            border-collapse: collapse !important;
            table-layout: fixed !important;
            margin: 0 auto !important;
        }
        table table table {
            table-layout: auto;
        }

        /* What it does: Uses a better rendering method when resizing images in IE. */
        img {
            -ms-interpolation-mode:bicubic;
        }

        /* What it does: A work-around for iOS meddling in triggered links. */
        .mobile-link--footer a,
        a[x-apple-data-detectors] {
            color:inherit !important;
            text-decoration: underline !important;
        }

        /* What it does: Prevents underlining the button text in Windows 10 */
        .button-link {
            text-decoration: none !important;
        }

    </style>

    <!-- Progressive Enhancements -->
    <style>
a,
.uk-link {
  color:#1e3e79;
  text-decoration: none;
  cursor: pointer;
}
a:hover,
.uk-link:hover {
  color: #497c95;
  text-decoration: none;
}
        /* What it does: Hover styles for buttons */
        .button-td,
        .button-a {
            transition: all 100ms ease-in;
        }
        .button-td:hover,
        .button-a:hover {
            background: #555555 !important;
            border-color: #555555 !important;
        }

        /* Media Queries */
        @media screen and (max-width: 480px) {

            /* What it does: Forces elements to resize to the full width of their container. Useful for resizing images beyond their max-width. */
            .fluid {
                width: 100% !important;
                max-width: 100% !important;
                height: auto !important;
                margin-left: auto !important;
                margin-right: auto !important;
            }

            /* What it does: Forces table cells into full-width rows. */
            .stack-column,
            .stack-column-center {
                display: block !important;
                width: 100% !important;
                max-width: 100% !important;
                direction: ltr !important;
            }
            /* And center justify these ones. */
            .stack-column-center {
                text-align: center !important;
            }

            /* What it does: Generic utility class for centering. Useful for images, buttons, and nested tables. */
            .center-on-narrow {
                text-align: center !important;
                display: block !important;
                margin-left: auto !important;
                margin-right: auto !important;
                float: none !important;
            }
            table.center-on-narrow {
                display: inline-block !important;
            }

        }
		

    </style>

</head>
<body width="100%" bgcolor="#fff;" style="margin: 0;">
    <center style="width: 100%; background: #f1f1f1;">

        <!-- Visually Hidden Preheader Text : BEGIN -->
        <div style="display:none;font-size:1px;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;mso-hide:all;font-family: sans-serif;">
            RussianTour.com
        </div>
        <!-- Visually Hidden Preheader Text : END -->

        <!--
            Set the email width. Defined in two places:
            1. max-width for all clients except Desktop Windows Outlook, allowing the email to squish on narrow but never go wider than 680px.
            2. MSO tags for Desktop Windows Outlook enforce a 680px width.

        -->
        <div style="max-width: 680px; margin: auto;">
            <!--[if mso]>
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="680" align="center">
            <tr>
            <td>
            <![endif]-->



            <!-- Email Body : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="max-width: 680px;">

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td  align="center" class="stack-column-center" style="text-align: justify; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; text-align: center; line-height: 20px; color: #555555;">
 
<img src="https://www.russiantour.com/media/com_acymailing/upload/rti289_1_.png"  width="289" height="165" alt="Русский тур" border="0" class="fluid" style="mso-height-rule: exactly; color: #555555;">

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : END -->							   
							   
 

                <!-- 1 Column Text + Button : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tr>
                                <td ><h2 align="center">ОТПРАВИТЬ ЗАЯВКУ</h2>

<b>Тур:</b> '.$_POST['form-data-to-send'].'<br>
<b>Ссылка:</b> '.$_POST['generated-id'].'<br>
<b>e-mail:</b> '.$_POST['form-mail'].'<br>
<b>Телефон:</b> '.$_POST['form-phone'].'<br>
<b>ФИО:</b>: '.$_POST['form-cognome'].'<br>
<b>Количество человек:</b>: '.$_POST['form-personas'].'<br>
<b>Комментарии:</b>: '.$_POST['form-comments'].'<br>




                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : END -->
              <!-- Clear Spacer : BEGIN -->
                <tr>
                    <td height="40" style="font-size: 0; line-height: 0;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Clear Spacer : END -->

             <!-- 3 Even Columns : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding: 10px 0;">
                        <!--[if mso]>
                        <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                        <tr>
                        <td align="center" valign="top" width="660">
                        <![endif]-->
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:660px;">
                            <tr>
                                <td align="center" valign="top" style="font-size:0;">
                                    <!--[if mso]>
                                    <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                                    <tr>
                                    <td align="left" valign="top" width="220">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:33.33%; min-width:220px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 10px 10px;">
                                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                                        <tr>
                                                            <td align="center">
                                                                  <a href="https://www.russiantour.com"><img src="https://russiantour.com/images/banners/a1/p1.jpg" width="200" height="" border="0" alt="alt_text" class="center-on-narrow"   max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td  align="center"  style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-top: 10px;" class="stack-column-center">
                                                               <a href="https://www.russiantour.com/rus/moskva-peterburg">Тур Москва Санкт-Петербург</a><br>
                                                                <a href="https://www.russiantour.com/rus/zolotoe-kolco">Тур Золотое Кольцо</a><br>
                                                                <a href="https://www.russiantour.com/rus/kruiz-po-volge">Речные круизы</a><br>
                                                                <a href="https://www.russiantour.com/rus/imperial-russia">Туры по Трансибу</a><br>
                                                                <a href="https://www.russiantour.com/rus/chernoe-more">Черное Море</a><br>


                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    <td align="left" valign="top" width="220">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:33.33%; min-width:220px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 10px 10px;">
                                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                                        <tr>
                                                            <td align="center" >
                                                                  <a href="https://www.russiantour.com"><img src="https://russiantour.com/images/banners/a1/p2.jpg" width="200" height="" border="0" alt="alt_text" class="center-on-narrow"   max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  align="center"  style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-top: 10px;" class="stack-column-center">
                                                                <a href="https://www.russiantour.com/rus">Отели Москвы</a><br>
                                                                <a href="https://www.russiantour.com/rus">Отели Санкт-Петербурга</a><br>
                                                                <a href="https://www.russiantour.com/rus">Апартаменты Москвы</a><br>
                                                                <a href="https://www.russiantour.com/rus">Апартаменты Санкт-Петербурга</a><br>
                                                                <a href="https://www.russiantour.com/rus">Отели России</a><br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    <td align="left" valign="top" width="220">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:33.33%; min-width:220px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 10px 10px;">
                                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                                        <tr>
                                                            <td align="center" >
                                                                <a href="https://www.russiantour.com"><img src="https://russiantour.com/images/banners/a1/p3.jpg" width="200" height="" border="0" alt="alt_text" class="center-on-narrow"   max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr  >
                                                            <td align="center"  style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-top: 10px;" class="stack-column-center">
                                                                 <a href="https://www.russiantour.com/rus/biznes-viza">Деловая виза в Россию</a><br>
                                                                <a href="https://www.russiantour.com/rus/ekskursii-moskve">Экскурсии по Москве</a><br>
                                                                <a href="https://www.russiantour.com/rus/ekskursii-sanktpeterburgu">Экскурсии в Санкт-Петербурге</a><br>
                                                                <a href="https://www.russiantour.com/rus/rzd">Жд билеты</a><br>
                                                                <a href="https://www.russiantour.com/rus/avia">Авиабилеты</a><br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                <!-- 3 Even Columns : END -->




            </table>
            <!-- Email Body : END -->

            <!-- Email Footer : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="background-color: #fff;max-width: 680px;">
                <tr>
                                                                                            <tr>
                                                            <td style=" text-align: center;">
                                                                <a href="https://www.russiatour.com"><img src="https://www.russiantour.com/media/com_acymailing/upload/rti289_1_.png" width="289px" height="" border="0" alt="alt_text" class="center-on-narrow" style="max-width: 289px; height: auto;   font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px;  "></a>
                                                            </td>
                                                        </tr>

					<td style="padding: 10px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center ; color: #888888;">
<br>
                     <span class="mobile-link--footer">
					 Международная компания «Русский Тур»  info@russiantour.com
					 <br>
					 Физический адрес: Ул. Шпалерная, д. 22 лит. А., пом. 9Н, 191123 Санкт-Петербург
					 <br>
					 Юридический адрес: Бизнес-центр Петровский Форт, офис 717, 
					 <br> 
					 Финляндский проспект, д.4 лит. А, 194044, Санкт-Петербург
					 <br>Тел.:+78126470690 Факс: +78126470690 *3
 


</span>

                        <br><br>
                  
                    </td>
                </tr>
            </table>
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>



';