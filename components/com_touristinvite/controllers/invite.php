<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

/**
 * Invite controller class.
 */
class TouristinviteControllerInvite extends TouristinviteController
{
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'Invite', $prefix = 'TouristinviteModel')
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

    public function save() {
        $model = $this->getModel('Invite', 'TouristinviteModel');
        $model->save($_POST);
    }

    public function validateinvite() {
        $model = $this->getModel('Invite', 'TouristinviteModel');
        $model->validateinvite($_GET['inviteid']);
    }

    public function showpdf() {
        $model = $this->getModel('Invite', 'TouristinviteModel');
        $model->showpdf($_GET['inviteid']);
    }

    public function delinvite() {
        $model = $this->getModel('Invite', 'TouristinviteModel');
        $model->delinvite($_GET['inviteid']);
    }
}