<?php

require('fpdf.php');

class PDF extends FPDF
{
    public $myY;
    public $table_last_y;
    public $demo;

    // Page header
    function Header()
    {
        // Logo
        $this->Image(JPATH_COMPONENT_SITE.'/assets/php/pdf/logo3.png',10,6,40);
        // Line break
        $this->Ln(20);
    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-20);
        $this->Cell(190,0.1,'',1,0,'C');
        $this->Cell(-190);
        $this->SetFont('Tahoma','',8);
        $this->Cell(0,10,'Russian Tour International LTD',0,0,'L');
        $this->SetY(-15);
        $this->Cell(0,10,'194044, St. Petersburg, Russia 4a, Finlyandskiy Prospekt, off. 424',0,0,'L');
        $this->SetY(-20);
        $this->Cell(0,10,'Tel. +7 812 647 06 90',0,0,'R');
        $this->SetY(-15);
        $this->Cell(0,10,'Fax +7 812 647 06 90',0,0,'R');

        if ($this->demo)
        {
            $this->SetFont('Arial','B',50);
            $this->SetTextColor(50,50,50);
            $this->SetY(-250);
            $this->Write('10','DEMO');

            $this->SetY(-150);
            $this->SetX(50);
            $this->Write('10','DEMO');

            $this->SetY(-50);
            $this->SetX(100);
            $this->Write('10','DEMO');
        }
    }

    function MyNextLine($set = false, $diff = 7)
    {
         if ($set)
            $this->myY = $set;
        else
            $this->myY += $diff;

        $this->SetY($this->myY);
    }
    function TableRow($data) {
        $height = 5;
        $lines = 1;
        $bigs = array();
        $sizes = array();
        foreach ($data as $k=>$v)//считаем количество строк в самой высокой ячейке в строке
        {
            $pre_lines = $this->MultiCell(30,$height,iconv('utf-8','cp1251',$v),0,'C',false,false,false  );
            if ($pre_lines > 1)
            {
                $sizes[] = $pre_lines;
                $bigs[] = $k;
            }

            if ($pre_lines > $lines)
                $lines = $pre_lines;
        }
        foreach ($data as $k=>$v)//рисуем строку
        {
            if ($k==0)//начало обработки строки
            {
                $this->table_last_y += $height;//сдвегает каретку на одинарную строку вниз
                $h = $height;
                if ($lines > 1)
                {
                    $h += $lines*3;
                }

                $x = 10;
            }
            else//сдвигаем каретку по горизонтали для прорисовки следующей ячейки строки
            {
                $x += 30;
            }

            if (in_array($k,$bigs)) {
                $hk = array_keys($bigs,$k);
                $h -= 4*($sizes[$hk[0]]-1);
            }

            $this->TableCell($v,$x,$h);

            if (in_array($k,$bigs)) {
                $hk = array_keys($bigs,$k);
                $h += 4*($sizes[$hk[0]]-1);
            }

            if ($k==5)
            {
                if ($lines > 1)
                {
                    $this->table_last_y += $lines*3;
                }
            }
        }
    }
    function TableCell($data,$x,$h=7) {
        $this->MyNextLine($this->table_last_y);
        $this->SetX($x);
        //$this->Cell(30,7,iconv('utf-8','cp1251',$data),0,0,'C'  );
        $lines = $this->MultiCell(30,$h,iconv('utf-8','cp1251',$data),1,'C',false,$x  );
        return $lines;
    }
    function TableDrawRow($h=7,$y='')
    {
        for ($i=1;$i<=5;$i++)
            $this->Cell(30,$h,iconv('utf-8','cp1251',$y),1);
        $this->Cell(30,$h,iconv('utf-8','cp1251',''),1,1);
    }
}

// Instanciation of inherited class
$pdf = new PDF();
if (isset($data['validate_date']))
    $pdf->demo = false;
else
    $pdf->demo = true;
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->AddFont('Tahoma','','tahoma.php');
$pdf->AddFont('Arial','','arial.php');
$pdf->AddFont('Arial','B','arialbd.php');
$pdf->SetFont('Arial','B',11);

//////////////////////////////////////////////////////////////title
$id = isset($table->pdf_id)?$table->pdf_id:$this->item->pdf_id;

$created = isset($table->invite_created)?$table->invite_created:$this->item->invite_created;
$created = date('d/m/Y',strtotime($created));

$pdf->Cell(0,10,iconv('utf-8','cp1251','ПОДТВЕРЖДЕНИЕ № '.$id),0,1,'C');
$pdf->Cell(0,0,iconv('utf-8','cp1251','о приеме иностранного туриста'),0,1,'C');

//////////////////////////////////////////////////////////////next line
$pdf->Cell(0,10,iconv('utf-8','cp1251','Кратность визы:'),0,0,'L');
$pdf->SetY(45);
$pdf->SetX(50);
$pdf->SetFont('Arial','',11);
$pdf->Cell(0,0,iconv('utf-8','cp1251','въездная-выездная однократная'),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->SetFont('Arial','B',11);
$pdf->MyNextLine(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Въезд с:'),0,0,'L'  );

$pdf->SetFont('Arial','',11);
$pdf->SetX(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$data['invite_from']),0,0,'L'  );

$pdf->SetFont('Arial','B',11);
$pdf->SetX(125);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Выезд:'),0,0,'L'  );

$pdf->SetFont('Arial','',11);
$pdf->SetX(150);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$data['invite_to']),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->SetFont('Arial','B',12);
$pdf->MyNextLine(false,3);
//////////////////////////////////////////////////////////////table prepare
$pdf->table_last_y = $pdf->GetY()+5;
//////////////////////////////////////////////////////////////table draw raws
$pdf->TableDrawRow(10);
/*for ($i=1;$i<=$data['people_count'];$i++)
    $pdf->TableDrawRow();*/
//////////////////////////////////////////////////////////////table header text
$pdf->MyNextLine(56);
$pdf->SetX(10);
$pdf->Cell(30,5,iconv('utf-8','cp1251','Фамилия'),0,0,'C'  );

$pdf->MyNextLine(56);
$pdf->SetX(40);
$pdf->Cell(30,5,iconv('utf-8','cp1251','Имена'),0,0,'C'  );

$pdf->MyNextLine(53);
$pdf->SetX(70);
$pdf->Cell(30,5,iconv('utf-8','cp1251','Дата'),0,0,'C'  );
$pdf->MyNextLine(false,5);
$pdf->SetX(70);
$pdf->Cell(30,5,iconv('utf-8','cp1251','рождения'),0,0,'C'  );
$pdf->MyNextLine();

$pdf->MyNextLine(56);
$pdf->SetX(100);
$pdf->Cell(30,5,iconv('utf-8','cp1251','Пол'),0,0,'C'  );

$pdf->MyNextLine(56);
$pdf->SetX(130);
$pdf->Cell(30,5,iconv('utf-8','cp1251','Гражданство'),0,0,'C'  );

$pdf->MyNextLine(53);
$pdf->SetX(160);
$pdf->Cell(30,5,iconv('utf-8','cp1251','Номер'),0,0,'C'  );
$pdf->MyNextLine(false,5);
$pdf->SetX(160);
$pdf->Cell(30,5,iconv('utf-8','cp1251','паспорта'),0,0,'C'  );
//////////////////////////////////////////////////////////////table rows
$pdf->SetFont('Arial','',11);

$names = array();
for($i=1;$i<=$data['people_count'];$i++)
{
    $pdf->TableRow(array(
        $data['last_name_'.$i],
        $data['first_name_'.$i],
        $data['birthdate_'.$i],
        str_replace(array('m','f'),array('МУЖСКОЙ','ЖЕНСКИЙ'),$data['sex_'.$i]),
        $data['gragd_'.$i],
        $data['passport_'.$i]
    ));
    $names[] = $data['last_name_'.$i].' '.$data['first_name_'.$i];
}
//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine();

$pdf->SetFont('Arial','B',11);
$pdf->MyNextLine(false,10);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Цель поездки:'),0,0,'L'  );

$pdf->SetFont('Arial','',11);
$pdf->SetX(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Туризм'),0,0,'L'  );

//////////////////////////////////////////////////////////////next line (block)
$pdf->SetFont('Arial','B',11);
$pdf->MyNextLine();
$pdf->Cell(0,0,iconv('utf-8','cp1251','Маршрут и место размещения:'),0,0,'L'  );

$pdf->SetFont('Arial','',11);
$pdf->MyNextLine(false,6);
//invite_hotels = ОТЕЛЬ_АНГЛ | ОТЕЛЬ_РУС | ГОРОД_АНГЛ | ГОРОД_РУС
$htls_rus = array();
$htls_en = array();
$cities_rus = array();
$cities_en = array();
foreach ($data['invite_hotels'] as $v)
{
    $expl = explode('|',$v);
    $htls_rus[] = 'Отель «'.$expl[1].'» ('.$expl[3].')';
    $htls_en[] = 'Hotel «'.$expl[0].'» ('.$expl[2].')';
}
foreach ($data['invite_cities'] as $v)
{
    $expl = explode('|',$v);
    $cities_rus[] = $expl[1];
    $cities_en[] = $expl[0];
}
$pdf->MultiCell(0,0,iconv('utf-8','cp1251',implode(', ',$htls_rus)),0,'L'  );
//$pdf->Cell(0,0,iconv('utf-8','cp1251',implode(',',$htls_rus)),0,0,'L'  );
//////////////////////////////////////////////////////////////next line (block)
$pdf->SetFont('Arial','B',11);
$pdf->MyNextLine(false,10);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Принимающая организация, адрес, № референса:'),0,0,'L'  );

$pdf->SetFont('Arial','',11);
$pdf->MyNextLine(false,5);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Общество с ограниченной ответственностью Международная Компания «Русский Тур», '),0,0,'L'  );
$pdf->MyNextLine(false,5);
$pdf->Cell(0,0,iconv('utf-8','cp1251','референс номер 012877, Санкт-Петербург, 194044,  Финляндский проспект 4, литер А, офис 424'),0,0,'L'  );
/*$pdf->MyNextLine(false,4);
$pdf->Cell(0,0,iconv('utf-8','cp1251','4, литер А, офис 424'),0,0,'L'  );*/

//////////////////////////////////////////////////////////////next line
$pdf->SetFont('Arial','B',11);
$pdf->MyNextLine(-80);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Подпись:'),0,0,'L'  );

//////////////////////////////////////////////////////////////next line (sign block)
$pdf->MyNextLine();
$pdf->Image(JPATH_COMPONENT_SITE.'/assets/php/pdf/timbro_cropped.png',15,$pdf->getY(),48);
$pdf->Image(JPATH_COMPONENT_SITE.'/assets/php/pdf/podpis.png',90,$pdf->getY(),32);
$pdf->MyNextLine(false,11);
$pdf->SetFont('Tahoma','',8);
$pdf->SetX(120);
$pdf->Cell(0,0,iconv('utf-8','cp1251','/Черемшенко О.Н./'),0,0,'L'  );
$pdf->MyNextLine(false,5);
$pdf->SetX(120);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Ген.Директор'),0,0,'L'  );
$pdf->MyNextLine();
$pdf->SetFont('Arial','',11);
$pdf->SetX(70);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$created),0,0,'L'  );

//////////////////////////////////////////////////////////////NEXT PAGE
//////////////////////////////////////////////////////////////title
$pdf->AddPage();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,10,iconv('utf-8','cp1251','Voucher № '.$id),0,1,'C');
$pdf->Cell(0,0,iconv('utf-8','cp1251','Ваучер № '.$id),0,1,'C');
$pdf->Cell(0,10,iconv('utf-8','cp1251','на оказание туристических услуг'),0,1,'C');

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine(55);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Date of entry:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$data['invite_from']),0,0,'L'  );

$pdf->SetFont('Arial','B',12);
$pdf->SetX(125);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Date of exit:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(150);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$data['invite_to']),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Въезд с:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$data['invite_from']),0,0,'L'  );

$pdf->SetFont('Arial','B',12);
$pdf->SetX(125);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Выезд:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(150);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$data['invite_to']),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine(false,10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Name:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
//$pdf->Cell(0,0,iconv('utf-8','cp1251',implode(',',$names)),0,0,'L'  );
$pdf->MultiCell(0,0,iconv('utf-8','cp1251',implode(', ',$names)),0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Object of journey:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Tourism'),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Цель поездки:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Туризм'),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine(false,10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Hotel:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
//$pdf->Cell(0,0,iconv('utf-8','cp1251',implode(',',$htls_en)),0,0,'L'  );
$pdf->MultiCell(0,0,iconv('utf-8','cp1251',implode(', ',$htls_en)),0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Размещение:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
//$pdf->Cell(0,0,iconv('utf-8','cp1251',implode(',',$htls_rus)),0,0,'L'  );
$pdf->MultiCell(0,0,iconv('utf-8','cp1251',implode(', ',$htls_rus)),0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine(false,10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Programme:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
//$pdf->Cell(0,0,iconv('utf-8','cp1251','City tour in '.implode(',',$cities_en)),0,0,'L'  );
$pdf->MultiCell(0,0,iconv('utf-8','cp1251','City tour in '.implode(', ',$cities_en)),0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->MyNextLine(false,10);
$pdf->SetFont('Arial','B',12);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Программа:'),0,0,'L'  );

$pdf->SetFont('Arial','',12);
$pdf->SetX(50);
//$pdf->Cell(0,0,iconv('utf-8','cp1251','Экскурсионное обслуживание в городах: '.implode(',',$cities_rus)),0,0,'L'  );
$pdf->MultiCell(0,0,iconv('utf-8','cp1251','Экскурсионное обслуживание в городах: '.implode(', ',$cities_rus)),0,'L'  );

//////////////////////////////////////////////////////////////next line (block)
$pdf->MyNextLine(false,10);
$pos = $pdf->getY();

///////////////////////////////left block
$pdf->SetFont('Arial','B',12);
$pdf->MyNextLine();
$pdf->Cell(0,0,iconv('utf-8','cp1251','Receiving company, address:'),0,0,'L'  );
$pdf->MyNextLine();
$pdf->Cell(0,0,iconv('utf-8','cp1251','Принимающая организация, адрес:'),0,0,'L'  );

///////////////////////////////right block
$pdf->SetFont('Arial','',12);

$pdf->MyNextLine($pos);
$pdf->SetX(90);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Общество с ограниченной ответственностью'),0,0,'L'  );

$pdf->MyNextLine();
$pdf->SetX(90);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Международная Компания «Русский Тур», референс'),0,0,'L'  );

$pdf->MyNextLine();
$pdf->SetX(90);
$pdf->Cell(0,0,iconv('utf-8','cp1251','номер 012877, Санкт-Петербург, 194044'),0,0,'L'  );

$pdf->MyNextLine();
$pdf->SetX(90);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Финляндский проспект 4, литер А, офис 424'),0,0,'L'  );

//////////////////////////////////////////////////////////////next line
$pdf->SetFont('Arial','B',12);
$pdf->MyNextLine(false,10);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Ministry of Foreign Affairs Index № 012877'),0,0,'L'  );

//////////////////////////////////////////////////////////////next lines pair
$pdf->SetFont('Arial','',12);
//$pdf->MyNextLine();
$pdf->MyNextLine(false,10);
$pdf->Cell(0,0,iconv('utf-8','cp1251',''),0,0,'L'  );
$pdf->MyNextLine();
$pdf->Cell(0,0,iconv('utf-8','cp1251',''),0,0,'L'  );

//////////////////////////////////////////////////////////////next line (sign block)
$pdf->MyNextLine(-70);
$pdf->Image(JPATH_COMPONENT_SITE.'/assets/php/pdf/timbro_cropped.png',15,$pdf->getY(),48);
$pdf->Image(JPATH_COMPONENT_SITE.'/assets/php/pdf/podpis.png',90,$pdf->getY(),32);

$pdf->MyNextLine(false,12);
$pdf->SetFont('Arial','B',12);
$pdf->SetX(63);
$pdf->Cell(0,0,iconv('utf-8','cp1251','Signature:'),0,0,'L'  );

$pdf->SetFont('Tahoma','',8);
$pdf->SetX(120);
$pdf->Cell(0,0,iconv('utf-8','cp1251','/Cheremshenko O.N./'),0,0,'L'  );

$pdf->MyNextLine(false,5);
$pdf->SetX(120);
$pdf->Cell(0,0,iconv('utf-8','cp1251','General Manager'),0,0,'L'  );

$pdf->MyNextLine();
$pdf->SetFont('Arial','',12);
$pdf->SetX(67);
$pdf->Cell(0,0,iconv('utf-8','cp1251',$created),0,0,'L'  );

$pdf->Output();
?>
