<?php

/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class TouristinviteController extends JControllerLegacy {
    //public $paymentUrl = 'test.paymentgate.ru/rebpayment';
    public $paymentUrl = '3dsec.paymentgate.ru/ipay';
    /**
     * Method to display a view.
     *
     * @param	boolean			$cachable	If true, the view output will be cached
     * @param	array			$urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return	JController		This object to support chaining.
     * @since	1.5
     */
    public function display($cachable = false, $urlparams = false) {
        if ($_GET['checkUser'])
        {//echo 123;exit;
            $this->checkUser($_GET['checkUser']);
            exit;
        }
        $viewName = JFactory::getApplication()->input->get('view');

        if ($viewName!='invite')
        {
            $user = JFactory::getUser();
            if ($user->get('id')) $this->updateOwner($user->get('id'),($viewName=='sendmail'));
            if ($viewName!='sendmail' && $viewName !='step3manual')
            {
                if ($_POST['userinfo']){
                    $userinfo = new stdClass();
                    $userinfo->visaandinivte_id = $_POST['userinfo']['visaandinivte_id'];
                    $userinfo->nome_del_corriere_postale = $_POST['userinfo']['nome_del_corriere_postale'];
                    $userinfo->numero_di_contratto = $_POST['userinfo']['numero_di_contratto'];
                    $userinfo->nome_e_cognome = $_POST['userinfo']['nome_e_cognome'];
                    $userinfo->societa = $_POST['userinfo']['societa'];
                    $userinfo->telefono = $_POST['userinfo']['telefono'];
                    $userinfo->nazione = $_POST['userinfo']['nazione'];
                    $userinfo->citta = $_POST['userinfo']['citta'];
                    $userinfo->indirizzo = $_POST['userinfo']['indirizzo'];
                    $userinfo->codice_postale = $_POST['userinfo']['codice_postale'];

                    JFactory::getDbo()->insertObject('#__touristinvite_userinfo', $userinfo, 'id');
                    $this->step4userInfoSendMail($user,$userinfo);
                }
                //isvet
                if ($_POST['modulo']){
                    $modulo = new stdClass();
                    $modulo->visamodulo_id = $_POST['modulo']['visamodulo_id'];
                    $modulo->luogo_di_nascita = $_POST['modulo']['luogo_di_nascita'];
                    $modulo->data_di_rilascio = $_POST['modulo']['data_di_rilascio'];
                    $modulo->precedenti_viaggi = $_POST['modulo']['precedenti_viaggi'];
                    $modulo->indirizzo_di_residenza = $_POST['modulo']['indirizzo_di_residenza'];
                    $modulo->numero_di_telefono = $_POST['modulo']['numero_di_telefono'];
                    $modulo->attuale_posto = $_POST['modulo']['attuale_posto'];
                    $modulo->bambini_sotto = $_POST['modulo']['bambini_sotto'];
                    $modulo->parenti_attualmente = $_POST['modulo']['parenti_attualmente'];
                    $modulo->commenti = $_POST['modulo']['commenti'];

                    JFactory::getDbo()->insertObject('#__touristinvite_modulo', $modulo, 'id');
                    $this->step4userModuloSendMail($user,$modulo);
                }



                //isvet end
                $this->step2();

                /*$userName = 'visto-russia-api';
                $password = 'visto-russia';*/
                /*$userName = 'visto';
                $password = 'PcH2PC72VH';*/
                $userName = JFactory::getApplication()->get('REBuserName');
                $password = JFactory::getApplication()->get('REBpassword');


                if ($viewName=='del' && intval($_GET['id']))
                {
                    $query = 'SELECT * FROM #__touristinvite_visaandinivte WHERE id = '.intval($_GET['id']). ' LIMIT 1' ;
                    $db=JFactory::getDBO();
                    $db->setQuery($query);
                    $lastRecord = $db->loadAssoc();
                    if (!$lastRecord || $lastRecord['user_id']!=$user->get('id'))
                    {
                        echo 'not found';
                        return;
                    }
                    ///////
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);
                    $conditions = array(
                        $db->quoteName('id') . ' = '.intval($_GET['id'])
                    );
                    $query->delete($db->quoteName('#__touristinvite_visaandinivte'));
                    $query->where($conditions);
                    $db->setQuery($query);
                    $result = $db->execute();
                    ///////
                    $db = JFactory::getDbo();
                    $query = $db->getQuery(true);
                    $conditions = array(
                        $db->quoteName('visa_invite_id') . ' = '.intval($_GET['id'])
                    );
                    $query->delete($db->quoteName('#__touristinvite_participants'));
                    $query->where($conditions);
                    $db->setQuery($query);
                    $result = $db->execute();
                    /////
                    JFactory::getApplication()->redirect('?option=com_touristinvite&view=list');exit;
                }
                elseif ($viewName=='list'){
                    $this->getTouristinviteDataAll($user);
                }
                else
                {
                    if ($_GET['getResponse']){
                        $orderID = $_GET['orderId'];
                        /*$info = var_export($_POST,1);
                        $info .= "/n----------------------------------/n";
                        file_put_contents('/home/visto/web/visto-russia.com/public_html/logs/payment.log',$info);*/

                        $req = 'https://'.$this->paymentUrl.'/rest/getOrderStatus.do?'.
                            'userName='.$userName.
                            '&password='.$password.
                            '&orderId='.$orderID;
                        //echo $req;
                        $resp = file_get_contents($req);
                        //echo $resp;
                        $resp = json_decode($resp);

                        if ($resp->ErrorCode==='0'){
                            if (strpos($_SERVER["HTTP_HOST"],'russiantour.com')===false)
                            {
                                $db = JFactory::getDBO();
                                /*$qqq = 'SELECT *  FROM #__touristinvite_payments';
                                $db->setQuery($qqq);
                                $payment = $db->loadAssocList();*/


                                $query = 'SELECT *  FROM #__touristinvite_payments WHERE '.
                                    'paymentID = \''.$resp->OrderNumber.'\''
                                    . ' and amount = '.$resp->Amount
                                    .' and outOrderID = \''.$orderID.'\''
                                    .' and status = 0 '
                                    .'LIMIT 1' ;
                                /*echo $query."\n\n";
                                var_dump($payment);exit;*/
                                $db->setQuery($query);

                                $payment = $db->loadAssoc();
                            }
                            if ($payment['id'])
                            {
                                $db = JFactory::getDBO();
                                $query = "UPDATE #__touristinvite_payments SET status = 1 WHERE  id = ".$payment['id']." AND outOrderID = '".$orderID."'";
                                $db->setQuery($query);
                                $db->execute();

                                $db = JFactory::getDBO();
                                $query = "UPDATE #__touristinvite_visaandinivte SET status = 2 WHERE  id = ".$payment['inviteorvisaid'];
                                $db->setQuery($query);
                                $db->execute();

                                $lastRecord = $this->getTouristinviteData($user);
                                $lastRecord = $lastRecord[0];

                                $payResult = 'success';
                                $isManual = false;

                            }
                            else {ini_set('display_errors',1);
                                $query = 'SELECT *  FROM #__touristinvite_manualpayments WHERE '.
                                    'paymentID = \''.$resp->OrderNumber.'\''
                                    . ' and amountRub = '.$resp->Amount
                                    .' and outOrderID = \''.$orderID.'\''
                                    .' and status = 0 '
                                    .'LIMIT 1' ;

                                $db = JFactory::getDBO();
                                $db->setQuery($query);

                                $payment = $db->loadAssoc();
                                if ($_GET['test']) {
                                    echo $query."\n\n";
                                    var_dump($payment);
                                    exit;
                                }
                                if ($payment['id'])
                                {
                                    $db = JFactory::getDBO();
                                    $query = "UPDATE #__touristinvite_manualpayments SET status = 1 WHERE  id = ".$payment['id']." AND outOrderID = '".$orderID."'";
                                    $db->setQuery($query);
                                    $db->execute();

                                    $payResult = 'success';
                                    $isManual = true;
                                }
                                else
                                {
                                    $payResult = 'fail';
                                    $file = 'notpayed';
                                }
                            }

                            if (!$isManual) {
                                $this->sendStep4Mail();
                                JFactory::getApplication()->redirect('?option=com_touristinvite&view=step4&payResult='.$payResult);exit;
                            }
                            else{
                                if ($payment['id']) $this->sendStep4MailManual($payment);
                                JFactory::getApplication()->redirect('?option=com_touristinvite&view=step4&manual='.$payment['id'].'&payResult='.$payResult);exit;
                            }
                        }
                        else
                        {
                            var_dump($resp);
                            exit;echo 'fail...';
                        }
                        //var_dump($payment);

                    }

                    $db = JFactory::getDBO();
                    $id = '';
                    if (intval($_GET['id']))
                    {
                        $id = intval($_GET['id']);
                        $id = ' id = '.$id.' AND ';
                    }
                    $query = 'SELECT * FROM #__touristinvite_visaandinivte WHERE '.$id.' user_id = '.$user->get('id'). ' ORDER BY id DESC LIMIT 1' ;
                    $db->setQuery($query);
                    $lastRecord = $db->loadAssoc();

                    if ($viewName =='step3' && $lastRecord['status']<1)
                    {
                        $query = 'UPDATE #__touristinvite_visaandinivte SET status = 1 WHERE status = 0 and id = '.intval($lastRecord['id']);
                        $db->setQuery($query);
                        $db->execute();
                    }

                    if (!$_GET['manualPayment'] && !$_GET['payResult'] && (!isset($lastRecord['status']) || $lastRecord['status']==2))
                    {

                        if ($viewName =='step3')
                        {
                            JFactory::getApplication()->redirect('?option=com_touristinvite&view=step1');exit;
                        }
                    }
                    else
                    {
                        if ($_GET['preparePayment'] && ($lastRecord['status']==1 || $_GET['manualPayment']))
                        {
                            if ($_GET['preparePayment']==2 && !$_GET['manualPayment'])
                            {
                                $query = 'SELECT *  FROM #__touristinvite_payments WHERE inviteorvisaid = '.$lastRecord['id'].' LIMIT 1' ;
                                $db->setQuery($query);
                                $payment = $db->loadAssoc();
                                if ($payment['id'])
                                    exit;
                            }

                            if (isset($lastRecord['id']) || $_GET['manualPayment']){
                                $curs = false;
                                $cursFile = file_get_contents('curs.txt');
                                if ($cursFile)
                                {
                                    $cursFile = explode('|',$cursFile);
                                    if ((date('H')+3)<14 || date('d.m.Y')==date('d.m.Y',$cursFile[1]))
                                    {
                                        $curs = $cursFile[0];
                                    }
                                }
                                if (!$curs)
                                {
                                    $xml = file_get_contents('http://www.cbr.ru/scripts/XML_daily.asp?date_req='.date('d/m/Y',time()+3600*24));
                                    if ($xml)
                                    {
                                        $movies = new SimpleXMLElement($xml);
                                        foreach ($movies->Valute as $valute){
                                            if ($valute->CharCode == 'EUR')
                                            {
                                                $curs = floatval(str_replace(',','.',$valute->Value));
                                                file_put_contents('curs.txt',$curs.'|'.time().'|'.date('d.m.Y H:i:s'));
                                                break;
                                            }
                                        }
                                    }
                                    elseif ($cursFile[0])
                                        $curs = $cursFile[0];
                                    else
                                        $curs = 85;
                                }
                                if (!$_GET['manualPayment'])
                                {
                                    $skidka = 1;
                                    $bdskidka = intval($lastRecord['skidka_val']);
                                    if ($bdskidka > 1 && $bdskidka < 100)
                                        $skidka = (100-$bdskidka)/100;
                                    $cost = intval($lastRecord['total_cost']*100*$skidka*$curs*1.02);

                                    $paymentID = time();
                                    $participantProfile = new stdClass();
                                    $participantProfile->status = 0;
                                    $participantProfile->inviteorvisaid = $lastRecord['id'];
                                    $participantProfile->paymentID = $paymentID;
                                    $participantProfile->amount = $cost;
                                    $participantProfile->curs = $curs;

                                    JFactory::getDbo()->insertObject('#__touristinvite_payments', $participantProfile, 'id');
                                    if ($participantProfile->id){
                                        if ($_GET['preparePayment']!=2)
                                        {
                                            //$req = 'https://'.$this->paymentUrl.'/rest/register.do?'.//odnostadiybiy platezh
                                            $req = 'https://'.$this->paymentUrl.'/rest/registerPreAuth.do?'.//dvuhstadiyniy platezh
                                                'userName='.$userName.
                                                '&password='.$password.
                                                '&orderNumber='.$paymentID.
                                                '&amount='.$cost.
                                                '&language=en'.
                                                //'&returnUrl=http://www.visto-russia.com/step2?getResponse=1';
                                                '&returnUrl='.JFactory::getApplication()->get('REBreturnUrlDomen').'/modulo-visto-turistico?getResponse=1';
                                            $resp = file_get_contents($req);
                                            $resp = json_decode($resp);
                                            if ($resp->formUrl)
                                            {
                                                $db = JFactory::getDBO();
                                                $query = "UPDATE #__touristinvite_payments SET outOrderID = '".$resp->orderId."' WHERE  id = ".intval($participantProfile->id);
                                                $db->setQuery($query);
                                                if ($db->execute())
                                                    header('Location: '.$resp->formUrl);
                                                echo 'error!';
                                            }
                                        }
                                        exit;
                                    }
                                }
                                else
                                {//exit;
                                    $code = strip_tags(addslashes($_GET['code']));
                                    $query = 'SELECT *  FROM #__touristinvite_manualpayments WHERE status = 0 AND '.
                                        'paymentID = \''.$code.'\' LIMIT 1' ;
                                    $db->setQuery($query);
                                    $payment = $db->loadAssoc();
                                    if ($payment['id']){
                                        $payTime = strtotime($payment['timeCreatedLinl']);

                                        $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
                                        $db->setQuery($query);
                                        $curTime = $db->loadAssoc();
                                        $curTime = strtotime($curTime['curTime']);

                                        if ($curTime >= $payTime+3600*24){
                                            echo '24 hours expired';
                                            exit;
                                        }
                                        $cost = intval($payment['amountEuro']*100*$curs*1.02);

                                        $query = 'UPDATE #__touristinvite_manualpayments SET'.
                                            ' amountRub = '.$cost.','.
                                            ' curs = '.$curs.','.
                                            ' status = 0 ,'.
                                            ' payTime = CURRENT_TIMESTAMP'.
                                            ' WHERE paymentID = \''.$payment['paymentID'].'\'';
                                        $db->setQuery($query);
                                        $db->execute();

                                        $req = 'https://'.$this->paymentUrl.'/rest/registerPreAuth.do?'.//dvuhstadiyniy platezh
                                            'userName='.$userName.
                                            '&password='.$password.
                                            '&orderNumber='.$payment['paymentID'].
                                            '&amount='.$cost.
                                            '&description='.$payment['description'].
                                            '&language=en'.
                                            //'&returnUrl=http://www.visto-russia.com/step2?getResponse=1';
                                            '&returnUrl='.JFactory::getApplication()->get('REBreturnUrlDomen').'/modulo-visto-turistico?getResponse=1';
                                        $resp = file_get_contents($req);
                                        $resp = json_decode($resp);
                                        if ($resp->formUrl)
                                        {
                                            $db = JFactory::getDBO();
                                            $query = "UPDATE #__touristinvite_manualpayments SET outOrderID = '".$resp->orderId."' WHERE  id = ".intval($payment['id']);
                                            $db->setQuery($query);
                                            if ($db->execute())
                                                header('Location: '.$resp->formUrl);
                                            echo 'error!';
                                        }
                                    }
                                    else{
                                        echo 'ERROR! Payment not found!';
                                    }
                                    exit;
                                }
                            }
                        }
                        elseif ($_GET['set2']){
                            $db = JFactory::getDBO();
                            $query = "UPDATE #__touristinvite_visaandinivte SET status = 2 WHERE  id = ".$lastRecord['id'];
                            $db->setQuery($query);
                            $db->execute();

                            JFactory::getApplication()->redirect('?option=com_touristinvite&view=step4');exit;
                        }
                        if (JFactory::getApplication()->input->get('view')!='step4' && JFactory::getApplication()->input->get('view')!='step1')
                        {
                            if ($lastRecord['status']==0 && JFactory::getApplication()->input->get('view')!='step2')
                            {
                                JFactory::getApplication()->redirect('?option=com_touristinvite&view=step2');exit;
                            }
                            elseif ($lastRecord['status']==1 && JFactory::getApplication()->input->get('view')!='step3'/* !$_GET['readyToPay']*/)
                            {
                                //JFactory::getApplication()->redirect('?option=com_touristinvite&view=step3&readyToPay=1');exit;
                            }
                        }
                    }
                    $this->getTouristinviteData($user);
                }
            }
        }


        require_once JPATH_COMPONENT . '/helpers/touristinvite.php';

        $view = JFactory::getApplication()->input->getCmd('view', 'hotels');
        JFactory::getApplication()->input->set('view', $view);


        parent::display($cachable, $urlparams);

        return $this;
    }

    public function getTouristinviteData($user,$id=''){
        $db = JFactory::getDBO();
        if ($id<>'' || intval($_GET['id']))
        {
            if ($id=='' && intval($_GET['id']))
                $id = intval($_GET['id']);
            $id = ' id = '.$id.' AND ';
        }
        $query = 'SELECT *  FROM #__touristinvite_visaandinivte WHERE '.$id.' user_id = '.$user->get('id'). ' ORDER BY id DESC LIMIT 1' ;
        $db->setQuery($query);
        //$this->lastRecord = $db->loadAssocList();
        $lastRecord = $db->loadAssocList();
        JFactory::getApplication()->input->set('lastRecord', $lastRecord);

        if ($lastRecord[0]['id'])
        {
            $db = JFactory::getDBO();
            $query = 'SELECT *  FROM #__touristinvite_participants WHERE  visa_invite_id = '.$lastRecord[0]['id'];
            $db->setQuery($query);
            //$this->participants = $db->loadAssocList();
            JFactory::getApplication()->input->set('participants', $db->loadAssocList());
        }
        return $lastRecord;
    }

    public function getTouristinviteDataAll($user){
        $result = array();
        $db = JFactory::getDBO();
        $query = 'SELECT *  FROM #__touristinvite_visaandinivte WHERE  user_id = '.$user->get('id'). ' ORDER BY id DESC LIMIT 5' ;
        $db->setQuery($query);
        $records = $db->loadAssocList();
        if (count($records))
        {
            foreach ($records as $record){
                $db2 = JFactory::getDBO();
                $query = 'SELECT *  FROM #__touristinvite_participants WHERE  visa_invite_id = '.$record['id'];
                $db->setQuery($query);
                $participants = $db2->loadAssocList();
                $result[] = array(
                    'record' => $record,
                    'participants' => $participants
                );
            }
        }
        JFactory::getApplication()->input->set('records', $result);
    }

    public function step2(){
        $params = JComponentHelper::getParams('com_touristinvite')->get('params');
        $user = JFactory::getUser();
//ini_set('display_errors',1);
        if (JFactory::getApplication()->input->get('view')=='step2' && count($_POST)){
            //var_dump($user);exit;
            $date_from = explode('/',$_POST['date_from']);
            if (count($date_from)==3)
                $date_from = $date_from[2].'/'.$date_from[1].'/'.$date_from[0];
            else
                $date_from = false;

            $date_to = explode('/',$_POST['date_to']);
            if (count($date_to)==3)
                $date_to = $date_to[2].'/'.$date_to[1].'/'.$date_to[0];
            else
                $date_to = false;

            $participants = array();
            foreach ($_POST['participants'] as $participant){
                if ($participant['first_name'] &&
                    $participant['second_name'] &&
                    $participant['gender'] &&
                    $participant['birthdate'] &&
                    $participant['nationality'] &&
                    $participant['passport'])
                {
                    $participant['birthdate'] = explode('/',$participant['birthdate']);
                    if (count($participant['birthdate'])==3)
                        $participant['birthdate'] = $participant['birthdate'][2].'-'.$participant['birthdate'][1].'-'.$participant['birthdate'][0];
                    else
                        $participant['birthdate'] = false;

                    $participant['nationality'] = strip_tags(addslashes(trim($participant['nationality'])));
                    $participant['citta_di_residenza'] = strip_tags(addslashes(trim($participant['citta_di_residenza'])));
                    $participants[] = $participant;
                }
            }

            $consulate = $_POST['consulate'];
            $insurance = isset($_POST['insurance'])?1:0;
            $urgent = isset($_POST['urgent'])?1:0;
            $form_filling = isset($_POST['form_filling'])?1:0;
            //$rus_visa = isset($_POST['rus_visa'])?1:0;
            $rus_visa = ($_POST['ho_registrazione']=='0')?0:1;

            foreach($_POST['cities_to_visit'] as $k=>$city){
                if (!$city)
                    unset($_POST['cities_to_visit'][$k]);
                else
                    $_POST['cities_to_visit'][$k] = strip_tags(addslashes(trim($city)));
            }
            $cities_to_visit = implode(',',$_POST['cities_to_visit']);

            $shipping = $_POST['shipping'];

            $add_hotel = isset($_POST['add_hotel'])?1:0;
            $add_appartamento = isset($_POST['add_appartamento'])?1:0;
            $add_volo_treno = isset($_POST['add_volo_treno'])?1:0;
            $add_transfers = isset($_POST['add_transfers'])?1:0;
            $add_escursioni = isset($_POST['add_escursioni'])?1:0;
            $add_other = strip_tags(addslashes(trim($_POST['add_other'])));

            $total_cost = $_POST['totalSumm'];

            if ($date_from && $date_to && count($participants) && $cities_to_visit)
            {
                // Create and populate an object.
                $profile = new stdClass();
                if ($_POST['promocode']){
                    $skidki = [];
                    for ($i=1;$i<=5;$i++){
                        if ($params->{'skidka'.$i.'1'} && intval($params->{'skidka'.$i.'2'})){
                            $skidki[$params->{'skidka'.$i.'1'}] = intval($params->{'skidka'.$i.'2'});
                        }
                    }

                    if (count($skidki) && $skidki[$_POST['promocode']])
                    {
                        $profile->skidka_name = $_POST['promocode'];
                        $profile->skidka_val = $skidki[$_POST['promocode']];
                    }
                }
                $profile->date_from = $date_from;
                $profile->date_to = $date_to;
                $profile->consulate = $consulate;
                $profile->insurance = $insurance;
                $profile->urgent = $urgent;
                $profile->form_filling = $form_filling;
                $profile->rus_visa = $rus_visa;
                $profile->cities_to_visit = $cities_to_visit;
                $profile->shipping = $shipping;
                $profile->add_hotel = $add_hotel;
                $profile->add_appartamento = $add_appartamento;
                $profile->add_volo_treno = $add_volo_treno;
                $profile->add_transfers = $add_transfers;
                $profile->add_escursioni = $add_escursioni;
                $profile->add_other = $add_other;
                $profile->total_cost = $total_cost;
                if ($user->get('id'))
                    $profile->user_id = $user->get('id');
                if ($_POST['ho_registrazione']=='piter')
                    $profile->ho_registrazione_piter = 1;
                if ($_POST['ho_registrazione']=='moscow')
                    $profile->ho_registrazione_moscow = 1;

                // Insert the object into the user profile table.
                if (intval($_POST['id']))
                {
                    $query = 'SELECT * FROM #__touristinvite_visaandinivte WHERE id = '.intval($_POST['id']). ' LIMIT 1' ;
                    $db=JFactory::getDBO();
                    $db->setQuery($query);
                    $lastRecord = $db->loadAssoc();
                    if (!$lastRecord || $lastRecord['user_id']!=$user->get('id'))
                    {
                        echo 'not found';
                        return;
                    }
                    $profile->id = intval($_POST['id']);
                    $result = JFactory::getDbo()->updateObject('#__touristinvite_visaandinivte', $profile, 'id');
                    $db = JFactory::getDbo();

                    $query = $db->getQuery(true);

                    // delete all custom keys for user 1001.
                    $conditions = array(
                        $db->quoteName('visa_invite_id') . ' = '.$profile->id
                    );

                    $query->delete($db->quoteName('#__touristinvite_participants'));
                    $query->where($conditions);

                    $db->setQuery($query);

                    $result = $db->execute();
                }
                else
                {
                    $result = JFactory::getDbo()->insertObject('#__touristinvite_visaandinivte', $profile, 'id');
                    $lastRecord = get_object_vars($profile);
                }
                if (isset($profile->id))
                {
                    foreach ($participants as $participant){
                        $participantProfile = new stdClass();
                        $participantProfile->first_name = $participant['first_name'];
                        $participantProfile->second_name = $participant['second_name'];
                        $participantProfile->gender = $participant['gender'];
                        $participantProfile->birthdate = $participant['birthdate'];
                        $participantProfile->nationality = $participant['nationality'];
                        $participantProfile->passport = $participant['passport'];
                        $participantProfile->citta_di_residenza = $participant['citta_di_residenza'];
                        $participantProfile->visa_invite_id = $profile->id;

                        JFactory::getDbo()->insertObject('#__touristinvite_participants', $participantProfile, 'id');
                    }

                    if (!$user->get('id'))
                        echo $profile->id;
                    else
                        $this->sendStep2Mail($lastRecord,$participants);
                    if (intval($_POST['tolist']))
                    {
                        JFactory::getApplication()->redirect('?option=com_touristinvite&view=list');
                        exit;
                    }
                    if (!$user->get('id'))
                    {
                        //////send mail begin
                        $mailer = JFactory::getMailer();
                        $config = JFactory::getConfig();
                        $sender = array(
                            $config->get( 'mailfrom' ),
                            $config->get( 'fromname' )
                        );

                        $mailer->setSender($sender);
                        $recipient = 'mail@russiantour.com';

                        $mailer->addRecipient($recipient);
                        //ini_set('display_errors',1);
                        $lastRecord = get_object_vars($profile);
                        //$this->getTouristinviteData($user,$profile->id);
                        $userName = $_POST['_newUserName'];
                        $userEmail = $_POST['_newUserEmail'];
                        $userPhone = $_POST['_newUserPhone'];
                        include('./components/com_touristinvite/views/mails/send_mail_no.php');
                        $mailer->isHTML(true);
                        $mailer->setSubject($subject);
                        $mailer->setBody($body);
                        $send = $mailer->Send();
                        /*if ( $send !== true ) {
                            echo 'Error sending email: ' . $send->__toString();
                        } else {
                            echo 'Mail sent';
                        }*/
                        //////send mail end
                    }
                }
            }
            else
                echo '<span style="color:red;">ERROR</span>';

            if (!$user->get('id'))
                exit;
        }
    }

    public function step4userInfoSendMail($user,$userinfo){
        //////send mail begin
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $mailer->setSender($sender);
        $recipient = $user->email;

         $mailer->addRecipient($recipient);
         $mailer->addRecipient('mail@russiantour.com');

         include('./components/com_touristinvite/views/mails/spedizione.php');
        $mailer->isHTML(true);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ( $send !== true ) {
            echo 'Error sending email: ' . $send->__toString();
        } else {
            //echo 'Mail sent';
        }
        //////send mail end
    }
	
	    public function step4userModuloSendMail($user,$modulo){
        //////send mail begin
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $mailer->setSender($sender);
        $recipient = $user->email;

        $mailer->addRecipient($recipient);
        $mailer->addRecipient('mail@russiantour.com');

        include('./components/com_touristinvite/views/mails/modulo.php');
        $mailer->isHTML(true);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ( $send !== true ) {
            echo 'Error sending email: ' . $send->__toString();
        } else {
            //echo 'Mail sent';
        }
        //////send mail end
    }

    public function checkUser($email){
        $query = "SELECT * FROM #__users WHERE email = '".strip_tags(addslashes(trim($email))). "' LIMIT 1" ;
        $db=JFactory::getDBO();
        $db->setQuery($query);
        $user = $db->loadAssoc();
        if (isset($user['id']))
            echo $user['username'];
        else
            echo 'false';
    }

    public function sendStep4Mail(){
        $user = JFactory::getUser();
        $lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
        $participants = JFactory::getApplication()->input->get('participants',null,'array');
        //var_dump($lastRecord['shipping']);
        if ($lastRecord['consulate']=='No')
        {
            include('./components/com_touristinvite/views/mails/payed/no.php');
        }
        elseif ($lastRecord['consulate']=='Genova')
        {
            include('./components/com_touristinvite/views/mails/payed/genovo.php');
        }
        elseif ($lastRecord['consulate']=='Milano')
        {
            include('./components/com_touristinvite/views/mails/payed/milano.php');
        }
        elseif ($lastRecord['consulate']=='Palermo')
        {
            include('./components/com_touristinvite/views/mails/payed/palermo.php');
        }
        elseif ($lastRecord['consulate']=='Roma')
        {
            include('./components/com_touristinvite/views/mails/payed/roma.php');
        }
        //////send mail begin
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $mailer->setSender($sender);
        $recipient = $user->email;

        $mailer->addRecipient($recipient);
        $mailer->addRecipient('mail@russiantour.com');
        //include('./components/com_touristinvite/views/mails/'.$file.'.php');
        $mailer->isHTML(true);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ( $send !== true ) {
            echo 'Error sending email: ' . $send->__toString();
        } else {
            //echo 'Mail sent';
        }
        //////send mail end
    }

    public function sendStep4MailManual($manualPayment){
        include('./components/com_touristinvite/views/mails/payed/manualUser.php');
        //////send mail begin
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $mailer->setSender($sender);
        $recipient = $manualPayment['email'];

        $mailer->addRecipient($recipient);
        $mailer->addRecipient('mail@russiantour.com');

        $mailer->isHTML(true);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ( $send !== true ) {
            echo 'Error sending email: ' . $send->__toString();
        } else {
            //echo 'Mail sent';
        }
        //////send mail end

        include('./components/com_touristinvite/views/mails/payed/manualAdmin.php');
        //////send mail begin
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $mailer->setSender($sender);

        $mailer->addRecipient('mail@russiantour.com');

        $mailer->isHTML(true);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ( $send !== true ) {
            echo 'Error sending email: ' . $send->__toString();
        } else {
            //echo 'Mail sent';
        }
        //////send mail end
    }

    public function sendStep2Mail($lastRecord,$participants){
        $user = JFactory::getUser();
        if (!isset($lastRecord['time_added']))
            $timeAdded = time();
        else
            $timeAdded = strtotime($lastRecord['time_added']);
        $daysNum = (strtotime($lastRecord['date_from'])-$timeAdded)/(3600*24);
        $daysLong = (strtotime($lastRecord['date_to'])-strtotime($lastRecord['date_from']))/(3600*24);
        $daysLong += 1;

        if (intval($_POST['showStatus'])!=1 && $lastRecord['consulate']=='No')
        {
            include('./components/com_touristinvite/views/mails/no.php');
        }
        elseif (
            intval($_POST['showStatus'])!=1 &&
            $lastRecord['status']==0 &&
            (
                $daysNum < 10 ||
                ($daysNum >=10 && $daysNum < 20 && !$lastRecord['urgent']) ||
                ($lastRecord['consulate']=='Milano' && $daysLong > 14)
            )
        )
        {
            include('./components/com_touristinvite/views/mails/create_yes.php');
        }
        else
        {
            if (intval($_POST['showStatus'])!=1)
            {
                $query = 'UPDATE #__touristinvite_visaandinivte SET status = 1 WHERE status = 0 and id = '.intval($lastRecord['id']);
                $db = JFactory::getDBO();
                $db->setQuery($query);
                $db->execute();
            }
            include('./components/com_touristinvite/views/mails/create_no.php');
        }

        //////send mail begin
        $mailer = JFactory::getMailer();
        $config = JFactory::getConfig();
        $sender = array(
            $config->get( 'mailfrom' ),
            $config->get( 'fromname' )
        );

        $mailer->setSender($sender);
        $recipient = $user->email;

        $mailer->addRecipient($recipient);
        $mailer->addRecipient('mail@russiantour.com');
        //include('./components/com_touristinvite/views/mails/'.$file.'.php');
        $mailer->isHTML(true);
        $mailer->setSubject($subject);
        $mailer->setBody($body);
        $send = $mailer->Send();
        if ( $send !== true ) {
            echo 'Error sending email: ' . $send->__toString();
        } else {
            //echo 'Mail sent';
        }
        //////send mail end
    }

    public function updateOwner($userId,$sendmail=false){
        if ($sendmail && intval($_GET['id'])){
            $query = 'UPDATE #__touristinvite_visaandinivte SET showStatus = 1'.
                ' WHERE showStatus = 0 and id = '.intval($_GET['id']).' and user_id = 0';
            $db = JFactory::getDBO();
            $db->setQuery($query);
            $db->execute();
        }
        if ($userId && intval($_POST['id'])/* && intval($_POST['showStatus'])*/){
            //$query = 'UPDATE #__touristinvite_visaandinivte SET showStatus = '.intval($_POST['showStatus']).', user_id = '.$userId.
            $query = 'UPDATE #__touristinvite_visaandinivte SET showStatus = 2, user_id = '.$userId.
                ' WHERE showStatus = 0 and id = '.intval($_POST['id']).' and user_id = 0';
            $db = JFactory::getDBO();
            $db->setQuery($query);
            $db->execute();

            $query = 'SELECT * FROM #__touristinvite_visaandinivte WHERE id = '.intval($_POST['id']). ' LIMIT 1' ;
            $db=JFactory::getDBO();
            $db->setQuery($query);
            $lastRecord = $db->loadAssoc();

            $db2 = JFactory::getDBO();
            $query = 'SELECT *  FROM #__touristinvite_participants WHERE  visa_invite_id = '.$lastRecord['id'];
            $db->setQuery($query);
            $participants = $db2->loadAssocList();

            $this->sendStep2Mail($lastRecord,$participants);
        }
    }
}
