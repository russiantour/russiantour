<?php

/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Touristinvite records.
 */
class TouristinviteModelInvite extends JModelList
{

    /**
     * Constructor.
     *
     * @param    array    An optional associative array of configuration settings.
     * @see        JController
     * @since    1.6
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                                'id', 'a.id',
                'ordering', 'a.ordering',
                'state', 'a.state',
                'created_by', 'a.created_by',
                'hotel_name', 'a.hotel_name',
                'hotel_city', 'a.hotel_city',

            );
        }
        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {


        // Initialise variables.
        $app = JFactory::getApplication();

        // List state information
        $limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit'));
        $this->setState('list.limit', $limit);

        $limitstart = $app->input->getInt('limitstart', 0);
        $this->setState('list.start', $limitstart);

        if ($list = $app->getUserStateFromRequest($this->context . '.list', 'list', array(), 'array')) {
            foreach ($list as $name => $value) {
                // Extra validations
                switch ($name) {
                    case 'fullordering':
                        $orderingParts = explode(' ', $value);

                        if (count($orderingParts) >= 2) {
                            // Latest part will be considered the direction
                            $fullDirection = end($orderingParts);

                            if (in_array(strtoupper($fullDirection), array('ASC', 'DESC', ''))) {
                                $this->setState('list.direction', $fullDirection);
                            }

                            unset($orderingParts[count($orderingParts) - 1]);

                            // The rest will be the ordering
                            $fullOrdering = implode(' ', $orderingParts);

                            if (in_array($fullOrdering, $this->filter_fields)) {
                                $this->setState('list.ordering', $fullOrdering);
                            }
                        } else {
                            $this->setState('list.ordering', $ordering);
                            $this->setState('list.direction', $direction);
                        }
                        break;

                    case 'ordering':
                        if (!in_array($value, $this->filter_fields)) {
                            $value = $ordering;
                        }
                        break;

                    case 'direction':
                        if (!in_array(strtoupper($value), array('ASC', 'DESC', ''))) {
                            $value = $direction;
                        }
                        break;

                    case 'limit':
                        $limit = $value;
                        break;

                    // Just to keep the default case
                    default:
                        $value = $value;
                        break;
                }

                $this->setState('list.' . $name, $value);
            }
        }

        // Receive & set filters
        if ($filters = $app->getUserStateFromRequest($this->context . '.filter', 'filter', array(), 'array')) {
            foreach ($filters as $name => $value) {
                $this->setState('filter.' . $name, $value);
            }
        }

        $this->setState('list.ordering', $app->input->get('filter_order'));
        $this->setState('list.direction', $app->input->get('filter_order_Dir'));
    }

    /**
     * Build an SQL query to load the list data.
     *
     * @return    JDatabaseQuery
     * @since    1.6
     */
    protected function getListQuery()
    {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query
            ->select(
                $this->getState(
                    'list.select', 'DISTINCT a.*'
                )
            );

        $query->from('`#__touristinvite_hotels` AS a');

        
    // Join over the users for the checked out user.
    $query->select('uc.name AS editor');
    $query->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');
    
		// Join over the created by field 'created_by'
		$query->join('LEFT', '#__users AS created_by ON created_by.id = a.created_by');

        // Filter by search in title
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (stripos($search, 'id:') === 0) {
                $query->where('a.id = ' . (int)substr($search, 3));
            } else {
                $search = $db->Quote('%' . $db->escape($search, true) . '%');
                
            }
        }

        

        // Add the list ordering clause.
        $orderCol = $this->state->get('list.ordering');
        $orderDirn = $this->state->get('list.direction');
        if ($orderCol && $orderDirn) {
            $query->order($db->escape($orderCol . ' ' . $orderDirn));
        }

        return $query;
    }

    public function getItems()
    {
        $items = parent::getItems();

        return $items;
    }

    public function getTable($type = 'invite', $prefix = 'TouristinviteTable', $config = array())
    {
        $this->addTablePath(JPATH_COMPONENT_ADMINISTRATOR.'/tables');
        return JTable::getInstance($type, $prefix, $config);
    }
    function save($data){

        $user = JFactory::getUser()->get('id');
        if ($user)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('turfirm_balance, turfirm_cost')
                ->from('#__touristinvite_turfirms')
                ->where('turfirm_user_id = ' . $user);
            $db->setQuery($query);
            $db = $db->loadObject();

            if (!$db)
            {
                echo 'Registration not confirmed. Please contact us to solve this problem.';
            }
            else
            {
                $data['cost'] = $db->turfirm_cost;ini_set('display_errors',1);

                $table = $this->getTable();
                $table->set('invite_from', date('Y-m-d H:i:s',strtotime(str_replace('/','.',$data['invite_from']))));
                $table->set('invite_to', date('Y-m-d H:i:s',strtotime(str_replace('/','.',$data['invite_to']))));
                $table->set('invite_created', date('Y-m-d H:i:s'));
                $table->set('invite_data', serialize($data));
                $table->set('created_by', JFactory::getUser()->get('id'));
                if (isset($data['inviteid']))
                    $table->set('id', $data['inviteid']);
                $table->store();

                if (isset($data['inviteid']))
                    echo "Invite successfully changed";
                else
                    echo "Invite successfully added";

                echo "<script>window.location = window.location.href.replace('save','')</script>";
            }
        }
        else
            echo "Y3ou must login first";
    }
    function validateinvite($inviteid=0) {
        $user = JFactory::getUser()->get('id');
        if ($user)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('turfirm_balance, turfirm_cost')
                ->from('#__touristinvite_turfirms')
                ->where('turfirm_user_id = ' . $user);
            $db->setQuery($query);
            $db = $db->loadObject();

            if (!$db)
            {
                echo 'Registration not confirmed. Please contact us to solve this problem.';
            }
            else
            {
                if ($inviteid)
                {
                    $db2 = JFactory::getDbo();
                    $query = $db2->getQuery(true);

                    $query
                        ->select('pdf_id')
                        ->from('#__touristinvite_invites')
                        ->order('pdf_id DESC');
                    $db2->setQuery($query);
                    $db2 = $db2->loadObject();
                    if (!$db2->pdf_id)
                        $pdf_id = 2014;
                    else
                        $pdf_id = $db2->pdf_id+1;

                    $db2 = JFactory::getDbo();
                    $query = $db2->getQuery(true);
                    $query
                        ->select('invite_data')
                        ->from('#__touristinvite_invites')
                        ->where(array(
                            'id = ' . intval($inviteid),
                            'created_by = '.$user
                        ));
                    $db2->setQuery($query);
                    $db2 = $db2->loadObject();

                    $data = unserialize($db2->invite_data);
                    if ($db->turfirm_balance >= $db->turfirm_cost*$data['people_count'])
                    {
                        $newbalance = $db->turfirm_balance-$db->turfirm_cost*$data['people_count'];
                        $data['old_balance'] = $db->turfirm_balance;
                        $data['new_balance'] = $newbalance;
                        $data['validate_date'] = date('m/d/Y');
                        $data['cost'] = $db->turfirm_cost;

                        $table = $this->getTable();
                        $table->set('id', intval($inviteid));
                        $table->set('invite_data', serialize($data));
                        $table->set('pdf_id',$pdf_id);
                        $table->store();

                        $db = JFactory::getDbo();

                        $query = 'UPDATE #__touristinvite_turfirms'
                            . ' SET turfirm_balance = \''.$newbalance.'\''
                            . ' WHERE turfirm_user_id = ' . (int) $user;

                        $db->setQuery($query);
                        $db->execute();

                        echo "Invite successfully validated";
                    }
                    else
                        echo 'Not enough money';
                }
                else
                    echo 'Give me ID';

                echo "<script>window.location = window.location.href.replace(/validateinvite.*/,'')</script>";
            }
        }
        else
            echo "Y4ou must login first";
    }
    function showpdf($inviteid=0) {
        $user = JFactory::getUser()->get('id');
        if ($user)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('turfirm_balance, turfirm_cost')
                ->from('#__touristinvite_turfirms')
                ->where('turfirm_user_id = ' . $user);
            $db->setQuery($query);
            $db = $db->loadObject();

            if (!$db)
            {
                echo 'Registration not confirmed. Please contact us to solve this problem.';
            }
            else
            {
                $table = JFactory::getDbo();
                $query = $table->getQuery(true);
                $query
                    ->select('*')
                    ->from('#__touristinvite_invites')
                    ->where(array(
                        'id = ' . intval($inviteid),
                        'created_by = '.$user
                    ));
                $table->setQuery($query);
                $table = $table->loadObject();

                $data = unserialize($table->invite_data);

                $doc = JFactory::getDocument();
                $doc->addScript(JUri::base() . '/components/com_touristinvite/assets/js/jspdf.js');
                $doc->addScript(JUri::base() . '/components/com_touristinvite/assets/js/libs/base64.js');
                $doc->addScript(JUri::base() . '/components/com_touristinvite/assets/js/libs/sprintf.js');

                include(JPATH_COMPONENT_SITE.'/assets/php/pdf/create.php');
                exit();
            }
        }
        else
            echo "Y5ou must login first";
    }
    function delinvite($inviteid=0) {
        $user = JFactory::getUser()->get('id');
        if ($user)
        {
            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('turfirm_balance, turfirm_cost')
                ->from('#__touristinvite_turfirms')
                ->where('turfirm_user_id = ' . $user);
            $db->setQuery($query);
            $db = $db->loadObject();

            if (!$db)
            {
                echo 'Registration not confirmed. Please contact us to solve this problem.';
            }
            else
            {
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query
                    ->delete('#__touristinvite_invites')
                    ->where(array(
                        'id = ' . intval($inviteid),
                        'created_by = '.$user
                    ));

                $db->setQuery($query);
                $db->execute();
                echo 'Deleted';
            }
            echo "<script>window.location = window.location.href.replace(/delinvite.*/,'')</script>";
        }
        else
            echo "Y6ou must login first";
    }
}