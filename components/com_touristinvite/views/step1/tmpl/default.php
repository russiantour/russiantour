<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;
for ($i=0;$i<=4;$i++)
    $cities_to_visit[$i] = '';
$form_filling = '';
$rus_visa = '';
$nextCityNumber = 2;
$shipping = 'Non includere';
$add_hotel = '';
$add_appartamento = '';
$add_volo_treno = '';
$add_transfers = '';
$add_escursioni = '';
$add_other = '';
if ($_GET['id']) {
    $lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array');
    $participants = JFactory::getApplication()->input->get('participants',null,'array');
    $lastRecord = $lastRecord[0];
    $dateFrom = explode('-',$lastRecord['date_from']);
    //var_dump($lastRecord,$dateFrom);
    $_POST['date_from'] = $dateFrom[2].'/'.$dateFrom[1].'/'.$dateFrom[0];
    $dateTo = explode('-',$lastRecord['date_to']);
    $_POST['date_to'] = $dateTo[2].'/'.$dateTo[1].'/'.$dateTo[0];
    $_POST['consulate'] = $lastRecord['consulate'];
    if (intval($lastRecord['insurance']))
        $_POST['insurance'] = $lastRecord['insurance'];
    if (intval($lastRecord['urgent']))
        $_POST['urgent'] = $lastRecord['urgent'];
    if (intval($lastRecord['form_filling']))
        $form_filling = 'checked';
    if (intval($lastRecord['rus_visa']))
        $rus_visa = 'checked';
    $cities_to_visit = explode(',',$lastRecord['cities_to_visit']);
    $nextCityNumber = count($cities_to_visit)+1;
    $shipping = $lastRecord['shipping'];
    if (intval($lastRecord['add_hotel']))
        $add_hotel = 'checked';
    if (intval($lastRecord['add_appartamento']))
        $add_appartamento = 'checked';
    if (intval($lastRecord['add_volo_treno']))
        $add_volo_treno = 'checked';
    if (intval($lastRecord['add_transfers']))
        $add_transfers = 'checked';
    if (intval($lastRecord['add_escursioni']))
        $add_escursioni = 'checked';
    $add_other = $lastRecord['add_other'];
}
//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_touristinvite', JPATH_ADMINISTRATOR);
$params = JComponentHelper::getParams('com_touristinvite')->get('params');
$nations = array("Italy","Spain","France","Germany","Czech Republic","Poland","Austria","Switzerland","Belgium","Bulgaria","Cyprus","Croatia","Denmark","Estonia","Finland","Greece","Hungary","Ireland","Latvia","Lithuania","Luxemburg","Malta","Norway","Portugal","Rumania","Slovakia","Slovenia","Sweden","the Netherlands");

$query = 'SELECT * FROM #__touristinvite_hotels'
    . ' GROUP BY hotel_city ORDER BY hotel_city';

$db = JFactory::getDbo();
$db->setQuery($query);
$items = $db->loadObjectList();

$cities=array('Moscow','Saint-Petersburg');

foreach ($items as $item)
{
    if ($item->hotel_city && !in_array($item->hotel_city,$cities))
    {
        $cities[]=$item->hotel_city;
    }
}

//$params = (isset($this->state->params)) ? $this->state->params : new JObject;
?>

 <script type="text/javascript">
   /* <![CDATA[ */
   goog_snippet_vars = function() {
     var w = window;
     w.google_conversion_id = 972026260;
     w.google_conversion_label = "d3mZCJyd9GUQlOO_zwM";
     w.google_conversion_value = 1.00;
     w.google_conversion_currency = "EUR";
     w.google_remarketing_only = false;
   }
   // DO NOT CHANGE THE CODE BELOW.
   goog_report_conversion = function(url) {
     goog_snippet_vars();
     window.google_conversion_format = "3";
     var opt = new Object();
     opt.onload_callback = function() {
     if (typeof(url) != 'undefined') {
       window.location = url;
     }
   }
   var conv_handler = window['google_trackConversion'];
   if (typeof(conv_handler) == 'function') {
     conv_handler(opt);
   }
}
/* ]]> */
</script>
<script type="text/javascript"
   src="//www.googleadservices.com/pagead/conversion_async.js">
</script>

<div id="system-message-container">
</div>
<form id="globalForm" style="margin: 0 0 9px;" method="post" action="/ita/?option=com_touristinvite&view=step2">
    <?php if ($_GET['id']) $id = $_GET['id']; else $id = 0; ?>
        <input type="hidden" value="<?php echo $id; ?>" name="id" id="id">
<style>
    .error {display: none;}
</style>



<div id="emailError" class="error">Attenzione: indirizzo mail non valido, verificare e correggere</div>
    <ul id="tab-content" class="tm-tab-content uk-switcher">
    <li class="uk-active">
	<?php $user = JFactory::getUser(); /*var_dump($user->get('name'));*/ ?>
<?php if ($user->get('id')) : ?>
    <ul class="uk-tab">
    <li class="uk-active"><a href="/ita/modulo-visto-turistico/?option=com_touristinvite&view=list">Pratiche visto</a></li>
    <li class=""><a href="https://siteheart.com/webconsultation/765464" target="_blank" onclick="popupWin = window.open(this.href, 'contacts', 'location,width=400,height=300,top=0'); popupWin.focus(); return false;">Assistenza</a></li>
    <li class=" "><a href="/ita/profilo">Modifica profilo</a></li>
    <li class=" "><a href="/ita/exit">Fine sessione</a></li>
</ul>
<?php endif; ?>
        <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Visto turistico russia</div>
        <br>
        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-success">1</span> <span class="uk-width-8-10 uk-badge uk-badge-success"><i class="uk-icon-spinner uk-icon-spin"></i> Compilazione dati</span> </div>
            <div class="uk-width-medium-1-4"><span   class="uk-badge uk-badge-notification ">2</span> <span  class=" uk-width-8-10 uk-badge"> Riepilogo ed istruzioni</span>    </div>
            <div class="uk-width-medium-1-4"><span   class="uk-badge uk-badge-notification ">3</span> <span  class=" uk-width-8-10 uk-badge">Pagamento</span>    </div>
            <div class="uk-width-medium-1-4" ><span class="uk-badge uk-badge-notification">4</span> <span  class="uk-width-8-10 uk-badge"> Invio documenti</span></div>
        </div>
        <br>
	    <div id="modal12" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
            <div class="uk-modal-dialog uk-text-justify">
                <a href="" class="uk-modal-close uk-close"></a>
                <h2>Selezionare la propria nazionalita`.</h2>
                <p> Nella lista sono presenti soltanto i paesi UE in quanto per i cittadini di questi paesi i prezzi ed i documenti da presentare sono gli stessi. Per i cittadini di Romania e Bulgaria e` necessario presentare anche il certificato di residenza in Italia o la carta d'identita` italiana se si ha la doppia cittadinanza. Per i cittadini extra UE possono variare il prezzo delle tasse consolari, i tempi di consegna e la documentazione da presentare; per un preventivo e` possibile inviare la richiesta via mail a visto@russiantour.com   </p>
            </div>
        </div>
								
        <div class="uk-grid">
            <div class="uk-width-medium-7-10">
            <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Date del viaggio </div>
            <div class="uk-grid">
                <div class="uk-width-1-1">
                    <h4 class="tm-article-subtitle">Selezionate la data di arrivo e di partenza  <a data-uk-modal="{target:'#modal1'}" href=" "><i class="uk-icon-question-circle"> </i> </a> </h4>
                    <div id="dataError" class="error uk-alert uk-alert-danger"><b>Attenzione:</b> indicare la data di arrivo e di partenza dalla Russia</div>
                    <div id="modal1" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                        <div class="uk-modal-dialog uk-text-justify">
                            <a href="" class="uk-modal-close uk-close"></a>
                            <h2>Selezionate la data di arrivo e di partenza</h2>
                            <p>Indicare le date del viaggio in Russia, considerando un periodo minimo di 3 giorni, ed un periodo massimo di 30 giorni, compreso il giorno di arrivo ed il giorno di partenza.
    Indicare 3 giorni anche se e` previsto un soggiorno piu` breve, l’importante e` che il viaggio sia effettuato entro i margini delle date del visto (arrivo non precedente alla data indicata, partenza non successiva alla data indicata).</p>
                        </div>
                    </div>
                    <br>
                </div>
                <div id="modal15" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog">
                        <a href="" class="uk-modal-close uk-close"></a>
                        <h2>Selezionate la data</h2>
                        <p>Selezionare la data sul calendario oppure inserirla nel formato gg/mm/aaaa</p>
                    </div>
                </div>
                <div class="uk-width-medium-1-2">
                    Data di arrivo in Russia
                    <a data-uk-modal="{target:'#modal15'}" href=" "><i class="uk-icon-question-circle"> </i> </a>
                    <div class="uk-form-controls">
                        <div class="uk-form-icon uk-width-1-1">
                            <i class="uk-icon-calendar"></i>
                            <input name="date_from" class="uk-width-1-1 uk-form-large" type="text" id="tm-form-check-in" value="<?php echo $_POST['date_from']?$_POST['date_from']:''; ?>">
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-1-2">Data di partenza dalla Russia <a data-uk-modal="{target:'#modal15'}" href=" "><i class="uk-icon-question-circle"> </i> </a>
                    <div class="uk-form-controls">
                        <div class="uk-form-icon uk-width-1-1">
                            <i class="uk-icon-calendar"></i>
                            <input name="date_to" class="uk-width-1-1 uk-form-large" type="text" id="tm-form-check-out" value="<?php echo $_POST['date_to']?$_POST['date_to']:''; ?>">
                        </div>
                    </div>
                </div>
            </div><br>
            <fieldset data-uk-margin="" style="display: none;" id="hiddenUser">
                <hr>
                 <div class="uk-grid">
                            <div class="uk-width-1-1">
				<input name="participants[id][second_name]" type="text" placeholder="Cognome" class="uk-margin-small-top uk-width-medium-1-4">
                <input name="participants[id][first_name]" type="text" placeholder="Nome" class="uk-margin-small-top uk-width-medium-1-4">
                <select name="participants[id][gender]" class="uk-margin-small-top uk-width-medium-2-10" autocomplete="off">
                    <option value="m">Maschile</option>
                    <option value="f">Femminile</option>
                </select>
                <input name="participants[id][birthdate]" style=" margin-top: 5px; "  class="uk-width-medium-2-10 participants-birthdate" type="text" placeholder="Data di nascita" >
                </div></div><div  style=" margin-top: 5px; " class="uk-grid">
                            <div class="uk-width-1-1">
                    <select name="participants[id][nationality]" class="uk-margin-small-top uk-width-medium-3-10  " autocomplete="off">
                        <option value="">--- Nazionalita ---</option>
                        <?php foreach ($nations as $nation) : ?>
                            <option value="<?php echo $nation; ?>"><?php echo $nation; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <input name="participants[id][passport]" type="text" placeholder="Numero di passaporto" class="uk-margin-small-top uk-width-medium-3-10">
                    <button class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser"><i class="uk-icon-remove"></i></button></div></div>
                </fieldset>
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Partecipanti al viaggio </div>
                <div class="uk-width-1-1">
                    <h4 class="tm-article-subtitle">Indicare i dati dei partecipanti al viaggio  <a data-uk-modal="{target:'#modal2'}" href=" "><i class="uk-icon-question-circle"> </i> </a>  </h4><div id="viaggioError"  class="error uk-alert uk-alert-danger"><b>Attenzione:</b> compilare tutti i dati dei partecipanti al viaggio</div>
                    <div id="modal2" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                        <div class="uk-modal-dialog uk-text-justify">
                            <a href="" class="uk-modal-close uk-close"></a>
                            <h2>Indicare i dati dei partecipanti al viaggio</h2>
                            <p> Indicare negli appositi spazi: nome, cognome, data di nascita, sesso e nazionalita` di ogni partecipante al viaggio. Compilare ogni campo anche per i minori: per ottenere un visto russo anche i bambini debbono possedere un proprio passaporto. Per i minori non accompagnati da entrambe i genitori, e` richiesta la seguente documentazione aggiuntiva: certificato di nascita con i dati dei genitori, certificato di stato di famiglia, copia del documento d'identita` del genitore o dei genitori che non accompagnano il minore, dichiarazione di assenso con firma autenticata dove si autorizza il viaggio del minore in sua/loro assenza.</p>
                        </div>
                    </div>
                </div>
                <div class="uk-form" id="persons">
                    <?php if (count($participants)) : ?>
                        <?php foreach($participants as $key=>$participant) :
                            $birthdate = $participant['birthdate'];
                            $birthdate = explode('-',$birthdate);
                            $birthdate = $birthdate[2].'/'.$birthdate[1].'/'.$birthdate[0];
                    ?>
                    <fieldset data-uk-margin="">
                         <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <input name="participants[<?php echo $key;?>][second_name]" value="<?php echo $participant['second_name']; ?>" type="text" placeholder="Cognome" class="uk-margin-small-top uk-width-medium-2-10">
                                <input name="participants[<?php echo $key;?>][first_name]" value="<?php echo $participant['first_name']; ?>" type="text" placeholder="Nome" class="uk-margin-small-top uk-width-medium-2-10">
                                <select name="participants[<?php echo $key;?>][gender]" class="uk-margin-small-top uk-width-medium-2-10" autocomplete="off">
                                    <option value="m" <?php if ($participant['gender']=='m') echo 'selected'; ?>>Maschile</option>
                                    <option value="f" <?php if ($participant['gender']=='f') echo 'selected'; ?>>Femminile</option>
                                </select>
                                <input name="participants[<?php echo $key;?>][birthdate]" value="<?php echo $birthdate; ?>" style=" margin-top: 5px; "  class="uk-width-medium-2-10 participants-birthdate" type="text" placeholder="Data di nascita" >
                            </div>
                         </div>
                         <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <select class="uk-margin-small-top uk-width-2-10" name="participants[<?php echo $key;?>][nationality]" autocomplete="off">
                                    <option value="">- Nazionalita -</option>
                                    <?php foreach ($nations as $nation) : ?>
                                    <option value="<?php echo $nation; ?>" <?php if ($nation==$participant['nationality']) echo 'selected'; ?>><?php echo $nation; ?></option>
                                    <?php endforeach; ?>
					            </select>&nbsp; &nbsp;
                                <a data-uk-modal="{target:'#modal12'}" href=" "> <i class="uk-icon-question-circle"> </i> </a>
                                &nbsp;&nbsp;
                                <input name="participants[<?php echo $key;?>][passport]" value="<?php echo $participant['passport']; ?>" type="text" placeholder="Numero di passaporto" class="uk-margin-small-top">
                                <?php if ($key==0) : ?>
                                <input name="participants[<?php echo $key;?>][citta_di_residenza]" value="<?php echo $participant['citta_di_residenza']; ?>" type="text" class="uk-margin-small-top" placeholder="Città di residenza">
                                <?php endif; ?>
                            </div>
                         </div>
                    </fieldset>
                        <?php endforeach; ?>
                        <?php else : ?>
                        <fieldset data-uk-margin="">
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <input name="participants[0][second_name]" type="text" placeholder="Cognome" class="uk-margin-small-top uk-width-medium-1-4">
                                    <input name="participants[0][first_name]" type="text" placeholder="Nome" class="uk-margin-small-top uk-width-medium-1-4">
                                    <select name="participants[0][gender]" class="uk-margin-small-top uk-width-medium-2-10" autocomplete="off">
                                        <option value="m">Maschile</option>
                                        <option value="f">Femminile</option>
                                    </select>
                                    <input name="participants[0][birthdate]" style=" margin-top: 5px; "  class="uk-width-medium-2-10 participants-birthdate" type="text" placeholder="Data di nascita" >
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-1-1">
                                    <select class="uk-margin-small-top uk-width-medium-3-10" name="participants[0][nationality]" autocomplete="off">
                                        <option value="">- Nazionalita -</option>
                                        <?php foreach ($nations as $nation) : ?>
                                            <option value="<?php echo $nation; ?>"><?php echo $nation; ?></option>
                                        <?php endforeach; ?>
                                    </select>&nbsp; &nbsp;
                                    <a data-uk-modal="{target:'#modal12'}" href=" "> <i class="uk-icon-question-circle"> </i> </a>
                                    &nbsp;&nbsp;
                                    <input name="participants[0][passport]" type="text" placeholder="Numero di passaporto" class="uk-margin-small-top uk-width-medium-3-10">
                                    <input name="participants[0][citta_di_residenza]" type="text" class="uk-margin-small-top uk-width-medium-1-4" placeholder="Città di residenza">
                                </div>
                            </div>
                        </fieldset>
                    <?php endif; ?>
                </div>
                <button class="uk-button uk-margin-small-top" id="addUser">Aggiungere un altra persona </button>
            </div>
            <div class="uk-width-medium-3-10">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Costo della pratica</div>
                <div style="line-height: 62px; font-size: 50px;"  class="uk-text-center  ">
                    <span id="totalSumm1">0</span> €
                    <input type="hidden" name="totalSumm" id="totalSumm">
                    <div style="font-size: 14px; text-align: left;line-height: 1;">
                         <div class="total2" id="totalSummiPersons" style="display: none;line-height: 1;"><span>Numero di persone: </span> <span></span></div>
						<div class="total1" id="totalSummMsc" style="display: none;line-height: 1;"><span>Registrazione del visto a Mosca</span></div>
                        <div class="total1" id="totalSummSpb" style="display: none;line-height: 1;"><span>Registrazione del visto a San Pietroburgo</span></div>
                        <div class="total1" id="totalSummiFast" style="display: none;line-height: 1;"><span>Procedura urgente e tasse consolari urgenti</span></div>
                        <div class="total1" id="totalSummA" style="display: none;line-height: 1;"><span>Lettera d’invito e voucher per il visto russo</span></div>
                        <div class="total1" id="totalSummB" style="display: none;line-height: 1;"><span>Assicurazione medico sanitaria</span></div>
                        <div class="total1" id="totalSummC" style="display: none;line-height: 1;"><span>registrazione</span></div>
                        <div class="total1" id="totalSummD" style="display: none;line-height: 1;"><span>consolare</span></div>
                        <div class="total1" id="totalSummE" style="display: none;line-height: 1;"><span>mServicePrice</span></div>
                                                <div class="total1" id="totalSummSpeseSpedizione" style="line-height: 1;"><span></span> </div>
                        <div id="totalSummG" style="display: none;line-height: 1;"><span>Compilazione modulo consolare</span></div>
                       
                    </div>

				<div>
 <!--
				   <input  name="promocode" class="uk-width-medium-2-10 uk-form " type="text" placeholder="купон" >
-->
	</div>
 			</div>
            </div>
        </div>
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Procedura completa visto </div>
                <h4 class="tm-article-subtitle">Indicate il consolato russo presso il quale volete affidarci la pratica, oppure indicate "No" se vi recherete autonomamente presso l'ufficio visti  <a data-uk-modal="{target:'#modal3'}" href=" "><i class="uk-icon-question-circle"> </i> </a></h4>
                <br>
                <div id="modal3" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog uk-text-justify">
                        <a href="" class="uk-modal-close uk-close"></a>
                        <h2>Indicate il consolato russo presso il quale volete affidarci la pratica, oppure indicate "No" se vi recherete autonomamente presso l'ufficio visti </h2>
                        <p> Se volete recarvi da soli presso l’ufficio visti , vi possiamo fornire la sola lettera d'invito, accompagnata o meno dell'assicurazione medico sanitaria (e`accettata in copia presso i consolati di Genova e Palermo, a Milano e Roma dai privati esigono l'originale). Se non avete la possibilità o la voglia di recarvi da soli in  Consolato, potete affidare tutta la pratica a Russian Tour, che si rechera` per vostro conto presso l’ufficio visti del Consolato indicato (Roma, Milano, Genova o Palermo).Presso il consolato di Milano, per visti superiori ai 14 giorni, e` generalmente richiesta la seguente documentazione aggiuntiva: prenotazioni alberghiere, fatture da parte degli hotel o dei sistemi di prenotazione, ricevute di pagamento dei servizi al 100%. Se non avete prenotato con noi l'intero viaggio, o se non disponete autonomamente di tutta questa documentazione, consigliamo di non consegnare autonomamente i documenti all'ufficio visti di Milano e di non selezionare Milano per la procedura completa.</p>
                    </div>
                </div>
                <?php $consulate = $_POST['consulate']?$_POST['consulate']:'No'; ?>
                <div data-uk-button-radio id="procedura_completa">
                    <button class="uk-button uk-button-primary <?php if ($consulate=='No') echo 'uk-active'; ?>" addcitta="cittano" cityid="0" onclick="return false;">No</button>
                    <button class="uk-button uk-button-primary <?php if ($consulate=='Roma') echo 'uk-active'; ?>" cityid="3"  addcitta="Presentazione domanda di visto presso il Consolato di Roma e tasse consolari " onclick="return false;">Roma</button>
					
                    <button class="uk-button uk-button-primary <?php if ($consulate=='Milano') echo 'uk-active'; ?>" cityid="1" addcitta="Presentazione domanda di visto presso il Consolato di Milano e tasse consolari" onclick="return false;">Milano</button>
                    <button class="uk-button uk-button-primary <?php if ($consulate=='Genova') echo 'uk-active'; ?>" cityid="6" addcitta="Presentazione domanda di visto presso il Consolato di Genova e tasse consolari" onclick="return false;">Genova</button>
                    <button class="uk-button uk-button-primary <?php if ($consulate=='Palermo') echo 'uk-active'; ?>" cityid="2" addcitta="Presentazione domanda di visto presso il Consolato di Palermo e tasse consolari" onclick="return false;">Palermo</button>
                    <input type="hidden" name="consulate" value="<?php echo $consulate; ?>" id="consulate">
                </div><br>
                <label>
                    <input name="insurance" type="checkbox" data-uk-tooltip id="ho_medica" <?php if (isset($_POST['insurance'])) echo 'checked'; ?>/>  Ho bisogno dell'assicurazione medica  <a data-uk-modal="{target:'#modal4'}" href=" "><i class="uk-icon-question-circle"> </i> </a></label>
                <div id="modal4" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog uk-text-justify">
                        <a href="" class="uk-modal-close uk-close"></a>
                        <h2>Ho bisogno dell'assicurazione medica</h2>
                        <p> Per poter richiedere e ottenere un visto turistico per la Russia, è obbligatorio stipulare un’assicurazione medico sanitaria, con un massimale di almeno 30.000 euro, che copra eventuali spese mediche per tutta la durata del soggiorno in Russia, con una compagnia assicurativa accettata dai consolati russi. Se effettuate con Russian Tour la procedura completa visto, la nostra assicurazione medica e` obbligatoriamente e automaticamente inclusa nel prezzo. Se avete invece intenzione di recarvi autonomamente presso l’ufficio visti con la nostra lettera d’invito, potete scegliere se acquistare con noi l’assicurazione o provvedere autonomamente. Attenzione: a Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare autonomamente la nostra assicurazione presso gliuffici consolari di queste due citta`. A Genova e Palermo vengono accettate anche le copie, quindi potrete stampare e presentare la polizza che riceverete da noi via mail il giorno lavorativo successivo al pagamento. Le tariffe dell'assicurazione sono raddoppiate per gli adulti sopra i 65 anni di eta` e per i bambini sotto i 3 anni di eta`.</p>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-2">
                <h4 class="tm-article-subtitle">Servizi aggiuntivi </h4>
                    <div  >
                        <label>
                            <input data-uk-tooltip type="checkbox" id="ho_urgente" name="urgent"  <?php if (isset($_POST['urgent'])) echo 'checked'; ?>>
                            Ho bisogno della procedura urgente
                            <a data-uk-modal="{target:'#modal5'}" href=" ">
                                <i class="uk-icon-question-circle"> </i>
                            </a>
                        </label>
                        <div id="modal5" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                            <div class="uk-modal-dialog uk-text-justify">
                                <a href="" class="uk-modal-close uk-close"></a>
                                <h2>Ho bisogno della procedura urgente</h2>
                                <p>Con la procedura ordinaria il visto sarà pronto in circa 15 giorni dal momento della ricezione della documentazione da parte dei nostri corrispondenti. La documentazione sara' presentata in consolato il giorno successivo alla ricezione, ed il visto sara' pronto in 10 giorni lavorativi. <br>
Con la procedura urgente il visto sara` pronto in 4 giorni lavorativi, e l'importo delle tasse consolari + tasse centro visti ammonta a 100 euro. Per la procedura urgente e' previsto un supplemento di 5 euro per la presentazione domanda di visto a Genova e Palemo, e di 15 euro a Roma e Milano.
In determinati casi, con extra-supplemento 21 euro, e` possibile procedere con la procedura super-urgente, con tempi di rilascio in un due giorni lavorativi.</p>
                            </div>
                        </div>
                    </div>
                    <label><input type="checkbox" id="compila" name="form_filling" <?php echo $form_filling; ?>>  Compilazione del modulo consolare per mio conto   <a data-uk-modal="{target:'#modal6'}" href=" "><i class="uk-icon-question-circle"> </i> </a> </label>
                    <div id="modal6" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                        <div class="uk-modal-dialog uk-text-justify">
                            <a href="" class="uk-modal-close uk-close"></a>
                            <h2>Compilazione del modulo consolare per mio conto</h2>
                            <p>Tra la documentazione da presentare all’ufficio visti per l’ottenimento del visto russo c’e` anche un modulo da compilare online sul sito del Consolato: https://visa.kdmid.ru<br>
                                Possiamo compilare per vostro conto questo modulo, il prezzo del servizio e` 12 euro a persona.    </p>
                        </div>
                    </div>
					                    <div id="modal7" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                        <div class="uk-modal-dialog uk-text-justify">
                            <a href="" class="uk-modal-close uk-close"></a>
                            <h2>Registrazione del visto russo </h2>
                            <p>Secondo le vigenti normative russe, ogni cittadino straniero che accede in territorio russo, e` obbligato a registrare il proprio visto se risiede in Russia per un periodo superiore a 7 giorni lavorativi. Se risiederete presso un hotel, questo provvederà direttamente alla registrazione del visto, dietro pagamento di una tassa locale.<br>
Se non risiederete presso un hotel, allora la registrazione andra` effettuata presso l'UFMS. Le modalità per la registrazione presso un alloggio privato sono gratuite, ma abbastanza complicate e soggette a variazioni. Per facilitarvi la pratica possiamo offrire questo servizio al prezzo di 35 Euro a San Pietroburgo e 45 euro a Mosca. Qualora intendiate avvalervi del servizio registrazione, indicate la citta` nella quale risiederete inizialmente, se il soggiorno sara` almeno di 3 giorni lavorativi. Se inferiore, indicate la citta` dove risiederete piu` a lungo, tenendo in considerazione che forniamo il servizio solo a Mosca e San Pietroburgo.</p>
                        </div>
                    </div>
                <label> Registrazione del visto russo
                    <a data-uk-modal="{target:'#modal7'}" href=" "><i class="uk-icon-question-circle"> </i> </a>
                </label>
                <select class="uk-width-medium-6-10" name="ho_registrazione">
                    <option value="0">Non ho bisogno della registrazione</option>
                    <option id="ho_registrazione_2" value="piter" <?php if ($_POST['ho_registrazione']=='piter') echo 'selected'; ?>>San Pietroburgo</option>

                    <option id="ho_registrazione_1" value="moscow" <?php if ($_POST['ho_registrazione']=='moscow') echo 'selected'; ?>>Mosca</option>
                </select>
                </fieldset>
                </div>
                <div class="uk-width-medium-1-2">
                    <div class="uk-button-dropdown" data-uk-dropdown="" aria-haspopup="true" aria-expanded="false">
                        <h4 class="tm-article-subtitle">Città da visitare</h4>
                        <h4 class=" uk-article-lead  margin0">Indicate le città che visiterete durante il vostro viaggio in Russia</h4>
                        <div class="uk-text-small">Se la citta` dove vi recherete non e` presente nella nostra lista, indicatela nei commenti</div>
                        <div id="cittaError" class="error uk-alert uk-alert-danger "><b>Attenzione:</b> selezionare almeno una citta` da visitare</div>
                        <br>
                        <fieldset class="margin0" data-uk-margin id="city1">
                            <select name="cities_to_visit[]" autocomplete="off">
                                <option value="">--- Città 1 ---</option>
                                <?php foreach ($cities as $city) : ?>
                                <option <?php if ($cities_to_visit[0]==$city) echo 'selected'; ?> value="<?php echo $city; ?>">
                                    <?php echo $city; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </fieldset>
                        <fieldset class="margin0"  data-uk-margin id="city2" <?php if ($cities_to_visit[1]=='') : ?>style="display: none;"<?php endif; ?>  >
                           <select name="cities_to_visit[]" autocomplete="off">
                               <option value="">--- Città 2 ---</option>
                               <?php foreach ($cities as $city) : ?>
                               <option value="<?php echo $city; ?>" <?php if ($cities_to_visit[1]==$city) echo 'selected'; ?>>
                                   <?php echo $city; ?>
                               </option>
                               <?php endforeach; ?>
                           </select>
                        </fieldset>
                        <fieldset   class="margin0"  data-uk-margin id="city3" <?php if ($cities_to_visit[2]=='') : ?>style="display: none;"<?php endif; ?>>
                            <select name="cities_to_visit[]" autocomplete="off">
                                <option value="">--- Città 3 ---</option>
                                <?php foreach ($cities as $city) : ?>
                                <option value="<?php echo $city; ?>" <?php if ($cities_to_visit[2]==$city) echo 'selected'; ?>>
                                    <?php echo $city; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </fieldset>
                        <fieldset  class="margin0"  data-uk-margin  id="city4" <?php if ($cities_to_visit[3]=='') : ?>style="display: none;"<?php endif; ?>  >
                            <select name="cities_to_visit[]" autocomplete="off">
                                <option value="">--- Città 4 ---</option>
                                <?php foreach ($cities as $city) : ?>
                                <option value="<?php echo $city; ?>" <?php if ($cities_to_visit[3]==$city) echo 'selected'; ?>>
                                    <?php echo $city; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </fieldset>
                        <fieldset   class="margin0"  data-uk-margin  id="city5" <?php if ($cities_to_visit[4]=='') : ?>style="display: none;"<?php endif; ?> >
                            <select name="cities_to_visit[]" autocomplete="off">
                                <option value="">--- Città 5 ---</option>
                                <?php foreach ($cities as $city) : ?>
                                <option value="<?php echo $city; ?>" <?php if ($cities_to_visit[4]==$city) echo 'selected'; ?>>
                                    <?php echo $city; ?>
                                </option>
                                <?php endforeach; ?>
                            </select>
                        </fieldset>
                        <button class="uk-button uk-margin-small-top" id="cityAdd" <?php if ($nextCityNumber > 5) : ?>style="display: none;"<?php endif; ?>>
                            Aggiungere citta`
                            <span id="cityCount"><?php echo $nextCityNumber; ?></span> di 5
                        </button>
                        <br>
                    </div>
                </div>
            </div>
            <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Spedizione dei documenti</div>
                <h4 class="tm-article-subtitle">Indicare se includere le spese di spedizione nella pratica  <a data-uk-modal="{target:'#modal8'}" href=" "><i class="uk-icon-question-circle"> </i> </a> </h4>
    <div id="modal8" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                                    <div class="uk-modal-dialog uk-text-justify">
                                        <a href="" class="uk-modal-close uk-close"></a>
                                        <h2>Indicare se includere le spese di spedizione nella pratica </h2>
                                        <p>Indicate “non includere” se vi recherete personalmente presso i nostri uffici a consegnare e ritirare la documentazione, oppure se la spedizione sara` effettuata a vostro carico. Indicare “spedizione andata e ritorno” se entrambe le spedizioni saranno a nostro carico, “solo ritorno” se procederete autonomamente e a vostro carico alla spedizione in andata ed affiderete a noi la spedizione di ritorno.
    </p>

                                    </div>
                                </div>
                <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#sity1'}" id="spese_spedizione">
                    <li ritorno="Spese di spedizione non incluse" aria-expanded=<?php echo ($shipping=='Non includere')?'"true" class="uk-active"':'"false"'; ?> spese_spedizione="0">
                        <a href="#">Non includere</a>
                    </li>
                    <li  ritorno="Spese di spedizione in andata e ritorno"  aria-expanded=<?php echo ($shipping=='Spedizione andata e ritorno')?'"true" class="uk-active"':'"false"'; ?> class="" spese_spedizione="2">
                        <a href="#">Spedizione andata e ritorno</a>
                    </li>
                    <li ritorno="Spesi di spedizione solo in ritorno" aria-expanded=<?php echo ($shipping=='Spedizione solo in ritorno')?'"true" class="uk-active"':'"false"'; ?> class="" spese_spedizione="1">
                        <a href="#">Spedizione solo in ritorno</a>
                    </li>
                    <input type="hidden" name="shipping" value="<?php echo $shipping; ?>" id="shipping">
                    <br><br>
                </ul>

            </div>
        </div>

        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Oltre al supporto per il visto ho bisogno di:</div>
                <h4 class="tm-article-subtitle">Indicare se oltre che per la pratica visto volete avvalervi del nostro supporto anche per la prenotazione di altri servizi  <a data-uk-modal="{target:'#modal9'}" href=" "><i class="uk-icon-question-circle"> </i> </a> </h4>
    <div id="modal9" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                                    <div class="uk-modal-dialog uk-text-justify">
                                        <a href="" class="uk-modal-close uk-close"></a>
                                        <h2>Indicare se oltre che per la pratica visto volete avvalervi del nostro supporto anche per la prenotazione di altri servizi</h2>
                                        <p>Oltre al servizio visto possiamo offrirvi, ai migliori prezzi disponibili sul mercato, supporto per la prenotazione del soggiorno ed altri servizi per la Russia (voli, treni, transfers, escursioni, tour). Prenotando con noi il soggiorno, in hotel o in appartamento, otterrete inoltre 20 euro di sconto su ogni procedura visto, che saranno scalati dalla quota corrisposta per gli altri servizi.</p>

                                    </div>
                                </div>
                <div class="uk-form">

                    <fieldset data-uk-margin="">
                        <label class="uk-margin-small-top">
                            <input name="add_hotel" type="checkbox" id="hotel" <?php echo $add_hotel; ?>>
                            Hotel
                        </label>
                        <label class="uk-margin-small-top">
                            <input name="add_appartamento" type="checkbox" id="appartamento" <?php echo $add_appartamento; ?>>
                            Appartamento
                        </label>
                        <label class="uk-margin-small-top">
                            <input name="add_volo_treno" type="checkbox" <?php echo $add_volo_treno; ?>>
                            Volo/Treno
                        </label>
                        <label class="uk-margin-small-top">
                            <input name="add_transfers" type="checkbox" <?php echo $add_transfers; ?>>
                            Transfers
                        </label>
                        <label class="uk-margin-small-top">
                            <input name="add_escursioni" type="checkbox" <?php echo $add_escursioni; ?>>
                            Escursioni
                        </label>
                    </fieldset>

                </div>
            </div>
        </div>

		<div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
    <div class="uk-modal-dialog uk-text-justify">
        <a href="" class="uk-modal-close uk-close"></a>
        <h2>Promo Code</h2>
        <p>Inserire qui eventuali codici promozionali. Lo sconto e` applicabile soltanto per gli utenti che effettuano la registrazione, o per quelli gia registrati, e l'importo scontato sara` indicato nella schermata successiva e nella mail che riceverete una volta inviata la richiesta.</p>
    </div>
</div>


 <h4 class="tm-article-subtitle">Commenti</h4>
        <div class="uk-grid">
            <div class="uk-width-1-1">
                <div class="uk-form">
                    <div class="uk-form-row">
                        <textarea name="add_other" cols="300" rows="50" class="uk-width-1-1" placeholder="Commenti" style="height: 50px;"><?php echo $add_other; ?></textarea>
                    </div>
                </div>
            </div>
            </div>
			<h4 class="tm-article-subtitle">Promo Code <a data-uk-modal="{target:'#modal11'}" href=" "><i class="uk-icon-question-circle"> </i> </a></h4>

			  <input  name="promocode"   type="text" placeholder="Promo Code  " >
    <input type="hidden" name="tolist" value="0" id="tolist">
    <?php if (!$user->get('id')) : ?>
    <input type="hidden" name="showStatus" value="0" id="showStatus">
    <?php else : ?>
        <input type="hidden" name="showStatus" value="2" id="showStatus"><br>
    <button class="uk-button" onclick="jQuery('#tolist').val(0);">Inviare la richiesta</button>
    <?php endif; ?>
    </li></ul>
<input type="hidden" name="_newUserName" id="_newUserName">
<input type="hidden" name="_newUserPhone" id="_newUserPhone">
<input type="hidden" name="_newUserEmail" id="_newUserEmail">
</form>
<?php if (!$user->get('id')) : ?>
<form>
<legend>Dati e contatti della persona di riferimento  <a data-uk-modal="{target:'#modal10'}" href=" "><i class="uk-icon-question-circle"> </i> </a>  </legend>

<div id="modal10" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
    <div class="uk-modal-dialog uk-text-justify">
        <a href="" class="uk-modal-close uk-close"></a>
        <h2>Dati e contatti della persona di riferimento</h2>
        <p>Indicare i dati della persona o dell’agenzia di riferimento, che sara` contattata dal nostro personale per l’avvio della pratica visto. </p>
    </div>
</div>
<div id="modal13" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
    <div class="uk-modal-dialog uk-text-justify">
        <a href="" class="uk-modal-close uk-close"></a>
        <h2>Area Clienti</h2>
        <p>Utilizzando il nome utente e la password indicati al momento della registrazione, potrete accedere al vostro profilo personale nell'area clienti, da dove e` possibile monitorare lo stato della richiesta attuale, visionare vecchie pratiche ed avviarne nuove.</p>
    </div>
</div>
 <div id="sendMailError" class="error uk-alert uk-alert-danger"><b>Attenzione:</b> Inserire il vostro nome e cognome e l'indirizzo e-mail al quale desiderate essere ricontattati.</div>

<input type="text" id="newUserName" name="newUserName" placeholder="Nome e cognome " value="<?php echo ($user->get('name'))?$user->get('name'):''; ?>">

<input type="text" id="newUserPhone" name="newUserPhone" placeholder="Telefono">

<input type="text" id="newUserEmail" name="newUserEmail" placeholder="Indirizzo e-mail" value="<?php echo ($user->get('email'))?$user->get('email'):''; ?>"><br>

<button class="uk-button" id="InviareButton" onclick="jQuery('#globalForm').submit();return false;">Proseguire</button>
<div class=" uk-hidden  toggle2" aria-hidden="false">



        <div class="uk-grid">
            <div class="uk-width-1-1 uk-text-justify">

Impostando il Nome Utente e la Password, e` possibile registrarsi all'Aerea Clienti del sito. <a data-uk-modal="{target:'#modal13'}" href=" "><i class="uk-icon-question-circle"> </i> </a>
				   <h4 class="tm-article-subtitle">Per effettuare le registrazione, e` sufficiente compilare i seguenti campi</h4>
				<div class="uk-form">
                    <fieldset data-uk-margin id="register">


                        <input type="text" id="newUserNomeUtente" name="newUserNomeUtente" placeholder="Nome utente">
                        <input type="password" id="newUserPassword1" name="newUserPassword1" placeholder="Impostare Password">
                       <input type="password" id="newUserPassword2" name="newUserPassword2" placeholder="Ripetere Password">
<br><br>
Gli utenti registrati possono avviare la pratica e procedere al pagamento direttamente online senza attendere di essere ricontattati da un nostro operatore, a meno che le condizioni indicate non richiedano una verifica.
 Se volete inviare la richiesta senza effettuare la registrazione, lasciate i campi vuoti e cliccate su "Inviare la richiesta senza effettuare la registrazione", e sarete ricontattati.
 <br><br>
                        <button class="uk-button" onclick="jQuery('#tolist').val(0);jQuery('#showStatus').val(2);checkUser(jQuery('input[name=newUserEmail]').val());return false;">Inviare i dati per la registrazione e la richiesta</button>
                    </fieldset>
                    <div data-uk-button-checkbox="">
<hr>

                        <button class="uk-button" onclick="sendMail();return false;">Inviare la richiesta senza effettuare la registrazione</button>




                    </div>
                </div>

            </div>

        </div>
     </div>
<?php if ($user->get('id') && $_GET['id']) : ?>
    <button class="uk-button" onclick="jQuery('#tolist').val(1);<?php if (!$user->get('id')) : ?>//preSubmit();return false;<?php endif; ?>">Salvare</button>
<?php endif; ?>
</form>
<?php endif; ?>
<script type="text/javascript">
    var personsNumber = <?php echo $_POST['personsNumber']?$_POST['personsNumber']:1; ?>;
    var mCompila = <?php echo $params->compila?$params->compila:0; ?>;

    var mItalianCities = [
        [1 , "Milano" ],
        [2 , "Palermo" ],
        [3 , "Roma" ],
        [6 , "Genova" ]
    ];
    var mRussianCities = [
        [1 , "Mosca" ],
        [2 , "St.Petersburg" ]
    ];
    var mInvitationPrice = [
        <?php echo $params->invitationprice1; ?>,
        <?php echo $params->invitationprice2; ?>]
    var mInsurancePrice = [
        [7,<?php echo $params->insuranceprice1; ?> ],
        [14,<?php echo $params->insuranceprice2; ?> ],
        [21,<?php echo $params->insuranceprice3; ?> ],
        [30,<?php echo $params->insuranceprice4; ?> ]
    ];
    var mServicePrice = [];
    mServicePrice[0] = [0,0];
    mServicePrice[1] = [<?php echo $params->serviceprice1; ?>,<?php echo $params->serviceprice5; ?>];
    mServicePrice[2] = [<?php echo $params->serviceprice2; ?>,<?php echo $params->serviceprice6; ?>];
    mServicePrice[3] = [<?php echo $params->serviceprice3; ?>,<?php echo $params->serviceprice7; ?>];
    mServicePrice[6] = [<?php echo $params->serviceprice4; ?>,<?php echo $params->serviceprice8; ?>];

    var mRegistrationPrice = [];
    mRegistrationPrice[0] = [0,0];
    mRegistrationPrice[1] = [<?php echo $params->registrationprice1; ?>,0];
    mRegistrationPrice[2] = [<?php echo $params->registrationprice2; ?>,0];
    var mConsulPrice = [];
    mConsulPrice[0] = [0,0];
    mConsulPrice[1] = [<?php echo $params->consulprice1; ?>,<?php echo $params->consulprice5; ?>];
    mConsulPrice[2] = [<?php echo $params->consulprice2; ?>,<?php echo $params->consulprice6; ?>];
    mConsulPrice[3] = [<?php echo $params->consulprice3; ?>,<?php echo $params->consulprice7; ?>];
    mConsulPrice[6] = [<?php echo $params->consulprice4; ?>,<?php echo $params->consulprice8; ?>];

    var mPostPrice = [];
    mPostPrice[0] = [0,0];
    mPostPrice[1] = [<?php echo $params->postprice1; ?>,<?php echo $params->postprice5; ?>];
    mPostPrice[2] = [<?php echo $params->postprice2; ?>,<?php echo $params->postprice6; ?>];
    mPostPrice[3] = [<?php echo $params->postprice3; ?>,<?php echo $params->postprice7; ?>];
    mPostPrice[6] = [<?php echo $params->postprice4; ?>,<?php echo $params->postprice8; ?>];



</script>
<link rel="stylesheet" href="/images/css/jqueryui.custom.css">
<script src="<?php echo JURI::root(true).'/images/jqueryui.custom.js'; ?>"></script>
<script src="<?php echo JURI::root(true).'/components/com_touristinvite/assets/js/mootools.js'; ?>"></script>
<script src="<?php echo JURI::root(true).'/components/com_touristinvite/assets/js/mootools-more.js'; ?>"></script>
<script src="<?php echo JURI::root(true).'/components/com_touristinvite/assets/js/visto.js?v12'; ?>"></script>
<div style="display: none;" id="data"></div>