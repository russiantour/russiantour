<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_touristinvite', JPATH_ADMINISTRATOR);
$params = JComponentHelper::getParams('com_touristinvite')->get('params');
//$params = (isset($this->state->params)) ? $this->state->params : new JObject;
$user = JFactory::getUser();
if ($user->get('id')){
    $lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
    $participants = JFactory::getApplication()->input->get('participants',null,'array');
}
?>

<ul id="tab-content" class="tm-tab-content uk-switcher">
<li class="uk-active"> 
    <ul class="uk-tab">
    <li class="uk-active"><a href="/ita/modulo-visto-turistico/?option=com_touristinvite&view=list">Pratiche visto</a></li>
    <li class=""><a href="https://siteheart.com/webconsultation/765464" target="_blank" onclick="popupWin = window.open(this.href, 'contacts', 'location,width=400,height=300,top=0'); popupWin.focus(); return false;">Assistenza</a></li>
    <li class=" "><a href="/ita/profilo">Modifica profilo</a></li>
    <li class=" "><a href="/ita/exit">Fine sessione</a></li>
</ul>
		<div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Visto turistico russia</div>
<br>

		<div class="uk-grid uk-grid-collapse"> 
 
	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">1</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Compilazione dati</span> </div>

	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">2</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Riepilogo ed istruzioni</span> </div>

	   <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification  uk-badge-success<?php echo $step3; ?> ">3</span> <span  class="uk-width-8-10  uk-badge-success uk-badge <?php echo $step3; ?>"> <i class="uk-icon-spinner uk-icon-spin"> </i>  Pagamento</span>    </div>
	   

	
		<div class="uk-width-medium-1-4" ><span class="uk-badge uk-badge-notification <?php echo $step4; ?>">4</span> <span  class="uk-width-8-10 uk-badge <?php echo $step4; ?>"> Invio documenti</span>
		</div>
     
   </div>    
   
   





 <?php

 $Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'rus_visa' => 'Registrazione del visto russo'
 );
 if (!$lastRecord['urgent']) unset($Servizi['urgent']);
 if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
 if (!$lastRecord['rus_visa']) unset($Servizi['rus_visa']);
    $ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
 //////////////////////
 $add = array(
     'add_hotel' => 'hotel',
     'add_appartamento' => 'appartamento',
     'add_volo_treno' => 'volo/treno',
     'add_transfers' => 'transfers',
     'add_escursioni' => 'escursioni'
 );
 if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
 if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
 if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
 if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
 if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
 $addTXT = count($add)?implode(',',$add):'';
 ?>

		
<br><br>		

<div class="uk-grid uk-grid-collapse"> 
<div class="uk-width-medium-1-1">
  <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">  Prima di effettuare il pagamento leggere le condizioni generali di contratto.</div>


    
<div ><div class="uk-float-left">
<label class="uk-margin-small-top">
                                    <input  type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);">
                                        </label> </div><div style="padding-top: 7px;" ><b>
         <a data-uk-modal="{target:'#modal11'}" href=" " >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL  </a></b></div></div>
   <br> 
	<b>	E` possibile procedere al pagamento della somma preventivata in una delle seguenti modalita`: </b>
<br>
<div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                                    <div class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large">
                                        <a href="" class="uk-modal-close uk-close"></a>
                                        <h2>Condizioni di fornitura del servizio:</h2><br />
										<br>
							Effettuando il pagamento, il Cliente affida all’Organizzatore ed alle sue agenzie corrispondenti in Italia la gestione dei documenti inerenti alla pratica richiesta visto, li autorizza al trattamento dei dati personali indicati per lo svolgimento del servizio richiesto, e&nbsp; li solleva da ogni responsabilità per eventuali rifiuti, ritardi, problemi e disguidi &nbsp;nell’ottenimento del visto, e all’ingresso nel paese richiedente visto, dovuti a cause non imputabili al loro operato, ossia:<br /> <br /> - il passaporto non venga accettato dal Consolato perche` con scadenza inferiore a 6 mesi o perche` considerato non integro, usurato, non firmato;<br /> - i dati personali del Cliente siano stati dallo stesso compilati in maniera errata al momento dell’invio della richiesta online sul sito web dell’Organizzatore; <br /> - il visto venga rifiutato per qualsiasi motivo a discrezione del Consolato (diniego del visto, modulo elettronico richiesta visto compilato dal Cliente in maniera non corretta, documenti spediti dal Cliente in maniera errata o incompleta);<br /> - ci siano ritardi nel rilascio del visto dovuti a impreviste chiusure al pubblico del Consolato o a improvvise modifiche dei tempi e delle modalita` di ottenimento del visto;<br /> - si verifichino smarrimento dei documenti da parte dei corrieri postali, consegne ad indirizzi errati, o ritardi nella consegna imputabili agli stessi corrieri;<br /> - il visto venga rilasciato con date di ingresso diverse da quelle richieste a causa di decisione o di errore del consolato;<br /> - si verifichino problemi alla frontiera con il personale di dogana che implichino il rimpatrio.<br /> <br /> 
Presso il consolato di Milano, per visti superiori ai 14 giorni, e` generalmente richiesta la seguente documentazione aggiuntiva: prenotazioni alberghiere, fatture da parte degli hotel o dei sistemi di prenotazione, ricevute di pagamento dei servizi al 100%. Se non avete prenotato con noi l'intero viaggio, o se non disponete autonomamente di tutta questa documentazione, consigliamo di non consegnare autonomamente i documenti all'ufficio visti di Milano e di non selezionare Milano per la procedura completa.  Se il Cliente decide comunque di presentare la domanda a Milano per visti superiori a 14 giorni in assenza delle condizioni necessarie, l'Organizzatore non e` responsabile nel caso di diniego del visto, e non viene effettuta alcuna compensazione o riprotezione presso altri consolati. <br /> <br />

Per i minori non accompagnati da entrambe i genitori, e` richiesta la seguente documentazione aggiuntiva: certificato di nascita con i dati dei genitori, certificato di stato di famiglia, copia del documento d'identita` del genitore o dei genitori che non accompagnano il minore, dichiarazione di assenso con firma autenticata dove si autorizza il viaggio del minore in sua/loro assenza.
							<br /> <br />La pratica non sara` avviata fino a quando il pagamento non sara’ stato effettuato e confermato. Nel caso in cui a causa della presenza di festivita` italiane o russe non sia possibile ottenere il visto entro i termini standard, o nel caso in cui la posizione geografica del Cliente non consenta una ricezione dei documenti con corriere entro la data di partenza indicata, la transazione effettuata con carta di credito dal Cliente non sara` autorizzata e l’importo tornera` integralmente sulla carta il primo giorno lavorativo successivo al pagamento. Il Cliente se lo riterra` opportuno potra` in seguito effettuare un nuovo pagamento indicando nuove date o passando a procedura urgente o extraurgente, ove possibile. Nel caso di pagamento con bonifico bancario, il Cliente potra` procedere ad eventuale integrazione (passando a procedura urgente o extraurgente, ove possibile) o richiedere e ricevere il completo rimborso della somma pagata.<br /> <br /> In caso di pagamento con bonifico bancario, il versamento e` da effettuare in Euro e le commissioni bancarie debbono essere shared (SHA) e non a carico del beneficiario. Qualora l’importo ricevuto sia inferiore a quello preventivato, verra` richiesta al Cliente una integrazione. In caso di pagamento con carta di credito si applica una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli, in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi. <br /> <br /> In caso di procedura completa, il Cliente e` tenuto a controllare i documenti all’atto della consegna. Non verranno accettati reclami se non comunicati il giorno stesso della ricezione dei documenti. In caso di ritardi, errori o mancate concessioni del visto imputabili all’operato dell’organizzatore o delle sue agenzie corrispondenti in Italia, l’Organizzatore provvedera` esclusivamente al rimborso della quota visto e degli altri servizi collegati alla prenotazione effettuata dal Cliente con l’Organizzatore o suo intermediario , e di nessun altra spesa dovuta a servizi acquistati con terzi o a problemi personali collegabili al mancato viaggio. <br /> <br />

Se il Cliente ha cliccato NO nella sezione "Procedura completa" sul form online , per ottenere il visto dovra` recarsi personalmente presso gli uffici predisposti dai consolati russi di Roma, Milano, Genova e Palermo, previo appuntamento, una volta appurati i tempi e le modalita` di presentazione della domanda di visto. Non e` possibile entrare in territorio russo con la sola stampa della lettera d'invito e dell'assicurazione, va richiesto ed ottenuto il visto russo prima di effettuare il viaggio. La lettera d'invito deve essere coperta da una prenotazione alberghiera, se e` stata effettuata una prenotazione autonomamente e` necessario indicare al nostro corrispondente l'hotel prenotato e le date della prenotazione, tenendo presente che il consolato potrebbe verificare la prenotazione. A Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare autonomamente copia delle nostre assicurazioni presso gli uffici consolari di queste due citta`, ed e` necessario che il Cliente si procuri autonomamente una polizza assicurativa in originale tra quelle accettate dall'ufficio consolare. A Genova e Palermo vengono accettate anche le copie, quindi e` possibile stampare e presentare la nostra polizza, che sara` spedita via mail in giornata, se il pagamento e` stato effettuato in orario lavorativo, o il primo giorno lavorativo successivo al pagamento.<br /><br />
							Qualsiasi variazione a quanto dichiarato dovrà essere concordata per iscritto (fax o e-mail). Per qualsiasi controversia sarà competente il Foro di San Pietroburgo. <br /> 
<br>
                                    </div>
                                </div>	
 <h4 class="tm-article-subtitle">Tramite bonifico bancario:</h4>
Bonifico bancario europeo (SEPA), le spese bancarie non devono essere a carico del beneficiario, ma condivise (SHA). Copia del pagamento deve essere inviata scannerizzata all’indirizzo mail visto@russiantour.com -  i pagamenti non notificati, non saranno presi in considerazione.
<br>


   <button class="uk-button Inviare"     id="COORDINATE_BANCARIE" disabled data-uk-toggle="{target:'#my-id'}">COORDINATE BANCARIE</button><br>
    <div aria-hidden="true" class="uk-hidden" id="my-id">
        <div class="uk-panel uk-panel-box  uk-container-center uk-width-medium-1-2">
            <div class="uk-panel-badge uk-badge uk-badge-danger">pagamento </div>
            <h3 class="uk-panel-title">COORDINATE BANCARIE</h3>
            <i class="uk-icon-university"></i> Beneficiary: RUSSIAN TOUR INTERNATIONAL LTD<br>
            Account number (IBAN): LV48AIZK0000010381601 <br>

            SWIFT: AIZKLV22<br>
            Bank: ABLV Bank<br>
            Address: 23 Elizabetes Street, Riga, LV-1010, Latvia <br>
            Casuale pagamento: pratica visto ID:<?php echo $lastRecord['id']; ?><br>
            Promo code: <span > <?php echo $lastRecord['skidka_name']; ?></span>  <br>
			Importo:  <?php if ($lastRecord['skidka_val']) : ?>

                    <span ><?php echo $lastRecord['total_cost']; ?></span> € -  <span ><?php echo $lastRecord['skidka_val']; ?></span> % = <span ><b><?php echo $itogCost = $lastRecord['total_cost'] - $lastRecord['total_cost']*$lastRecord['skidka_val']/100; ?> € </b></span>
					<br>
<?php else : ?>
  <span ><?php echo $lastRecord['total_cost']; ?></span> €  
<?php endif; ?>
   <br>

        </div>

    </div>
   
   <br>

<h4 class="tm-article-subtitle">Con carta di credito:</h4>
Il sistema di pagamento è sicuro, il nostro sito internet supporta la crittografia a 256-bit. Le informazioni inserite non verranno fornite a terzi, salvo nei casi previsti dalla legislazione della Federazione Russa. Le operazioni con carta di credito sono condotte in stretta conformità con i requisiti di sistemi di pagamento internazionali Visa Int. e MasterCard Europe Sprl. <br>
Sul pagamento con carta di credito, viene applicata una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi.<br>

      <button class="uk-button Inviare"    onclick="location.href='?option=com_touristinvite&preparePayment=1'" id="Inviare" disabled>PAGAMENTO CON CARTA DI CREDITO</button>


   </div>

        </div>
		




			

        
    
</li>

<script>
    window.onload = function(){
        jQuery('#COORDINATE_BANCARIE').on('click',function(){jQuery.ajax({url: '?option=com_touristinvite&preparePayment=2',type: 'get'});return true;});

    }
</script>

