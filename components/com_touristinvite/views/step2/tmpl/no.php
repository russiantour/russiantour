<?php
$step2 = 'uk-badge-success';
$step3 = '';
$step4 = '';
?>
<?php /*if ($_GET['readyToPay']) : $step2 = 'uk-badge-warning'; $step3 = 'uk-badge-success'; ?><a href="?option=com_touristinvite&preparePayment=1">ОПЛАТИТЬ!!!</a><?php endif; ?>
<?php if ($_GET['payResult']) : $step2 = 'uk-badge-warning'; $step3 = 'uk-badge-warning'; $step4 = 'uk-badge-success'; echo $_GET['payResult']; endif;*/ ?>

<ul id="tab-content" class="tm-tab-content uk-switcher">
    <li class="uk-active">
    <ul class="uk-tab">
    <li class="uk-active"><a href="/ita/modulo-visto-turistico/?option=com_touristinvite&view=list">Pratiche visto</a></li>
    <li class=""><a href="https://siteheart.com/webconsultation/765464" target="_blank" onclick="popupWin = window.open(this.href, 'contacts', 'location,width=400,height=300,top=0'); popupWin.focus(); return false;">Assistenza</a></li>
    <li class=" "><a href="/ita/profilo">Modifica profilo</a></li>
    <li class=" "><a href="/ita/exit">Fine sessione</a></li>
</ul>
        <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Visto turistico russia</div>
        <br>
        <div class="uk-grid uk-grid-collapse">

            <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">1</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Compilazione dati</span> </div>

            <div class="uk-width-medium-1-4"><span   class="uk-badge uk-badge-notification <?php echo $step2; ?> ">2</span> <span  class="uk-width-8-10 uk-badge <?php echo $step2; ?>"> <i class="uk-icon-spinner uk-icon-spin"> </i>  Consolato: </span>    </div>
            <div class="uk-width-medium-1-4"><span   class="uk-badge uk-badge-notification <?php echo $step3; ?> ">3</span> <span  class=" uk-width-8-10 uk-badge <?php echo $step3; ?>">Pagamento</span>    </div>
            <div class="uk-width-medium-1-4" ><span class="uk-badge uk-badge-notification <?php echo $step4; ?>">4</span> <span  class="uk-width-8-10 uk-badge <?php echo $step4; ?>"> Invio documenti</span>
            </div>

        </div>
        <br>
        <div class="uk-grid">
            <div class="uk-width-medium-7-10">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Riepilogo dati </div>
                <h4 class="tm-article-subtitle">Date del viaggio</h4>
                <div class="uk-grid">


                    <div class="uk-width-1-2">Data di arrivo in Russia
                        <div class="uk-text-large">
                            <?php $dateFrom = explode('-',$lastRecord['date_from']); echo $dateFrom[2].'/'.$dateFrom[1].'/'.$dateFrom[0]; ?>
                        </div>


                    </div>
                    <div class="uk-width-1-2">Data di partenza dalla Russia
                        <div class="uk-text-large"><?php $dateTo = explode('-',$lastRecord['date_to']); echo $dateTo[2].'/'.$dateTo[1].'/'.$dateTo[0]; ?></div>
                    </div>
                </div>


                <div>
                    <h4 class="tm-article-subtitle">Partecipanti al viaggio</h4>

                </div>
                <?php if (count($participants)) : ?>
                    <?php foreach($participants as $participant) :
                        $gender = ($participant['gender']=='f')?'female':'male';
                        ?>
                        <article class="uk-comment">
                            <header class="uk-comment-header">
                                <img class="uk-comment-avatar" src="/images/yootheme/uikit_avatar.svg" width="50" height="50" alt="">
                                <h4 class="uk-comment-title">Nome: <b><?php echo $participant['first_name']; ?></b> Cognome:<b><?php echo $participant['second_name']; ?> </b> Data di nascita: <b> <?php $birthDate = explode('-',$participant['birthdate']); echo $birthDate[2].'/'.$birthDate[1].'/'.$birthDate[0]; ?></b> Sesso: <b><?php echo $gender; ?></b> <br>
                                    Nazionalita <b><?php echo $participant['nationality']; ?></b> Numero di passaporto <b><?php echo $participant['passport']; ?></b></h4>

                            </header>

                        </article>
                    <?php endforeach; ?>
                <?php endif; ?>

                </article>
            </div>





            <div class="uk-width-medium-3-10">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Costo della pratica</div>
                <h4 class=" uk-text-center margin0">ID: <b><?php echo $lastRecord['id']; ?></b></h4>
                <div class="uk-text-center uk-text-large ">Prezzo<br>
                   
                
 
<?php if ($lastRecord['skidka_val']) : ?>

                    <span ><?php echo $lastRecord['total_cost']; ?></span> € -  <span ><?php echo $lastRecord['skidka_val']; ?></span> % = <span ><?php echo $itogCost = $lastRecord['total_cost'] - $lastRecord['total_cost']*$skidka/100; ?> € </span>
					<br>Promo code<span > <?php echo $lastRecord['skidka_name']; ?></span>  <br>
<?php else : ?>
  <span ><?php echo $lastRecord['total_cost']; ?></span> €  
<?php endif; ?>
							
							
							</div>
				

            </div>

        </div>
        <?php

        $Servizi = array(
            'urgent' => 'Ho bisogno della procedura urgente',
            'form_filling' => 'Compilazione del modulo consolare per mio conto',
            'rus_visa' => 'Registrazione del visto russo'
        );
        if (!$lastRecord['urgent']) unset($Servizi['urgent']);
        if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
        if (!$lastRecord['rus_visa']) unset($Servizi['rus_visa']);
        $ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
        //////////////////////
        $add = array(
            'add_hotel' => 'hotel',
            'add_appartamento' => 'appartamento',
            'add_volo_treno' => 'volo/treno',
            'add_transfers' => 'transfers',
            'add_escursioni' => 'escursioni'
        );
        if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
        if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
        if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
        if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
        if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
        $addTXT = count($add)?implode(',',$add):'';
        ?>
        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase"> Riepilogo servizi richiesti </div>
                <h4 class="tm-article-subtitle"><b>Solo documentazione,</b></h4>
                <h4  style="margin-top: 1px;margin-bottom: 0px;">Presentazione domanda in Consolato:  <b><?php echo $lastRecord['consulate']; ?>.</b></h4>
                <h4  style="margin-top: 1px;margin-bottom: 0px;">Assicurazione:: <b><?php echo $lastRecord['insurance']?'Inclusa':'No Inclusa'; ?></b></h4>
                <h4  class="uk-display-inline">Citta` da visitare: <b><?php echo $lastRecord['cities_to_visit']; ?> </b></h4>      <br>
                <h4  class="uk-display-inline">Servizi: <b><?php echo $ServiziTXT;//TODO: city?>  </b></h4><br>

                <h4  class="uk-display-inline">Spedizione: <b><?php echo $lastRecord['shipping']; ?></b></h4>     <br>
                <h4  class="uk-display-inline">Ho bisogno anche di: <b><?php echo $addTXT; ?></b></h4> <br>
                <h4  class="uk-display-inline">Commenti: </h4><div class="uk-text-small"><?php echo $lastRecord['add_other']; ?> </div> <br>


            </div>

        </div>

        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1 uk-text-justify">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase"> Controllare la correttezza dei dati inviati.</div>
                                    Grazie per aver inviato la richiesta! Le richieste per solo documentazione richiedono una verifica da parte di un nostro operatore. Sarete contattati al piu` presto telefonicamente o via mail. 
Una volta confermata la pratica ed effettuato il pagamento, riceverete via mail la lettera d'invito  ed e una copia dell'assicurazione medica necessarie per l'ottenimento del visto russo. 
Avendo cliccato NO nella sezione "Procedura completa" sul form online , per ottenere il visto dovrete recarvi personalmente presso gli uffici predisposti dai consolati russi di Roma, Milano, Genova e Palermo, previo appuntamento, una volta appurati i tempi e le modalita` di presentazione della domanda di visto. Non e` possibile entrare in territorio russo con la sola stampa della lettera d'invito e dell'assicurazione, va richiesto ed ottenuto il visto russo prima di effettuare il viaggio. La lettera d'invito deve essere coperta da una prenotazione alberghiera, se avete effettuato una prenotazione autonomamente dovrete indicare al nostro corrispondent l'hotel prenotato e le date della prenotazione, tenendo presente che il consolato potrebbe verificare la prenotazione. A Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare autonomamente copia delle nostre assicurazioni presso gli uffici consolari di queste due citta`, ed e` necessario che il Cliente si procuri autonomamente una polizza assicurativa in originale tra quelle accettate dall'ufficio consolare. A Genova e Palermo vengono accettate anche le copie, quindi e` possibile stampare e presentare la nostra polizza,  che sara` spedita via mail in giornata, se il pagamento e` stato effettuato in orario lavorativo, o il primo giorno lavorativo successivo al pagamento.            </div>

        </div>


        <br>
        <?php if ($lastRecord['status']==1) : ?>
            <a class="uk-button"  href="/ita/?option=com_touristinvite&view=step3&readyToPay=1">MODALITA` DI PAGAMENTO</a>
        <?php endif; ?>
        <br>








    </li>