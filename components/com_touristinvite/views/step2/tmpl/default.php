<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;
//ini_set('display_errors',1);
//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_touristinvite', JPATH_ADMINISTRATOR);
$params = JComponentHelper::getParams('com_touristinvite')->get('params');
//$params = (isset($this->state->params)) ? $this->state->params : new JObject;
$user = JFactory::getUser();
if ($user->get('id')){
    $lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
    $participants = JFactory::getApplication()->input->get('participants',null,'array');
    $daysNum = (strtotime($lastRecord['date_from'])-strtotime($lastRecord['time_added']))/(3600*24);
    $daysLong = (strtotime($lastRecord['date_to'])-strtotime($lastRecord['date_from']))/(3600*24);
    $daysLong += 1;
    $skidka = ($lastRecord['skidka_val'])?$lastRecord['skidka_val']:1;
    $itogCost = $lastRecord['total_cost'] - $lastRecord['total_cost']*$skidka/100;
    //var_dump($lastRecord['id'],'$daysNum',$daysNum);
    if ($lastRecord['consulate']=='No')
    {
        include('no.php');
        include('./components/com_touristinvite/views/mails/no.php');
    }
    elseif (
        $lastRecord['status']==0 &&
        (
            $daysNum < 10 ||
            ($daysNum >=10 && $daysNum < 20 && !$lastRecord['urgent']) ||
            ($lastRecord['consulate']=='Milano' && $daysLong > 13)
        )
    )
    {
        include('confirm.php');
        include('./components/com_touristinvite/views/mails/create_yes.php');
    }
    else
    {
        $query = 'UPDATE #__touristinvite_visaandinivte SET status = 1 WHERE status = 0 and id = '.intval($lastRecord['id']);
        $db = JFactory::getDBO();
        $db->setQuery($query);
        $db->execute();
        include('noConfirm.php');
        include('./components/com_touristinvite/views/mails/create_no.php');
    }


}
?>

				