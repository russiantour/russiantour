<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class TouristinviteViewStep2 extends JViewLegacy {
    /*public $lastRecord;
    public $participants;*/
    /*protected $state;
    protected $item;
    protected $form;
    protected $params;*/

    /**
     * Display the view
     */
    public function display($tpl = null) {

        //else
        /*{
            $db = JFactory::getDBO();
            $query = 'SELECT *  FROM #__touristinvite_visaandinivte WHERE  user_id = '.$user->get('id'). ' ORDER BY id DESC LIMIT 1' ;
            $db->setQuery($query);
            $lastRecord = $db->loadAssoc();
            if (!$_GET['payResult'] && (!isset($lastRecord['status']) || $lastRecord['status']==2))
            {
                //JFactory::getApplication()->redirect('?option=com_touristinvite&view=step1');exit;
            }
        }*/

        parent::display($tpl);
    }
}
