<?php

/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class TouristinviteViewSendmail extends JViewLegacy {

    /*protected $state;
    protected $item;
    protected $form;
    protected $params;*/

    /**
     * Display the view
     */
    public function display($tpl = null) {
        /*ini_set('display_errors',1);
        $menus = JSite::getMenu();
        $menu = $menus->getActive();
        //$menu_params = new JParameter( $menu->params );
        //var_dump($menu_params->get( 'description'));
        //var_dump($this,$menu->params->get('menu-meta_description'));
        $doc = &JFactory::getDocument();
        $doc->setMetaData('description', $menu->params->get( 'description'),true);
        $this->description = '123';//$menu->params->get( 'description');*/
        parent::display($tpl);
    }
}
