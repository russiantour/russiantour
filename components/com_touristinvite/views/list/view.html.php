<?php

/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View to edit
 */
class TouristinviteViewList extends JViewLegacy {

    /*protected $state;
    protected $item;
    protected $form;
    protected $params;*/

    /**
     * Display the view
     */
    public function display($tpl = null) {
        parent::display($tpl);
    }
}
