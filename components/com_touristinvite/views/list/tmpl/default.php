<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> -
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_touristinvite', JPATH_ADMINISTRATOR);
$params = JComponentHelper::getParams('com_touristinvite')->get('params');
//$params = (isset($this->state->params)) ? $this->state->params : new JObject;
$user = JFactory::getUser();
if ($user->get('id')) :
    $records = JFactory::getApplication()->input->get('records',null,'array');
$userProfile = JUserHelper::getProfile( $user->get('id') );
?>


<li class="uk-active">
    <ul class="uk-tab">
    <li class="uk-active"><a href="/ita/modulo-visto-turistico/?option=com_touristinvite&view=list">Pratiche visto</a></li>
    <li class=""><a href="https://siteheart.com/webconsultation/765464" target="_blank" onclick="popupWin = window.open(this.href, 'contacts', 'location,width=400,height=300,top=0'); popupWin.focus(); return false;">Assistenza</a></li>
    <li class=" "><a href="/ita/profilo">Modifica profilo</a></li>
    <li class=" "><a href="/ita/exit">Fine sessione</a></li>
</ul>

<div class="uk-grid uk-grid-collapse">
    <div class="uk-width-1-1 uk-text-right">
        Salve <?php echo $user->get('name'); ?>
        <br>
        Il suo indirizzo mail:  <?php echo $user->get('email'); ?><br>Il suo numero di telefono:
<?php echo "" . $userProfile->profile['phone']; ?> <br>
        <a href="/ita/modulo-visto-turistico"class="uk-button uk-button-success" data-uk-tooltip=""><i class="uk-icon-arrow-circle-o-right"></i> Nuova pratica </a>
    </div> </div>
    <?php if (count($records)) :
        foreach ($records as $key=>$record) :
            switch ($record['record']['status']) {
                case 0:
                    $step2 = 'uk-badge-success';
                    $step3 = '';
                    $step4 = '';
                    break;
                case 1:
                    $step2 = 'uk-badge-warning';
                    $step3 = 'uk-badge-success';
                    $step4 = '';
                    break;
                case 2:
                    $step2 = 'uk-badge-warning';
                    $step3 = 'uk-badge-warning';
                    $step4 = 'uk-badge-success';
                    break;
            }
            $icon = array(2=>'',3=>'',4=>'');
            $icon[$record['record']['status']+2] = '<i class="uk-icon-spinner uk-icon-spin"> </i>';
    ?>
<div class="uk-panel uk-panel-box">
    <br>
    <div class="uk-grid uk-grid-collapse">
        <div class="uk-width-medium-1-4">
            <span class="uk-badge uk-badge-notification uk-badge-warning">1</span>
            <span class="uk-width-8-10 uk-badge uk-badge-warning">Compilazione dati</span>
        </div>
        <div class="uk-width-medium-1-4">
            <span class="uk-badge uk-badge-notification <?php echo $step2; ?>">2</span>
            <span class="uk-width-8-10 uk-badge <?php echo $step2; ?>"> <?php echo $icon[2]; ?>RIEPILOGO ED ISTRUZIONI</span>
        </div>
        <div class="uk-width-medium-1-4">
            <span class="uk-badge uk-badge-notification <?php echo $step3; ?>">3</span>
            <span class="uk-width-8-10 uk-badge <?php echo $step3; ?>"> <?php echo $icon[3]; ?> Pagamento</span>
        </div>
        <div class="uk-width-medium-1-4">
            <span class="uk-badge uk-badge-notification <?php echo $step4; ?>">4</span>
            <span class="uk-width-8-10 uk-badge <?php echo $step4; ?>"> <?php echo $icon[4]; ?> Invio documenti</span>
        </div>
    </div>
    <br>
    <div class="uk-grid">
        <div class="uk-width-medium-7-10">
            <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Riepilogo dati </div>
            <h4 class="tm-article-subtitle">Date del viaggio</h4>
            <div class="uk-grid">
                <div class="uk-width-1-2">Data di arrivo in Russia
                    <div class="uk-text-large">
                        <?php $dateFrom = explode('-',$record['record']['date_from']); echo $dateFrom[2].'/'.$dateFrom[1].'/'.$dateFrom[0]; ?>
                    </div>
                </div>
                <div class="uk-width-1-2">Data di partenza dalla Russia
                    <div class="uk-text-large">
                        <?php $dateTo = explode('-',$record['record']['date_to']); echo $dateTo[2].'/'.$dateTo[1].'/'.$dateTo[0]; ?>
                    </div>
                </div>
            </div>
            <div>
                <h4 class="tm-article-subtitle">Partecipanti al viaggio</h4>
            </div>
            <?php foreach ($record['participants'] as $participant) :
                $gender = ($participant['gender']=='f')?'female':'male';
            ?>
            <article class="uk-comment">
                <header class="uk-comment-header">
                    <img class="uk-comment-avatar" src="/images/yootheme/uikit_avatar.svg" width="50" height="50" alt="">
                    <h4 class="uk-comment-title">
                        Nome: <b><?php echo $participant['first_name']; ?></b>
                        Cognome:<b><?php echo $participant['second_name']; ?> </b>
                        Data di nascita: <b>
                            <?php $birthDate = explode('-',$participant['birthdate']);
                            echo $birthDate[2].'/'.$birthDate[1].'/'.$birthDate[0]; ?>
                        </b>
                        Sesso: <b><?php echo $gender; ?></b> <br>
                        Nazionalita <b><?php echo $participant['nationality']; ?></b>
                        Numero di passaporto <b><?php echo $participant['passport']; ?></b>
                    </h4>
                </header>
            </article>
            <?php endforeach; ?>
        </div>

        <div class="uk-width-medium-3-10">
            <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">
                Costo della pratica
            </div>
            <h4 class=" uk-text-center margin0">ID: <b><?php echo $record['record']['id']; ?></b></h4>

            <div class="uk-text-center uk-text-large ">
                Prezzo
                <br>
                <span><?php echo $record['record']['total_cost']; ?></span> €
                <br>
                <?php if ($record['record']['status']==0) : ?>
                <?php elseif ($record['record']['status']==1) : ?>
                <a class="uk-button" data-uk-tooltip="" href="?option=com_touristinvite&view=step3&id=<?php echo $record['record']['id']; ?>">
                    <i class="uk-icon-credit-card"> </i>  Pagare
                </a>
                <?php elseif ($record['record']['status']==2) : ?>
                <a class="uk-button" data-uk-tooltip="" href="?option=com_touristinvite&view=step4&id=<?php echo $record['record']['id']; ?>">
                     <i class="uk-icon-file-text-o"> </i> Pagato
                </a>
                <?php endif; ?>
            </div>
        </div>

        <div class="uk-width-medium-1-1 ">
            <a data-uk-toggle="{target:'#ids<?php echo $key; ?>'}"><h3 class="uk-button uk-width-1-1 uk-margin-small-bottom">Maggiori informazioni</h3></a>
            <div id="ids<?php echo $key; ?>" class="uk-hidden" aria-hidden="true">
                <?php
                $Servizi = array(
                'urgent' => 'Ho bisogno della procedura urgente',
                'form_filling' => 'Compilazione del modulo consolare per mio conto',
                'rus_visa' => 'Registrazione del visto russo'
                );
                if (!$record['record']['urgent']) unset($Servizi['urgent']);
                if (!$record['record']['form_filling']) unset($Servizi['form_filling']);
                if (!$record['record']['rus_visa']) unset($Servizi['rus_visa']);
                $ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
                //////////////////////
                $add = array(
                'add_hotel' => 'hotel',
                'add_appartamento' => 'appartamento',
                'add_volo_treno' => 'volo/treno',
                'add_transfers' => 'transfers',
                'add_escursioni' => 'escursioni'
                );
                if (!$record['record']['add_hotel']) unset($add['add_hotel']);
                if (!$record['record']['add_appartamento']) unset($add['add_appartamento']);
                if (!$record['record']['add_volo_treno']) unset($add['add_volo_treno']);
                if (!$record['record']['add_transfers']) unset($add['add_transfers']);
                if (!$record['record']['add_escursioni']) unset($add['add_escursioni']);
                $addTXT = count($add)?implode(',',$add):'';
                ?>
                <div class="uk-grid uk-grid-collapse">
                    <div class="uk-width-medium-1-1">
                        <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase"> Riepilogo servizi richiesti </div>
                        <h4 class="tm-article-subtitle">Procedura completa</h4>
                        <h4  style="margin-top: 1px;margin-bottom: 0px;">Riepilogo ed istruzioni: <b><?php echo $record['record']['consulate']; ?>.</b></h4>
                        <h4  style="margin-top: 1px;margin-bottom: 0px;">Assicurazione:: <b><?php echo $record['record']['insurance']?'Inclusa':'No Inclusa'; ?></b></h4>
                        <h4  class="uk-display-inline">Citta` da visitare: <b><?php echo $record['record']['cities_to_visit']; ?> </b></h4>      <br>
                        <h4  class="uk-display-inline">Servizi: <b><?php echo $ServiziTXT;//TODO: city?>  </b></h4><br>

                        <h4  class="uk-display-inline">Spedizione: <b><?php echo $record['record']['shipping']; ?></b></h4>     <br>
                        <h4  class="uk-display-inline">Ho bisogno anche di: <b><?php echo $addTXT; ?></b></h4> <br>
                        <h4  class="uk-display-inline">Commenti: </h4><div class="uk-text-small"><?php echo $record['record']['add_other']; ?> </div> <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<br>
        <?php endforeach;
    endif; ?>
	<ul class="uk-pagination">
                                <li class="uk-disabled"><span><i class="uk-icon-angle-double-left"></i></span></li>
                                <li class="uk-active"><span>1</span></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><span>...</span></li>
                                <li><a href="#">20</a></li>
                                <li><a href="#"><i class="uk-icon-angle-double-right"></i></a></li>
                            </ul>
</li>
<?php endif; ?>