<?php
//$lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
//$participants = JFactory::getApplication()->input->get('participants',null,'array');

$Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'ho_registrazione_piter' => 'Registrazione San Pietroburgo',
    'ho_registrazione_moscow' => 'Registrazione Mosca'
);
if (!$lastRecord['urgent']) unset($Servizi['urgent']);
if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
if (!$lastRecord['ho_registrazione_piter']) unset($Servizi['ho_registrazione_piter']);
if (!$lastRecord['ho_registrazione_moscow']) unset($Servizi['ho_registrazione_moscow']);
$ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
//////////////////////
$add = array(
    'add_hotel' => 'hotel',
    'add_appartamento' => 'appartamento',
    'add_volo_treno' => 'volo/treno',
    'add_transfers' => 'transfers',
    'add_escursioni' => 'escursioni'
);
if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
$addTXT = count($add)?implode(',',$add):'';
$user = JFactory::getUser($lastRecord['user_id']);
$userProfile = JUserHelper::getProfile( $user->get('id') );
?>
<?php $subject = "Richiesta pratica visto confermata."; ?>
<?php $body = '
<table class="  -webkit-text-stroke-width: 0px;"
        width="970" align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tbody>
          <tr align="center">
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><big><big><img shrinktofit="true"
                    alt=" "
                    data-cke-saved-src="https://www.visto-russia.com/images/logo3.png"
                    src="https://www.visto-russia.com/images/logo3.png"
                    style="cursor: default; width: 970px; height:
                    167px;"></big></big></td>
          </tr>
          <tr align="center">
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><br>
              <big><big>Richiesta pratica visto confermata! A seguire un
                  riepilogo dei dati inseriti:</big></big><br>
              <br>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211); text-align: right;" valign="top">
              <p><span style="font-size: 20px;"><strong></strong></span>Costo

                della praticaI ID № '.$lastRecord['id'].'</p>
              <p><span style="font-size: 20px;"><strong><big>Prezzo:'.$lastRecord['total_cost'].'
                      €</big></strong></span><br>
              </p>
            </td>
          </tr>
          <tr>
            <td style="border: 1px dotted rgb(211, 211, 211);"
              valign="top">
              <p>Data di arrivo in Russia:<span
                  class="Apple-converted-space"> </span><br>
              </p>
              <p><strong><big>'.date('d/m/Y',strtotime($lastRecord['date_from'])).'</big></strong><br>
                <br>
                <br>
              </p>
            </td>
            <td style="border: 1px dotted rgb(211, 211, 211);"
              valign="top">
              <p>Data di partenza dalla Russia<span
                  class="Apple-converted-space"> </span><br>
              </p>
              <p><strong><big>'.date('d/m/Y',strtotime($lastRecord['date_to'])).'</big></strong><br>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><big><strong>Partecipanti al
                    viaggio</strong></big></em></td>
          </tr>';
    if (count($participants)) :
        foreach($participants as $participant) :
            $gender = ($participant['gender']=='f')?'female':'male';

            $body .= '<tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>Nome: <strong>'.$participant['first_name'].'</strong> Cognome:<strong>'.$participant['second_name'].'</strong> Data

                di nascita: <strong>'.date('d/m/Y',strtotime($participant['birthdate'])).'</strong> Sesso: <strong>'.$gender.'</strong></div>
              <div>Nazionalita<strong> '.$participant['nationality'].'</strong> Numero di
                passaporto <strong>'.$participant['passport'].'</strong></div>
            </td>
          </tr>';
        endforeach;
    endif;
        $body .= '<tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Riepilogo servizi
                    richiesti:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>Procedura completa:  '.$lastRecord['consulate'].'.</div>
              <div>Assicurazione: '.($lastRecord['insurance']?'Inclusa':'Non Inclusa').'</div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Citta` da visitare:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">'.$lastRecord['cities_to_visit'].'</td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Servizi aggiuntivi:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>'.$ServiziTXT.'</div>
              <div>Spedizione: '.$lastRecord['shipping'].'</div>
              <div>Ho bisogno anche di: '.$addTXT.'</div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Commenti:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">'.$lastRecord['add_other'].'</td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Mail: <a moz-do-not-send="true"
                class="moz-txt-link-abbreviated"
                href="'.$user->get('email').'">'.$user->get('email').'</a></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Telefono:+ '.$userProfile->profile['phone'].'</td>
          </tr>

          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
<p>
Utilizzando il nome utente e la password indicati al momento della registrazione, potrete accedere al <a href="https://www.visto-russia.com/ita/area-clienti"> vostro profilo personale nell\'area clienti</a>, da dove sara` possibile monitorare lo stato della richiesta attuale ed avviare nuove pratiche in futuro.

</p>

<p>
    <b>Siamo lieti di informarla che siamo nei tempi utili per l’evasione della pratica nelle modalita` richieste, ed e` possibile procedere all’avvio della
    pratica per l’ottenimento del visto russo.</b>
</p>
<p>
    <strong>Attenzione</strong>
    : la pratica viene avviata soltanto una volta effettuato il pagamento, se i tempi sono stretti e` consigliabile procedere immediatamente. Se il pagamento
    avviene dopo un certo numero di giorni potrebbe poi essere necessaria una integrazione per la procedura urgente o si potrebbe non fare piu` in tempo ad
    evadere la richiesta.
</p>


<p>
Avendo cliccato NO nella sezione "Procedura completa" sul form online , per ottenere il visto dovrete recarvi personalmente presso gli uffici predisposti dai consolati russi di Roma, Milano, Genova e Palermo, previo appuntamento, una volta appurati i tempi e le modalità di presentazione della domanda di visto. Non è possibile entrare in territorio russo con la sola stampa della lettera d\'invito e dell\'assicurazione, va richiesto ed ottenuto il visto russo prima di effettuare il viaggio. 
</p>

<p>
La lettera d\'invito deve essere coperta da una prenotazione alberghiera, se avete effettuato una prenotazione autonomamente dovrete indicare al nostro corrispondente l\'hotel prenotato e le date della prenotazione, tenendo presente che il consolato potrebbe verificare la prenotazione. A Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare autonomamente copia delle nostre assicurazioni presso gli uffici consolari di queste due citta`, ed è necessario che il Cliente si procuri autonomamente una polizza assicurativa in originale tra quelle accettate dall\'ufficio consolare. A Genova e Palermo vengono accettate anche le copie, quindi e` possibile stampare e presentare la nostra polizza, che sarà spedita via mail in giornata, se il pagamento è stato effettuato in orario lavorativo, o il primo giorno lavorativo successivo al pagamento.
</p>


<p>
    <br/>
     <a href="https://www.visto-russia.com/?option=com_touristinvite&view=step3&id='.$lastRecord['id'].'">Per procedere al pagamento cliccare qui.</a>
</p>
<p>
Entro una giornata lavorativa dalla notifica del pagamento, riceverete via mail la lettera d\'invito ed e una copia dell\'assicurazione medica necessarie per l\'ottenimento del visto russo. 
</p>

<br><br>Cordiali saluti, <br><br>
Lo Staff di Visto-Russia.com


</td>
          </tr>
        </tbody>
      </table>
'; ?>