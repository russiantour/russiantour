<?php
$lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
$participants = JFactory::getApplication()->input->get('participants',null,'array');

$Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'rus_visa' => 'Registrazione del visto russo'
);
if (!$lastRecord['urgent']) unset($Servizi['urgent']);
if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
if (!$lastRecord['rus_visa']) unset($Servizi['rus_visa']);
$ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
//////////////////////
$add = array(
    'add_hotel' => 'hotel',
    'add_appartamento' => 'appartamento',
    'add_volo_treno' => 'volo/treno',
    'add_transfers' => 'transfers',
    'add_escursioni' => 'escursioni'
);
if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
$addTXT = count($add)?implode(',',$add):'';

?>

<?php $subject = 'Vi ringraziamo per il pagamento! La pratica visto e` stata avviata.!'; ?>
<?php $body = '


<table border="0">
<tbody>
<tr align="left">
<td style="border: 1px dotted #d3d3d3;" valign="top">
Potete scaricare la lettera dinvito ed il voucher per il visto russo a questo indirizzo: ................................. (address dlia skachanie priglashenie – STEP 4).<br /><br /> Qualora abbiate acquistato anche lassicurazione medica, una copia dellassicurazione vi verra` inviata scannerizzata via mail.<br /><br /> Vi ringraziamo per la fiducia e vi auguriamo buon viaggio!<br /><br /> Cordiali saluti, <br /><br /> Lo Staff di Visto-Russia.com

</td>
</tr>
</tbody>
</table>

'; ?>

