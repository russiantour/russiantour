<?php $subject = 'Pagamento ricevuto. Riceverete la documentazione entro una giornata lavorativa.'; ?>
<?php $body = '
    Vi ringraziamo per il pagamento!
    <br/>
    Riceverete la lettera d’invito via mail entro una giornata lavorativa all’indirizzo indicato in fase di registrazione.
    <br/>
    Qualora abbiate acquistato anche l\'assicurazione medica, una copia dell\'assicurazione vi verra` inviata scannerizzata via mail.
    <br/>
    Qualora abbiate acquistato anche la registrazione, una copia del voucher per la registrazione vi verra` inviata via mail.
</p>
<p>
    Prima di effettuare il pagamento, avete accettato online le seguenti “Condizioni di fornitura del servizio”:
</p>
<p>
    Effettuando il pagamento, il Cliente affida all’Organizzatore ed alle sue agenzie corrispondenti in Italia la gestione dei documenti inerenti alla pratica
    richiesta visto, li autorizza al trattamento dei dati personali indicati per lo svolgimento del servizio richiesto, e li solleva da ogni responsabilità per
    eventuali rifiuti, ritardi, problemi e disguidi nell’ottenimento del visto, e all’ingresso nel paese richiedente visto, dovuti a cause non imputabili al
    loro operato, ossia:
    <br/>
    <br/>
    - il passaporto non venga accettato dal Consolato perche` con scadenza inferiore a 6 mesi o perche` considerato non integro, usurato, non firmato;
    <br/>
    - i dati personali del Cliente siano stati dallo stesso compilati in maniera errata al momento dell’invio della richiesta online sul sito web
    dell’Organizzatore;
    <br/>
    - il visto venga rifiutato per qualsiasi motivo a discrezione del Consolato (diniego del visto, modulo elettronico richiesta visto compilato dal Cliente in
    maniera non corretta, documenti spediti dal Cliente in maniera errata o incompleta);
    <br/>
    - ci siano ritardi nel rilascio del visto dovuti a impreviste chiusure al pubblico del Consolato o a improvvise modifiche dei tempi e delle modalita` di
    ottenimento del visto;
    <br/>
    - si verifichino smarrimento dei documenti da parte dei corrieri postali, consegne ad indirizzi errati, o ritardi nella consegna imputabili agli stessi
    corrieri;
    <br/>
    - il visto venga rilasciato con date di ingresso diverse da quelle richieste a causa di decisione o di errore del consolato;
    <br/>
    - si verifichino problemi alla frontiera con il personale di dogana che implichino il rimpatrio.
    <br/>
    <br/>
    Presso il consolato di Milano, per visti superiori ai 14 giorni, e` generalmente richiesta la seguente documentazione aggiuntiva: prenotazioni alberghiere,
    fatture da parte degli hotel o dei sistemi di prenotazione, ricevute di pagamento dei servizi al 100%. Se non avete prenotato con noi l\'intero viaggio, o
    se non disponete autonomamente di tutta questa documentazione, consigliamo di non consegnare autonomamente i documenti all\'ufficio visti di Milano e di non
    selezionare Milano per la procedura completa. Se il Cliente decide comunque di presentare la domanda a Milano per visti superiori a 14 giorni in assenza
    delle condizioni necessarie, l\'Organizzatore non e` responsabile nel caso di diniego del visto, e non viene effettuta alcuna compensazione o riprotezione
    presso altri consolati.
    <br/>
    <br/>
    Per i minori non accompagnati da entrambe i genitori, e` richiesta la seguente documentazione aggiuntiva: certificato di nascita con i dati dei genitori,
    certificato di stato di famiglia, copia del documento d\'identita` del genitore o dei genitori che non accompagnano il minore, dichiarazione di assenso con
    firma autenticata dove si autorizza il viaggio del minore in sua/loro assenza.
    <br/>
    <br/>
    La pratica non sara` avviata fino a quando il pagamento non sara’ stato effettuato e confermato. Nel caso in cui a causa della presenza di festivita`
    italiane o russe non sia possibile ottenere il visto entro i termini standard, o nel caso in cui la posizione geografica del Cliente non consenta una
    ricezione dei documenti con corriere entro la data di partenza indicata, la transazione effettuata con carta di credito dal Cliente non sara` autorizzata e
    l’importo tornera` integralmente sulla carta il primo giorno lavorativo successivo al pagamento. Il Cliente se lo riterra` opportuno potra` in seguito
    effettuare un nuovo pagamento indicando nuove date o passando a procedura urgente o extraurgente, ove possibile. Nel caso di pagamento con bonifico
    bancario, il Cliente potra` procedere ad eventuale integrazione (passando a procedura urgente o extraurgente, ove possibile) o richiedere e ricevere il
    completo rimborso della somma pagata.
    <br/>
    <br/>
    In caso di pagamento con bonifico bancario, il versamento e` da effettuare in Euro e le commissioni bancarie debbono essere shared (SHA) e non a carico del
    beneficiario. Qualora l’importo ricevuto sia inferiore a quello preventivato, verra` richiesta al Cliente una integrazione. In caso di pagamento con carta
    di credito si applica una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli, in base al tasso di cambio
    ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca
    corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi.
    <br/>
    <br/>
    In caso di procedura completa, il Cliente e` tenuto a controllare i documenti all’atto della consegna. Non verranno accettati reclami se non comunicati il
    giorno stesso della ricezione dei documenti. In caso di ritardi, errori o mancate concessioni del visto imputabili all’operato dell’organizzatore o delle
    sue agenzie corrispondenti in Italia, l’Organizzatore provvedera` esclusivamente al rimborso della quota visto e degli altri servizi collegati alla
    prenotazione effettuata dal Cliente con l’Organizzatore o suo intermediario , e di nessun altra spesa dovuta a servizi acquistati con terzi o a problemi
    personali collegabili al mancato viaggio.
    <br/>
    <br/>
    Se il Cliente ha cliccato NO nella sezione "Procedura completa" sul form online , per ottenere il visto dovra` recarsi personalmente presso gli uffici
    predisposti dai consolati russi di Roma, Milano, Genova e Palermo, previo appuntamento, una volta appurati i tempi e le modalita` di presentazione della
    domanda di visto. Non e` possibile entrare in territorio russo con la sola stampa della lettera d\'invito e dell\'assicurazione, va richiesto ed ottenuto il
    visto russo prima di effettuare il viaggio. La lettera d\'invito deve essere coperta da una prenotazione alberghiera, se e` stata effettuata una
    prenotazione autonomamente e` necessario indicare al nostro corrispondente l\'hotel prenotato e le date della prenotazione, tenendo presente che il
    consolato potrebbe verificare la prenotazione. A Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare
    autonomamente copia delle nostre assicurazioni presso gli uffici consolari di queste due citta`, ed e` necessario che il Cliente si procuri autonomamente
    una polizza assicurativa in originale tra quelle accettate dall\'ufficio consolare. A Genova e Palermo vengono accettate anche le copie, quindi e` possibile
    stampare e presentare la nostra polizza, che sara` spedita via mail in giornata, se il pagamento e` stato effettuato in orario lavorativo, o il primo
    giorno lavorativo successivo al pagamento.
    <br/>
    <br/>
    Qualsiasi variazione a quanto dichiarato dovrà essere concordata per iscritto (fax o e-mail). Per qualsiasi controversia sarà competente il Foro di San
    Pietroburgo.
    <br/>
    <br/>
    Vi ringraziamo per la fiducia e vi auguriamo buon viaggio!
    <br/>
    Cordiali saluti,
    <br/>
    Lo Staff di Visto-Russia.com


</p>
'; ?>