<?php $subject = 'Pagamento ricevuto. Istruzioni e indirizzo per la spedizione dei documenti.'; ?>
<?php $body = '<p>
    Vi ringraziamo per il pagamento! La pratica visto e` stata avviata.
    <br/>
    <br/>
    La seguente documentazione va consegnata o spedita tramite corriere espresso o raccomandata postale all`indirizzo successivamente menzionato:
</p>
<ul type="disc">
    <li>
        Passaporto con validità di almeno 6 mesi superiore alla data di previsto rientro e con almeno due pagine libere. Il passaporto deve essere integro, non
        rovinato, non usurato. In particolare verificate che non ci siano scollature tra la pellicola trasparente che ricopre la foto ed il resto del
        passaporto. Il passaporto deve essere firmato.
    </li>
    <li>
        Due fototessere a colori, su sfondo bianco, formato 35x45 mm. Le foto devono essere scattate da non più di sei mesi e non devono essere uguali alle
        foto del passaporto. L\'espressione del viso deve essere neutra, l\'immagine frontale, senza occhiali da sole o altri elementi che coprano parte del
        viso. In primo piano devono essere il viso e la parte superiore delle spalle. Il volto deve occupare circa il 70-80% della foto. La foto non deve
        essere nè incollata nè pinzata.
    </li>

'.(($lastRecord['form_filling']==0)?'
    <li>
        Una copia del <a href="http://visa.kdmid.ru/">modulo richiesta d\'ingresso (da compilare online sul sito del Consolato e stampare)</a> Il modulo ha validita` 30 giorni, non compilatelo piu` di un mese prima rispetto alla presentazione della pratica in consolato.
    </li>
	<li>

	Una copia del <a href="https://www.visto-russia.com/images/richiesta_incarico.pdf">modulo richiesta d\'incarico</a>
	</li>
	
	
	<br>
Compilare il modulo solo dopo aver ricevuto via mail le istruzioni dal nostro corrispondente. Nella mail, che riceverete entro un giorno lavorativo successivo al pagamento, saranno indicati il confirmation number della lettera d\'invito, il nome e l\'indirizzo dell\'organizzazione invitante, la compagnia assicurativa ed il numero di polizza. <br /><br /><b>Aiuto per la compilazione del modulo richiesta d\'ingresso</b><br />Luogo di nascita: Citta` e nazione nella quale siete nati. Se in precedenza avevate nazionalita’ russa o dell’Unione Sovietica indicare perche’ e’ stata persa e quando. Se siete nati in Russia scrivere in che Paese siete emigrati e quando. <br />Data di rilascio e data di scadenza del passaporto.<br />Se avete gia visitato la Russia scrivete quante volte ci siete stati e specificate le date dell\'ultimo viaggio (da-a)<br />Indirizzo di residenza: via, numero civico, citta`, CAP, nazione<br />Numero di telefono: indicare un numero al quale sia possibile facilmente contattarvi<br />Attuale posto di lavoro o di studio: posizione ricoperta, indirizzo e numero di telefono.<br />Se ci sono bambini sotto i 16 anni o altri parenti presenti sul vostro passaporto che viaggiano con voi, indicare: grado di parentela, nome e cognome, data di nascita, ed indirizzo di residenza.<br />Se avete attualmente parenti in Russia, indicare: grado di parentela, nome e cognome, data di nascita, ed indirizzo di residenza
':'').'

'.(($lastRecord['form_filling']==1)?'
  <li>
  Una copia del modulo richiesta d\'ingresso:<a href="https://www.visto-russia.com/?option=com_touristinvite&view=step4&id='.$lastRecord['id'].'"> cliccare qui per inviarci i dati necessari alla compilazione per vostro conto.</a>
</li>
	<li>

	Una copia del <a href="https://www.visto-russia.com/images/richiesta_incarico.pdf">modulo richiesta d\'incarico</a>
	</li>
	
':'').'
	
	

</ul>
<p>
    Con la procedura ordinaria il visto sara` pronto in circa 15 giorni dal momento della ricezione della documentazione da parte dei nostri corrispondenti. La
    documentazione sara\' presentata in consolato il giorno lavorativo successivo alla ricezione, ed il visto sara\' pronto in 10 giorni lavorativi. Con la
    procedura urgente il visto sara` pronto in 4 giorni lavorativi.
</p>
<p>
    <strong>Indirizzo per la spedizione dei documenti:</strong>
</p>
<p>
    L\'EXPRESS SAS
    <br/>
    Via Pisacane,28R
    <br/>
    16129 GENOVA
    <br/>
    Tel. 010 542490
    <br/>
    ORARIO APERTURA LUNEDI-VENERDI 9.00-12.00 15.00-18.30
    <br/>
    Contatto: Maria 010 542490
</p>
<p>
    Se avete la possibilita` di recarvi fisicamente presso l’agenzia, previo accordi telefonici con il nostro corrispondente Maria 010 542490 potrete
    consegnare e ritirare a mano i documenti a Genova.
</p>
<p>Qualora in fase di preventivo abbiate acquistato le spese di spedizione anche in andata, effettuate la spedizione in porto assegnato, contattando la TNT al n. 199.803.868 (per prenotare la presa della busta), e scrivendo sulla busta Porto Assegnato.</p>
<p>
    Il consolato lavora di lunedi, mercoledi e venerdi: se i documenti vengono presentati lunedi o mercoledi il visto e` pronto in 10 giorni lavorativi (con
    procedura urgente il visto si ottiene in 4 gg lavorativi), mentre se vengono presentati venerdi il primo giorno utile sara` considerato il lunedi
    successivo.
</p>




 

<p>
    <strong>Prima di effettuare il pagamento, avete accettato online le seguenti “Condizioni di fornitura del servizio”:</strong>
</p>
<p>
    Effettuando il pagamento, il Cliente affida all’Organizzatore ed alle sue agenzie corrispondenti in Italia la gestione dei documenti inerenti alla pratica
    richiesta visto, li autorizza al trattamento dei dati personali indicati per lo svolgimento del servizio richiesto, e li solleva da ogni responsabilità per
    eventuali rifiuti, ritardi, problemi e disguidi nell’ottenimento del visto, e all’ingresso nel paese richiedente visto, dovuti a cause non imputabili al
    loro operato, ossia:
</p>

- il passaporto non venga accettato dal Consolato perche` con scadenza inferiore a 6 mesi o perche` considerato non integro, usurato, non firmato;<br />- i dati personali del Cliente siano stati dallo stesso compilati in maniera errata al momento dell’invio della richiesta online sul sito web dell’Organizzatore;&nbsp;<br />- il visto venga rifiutato per qualsiasi motivo a discrezione del Consolato (diniego del visto, modulo elettronico richiesta visto compilato dal Cliente in maniera non corretta, documenti spediti dal Cliente in maniera errata o incompleta);<br />- ci siano ritardi nel rilascio del visto dovuti a impreviste chiusure al pubblico del Consolato o a improvvise modifiche dei tempi e delle modalita` di ottenimento del visto;<br />- si verifichino smarrimento dei documenti da parte dei corrieri postali, consegne ad indirizzi errati, o ritardi nella consegna imputabili agli stessi corrieri;<br />- il visto venga rilasciato con date di ingresso diverse da quelle richieste a causa di decisione o di errore del consolato;<br />- si verifichino problemi alla frontiera con il personale di dogana che implichino il rimpatrio.<br /><br />Presso il consolato di Milano, per visti superiori ai 14 giorni, e` generalmente richiesta la seguente documentazione aggiuntiva: prenotazioni alberghiere, fatture da parte degli hotel o dei sistemi di prenotazione, ricevute di pagamento dei servizi al 100%. Se non avete prenotato con noi l\'intero viaggio, o se non disponete autonomamente di tutta questa documentazione, consigliamo di non consegnare autonomamente i documenti all\'ufficio visti di Milano e di non selezionare Milano per la procedura completa. Se il Cliente decide comunque di presentare la domanda a Milano per visti superiori a 14 giorni in assenza delle condizioni necessarie, l\'Organizzatore non e` responsabile nel caso di diniego del visto, e non viene effettuta alcuna compensazione o riprotezione presso altri consolati.&nbsp;<br /><br />Per i minori non accompagnati da entrambe i genitori, e` richiesta la seguente documentazione aggiuntiva: certificato di nascita con i dati dei genitori, certificato di stato di famiglia, copia del documento d\'identita` del genitore o dei genitori che non accompagnano il minore, dichiarazione di assenso con firma autenticata dove si autorizza il viaggio del minore in sua/loro assenza.&nbsp;<br /><br />La pratica non sara` avviata fino a quando il pagamento non sara’ stato effettuato e confermato. Nel caso in cui a causa della presenza di festivita` italiane o russe non sia possibile ottenere il visto entro i termini standard, o nel caso in cui la posizione geografica del Cliente non consenta una ricezione dei documenti con corriere entro la data di partenza indicata, la transazione effettuata con carta di credito dal Cliente non sara` autorizzata e l’importo tornera` integralmente sulla carta il primo giorno lavorativo successivo al pagamento. Il Cliente se lo riterra` opportuno potra` in seguito effettuare un nuovo pagamento indicando nuove date o passando a procedura urgente o extraurgente, ove possibile. Nel caso di pagamento con bonifico bancario, il Cliente potra` procedere ad eventuale integrazione (passando a procedura urgente o extraurgente, ove possibile) o richiedere e ricevere il completo rimborso della somma pagata.<br /><br />In caso di pagamento con bonifico bancario, il versamento e` da effettuare in Euro e le commissioni bancarie debbono essere shared (SHA) e non a carico del beneficiario. Qualora l’importo ricevuto sia inferiore a quello preventivato, verra` richiesta al Cliente una integrazione. In caso di pagamento con carta di credito si applica una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli, in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi.&nbsp;<br /><br />In caso di procedura completa, il Cliente e` tenuto a controllare i documenti all’atto della consegna. Non verranno accettati reclami se non comunicati il giorno stesso della ricezione dei documenti. In caso di ritardi, errori o mancate concessioni del visto imputabili all’operato dell’organizzatore o delle sue agenzie corrispondenti in Italia, l’Organizzatore provvedera` esclusivamente al rimborso della quota visto e degli altri servizi collegati alla prenotazione effettuata dal Cliente con l’Organizzatore o suo intermediario , e di nessun altra spesa dovuta a servizi acquistati con terzi o a problemi personali collegabili al mancato viaggio.&nbsp;<br />
Se il Cliente ha cliccato NO nella sezione "Procedura completa" sul form online , per ottenere il visto dovra` recarsi personalmente presso gli uffici predisposti dai consolati russi di Roma, Milano, Genova e Palermo, previo appuntamento, una volta appurati i tempi e le modalita` di presentazione della domanda di visto. Non e` possibile entrare in territorio russo con la sola stampa della lettera d\'invito e dell\'assicurazione, va richiesto ed ottenuto il visto russo prima di effettuare il viaggio. La lettera d\'invito deve essere coperta da una prenotazione alberghiera, se e` stata effettuata una prenotazione autonomamente e` necessario indicare al nostro corrispondente l\'hotel prenotato e le date della prenotazione, tenendo presente che il consolato potrebbe verificare la prenotazione. A Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare autonomamente copia delle nostre assicurazioni presso gli uffici consolari di queste due citta`, ed e` necessario che il Cliente si procuri autonomamente una polizza assicurativa in originale tra quelle accettate dall\'ufficio consolare. A Genova e Palermo vengono accettate anche le copie, quindi e` possibile stampare e presentare la nostra polizza, che sara` spedita via mail in giornata, se il pagamento e` stato effettuato in orario lavorativo, o il primo giorno lavorativo successivo al pagamento.<br /><br />Qualsiasi variazione a quanto dichiarato dovrà essere concordata per iscritto (fax o e-mail). Per qualsiasi controversia sarà competente il Foro di San Pietroburgo. <br>
<p>
    Vi ringraziamo per la fiducia e vi auguriamo buon viaggio!
</p>
<p>
    Cordiali saluti,
    <br/>
    Lo Staff di Visto-Russia.com
</p>


'; ?>