<?php
//$lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
//$participants = JFactory::getApplication()->input->get('participants',null,'array');

$Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'ho_registrazione_piter' => 'Registrazione San Pietroburgo',
    'ho_registrazione_moscow' => 'Registrazione Mosca'
);
if (!$lastRecord['urgent']) unset($Servizi['urgent']);
if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
if (!$lastRecord['ho_registrazione_piter']) unset($Servizi['ho_registrazione_piter']);
if (!$lastRecord['ho_registrazione_moscow']) unset($Servizi['ho_registrazione_moscow']);

$ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
//////////////////////
$add = array(
    'add_hotel' => 'hotel',
    'add_appartamento' => 'appartamento',
    'add_volo_treno' => 'volo/treno',
    'add_transfers' => 'transfers',
    'add_escursioni' => 'escursioni'
);
if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
$addTXT = count($add)?implode(',',$add):'';
$user = JFactory::getUser(); 
$userProfile = JUserHelper::getProfile( $user->get('id') );
?>
<?php $subject = "Richiesta pratica visto (cliente non registrato) "; ?>
<?php $body = '
<table class="  -webkit-text-stroke-width: 0px;"
        width="970" align="center" border="0" cellpadding="0"
        cellspacing="0">
        <tbody>
          <tr align="center">
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><big><big><img shrinktofit="true"
                    alt=" "
                    data-cke-saved-src="https://www.visto-russia.com/images/logo3.png"
                    src="https://www.visto-russia.com/images/logo3.png"
                    style="cursor: default; width: 970px; height:
                    167px;"></big></big></td>
          </tr>
          <tr align="center">
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><br>
              <big><big>Vi ringraziamo per la richiesta! A seguire un
                  riepilogo dei dati inseriti:</big></big><br>
              <br>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211); text-align: right;" valign="top">
              <p><span style="font-size: 20px;"><strong></strong></span>Costo

                della praticaI ID № '.$lastRecord['id'].'</p>
              <p><span style="font-size: 20px;"><strong><big>Prezzo:'.$lastRecord['total_cost'].'
                      €</big></strong></span><br>
              </p>
            </td>
          </tr>
          <tr>
            <td style="border: 1px dotted rgb(211, 211, 211);"
              valign="top">
              <p>Data di arrivo in Russia:<span
                  class="Apple-converted-space"> </span><br>
              </p>
              <p><strong><big>'.date('d/m/Y',strtotime($lastRecord['date_from'])).'</big></strong><br>
                <br>
                <br>
              </p>
            </td>
            <td style="border: 1px dotted rgb(211, 211, 211);"
              valign="top">
              <p>Data di partenza dalla Russia<span
                  class="Apple-converted-space"> </span><br>
              </p>
              <p><strong><big>'.date('d/m/Y',strtotime($lastRecord['date_to'])).'</big></strong><br>
              </p>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><big><strong>Partecipanti al
                    viaggio</strong></big></em></td>
          </tr>';
    if (count($participants)) :
        foreach($participants as $participant) :
            $gender = ($participant['gender']=='f')?'female':'male';

            $body .= '<tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>Nome: <strong>'.$participant['first_name'].'</strong> Cognome:<strong>'.$participant['second_name'].'</strong> Data

                di nascita: <strong>'.date('d/m/Y',strtotime($participant['birthdate'])).' </strong> Sesso: <strong>'.$gender.'</strong></div>
              <div>Nazionalita<strong> '.$participant['nationality'].'</strong> Numero di
                passaporto <strong>'.$participant['passport'].'</strong></div>
            </td>
          </tr>';
        endforeach;
    endif;
        $body .= '<tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Riepilogo servizi
                    richiesti:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>Procedura completa</div>
              <div>Consolato: '.$lastRecord['consulate'].'.</div>
              <div>Assicurazione: '.($lastRecord['insurance']?'Inclusa':'Non Inclusa').'</div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Citta` da visitare:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">'.$lastRecord['cities_to_visit'].'</td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Servizi aggiuntivi:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">
              <div>'.$ServiziTXT.'</div>
              <div>Spedizione: '.$lastRecord['shipping'].'</div>
              <div>Ho bisogno anche di: '.$addTXT.'</div>
            </td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top"><em><strong><big>Commenti:</big></strong></em></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">'.$lastRecord['add_other'].'</td>
          </tr>
                    <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Nome e cognome: '.$userName.'</td>
          </tr>
		  <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Mail: <a moz-do-not-send="true"
                class="moz-txt-link-abbreviated"
                href="'.$userEmail.'">'.$userEmail.'</a></td>
          </tr>
          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">Telefono:+  '.$userPhone.' </td>
          </tr>

          <tr>
            <td colspan="2" style="border: 1px dotted rgb(211, 211,
              211);" valign="top">

			  
			  <p>
			  Il nostro operatore a breve verificherà i dettagli della vostra pratica; se i dati sono stati inseriti correttamente e siamo nei tempi utili per evaderla, riceverete una mail con le istruzioni per il pagamento e con il dettaglio della documentazione da preparare e l\'indirizzo al quale spedire o consegnare i documenti. In caso contrario sarete contattati via mail o telefonicamente per chiarimenti. In caso di necessita`, potete contattarci via mail all’indirizzo visto@russiantour.com o telefonicamente al numero verde gratuito 800 404000. </p>

<br><br>Cordiali saluti, <br><br>
Lo Staff di Visto-Russia.com

</td>
          </tr>
        </tbody>
      </table>

'; ?>