<?php $subject = "Dati per la compilazione del modulo consolare"; ?>
<?php $body = 'Abbiamo rievuto i vostri dati per la compilazione del modulo consolare, dettaglio a seguire:<br> Luogo di nascita: '
.$modulo->luogo_di_nascita.'<br>Data di rilascio del passaporto e scadenza: '  
.$modulo->data_di_rilascio.'<br>Precedenti viaggi in Russia: ' 
.$modulo->precedenti_viaggi.'<br>Indirizzo di residenza: ' 
.$modulo->indirizzo_di_residenza.'<br>Telefono: ' 
.$modulo->numero_di_telefono.'<br>Attuale posto di lavoro e di studio: ' 
.$modulo->attuale_posto.'<br>Bambini sotto i 16 anni che viaggiano con voi: ' 
.$modulo->bambini_sotto.'<br>Parenti attualmente in Russia: ' 
.$modulo->parenti_attualmente.'<br><br>Vi ringraziamo per la fiducia e vi auguriamo buon viaggio!<br>

Cordiali saluti, <br>
Lo Staff di Visto-Russia.com
'
; 
?>