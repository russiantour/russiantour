<?php

/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of Touristinvite.
 */
class TouristinviteViewInvite extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $params;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $items_per_page = 5;
        $user = JFactory::getUser()->get('id');
        if ($user)
        {
            $app = JFactory::getApplication();

            $this->state = $this->get('State');
            //$this->items = $this->get('Items');
            $db = JFactory::getDbo();

            $query = 'SELECT * FROM #__touristinvite_hotels'
                . ' ORDER BY hotel_city, hotel_name';

            $db->setQuery($query);
            $this->items = $db->loadObjectList();

            $this->cities=array();
            $cities=array();

            foreach ($this->items as $item)
            {
                if ($item->hotel_city && !in_array($item->hotel_city,$cities))
                {
                    $cities[]=$item->hotel_city;
                    $this->cities[]=$item;
                }
            }

            $this->pagination = $this->get('Pagination');
            $this->params = $app->getParams('com_touristinvite');


            // Check for errors.
            if (count($errors = $this->get('Errors'))) {
    ;
                throw new Exception(implode("\n", $errors));
            }

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('tf.turfirm_balance, tf.turfirm_cost, u.name')
                ->from('#__touristinvite_turfirms as tf, #__users as u')
                ->where('tf.turfirm_user_id = ' . $user.' and u.id=tf.turfirm_user_id');
            $db->setQuery($query);
            $db = $db->loadObject();

            $this->turfirm_balance = $db->turfirm_balance;
            $this->turfirm_cost = $db->turfirm_cost;
            $this->turfirm_name = $db->name;

            $uri = JURI::getInstance();
            $sortVar = $uri->getVar('sortVar')?$uri->getVar('sortVar'):'id';
            $sortDir = $uri->getVar('sortDir')?$uri->getVar('sortDir'):'desc';

            $pageNum = intval($uri->getVar('page'))?intval($uri->getVar('page')):1;

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('*')
                ->from('#__touristinvite_invites')
                ->where('created_by = '.$user)
                ->order("`".$sortVar."` ".$sortDir." LIMIT ".($pageNum-1)*$items_per_page.",".$items_per_page);
            $db->setQuery($query);
            $this->invites = $db->loadObjectList();

            $db = JFactory::getDbo();
            $query = $db->getQuery(true);
            $query
                ->select('count(id) as count')
                ->from('#__touristinvite_invites')
                ->where('created_by = '.$user);
            $db->setQuery($query);
            $count = $db->loadResult();
            $this->pagesCount = ceil($count/$items_per_page);

            if (isset($_GET['inviteid']))
            {
                $db = JFactory::getDbo();
                $query = $db->getQuery(true);
                $query
                    ->select('*')
                    ->from('#__touristinvite_invites')
                    ->where(array(
                        'created_by = '.$user,
                        'id = '.intval($_GET['inviteid'])
                    ));
                $db->setQuery($query);
                $this->editinvite = $db->loadObject();
                $this->editinvite = unserialize($this->editinvite->invite_data);

            }

            $this->_prepareDocument();
            parent::display($tpl);
        }
        else
echo '' ;


    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $title = null;

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        if ($menu) {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else {
            $this->params->def('page_heading', JText::_('COM_TOURISTINVITE_DEFAULT_PAGE_TITLE'));
        }
        $title = $this->params->get('page_title', '');
        if (empty($title)) {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        } elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
            $title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
        }
        $this->document->setTitle($title);

        if ($this->params->get('menu-meta_description')) {
            $this->document->setDescription($this->params->get('menu-meta_description'));
        }

        if ($this->params->get('menu-meta_keywords')) {
            $this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
        }

        if ($this->params->get('robots')) {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }
    }

}