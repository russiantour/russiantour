<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->state->get('list.ordering');
$listDirn = $this->state->get('list.direction');
$canCreate = $user->authorise('core.create', 'com_touristinvite');
$canEdit = $user->authorise('core.edit', 'com_touristinvite');
$canCheckin = $user->authorise('core.manage', 'com_touristinvite');
$canChange = $user->authorise('core.edit.state', 'com_touristinvite');
$canDelete = $user->authorise('core.delete', 'com_touristinvite');

?>
<h1><? echo $this->turfirm_name; ?></h1><br>
<? echo JText::_('COM_TOURISTINVITE_INVITE_YOUR_BALANCE'); ?> <strong><? echo $this->turfirm_balance; ?></strong><br>
<? echo JText::_('COM_TOURISTINVITE_INVITE_INVITE_COST'); ?> <strong><? echo $this->turfirm_cost; ?></strong><br>



<form method="post" id="inviteform" action="<? echo JFactory::getURI()->getPath().'/save'; ?>">
    <div id="inviteerror"></div>
    <? if ($_GET['inviteid']) { ?><input type="hidden" name="inviteid" value="<? echo $_GET['inviteid']; ?>"><? } ?>



    <? echo JText::_('COM_TOURISTINVITE_INVITE_FROM'); ?>
    <?php
    if (isset($this->editinvite))
        $val = str_replace('/','.',$this->editinvite['invite_from']);
    else
        $val = date('d.m.Y');
    echo JHtml::_('calendar', $val, 'invite_from', 'invite_from', '%d/%m/%Y', array());?>
    <!--input type="text" name="invite_from"-->
    <br>

    <? echo JText::_('COM_TOURISTINVITE_INVITE_TO'); ?><?php
    if (isset($this->editinvite))
        $val = str_replace('/','.',$this->editinvite['invite_to']);
    else
        $val = date('d.m.Y');
    echo JHtml::_('calendar', $val, 'invite_to', 'invite_to', '%d/%m/%Y', array());?>
    <!--input type="text" name="invite_to"-->
    <br>

    <? echo JText::_('COM_TOURISTINVITE_CITIES'); ?>
    <select id="invite_cities" name="invite_cities[]" multiple data-placeholder="<? echo JText::_('COM_TOURISTINVITE_CHOOSE_CITIES'); ?>">
        <?php foreach ($this->cities as $i => $item) :
            $invite_cities_option = $item->hotel_city.'|'.$item->hotel_city_rus;
            if (isset($this->editinvite) && in_array($invite_cities_option,$this->editinvite['invite_cities']))
                $sel = 'selected';
            else
                $sel = '';
            ?>
            <option  value='<? echo $invite_cities_option; ?>' <? echo $sel; ?>><? echo $item->hotel_city; ?></option>
        <?php endforeach; ?>
    </select>
    <br>

    <? echo JText::_('COM_TOURISTINVITE_TITLE_HOTELS'); ?>
    <select id="invite_hotels" name="invite_hotels[]" multiple data-placeholder="<? echo JText::_('COM_TOURISTINVITE_CHOOSE_HOTELS'); ?>">
    <?php foreach ($this->items as $i => $item) :
        $invite_hotels_option = $item->hotel_name.'|'.$item->hotel_name_rus.'|'.$item->hotel_city.'|'.$item->hotel_city_rus;
        if (isset($this->editinvite) && in_array($invite_hotels_option,$this->editinvite['invite_hotels']))
            $sel = 'selected';
        else
            $sel = '';
        ?>
        <option disabled id="<?php echo $item->hotel_city.'|'.$item->hotel_city_rus; ?>" value='<? echo $invite_hotels_option; ?>' <? echo $sel; ?>><? echo $item->hotel_name.' ('.$item->hotel_city.')'; ?></option>
    <?php endforeach; ?>
    </select>
    <br>


    <script>
        function checkhotels(){
            jQuery("#invite_hotels option").attr('disabled',true);
            jQuery('#invite_cities').find('option:selected').each(function(){
                var city_value = jQuery(this).val();
                jQuery('[id="'+city_value+'"]:disabled').attr('disabled',false);
            });
            jQuery('#invite_hotels').find('option:selected').each(function(){
                var id = jQuery(this).attr('id');
                jQuery('#invite_hotels option[id="'+id+'"]:not(:selected)').attr('disabled',true);
            });
            jQuery("#invite_hotels").trigger("liszt:updated");
        }
        checkhotels();
        jQuery('#invite_cities').on('change',checkhotels);
        jQuery('#invite_hotels').on('change',checkhotels);
    </script><span>
    <? echo JText::_('COM_TOURISTINVITE_INVITE_PEOPLE_NUMBER');
    if (isset($this->editinvite))
        $val = $this->editinvite['people_count'];
    else
        $val = '';
    ?></span>
    <input type="text" style="width: 20px;"  name="people_count" value="<? echo $val; ?>"> <a id="apply"><button class="but"><? echo JText::_('COM_TOURISTINVITE_APPLY'); ?></button></a>
    <br>

    <table <? if (!isset($this->editinvite)) { ?>style="display: none;"<? } ?> id="guests">
        <tr>
            <td><? echo JText::_('COM_TOURISTINVITE_GUESTS_FIRST_NAME'); ?></td>
            <td><? echo JText::_('COM_TOURISTINVITE_GUESTS_LAST_NAME'); ?></td>
            <td><? echo JText::_('COM_TOURISTINVITE_GUESTS_BIRTHDATE'); ?></td>
            <td><? echo JText::_('COM_TOURISTINVITE_GUESTS_CITIZENSHIP'); ?></td>
            <td><? echo JText::_('COM_TOURISTINVITE_GUESTS_SEX'); ?></td>
            <td><? echo JText::_('COM_TOURISTINVITE_GUESTS_PASPORT'); ?></td>
        </tr>
        <? if (isset($this->editinvite)) { ?>
            <? for ($i=1;$i<=$this->editinvite['people_count'];$i++) {
                ?><tr>
                <td><input type="text" onchange="nodigits(this)" name="first_name_<? echo $i; ?>" value="<? echo $this->editinvite['first_name_'.$i]; ?>"></td>
                <td><input type="text" onchange="nodigits(this)" name="last_name_<? echo $i; ?>" value="<? echo $this->editinvite['last_name_'.$i]; ?>"></td>
                <td>
                    <? echo JHtml::_('calendar', str_replace('/','.',$this->editinvite['birthdate_'.$i]), "birthdate_".$i, "birthdate_".$i, '%d/%m/%Y', array()); ?>
                </td>
                <td><input type="text" name="gragd_<? echo $i; ?>" value="<? echo $this->editinvite['gragd_'.$i]; ?>"></td>
                <td>
                    <select name="sex_<? echo $i; ?>">
                        <option <? if ($this->editinvite['sex_'.$i]=='m') echo 'selected'; ?> value="m"><? echo JText::_('COM_TOURISTINVITE_SEX_MALE'); ?></option>
                        <option <? if ($this->editinvite['sex_'.$i]=='f') echo 'selected'; ?> value="f"><? echo JText::_('COM_TOURISTINVITE_SEX_FEMALE'); ?></option>
                    </select>
                </td>
                <td><input type="text" name="passport_<? echo $i; ?>" value="<? echo $this->editinvite['passport_'.$i]; ?>"></td>
                </tr><?
            } ?>
        <? } ?>
    </table>
    <br>

    <input type="submit" value="<? echo JText::_('COM_TOURISTINVITE_SEND'); ?>">
</form>
<? $uri = JUri::getInstance(); ?>
<div style="display: none;" id="birthdate_template">
    <? echo JHtml::_('calendar', date('d.m.Y'), "birthdate_[id]", "birthdate_[id]", '%d/%m/%Y', array()); ?>
</div>
<?
function checkSortArrow($uri,$name){
    $sortVar = $uri->getVar('sortVar');
    if (!$sortVar)
        $sortVar = 'id';
    if ($sortVar==$name)
    {
        $sortDir = $uri->getVar('sortDir')?$uri->getVar('sortDir'):'desc';
        if ($sortDir=='asc')
            echo '&#9660;';
        else
            echo '&#9650;';
    }
}
?>
<table  class="bordered">
    <tr>
        <th>
            <a href="<? echo $uri->getPath();?>?<? echo $uri->buildQuery(array(
                'sortVar'=>'id',
                'sortDir'=>($uri->getVar('sortDir')=='asc')?'desc':'asc',
                'page'=>$uri->getVar('page'),
                'inviteid'=>$uri->getVar('inviteid'))); ?>"><? echo JText::_('COM_TOURISTINVITE_INVITE_ID'); ?> <? checkSortArrow($uri,'id'); ?></a></th>
        <th><a href="<? echo $uri->getPath();?>?<? echo $uri->buildQuery(array(
                'sortVar'=>'pdf_id',
                'sortDir'=>($uri->getVar('sortDir')=='asc')?'desc':'asc',
                'page'=>$uri->getVar('page'),
                'inviteid'=>$uri->getVar('inviteid'))); ?>"><? echo JText::_('COM_TOURISTINVITE_INVITE_PDF_ID'); ?> <? checkSortArrow($uri,'pdf_id'); ?></a></th>
        <th><? echo JText::_('COM_TOURISTINVITE_DATE_OF_ISSUE'); ?></th>
        <th><? echo JText::_('COM_TOURISTINVITE_CLIENT_INFORMATION'); ?></th>
        <th><? echo JText::_('COM_TOURISTINVITE_CITIES_AND_HOTELS'); ?></th>
        <th style="text-align: center" ><a href="<? echo $uri->getPath();?>?<? echo $uri->buildQuery(array(
                'sortVar'=>'invite_from',
                'sortDir'=>($uri->getVar('sortDir')=='asc')?'desc':'asc',
                'page'=>$uri->getVar('page'),
                'inviteid'=>$uri->getVar('inviteid'))); ?>"><? echo JText::_('COM_TOURISTINVITE_FROM'); ?> <? checkSortArrow($uri,'invite_from'); ?></a></th>
        <th style="text-align: center"><a href="<? echo $uri->getPath();?>?<? echo $uri->buildQuery(array(
                'sortVar'=>'invite_to',
                'sortDir'=>($uri->getVar('sortDir')=='asc')?'desc':'asc',
                'page'=>$uri->getVar('page'),
                'inviteid'=>$uri->getVar('inviteid'))); ?>"><? echo JText::_('COM_TOURISTINVITE_TO'); ?> <? checkSortArrow($uri,'invite_to'); ?></a></th>
        <th><? echo JText::_('COM_TOURISTINVITE_PRICE'); ?></th>
        <th><? echo JText::_('COM_TOURISTINVITE_STATUS'); ?></th>
    </tr>
<?
$uri = JURI::getInstance();
$url = $uri->toString();

preg_match('/.*\/invite/', $url, $theurl);
foreach ($this->invites as $inv) {
    $invdata = unserialize($inv->invite_data);?>
    <tr>
        <td><? echo $inv->id; ?></td>
        <td style="text-align: center"><? $pdfid = $inv->pdf_id?$inv->pdf_id:'-'; echo $pdfid ?></td>
        <td><? echo date('m/d/Y',strtotime($inv->invite_created)); ?></td>
        <td><? for ($i=1;$i<=$invdata['people_count'];$i++) {
            echo $invdata['last_name_'.$i].' '.$invdata['first_name_'.$i].'<br> passport '.$invdata['passport_'.$i].'<br> Birthday '.$invdata['birthdate_'.$i].';<hr>';
        } ?></td>
        <td><? foreach ($invdata['invite_hotels'] as $hotl) echo $hotl.';<br>'; ?></td>
        <td><? $inv->invite_from = explode(' ',$inv->invite_from); $inv->invite_from = explode('-',$inv->invite_from[0]); echo $inv->invite_from[2].'/'.$inv->invite_from[1].'/'.$inv->invite_from[0]; ?></td>
        <td><? $inv->invite_to = explode(' ',$inv->invite_to); $inv->invite_to = explode('-',$inv->invite_to[0]); echo $inv->invite_to[2].'/'.$inv->invite_to[1].'/'.$inv->invite_to[0]; ?></td>
        <td style="text-align: center" ><? echo $invdata['cost']; ?></td>
        <td>
            <?
                if (isset($invdata['validate_date']))
                    echo $invdata['validate_date'].'<br><a target="_blank" href="'.$theurl[0].'/showpdf?inviteid='.$inv->id.'">'.JText::_('COM_TOURISTINVITE_INVITE_PRINT').'</a>';
                else
                    echo '<a target="_blank" href="'.$theurl[0].'/showpdf?inviteid='.$inv->id.'">'.JText::_('COM_TOURISTINVITE_INVITE_DEMO_PRINT').'</a><br>'.
                    '<a href="'.$theurl[0].'/validateinvite?inviteid='.$inv->id.'" onclick="return confirm(\'Do you really want to validate?\')">'.JText::_('COM_TOURISTINVITE_INVITE_VALIDATE').' </a><br>'.
                    '<a href="'.$uri->getPath().'?'.$uri->buildQuery(array(
                'sortVar'=>$uri->getVar('sortVar'),
                'sortDir'=>$uri->getVar('sortDir'),
                'page'=>$uri->getVar('page'),
                'inviteid'=>$inv->id)).'">'.JText::_('COM_TOURISTINVITE_INVITE_EDIT').'</a><br>'.
                    '<a href="'.$theurl[0].'/delinvite?inviteid='.$inv->id.'" onclick="return confirm(\'Do you really want to delete?\')">'.JText::_('COM_TOURISTINVITE_INVITE_DELETE').'</a>';
                ?>
            <div id="invdata_<? echo $inv->id; ?>" style="display: none;">
            <?
            foreach ($invdata as $k=>$v)
            {
                if (gettype($v)=='array')
                    foreach ($v as $k1=>$v1)
                        echo '<div id="'.$k.'">'.$v1.'</div>';
                else
                    echo '<div id="'.$k.'">'.$v.'</div>';
            }
            ?>
            </div>
        </td>
    </tr>
<? } ?>
</table>
<div class="pagination">
    <ul>
    <?
    $curr_page = $uri->getVar('page');
    if ($curr_page>5)
    {
        $p_from = $curr_page-5;
        echo '<li><a href="'.$uri->getPath().'?'.$uri->buildQuery(array(
                'sortVar'=>$uri->getVar('sortVar'),
                'sortDir'=>$uri->getVar('sortDir'),
                'page'=>$p_from,
                'inviteid'=>$uri->getVar('inviteid'))).'">&lt;&lt;</a></li>';
    }
    else
        $p_from = 1;

    if ($curr_page<$this->pagesCount-5)
        $p_to = $curr_page+5;
    else
        $p_to = $this->pagesCount;

    for ($i=$p_from;$i<=$p_to;$i++) {
        if (($uri->getVar('page') && $uri->getVar('page')==$i) || (!$uri->getVar('page') && $i==1))
            $myPageSel = 'class="active"';
        else
            $myPageSel = '';
        echo '<li '.$myPageSel.'><a href="'.$uri->getPath().'?'.$uri->buildQuery(array(
                'sortVar'=>$uri->getVar('sortVar'),
                'sortDir'=>$uri->getVar('sortDir'),
                'page'=>$i,
                'inviteid'=>$uri->getVar('inviteid'))).'">'.$i.'</a></li>';
    }
    if ($curr_page<$this->pagesCount-5) {
        echo '<li><a href="'.$uri->getPath().'?'.$uri->buildQuery(array(
                'sortVar'=>$uri->getVar('sortVar'),
                'sortDir'=>$uri->getVar('sortDir'),
                'page'=>$p_to+1,
                'inviteid'=>$uri->getVar('inviteid'))).'">&gt;&gt;</a></li>';
    }
    ?>
    </ul>
</div>
<? //div class="download-pdf">PDF</div?>
<script>
    var tr='<tr><td><input onchange="nodigits(this)" type="text" name="first_name_[id]"></td>';
    tr += '<td><input onchange="nodigits(this)" type="text" name="last_name_[id]"></td>';
    tr += '<td>'+jQuery('#birthdate_template').html()+'</td>';

    tr += '<td><select name="gragd_[id]"><option value="0" selected="selected"></option>';
    tr += '<option value="Италия">Italy</option>';
    tr += '<option value="США">USA</option>';
    tr += '<option value="Великобритания">Great Britain</option>';
    tr += '<option value="Испания">Spain</option>';
    tr += '<option value="Франция">France</option>';
    tr += '<option value="Германия">Germany</option>';
    tr += '<option value="Чешская Республика">Czech Republic</option>';
    tr += '<option value="Польша">Poland</option>';
    tr += '<option value="Австрия">Austria</option>';
    tr += '<option value="Швейцария">Switzerland</option>';
    tr += '<option value="Сан-Марино">San Marino</option>';
    tr += '<option value="Канада">Canada</option>';
    tr += '<option value="Андорра">Andorra</option>';
    tr += '<option value="Аргентина">Argentina</option>';
    tr += '<option value="Австралия">Australia</option>';
    tr += '<option value="Барбадос">Barbados</option>';
    tr += '<option value="Бельгия">Belgium</option>';
    tr += '<option value="Боливия">Bolivia</option>';
    tr += '<option value="Босния и Герцеговина">Bosnia and Herzegovina</option>';
    tr += '<option value="Бразилия">Brazil</option>';
    tr += '<option value="Болгария">Bulgaria</option>';
    tr += '<option value="Чили">Chile</option>';
    tr += '<option value="Колумбия">Colombia</option>';
    tr += '<option value="Коста-Рика">Costa Rica</option>';
    tr += '<option value="Хорватия">Croatia</option>';
    tr += '<option value="Куба">Cuba</option>';
    tr += '<option value="Кипр">Cyprus</option>';
    tr += '<option value="Дания">Denmark</option>';
    tr += '<option value="Доминиканская Республика">Dominican Republic</option>';
    tr += '<option value="Эквадор">Ecuador</option>';
    tr += '<option value="Эстония">Estonia</option>';
    tr += '<option value="Острова Фиджи">Fiji Islands</option>';
    tr += '<option value="Финляндия">Finland</option>';
    tr += '<option value="Греция">Greece</option>';
    tr += '<option value="Гватемала">Guatemala</option>';
    tr += '<option value="Гаити">Haiti</option>';
    tr += '<option value="Гондурас">Honduras</option>';
    tr += '<option value="Венгрия">Hungary</option>';
    tr += '<option value="Исландия">Iceland</option>';
    tr += '<option value="Индонезия">Indonesia</option>';
    tr += '<option value="Ирландия">Ireland</option>';
    tr += '<option value="Ямайка">Jamaica</option>';
    tr += '<option value="Япония">Japan</option>';
    tr += '<option value="Латвия">Latvia</option>';
    tr += '<option value="Лихтенштейн">Liechtenstein</option>';
    tr += '<option value="Литва">Lithuania</option>';
    tr += '<option value="Люксембург">Luxemburg</option>';
    tr += '<option value="Люксембург">Macedonia</option>';
    tr += '<option value="Мальдивы">Maldives</option>';
    tr += '<option value="Мальта">Malta</option>';
    tr += '<option value="Мексика">Mexico</option>';
    tr += '<option value="Монако">Monaco</option>';
    tr += '<option value="Новая Зеландия">New Zealand</option>';
    tr += '<option value="Никарагуа">Nicaragua</option>';
    tr += '<option value="Норвегия">Norway</option>';
    tr += '<option value="Панама">Panama</option>';
    tr += '<option value="Парагвай">Paraguay</option>';
    tr += '<option value="Перу">Peru</option>';
    tr += '<option value="Португалия">Portugal</option>';
    tr += '<option value="Пуэрто-Рико">Puerto Rico</option>';
    tr += '<option value="Румыния">Rumania</option>';
    tr += '<option value="Сальвадор">Salvador</option>';
    tr += '<option value="Сербия и Черногория">Serbia and Montenegro</option>';
    tr += '<option value="Словакия">Slovakia</option>';
    tr += '<option value="Словения">Slovenia</option>';
    tr += '<option value="Южная Корея">South Korea</option>';
    tr += '<option value="Швеция">Sweden</option>';
    tr += '<option value="Нидерланды">the Netherlands</option>';
    tr += '<option value="Уругвай">Uruguay</option>';
    tr += '<option value="Венесуэла">Venezuela</option></select></td>';

    tr += '<td><select name="sex_[id]"><option value="m"><? echo JText::_('COM_TOURISTINVITE_SEX_MALE'); ?></option>';
    tr += '<option value="f"><? echo JText::_('COM_TOURISTINVITE_SEX_FEMALE'); ?></option></select></td>';

    tr += '<td><input type="text" name="passport_[id]"></td></tr>';

    jQuery('#apply').on('click',function(){
        jQuery('table#guests').show();
        var length = jQuery('table#guests tr').length;
        var int = parseInt(jQuery('input[name="people_count"]').val());
        if (int > 10) {
            alert('Not more than 10');
            int=10;
            jQuery('input[name="people_count"]').val(int);
        }

        if (length > (int+1))
            for (i=length-1;i>int;i--)
                jQuery('table#guests tr:eq('+i+')').remove();
        else if (length-1 < int)
            for(i=length;i<int+1;i++)
            {
                var ntr = tr.replace(/\[id\]/g,i);
                jQuery('table#guests').append(ntr);

                Calendar.setup({
                    // Id of the input field
                    inputField: "birthdate_"+i,
                    // Format of the input field
                    ifFormat: "%d/%m/%Y",
                    // Trigger for the calendar (button ID)
                    button: "birthdate_"+i+"_img",
                    // Alignment (defaults to "Bl")
                    align: "Tl",
                    singleClick: true,
                    firstDay: 0
                });
            }
    });

    jQuery('form#inviteform').on('submit',function(){
        jQuery('#inviteerror').html('');
        var flag = true;
        jQuery('input[type="text"][name],select').each(function(){
            if (!jQuery(this).val())
                flag = false;
        });


        var today = new Date();
        var today_start = new Date(today.getFullYear(),today.getMonth(),today.getDate());
        var fromval = jQuery('#invite_from').val().split('/');
        var toval = jQuery('#invite_to').val().split('/');

        jQuery('input[id*="birthdate_"]').each(function(){
            var birth = jQuery(this).val().split('/');
            if (!jQuery(this).val().test(/^\d{2}\/\d{2}\/\d{4}$/))
            {
                jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Birthday must be in dd/mm/yyyy<br>');
                flag = false;
            }
            if (Date.parse(birth[1]+'/'+birth[0]+'/'+birth[2]) > Date.parse(today_start))
            {
                jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Birthday can not be in future<br>');
                flag = false;
            }
            if (Date.parse(birth[1]+'/'+birth[0]+'/'+birth[2]) < Date.parse('01/01/1900'))
            {
                jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Year of birth can not be less then 1900<br>');
                flag = false;
            }
        })

        if (Date.parse(fromval[1]+'/'+fromval[0]+'/'+fromval[2]) < Date.parse(today_start))
        {
            jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Check-in can not be early then today<br>');
            flag = false;
        }

        if (Date.parse(toval[1]+'/'+toval[0]+'/'+toval[2]) < Date.parse(fromval[1]+'/'+fromval[0]+'/'+fromval[2])+86400000)
        {
            jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Check out must be at least next day after check-in<br>');
            flag = false;
        }

        if (Date.parse(toval[1]+'/'+toval[0]+'/'+toval[2]) > Date.parse(fromval[1]+'/'+fromval[0]+'/'+fromval[2])+86400000*30)
        {
            jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Check out must be not more than 30 days after check-in<br>');
            flag = false;
        }

        if (!flag)
            jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'All fields are required<br>');
        else if (jQuery('input[name="first_name_'+parseInt(jQuery('input[name="people_count"]').val())+'"]').length<1){
            flag = false;
            jQuery('#inviteerror').html(jQuery('#inviteerror').html()+'Apply people number and enter their data<br>');
        }
        else
        {
            jQuery('#inviteerror').html('');
            return true;
        }

        return false;
    });
    function nodigits(t){t.value=t.value.replace(/\d+/g,'')};
    jQuery("#invite_hotels").chosen({max_selected_options: 5});

</script>
<style>
    #inviteform #guests input, #inviteform #guests select {
        width: 100px;
    }

    .disabled-result {
        display: none !important;
    }

    .active-result {
        text-transform: capitalize;
    }

    #inviteerror {
        color: red;
    }
</style>