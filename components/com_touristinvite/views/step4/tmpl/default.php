<?php
if ($_GET['test']) {echo 12;exit;}
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_touristinvite', JPATH_ADMINISTRATOR);
$params = JComponentHelper::getParams('com_touristinvite')->get('params');
//$params = (isset($this->state->params)) ? $this->state->params : new JObject;
$user = JFactory::getUser();
if (isset($_GET['manual']) && intval($_GET['manual'])){
    $query = 'SELECT *  FROM #__touristinvite_manualpayments WHERE '.
        'id = '.intval($_GET['manual'])
        .' LIMIT 1' ;
    $db = JFactory::getDBO();
    $db->setQuery($query);
    $manualPayment = $db->loadAssoc();
    include('./components/com_touristinvite/views/step4/substeps/manual.php');
}
elseif ($user->get('id')){
    $lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
    $participants = JFactory::getApplication()->input->get('participants',null,'array');
    //var_dump($lastRecord['shipping']);


    if ($lastRecord['consulate']=='No')
    {
        include('./components/com_touristinvite/views/mails/payed/no.php');
        include('./components/com_touristinvite/views/step4/substeps/no.php');
    }
        //include('./components/com_touristinvite/views/step4/substeps/pdf.php');
    /*elseif ($lastRecord['consulate']!='No' && $lastRecord['shipping']=='Non includere')
        include('./components/com_touristinvite/views/step4/substeps/no.php');*/
    elseif ($lastRecord['consulate']=='Genova')
    {
        include('./components/com_touristinvite/views/mails/payed/genovo.php');
        include('./components/com_touristinvite/views/step4/substeps/genovo.php');
    }
    elseif ($lastRecord['consulate']=='Milano')
    {
        include('./components/com_touristinvite/views/mails/payed/milano.php');
        include('./components/com_touristinvite/views/step4/substeps/milano.php');
    }
    elseif ($lastRecord['consulate']=='Palermo')
    {
        include('./components/com_touristinvite/views/mails/payed/palermo.php');
        include('./components/com_touristinvite/views/step4/substeps/palermo.php');
    }
    elseif ($lastRecord['consulate']=='Roma')
    {
        include('./components/com_touristinvite/views/mails/payed/roma.php');
        include('./components/com_touristinvite/views/step4/substeps/roma.php');
    }



    else {


?>
<?php
$step2 = 'uk-badge-success';
$step3 = '';
$step4 = '';
?>
<br>
Вы оплатили!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
<br>


<ul id="tab-content" class="tm-tab-content uk-switcher">
<li class="uk-active"> 

		<div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Visto turistico russia</div>
<br>
		<div class="uk-grid uk-grid-collapse"> 
 
	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">1</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Compilazione dati</span> </div>

	   
	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">2</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Controllo</span> </div>
	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">3</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Pagamento</span> </div>
		
		<div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-success">4</span> <span class="uk-width-8-10 uk-badge uk-badge-success"><i class="uk-icon-spinner uk-icon-spin"></i>Invio documenti</span> </div>

   </div>    
<br>
    <div class="uk-grid">
        <div class="uk-width-medium-7-10">
		<div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Riepilogo dati </div>
                  <h4 class="tm-article-subtitle">Date del viaggio</h4>
				<div class="uk-grid">

				
				<div class="uk-width-1-2">Data di arrivo in Russia
                    <div class="uk-text-large">
                        <?php echo $lastRecord['date_from']; ?>
                    </div>
                          
                    
                </div>
                <div class="uk-width-1-2">Data di partenza dalla Russia
                    <div class="uk-text-large"><?php echo $lastRecord['date_to']; ?></div>
                </div>
            </div>
				
			
		<div>
		  <h4 class="tm-article-subtitle">Partecipanti al viaggio</h4>

		</div>
            <?php if (count($participants)) : ?>
            <?php foreach($participants as $participant) :
                    $gender = ($participant['gender']=='f')?'female':'male';
                    ?>
        <article class="uk-comment">
            <header class="uk-comment-header">
                <img class="uk-comment-avatar" src="/images/yootheme/uikit_avatar.svg" width="50" height="50" alt="">
                <h4 class="uk-comment-title">Nome: <b><?php echo $participant['first_name']; ?></b> Cognome:<b><?php echo $participant['second_name']; ?> </b> Data di nascita: <b> <?php echo $participant['birthdate']; ?></b> Sesso: <b><?php echo $gender; ?></b> <br>
                 Nazionalita <b><?php echo $participant['nationality']; ?></b> Numero di passaporto <b><?php echo $participant['passport']; ?></b></h4>

            </header>

        </article>
            <?php endforeach; ?>
            <?php endif; ?>

                                </article>
		</div>


		
           
        
        <div class="uk-width-medium-3-10">
		<div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Costo della pratica</div>
         <h4 class=" uk-text-center margin0">ID: <b><?php echo $lastRecord['id']; ?></b></h4>
            <div class="uk-text-center uk-text-large ">Prezzo<br>
                <span ><?php echo $lastRecord['total_cost']; ?></span> €
            </div>
           
        </div>
		
    </div>



 <?php

 $Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'rus_visa' => 'Registrazione del visto russo'
 );
 if (!$lastRecord['urgent']) unset($Servizi['urgent']);
 if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
 if (!$lastRecord['rus_visa']) unset($Servizi['rus_visa']);
    $ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
 //////////////////////
 $add = array(
     'add_hotel' => 'hotel',
     'add_appartamento' => 'appartamento',
     'add_volo_treno' => 'volo/treno',
     'add_transfers' => 'transfers',
     'add_escursioni' => 'escursioni'
 );
 if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
 if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
 if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
 if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
 if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
 $addTXT = count($add)?implode(',',$add):'';
 ?>
<div class="uk-grid uk-grid-collapse"> 
<div class="uk-width-medium-1-1">
  <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase"> Riepilogo servizi richiesti </div>
 <h4 class="tm-article-subtitle">Procedura completa</a></h4>
<h4  style="margin-top: 1px;margin-bottom: 0px;">Consolato: <b><?php echo $lastRecord['consulate']; ?>.</b></h4>
<h4  style="margin-top: 1px;margin-bottom: 0px;">Assicurazione:: <b><?php echo $lastRecord['insurance']?'Inclusa':'No Inclusa'; ?></b></h4>
 <h4  class="uk-display-inline">Citta` da visitare: <b><?php echo $lastRecord['cities_to_visit']; ?> </b></h4>      <br>
 <h4  class="uk-display-inline">Servizi: <b><?php echo $ServiziTXT;//TODO: city?>  </b></h4><br>
 
<h4  class="uk-display-inline">Spedizione: <b><?php echo $lastRecord['shipping']; ?></b></h4>     <br>
<h4  class="uk-display-inline">Ho bisogno anche di: <b><?php echo $addTXT; ?></b></h4> <br>
 <h4  class="uk-display-inline">Commenti: </h4><div class="uk-text-small"><?php echo $lastRecord['add_other']; ?> </div> <br>
    
   
   </div>

        </div>

    <div class="uk-grid">
        <div class="uk-width-1-1">
        <form class="uk-form">

				<div>

</div>

			

        
    
</li>
<?php

    }

    //echo $body;
} ?>