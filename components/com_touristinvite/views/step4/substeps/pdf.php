<?php
$query = 'SELECT * FROM #__touristinvite_userinfo WHERE visaandinivte_id = '.intval($lastRecord['id']). ' LIMIT 1' ;
$db=JFactory::getDBO();
$db->setQuery($query);
$userInfo = $db->loadAssoc();
?>
<?php $subject = 'Pagamento ricevuto. Istruzioni e indirizzo per la spedizione dei documenti.'; ?>
<?php $body = '	  <div class="uk-grid" id="tohide">
<ul class="uk-tab">
    <li class="uk-active"><a href="/ita/modulo-visto-turistico/?option=com_touristinvite&view=list">Pratiche visto</a></li>
    <li class="uk-disabled"><a href="#">Assistenza</a></li>
    <li class="uk-disabled"><a href="#">Modifica profilo</a></li>
    <li class="uk-disabled"><a href="#">Fine sessione</a></li>
</ul>
    <div style="margin-bottom: 10px;" class="uk-width-1-1">

	<div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase">Visto turistico russia</div></div>

	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">1</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Compilazione dati</span> </div>
   
	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">2</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Controllo</span> </div>
	  <div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-warning">3</span> <span class="uk-width-8-10 uk-badge uk-badge-warning">Pagamento</span> </div>
		
		<div class="uk-width-medium-1-4"><span class="uk-badge uk-badge-notification uk-badge-success">4</span> <span class="uk-width-8-10 uk-badge uk-badge-success"> Invio documenti</span> </div>

    
    <div class="uk-width-1-1 uk-text-justify"><br>
	<img  src="/images/citta/roma.jpg" alt="Roma Visa"><br><br>

<div class="uk-grid uk-grid-collapse"> 
<div class="uk-width-medium-1-1">
    <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase"> Vi ringraziamo per il pagamento! La pratica visto e` stata avviata.</div>
</div></div>

<br />La seguente documentazione va consegnata o spedita tramite corriere espresso o raccomandata all`indirizzo successivamente menzionato: <br />
<ul>
<li>Passaporto con validità di almeno 6 mesi superiore alla data di previsto rientro e con almeno due pagine libere consecutive. Il passaporto deve essere integro, non rovinato, non usurato. In particolare verificate che non ci siano scollature tra la pellicola trasparente che ricopre la foto ed il resto del passaporto. Il passaporto deve essere firmato.</li>
<li>Due&nbsp;fototessere a colori, su sfondo bianco, formato 35x45 mm. Le foto devono essere scattate da non più di sei mesi e non devono essere uguali alle foto del passaporto. L`espressione del viso deve essere neutra, l`immagine frontale, senza occhiali da sole o altri elementi che coprano parte del viso. In primo piano devono essere il viso e la parte superiore delle spalle. Il volto deve occupare circa il 70-80% della foto. La foto non deve essere nè incollata nè pinzata.</li>
<li>Una copia del <a  target="_blank" href="http://visa.kdmid.ru/">modulo richiesta d\'ingresso (da compilare online sul sito del Consolato e stampare)</a></li>
</ul> 
<span class="uk-badge uk-badge-danger ">ATTENZIONE:</span><br> Compilare il modulo solo dopo aver ricevuto via mail le istruzioni dal nostro corrispondente. Nella mail, che riceverete entro un giorno lavorativo successivo al pagamento, saranno indicati il confirmation number della lettera d\'invito, il nome e l\'indirizzo dell\'organizzazione invitante, la compagnia assicurativa ed il numero di polizza. Nel caso in cui abbiate selezionato il servizio aggiuntivo "Compilazione del modulo consolare per mio conto" la compilazione sara` da noi effettuata <a data-uk-modal="{target:\'#modal11\'}" href=" ">dopo che ci avrete fornito le necessarie informazioni via mail.</a>

 <div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
 
  <div class="uk-modal-dialog uk-modal-dialog-large">
                                        <a href="" class="uk-modal-close uk-close"></a>
 
<h3 class="uk-panel-title">DATI PER LA COMPILAZIONE DEL MODULO RICHIESTA D’INGRESSO</h3>

<li><strong>LUOGO DI NASCITA:</strong> Citta` e nazione nella quale siete nati. Se in precedenza avevate nazionalita’ russa o dell’Unione Sovietica indicare perche’ e’ stata persa e quando. Se siete nati in Russia scrivere in che Paese siete emigrati e quando. &nbsp;&nbsp;</li>
<li><strong>DATA DI RILASCIO DEL PASSAPORTO E DATA DI SCADENZA.</strong></li>
<li><strong>SE AVETE GIA’ VISITATO LA RUSSIA SCRIVERE QUANTE VOLTE CI SIETE STATI E SPECIFICARE LE DATE DELL’ULTIMO VIAGGIO (DA-A)</strong></li>
<li><strong>INDIRIZZO DI RESIDENZA:</strong> via, numero civico, citta`, CAP, nazione</li>
<li><strong>NUMERO DI TELEFONO:</strong> indicare un numero al quale sia possibile facilmente contattarvi</li>
<li><strong>ATTUALE POSTO DI LAVORO O DI STUDIO:</strong> posizione ricoperta, indirizzo e numero di telefono.</li>
<li><strong>SE CI SONO BAMBINI SOTTO I 16 ANNI O ALTRI PARENTI PRESENTI SUL VOSTRO PASSAPORTO CHE VIAGGIANO CON VOI, INDICARE:</strong> grado di parentela, nome e cognome, data di nascita, ed indirizzo di residenza.</li>
<li><strong>SE AVETE ATTUALMENTE PARENTI IN RUSSIA, INDICARE:</strong> grado di parentela, nome e cognome, data di nascita, ed indirizzo di residenza.</li></pre>
</div></div>.
	
	
	<br /><br>
<div class="uk-panel uk-panel-box  uk-container-center uk-width-medium-1-2">
                                <div class="uk-panel-badge uk-badge ">Indirizzo</div>
                                <h3 class="uk-panel-title">Roma</h3>
                               <i class="uk-icon-calculator"></i> Zama World Visa<br />
<i class="uk-icon-home"></i> C.ne gianicolense, 88 - 00152 Roma <br>

<i class="uk-icon-clock-o"></i> Lunedi - Venerdi 9.30-13.00 14.30-18.00<br />
<i class="uk-icon-phone"></i> 06 5342701 (Viktoria)<br />
                            </div>
<br>
     Se avete la possibilita` di recarvi fisicamente presso l`agenzia, previo accordi telefonici con la nostra corrispondente Viktoria potrete consegnare e ritirare a mano i documenti a Roma. Con la procedura ordinaria il visto sara` pronto in circa 15 giorni dal momento della ricezione della documentazione da parte dei nostri corrispondenti. La documentazione sara \' presentata in consolato il giorno lavorativo successivo alla ricezione, ed il visto sara\' pronto in 10 giorni lavorativi. Con la procedura urgente il visto sara` pronto in 4 giorni lavorativi dal momento della presentazione della documentazione in Consolato da parte dei nostri corrispondenti.<br>

Per indicare l’indirizzo al quale ritirare e rispedire i vostri documenti compilate il form a seguire. Compilate i campi qualora abbiate incluso le spese di spedizione, o qualora vogliate indicare l`indirizzo al quale effettuare la spedizione a vostro carico o il numero di contratto del vostro personale corriere espresso.  <br>


<div>
           

<br>
<div id="inputs" '.(isset($userInfo['id'])?'style="display:none;"':'').'>
   <input type="text" placeholder="Nome del corriere postale" class="uk-margin-small-top" id="nome_del_corriere_postale">
    <input type="text" placeholder="Numero di contratto" class="uk-margin-small-top" id="numero_di_contratto"><br>
              <input type="text" placeholder="Nome e cognome" class="uk-margin-small-top" id="nome_e_cognome">
              <input type="text" placeholder="Societa`" class="uk-margin-small-top" id="societa">
              <input type="text" placeholder="Telefono" class="uk-margin-small-top" id="telefono">
              <input type="text" placeholder="Nazione" class="uk-margin-small-top" id="nazione">
              <input type="text" placeholder="Citta`" class="uk-margin-small-top" id="citta">
              <input type="text" placeholder="Indirizzo" class="uk-margin-small-top" id="indirizzo">
              <input type="text" placeholder="Codice postale" class="uk-margin-small-top" id="codice_postale"><br>
              <a class="uk-button"  href="" onclick="submitInputs(); return false;">INVIARE I DATI</a><br>
              </div>
              </li>
             


</div>




<div class="uk-text-small" '.(!isset($userInfo['id'])?'style="display:none;"':'').' id="hiddenText">Vi ringraziamo per la fiducia e vi auguriamo buon viaggio!<br>
Cordiali saluti,<br>
Lo Staff di Visto-Russia.com
</div>


</div></div>
'; ?>
<script>
    function submitInputs(){
        jQuery.post( "/?option=com_touristinvite", {
            'userinfo[visaandinivte_id]': <?php echo $lastRecord['id']; ?>,
            'userinfo[nome_del_corriere_postale]': jQuery('#nome_del_corriere_postale').val(),
            'userinfo[numero_di_contratto]': jQuery('#numero_di_contratto').val(),
            'userinfo[nome_e_cognome]': jQuery('#nome_e_cognome').val(),
            'userinfo[societa]': jQuery('#societa').val(),
            'userinfo[telefono]': jQuery('#telefono').val(),
            'userinfo[nazione]': jQuery('#nazione').val(),
            'userinfo[citta]': jQuery('#citta').val(),
            'userinfo[indirizzo]': jQuery('#indirizzo').val(),
            'userinfo[codice_postale]': jQuery('#codice_postale').val()
            }
        )
            .done(function( data ) {
                jQuery('#inputs').hide();
                jQuery('#hiddenText').show();
            });
    }

</script>