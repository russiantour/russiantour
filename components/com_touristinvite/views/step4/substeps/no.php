<?php echo'   <ul class="uk-tab">
    <li class="uk-active"><a href="/ita/modulo-visto-turistico/?option=com_touristinvite&view=list">Pratiche visto</a></li>
    <li class="uk-disabled"><a href="#">Assistenza</a></li>
    <li class="uk-disabled"><a href="#">Modifica profilo</a></li>
    <li class="uk-disabled"><a href="#">Fine sessione</a></li>
</ul>     <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1 uk-text-justify">
                <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase"> Grazie per aver effettuato il pagamento! </div>
                                    
Riceverete via mail la lettera d\'invito  ed e una copia dell\'assicurazione medica necessarie per l\'ottenimento del visto russo in giornata, se il pagamento e` stato effettuato in orario lavorativo, o il primo giorno lavorativo successivo al pagamento.
Avendo cliccato NO nella sezione "Procedura completa" sul form online , per ottenere il visto dovrete recarvi personalmente presso gli uffici predisposti dai consolati russi di Roma, Milano, Genova e Palermo, previo appuntamento, una volta appurati i tempi e le modalita` di presentazione della domanda di visto. Non e` possibile entrare in territorio russo con la sola stampa della lettera d\'invito e dell\'assicurazione, va richiesto ed ottenuto il visto russo prima di effettuare il viaggio. La lettera d\'invito deve essere coperta da una prenotazione alberghiera, se avete effettuato una prenotazione autonomamente dovrete indicare al nostro corrispondent l
\'hotel prenotato e le date della prenotazione, tenendo presente che il consolato potrebbe verificare la prenotazione. A Roma e Milano vengono accettate soltanto assicurazioni in originale, quindi non e` possibile presentare autonomamente copia delle nostre assicurazioni presso gli uffici consolari di queste due citta`, ed e` necessario che il Cliente si procuri autonomamente una polizza assicurativa in originale tra quelle accettate dall\'ufficio consolare. A Genova e Palermo vengono accettate anche le copie, quindi e` possibile stampare e presentare la nostra polizza.    </div>

        </div>
'; ?>