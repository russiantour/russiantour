<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

if ($_GET['code'])
{
    $code = strip_tags(addslashes($_GET['code']));
    $query = "SELECT *  FROM #__touristinvite_manualpayments WHERE status = 0 and ".
        "paymentID = '".$code."' LIMIT 1" ;
    $db=JFactory::getDBO();
    $db->setQuery($query);
    $manualPayment = $db->loadAssoc();

    if ($manualPayment['id']){
        $payTime = strtotime($manualPayment['timeCreatedLinl']);

        $query = 'SELECT CURRENT_TIMESTAMP as curTime' ;
        $db->setQuery($query);
        $curTime = $db->loadAssoc();
        $curTime = strtotime($curTime['curTime']);

        if ($curTime >= $payTime+3600000*24){
            echo '24 hours expired';
            exit;
        }
    }
}

//Load admin language file
$lang = JFactory::getLanguage();
$lang->load('com_touristinvite', JPATH_ADMINISTRATOR);
$params = JComponentHelper::getParams('com_touristinvite')->get('params');
//$params = (isset($this->state->params)) ? $this->state->params : new JObject;
$user = JFactory::getUser();
if ($user->get('id')){
    $lastRecord = JFactory::getApplication()->input->get('lastRecord',null,'array')[0];
    $participants = JFactory::getApplication()->input->get('participants',null,'array');
}

?>

<ul id="tab-content" class="tm-tab-content uk-switcher">
<li class="uk-active"> 

		 





 <?php

 $Servizi = array(
    'urgent' => 'Ho bisogno della procedura urgente',
    'form_filling' => 'Compilazione del modulo consolare per mio conto',
    'rus_visa' => 'Registrazione del visto russo'
 );
 if (!$lastRecord['urgent']) unset($Servizi['urgent']);
 if (!$lastRecord['form_filling']) unset($Servizi['form_filling']);
 if (!$lastRecord['rus_visa']) unset($Servizi['rus_visa']);
    $ServiziTXT = count($Servizi)?implode(',',$Servizi):'';
 //////////////////////
 $add = array(
     'add_hotel' => 'hotel',
     'add_appartamento' => 'appartamento',
     'add_volo_treno' => 'volo/treno',
     'add_transfers' => 'transfers',
     'add_escursioni' => 'escursioni'
 );
 if (!$lastRecord['add_hotel']) unset($add['add_hotel']);
 if (!$lastRecord['add_appartamento']) unset($add['add_appartamento']);
 if (!$lastRecord['add_volo_treno']) unset($add['add_volo_treno']);
 if (!$lastRecord['add_transfers']) unset($add['add_transfers']);
 if (!$lastRecord['add_escursioni']) unset($add['add_escursioni']);
 $addTXT = count($add)?implode(',',$add):'';
 ?>
<img src="https://russiantour.com/images/visa_master.jpg" width="1200" height="400" alt="condizionivendita" style="
    margin-left: -5px;
    max-width: 101%;
">

		
<br><br>		


<div class="uk-grid uk-grid-collapse"> 
<div class="uk-width-medium-1-1">
 <div class="uk-panel-box-secondary uk-text-center uk-text-contrast uk-text-bold uk-text-uppercase" style="
    background-color: rgb(113, 150, 176);
    /* padding-left: 1px; */
    background-image: url('../less/uikit/images/default/panel-box-noise-bg.png');
    box-shadow: 0 1px 3px rgba(15, 24, 34, 0.3), inset 0 0 1px rgba(255, 255, 255, 0.15), inset 0 1px 0 rgba(255, 255, 255, 0.12);
    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.66);
    color: #fff;
">
  Prima di effettuare il pagamento leggere le condizioni generali di contratto.</div>


    
<div ><div class="uk-float-left">
<label class="uk-margin-small-top">
                                    <input  type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);">
                                        </label> </div><div style="padding-top: 7px;" ><b>
         <a data-uk-modal="{target:'#modal11'}" href=" " >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL  </a></b></div></div>
 



<div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                                    <div  class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large">
                                        <a href="" class="uk-modal-close uk-close"></a>
                                        <h2>Condizioni di fornitura del servizio:</h2><br />
					<p style="text-align: justify;"><strong>TASSO DI CAMBIO </strong>- I prezzi da catalogo sono soggetti a variazioni in seguito a variazione del tasso di cambio euro/rublo nel caso in cui al momento del pagamento il tasso sia superiore del 10% rispetto al tasso di cambio indicato in fase di preventivo.</p>
<p style="text-align: justify;"><strong>MODALITA` DI PAGAMENTO</strong> - In caso di pagamento con bonifico bancario, il versamento e` da effettuare in Euro e le commissioni bancarie debbono essere shared (SHA) e non a carico del beneficiario. Qualora l’importo ricevuto sia inferiore a quello preventivato, verra` richiesta al Cliente una integrazione. In caso di pagamento con carta di credito si applica una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli, in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi.</p>
<p style="text-align: justify;"><strong>PRENOTAZIONI</strong> - L'accettazione delle prenotazioni si intende perfezionata, con conseguente conclusione del contratto, nel momento in cui l'organizzatore invierà relativa conferma, anche a mezzo sistema telematico, al turista presso il proprio recapito o eventualmente presso l'agenzia di viaggi intermediaria. L’organizzatore fornirà prima della partenza le indicazioni relative al pacchetto turistico non contenute nei documenti contrattuali, negli opuscoli ovvero in altri mezzi di comunicazione scritta.</p>
<p style="text-align: justify;"><strong>PAGAMENTI</strong>&nbsp; - La misura dell'acconto da versare all'atto della prenotazione ovvero all'atto della richiesta impegnativa e la data entro cui, prima della partenza, dovrà essere effettuato il saldo, verrano indicati al momento della richiesta di prenotazione. Il mancato pagamento delle somme di cui sopra da parte del turista o il mancato versamento delle stesse da parte dell'agenzia intermediaria all'organizzatore alle date stabilite costituisce clausola risolutiva espressa tale da determinarne, da parte dell'agenzia intermediaria e/o dell'organizzatore, la risoluzione di diritto.</p>
<p style="text-align: justify;"><strong>PREZZO</strong> - Il prezzo del pacchetto turistico è determinato all’accettazione della prenotazione, con riferimento a quanto indicato in catalogo o programma fuori catalogo ed agli eventuali aggiornamenti degli stessi cataloghi o programmi fuori catalogo successivamente intervenuti. Esso potrà essere variato fino a 20 giorni precedenti la partenza e soltanto in conseguenza alle variazioni di: — costi di trasporto, incluso il costo del carburante; — diritti e tasse su alcune tipologie di servizi turistici quali imposte, tasse o diritti di atterraggio, di sbarco o di imbarco nei podi e negli aeroporti; — tassi di cambio applicati al pacchetto in questione.</p>
<p style="text-align: justify;"><strong>MODIFICA O ANNULLAMENTO DEL PACCHETTO TURISTICO PRIMA DELLA PARTENZA</strong>&nbsp; - Prima della partenza l'organizzatore o l'intermediario che abbia necessità di modificare in modo significativo uno o più elementi del contratto, ne dà immediato avviso in forma scritta al turista, indicando il tipo di modifica e la variazione del prezzo che ne consegue. Ove non accetti la proposta di modifica, il turista potrà esercitare alternativamente il diritto di riacquisire la somma già pagata o di godere dell'offerta di un pacchetto turistico. Il turista può esercitare i diritti sopra previsti anche quando l'annullamento dipenda dal mancato raggiungimento del numero minimo di partecipanti previsto nel Catalogo o nel Programma fuori catalogo. Per gli annullamenti diversi da quelli causati dal mancato raggiungimento del numero minimo di partecipanti, nonché per quelli diversi dalla mancata accettazione da parte del turista del pacchetto turistico alternativo offerto, l'organizzatore che annulla, restituirà al turista quanto dallo stesso pagato e incassato dall'organizzatore, eventualmente tramite l'agente di viaggio.</p>
<p style="text-align: justify;"><strong>MODALITA` DI ANNULLAMENTO E PENALI</strong> - Per procedere con la cancellazione, è necessario inviare una e-mail all΄indirizzo <span id="cloak27299"><a href="mailto:info@russiantour.com">info@russiantour.com</a></span><script type="text/javascript">
 //<!--
 document.getElementById('cloak27299').innerHTML = '';
 var prefix = '&#109;a' + 'i&#108;' + '&#116;o';
 var path = 'hr' + 'ef' + '=';
 var addy27299 = '&#105;nf&#111;' + '&#64;';
 addy27299 = addy27299 + 'r&#117;ss&#105;&#97;nt&#111;&#117;r' + '&#46;' + 'c&#111;m';
 var addy_text27299 = '&#105;nf&#111;' + '&#64;' + 'r&#117;ss&#105;&#97;nt&#111;&#117;r' + '&#46;' + 'c&#111;m';
 document.getElementById('cloak27299').innerHTML += '<a ' + path + '\'' + prefix + ':' + addy27299 + '\'>'+addy_text27299+'<\/a>';
 //-->
 </script> con la richiesta di cancellazione, mettendo in copia l'indirizzo e-mail del manager che sta seguendo la vostra pratica. La richiesta di cancellazione deve pervenire entro le ore 17.30 durante i giorni lavorativi. E` vostra cura verificare (con una telefonata o una mail di richiesta conferma) che la richiesta di cancellazione sia correttamente pervenuta. I rimborsi dovuti ad annullamento dei servizi prenotati da parte del cliente, vengono effettuati con spese bancarie totalmente a carico del cliente. Nel caso di rimborsi dovuti a disservizi o nostre errate prenotazioni, le spese bancarie saranno tutte a nostro carico.</p>
<ul>
<li style="text-align: justify;"><strong>Penali per annullamento prenotazione alberghiera</strong>: Soltanto al momento della prenotazione alberghiera saranno indicati i termini di annullamento e rimborso, in quanto sono molto variabili da hotel ad hotel e da stagione a stagione. In occasione di importanti eventi o di alcune fiere a Mosca, la penale e` del 100% sin dal momento del pagamento. Sarete comunque avvisati delle modalita` al momento della prenotazione, prima di effettuare il pagamento. Indipendentemente dalla cancellation policy dell'hotel, una notte sara` trattenuta per cancellazioni effettuate a meno di una settimana dall'arrivo. In caso di overbooking, o nostro errore nella prenotazione (date, o differente hotel/tipologia di stanza), l'agenzia si impegna a cercare la possibilita` di riposizionamento in differenti hotel di standard simile o in stanze della&nbsp;tipologia prescelta,&nbsp;e qualora questo non fosse possibile, o le alternative proposte non siano di gradimento, procedera` al completo rimborso del pagamento effettuato senza penali. Russian Tour non e'&nbsp; tenuta a garantire al 100% il riposizionamento qualora sia oggettivamente impossibile, e non e` obbligata al riposizionamento in hotel di classe o costo di gran lunga superiore, a meno che il cliente non sia pronto a supportare la differenza di costo (netta, senza ovviamente commissioni aggiuntive).</li>
<li style="text-align: justify;"><strong>Penali per annullamento prenotazione appartamenti</strong>: Soltanto al momento della prenotazione saranno indicati i termini di annullamento e rimborso, in quanto sono molto variabili da appartamento ad appartamento e da stagione a stagione. Per prenotazioni in periodi di punta o per soggiorni di lunga durata, li dove e' previsto l'anticipo di due o tre notti, per annullamenti a meno di due settimane dall'arrivo viene solitamente trattenuto l'intero anticipo. Nel caso in cui abbiate provveduto al pagamento anticipato di tutta la somma, il totale meno la penale vi sara` rimborsato nell'arco di quindici giorni dal momento della richiesta di annullamento, qualora abbiate fornito tutti i dettagli e le coordinate bancarie per il rimborso. In caso di nostro errore nella prenotazione (date o appartamento diverso) o improvvisa indisponibilita` o impraticabilita` dell'appartamento prenotato, Russian Tour si impegna a cercare la possibilita` di riposizionamento in differenti appartamenti di standard simile e qualora questo non fosse possibile o le alternative proposte non siano di gradimento, procedera` al completo rimborso del pagamento effettuato senza penali. Russian Tour non e' tenuta a garantire al 100% il riposizionamento qualora sia oggettivamente impossibile, e non e` obbligata al riposizionamento in appartamenti di classe o costo di gran lunga superiore, a meno che il cliente non sia pronto a supportare la differenza di costo.</li>
<li style="text-align: justify;"><strong>Penali per annullamento biglietteria aerea e ferroviaria</strong>: Per i biglietti ferroviari ed aerei le condizioni di cancellazione dipendono dalle compagnie aeree e ferroviarie&nbsp;e sono riportate nel biglietto cartaceo o elettronico.&nbsp;Generalmente le tariffe econom prevedono la perdita del 100% del biglietto, fino ad arrivare alle tariffe business class che permettono l'annullamento fino all'ultimo minuto. I tempi di rimborso dipendono da quelli delle compagnie aeree e ferroviarie. Nel caso remoto in cui&nbsp;la prenotazione sia stata fatta in maniera non corretta dai nostri operatori, l'importo del biglietto sara` rimborsato.</li>
<li style="text-align: justify;"><strong>Penali per annullamento escursioni ed altri servizi a terra</strong>: soltanto al momento della prenotazione saranno indicati i termini di annullamento e rimborso, in quanto sono molto variabili a seconda del tipo di servizio e del periodo dell’anno. E` possibile ottenere il rimborso solo nel caso in cui i servizi o i biglietti d'ingresso non siano giá stati acquistati e pagati presso fornitori esterni al momento della ricezione della richiesta di annullamento.</li>
</ul>
<p style="text-align: justify;"><strong>MODIFICHE DOPO LA PARTENZA</strong> - L’organizzatore, qualora dopo la partenza si trovi nell'impossibilità di fornire per qualsiasi ragione, tranne che per un fatto proprio del turista, una parte essenziale dei servizi contemplati in contratto, dovrà predisporre soluzioni alternative, senza supplementi di prezzo a carico del contraente e qualora le prestazioni fornite siano di valore inferiore rispetto a quelle previste, rimborsarlo in misura pari a tale differenza. Qualora non risulti possibile alcuna soluzione alternativa, l'organizzatore fornirà senza supplemento di prezzo, un mezzo di trasporto equivalente a quello originario previsto per il ritorno al luogo di partenza o al diverso luogo eventualmente pattuito, compatibilmente alle disponibilità di mezzi e posti, e lo rimborserà nella misura della differenza tra il costo delle prestazioni previste e quello delle prestazioni effettuate fino al momento del rientro anticipato.</p>
<p style="text-align: justify;"><strong>SOSTITUZIONI</strong> - Il turista rinunciatario può farsi sostituire da altra persona sempre che: a) l'organizzatore ne sia informato per iscritto almeno 14 giorni lavorativi prima della data fissata per la partenza, ricevendo contestualmente comunicazione circa le ragioni della sostituzione e le generalità del cessionario; b) il cessionario soddisfi tutte le condizioni per la fruizione del servizio ed in particolare i requisiti relativi al passaporto, ai visti, ai certificati sanitari; c) i servizi medesimi o altri servizi in sostituzione possano essere erogati a seguito della sostituzione; d) il sostituto rimborsi all'organizzatore tutte le spese aggiuntive sostenute per procedere alla sostituzione, nella misura che gli verrà quantificata prima della cessione. Il cedente ed il cessionario sono solidalmente responsabili per il pagamento del saldo del prezzo nonché degli importi di cui alla lettera d) del presente articolo.</p>
<p style="text-align: justify;"><strong> CLASSIFICAZIONE ALBERGHIERA</strong> - La classificazione uffciale delle strutture alberghiere viene fornita in catalogo od in altro materiale informativo soltanto in base alle espresse e formali indicazioni delle competenti autorità del paese in cui il servizio è erogato. In assenza di classificazioni uffciali riconosciute dalle competenti Pubbliche Autorità cui il servizio si riferisce, l'organizzatore si riserva la facoltà di fornire in catalogo o nel dépliant una propria descrizione della struttura ricettiva, tale da permettere una valutazione e conseguente accettazione della stessa da parte del turista.</p>
<p style="text-align: justify;"><strong>REGIME DI RESPONSABILITÀ</strong> - L'organizzatore risponde dei danni arrecati al turista a motivo dell'inadempimento totale o parziale delle prestazioni contrattualmente dovute, nei limiti della somma percepita come pagamento, sia che le stesse vengano effettuate da lui personalmente che da terzi fornitori dei servizi, a meno che provi che l'evento è derivato da fatto del turista (ivi comprese iniziative autonomamente assunte da quest'ultimo nel corso dell'esecuzione dei servizi turistici) o dal fatto di un terzo a carattere imprevedibile o inevitabile, da circostanze estranee alla fornitura delle prestazioni previste in contratto, da caso fortuito, da forza maggiore, ovvero da circostanze che lo stesso organizzatore non poteva, secondo la diligenza professionale, ragionevolmente prevedere o risolvere. L'intermediario presso il quale sia stata effettuata la prenotazione del pacchetto turistico non risponde in alcun caso delle obbligazioni nascenti dall'organizzazione del viaggio, ma è responsabile esclusivamente delle obbligazioni nascenti dalla sua qualità di intermediario e, comunque, nei limiti previsti per tale responsabilità dalle norme vigenti in materia.</p>
<p style="text-align: justify;"><strong>CONDIZIONI DI FORNITURA DEL SERVIZIO OTTENIMENTO VISTO</strong> - Il turista affida all’organizzatore ed alle sue agenzie corrispondenti in Italia, o all’intermediario, la gestione dei documenti inerenti alla pratica richiesta visto, li autorizza al trattamento dei dati personali indicati per lo svolgimento del servizio richiesto (in conformità con l’art. 13 del decreto legislativo del 30/06/03 n°196), e li solleva da ogni responsabilità per eventuali rifiuti, ritardi, problemi e disguidi nell’ottenimento del visto, e all’ingresso nel paese richiedente visto, dovuti a cause non imputabili al loro operato, ossia:</p>
<ul>
<li style="text-align: justify;">il passaporto non venga accettato dal Consolato perchè con scadenza inferiore a 6 mesi o perchè considerato non integro, usurato, non firmato;</li>
<li style="text-align: justify;">il visto venga rifiutato per qualsiasi motivo a discrezione del Consolato (diniego del visto, moduli compilati dal turista in maniera non corretta, documenti spediti dal turista in maniera errata o incompleta);</li>
<li style="text-align: justify;">ci siano ritardi nel rilascio del visto dovuti a impreviste chiusure al pubblico del Consolato o a improvvise modifiche dei tempi e delle modalità di ottenimento del visto;</li>
<li style="text-align: justify;">si verifichino smarrimento dei documenti da parte dei corrieri postali, consegne ad indirizzi errati, o ritardi nella consegna imputabili agli stessi corrieri;</li>
<li style="text-align: justify;">il visto venga rilasciato con date di ingresso diverse da quelle richieste a causa di decisione o di errore del consolato;</li>
<li style="text-align: justify;">si verifichino problemi alla frontiera con il personale di dogana che implichino il rimpatrio.</li>
</ul>
<p style="text-align: justify;">La pratica va pagata anticipatamente e non sarà avviata fino a quando il pagamento non sarà stato effettuato e notificato. Il turista è tenuto a controllare i documenti all’atto della consegna. Non verranno accettati reclami se non comunicati il giorno stesso della ricezione dei documenti.</p>
<p style="text-align: justify;">In caso di ritardi, errori o mancate concessioni del visto imputabili all’operato dell’organizzatore o delle sue agenzie corrispondenti in Italia, l’organizzatore provvederà esclusivamente al rimborso della quota visto e degli altri servizi collegati alla prenotazione effettuata dal turista con l’organizzatore o suo intermediario, e di nessun altra spesa dovuta a servizi acquistati con terzi o a problemi personali collegabili al mancato viaggio.</p>
<p style="text-align: justify;"><strong>OBBLIGO DI ASSISTENZA</strong> - L'organizzatore è tenuto a prestare le misure di assistenza al turista secondo il criterio di diligenza professionale con esclusivo riferimento agli obblighi a proprio carico per disposizione di legge o di contratto. L’organizzatore e l'intermediario sono esonerati dalle rispettive responsabilità, quando la mancata od inesatta esecuzione del contratto è imputabile al turista o è dipesa dal fatto di un terzo a carattere imprevedibile o inevitabile, ovvero è stata causata da un caso fortuito o di forza maggiore.</p>
<p style="text-align: justify;"><strong>RECLAMI E DENUNCE</strong> - Ogni mancanza nell'esecuzione del contratto deve essere contestata dal turista durante la fruizione del pacchetto mediante tempestiva presentazione di reclamo affinché l'organizzatore, il suo rappresentante locale o l'accompagnatore vi pongano tempestivamente rimedio. In caso contrario il risarcimento del danno sarà diminuito o escluso. Il turista dovrà altresì - a pena di decadenza - sporgere reclamo mediante l'invio di una raccomandata, con avviso di ricevimento, o altro mezzo che garantisca la prova dell'avvenuto ricevimento, all'organizzatore o all'intermediario, entro e non oltre dieci giorni lavorativi dalla data di rientro nel luogo di partenza.</p>
<p style="text-align: justify;"><strong>ASSICURAZIONE CONTRO LE SPESE DI ANNULLAMENTO E DI RIMPATRIO</strong> - Se non espressamente comprese nel prezzo, è possibile, ed anzi consigliabile, stipulare al momento della prenotazione presso gli uffci dell'intermediario speciali polizze assicurative contro le spese derivanti dall'annullamento del pacchetto turistico, da eventuali infortuni e da vicende relative ai bagagli trasportati. Sarà altresì possibile stipulare un contratto di assistenza che copra le spese di rimpatrio in caso di incidenti, malattie, casi fortuiti e/o di forza maggiore. Il turista eserciterà i diritti nascenti da tali contratti esclusivamente nei confronti delle Compagnie di Assicurazioni stipulanti, alle condizioni e con le modalità previste da tali polizze.</p>
<p style="text-align: justify;"><strong>PRIVACY</strong> - Informativa ex ad. 13 D. Lgs. n.196/03 (Codice in materia di protezione dei dati personali) Il trattamento dei dati personali è effettuato sia in forma cartacea che in forma digitale, nel pieno rispetto del D. Lgs. 196/2003, per le finalità di conclusione del contratto e per l'esecuzione delle prestazioni che formano oggetto del pacchetto turistico. Il conferimento dei dati è necessario. I dati personali non saranno oggetto di diffusione, ma di comunicazione ai soli fornitori dei servizi componenti il pacchetto turistico acquistato.</p>
<p style="text-align: justify;"><strong>VETTORE AEREO - </strong>Qualora aveste con noi acquistato il volo aereo, il nome del vettore aereo che effettuerà il/i vostro volo/i è indicato nel foglio di conferma prenotazione; eventuali variazioni Vi verranno comunicate tempestivamente. Il costo del carburante è soggetto a variazioni che possono modificare il prezzo da contratto; tali variazioni vengono computate a passeggero e sono il risultato dell'applicazione del criterio di calcolo adottato dal vettore che esegue il servizio. L'articolo 9 fascicolo 3 "sulle attivita` turistiche" e gli atti&nbsp; del "Codice Aereo e regole dell'aviazione federale" prevedono che, in caso di annullamenti, scioperi, spostamenti, ritardi del vettore aereo, la responsabilita` per tutti i danni materiali e morali conseguenti al disservizio ricade sulla compagnia aerea. Russian Tour International non e` responsabile per circostanze non direttamente imputabili al lavoro di intermermediazione effettuato, e collegate ad eventi non ragionevolmente prevedibili o risolvibili secondo la diligenza professionale. Il bliglietto aereo rappresenta il contratto tra il cliente e la compagnia aerea, tutte le pretese collegate a disservizi e problemi del vettore aereo possono essere regolate direttamente con la stessa.</p>
<p style="text-align: justify;"><strong>RESPONSABILITÀ DEI VETTORI AEREI</strong> — Reg. 2027/97 I vettori aerei comunitari e quelli appartenenti a Stati aderenti alla Convenzione di Montreal 1999 sono soggetti al seguente regime di responsabilità: non sussistono limiti finanziari alla responsabilità del vettore aereo per i danni da morte, ferite o lesioni personali del passeggero. Per danni superiori a 100000 DSP (equivalenti a circa Euro 120.000) il vettore aereo può contestare una richiesta di risarcimento solo se è in grado di provare che il danno non gli è imputabile. In caso di ritardo nel trasporto passeggeri il vettore è responsabile per il danno fino ad un massimo di 4150 DSP (circa 5000 Euro). In caso di distruzione, perdita, danneggiamento o ritardo nella riconsegna dei bagagli, il vettore aereo è responsabile per il danno fino a 1000 DSP (circa 1200 Euro). È possibile effettuare una dichiarazione speciale di maggior valore del bagaglio o sotto-scrivere apposita assicurazione col pagamento del relativo supplemento al più tardi al momento dell'accettazione. I Vettori non appartenenti a uno Stato aderente alla Convenzione di Montreal possono applicare regimi di responsabilità differenti da quello sopra riportato. La responsabilità del tour operator nei confronti del passeggero resta in ogni caso disciplinata dal Codice del Consumo e dalle Condizioni Generali di Contratto pubblicate nel presente catalogo.</p>
 
						
						
						
                                    </div>
                                </div>	

<br>



    
        <div class="uk-panel uk-panel-box  uk-container-left uk-width-medium-1-2">
            
         Data <?php echo $manualPayment['timeCreated']; ?> <br>
		     
             Nome e cognome : <?php echo $manualPayment['fio']; ?> <br>
			Telefono: <?php echo $manualPayment['telefon']; ?><br>
			Email: <?php echo $manualPayment['email']; ?><br>
			Causale del pagamento: <?php echo $manualPayment['description']; ?><br>
            Importo:  <?php echo $manualPayment['amountEuro']; ?>  <i class="uk-icon-euro"></i><br>

        </div>
  
   Il link per il pagamento e` attivo 24 ore. Se non effettuate il pagamento entro 24 ore    dovrete richiedere all'operatore un nuovo link.
   <br>
<div class="uk-text-justify"> 
Il sistema di pagamento è sicuro, il nostro sito internet supporta la crittografia a 256-bit. Le informazioni inserite non verranno fornite a terzi, salvo nei casi previsti dalla legislazione della Federazione Russa. Le operazioni con carta di credito sono condotte in stretta conformità con i requisiti di sistemi di pagamento internazionali Visa Int. e MasterCard Europe Sprl. <br>
Sul pagamento con carta di credito, viene applicata una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi. Barrando la casella "Ho letto ed accetto i termini e le condizioni si servizio della Russian Tour International" l'intestatario della carta di credito riconosce ed autorizza il prelievo dalla sua carta alle condizioni indicate.<br></div>

      <button class="uk-button Inviare"    onclick="location.href='?option=com_touristinvite&preparePayment=1&manualPayment=1&code=<?php echo $code; ?>'" id="Inviare" disabled>PAGAMENTO CON CARTA DI CREDITO</button>


   </div>

        </div>
		




			

        
    
</li>

<script>
    window.onload = function(){
        jQuery('#COORDINATE_BANCARIE').on('click',function(){jQuery.ajax({url: '?option=com_touristinvite&preparePayment=2',type: 'get'});return true;});

    }
</script>

