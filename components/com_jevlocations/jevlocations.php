<?php
/**
 * copyright (C) 2008 GWE Systems Ltd - All rights reserved
 */

defined( 'JPATH_BASE' ) or die( 'Direct Access to this location is not allowed.' );

//error_reporting(E_ALL);

jimport('joomla.filesystem.path');

include_once(JPATH_SITE."/components/com_jevents/jevents.defines.php");

if (!defined("JEVEX_COM_COMPONENT")){
	define("JEVEX_COM_COMPONENT","com_jevlocations");
	define("JEVEX_COMPONENT",str_replace("com_","",JEVEX_COM_COMPONENT));
}
if (!defined("JEV_COM_COMPONENT")){
	include_once(JPATH_SITE."/components/com_jevents/jevents.defines.php");
}
JLoader::register('JevLocationsHelper',JPATH_ADMINISTRATOR."/components/com_jevlocations/libraries/helper.php");
JLoader::register('JevCfForm',JPATH_SITE."/plugins/jevents/jevcustomfields/customfields/jevcfform.php");
JLoader::register('JevHtmlBootstrap' , JEV_PATH."libraries/bootstrap.php");
JLoader::register('JEVHelper',JPATH_SITE."/components/com_jevents/libraries/helper.php");

// load admin language too
$lang 		= JFactory::getLanguage();
$lang->load("com_jevlocations", JPATH_ADMINISTRATOR);

// In Joomla 1.6 JComponentHelper::getParams(JEV_COM_COMPONENT) is a clone so the menu params do not propagate so we force this here!
if (version_compare(JVERSION, "1.6.0", 'ge')){
	$newparams	= JFactory::getApplication('site')->getParams();
	// Because the application sets a default page title,
	// we need to get it from the menu item itself
	$menu = JFactory::getApplication()->getMenu()->getActive();
	if ($menu) {
		$newparams->def('page_heading', $newparams->get('page_title', $menu->title));
	}
	else {
		$params = JComponentHelper::getParams(JEVEX_COM_COMPONENT);
		$newparams->def('page_heading', $params->get('page_title')) ;
	}
	$component = JComponentHelper::getComponent(JEVEX_COM_COMPONENT);
	if ($newparams->get('modlatest_useLocalParam',1) == 0)
	{
		$parametersNames = array("CustFmtStr","Days","DisDateStyle","DisTitleStyle","DispLinks","DispYear",
			"LinkCloaking","LinkToCal","MaxEvents","Mode","NoRepeat","SortReverse","inccss","multiday","useLocalParam");

		foreach($parametersNames as $parameterName)
		{
			$parameterName = "modlatest_".$parameterName;
			$newparams->set($parameterName,$component->params->get($parameterName));
		}
	}
	$component->params = $newparams;
}
$params = JComponentHelper::getParams(JEVEX_COM_COMPONENT);

// Split task into command and task
$cmd = JFactory::getApplication()->input->get('task', false);

if (!$cmd) {
	$view =	JFactory::getApplication()->input->get('view', false);
	$layout = JFactory::getApplication()->input->get('layout', "show");
	if ($view && $layout){
		$cmd = $view.'.'.$layout;
	}
	else $cmd = "locations.locations";
}

if (strpos($cmd, '.') != false) {
	// We have a defined controller/task pair -- lets split them out
	list($controllerName, $task) = explode('.', $cmd);

	// Define the controller name and path
	$controllerName	= strtolower($controllerName);
	$controllerPath	= JPATH_COMPONENT."/".'controllers'."/".$controllerName.'.php';
	$controllerName = "Front".$controllerName;

	// If the controller file path exists, include it ... else lets die with a 500 error
	if (file_exists($controllerPath)) {
		require_once($controllerPath);
	} else {
		JError::raiseError(500, 'Invalid Controller '.$controllerName);
	}
} else {
	// Base controller, just set the task
	$controllerName = $view;
	$task = $layout;
}

// Stop viewing ALL events - it could take VAST amounts of memory
$cfg = JEVConfig::getInstance();
if ($cfg->get('blockall', 0) && ( JRequest::getInt("limit", -1) == 0 || JRequest::getInt("limit", -1) > 100 ))
{
	JRequest::setVar("limit", 100);
	JFactory::getApplication()->setUserState("limit", 100);
}

// Set the name for the controller and instantiate it
$controllerClass = ucfirst($controllerName).'Controller';
if (class_exists($controllerClass)) {
	$controller = new $controllerClass();
} else {
	JError::raiseError(500, 'Invalid Controller Class - '.$controllerClass.(file_exists($controllerPath)?" Exists":" doesnt Exist" ));
}

$config	= JFactory::getConfig();

// Perform the Request task
$controller->execute($task);

// Set the browser title to include site name if required
$title =  JFactory::getDocument()->GetTitle();
$app = JFactory::getApplication();
if (empty($title)) {
	$title = $app->getCfg('sitename');
}
elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
	$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
}
elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
	$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
}
JFactory::getDocument()->SetTitle($title);

// Redirect if set by the controller
$controller->redirect();
