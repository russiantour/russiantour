<?php

/**
 * @package     JEvents Locations Manager
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      Carlos <carcam@jevents.net> - http://www.jevents.net
 */
// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.view');

/**
 * View class for a list of .
 */
class JevlocationsViewJevlocations extends JViewLegacy {

    protected $items;
    protected $pagination;
    protected $state;
    protected $params;
	protected $menuitemid;

    /**
     * Display the view
     */
    public function display($tpl = null) {
        $app = JFactory::getApplication();

		$input = $app->input;
		$token = $input->get( 'token' );

		$newToken = JSession::getFormToken();
		//if(JSession::getFormToken() == $token)
		if(true)
		{
			$typeahead = $input->get('typeahead');
			$model = $this->getModel();
			$model->set("typeahead",$typeahead);
			$this->menuitemid = $input->getInt( 'Itemid' );
			$this->params = $app->getParams('com_jevlocations');
			$this->state = $this->get('State');
			$this->items = $this->get('Items');

			// Check for errors.
			if (count($errors = $this->get('Errors'))) {
				throw new Exception(implode("\n", $errors));
			}

			$this->_prepareDocument();
		}
        parent::display($tpl);
    }

    /**
     * Prepares the document
     */
    protected function _prepareDocument() {
		// Get the document object.
		$this->document = JFactory::getDocument();

		// Set the MIME type for JSON output.
		//$this->document->setMimeEncoding('application/json');

		// Change the suggested filename.
		//JResponse::setHeader('Content-Disposition','attachment;filename=".json"');

    }

}