<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined('_JEXEC' ) or die('Restricted access');
if (!$this->loadedFromTemplate('com_jevlocations.locations.list', "list"))
{

	$locparams = JComponentHelper::getParams("com_jevlocations");
	$usecats = $locparams->get("usecats",0);

	$mediaparams = JComponentHelper::getParams('com_media');
	$mediabase = JURI::root().$mediaparams->get('image_path', 'images/stories');
	// folder relative to media folder
	$folder = "jevents/jevlocations";
	$Itemid = JRequest::getInt("Itemid");
        //We get the columns ordering
        $columnsArray = explode(",",$locparams->get('columns','location,events,image,country,state,city'));

        // Get the custom fields data
        JevLocationsHelper::getLocationsCustomFields($this->items);

        $compparams = JComponentHelper::getParams("com_jevlocations");
        $template = $compparams->get("fieldtemplate", "");

        $cfarray = array();
        if ($template != "" && $compparams->get("custinlist") && JPluginHelper::isEnabled("jevents", 'jevcustomfields')) {
            $xmlfile = JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/templates/" . $template;
            if (file_exists($xmlfile)) {
                JLoader::register('JevCfForm', JPATH_SITE . "/plugins/jevents/jevcustomfields/customfields/jevcfform.php");
                $jcfparams = JevCfForm::getInstance("com_jevlocations.customfields", $xmlfile, array('control' => 'jform', 'load_data' => true), true, "/form");
                $customfields = array();
                $groups = $jcfparams->getFieldsets();
                foreach ($groups as $group => $element) {
                    $fsparams = $jcfparams->getFieldset($group);

                    if ($fsparams) {
                        foreach ($fsparams as $p => $node) {
                            $nodename = $node->attribute('name');
                            $nodelabel = $node->attribute('label');
                            if ($nodelabel && $nodename) {
                                $cfarray[$nodename] = JText::_($nodelabel);
                            }
                        }
                    }
                }
            }
        }

        if($locparams->get('showlocationlatestevents',0))
        {
            require_once (JPATH_SITE . "/modules/mod_jevents_latest/helper.php");
            $jevhelper = new modJeventsLatestHelper();
            $theme = JEV_CommonFunctions::getJEventsViewName();
            JPluginHelper::importPlugin("jevents");
            $viewclass = $jevhelper->getViewClass($theme, 'mod_jevents_latest', $theme ."/". "latest", $locparams);
            // record what is running - used by the filters
            $registry = JRegistry::getInstance("jevents");
            $registry->set("jevents.activeprocess", "mod_jevents_latest");
            $registry->set("jevents.moduleid", "cb");

            $menuitem = intval($locparams->get("targetmenu", 0));
        }
?>

	<?php
	$app=  JFactory::getApplication('site');
	$params = $app->getParams();
	$active	= $app->getMenu()->getActive();
	if ($active){
		$locparams->merge($active->params);
	}
	?>
	<div class="jevbootstrap">
		<?php if ($locparams->get('show_page_heading', 0)) : ?>
			<h1>
				<?php echo $this->escape($locparams->get('page_heading',$locparams->get('page_title', $active? $active->title:""))); ?>
			</h1>
		<?php endif; ?>

		<form action="<?php echo JRoute::_("index.php?option=com_jevlocations&task=locations.locations&layout=locations&Itemid=$Itemid");?>" method="post" name="adminForm" id="adminForm" >
		<?php
			if ($locparams->get("showfilters",1))
			{
				if ( $locparams->get("newfilterlayout",0) )
				{
					echo $this->loadTemplate("filter_bootstrap");
				}
				else
				{
					echo $this->loadTemplate("filter");
				}
			}
		?>

		<div >
			<table class="adminlist">
			<thead>
				<tr>
													<?php
															foreach($columnsArray as $fieldindex => $field)
															{
																if($field==="location")
																{
																	?>
																		<th class="title">
																			<?php echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_LOCATION'), 'loc.title', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations"); ?>
																		</th>
																	<?php
																}
																else if ($field === "events")
																{
																			if ($locparams->get('linktocalendar',1))
																			{?>
																			<th>
																					<?php echo JText::_('COM_JEVLOCATIONS_LOCATION_EVENTS'); ?>
																			</th>
																	<?php
																			 }
																}
																else if ($field === "image")
																{
																	   if ($locparams->get('showimage',1)){ ?>
																			<th>
																				<?php
																				echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_LOCATION_IMAGE'), 'loc.image', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations");
																				?>
																			</th>
																	<?php
																			 }
																}
																else if ($field === "country")
																{
																		?>
																			<th>
																						<?php
																						if (!$usecats) echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_COUNTRY'), 'loc.country', $this->lists['order_Dir'], $this->lists['order'], "locations.locations" );
																						else  echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_COUNTRY'), 'cc1.title', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations");
																						?>
																			</th>
																	<?php

																}
																else if ($field === "state")
																{
																	   ?>
																			<th>
																					<?php
																					if (!$usecats) echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_STATE'), 'loc.state', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations");
																					else echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_STATE'), 'cc2.title', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations");
																					?>
																			</th>
																	<?php

																}
																else if ($field === "city")
																{
																	   ?>
																			<th>
																					<?php
																					if (!$usecats) echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_CITY'), 'loc.city', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations");
																					else echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_CITY'), 'cc3.title', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations");
																					?>
																			</th>
																	<?php

																}
																else if ($field === "phone")
																{
																	   ?>
																			<th>
																					<?php echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_PHONE'), 'loc.phone', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations"); ?>
																			</th>
																	<?php

																}
																else if ($field === "url")
																{
																	   ?>
																			<th>
																					<?php echo JText::_('COM_JEVLOCATIONS_LOCATION_WEBSITE'); ?>
																			</th>
																	<?php

																}
																else if ($field === "street")
																{
																	   ?>
																			<th>
																					<?php echo JText::_('COM_JEVLOCATIONS_STREET'); ?>
																			</th>
																	<?php

																}
																else if ($field === "postcode")
																{
																	   ?>
																			<th>
																					<?php echo JHTML::_('grid.sort',  JText::_('COM_JEVLOCATIONS_POSTCODE'), 'loc.postcode', $this->lists['order_Dir'], $this->lists['order'] , "locations.locations"); ?>
																			</th>
																	<?php

																}
                                                                                                                                else if (isset($cfarray[$field])){
																	?>
																			<th>
																					<?php
																					echo JText::_($cfarray[$field]);
																					?>
																			</th>
																	<?php
                                                                                                                                }
															}
													?>
				</tr>
			</thead>
	        <?php if($this->pagination->pagesTotal > 1) : ?>
			<tfoot>
				<tr>
					<td colspan="<?php echo sizeof($columnsArray); ?>">
						<?php echo $this->pagination->getListFooter(); ?>
					</td>
				</tr>
			</tfoot>
	        <?php endif; ?>
			<tbody>
			<?php

			$targetid = intval($locparams->get("targetmenu",0));
			//we check target menu item param in global configuration if no menu param
			if($targetid == 0)
			{
				$targetid = intval($locparams->get("target_itemid",0));
			}


			if ($targetid>0){
				$menu =  JFactory::getApplication()->getMenu();
				$targetmenu = $menu->getItem($targetid);
				if ($targetmenu->component!="com_jevents"){
					$targetid = JEVHelper::getItemid();
				}
				else {
					$targetid = $targetmenu->id;
				}
			}
			else {
				$targetid = JEVHelper::getItemid();
			}
			$task = $locparams->get("jevview","month.calendar");


			$k = 0;
			for ($i=0, $n=count( $this->items ); $i < $n; $i++)
			{
				$row = &$this->items[$i];

				$tmpl = "";
				if (JRequest::getString("tmpl","")=="component"){
					$tmpl = "&tmpl=component";
				}

				$link 	= JRoute::_( 'index.php?option=com_jevlocations&task=locations.detail&loc_id='. $row->loc_id . $tmpl ."&se=1"."&title=".JApplication::stringURLSafe($row->title));
				$targetmenu = $row->targetmenu>0?$row->targetmenu:$targetid;
				$eventslink = JRoute::_("index.php?option=com_jevents&task=$task&loclkup_fv=".$row->loc_id."&Itemid=".$targetmenu);

				$priority = ( ($row->priority > 0)? "priority-".$row->priority : "" );

				// global list
				$global	= $this->_globalHTML($row,$i);

				$ordering = ($this->lists['order'] == 'loc.ordering');
				if ($this->usecats){
					if(isset($row->c3title)){
						$country = $row->c3title;
						$province = $row->c2title;
						$city = $row->c1title;
					}
					else if(isset($row->c2title)){
						$country = $row->c2title;
						$province = $row->c1title;
						$city = false;
					}
					else {
						$country = $row->c1title;
						$province = false;
						$city = false;
					}
				}
				else {
					$country = $row->country;
					$province = $row->state;
					$city = $row->city;
				}
				?>
				<tr class="<?php echo "row$k $priority"; ?>">
													<?php
															 foreach($columnsArray as $field)
															{
																if($field==="location")
																{
													?>
					<td>
						<span class="editlinktip hasTip" title="<?php echo JText::_( 'COM_JEVLOCATIONS_VIEW_LOCATION' );?>::<?php echo $this->escape($row->title); ?>">
							<a href="<?php echo $link; ?>">
								<?php echo $this->escape($row->title); ?></a>
						</span>
					</td>
													<?php
																}
																else if($field==="events")
																{
																	if ($locparams->get('linktocalendar',1)){
													?>
					<td align="center">
						<?php if ($row->hasEvents) {?>
											<?php if (!$locparams->get('showlocationlatestevents',0)): ?>
												<span class="editlinktip hasTip" title="<?php echo JText::_( 'COM_JEVLOCATIONS_VIEW_EVENTS_AT' );?>::<?php echo $this->escape($row->title); ?>">
														<a href="<?php echo $eventslink; ?>">
																<img src="<?php echo JURI::base();?>components/com_jevlocations/assets/images/jevents_event_sml.png" alt="Calendar" style="height:24px;margin:0px;"/>
																											</a>
												</span>
											<?php else: ?>
												<?php
													$loclkup_fv = JRequest::setVar("loclkup_fv", $row->loc_id);
                                                                                                        JevHelper::setMenuFilter("loclkup_fv", $row->loc_id);
													$locparams->set("extras0", "jevl:".$row->loc_id);
													$locparams->set("target_itemid", $targetmenu);
													$registry->set("jevents.moduleparams", $locparams);
													$modview = new $viewclass($locparams, "locblog".$row->loc_id);
                                                                                                        $modview->modid="locblog".$row->loc_id;
													JRequest::setVar("loclkup_fv", $loclkup_fv);
                                                                                                        JevHelper::clearMenuFilter("loclkup_fv");

													echo "<br style='clear:both'/>";

													$task = $locparams->get("jevview", "month.calendar");
													$link = JRoute::_("index.php?option=com_jevents&task=$task&loclkup_fv=" . $row->loc_id . "&Itemid=" . $targetmenu);

													echo "<strong>" . JText::sprintf("COM_JEVLOCATIONS_ALL_EVENTS", $link) . "</strong>";

												?>
											<?php endif; ?>
						<?php } ?>
					</td>
					<?php
																	}
																}
																else if($field==="image")
																{
																	  if ($locparams->get('showimage',1)){ ?>
																			<td align="center">
																					<?php
																					if ($row->image!=""){
																							$thimg = '<img src="'.$mediabase.'/'.$folder.'/thumbnails/thumb_'.$row->image.'" />' ;
																							?>
																					<span class="editlinktip hasTip" title="<?php echo JText::_( 'COM_JEVLOCATIONS_VIEW_LOCATION' );?>::<?php echo $this->escape($row->title); ?>">
																							<a href="<?php echo $link; ?>"><?php	echo $thimg; ?></a>
																					</span>
																					<?php
																					}
																					?>
																			</td>
					<?php }
																}
																else if($field==="country")
																{
																?>
								<td>
									<?php echo $this->escape($country); ?>
								</td>
																<?php }
																 else if($field==="state")
																{?>
					<td>
						<?php echo $this->escape($province); ?>
					</td>
													<?php }
																 else if($field==="city")
																{?>
					<td>
						<?php echo $this->escape($city); ?>
					</td>
													<?php
																}
																else if($field==="phone")
															   {?>
				   <td>
					   <?php echo $row->phone; ?>
				   </td>
												   <?php
															   }
															   else if($field==="url")
															  {?>
				  <td>
					  <?php echo JevLocationsHTML::getURLElement($row->url); ?>
				  </td>
												  <?php
															  }
															  else if($field==="street")
															 {?>
				 <td>
					 <?php echo $row->street; ?>
				 </td>
												 <?php
															 }
															  else if($field==="postcode")
															 {?>
				 <td>
					 <?php echo $row->postcode; ?>
				 </td>
												 <?php
															 }
																 else
																{?>
					<td>
						<?php
                                                echo isset($row->customfields[$field]) ? $this->escape($row->customfields[$field]["value"]) : "";
                                                ?>
					</td>
													<?php
																}

															}?>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</tbody>
			</table>
		</div>

		<?php if ($locparams->get("showmap",0)) echo $this->loadTemplate("map") ?>

			<input type="hidden" name="option" value="com_jevlocations" />
			<input type="hidden" name="Itemid" value="<?php echo $Itemid;?>" />
			<input type="hidden" name="task" value="locations.locations" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
			<?php if (JRequest::getString("tmpl","")=="component"): ?>
			<input type="hidden" name="tmpl" value="component" />
			<?php endif; ?>
			<?php echo JHTML::_( 'form.token' ); ?>
		</form>
	</div>
<?php }
echo JevHtmlBootstrap::tooltip('.hasTip');
