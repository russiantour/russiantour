<?php 
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined('_JEXEC' ) or die('Restricted access'); 

?>

<div class="jevlocations-filter row-fluid"  style="list-style-type: none ">
	<div class="jevlocations-filter-text span3">
		<?php echo JText::_( 'COM_JEVLOCATIONS_FILTER' ); ?>:<br/>
		<input type="text" class="text-area input-medium" name="search" id="jevsearch" value="<?php echo $this->lists['search'];?>" class="text_area" onchange="document.adminForm.submit();" />
	</div>
	<div class="jevlocations-filter-category span3">
		<?php
			echo JText::_( 'COM_JEVLOCATIONS_FILTER_CATEGORY_LABEL' ). ":";
			echo $this->lists['loccat'];
		?>
	</div>
	<?php if($this->usecats):?>
		<div class="jevlocations-filter-cities span3">
			<?php
			echo JText::_("COM_JEVLOCATIONS_ALL_CITIES"). ":";
			echo $this->lists['catid'];
			?>
		</div>
	<?php endif; ?>
	<?php $cities = false; ?>
	<div class="jevlocations-filter-buttons span2">
		<button class="jevlocations-filter-submit-button btn btn-primary btn-large btn-block btn-small" onclick="this.form.submit();"><?php echo JText::_( 'COM_JEVLOCATIONS_GO' ); ?></button>
		<button class="jevlocations-filter-reset-button btn btn-large btn-block btn-small" onclick="document.getElementById('jevsearch').value='';
						this.form.getElementById('filter_loccat').value='0';
						<?php if ($cities) : ?> this.form.getElementById('loccity_fv').value=''; <?php endif; ?>
						this.form.submit();"><?php echo JText::_( 'COM_JEVLOCATIONS_RESET' ); ?></button>
	</div>

</div>
