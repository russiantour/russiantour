<?php 
defined('_JEXEC' ) or die('Restricted access');

if (JRequest::getInt("pop",0)) return;

$compparams = JComponentHelper::getParams("com_jevlocations");

$googleurl = JevLocationsHelper::getApiUrl(); //$compparams->get("googlemaps",'http://maps.google.com');
JevLocationsHelper::loadApiScript();

$task = JRequest::getString("jevtask","");

if (!$this->items || count($this->items)==0) return;

$zoom = 10;
$document = JFactory::getDocument();
$document->addStyleDeclaration("div.mainlocmap {clear:left;} div#gmapMulti{margin:5px auto} #gmapMulti img { max-width: inherit;}");
$maptype =  $compparams->get("maptype", "ROADMAP") ;
$disableautopan = $compparams->get("autopan", 1)  ? "false":"true";
$redirecttodirections = $compparams->get("redirecttodirections", 0);
$googleDirections = $compparams->get('googledirections', 'https://maps.google.com');
$maplinknewwindow = $compparams->get('maplinktonewwindow', '0');

?>
<div class='mainlocmap'>
<?php
$root = JURI::root();
$Itemid = JRequest::getInt("Itemid");
$script = "var urlroot = '".JURI::root()."media/com_jevlocations/images/';\n";
$script.=<<<SCRIPT
var myMapMulti = false;


function addPoint(lat, lon, locid, loctitle, locurl, evttitle, icon){
		// Create our "tiny" marker icon
		var blueIcon = new google.maps.MarkerImage(urlroot + icon,
		// This marker is 32 pixels wide by 32 pixels tall.
		new google.maps.Size(32, 32),
		// The origin for this image is 0,0 within a sprite
		new google.maps.Point(0,0),
		// The anchor for this image is the base of the flagpole at 0,32.
		new google.maps.Point(16, 32));
		                
		// Set up our GMarkerOptions object
		var point = new google.maps.LatLng(lat,lon);
		markerOptions = { icon:blueIcon, draggable:false , map:myMapMulti,  icon:blueIcon, disableAutoPan:$disableautopan, position:point};
		
		var myMarkerMulti = new google.maps.Marker(markerOptions);
		// use 
		var infowindow = new google.maps.InfoWindow({
			disableAutoPan:$disableautopan,
			size: new google.maps.Size(150,50),
			content: "<div style='color:rgb(134,152,150);font-weight: bold;max-width:300px!important;'>"+evttitle+"<br/><div style='color:#454545;font-weight:normal;max-height:200px;overflow:hidden;'>"+loctitle+"</div></div>"
		});
		google.maps.event.addListener(myMarkerMulti, "mouseover", function(e) {
			  infowindow.open(myMapMulti,myMarkerMulti);
		});
		google.maps.event.addListener(myMarkerMulti, "mouseout", function(e) {
			  infowindow.close(myMapMulti,myMarkerMulti);
		});
		google.maps.event.addListener(myMarkerMulti, "click", function(e) {
			if($maplinknewwindow){
				window.open(locurl,"_-blank");
			}
			else{
				jQuery(location).attr('href', locurl);
			}

		});

}

function myMaploadMulti(){
		
SCRIPT;
$minlon = 0;
$minlat = 0;
$maxlon = 0;
$maxlat = 0;
$first = true;
foreach ($this->items as $location) {
	if ($location->geozoom==0) continue;
	if ($first){
		$minlon = floatval($location->geolon);
		$minlat = floatval($location->geolat);
		$maxlon = floatval($location->geolon);
		$maxlat = floatval($location->geolat);
		$first=false;
	}
	$minlon = floatval($location->geolon)>$minlon?$minlon:floatval($location->geolon);
	$minlat = floatval($location->geolat)>$minlat?$minlat:floatval($location->geolat);
	$maxlon = floatval($location->geolon)<$maxlon?$maxlon:floatval($location->geolon);
	$maxlat = floatval($location->geolat)<$maxlat?$maxlat:floatval($location->geolat);
}
if ($minlon==$maxlon){
	$minlon-=0.002;
	$maxlon+=0.002;
}
if ($minlat==$maxlat){
	$minlat-=0.002;
	$maxlat+=0.002;
}
$midlon = ($minlon + $maxlon)/2.0;
$midlat = ($minlat + $maxlat)/2.0;

$script.=<<<SCRIPT

var myOptions = {
	// disable mouse scroll wheel
	// scrollwheel: false,
	center: new google.maps.LatLng($midlat,$midlon),
	mapTypeId: google.maps.MapTypeId.$maptype
}

myMapMulti = new google.maps.Map(document.getElementById("gmapMulti"),myOptions );

var bounds = new google.maps.LatLngBounds(new google.maps.LatLng($minlat,$minlon), new google.maps.LatLng($maxlat,$maxlon));

SCRIPT;
foreach ($this->items as $location) {
	if ($location->loc_id==0) continue;
	$title = $location->title;
	// for the description use this
	$description = $location->description;

	$locUrl = JRoute::_("index.php?option=com_jevlocations&task=locations.detail&se=1&Itemid=" . $Itemid ."&loc_id=" . $location->loc_id);
	// for the address use this 
	//$description = $location->street."<br/>".$location->city."<br/>".$location->state."<br/>".$location->postcode."<br/>".$location->country;
	$script.="	addPoint($location->geolat,	$location->geolon, $location->loc_id,
							'" . addslashes(str_replace(array("\n","\r"),"",$description)) . "',
							'" . $locUrl."', '".addslashes(str_replace("\n","",$title)). "', '".$location->mapicon."');\n";
 	}
	
$script.=<<<SCRIPT
   myMapMulti.fitBounds(bounds);
};
		
jQuery(document).ready(function (){window.setTimeout("myMaploadMulti()",1000);});
SCRIPT;
$document = JFactory::getDocument();
$document->addScriptDeclaration($script);

?>
<div id="gmapMulti" style="width: 600px; height:600px;overflow:hidden;"></div>

</div>