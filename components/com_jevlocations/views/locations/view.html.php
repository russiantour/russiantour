<?php

/**
 * copyright (C) 2008 JEV Systems Ltd - All rights reserved
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC' ) or die();

include_once(JPATH_COMPONENT_ADMINISTRATOR . "/views/" . basename(dirname(__FILE__)) . "/" . basename(__FILE__));
JLoader::register('JevLocationsHTML', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/jevlocationshtml.php");

/**
 * HTML View class for the component
 *
 * @static
 */
class FrontLocationsViewLocations extends AdminLocationsViewLocations
{

	function __construct($config = array())
	{
		parent::__construct($config);

	}

	function locations($tpl = null)
	{
		JHtml::stylesheet('com_jevlocations/pagination.css',array(),true);
		JHtml::stylesheet('com_jevlocations/jevlocations.css',array(),true);
		// make sure sorting JS is loaded
		$user =  JFactory::getUser();

		JHtmlBehavior::core();
		JevHtmlBootstrap::framework();
		JevHtmlBootstrap::loadCss();

		JHtml::script('com_jevlocations/geolocation.js', false, true, false, false, true);

		JLoader::register('JEventsHTML', JPATH_SITE . "/components/com_jevents/libraries/jeventshtml.php");

		$mainframe = JFactory::getApplication();
		$option = JRequest::getCmd("option");

		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}

		$db = JFactory::getDBO();
		$uri =  JFactory::getURI();

		$itemid = JFactory::getApplication()->input->get("Itemid",0,"int");
		$filter_order = $mainframe->getUserStateFromRequest($option . 'loc_filter_order'.$itemid, 'filter_order', 'loc.ordering', 'cmd');
		$filter_order_Dir = $mainframe->getUserStateFromRequest($option . 'loc_filter_order_Dir'.$itemid, 'filter_order_Dir', '', 'word');
		$filter_cityid = $mainframe->getUserStateFromRequest($option . 'loc_filter_catid'.$itemid, 'filter_catid', 0, 'int');
		// table ordering
		$lists['order_Dir'] = $filter_order_Dir;
		$lists['order'] = $filter_order;

		$filter_catid = $mainframe->getUserStateFromRequest($option . 'loc_filter_loccat'.$itemid, 'filter_loccat', 0, 'int');
		$javascript = 'onchange="document.adminForm.submit();" size="1"';

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);
		$multicatfilters = $compparams->get("multicatfilter", array());
		if (is_array($multicatfilters))
		{
			JArrayHelper::toInteger($multicatfilters);
		}
		else if ($multicatfilters != "")
		{
			$multicatfilters = array(intval($multicatfilters));
		}
		$multicatfilters = implode(",",$multicatfilters);

		JLoader::register('JevLocationsHTML', JPATH_ADMINISTRATOR . "/components/com_jevlocations/libraries/jevlocationshtml.php");
		$lists['loccat'] = JevLocationsHTML::buildCategorySelect2(intval($filter_catid), $javascript, $multicatfilters, false, false, 0, 'filter_loccat', 'com_jevlocations',false);
		$lists['loccat'] = str_replace(JText::_('JEV_EVENT_ALLCAT'), JText::_('ALL_CATEGORIES'), $lists['loccat']);

		if ($usecats)
		{
			$lists['catid'] = JevLocationsHTML::buildCategorySelect(intval($filter_cityid), $javascript, "", false, false, 0, 'filter_catid', 'com_jevlocations');
			$lists['catid'] = str_replace(JText::_('COM_JEVLOCATIONS_ALL_CATEGORIES'), JText::_("COM_JEVLOCATIONS_ALL_CITIES"), $lists['catid']);
		}
		$search = $mainframe->getUserStateFromRequest($option . 'loc_search'.$itemid, 'search', '', 'string');
		$search = JString::strtolower($search);
		// search filter
		$lists['search'] = $search;

		$compparams = JComponentHelper::getParams("com_jevlocations");
		$usecats = $compparams->get("usecats", 0);
		$this->assignRef('usecats', $usecats);

		$catfilters = $compparams->get("catfilter", "");

		if (is_array($catfilters) || ($catfilters != ""))
		{
			if (JFactory::getUser()->authorise('core.admin'))
			{
				JFactory::getApplication()->enqueueMessage(
					JText::_("COM_JEVLOCATIONS_OLD_CATEGORIES_WARNING")	. "<br/>" . JText::_("JEV_ONLY_ADMIN_MESSAGE"),
					'error'
				);
			}
		}

		$model = $this->getModel();
		$limitstart = JRequest::getInt("limitstart", 0) === 0 ? JRequest::getInt("locations_limitstart", 0) : JRequest::getInt("limitstart", 0);
		$model->setState("limitstart", $limitstart);
		$totalItems = $model->getPublicTotal();
		$items = $model->getPublicData();
		$pagination = $model->getPublicPagination();

		// Check if location has any events	- a very crude test
		jimport("joomla.utilities.date");
		$startdate = new JDate("-" . $compparams->get("checkeventbefore", 30) . " days");
		$enddate = new JDate("+" . $compparams->get("checkeventafter", 30) . " days");

		foreach ($items as &$item)
		{
			if ($compparams->get('checkevents', 1))
			{
				$item->hasEvents = $model->hasEvents($item->loc_id, $startdate->toSql(), $enddate->toSql());
			}
			else
			{
				$item->hasEvents = 1;
			}

			unset($item);
		}

		$this->assignRef('items', $items);
		$this->assignRef('lists', $lists);
		$this->assignRef('usecats', $usecats);
		$this->assignRef('pagination', $pagination);

		// Call the MetaTag setter function.
		JevLocationsHelper::SetMetaTags();

		parent::display($tpl);

	}

	function overview($tpl = null)
	{
		JLoader::register('JToolbarHelper', JPATH_ADMINISTRATOR . "/" . 'includes' . "/" . 'toolbar.php');
		JHtml::stylesheet('com_jevlocations/admin.css',array(),true);
		JHtml::stylesheet('com_jevlocations/pagination.css',array(),true);

		$model =  $this->getModel();
		$limitstart = JRequest::getInt("limitstart", 0) === 0 ? JRequest::getInt("locations_limitstart", 0) : JRequest::getInt("limitstart", 0);
		$model->setState("limitstart", $limitstart);
		$model->setState("limit", JRequest::getInt("limit", 0));

		parent::overview($tpl);

	}

	function select($tpl = null)
	{
		// Make sure form stuff is loaded
		$user =  JFactory::getUser();

		// Include jQuery framework
		JHtml::_('jquery.framework');
		JHtml::_('jquery.ui', array('core'));
		JHtml::_('bootstrap.framework');
		JHtml::_('bootstrap.loadCss');

		JLoader::register('JToolbarHelper', JPATH_ADMINISTRATOR . "/" . 'includes' . "/" . 'toolbar.php');
		JHtml::stylesheet('com_jevlocations/admin.css',array(),true);
		JHtml::stylesheet('com_jevlocations/pagination.css',array(),true);

		$model =  $this->getModel();

		parent::select($tpl);

	}

	function edit($tpl = null)
	{
		JLoader::register('JToolbarHelper', JPATH_ADMINISTRATOR . "/" . 'includes' . "/" . 'toolbar.php');
		JHtml::stylesheet('com_jevlocations/admin.css',array(),true);
		JHtml::stylesheet('com_jevlocations/pagination.css',array(),true);

		parent::edit($tpl);

	}

	function loadedFromTemplate($template_name, $location, $mask = false)
	{

		$db = JFactory::getDBO();

		JevLocationsHelper::SetMetaTags();

		// find published template
		static $templates;
		if (!isset($templates))
		{
			$templates = array();
		}
		// Lets check if we have editted before! if not... rename the custom file.
		if (JFile::exists(JPATH_SITE . "/components/com_jevents/assets/css/jevcustom.css"))
		{
			// It is definitely now created, lets load it!
			JEVHelper::stylesheet('jevcustom.css', 'components/' . JEV_COM_COMPONENT . '/assets/css/');
		}
		if (!array_key_exists($template_name, $templates))
		{
			$db->setQuery("SELECT * FROM #__jev_defaults WHERE state=1 AND name= " . $db->Quote($template_name) . " AND " . 'language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
			$templates[$template_name] = $db->loadObjectList("language");
			if (isset($templates[$template_name][JFactory::getLanguage()->getTag()]))
			{
				$templates[$template_name] = $templates[$template_name][JFactory::getLanguage()->getTag()];
			}
			else if (isset($templates[$template_name]["*"]))
			{
				$templates[$template_name] = $templates[$template_name]["*"];
			}
			else if (is_array($templates[$template_name]) && count($templates[$template_name])>0)
			{
				$templates[$template_name] = current($templates[$template_name]);
			}
			else {
				$templates[$template_name] = null;
			}
		}

		if (is_null($templates[$template_name]) || !$templates[$template_name] || $templates[$template_name]->value == "")
			return false;

		$template = $templates[$template_name];

		$jevparams = JComponentHelper::getParams(JEV_COM_COMPONENT);

		$locationsList = array();
		//We create a container for the locations so that we do not have to duplicate the code that performs the magic ;o)
		if (is_string($location))
		{
			$actualLayout = $location;
			foreach ($this->items as $location)
			{
				$locationsList [] = $location;
			}
			$layout = 'locations'; //We set the same layout as in the regular list layout to use the same templates and logic

			$this->setLayout($layout);
		}
		else
		{
			$actualLayout = 'detail';
			$locationsList [] = $location;
			$layout = 'detail'; // simulate event detail page to get ALL the fields
		}

		// Location fields
		$compparams = JComponentHelper::getParams("com_jevlocations");
		if ($compparams->get("gwidth", -1) == -1)
		{
			$jevplugin = JPluginHelper::getPlugin("jevents", "jevlocations");
			JPluginHelper::importPlugin("jevents", $jevplugin->name);
			$jevpluginparams = new JRegistry($jevplugin->params);
		}
		else
		{
			$jevpluginparams = $compparams;
		}
		// fill in any missing fields
		$detailpopup = $jevpluginparams->get("detailpopup", 1);
		$map = '<div id="gmap" style="width:' . $jevpluginparams->get("gwidth", 200) . 'px; height:' . $jevpluginparams->get("gheight", 150) . 'px;overflow:hidden !important;"></div>';
		JFactory::getDocument()->addStyleDeclaration(" #gmap img { max-width: none !important;}");

		$Itemid = JRequest::getInt("Itemid");

		$usecats = $compparams->get("usecats", 0);

		$locationsCount = sizeof($locationsList);

		$locationsTemplate = array();

		foreach ($locationsList as $location)
		{
			$template_value = $template->value;
			// strip carriage returns other wise the preg replace doesn;y work - needed because wysiwyg editor may add the carriage return in the template field
			$template_value = str_replace("\r", '', $template_value);
			$template_value = str_replace("\n", '', $template_value);
			// non greedy replacement - because of the ?
			$template_value = preg_replace_callback('|{{.*?}}|', array($this, 'cleanLabels'), $template_value);

			$location->map = $map;
			$loc_id = $location->loc_id;

			if ($detailpopup)
			{
				$locurl = JRoute::_("index.php?option=com_jevlocations&task=locations.detail&tmpl=component&loc_id=$loc_id&title=" . JApplication::stringURLSafe($location->title));
			}
			else
			{
				$locurl = JRoute::_("index.php?option=com_jevlocations&task=locations.detail&se=1&loc_id=$loc_id&title=" . JApplication::stringURLSafe($location->title));
			}

			$document = JFactory::getDocument();
			$pwidth = $jevpluginparams->get("pwidth", "750");
			$pheight = $jevpluginparams->get("pheight", "500");

			$modalSelector = "jevlocationModal";
			$modalCss = "#".$modalSelector.'{width: '.$pwidth.'px;}';
			$document->addStyleDeclaration($modalCss);

			$modalScript = 'jevModalPopup(\''.$modalSelector.'\', \'' . $locurl . '\', \'\');';

			if ($detailpopup)
			{
				$location->linkstart = "<a href=\"javascript:jevModalPopup('$modalSelector','$locurl','');\">";
			}
			else
			{
				$location->linkstart = "<a href='$locurl'>";
			}

			if ($usecats)
			{
				if (isset($location->c1title))
					$location->city = $location->c1title;
				if (isset($location->c2title))
					$location->state = $location->c2title;
				if (isset($location->c3title))
					$location->country = $location->c3title;
			}

			// pass location through content plugins
			JPluginHelper::importPlugin('content');
			// pass location through jevents plugins
			JPluginHelper::importPlugin('jevents');
			$tmprow = new stdClass();
			$tmprow->text = $location->description;
			$params = new JRegistry(null);
			$dispatcher = JEventDispatcher::getInstance();
			$dispatcher->trigger('onContentPrepare', array('com_jevents', &$tmprow, &$params, 0));
			$location->description = $tmprow->text;
			$dispatcher->trigger('onLocationDisplay', array(&$location));

			// Make links live
			$location->rawurl = $location->url;
			$pattern = '[a-zA-Z0-9&?_.,=%\-\/]';
			if (strpos($location->url, "http://") === false && trim($location->url)!=""  && trim($location->url)!="/")
				$location->url = "http://" . trim($location->url);
			if (trim($location->url)!="") {
				$location->url = preg_replace('#(http://)(' . $pattern . '*)#i', '<a href="\\1\\2"  target="_blank">\\1\\2</a>', $location->url);
			}

			$event = new stdClass();
			$event->_jevlocation = $location;
			$event->_location = $location;
			// now replace the fields
			$search = array();
			$replace = array();
			$blank = array();

			$jevLocationsPlugins = array("locations", "jcomments", "facebooksocial");
			foreach ($jevLocationsPlugins as $pluginName)
			{
				$classname = "plgJeventsJev".$pluginName;
				if ($classname && is_callable(array($classname, "substitutefield")))
				{
					$fieldNameArray = call_user_func(array($classname, "fieldNameArray"), $layout);
					if (isset($fieldNameArray["values"]))
					{
						foreach ($fieldNameArray["values"] as $fieldname)
						{
							$search[] = "{{" . $fieldname . "}}";
							$replace[] = call_user_func(array($classname, "substitutefield"), $event, $fieldname);
							if (is_callable(array($classname, "blankfield")))
							{
								$blank[] = call_user_func(array($classname, "blankfield"), $event, $fieldname);
							}
							else
							{
								$blank[] = "";
							}

							//We need to add also old CF tags for backwards compatibility
							$isNewCF = 0;
							$oldCustomField = str_replace("lcf_", "", $fieldname, $isNewCF);
							if($isNewCF)
							{
								$search[] = "{{" . $oldCustomField . "}}";
								$replace[] = call_user_func(array($classname, "substitutefield"), $event, $oldCustomField);
								if (is_callable(array($classname, "blankfield")))
								{
									$blank[] = call_user_func(array($classname, "blankfield"), $event, $oldCustomField);
								}
								else
								{
									$blank[] = "";
								}
							}
						}
					}
				}
			}

			$classname = "plgJEventsjevjcomments";
			if (false && $classname && is_callable(array($classname, "substitutefield")))
			{
				$fieldNameArray = call_user_func(array($classname, "fieldNameArray"), $layout);
				$fieldNameArray2 = call_user_func(array($classname, "fieldNameArray"), 'location');
				if (isset($fieldNameArray["values"]) && isset($fieldNameArray2["values"]))
				{
					$fieldNameArray["values"] = array_merge($fieldNameArray["values"], $fieldNameArray2["values"]);
					foreach ($fieldNameArray["values"] as $fieldname)
					{
						$search[] = "{{" . $fieldname . "}}";
						$replace[] = call_user_func(array($classname, "substitutefield"), $event, $fieldname);
						if (is_callable(array($classname, "blankfield")))
						{
							$blank[] = call_user_func(array($classname, "blankfield"), $event, $fieldname);
						}
						else
						{
							$blank[] = "";
						}
					}
				}
			}

			// word counts etc.
			for ($s = 0; $s < count($search); $s++)
			{
				if (strpos($search[$s], "TRUNCATED_DESC:.*?") > 0)
				{
					global $tempreplace, $tempevent, $tempsearch;
					$tempreplace = $replace[$s];
					$tempsearch = $search[$s];
					$tempevent = $event;
					$template_value = preg_replace_callback("|$tempsearch|", array($this, 'jevSpecialHandling'), $template_value);
				}
			}

			for ($s = 0; $s < count($search); $s++)
			{
				global $tempreplace, $tempevent, $tempsearch, $tempblank;
				$tempreplace = $replace[$s];
				$tempblank = $blank[$s];
				$tempsearch = str_replace("}}", "#", $search[$s]);
				$tempevent = $event;
				$template_value = preg_replace_callback("|$tempsearch(.+?)}}|", array($this, 'jevSpecialHandling2'), $template_value);
			}

			$template_value = str_replace($search, $replace, $template_value);

			// non greedy replacement - because of the ?
			$template_value = preg_replace_callback('|{{.*?}}|', array($this, 'cleanUnpublished'), $template_value);

			$locationsTemplate [] = $template_value;
		}

		$final_template = "<form action=\"" . JRoute::_("index.php?option=com_jevlocations&task=locations.locations&layout=locations_blog&Itemid=$Itemid") . "\" method=\"post\" id=\"adminForm\" name=\"adminForm\">";
		if ($actualLayout == "bloglist")
		{

			if ($compparams->get("showfilters", 1))
			{
				$filterCode = "<div class=\"jevloc-category-filter\">";
				$filterCode .= JText::_('COM_JEVLOCATIONS_FILTER') . ": ";
				$filterCode .="<input class=\"inputbox\" type=\"text\" name=\"search\" id=\"jevsearch\" value=\"" . $this->lists['search'] . "\" class=\"text_area\" onchange=\"document.adminForm.submit();\" /> ";

				$cities = false;
				/*
				$db = JFactory::getDbo();
				$db->setQuery("SELECT DISTINCT city as value, city as text FROM #__jev_locations where city <> '' ORDER BY city ASC");
				$cities = $db->loadObjectList();
				 if ($cities) {
					array_unshift($cities, JHTML::_('select.option', '', 'Select City', 'value', 'text' ));
					$loccity_fv = JFactory::getApplication()->getUserStateFromRequest($option . 'loc_city_fv', 'loccity_fv', "", 'string');
					$filterCode .=  JHTML::_('select.genericlist', $cities, "loccity_fv", ' onchange="document.adminForm.submit();" ', 'value', 'text', $loccity_fv );
				 }
				 */
				$filterCode .= $this->lists['loccat'];

				$filterCode .= "<button onclick=\"this.form.submit();\">" . JText::_('COM_JEVLOCATIONS_GO') . "</button> ";
				$filterCode .= "<button onclick=\"document.getElementById('jevsearch').value='';"
						. "this.form.getElementById('filter_loccat').value='0';"
						.  ($cities?  "this.form.getElementById('loccity_fv').value=''; " : "")
						. "this.form.submit();\">" . JText::_('COM_JEVLOCATIONS_RESET') . "</button> ";
				$filterCode .= "</div>";
				$final_template .=$filterCode;
			}

			$final_template .= "<div class='jevlocation_blog_items'>\n";
			foreach ($locationsTemplate as $locationTemplate)
			{
				$final_template .= "<div class=\"jevloc-container\">" . $locationTemplate . "</div>";
			}
			$final_template .= "</div>\n";
			if ($compparams->get("showmap", 0))
			{
				$final_template .= $this->loadTemplate("map");
			}
		}
		else
		{
			$final_template = $template_value;
		}
		if (isset($this->pagination) && $this->pagination->total>$this->pagination->limit)
		{
			$final_template .= '<div style="width:100%" class="jevpagination">';
			$final_template .= $this->pagination->getListFooter();
			$final_template .= '</div>';
		}
		$final_template .="</form>";

		$final_template = JHTML::_('content.prepare',$final_template, null,"com_jevents");
		echo $final_template;

		return true;

	}

	function cleanLabels($matches)
	{
		if (count($matches) == 1)
		{
			$parts = explode(":", $matches[0]);
			if (count($parts) > 0)
			{
				if (strpos($matches[0], "://") > 0)
				{
					return "{{" . $parts[count($parts) - 1];
				}
				array_shift($parts);
				return "{{" . implode(":", $parts);
			}
			return "";
		}
		return "";

	}

	function cleanUnpublished($matches)
	{
		if (count($matches) == 1)
		{
			return "";
		}
		return $matches;

	}

	function jevSpecialHandling($matches)
	{
		if (count($matches) == 1 && strpos($matches[0], ":") > 0)
		{
			global $tempreplace, $tempevent, $tempsearch;
			$parts = explode(":", $matches[0]);
			if (count($parts) == 2)
			{
				$wordcount = intval(str_replace("}}", "", $parts[1]));
				$value = strip_tags($tempreplace);

				$value = str_replace("  ", " ", $value);
				$words = explode(" ", $value);
				if (count($words) > $wordcount)
				{
					$words = array_slice($words, 0, $wordcount);
					$words[] = " ...";
				}
				return implode(" ", $words);
			}
			else
			{
				return $matches[0];
			}
		}
		else if (count($matches) == 1)
			return $matches[0];

	}

	function jevSpecialHandling2($matches)
	{
		if (count($matches) == 2 && strpos($matches[0], "#") > 0)
		{
			global $tempreplace, $tempevent, $tempsearch, $tempblank;
			$parts = explode("#", $matches[1]);
			if ($tempreplace == $tempblank)
			{
				if (count($parts) == 2)
				{
					return $parts[1];
				}
				else
					return "";
			}
			else if (count($parts) >= 1)
			{
				return sprintf($parts[0], $tempreplace);
			}
		}
		else
			return "";

	}

}
