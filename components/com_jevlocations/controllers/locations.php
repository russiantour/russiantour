<?php
/**
 * JEvents Component for Joomla 1.5.x
 *
 * @version     $Id$
 * @package     JEvents
 * @copyright   Copyright (C) 2006-2008 JEvents Project Group
 * @license     GNU/GPLv2, see http://www.gnu.org/licenses/gpl-2.0.html
 * @link        http://joomlacode.org/gf/project/jevents
 */

defined( 'JPATH_BASE' ) or die( 'Direct Access to this location is not allowed.' );

include_once(JPATH_COMPONENT_ADMINISTRATOR."/controllers/".basename(__FILE__));

class FrontLocationsController extends AdminLocationsController {
	function __construct($config = array())
	{
		parent::__construct($config);
		//JModel::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR."/models/");
		$this->addModelPath(JPATH_COMPONENT_ADMINISTRATOR."/models/");
		$this->registerDefaultTask("locations");
		$this->registerTask( 'list',  'overview' );
		$this->registerTask( 'locations_blog',  'locations' );
	}

	function detail() {

		$locid	= JRequest::getInt( 'loc_id', 0 );

		if($locid == 0)
		{
			$compparams = JComponentHelper::getParams("com_jevlocations");
			$locid = $compparams->get("selectedlocation",0);
		}

		JRequest::setVar("cid",array($locid));

		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		// Set the layout
		$this->view->setLayout('detail');

		// Get/Create the model
		if ($model =  $this->getModel("location", "LocationsModel")) {
			// Push the model into the view (as default)
			$this->view->setModel($model, true);
		}

		$this->view->detail();
	}

	function edit($key = null, $urlVar = null)
	{
		$cid = JRequest::getVar( 'cid', JRequest::getVar( 'loc_id', array(0), 'post', 'array' ), 'post', 'array' );
                if (is_array($cid) && isset($cid[0])){
                    $cid = (int) $cid[0];
                }
		$this->_authoriseAccess($cid);

		// Load backend language file!
		$lang = JFactory::getLanguage();
		$lang->load("joomla",JPATH_ADMINISTRATOR);
		
		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		// Set the layout
		$this->view->setLayout('edit');
		$this->view->assign('title'   , JText::_("Edit_Location"));

		// Get/Create the model
		if ($model =  $this->getModel('location', "LocationsModel")) {
			// Push the model into the view (as default)
			$this->view->setModel($model, true);
		}

		$returntask	= JRequest::getVar( 'returntask', "locations.overview");
		if ($returntask!="locations.list" && $returntask!="locations.overview" && $returntask!="locations.select"){
			$returntask="locations.overview";
		}
		$this->view->assign('returntask'   , $returntask);

		// Get the media component configuration settings
		$mediaparams = JComponentHelper::getParams('com_media');
		// Set the path definitions
		define('JEVP_MEDIA_BASE',    JPATH_ROOT."/".$mediaparams->get('image_path', 'images'."/".'stories'));
		define('JEVP_MEDIA_BASEURL', JURI::root(true).'/'.$mediaparams->get('image_path', 'images/stories'));

		$jevuser = JEVHelper::getAuthorisedUser();
		$this->view->assign('jevuser',$jevuser);

		$this->view->edit();
	}

	function overview()
	{
		// Load backend language file!
		$lang = JFactory::getLanguage();
		$lang->load("joomla",JPATH_ADMINISTRATOR);
		parent::overview();
	}

	function ajaxmap() {
		$modid = intval((JRequest::getVar('modid', 0)));
		if ($modid<=0){
			echo "<script>alert('bad mod id');</script>";
			return;
		}
		$x = JRequest::getFloat('x', 0);
		$y = JRequest::getFloat('y', 0);
		$zoom = JRequest::getInt('zoom', 10);

		$user = JFactory::getUser();
		$query = "SELECT id, params"
		. "\n FROM #__modules AS m"
		. "\n WHERE m.published = 1"
		. "\n AND m.id = ". $modid
		. "\n AND m.access <= ". (int) $user->aid
		. "\n AND m.client_id != 1";
		$db	= JFactory::getDBO();
		$db->setQuery( $query );
		$modules = $db->loadObjectList();
		if (count($modules)<=0){
			if (!$modid<=0){
				echo "<script>alert('bad mod id');</script>";
				return;
			}
		}
		$params = new JRegistry( $modules[0]->params );

		JRequest::setVar("tmpl","component");

		// find the locations nearest the address being searched
		// Assume 50km distance for now
		// See http://calgary.rasc.ca/latlong.htm
		//
		// Latitude is how far north/south you are
		// The Formula for Longitude Distance at a Given Latitude (theta) in Km:
		// 1� of Longitude = 111.41288 * cos(theta) - 0.09350 * cos(3 * theta) + 0.00012 * cos(5 * theta)
		//
		// The Formula for Latitude Distance at a Given Latitude (theta) in Km:
		// 1� of Latitude = 111.13295 - 0.55982 * cos(2 * theta) + 0.00117 * cos(4 * theta)
		//
		// In terms of inputs
		// Longitude = x
		// Latitude = y

		$lonscaling = 111.41288 * cos(deg2rad($y)) - 0.09350 * cos(deg2rad(3 * $y)) + 0.00012 * cos(deg2rad(5 * $y));
		$lonscaling = $lonscaling*$lonscaling;

		$latscaling = 111.13295 - 0.55982 * cos(deg2rad(2 * $y)) + 0.00117 * cos(deg2rad(4 * $y));
		$latscaling = $latscaling*$latscaling;

		$maxdistance = 50;
		$maxdistance2 = $maxdistance*$maxdistance ;

		$query = "SELECT * FROM jos_jev_locations WHERE ($latscaling * (geolat - $y)* (geolat - $y) + $lonscaling * (geolon - $x)* (geolon - $x))< $maxdistance2 AND published=1 AND access <= ". (int) $user->aid;
		$db->setQuery( $query );
		$locations = $db->loadObjectList();
		//echo $db->_sql."<br/><br/>";

		if (count($locations)==0) return;

		$loclist = array();
		foreach ($locations as $location) {
			$loclist[] = $location->loc_id;
		}
		$registry = JFactory::getConfig();
		$registry->set("jevlocations.locations",implode(",",$loclist));

		include_once(JPATH_SITE."/components/".JEV_COM_COMPONENT."/jevents.defines.php");
		$this->datamodel  =  new JEventsDataModel();

		list($year,$month,$day) = JEVHelper::getYMD();
		$startdate 	= mktime( 0, 0, 0,  $month,  $day, $year );
		$enddate 	= mktime( 0, 0, 0,  $month+1, $day, $year );

		$filters = jevFilterProcessing::getInstance(array("locationlist","published","justmine","category","search"),JPATH_COMPONENT."/filters/");
		$events = $this->datamodel->queryModel->listIcalEvents($startdate,$enddate,"",$filters);
		
		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		$points = new stdClass();
		// centering and zoom of map
		$points->zoom=$zoom;
		$points->x = $x;
		$points->y = $y;

		$points->data= array();

		foreach ($locations as $location) {
			$point = new stdClass();
			$point->x = $location->geolon;
			$point->y = $location->geolat;
			$point->description = $location->title;
			$points->data[]= $point;
		}

		// Find bounding rectangle
		$swx=false;
		foreach ($points->data as $point) {
			if ($swx===false){
				$swx=$point->x;
				$swy=$point->y;
				$nex=$point->x;
				$ney=$point->y;
			}
			$swx=($point->x<$swx)?$point->x:$swx;
			$swy=($point->y<$swy)?$point->y:$swy;
			$nex=($point->x>$nex)?$point->x:$nex;
			$ney=($point->y>$ney)?$point->y:$ney;
		}
		$points->swx = $swx;
		$points->swy = $swy;
		$points->nex = $nex;
		$points->ney = $ney;

		$points = json_encode($points);

		// Set the layout
		$this->view->setLayout('ajaxmap');
		$this->view->assign("points",$points);

		$this->view->assign("modid",$modid);

		$this->view->display();
	}

	function locations( )
	{
		// get the view
		$viewName = "locations";
		$this->view = $this->getView($viewName,"html");

		// Set the layout
		$layout  = JRequest::getString("layout", "locations");
                                    
		$this->view->setLayout($layout);
		$this->view->assign('title'   , JText::_("COM_JEVLOCATIONS_LOCATIONS_LIST"));

		// Get/Create the model
		if ($model =  $this->getModel($viewName, "LocationsModel")) {
			// Push the model into the view (as default)
			$this->view->setModel($model, true);
		}

		$this->view->locations();
	}

	function orderdown()
	{
		$this->_authoriseAccess();

		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$model =  $this->getModel("locations", "LocationsModel");
		$model->move(1);
		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list'.$tmpl) );
	}

	function saveorder()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );

		$cid 	= JRequest::getVar( 'cid',  JRequest::getVar( 'loc_id', array(), 'post', 'array' ), 'post', 'array' );
		$order 	= JRequest::getVar( 'order', array(), 'post', 'array' );
		JArrayHelper::toInteger($cid);
		JArrayHelper::toInteger($order);

		$this->_authoriseAccess($cid);

		$model =  $this->getModel("location", "LocationsModel");
		$model->saveorder($cid, $order);

		$tmpl = "";
		if (JRequest::getString("tmpl","")=="component"){
			$tmpl ="&tmpl=component";
		}

		$msg = 'New ordering saved';
		$this->setRedirect( JRoute::_('index.php?option=com_jevlocations&task=locations.list' .  $tmpl, false),$msg);
	}

	function fixCreationPermissions() {
		$jevuser = JEVHelper::getAuthorisedUser();
		if 	($jevuser && ($jevuser->cancreateown || $jevuser->cancreateglobal)){
			// if jevents is not in authorised only mode then switch off this user's permissions
			$params = JComponentHelper::getParams('com_jevents');
			$authorisedonly = $params->get("authorisedonly",0);
			if (!$authorisedonly){
				$params = JComponentHelper::getParams("com_jevlocations");
				$loc_own = $params->get("loc_own",25);
				$juser = JFactory::getUser();
				if ($juser->gid<intval($loc_own)){
					$jevuser->cancreateown=false;
					$jevuser->cancreateglobal=false;
					return false;
				}
				$loc_global = $params->get("loc_global",24);
				if ($juser->gid<intval($loc_global)){
					$jevuser->cancreateglobal=false;
				}
			}
			return true;

		}
	}

	private function notifyAdmin($location){
		$params = JComponentHelper::getParams("com_jevlocations");
		$anoncreate = $params->get("anoncreate",25);
		$user = JFactory::getUser();
		if ($anoncreate && $user->id==0){
			$locadmin = $params->get("locadmin",62);

			$subject = $params->get("notifysubject","");
			$message = $params->get("notifymessage","");
			if ($subject =="" || $message =="") return;
			$message = str_replace("{TITLE}",$location->title,$message);
			$message = str_replace("{NAME}",$location->anonname,$message);
			$message = str_replace("{EMAIL}",$location->anonemail,$message);
			$adminuser = JEVHelper::getUser($locadmin);
			JFactory::getMailer()->sendMail($adminuser->email,$adminuser->name,$adminuser->email,$subject,$message);
		}
	}
}
