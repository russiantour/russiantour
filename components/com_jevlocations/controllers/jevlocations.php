<?php
/**
 * @version     1.0.0
 * @package     com_zoocalendar
 * @copyright   Copyright (C) 2015. Todos los derechos reservados.
 * @license     Licencia Pública General GNU versión 2 o posterior. Consulte LICENSE.txt
 * @author      Carlos <carlos@joomladesigner.com> - http://www.joomladesignstudios.com
 */

// No direct access.
defined('_JEXEC') or die;

require_once JPATH_COMPONENT.'/controller.php';

class JevlocationsControllerJevlocations extends JevlocationsController
{
	function __construct($config = array())
	{
		parent::__construct($config);
		$this->addModelPath(JPATH_COMPONENT_ADMINISTRATOR."/models/");
	}
	/**
	 * Proxy for getModel.
	 * @since	1.6
	 */
	public function &getModel($name = 'Locations', $prefix = 'LocationsModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));
		return $model;
	}

}