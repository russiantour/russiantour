<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

/**
 * Class ViaggioHelpersViaggio
 *
 * @since  1.6
 */

class ViaggioHelpersViaggio
{
	public static function createClient($clientArray, $order_id, $user_id, $tour_id){
		$query = "INSERT INTO `#__viaggio_clients` (`id`, `asset_id`, `created_by`, `tour_id`, `cognome`, `nome`, `sex`, `birthday`, `nazionalita`, `telephone`, `email`, `order_id`) VALUES (NULL, '', '".$user_id."', '$tour_id', '".$clientArray['cognome']."', '".$clientArray['nome']."', '".$clientArray['sex']."', '".ViaggioHelpersViaggio::dateToSQLformat($clientArray['nascita'])."', '".$clientArray['nazionalita']."', '', '', '$order_id');";// return $query;
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$result = $db->execute();
			
		return $db->insertid();
	}
	public static function registerUser($email,$password)
	{
		if(!JUserHelper::getUserId($email))
		{
			$password = JUserHelper::hashPassword($password);
			$query = "INSERT INTO `#__users` (`id`, `name`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`) VALUES (NULL, '$email', '$email', '$email', '$password', '0', '0', '".date("Y-m-d h:i:s")."', '0000-00-00 00:00:00.000000', '', '', '0000-00-00 00:00:00.000000', '0', '', '', '0');"; 
			$db = JFactory::getDbo();
			$db->setQuery($query);
			$result = $db->execute();
			
			return JFactory::getUser( $db->insertid() );
		}
			//echo JUserHelper::getUserId($email);
		if(false){
			echo '<pre>';
			//var_dump( $query);
			//var_dump($password);
			//var_dump($email);
			//var_dump($db->insertid());
			//var_dump($db->explain());
			echo '</pre>';
		}
		return false;
	}
	
	public static function getValuteRate()
	{
		$url = 'http://www.cbr.ru/scripts/XML_daily.asp';
		$answer = file_get_contents($url);
		$index = new SimpleXMLElement($answer);
		foreach($index as $val)
		{
			if($val->NumCode == 978){
				return str_replace(',','.',$val->Value);
			}
		}
	}
	
	public static function getItemDefaultCost($item,$category=0)
	{
		//506(Стоимость гостиницы 3*)+28(Стоимость услуг)+65(Консульский сбор)+16(Стоимость страховки на 7 день)+(20/2)(Доставка визы делим на 2) = 625 евро
		
		//get settings to item
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$item->statndarvisa = $params->get('statndarvisa');
		//$item->urgentevisa = $params->get('urgentevisa');
		$item->sendvisa = $params->get('sendvisa');
		$item->visa7day = $params->get('visa7day');
		//$item->visa1day = $params->get('visa1day');
		$item->servicecost = $params->get('servicecost');
		
		$cost = 0;
		
		//Если нету 3 звезды то считать 4 звездночные!
		if( $item->inn3cost ){
			$cost = $item->inn3cost;
		}else{
			$cost = $item->inn4cost;
		}
		
		//если без звезд
		if( $item->nostell1 ) $cost = $item->nostell1;
		
		//506(Стоимость гостиницы 3*)+28(Стоимость услуг)+65(Консульский сбор)+16(Стоимость страховки на 7 день)+(20/2)(Доставка визы делим на 2) = 625 евро
		//return $cost.'+'.$item->servicecost.' + '.$item->statndarvisa.' + '.$item->visa7day.' + '.($item->sendvisa/2).'='.($cost + $item->servicecost + $item->statndarvisa + $item->visa7day + $item->sendvisa/2);
		if($category){
			$cost = $cost + $item->servicecost + $item->statndarvisa + $item->visa7day + $item->sendvisa/2;
		}else{
			$cost = $item->singola3 + $item->servicecost + $item->statndarvisa + $item->visa7day + $item->sendvisa;
		}
		
		return $cost;
	}
	
	public static function saveOrderArray($object)
	{		 
		// Update their details in the users table using id as the primary key.
		$result = JFactory::getDbo()->updateObject('#__viaggio_orders', $object, 'id');
	}
	
	public static function getOrderObject($order_number)
	{
		$db = JFactory::getDbo();
		$query2 = "SELECT * FROM `#__viaggio_orders` where id = ".intval($order_number);
		$db->setQuery($query2);
		$result = $db->loadObject();
		if($result) return $result;
		$result = new stdClass();
		$result->order_id = 0;
		return $result;
	}
	
	public static function paymentApiGetOrderStatus($orderId, $settings)
	{
		$paramsGetORder = '?userName='.$settings->paymentApi->userName.'&password='.$settings->paymentApi->password.'&orderId='.$orderId;
		$url = 'https://web.rbsuat.com/reb/rest/getOrderStatus.do'.$paramsGetORder;
		$answer = file_get_contents($url);
		$answer = json_decode($answer);
		return $answer;
	}
	public static function paymentApiRegisterOrder($orderNumber, $amount, $settings)
	{
		///application_context/rest/register.do
		//$settings->paymentApi->returnUrl
		$paramsRegister = '?userName='.$settings->paymentApi->userName.'&password='.$settings->paymentApi->password.'&orderNumber='.$orderNumber.'&amount='.$amount.'&returnUrl='.urlencode( $settings->paymentApi->returnUrl.'/index.php?option=com_viaggio&view=paid&order_number='.$orderNumber );
		//echo $paramsRegister;
		$url = 'https://web.rbsuat.com/reb/rest/register.do'.$paramsRegister;
		$answer = file_get_contents($url);
		$answer = json_decode($answer);
		return $answer;
	}
	
	public static function sendEmail($body,$subject,$email,$settings)
	{
		//отправка email уведомления
		/* получатели */
		$to= $email;// . ", " ; //обратите внимание на запятую
		//$to .= "Kelly <kelly@example.com>";

		/* тема/subject */
		//$subject = "New order #11";

		/* сообщение */
		if($settings->email->templatePath) include($settings->email->templatePath);

		/* Для отправки HTML-почты вы можете установить шапку Content-type. */
		$headers= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";

		/* дополнительные шапки */
		$headers .= 'From: '.$settings->email->from.' \r\n';
		//$headers .= "Cc: birthdayarchive@example.com\r\n";
		//$headers .= "Bcc: birthdaycheck@example.com\r\n";

		/* и теперь отправим из */
		mail($to, $subject, $body, $headers);
	}
	
	public static function sendSms($text,$telephone,$settings)
	{
		$src = '<?xml version="1.0" encoding="UTF-8"?>
			<SMS>
				<operations>
				<operation>SEND</operation>
				</operations>
				<authentification>
				<username>'.$settings->sms->username.'</username>
				<password>'.$settings->sms->password.'</password>
				</authentification>
				<message>
				<sentdate></sentdate>
				<sender>'.$settings->sms->sender.'</sender>
				<text>'.$text.'</text>
				</message>
				<numbers>
				<number>'.$telephone.'</number>
				</numbers>
			</SMS>';
		// messageID="msg1"
		$Curl = curl_init();
		$CurlOptions = array(
			CURLOPT_URL=>'http://api.atompark.com/members/sms/xml.php',
			CURLOPT_FOLLOWLOCATION=>false,
			CURLOPT_POST=>true,
			CURLOPT_HEADER=>false,
			CURLOPT_RETURNTRANSFER=>true,
			CURLOPT_CONNECTTIMEOUT=>15,
			CURLOPT_TIMEOUT=>100,
			CURLOPT_POSTFIELDS=>array('XML'=>$src),
		);
		curl_setopt_array($Curl, $CurlOptions);
		if(false === ($Result = curl_exec($Curl))) {
			throw new Exception('Http request failed');
		}
	 
		curl_close($Curl);
	 
		return $Result;
	}
	/**
	* Get category name using category ID
	* @param integer $category_id Category ID
	* @return mixed category name if the category was found, null otherwise
	*/
	public static function getCategoryNameByCategoryId($category_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select('title')
			->from('#__categories')
			->where('id = ' . intval($category_id));

		$db->setQuery($query);
		return $db->loadResult();
	}
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_viaggio/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_viaggio/models/' . strtolower($name) . '.php';
			$model = JModelLegacy::getInstance($name, 'ViaggioModel');
		}

		return $model;
	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item)
    {
        $permission = false;
        $user       = JFactory::getUser();

        if ($user->authorise('core.edit', 'com_viaggio'))
        {
            $permission = true;
        }
        else
        {
            if (isset($item->created_by))
            {
                if ($user->authorise('core.edit.own', 'com_viaggio') && $item->created_by == $user->id)
                {
                    $permission = true;
                }
            }
            else
            {
                $permission = true;
            }
        }

        return $permission;
    }
	
	/*
	//38 параметров калькулятора
	//hotel
	$inn4cost = 500;
	$inn3cost = 400;
	$halfboard4 = 100;
	$halfboard3 = 70;
	$singola3 = 700;
	$singola4 = 900;

	//cruise
	$nostell1 = 1000;
	$nostell2 = 600;

	//transib
	$classic = 10000;
	$classicsingola = 18000;
	$superior = 20000;
	$superiorsingola = 38000;
	$superiorplus = 30000;
	$superiorplussingola = 58000;
	$nostalgic = 40000;
	$nostalgicsingola = 70000;
	$bolshoy4 = 50000;
	$bolshoy4singola = 95000;
	$platinum5 = 75000;
	$platinum5singola = 140000;
	$russiandays = 10;

	//component settings
	$servicecost = 28;//стоимость услуг
	$statndarvisa = 65;//консульский сбор
	$urgentevisa = 45;//срочный конс сбор
	$sendvisa = 20;//доставка
	$visa7day = 16; //Стандартная страховка выдаеться на 7 дней. Она входит в формулу. Очень важно в формуле :  от 0(лет) до 3(лет)  умножаем на 2 _____ от 3(лет) до 65(лет)   умножаем на 0 ____  от 65(лет) до 80(лет) умножаем на 2 ___  от 80 (лет) до  вечности умножаем на 3
	$visa1day = 2;
	$visamongolia = 20;
	$visachina = 50;

	//drive
	$type = 'hotel';
	$subtype = '4';
	$count = 2;
	$halfboard = 0;
	$age1 = 33;
	$age2 = 5;
	$needvisamongolia = 0;
	$needvisachina = 0;
	$urgent = 0; //срочность
	*/
	public static function getResult($inn4cost,$inn3cost,$halfboard4,$halfboard3,$singola4,$singola3, $type, $subtype, $count, $halfboard, $nostell1, $nostell2, $classic,$classicsingola,$superior,$superiorsingola,$superiorplus,$superiorplussingola,$nostalgic,$nostalgicsingola,$bolshoy4,$bolshoy4singola,$platinum5,$platinum5singola,$russiandays, $servicecost,$statndarvisa,$urgentevisa,$sendvisa,$visa7day,$visa1day,$visamongolia,$visachina, $age1, $age2, $needvisamongolia, $needvisachina, $urgent, $formula)
	{
		/*echo "<script type='text/javascript'>console.log('inn4cost:$inn4cost,inn3cost:$inn3cost,halfboard4:$halfboard4,halfboard3:$halfboard3,singola4:$singola4,singola3:$singola3, type:$type, subtype:$subtype, count:$count, halfboard:$halfboard, nostell1:$nostell1, nostell2:$nostell2, classic:$classic,classicsingola:$classicsingola,superior:$superior,superiorsingola:$superiorsingola,superiorplus:$superiorplus,superiorplussingola:$superiorplussingola,nostalgic:$nostalgic,nostalgicsingola:$nostalgicsingola,bolshoy4:$bolshoy4,bolshoy4singola:$bolshoy4singola,platinum5:$platinum5,platinum5singola:$platinum5singola,russiandays:$russiandays, servicecost:$servicecost,statndarvisa:$statndarvisa,urgentevisa:$urgentevisa,sendvisa:$sendvisa,visa7day:$visa7day,visa1day:$visa1day,visamongolia:$visamongolia,visachina:$visachina, age1:$age1, age2:$age2, needvisamongolia:$needvisamongolia, needvisachina:$needvisachina, urgent:$urgent');</script>";*/
		$result = 0;
		$log = "type($type)|subtype($subtype)|";
		if($type == 'hotel')
		{
			//$result = 'hotel';
			if($subtype == 3)
			{
				//$result = '3*';
				if($count == 1)
				{
					$log .= "+ singola3($singola3)";
					$result = $result + $singola3;
					if($halfboard){
						$result = $result + $halfboard3;
						$log .= "+ halfboard3($halfboard3)";
					} 
				}else if($count == 2)
				{
					$log .= "+ inn3cost*2($inn3cost*2)";
					$result = $result + ( $inn3cost * 2 );
					if($halfboard){
						$result = $result + ( $halfboard3*2 );
						$log .= "+ halfboard3*2($halfboard3*2)";
					} 
				}
			}else if($subtype == 4)
			{
				//$result = '4*';
				if($count == 1)
				{
					$log .= "+ singola4($singola4)";
					$result = $result + $singola4;
					if($halfboard){
						$result = $result + $halfboard4;
						$log .= "+ halfboard4($halfboard4)";
					} 
				}else if($count == 2)
				{
					$log .= "+ inn4cost*2($inn4cost*2)";
					$result = $result + ( $inn4cost * 2 );
					if($halfboard){
						$result = $result + ( $halfboard4*2 );
						$log .= "+ halfboard4*2($halfboard4*2)";
					} 
				}
			}
		}else if($type == 'cruise')
		{
			//$result = 'cruise';
			if($count == 1)
			{
				$log .= "+( nostell1($nostell1) )";
				$result = $result + $nostell1;
			}else if($count == 2)
			{
				$log .= "+( nostell2($nostell2) )";
				$result = $result + ( $nostell2 * 2 );
			}
		}else if($type == 'transib')
		{
			//$result = 'transib';
			if($subtype == 'classic')
			{
				//$result = 'classic';
				if($count == 1)
				{
					$log .= "+( classicsingola($classicsingola) )";
					$result = $result + $classicsingola;
				}else if($count == 2)
				{
					$log .= "+( classic*2($classic*2) )";
					$result = $result + ( $classic * 2 );
				}
			}else if($subtype == 'superior')
			{
				//$result = 'superior';
				if($count == 1)
				{
					$log .= "+( superiorsingola($superiorsingola) )";
					$result = $result + $superiorsingola;
				}else if($count == 2)
				{
					$log .= "+( superior*2($superior*2) )";
					$result = $result + ( $superior * 2 );
				}
			}else if($subtype == 'superiorplus')
			{
				//$result = 'superiorplus';
				if($count == 1)
				{
					$log .= "+( superiorplussingola($superiorplussingola) )";
					$result = $result + $superiorplussingola;
				}else if($count == 2)
				{
					$log .= "+( superiorplus*2($superiorplus*2) )";
					$result = $result + ( $superiorplus * 2 );
				}
			}else if($subtype == 'nostalgic')
			{
				//$result = 'nostalgic';
				if($count == 1)
				{
					$log .= "+( nostalgicsingola($nostalgicsingola) )";
					$result = $result + $nostalgicsingola;
				}else if($count == 2)
				{
					$log .= "+( nostalgic*2($nostalgic*2) )";
					$result = $result + ( $nostalgic * 2 );
				}
			}else if($subtype == 'bolshoy4')
			{
				//$result = 'bolshoy4';
				if($count == 1)
				{
					$log .= "+( bolshoy4singola($bolshoy4singola) )";
					$result = $result + $bolshoy4singola;
				}else if($count == 2)
				{
					$log .= "+( bolshoy4*2($bolshoy4*2) )";
					$result = $result + ( $bolshoy4 * 2 );
				}
			}else if($subtype == 'platinum5')
			{
				//$result = 'platinum5';
				if($count == 1)
				{
					$log .= "+( platinum5singola($platinum5singola) )";
					$result = $result + $platinum5singola;
				}else if($count == 2)
				{
					$log .= "+( platinum5*2($platinum5*2) )";
					$result = $result + ( $platinum5 * 2 );
				}
			}
		}
		
		//стоимость услуг
		if($count == 1)
		{
			$log .= "+servicecost($servicecost)";
			$result = $result + $servicecost;
		}else if($count == 2)
		{
			$log .= "+servicecost*2($servicecost*2)";
			$result = $result + ( $servicecost * 2 );
		}
		
		//доставка
		$result = $result + $sendvisa;
		$log .= "+sendvisa($sendvisa)";
		
		if($needvisamongolia)
		{
			if($count == 1)
			{
				$log .= "+visamongolia($visamongolia)";
				$result = $result + $visamongolia;
			}else if($count == 2)
			{
				$log .= "+visamongolia*2($visamongolia*2)";
				$result = $result + ( $visamongolia * 2 );
			}
		}

		if($needvisachina)
		{
			if($count == 1)
			{
				$log .= "+visachina($visachina)";
				$result = $result + $visachina;
			}else if($count == 2)
			{
				$log .= "+visachina*2($visachina*2)";
				$result = $result + ( $visachina * 2 );
			}
		}
		
		//если срочный консульский сбор
		if($urgent)
		{
			if($count == 1)
			{
				$log .= "+urgentevisa($urgentevisa)";
				$result = $result + $urgentevisa;
			}else if($count == 2)
			{
				$log .= "+urgentevisa*2($urgentevisa*2)";
				$result = $result + ( $urgentevisa * 2 );
			}
		}

		//консульский сбор
		if($count == 1)
		{
			$log .= "+statndarvisa($statndarvisa)";
			$result = $result + $statndarvisa;
		}else if($count == 2)
		{
			$log .= "+statndarvisa*2($statndarvisa*2)";
			$result = $result + ( $statndarvisa * 2 );
		}
		
		$log .= "|russiandays:$russiandays|";
		
		//если стандартная виза или если стандартная виза + доп. виза
		if($russiandays < 8)
		{
			$log .= "|стандартная виза 7 дней|";
			if($age1 < 3)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
			}else if($age1 < 65 && $age1 >= 3)
			{
				$log .= "+visa7day*1($visa7day*1)";
				$result = $result + ($visa7day * 1);
			}else if($age1 < 80 && $age1 >= 65)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
			}else if($age1 >= 80)
			{
				$log .= "+visa7day*3($visa7day*3)";
				$result = $result + ($visa7day * 3);
			}
				
			if($count == 2)
			{
				if($age2 < 3)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
				}else if($age2 < 65 && $age2 >= 3)
				{
					$log .= "+visa7day*1($visa7day*1)";
					$result = $result + ($visa7day * 1);
				}else if($age2 < 80 && $age2 >= 65)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
				}else if($age2 >= 80)
				{
					$log .= "+visa7day*3($visa7day*3)";
					$result = $result + ($visa7day * 3);
				}
			}
		}else if($russiandays > 7)
		{
			$log .= "|стандартная виза 7 дней+доп|";
			if($age1 < 3)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
			}else if($age1 < 65 && $age1 >= 3)
			{
				$log .= "+visa7day*1($visa7day*1)";
				$result = $result + ($visa7day * 1);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 1 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 1 );
			}else if($age1 < 80 && $age1 >= 65)
			{
				$log .= "+visa7day*2($visa7day*2)";
				$result = $result + ($visa7day * 2);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
			}else if($age1 >= 80)
			{
				$log .= "+visa7day*3($visa7day*3)";
				$result = $result + ($visa7day * 3);
				$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 3 )";
				$result = $result + ( ($russiandays - 7) * $visa1day * 3 );
			}
				
			if($count == 2)
			{
				if($age2 < 3)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
				}else if($age2 < 65 && $age2 >= 3)
				{
					$log .= "+visa7day*1($visa7day*1)";
					$result = $result + ($visa7day * 1);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 1 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 1 );
				}else if($age2 < 80 && $age2 >= 65)
				{
					$log .= "+visa7day*2($visa7day*2)";
					$result = $result + ($visa7day * 2);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 2 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 2 );
				}else if($age2 >= 80)
				{
					$log .= "+visa7day*3($visa7day*3)";
					$result = $result + ($visa7day * 3);
					$log .= "+( (russiandays($russiandays) - 7) * visa1day($visa1day) * 3 )";
					$result = $result + ( ($russiandays - 7) * $visa1day * 3 );
				}
			}
		}
		if(true){
			if(isset($_REQUEST['view']) ){
				if($_REQUEST['view'] == 'showtour') return "<script>console.log('$log');</script>$result";
			}
			if(isset($_REQUEST['task']) ){
				if($_REQUEST['task'] == 'calcTourCost') return "<script>console.log('$log');</script>$result";
			}
			if(isset($formula) ){
				if($formula) return $log.' = '.$result;
			}
		}
		return $result;
	}
	
	public static function getCost($tour_id,$subtype=0,$count=1,$age1=18,$age2=18,$halfboard = 0,
		$needvisamongolia = 0,
		$needvisachina = 0, $formula = 0)
	{
		/*echo "<script type='text/javascript'>console.log('getCost()_tour_id:$tour_id,subtype:$subtype, count:$count, age1:$age1, age2:$age2, halfboard:$halfboard, needvisamongolia:$needvisamongolia, needvisachina:$needvisachina');</script>";*/
		//get tour data
		$query = "SELECT * FROM `#__viaggio_tours` WHERE id = '$tour_id';";
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$item = $db->loadObject();
		
		//get settings to plugin
		$app = JFactory::getApplication();
		$params = $app->getParams();
		
		//38 параметров калькулятора
		//hotel
		$inn4cost = $item->inn4cost;
		$inn3cost = $item->inn3cost;
		$halfboard4 = $item->halfboard4;
		$halfboard3 = $item->halfboard3;
		$singola3 = $item->singola3;
		$singola4 = $item->singola4;

		//cruise
		$nostell1 = $item->nostell1;
		$nostell2 = $item->nostell2;

		//transib
		$classic = $item->classic;
		$classicsingola = $item->classicsingola;
		$superior = $item->superior;
		$superiorsingola = $item->superiorsingola;
		$superiorplus = $item->superiorplus;
		$superiorplussingola = $item->superiorplussingola;
		$nostalgic = $item->nostalgic;
		$nostalgicsingola = $item->nostalgicsingola;
		$bolshoy4 = $item->bolshoy4;
		$bolshoy4singola = $item->bolshoy4singola;
		$platinum5 = $item->platinum5;
		$platinum5singola = $item->platinum5singola;
		$russiandays = $item->russiandays;

		//component settings
		$servicecost = $params->get('servicecost');//стоимость услуг
		$statndarvisa = $params->get('statndarvisa');//консульский сбор
		$urgentevisa = $params->get('urgentevisa');//срочный конс сбор
		$sendvisa = $params->get('sendvisa');//доставка
		$visa7day = $params->get('visa7day'); //Стандартная страховка выдаеться на 7 дней. Она входит в формулу. Очень важно в формуле :  от 0(лет) до 3(лет)  умножаем на 2 _____ от 3(лет) до 65(лет)   умножаем на 0 ____  от 65(лет) до 80(лет) умножаем на 2 ___  от 80 (лет) до  вечности умножаем на 3
		$visa1day = $params->get('visa1day');
		$visamongolia = $params->get('visamongolia');
		$visachina = $params->get('visachina');

		//drive
		$type = 'hotel';
		$urgent = 0; //срочность
		
		//дней до начала тура
		$daysTo = (strtotime( $item->from )-time())/3600/24;
		
		//продолжительность тура
		$daysFromTo = ( ( strtotime( $item->to )-strtotime( $item->from ) ) / 3600 / 24 ) +1;
		
		//if($daysTo < 10) return 0;
		
		//срочность, 20 или меньше дней, то срочно
		if($daysTo <= 20 && $daysTo >= 10) $urgent = 1;
		
		//тип тура
		if($nostell1 > 1) $type = 'cruise';		
		if($russiandays > 1){ $type = 'transib'; }else{ $russiandays = $daysFromTo; }
		
		
		if($subtype == NULL)
		{
			if($type == 'cruise'){
				$subtype = '';
				if($nostell1 == 0 OR $nostell2 == 0) return 0;
			}else if($type == 'transib')
			{
				if($classic > 0){
					$subtype = 'classic';
				}else{
					if($superior > 0){
						$subtype = 'superior';
					}else{
						if($superiorplus > 0){
							$subtype = 'superiorplus';
						}else{
							if($nostalgic > 0){
								$subtype = 'nostalgic';
							}else{
								if($bolshoy4 > 0){
								$subtype = 'bolshoy4';
								}else{
									if($platinum5 > 0){
										$subtype = 'platinum5';
									}else{
										return 0;
									}
								}
							}
						}
					}
				}
			}else if($type == 'hotel')
			{
				if($inn3cost > 0){
					$subtype = '3';
				}else{
					if($inn4cost > 0){
						$subtype = '4';
					}else{
						return 0;
					}
				}
			}
		}
		//echo $type;
		
		//echo '<pre>'.print_r($item,1).'</pre>';
		return ViaggioHelpersViaggio::getResult($inn4cost,$inn3cost,$halfboard4,$halfboard3,$singola4,$singola3, $type, $subtype, $count, $halfboard, $nostell1, $nostell2, $classic,$classicsingola,$superior,$superiorsingola,$superiorplus,$superiorplussingola,$nostalgic,$nostalgicsingola,$bolshoy4,$bolshoy4singola,$platinum5,$platinum5singola,$russiandays, $servicecost,$statndarvisa,$urgentevisa,$sendvisa,$visa7day,$visa1day,$visamongolia,$visachina, $age1, $age2, $needvisamongolia, $needvisachina, $urgent, $formula);
	}
	
	public static function dateToSQLformat($date)
	{
		$date = str_replace('/','-',$date);
		$date_timestamp = strtotime($date);
		return date('Y-m-d', $date_timestamp);
	}
	
	public static function calculate_age($birthday) {
		$birthday = str_replace('/','-',$birthday);
		$birthday_timestamp = strtotime($birthday);
		$age = date('Y') - date('Y', $birthday_timestamp);
		if (date('md', $birthday_timestamp) > date('md')) {
			$age--;
		}
		return $age;
	}
	
	public static function detDuration($from, $to)
	{
		return ( ( strtotime( $to )-strtotime( $from ) ) / 3600 / 24 ) +1;
	}
	
	public static function getCategory($id)
	{
		$db = JFactory::getDbo();
		$query3 = 'SELECT * FROM `#__categories` where id = "'.$db->escape($id).'"';
		
		$db->setQuery($query3);
		return $db->loadObject();
	}
	
	public static function getLinkById($id)
	{
		
	}
}
