<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */

// No direct access.
defined('_JEXEC') or die;

/**
 * Showtours list controller class.
 *
 * @since  1.6
 */
class ViaggioControllerShowtours extends ViaggioController
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional
	 * @param   array   $config  Configuration array for model. Optional
	 *
	 * @return object	The model
	 *
	 * @since	1.6
	 */
	public function &getModel($name = 'Showtours', $prefix = 'ViaggioModel', $config = array())
	{
		$model = parent::getModel($name, $prefix, array('ignore_request' => true));

		return $model;
	}
}
