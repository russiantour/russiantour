<?php
/**
 * @version     1.0.0
 * @package     com_touristinvite
 * @copyright   © 2014. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Strshot <kaktus_mov@mail.ru> - 
 */
// no direct access
defined('_JEXEC') or die;

if ($_GET['payment_id'])
{
    $payment_id = strip_tags(addslashes($_GET['payment_id']));
    $query = "SELECT *  FROM #__viaggio_manualpayments WHERE ".
        //"status IN (0,2) and ".
        "paymentID = '".$payment_id."' LIMIT 1" ;
    $db=JFactory::getDBO();
    $db->setQuery($query);
    $manualPayment = $db->loadAssoc();

    $query = "SELECT * FROM `#__viaggio_orders` WHERE id = ".$manualPayment['order_id'];
    $db->setQuery($query);
    $manualPayment['order'] = $db->loadAssoc();

    $query = "SELECT * FROM `#__viaggio_trips` WHERE id = ".$manualPayment['trip_id'];
    $db->setQuery($query);
    $manualPayment['trip'] = $db->loadAssoc();

    $query = "SELECT * FROM #__viaggio_clients WHERE order_id = ".$manualPayment['order_id'];
    $db->setQuery($query);
    $manualPayment['clients'] = $db->loadAssocList();

    $tmp_data = ViaggioHelpersFrontend::getOrderObject($manualPayment['order_id']);
    $manualPayment['tour'] = $tmp_data->tour;
}

//Load admin language file
$lang = JFactory::getLanguage();
//var_dump($lang->getTag());//it-IT,es-ES,en-GB
include 'langs/'.$lang->getTag().'.php'
?>

