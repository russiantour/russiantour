<?php
include 'translit.php';

//include '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';

?>
<div class="uk-grid">
 <div class="uk-width-1-2">
<p class="uk-text-center"><strong>ДОГОВОР-ОФЕРТА<br> 
 о реализации туристского продукта № B<?php echo $manualPayment['order']['id']; ?></strong> <br>
 Россия, Санкт-Петербург <br> <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?></p>
 
 Общество с ограниченной ответственностью  <?php echo $name_ru; ?> (ООО <?php echo $name_ru; ?>), Российская Федерация, именуемое в дальнейшем «Туроператор», в лице Генерального директора Черемшенко Ольги Николаевны, действующей на основании Устава, с одной стороны, и <b><?php  echo   translitText($manualPayment['order']['nomecgome']);  ?>  </b>   , электронный адрес <?php  echo $manualPayment['order']['email']; ?>, </b >именуемый (ая) в дальнейшем «Заказчик», с другой стороны, вместе и по отдельности именуемые «Стороны», заключили настоящий Договор о нижеследующем: <br>
 Настоящий Договор-оферта (далее – «Договор», «Договор-оферта») является письменным предложением (Офертой) Туроператора заключить Договор, направляемое Заказчику в соответствии со ст. 432-444 ГК РФ.</p>
 Договор заключается путем полного и безоговорочного принятия (акцепта) оферты Заказчиком в порядке, установленном п. 3 ст. 438 ГК РФ, и является подтверждением соблюдения письменной формы договора в соответствии с п. 3 ст. 434 ГК РФ.<br>
 Текст настоящего Договора-оферты расположен по адресу: <div class="uk-text-truncate">https://www.russiantour.com/ita/bonifico?payment_id=m<?php echo $manualPayment['order']['id']; ?> </div><br>
 
<p class="uk-text-center uk-text-bold" > Терминология: </p>
Заказчик туристского продукта (Заказчик) - турист или иное лицо, заказывающее туристский продукт от имени туриста, в том числе законный представитель несовершеннолетнего туриста;<br>
Сайт  – интернет-сайт  www.russiantour.com, с помощью которого Заказчик осуществляет действия по заказу туристского продукта (действия, направленные на заключение настоящего Договора, в том числе действия по выбору услуг с помощью программных средств сайта и оформлению заявки); <br>
Оферта – письменное предложение Туроператора заключить Договор;<br>
Акцепт оферты - полное и безоговорочное принятие Заказчиком оферты путем осуществления действий, указанных в оферте;<br>
Туристская деятельность - туроператорская и турагентская деятельность, а также иная деятельность по организации путешествий;<br>
Туристский продукт - комплекс услуг по перевозке и размещению, оказываемых за общую цену (независимо от включения в общую цену стоимости экскурсионного обслуживания и (или) других услуг) по договору о реализации туристского продукта;<br>
Туроператорская деятельность - деятельность по формированию, продвижению и реализации туристского продукта, осуществляемая юридическим лицом (далее - Туроператор).<br>
<p class="uk-text-center uk-text-bold">1. ПРЕДМЕТ ДОГОВОРА</p> 
1.1. В соответствии с Договором Туроператор обязуется обеспечить Заказчику комплекс услуг, входящих в Туристский продукт, полный перечень которых указывается в Заявке на реализацию туристского продукта (Приложение № 1 к Договору) (далее - Туристский продукт), а Заказчик обязуется оплатить Туристский продукт на условиях настоящего Договора.<br>
1.2. Сведения о Заказчике в объеме, необходимом для исполнения Договора, указаны в заявке на реализацию туристского продукта.<br>
1.3. Туроператор предоставляет Заказчику следующие услуги:<br>
1.3.1. Визовая поддержка;<br>
1.3.2. Единые турпакеты (туры на несколько дней);<br>
1.3.3. Приобретение авиабилетов;<br>
1.4. Перечень услуг, указанных в п. 1.3. настоящего Договора не является исчерпывающим и может быть сторонами расширен.<br>
1.5. Конкретный перечень услуг по Договору выбирается Заказчиком в Заявке о реализации туристского продукта (Приложение № 1 к Договору).<br>
1.6. Настоящий Договор является публичной офертой.<br>
1.7. Акцептом настоящего Договора-оферты со стороны Заказчика является проставление отметки “V” в графе “HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA <?php echo $name; ?> “расположенной по адресу:<div class="uk-text-truncate">https://www.russiantour.com/ita/bonifico?payment_id=m<?php echo $manualPayment['order']['id']; ?></div><br>
<strong>. </strong>Проставление Заказчиком отметки “V” в указанной графе обозначает согласие Заказчика с условиями Договора, принятие Заказчиком условий настоящего Договора.<br>
 


<p class="uk-text-center uk-text-bold">2. ЦЕНА ТУРИСТСКОГО ПРОДУКТА. ПОРЯДОК ОПЛАТЫ И АННУЛЯЦИИ.</p>
2.1. Общая цена Туристского продукта, приобретаемого по Договору (стоимость услуг), определяется на основании заявки на реализацию туристского продукта и указывается в инвойсе. Инвойс является неотъемлемой частью Договора.Когда отношение курса рубля к евро, устанавливаемого ЦБ РФ, с момента подтверждения заявки на реализацию туристского продукта изменилось более чем на 10% (десять процента) по сравнению с курсом рубля к евро на дату окончательного расчета за комплекс услуг, входящих в Туристский продукт, Туроператор имеет право изменить стоимость услуг пропорционально изменению данного валютного курса ЦБ РФ. <br>
2.2. Оплата осуществляется Заказчиком на основании выставленного инвойса путем осуществления банковского перевода.<br>
2.3. Заказчик обязан оплатить услуги Туроператора  в течение 14 дней с момента выставления инвойса, если не была запрошена более срочная оплата в связи со срочностью заявки. Заказчик обязан прислать подтверждение оплаты в течение 3-х дней с момента выставления инвойса.  В случае срочной оплаты Заказчик обязан прислать подтверждение оплаты в течение 1-ого дня с момента выставления инвойса. Оплата может производиться третьими лицами. При несоблюдении Заказчиком сроков и порядка оплаты Туроператор имеет право одностороннего отказа от исполнения услуг по настоящему договору. <br>
2.4. Банковские комиссии распределяются между Сторонами в следующем порядке: Заказчик оплачивает комиссии обслуживающего Заказчика банка, комиссии иных банков оплачивает Туроператор (SHA).<br>
2.5. Стоимость услуг указывается в инвойсе в Евро.<br>
2.6. Валюта платежа: Евро.<br>
2.7. Датой оплаты Заказчиком услуг, оказываемых Туроператором по Договору, является дата зачисления денежных средств на банковский счет Туроператора. При изменении курса рубля к евро, устанавливаемого ЦБ РФ, более чем на 10% (десять процента) между датой выставления инвойса и датой поступления денежных средств на банковский счет Туроператора, Туроператор имеет право изменить стоимость услуг пропорционально изменению данного валютного курса ЦБ РФ.<br> 

2.8. Инвойс с момента его полной оплаты Заказчиком подтверждает оказание услуг Туроператором по соответствующей Заявке на реализацию туристского продукта и признается Сторонами в качестве акта выполненных работ.<br>
2.9. Заказчик имеет право полностью или частично аннулировать заявку на турпакет. Конкретный перечень условий аннуляции по Договору указывается Туроператором в Заявке о реализации туристского продукта (Приложение № 1 к Договору).<br>


<p class="uk-text-center uk-text-bold">3. ВЗАИМОДЕЙСТВИЕ СТОРОН</p>
3.1. Туроператор обязан:<br>
- предоставить Заказчику достоверную информацию о потребительских свойствах Туристского продукта, а также информацию, предусмотренную Заявкой на реализацию туристского продукта;<br>
- передать Заказчику оформленные визовые документы по мере их готовности;<br>
- принять необходимые меры по обеспечению безопасности персональных данных Заказчика, в том числе при их обработке и использовании;<br>
- оказать Заказчику все услуги, входящие в Туристский продукт, самостоятельно или с привлечением третьих лиц, на которых Туроператором возлагается исполнение части или всех его обязательств перед Заказчиком и (или) туристом (в случае если Заказчик заказывает Туроператору туристский продукт от имени туриста, а также является законным представителем несовершеннолетнего туриста).<br>
3.2. Туроператор вправе:<br>
- не приступать к оказанию услуг по Договору до полной оплаты Заказчиком заказанных услуг.<br>
3.3. Заказчик обязан:<br>
- оплатить Туристский продукт в соответствии с Договором;<br>
- довести до туриста условия Договора, иную информацию, указанную в Договоре и приложениях к нему, а также передать ему документы, полученные от Туроператора для совершения туристом путешествия;<br>
- предоставить Туроператору свои контактные данные, а также контактные данные туриста, необходимые для оперативной связи, а также оформления Туристского продукта;<br>
- предоставить Туроператору документы и сведения, необходимые для исполнения Договора, в соответствии с требованиями, предъявленными Туроператором (необходимые требования к документам и сведениям размещены на Сайте либо доводятся до сведения Заказчика дополнительно);<br>
- проверить полученную от Туроператора визу и сообщить в течении 24 часов об ошибках или несовпадениях между данными, указанными в визе и в паспорте; <br>
- проверить приобретенный турпакет;<br>
- проверить авиабилеты.<br>
3.4. Заказчик вправе:<br>
- получить копию свидетельства о внесении сведений о Туроператоре в реестр;<br>
- получить оформленные визовые документы;<br>
- получить приобретенный турпакет;<br>
- получить приобретенные авиабилеты.<br>
3.5. Стороны несут ответственность за неисполнение или ненадлежащее исполнение своих обязательств в соответствии с законодательством Российской Федерации.<br>
3.6. Туроператор не несет ответственность:<br>
- за действия посольств (консульств) иностранных государств, иных организаций, за исключением организаций, которые привлечены Туроператором для оказания услуг, входящих в Туристский продукт, в том числе за отказ иностранного посольства (консульства) в выдаче (задержке) въездных виз туристам по маршруту путешествия, если в иностранное посольство (консульство) Туроператором либо непосредственно Заказчиком в установленные сроки были представлены все необходимые документы;<br>
- за отказ туристам в выезде/въезде при прохождении паспортного пограничного или таможенного контроля, либо применение к Заказчику органами, осуществляющими пограничный или таможенный контроль, штрафных санкций по причинам, не связанным с выполнением Туроператором своих обязательств по Договору;<br>
- за отмену, перенос или изменение в расписании движения авиарейсов, в том числе связанные с забастовками Авиакомпаний; <br>
- изменение в  расписании движения/ времени отправления или прибытия/ маршрута движения или отмены железнодорожного и автобусного транспорта, паромов и теплоходов; <br>
- за изменения времени начала  (окончания) прочих услуг, входящих в турпакет; <br>
- за отмену прочих услуг, входящих в турпакет, по причинам, независящим от Туроператора.  <br>


3.7. Заказчик вправе предъявить претензии к полученным в результате оказания Туроператором визовым документам в течении 24 часов с момента их получения. По истечении указанного срока претензии к визовым документам Туроператором не принимаются и он не несет ответственности за возникающие проблемы на пунктах пересечения государственной границы РФ.<br>
3.8. Заключением настоящего Договора Заказчик подтверждает, что ознакомлен со всеми Правилами, размещенным на Сайте <div class="uk-text-truncate"> https://www.russiantour.com/ita/bonifico?payment_id=m<?php echo $manualPayment['order']['id']; ?></div> <br>
<p class="uk-text-center uk-text-bold">4. ПРЕТЕНЗИИ. ПОРЯДОК РАЗРЕШЕНИЯ СПОРОВ</p>
4.1. Претензии в связи с нарушением условий Договора предъявляются Заказчиком Туроператору в порядке и на условиях, которые предусмотрены законодательством Российской Федерации.<br>
4.2. Претензии к качеству Туристского продукта предъявляются Туроператору в письменной форме в течение 20 дней с даты окончания действия Договора и подлежат рассмотрению в течение 10 дней с даты получения претензий.<br>
4.3. В случае, если претензии возникли из-за проблем или ошибок, относящихся к деятельности туроператора в соответствии с условиями Договора, Туроператор возвращает Заказчику исключительно стоимость оплаты услуг на основании  настоящего Договора. Все остальные расходы, приобретенные Заказчиком лично или через третьих лиц, по причинам задержки или отмены поездки Туроператором не возмещаются. <br>
4.4. Споры, связанные с настоящим Договором, подлежат рассмотрению в суде субъекта Российской Федерации, где зарегистрирован Туроператор, в соответствии с положениями действующего законодательства РФ.<br>

<p class="uk-text-center uk-text-bold">5. ОТВЕТСТВЕННОСТЬ</p>
5.1
Туроператор не несет ответственность за следующие обстоятельства:<br>
- загранпаспорт Заказчика (туриста) не принимается консульским учреждением для оформления визы и/или пограничными властями РФ по причине его повреждения, изношенности, отсутствия необходимых подписей, печатей, свободных страниц; а также по причине того, что до истечения срока действия загранпаспорта осталось менее 6 месяцев;<br>
- Заказчик предоставил неверные, недостоверные, ошибочные, неполные данные при заполнении Заявки на реализацию туристского продукта;<br>
- отказ в оформлении и выдаче визы по усмотрению консульского учреждения по причинам, не зависящим от Туроператора;<br>
- отказ в оформлении и выдаче визы по причине неправильного заполнения Заказчиком электронной анкеты, наличия ошибок в представленных Заказчиком документах, предоставлении неполного комплекта документов и др.;<br>
- отказ во въезде по электронной визе непосредственно в пункте пересечения государственной границы РФ или возникшие проблемы при выезде с территории РФ в связи с обстоятельствами, перечисленными в пункте 7.9 настоящего Договора<br>
- задержка в выдаче визы по причине внепланового закрытия консульского учреждения для третьих лиц, в том числе представителей туристских, туроператорских компаний; внезапного изменения графика работы визового отдела консульского учреждения;<br>
- утрата документов по вине почтовых курьеров (служб); осуществление доставки по неверному адресу; задержка доставки по вине почтовых курьеров (служб);
 - выдача визы с ошибочной датой въезда, или датой, не совпадающей с датой, определенной консульским учреждением;<br>
- при прохождении пограничного контроля у Заказчика (туриста) возникли сложности с сотрудниками пограничной службы, в результате чего Заказчику (туристу) отказано во въезде на территорию Российской Федерации;<br>
- за невыполнение Заказчиком рекомендаций Туроператора, указанных в п. 7.7 настоящего Договора;<br>
- невозможность Заказчиком (туристом) воспользоваться оказанными Туроператором на настоящему Договору услугами в том случае, если такая невозможность возникла по обстоятельствам, за которые Туроператор не отвечает.
-  за проблемы на пунктах  пересечения государственной границы РФ из-за ошибок или несовпадений между данными, указанными в визе и в паспорте, возникшие по причине отсутствия проверки и сообщения об ошибках от Заказчика Туроператору в течение 24 часов  с момента получения визовых документов <br>

5.2. В случае отказа в выдаче визы, произошедшего по вине Туроператора, последний обязан возместить в полном объеме затраты Заказчика на услуги Туроператора по оказанию визовой поддержки в рамках настоящего Договора.<br>
5.3. Стороны освобождаются от ответственности за частичное или полное невыполнение обязательств по Договору, если это неисполнение является следствием наступления обстоятельств непреодолимой силы, то есть возникших в результате чрезвычайных и непредотвратимых при данных условиях обстоятельств, которые Стороны не могли ни предвидеть, ни предотвратить разумными мерами.<br>
Если данные обстоятельства будут продолжаться более 14 (четырнадцати) календарных дней, каждая из Сторон вправе отказаться от исполнения обязательств по Договору, и в этом случае ни одна из Сторон не будет иметь права на возмещение другой Стороной возможных убытков по основаниям непреодолимой силы.<br>

 

<p class="uk-text-center uk-text-bold">6. СРОК ДЕЙСТВИЯ ДОГОВОРА, ПОРЯДОК ИЗМЕНЕНИЯ И РАСТОРЖЕНИЯ ДОГОВОРА</p>
6.1. Договор вступает в силу с даты акцепта Заказчиком его условий в соответствии с п. 1.7. Договора и действует до исполнения сторонами своих обязательств, но не позднее 31 декабря 2020 г.<br>
6.2. Договор может быть изменен или расторгнут в случаях и порядке, предусмотренном законодательством Российской Федерации, в том числе по соглашению Сторон, оформленному в письменной форме.<br>
Любые изменения в Туристский продукт, иные условия Заявки на реализацию туристского продукта допускаются только по письменному дополнительному соглашению Сторон. Письменная форма изменений в Заявку на реализацию туристского продукта считается также соблюденной при согласовании таких изменений Сторонами по электронной почте. Для этих целей со стороны Туроператора используется следующей адрес электронной почты: <a href="mailto:<?php echo $mp_user->email; ?>"><?php echo $mp_user->email; ?></a><br>
Со стороны Заказчика используется следующей адрес электронной почты <?php  echo $manualPayment['order']['email']; ?> <br>

<p class="uk-text-center uk-text-bold">7. ЗАКЛЮЧИТЕЛЬНЫЕ ПОЛОЖЕНИЯ </p>
7.1. Сведения о заключении в пользу туристов договора добровольного страхования, условиями которого предусмотрена обязанность страховщика осуществить оплату и (или) возместить расходы на оплату медицинской помощи в экстренной и неотложной формах, оказанной туристу на территории страны временного пребывания при наступлении страхового случая в связи с получением травмы, отравлением, внезапным острым заболеванием или обострением хронического заболевания, включая медицинскую эвакуацию туриста в стране временного пребывания и из страны временного пребывания в страну постоянного проживания, и (или) возвращения тела (останков) туриста из страны временного пребывания в страну постоянного проживания в соответствии с требованиями законодательства Российской Федерации и страны временного пребывания указаны в Заявке на реализацию туристского продукта.<br>
7.2. Заказчик предоставляет согласие, а также подтверждает, что в соответствии с требованиями Федерального закона РФ № 152-ФЗ от 27.07.2006 г. «О персональных данных» им получено согласие от всех туристов, указанных в Приложении № 1  к Договору, на обработку и передачу своих персональных данных и персональных данных лиц, указываемых в Заявке на реализацию туристского продукта, Туроператору и третьим лицам для исполнения Договора (в том числе для оформления виз, приобретения авиабилетов, турпакета).<br>
7.3. В случае, если Заказчик осуществляет заказ Туристского продукта в интересах туриста, Заказчик подтверждает, что обладает необходимыми полномочиями для представления интересов туриста в отношениях с Туроператором.<br>
7.4. Все приложения, а также изменения (дополнения) к Договору являются его неотъемлемой частью.<br>
7.5. Во всем ином, что не урегулировано Договором, Стороны руководствуются правом Российской Федерации.<br>
7.6. При обращении в Генеральное консульство Российской Федерации в Милане по вопросу получения визы в случае, если желаемый срок пребывания в стране превышает 14 дней, как правило, требуется предоставить дополнительные документы: документы, подтверждающие бронирование отелей, счета из отелей или&nbsp;систем бронирования, квитанции о 100% оплате услуг. Если Заказчиком не забронировано место пребывания заранее на весь срок пребывания в стране назначения и не собран самостоятельно весь пакет документов, не рекомендуется подавать документы в визовый отдел Генерального консульства Российской Федерации в Милане. Если Заказчиком будет принято решение о подаче заявления на выдачу визы в описываемой выше ситуации в отсутствие указанных документов, Туроператор не несет ответственности за возможные неблагоприятные последствия для Заказчика, в том числе за возможный отказ в выдаче визы. В данном случае услуги по Договору будут считаться выполненными Туроператором в полном объеме. Обязанности по выплате каких-либо компенсаций в пользу Заказчика не возникает, равно как не возникает у Туроператора обязанности повторной подачи документов в это же консульское учреждение либо любое иное.<br>
7.7. Для несовершеннолетних, планирующих поездку в Российскую Федерацию, без сопровождения обоих родителей&nbsp;дополнительно необходимы следующие документы: свидетельство о рождении с данными о родителях, свидетельство о семейном положении, копия документа, удостоверяющего личность родителя или родителей, заявление о согласии родителя (родителей) несовершеннолетнего на посещение Российской Федерации без его (их) сопровождения.<br>
7.8. В случае, если невозможно получить визу в стандартные сроки из-за итальянских или российских праздничных дней либо в случае, если географическое расположение Заказчика не позволяет доставить документы почтовым курьером (службой) до указанной в Заявке даты вылета, произведенная оплата подлежит возврату на банковскую карту/банковский счет Заказчика не позднее трех дней с даты оплаты.&nbsp;Заказчик вправе, изменив дату получения визы, заключить новый Договор на новых условиях. Заказчик также вправе воспользоваться возможностью срочного или срочного в течение дня оформления визы.<br>
 

7.9 В случае оформления электронной визы Заказчик (турист) обязан распечатать и сохранить документы (уведомление о выдаче электронной визы и страховой полис), полученные от Туроператора, с целью предоставления перевозчику и пограничным властям Российской Федерации. Заказчик уведомлён о том, что электронная виза действительна для въезда в регион, для которого она была запрошена (Санкт-Петербург и Ленинградская область или Калининград и область или Дальневосточный федеральный округ), для пребывания и перемещения исключительно в границах данного региона; что въезд на территорию Российской Федерации и выезд с территории РФ возможны исключительно через пункты пограничного контроля РФ одного и того же субъекта Российской Федерации, находящиеся в Санкт-Петербурге: аэропорт «Пулково», «Большой порт Санкт-Петербург» (Морской вокзал), «Пассажирский порт Санкт-Петербург», в Ленинградской области: автомобильные пункты пропуска «Торфяновка», «Брусничное», «Светогорск», автомобильный и пешеходный пункт пропуска «Ивангород», порт «Высоцк» - Калининградская область: железнодорожные пункты пропуска «Пограничный», «Хасан», «Махалино», «Мамоново», «Советск», автомобильные пункты пропуска «Багратионовск», «Гусев», «Мамоново (Гжехотки)», «Мамоново (Гроново)», «Морское», «Пограничный», «Советск», «Чернышевское», морские пункты пропуска «Калининград (участки в городах Калининград, Балтийск и Светлый)», аэропорт «Калининград (Храброво)» - Приморский край:     воздушный пункт пропуска «Владивосток (Кневичи)», морские пункты пропуска «Владивосток», «Зарубино», «Посьет», железнодорожные пункты пропуска «Пограничный», «Хасан», «Махалино», автомобильные пункты пропуска «Полтавка», «Турий Рог» - Сахалинская область: воздушный пункт пропуска «Южно-Сахалинск (Хомутово)», морской пункт пропуска «Корсаков» - Камчатский Край: воздушный пункт пропуска «Петропавловск-Камчатский (Елизово)»,  морской пункт пропуска «Петропавловск-Камчатский» - Амурская область: воздушный пункт пропуска «Благовещенск» - Хабаровский край: воздушный пункт пропуска «Хабаровск (Новый)» - Чукотский автономный округ: воздушный пункт пропуска «Анадырь (Угольный)» - Республика Бурятия: воздушный пункт пропуска «Улан-Удэ (Мухино)» - Забайкальский край: воздушный пункт пропуска «Чита (Кадала)»; о том, что максимальный срок пребывания по электронной визе составляет 8 дней/7 ночей; что выдача электронной визы не гарантирует право въезда на территорию Российской Федерации; что во въезде в РФ может быть отказано непосредственно на пункте пересечения государственной границы в случаях, предусмотренных Федеральным Законом 114 «О порядке Выезда из Российской Федерации и Въезда в Российскую Федерацию» от 15 августа 1996 года; что электронная виза ни как не модифицируется, не продлевается, и в случае возникновения любых проблем, Туроператор не сможет помочь Заказчику с пересечением государственной границы РФ. <br>

7.10. Заключением настоящего Договора Заказчик подтверждает, что он полностью ознакомлен Туроператором со всеми условиями и требованиями консульских учреждений, правилами въезда на территорию Российской Федерации, нахождения и выезда с ее территории, и они ему понятны. Риск совершения тех или действий, выбор того или иного варианта оказания услуги возлагается на Заказчика.<br>
7.11. Со стороны Туроператора для исполнения настоящего Договора, в том числе для направления какой-либо информации в рамках Договора, используется исключительно адрес электронной почты: <a href="mailto:<?php echo $mp_user->email; ?>"><?php echo $mp_user->email; ?></a><br>
7.12. Со стороны Заказчика для исполнения настоящего Договора, в том числе для направления какой-либо информации, получения информации в рамках Договора, используется исключительно адрес электронной почты:  <?php  echo $manualPayment['order']['email']; ?>  указанный Заказчиком при оформлении онлайн-запроса на Сайте.<br>

<p class="uk-text-center uk-text-bold">8. АДРЕСА И РЕКВИЗИТЫ СТОРОН. ИНФОРМАЦИЯ О ТУРОПЕРАТОРЕ И О ЛИЦЕ, ПРЕДОСТАВИВШЕМ ТУРОПЕРАТОРУ ФИНАНСОВОЕ ОБЕСПЕЧЕНИЕ ОТВЕТСТВЕННОСТИ</p>
<p class="uk-text-center uk-text-bold"> 1. Сведения о Туроператоре:</p>
<table>
<tbody>
<tr>
<td>
Полное наименование<br>
</td>
<td>
Общество с ограниченной ответственностью <?php echo $name_ru; ?><br>
</td>
</tr>
<tr>
<td>
Сокращенное наименование<br>
</td>
<td>
<strong>ООО "<?php echo $name_ru; ?>"</strong><br>
</td>
</tr>
<tr>
<td>
Адрес (место нахождения)<br>
</td>
<td>
<?php echo $adres_ru; ?><br>
</td>
</tr>
<tr>
<td>
Почтовый адрес<br>
</td>
<td>
<?php echo $adres_po_ru; ?><br>
</td>
</tr>
<tr>
<td>
Реестровый номер<br>
</td>
<td>
РТО <?php echo $mbt; ?><br>
</td>
</tr>
<tr>
<td>
Телефон/факс<br>
</td>
<td>
+7 (812) 6470690<br>
</td>
</tr>
<tr>
<td>
Электронная почта/Сайт<br>
</td>
<td>
<?php echo $email; ?> 
<a href="http://www.russiantour.com">www.russiantour.com</a><br>
</td>
</tr>
</tbody>
</table>
2. Сведения об организации, предоставившей Туроператору финансовое<br>
обеспечение ответственности Туроператора:<br>
<table>
<tbody>
<tr>
<td>
Вид финансового обеспечения ответственности туроператора<br>
</td>
<td>
Страхования гражданской ответственности туроператора - внутренний туризм<br> международный въездной<br>
</td>
</tr>
<tr>
<td>
Размер финансового обеспечения<br>
</td>
<td>
500000 руб.<br>
</td>
</tr>
<tr>
<td>
Дата и срок действия договора страхования ответственности туроператора или банковской гарантии<br>
</td>
<td>
<?php echo $mbt_data_ru; ?><br>
</td>
</tr>
<tr>
<td>
Наименование организации, предоставившей финансовое обеспечение ответственности туроператора<br>
</td>
<td>
АО "Либерти Страхование"<br>
</td>
</tr>
<tr>
<td>
Адрес (местонахождение)<br>
</td>
<td>
196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А<br>
</td>
</tr>
<tr>
<td>
Почтовый адрес<br>
</td>
<td>
196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А<br>
</td>
</tr>
<tr>
<td>
Сайт<br>
</td>
<td>
<a href="http://www.liberty24.ru">www.liberty24.ru</a><br>
</td>
</tr>
<tr>
<td>
Электронная почта<br>
</td>
<td>
<a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a><br>
</td>
</tr>
 <tr>
 <td colspan="2">
 
Туроператор: <?php echo $ooo_ru; ?>
<img src="<?php echo $pech_url; ?>" style="height: auto;width:400px;">



 
 
</td>
</tr>
 
<tr>
 <tr>
<td colspan="2">
 <p class="uk-text-center uk-text-bold"> Приложение № 1 к договору-оферте о реализации туристского продукта<br> №B<?php echo $manualPayment['order']['id']; ?> от  <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?> . Россия, Санкт-Петербург </p>
Заявка на реализацию туристского продукта<br>
Дата заявки:<b>  <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?>   г</b><br>
Категория и вид визы:<b>  Туристическая виза</b><br>
Консульское учреждение: <b> Генеральное консульство России в Италии</b><br> 
Услуга полного оформления  (Да/Нет): <b>Да</b><br>
Необходимость оформления медицинского страховани (Да/Нет): <b>Да</b><br> 
Консульский сбор (Да/Нет): <b>Да</b><br>
<?php if (isset($manualPayment['clients'][1]) && $manualPayment['clients'][1]['nome'] == '')  { ?>
   <p class="uk-text-center "> Данные о туристах:</p>
   ГРУППА  <b>(<?php echo $manualPayment['order']['clientcount'];?>)</b> человек<br>
<?php } ?>
<?php  foreach ($manualPayment['clients'] as $n=>$client) {
    if ($n==1 && $client['nome'] == '')
        break;
    ?>
		 <p class="uk-text-center "> Турист № : <?php  echo ($n+1);?> </p>
Данные о туристе: <br>
Фамилия, имя: <b>  <?php  echo translitText($client['cognome']); ?>  <?php  echo translitText($client['nome']); ?> </b> <br>
 

Пол:<b>  <?php   if(($client['sex']) == 'm'){ echo 'Мужской';}else{ echo 'Женский';};  ?>   </b> <br>
<?php echo (($client['numero_di_passaporto']=='')?'':'Паспорт: '.$client['numero_di_passaporto'].'<br/>') ;?>
<?php echo (($client['birthday']=='1970-01-01')?'':'Дата рождения: '.date('d/m/Y',strtotime($client['birthday'])).'<br/>') ;?>

Гражданство/подданство:<b> Италия</b><br>
Турпакета (тур):<b>    <?php echo $manualPayment['trip']['name_rus'];?> </b> <br> 
Города посещения:<b> <?php echo $manualPayment['tour']->citys_rus ;?></b><br>
 
<?php echo (($manualPayment['order']['date_from']=='0000-00-00')?'':' Дата начала тура: '.date('d/m/Y',strtotime($manualPayment['order']['date_from'])).'<br/>') ;?>
<?php echo (($manualPayment['order']['date_to']=='0000-00-00')?'':'  Дата завершения тура: '.date('d/m/Y',strtotime($manualPayment['order']['date_to'])).'<br/>') ;?>
 
Авиабилеты:<b> Нет</b> <br>
Ж/Д билеты:<b> Нет</b> <br>
  <?php   } ?>
    <?php echo (($manualPayment['order']['cancellation']=='')?'':'<br>УСЛОВИЯ АННУЛЯЦИИ:<br> '.$manualPayment['order']['cancellation'].'<br/>') ;?>
 
</td>
</tr>

</tbody>
</table>
</td>
 </div>
  <div class="uk-width-1-2">
<p class="uk-text-center"><strong>CONTRATTO-OFFERTA</strong><br>
<strong>di realizzazione di un Prodotto turistico № </strong> B<?php echo $manualPayment['order']['id']; ?><br>
Russia, San Pietroburgo<br>
  <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?><br></p>
La Società a responsabilità limitata <?php echo $name; ?>. di seguito denominata “Tour Operator”, nella persona del Direttore Generale Cheremshenko Olga Nikolaevna, agente in base allo Statuto, da una parte, 
  e  <b><?php  echo  $manualPayment['order']['nomecgome'];  ?>  </b> ,   mail <?php  echo $manualPayment['order']['email']; ?>     di seguito denominato(a) “Committente”, dall’altra parte, unitamente e singolarmente denominati le “Parti”, hanno stipulato il presente Contratto in merito a quanto segue:<br>
Il presente Contratto-offerta (di seguito “Contratto”, “Contratto-offerta”) è una proposta scritta (Offerta) del Tour Operator di concludere un Contratto indirizzato al Committente ai sensi degli artt. 432-444 del Codice Civile della Federazione Russa.<br>
Il Contratto viene stipulato tramite accettazione totale e incondizionata dell’offerta da parte del Committente nelle modalità stabilite al c. 3 art. 438 del Codice Civile della Federazione Russa ed è conferma del rispetto della forma scritta del contratto ai sensi del c. 3 art. 434 del Codice Civile della Federazione Russa.<br>
Il testo del presente Contratto-offerta è disponibile all’indirizzo:  <div class="uk-text-truncate">  http://www.russiantour.com/ita/bonifico?payment_id=m<?php echo $manualPayment['order']['id']; ?></div><br>
<p ><strong>Terminologia:</strong><br></p>
Committente del Prodotto turistico (Committente): il turista o una terza persona che ha prenotato un Prodotto turistico a nome del turista, incluso il rappresentante legale di un turista minorenne;<br>
<strong>Sito:</strong> sito internet <strong> www.russiantour.com , </strong>con l’aiuto del quale il Committente svolge azioni per l’ordine di un prodotto turistico (azioni volte alla stipula del presente Contratto, incluse le azioni per la scelta dei servizi con l’aiuto di software del sito e redazione della richiesta);<br>
Offerta: proposta scritta del Tour Operator di stipulare un Contratto;<br>
Accettazione dell’offerta: accettazione totale e incondizionata da parte del Committente dell’offerta tramite compimento delle azioni indicate nell’offerta;<br>
Attività turistica: attività del Tour Operator e dell’agenzia turistica, nonché altra attività relativa all’organizzazione di viaggi;<br>
Prodotto turistico: insieme di servizi per il trasporto e l’alloggio, forniti a un prezzo complessivo(indipendentemente dall’inserimento nel prezzo generale del costo di escursioni e/o altri servizi) su contratto per la realizzazione di un Prodotto turistico;<br>
Attività del Tour Operator: attività per la formazione, promozione e realizzazione di un Prodotto turistico svolta da una persona giuridica (di seguito: Tour Operator).<br>
<p class="uk-text-center"><strong>1. OGGETTO DEL CONTRATTO</strong></p>
1.1. In accordo al Contratto, il Tour Operator è tenuto a garantire la prestazione al Committente dei servizi facenti parte del Prodotto turistico, il cui elenco completo è indicato nella Richiesta di realizzazione di un Prodotto turistico (Allegato № 1 al Contratto) (di seguito: Prodotto turistico), e il Committente è tenuto a pagare il Prodotto turistico alle condizioni del presente Contratto..<br>
1.2. Le informazioni sul Committente nella misura necessaria all’esecuzione del Contratto sono indicate nella richiesta di realizzazione di un Prodotto turistico.<br>
1.3. Il Tour Operator fornisce al Committente i seguenti servizi:<br>
1.3.1. Assistenza per l’ottenimento del visto consolare;<br>
1.3.2. Singoli pacchetti turistici (tour di alcuni giorni);<br>
1.3.3 Acquisto di biglietti aerei<br>
1.4. L’elenco dei servizi indicati al p. <br>
1.3. del presente Contratto non è esaustivo e può essere ampliato dalle parti.<br>
1.5. L’elenco concreto dei servizi in base al Contratto è scelto dal Committente nella Richiesta di realizzazione di un prodotto turistico (Allegato № 1 al Contratto).<br>
1.6. Il presente Contratto costituisce un’offerta pubblica.<br>
1.7 L’accettazione del presente Contratto-offerta da parte del Committente è costituita dall’apposizione del segno di spunta “V” nel campo “HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA <?php echo $name; ?>, all’indirizzo: <div class="uk-text-truncate">   https://www.russiantour.com/ita/bonifico?payment_id=m<?php echo $manualPayment['order']['id']; ?> </div> <br>
L’apposizione da parte del Committente del segno di spunta “V” nel campo indicato costituisce il consenso del Committente alle condizioni del Contratto, l’accettazione da parte del Committente delle condizioni del presente Contratto.<br>
 

<p class="uk-text-center">2. PREZZO DEL PRODOTTO TURISTICO. MODALITÀ DI PAGAMENTO</p>
2.1. Il prezzo complessivo del Prodotto turistico acquistato con Contratto (costo del servizio) è determinato in base alla richiesta di realizzazione di un Prodotto turistico ed è indicato nella fattura. La fattura costituisce parte integrante del Contratto.Quando il tasso di cambio rublo/euro, fissato dalla banca centrale della Federazione Russa, dal momento dell’accettazione della proposta per la realizzazione di un prodotto turistico vari più del 10% (dieci per cento) rispetto al tasso di cambio rublo/euro al momento del pagamento del saldo definitivo per il pacchetto turistico, il Tour Operator ha la facoltà di modificare il prezzo dei servizi proporzionalmente alla variazione del tasso di cambio fissato della Banca Centrale della Federazione Russa.<br>
2.2. Il pagamento deve essere effettuato dal Committente nelle seguenti modalità: In base alla fattura emessa tramite bonifico bancario. <br>
2.3. Il Committente è tenuto a pagare i servizi del Tour Operator entro 14 giorni dall’emissione della fattura, a meno che non sia richiesto un pagamento più veloce per l’urgenza della pratica. Il Committente è tenuto a inviare la conferma dell’avvenuto pagamento entro 3 giorni dall’emissione della fattura. In caso di pagamento urgente, il Committente è tenuto a inviare la conferma dell’avvenuto pagamento entro 1 giorno dall’emissione della fattura. Il pagamento può essere effettuato da terzi. Se il Cliente non rispetta i termini e la procedura di pagamento, il Tour Operator ha il diritto di rifiutare unilateralmente di prestare i servizi previsti dal presente Contratto.<br>
2.4. Le commissioni bancarie sono suddivise tra le Parti nelle seguenti modalità: il Committente paga le commissioni della sua banca, le commissioni delle altre banche sono pagate dal Tour Operator (SHA).<br>
2.5. Il costo dei servizi è indicato nella fattura in Euro.<br>
2.6. Valuta di pagamento: Euro.<br>
2.7. La data di pagamento da parte del Committente dei servizi prestati dal Tour Operator in base al Contratto è la data di versamento del denaro sul conto bancario del Tour Operator. Nel caso di variazioni del tasso di cambio rublo/euro, fissato dalla banca centrale della Federazione Russa, superiori al 10% (dieci per cento) tra la data dell’emissione della fattura  e la data di ricezione dei fondi sul conto bancario del Tour Operator, il Tour Operator ha la facoltà di modificare il prezzo dei servizi proporzionalmente alla variazione del tasso di cambio fissato della Banca Centrale della Federazione Russa.<br>
2.8. La fattura dal momento del suo pagamento totale da parte del Committente conferma la prestazione dei servizi da parte del Tour Operator per la realizzazione di un prodotto turistico ed è riconosciuta dalle Parti come atto di esecuzione dei lavori.<br>
2.9. Il Committente ha il diritto di annullare in tutto o in parte il pacchetto turistico. L'elenco specifico delle condizioni di cancellazione del Contratto è indicato dal Tour Operator nell'Allegato № 1 al contratto-offerta di realizzazione di un Prodotto turistico.<br>



<p class="uk-text-center"><strong>3. INTERAZIONE TRA LE PARTI</strong></p>
3.1. Il Tour operator è tenuto a:<br>
- fornire al Committente informazioni attendibili sulle caratteristiche di consumo del Prodotto turistico, nonché le informazioni previste dalla Richiesta di realizzazione di un Prodotto turistico;<br>
- consegnare al Committente i documenti per il visto man mano che sono pronti;<br>
- intraprendere le misure necessarie per garantire la sicurezza dei dati personali del Committente, anche durante il loro trattamento e utilizzo;<br>
- prestare al Committente tutti i servizi che fanno parte del Prodotto turistico, autonomamente o coinvolgendo terze persone alle quali il Tour operator affida l’esecuzione di parte o di tutti gli obblighi nei confronti del Committente e/o del turista (nel caso in cui il Committente ordini al Tour Operator un Prodotto turistico a nome del turista, oppure sia il legale rappresentante di un turista minorenne).<br>
3.2. Il Tour Operator ha il diritto di:<br>
- non avviare la prestazione dei servizi secondo il Contratto prima del pagamento totale dei servizi prenotati da parte del Committente.<br>
3.3. Il Committente è tenuto a:<br>
- pagare il Prodotto turistico ai sensi del Contratto;<br>
- far comprendere al turista le condizioni del Contratto, altre informazioni indicate nel Contratto e relativi allegati, a trasmettergli i documenti ricevuti dal Tour Operator per il compimento del viaggio del turista;<br>
- fornire al Tour Operator i propri riferimenti, i riferimenti del turista, necessari per un contatto operativo e per la preparazione del Prodotto turistico;<br>
- fornire al Tour operator i documenti e le informazioni necessarie all’esecuzione del Contratto, ai sensi delle richieste presentate dal Tour operator (richieste necessarie per i documenti e le informazioni pubblicate sul Sito oppure portate a conoscenza del Committente in altra sede).<br>
- controllare il visto ricevuto dal Tour Operator e comunicare entro 24 ore dalla ricezione della documentazione la presenza di aventuali errori o discrepanze tra i dati del visto e quelli del passaporto;<br>
- controllare il pacchetto turistico acquistato;<br>
- controllarei biglietti aerei.<br>
3.4. Il Committente ha il diritto di:<br>
- ottenere una copia dell’atto di inserimento delle informazioni del Tour Operator nel registro;<br>
- ottenere i documenti per il visto;<br>
- ottenere il pacchetto turistico acquistato;<br>
- ottenere i biglietti aerei acquistati.<br>
3.5. Le Parti sono responsabili per la mancata esecuzione o l’esecuzione inappropriata dei propri obblighi ai sensi della legislazione della Federazione Russa.<br>
3.6. Il Tour Operator non è responsabile per:<br>
- le azioni delle Ambasciate (Consolati) di stati esteri, di altre organizzazioni, ad eccezione delle organizzazioni coinvolte dal Tour Operator per la prestazione dei servizi che fanno parte del Prodotto turistico, incluso il rifiuto dell’Ambasciata (Consolato) straniero al rilascio (ritardo) del visto di ingresso ai turisti per il loro percorso di viaggio qualora il Tour Operator o direttamente il Committente abbiano presentato nei tempi previsti all’Ambasciata (Consolato) straniero tutti i documenti necessari.<br>

- per il rifiuto ai turisti di uscita/ingresso al controllo passaporti o al controllo doganale o per l’applicazione di sanzioni al Committente da parte degli organi del controllo passaporti e del controllo doganale per motivi non relativi allo svolgimento da parte del Tour Operator dei propri obblighi in base al Contratto;<br>
- per l’annullamento, la sostituzione, o la modifica di orario dei voli aerei, compresi quelli dovuti agli scioperi delle compagnie aeree; <br>
- per le modifiche degli orari programmati/ dell’orario di partenza o di arrivo/ del percorso di viaggio o per l'annullamento dei treni, dei pullman, dei traghetti e delle navi da crociera; <br>
- per modifiche all’orario di inizio (termine) dei servizi, inclusi nel pacchetto turistico; <br>
- per l’annullamento dei servizi inclusi nel pacchetto turistico, per motivi non imputabili al Tour Operator. <br>


3.7. Il Committente ha il diritto di avanzare reclami per i documenti del visto ricevuti in conseguenza delle azioni del Tour Operator entro 24 ore dal momento della ricezione. Al termine del periodo indicato, i reclami per i documenti del visto non saranno accettati dal Tour Operator, ed il Tour operator non sarà responsabile per eventuali problemi al punto di controllo di frontiera. <br>
3.8. Con la firma del presente Contratto, il Committente conferma di aver preso visione di tutte le norme e le modalità pubblicate sul Sito  <div class="uk-text-truncate">  https://www.russiantour.com/ita/bonifico?payment_id=m<?php echo $manualPayment['order']['id']; ?> </div> <br> 
<p class="uk-text-center"><strong>4. RECLAMI. MODALITÀ DI RISOLUZIONE DELLE CONTROVERSIE</strong></p>
4.1. I reclami relativi alla violazione delle condizioni del contratto di realizzazione del Prodotto turistico sono da presentarsi dal Committente al Tour Operator nelle modalità e alle condizioni previste dalla legislazione della Federazione Russa.<br>
4.2. I reclami relativi alla qualità del Prodotto turistico fornito dal Tour Operator sono da presentarsi in forma scritta entro 20 giorni dalla data di termine della validità del contratto di realizzazione del Prodotto turistico e sono soggetti all’esame entro 10 giorni dalla data di ricevimento dei reclami.<br>
4.3 Nel caso in cui il reclamo venga accettato, in quanto problemi o errori siano chiaramente imputabili all’operato del Tour Operator in base ai termini del contratto , il Tour Operator provvederà esclusivamente al rimborso della quota percepita in base ai servizi fatturati in relazione al presente Contratto, e di nessun’altra spesa dovuta a servizi acquistati personalmente o con terze parti da parte del Committente o collegata a problemi personali del Committente a causa di mancato viaggio o ritardi. 
4.4. Le controversie relative al presente Contratto sono soggette all'esame del tribunale della Federazione Russa situato nel soggetto federale russo in cui è registrato il Tour Operator, ai sensi delle disposizioni della vigente legislazione della Federazione Russa.<br>
<p class="uk-text-center"><strong>5. RESPONSABILITÀ</strong></p>
5.1. Il Tour Operator non è responsabile per le seguenti circostanze:<br>
- il passaporto del Committente (turista) non viene accettato dagli enti consolari e/o dalle autorità russe al controllo di frontiera per il rilascio/accettazione del visto perché danneggiato, consumato, non firmato, privo dei timbri necessari e/o pagine libere; nonché perché alla scadenza dello stesso mancano meno di 6 mesi;<br>
- il Committente ha fornito dati falsi, incorretti, errati, incompleti nella compilazione della Richiesta per la realizzazione del prodotto turistico;<br>
- rifiuto di redazione e rilascio del visto a discrezione dell’ente consolare per motivi indipendenti dal Tour Operator;<br>
- rifiuto di redazione e rilascio del visto per motivi di scorretta compilazione da parte del Committente del modulo elettronico, presenza di errori nei documenti forniti dal Committente, fornitura di un set di documenti incompleto, etc.;<br>
- rifiuto di ingresso con visto elettronico nel punto di attraversamento del confine di stato della Federazione Russa o problemi riscontrati all'uscita dal territorio della Federazione Russa in relazione alle circostanze elencate nel punto 7.9 del presente Contratto<br>
- ritardo nel rilascio del visto per motivi di chiusura improvvisa dell’ente consolare a terzi, inclusi i rappresentanti delle società turistiche, dei tour operator; modifiche improvvise all’orario di apertura dell’ufficio visti dell’ente consolare;<br>
- perdita di documenti per colpa dei corrieri (servizi) postali; consegna a un indirizzo errato; ritardo della consegna per colpa dei corrieri (servizi) postali;<br>
- rilascio del visto con data di ingresso errata o con data non corrispondente alla data indicata, da parte dell’ente consolare;<br>
- durante il controllo di frontiera il Committente (turista) ha difficoltà con i dipendenti del servizio di frontiera, in conseguenza delle quali al Committente (turista) viene rifiutato l’ingresso nella Federazione Russa;<br>
- per il mancato rispetto da parte del Committente delle raccomandazioni del Tour Operator indicate al punto 7.7 del Contratto;<br>
- per l’impossibilità da parte del Committente (turista) di utilizzare i servizi prestati dal Tour Operator in base al presente Contratto nel caso in cui tale impossibilità sia sorta per circostanze per le quali il Tour Operator non è responsabile.<br>
-  problemi al punto di controllo di frontiera per errori o discrepanze tra i dati del visto e quelli del passaporto, in seguito al mancato controllo e alla mancata comunicazione degli errori da parte del committente al Tour Operator entro 24 ore dal momento della ricezione della documentazione .<br>

5.2. In caso di rifiuto al rilascio del visto per colpa di errori imputabili al Tour Operator, quest’ultimo è tenuto a rimborsare completamente al Committente le spese sostenute per i servizi prestati da parte del Tour Operator per l’ottenimento del visto nell’ambito del presente Contratto.<br>
5.3. Le Parti sono esonerate dalla responsabilità per il mancato adempimento parziale o totale degli obblighi del Contratto, se tale mancato adempimento è conseguenza di circostanze di forza maggiore, ovvero di circostanze sorte per condizioni d’emergenza e imprevedibili, che le Parti non avrebbero potuto prevedere, né prevenire con misure ragionevoli.<br>
Se tali circostanze dovessero prolungarsi per più di 14 (quattordici) giorni di calendario, ognuna delle Parti avrà il diritto di rifiutare l’adempimento degli obblighi del Contratto e in tal caso nessuna delle Parti avrà il diritto alla compensazione dall’altra Parte delle possibili perdite motivate da cause di forza maggiore.<br>

 

<p class="uk-text-center"><strong>6. TEMPISTICHE DI VALIDITÀ DEL CONTRATTO, MODALITÀ DI MODIFICHE E SCIOGLIMENTO DEL CONTRATTO</strong></p>
6.1. Il Contratto entra in vigore dalla data di accettazione da parte del Committente delle sue condizioni ai sensi del p.1.7 del Contratto ed è valido fino all’esecuzione degli obblighi delle Parti, ma non oltre il 31 dicembre 2020.<br>
6.2. Il Contratto può essere modificato o sciolto nei casi e nelle modalità previste dalla legislazione della Federazione Russa è su accordo delle Parti, se tale accordo viene redatto in forma scritta.<br>
Qualsiasi modifica al Prodotto turistico, altre condizioni della Richiesta per la realizzazione di un prodotto turistico sono ammesse solo su accordo scritto aggiuntivo delle Parti.<br>
La forma scritta delle modifiche della Richiesta per la realizzazione di un prodotto turistico è considerata rispettata anche in caso di accordo di tali modifiche tra le Parti per posta elettronica. A questo scopo da parte del Tour Operator è utilizzato il seguente indirizzo di posta elettronica: <a href="mailto: <?php echo $mp_user->email; ?>"> <?php echo $mp_user->email; ?></a><br>
Da parte del Committente viene utilizzato il seguente indirizzo di posta elettronica <?php  echo $manualPayment['order']['email']; ?>.<br>
 
<p class="uk-text-center"><strong>7. DISPOSIZIONI CONCLUSIVE</strong></p>
7.1. Le informazioni sulla eventuale stipula a favore dei turisti del contratto di assicurazione volontaria, tra le cui condizioni vi è l’obbligo dell’assicuratoredi pagare e/o rimborsare le spese di pagamento dell’assistenza medica urgente e d’emergenza prestata al turista nel territorio del paese di soggiorno temporaneo al momento dell’evento assicurato relativamente alla ricezione di un trauma, all’intossicazione, a una improvvisa malattia grave o al peggioramento di una malattia cronica, inclusa l’evacuazione medica del turista nel paese di soggiorno temporaneo e dal paese di soggiorno temporaneo nel paese di residenza e/o il rimpatrio della salma (resti) del turista dal paese di soggiorno temporaneo nel paese di residenza ai sensi dei dettami della legislazione della Federazione Russa e del paese di soggiorno temporaneo, sono indicate nella Richiesta di realizzazione del Prodotto turistico.<br>
7.2. Il Committente fornisce il consenso e conferma altresì che, ai sensi dei requisiti della Legge Federale della Federazione Russa N°152-FZ del 27.07.2006 “Sui dati personali”, ha ottenuto, da parte di tutti i turisti indicati nell’Allegato № 1 al Contratto, il consenso al trattamento e alla trasmissione dei propri dati personali e dei dati personali delle persone indicate nella richiesta di realizzazione di un prodotto turistico, al Tour Operator e a terze persone per l’esecuzione del Contratto (incluso l’ottenimento del visto, l’acquisto dei biglietti aerei, del pacchetto turistico).<br>
7.3. Nel caso in cui il Committente effettui l’ordine di un Prodotto turistico negli interessi di un turista, il Committente conferma di possedere i potere necessari alla rappresentanza degli interessi del turista nei confronti del Tour Operator.<br>
7.4. Tutti gli allegati, nonché le modifiche (integrazioni) al Contratto ne costituiscono parte integrante.<br>
7.5. Per tutto quanto non regolamentato dal Contratto, le Parti si atterranno al diritto della Federazione Russa.<br>
7.6. In caso di richiesta al Consolato Generale della Federazione Russa a Milano per questioni di ottenimento del visto nel caso in cui il periodo di soggiorno nel paese superi i 14 giorni, di norma, occorre presentare ulteriori documenti a conferma della prenotazione alberghiera, le fatture degli hotel o dei sistemi di prenotazione, ricevute di pagamento del 100% dei servizi. Se il Committente non ha prenotato per tempo un alloggio per tutta la durata del soggiorno nel paese di destinazione e non ha raccolto autonomamente tutto il set di documenti necessari, si sconsiglia di presentare i documenti all’ufficio visti del Consolato Generale della Federazione Russa a Milano. Se il Committente prenderà la decisione di presentare la richiesta di rilascio del visto nel caso sopra descritto in assenza dei documenti indicati, il Tour Operator non è responsabile delle possibili negative conseguenze per il Committente, incluso il possibile rifiuto di rilascio del visto. In tal caso i servizi del Contratto saranno considerati forniti completamente da parte del Tour Operator. L’obbligo per il pagamento di qualsiasi rimborso a favore del Committente non sorge, così come non sorge l’obbligo per il Tour Operator di ripresentare i documenti allo stesso ente consolare o ad un altro.<br>
7.7. Per i minorenni che prevedono un viaggio nella Federazione Russa non accompagnati da entrambi i genitori, sono altresì necessari i seguenti documenti: atto di nascita riportante i dati dei genitori, certificato dello stato di famiglia, copia di un documento di identità del genitore o dei genitori, dichiarazione di consenso del genitore (dei genitori) del minorenne per la visita della Federazione Russa senza la sua (loro) presenza.<br>
7.8 Nel caso in cui sia impossibile ottenere il visto nei tempi standard a causa di festività italiane o russe o nel caso in cui la posizione geografica del Committente non permetta la consegna dei documenti con corriere (servizio) postale entro la data del viaggio indicata nella Richiesta, il pagamento effettuato è soggetto a rimborso sulla carta bancaria /sul conto corrente bancario del Committente entro tre giorni dalla data del pagamento. Il Committente ha il diritto, cambiando la data di ottenimento del visto, di stipulare un nuovo Contratto a nuove condizioni. Il Committente ha altresì il diritto di utilizzare la possibilità di rilascio del visto urgente o urgente in giornata.<br>

7.9 Se il committente opta per la richiesta del visto elettronico russo, è tenuto a stampare e conservare la documentazione (la notifica del rilascio del visto elettronico e la polizza assicurativa) ricevuta dal Tour Operator in modo da poter presentarla alla compagnia aerea e alla polizia di frontiera della Federazione Russa. Il committente è consapevole che il visto elettronico è valido esclusivamente per l’ingresso nella regione per la quale è stato richiesto (San-Pietroburgo e la regione di Leningrado oppure la regione di Kaliningrad, oppure per una delle aree del Distretto Federale dell’Estremo Oriente Russo) e per il soggiorno e gli spostamenti nei limiti della stessa regione/area della Federazione Russa; che l'ingresso nel territorio della Federazione Russa e l'uscita dal territorio della Federazione Russa sono possibili solo attraverso i punti di controllo di frontiera situati nello stesso distretto/area federale, e che i punti di controllo di frontiera sono a San Pietroburgo: Aeroporto Pulkovo, “Grande porto di San Pietroburgo” (Stazione marittima), “ Porto passeggeri di San Pietroburgo " - nella regione di Leningrado: checkpoint automobilistici "Torfyanovka "," Brusnichnoe "," Svetogorsk ", checkpoint automobilistico e pedonale" Ivangorod ", porto" Vysotsk " - Regione di Kaliningrad: checkpoint ferroviari “Khasan”, “Makhalino”, “Mamonovo”, “Sovetsk”, checkpoint automobilistici “Bagrationovsk”, “Gusev”, “Mamonovo (Gzhehotki)”, “Mamonovo (Gronovo)”, “Marine” , "Pogranichny", "Sovetsk", "Chernyshevskoye", checkpoint marittimi "Kaliningrad (sezioni nelle città di Kaliningrad, Baltiysk e Svetly)", aeroporto "Kaliningrad (Khrabrovo)"- Primorski Krai: punti di controllo aerei «Vladivostok (Aeroporto di Knevichy)», punti di controllo marittimi «Vladivostok», «Zarubino», «Posiet», punti di controllo ferroviari "Pogranichny", "Khasan", "Makhalino", punti di controllo stradali «Poltavka», «Turiy Rog» - nella Sakhalin Oblast: punti di controllo aerei «Yuzhno-Sakhalinsk (Khomutovo Airport)», punti di controllo marittimi «Korsakov» - Kamchatka Krai: punti di controllo aerei «Petropavlovsk-Kamchatsky (Yelizovo Airport)», punti di controllo marittimi «Petropavlovsk-Kamchatsky» - nell’Amur Region: punti di controllo aerei «Blagoveschensk» - nel Khabarovsk Krai: punti di controllo aerei «Khabarovsk (Novy Airport)» - nel Chukotka Autonomous Okrug: punti di controllo aerei «Anadyr (Ugolny Airport)» - Republic of Buryatia: punti di controllo aerei:«Ulan-Ude (Muhino)» -  Zabaikalsky Krai: punti di controllo aerei «Chita (Kadala)»; che il periodo massimo di permanenza in Russia con il visto elettronico è di 8 giorni/7notti; che il rilascio del visto elettronico non garantisce l’ingresso nel territorio della Federazione Russa; che l'ingresso potrà essere negato in un punto di frontiera qualora sorgessero le circostanze di cui alla Legge federale n. 114 FZ "Sulla procedura di uscita dalla Federazione Russa e di Ingresso nella Federazione Russa" del 15 agosto 1996; che il visto elettronico non è modificabile ne prolungabile, e che in caso di qualsiasi problema al punto di controllo di frontiera il Tour Operator non potrà fornire alcun tipo di supporto.<br>
 

7.10. Con la stipula del presente Contratto il Committente conferma di aver preso visione di tutte le condizioni e richieste degli enti consolari delle norme di ingresso nella Federazione Russa, di soggiorno e di uscita dalla stessa, e che le stesse sono chiare. Il rischio conseguente a certe azioni, di scelta di una opzione piuttosto di un’altra per la prestazione dei servizi è a carico del Committente.<br>
7.11 Da parte del Tour Operator per l’esecuzione del presente Contratto, incluso l’invio di qualsivoglia informazione nell’ambito del Contratto, utilizzare esclusivamente l’indirizzo: <a href="mailto:<?php echo $mp_user->email; ?>"><?php echo $mp_user->email; ?></a><br>
7.12 Da parte del Committente per l’esecuzione del presente Contratto, incluso l’invio di qualsivoglia informazione, il ricevimento di informazioni nell’ambito del Contratto, utilizzare esclusivamente l’indirizzo  <?php  echo $manualPayment['order']['email']; ?> indicato dal Committente durante la compilazione della richiesta online sul Sito.<br>

 

<p class="uk-text-center"><strong>8. INDIRIZZI E COORDINATE DELLE PARTI. INFORMAZIONI SUL TOUR OPERATOR E SOCIETA` CHE FORNISCE AL TOUR OPERATOR LA GARANZIA FINANZIARIA DELLA RESPONSABILITÀ</strong></p>
1. Informazioni sul Tour Operator:<br>
<table>
<tbody>
<tr>
<td>
<p>Denominazione completa</p>
</td>
<td>
<p>Società a responsabilità limitata <?php echo $name; ?><?php echo $name; ?></p>
</td>
</tr>
<tr>
<td>
<p>Denominazione abbreviata</p>
</td>
<td>
<p><?php echo $name; ?></p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo (sede legale)</p>
</td>
<td>
<p><?php echo $adres; ?></p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo postale</p>
</td>
<td>
<p><?php echo $adres_po; ?></p>
</td>
</tr>
<tr>
<td>
<p>Numero di registro</p>
</td>
<td>
<p>MBT <?php echo $mbt; ?></p>
</td>
</tr>
<tr>
<td>
<p>Telefono / fax</p>
</td>
<td>
<p>+7 (812) 6470690</p>
</td>
</tr>
<tr>
<td>
<p>Posta elettronica / Sito</p>
</td>
<td>
<p><?php echo $email; ?> </p>
<p><a href="http://www.russiantour.com/">www.russiantour.com</a></p>
</td>
</tr>
</tbody>
</table>
<p>2. Informazioni sull’organizzazione che fornisce al Tour Operator la garanzia finanziaria della responsabilità del Tour Operator:</p>
<table>
<tbody>
<tr>
<td>
<p>Tipo di garanzia finanziaria della</p>
<p>Responsabilità del tour operator</p>
</td>
<td>
<p>Assicurazione di responsabilità civile dei tour operator - turismo interno</p>
<p>ed internazionale in ingresso</p>
</td>
</tr>
<tr>
<td>
<p>Importo della garanzia finanziaria</p>
</td>
<td>
<p>500000 rubli</p>
</td>
</tr>
<tr>
<td>
<p>Data e termine di validità del contratto di assicurazione della responsabilità del Tour operator o della garanzia bancaria</p>
</td>
<td>
<p><?php echo $mbt_data; ?></p>
</td>
</tr>
<tr>
<td>
<p>Denominazione dell’organizzazione che fornisce la garanzia finanziaria della responsabilità del tour operator</p>
</td>
<td>
<p>Liberty Insurance Ltd.</p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo (sede legale)</p>
</td>
<td>
<p>196044 San Pietroburgo, Moskovsky Pr. 79A</p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo postale</p>
</td>
<td>
<p>196044 San Pietroburgo, Moskovsky Pr. 79A</p>
</td>
</tr>
<tr>
<td>
<p>Sito</p>
</td>
<td>
<p><a href="http://www.liberty24.ru/">www.liberty24.ru</a></p>
</td>
</tr>
<tr>
<td>
<p>E-mail</p>
</td>
<td>
<p><a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a></p>
</td>
</tr>

 <tr>
 <td colspan="2">
Tour Operator: <?php echo $ooo; ?>
<img src="<?php echo $pech_url; ?>" style="height: auto;width: 400px;" >
 


</td>
</tr>

 <tr>
<td colspan="2">
 <p class="uk-text-center uk-text-bold">Allegato № 1 al contratto-offerta di realizzazione di un Prodotto turistico<br> № B<?php echo $manualPayment['order']['id']; ?> del  <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?>  . Russia, San Pietroburgo </p>
Richiesta di realizzazione di un Prodotto turistico<br>
Data della Richiesta:<b>  <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?> г</b><br>
Categoria e tipo di visto:<b>  Visto turistico</b><br>
Ente consolare: <b> Consolato Generale della Russia in Italia</b><br> 
Procedura completa (Si/No) : <b>Sì</b><br>
Necessità di richiedere l’assicurazione medica (Si/No): <b>Sì</b><br> 
Diritti consolari (Si/No): <b>Sì</b><br>

<?php if (isset($manualPayment['clients'][1]) && $manualPayment['clients'][1]['nome'] == '')  { ?>
   <p class="uk-text-center ">Dati dei turisti:</p>
   Gruppo  <b>(<?php echo $manualPayment['order']['clientcount'];?>)</b> persone<br>
<?php } ?>


       
<?php  foreach ($manualPayment['clients'] as $n=>$client) {
    if ($n==1 && $client['nome'] == '')
        break;
    ?>
		 <p class="uk-text-center "> Turista № : <?php  echo ($n+1);?> </p>
Dati del turista: <br>
Cognome, nome: <b> <?php  echo  $client['cognome']; ?>  <?php  echo  $client['nome']; ?>  </b> <br>
<?php echo (($client['birthday']=='1970-01-01')?'':'Data di nascita: '.date('d/m/Y',strtotime($client['birthday'])).'<br/>') ;?>
Sesso:<b>  <?php   if(($client['sex']) == 'm'){ echo 'Maschio';}else{ echo 'Femmina';};  ?>     </b> <br>
<?php echo (($client['numero_di_passaporto']=='')?'':'Passaporto: '.$client['numero_di_passaporto'].'<br/>') ;?>
Cittadinanza/nazionalità:<b> Italia</b><br>
Pacchetto turistico:<b>   <?php echo $manualPayment['trip']['name_ita'];?>   </b><br>
Citta da visitare:<b> <?php echo $manualPayment['tour']->citys ;?></b><br>

 <?php echo (($manualPayment['order']['date_from']=='0000-00-00')?'':' Data di inizio del tour: '.date('d/m/Y',strtotime($manualPayment['order']['date_from'])).'<br/>') ;?>
<?php echo (($manualPayment['order']['date_to']=='0000-00-00')?'':'  Data di termine del tour: '.date('d/m/Y',strtotime($manualPayment['order']['date_to'])).'<br/>') ;?>
Biglietti aerei  (Si/No): <b>No</b>  <br>
Biglietti ferroviari (Si/No):<b> No</b><br>
  
  <?php   } ?>
  
  <?php echo (($manualPayment['order']['cancellation_ita']=='')?'':'<br>CONDIZIONI DI CANCELLAZIONE:<br> '.$manualPayment['order']['cancellation_ita'].'<br/>') ;?>
 

</td>
</tr>
</tbody>
</table>
</td>
</tr>
</div>
</div>
</div>
 
 