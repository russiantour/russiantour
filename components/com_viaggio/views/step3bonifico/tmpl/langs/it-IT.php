<?php
include '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';
$mp_user = JFactory::getUser($manualPayment['userId']);
?>
<ul id="tab-content" class="tm-tab-content uk-switcher">
    <li class="uk-active">

        <br><br>

        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1">
                <div class="uk-panel-box-secondary uk-text-center   uk-text-bold uk-text-uppercase">  Prima di effettuare il pagamento leggere le condizioni generali di contratto.</div>


 <br>
                <div ><div class="uk-float-left">
                        <label class="uk-form container-checkbox uk-margin-small-top">
                            <input  type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);" <?php if ($manualPayment['payment_initiated'] == 2) echo 'checked="checked"'; ?>><span class="checkmark"></span>
                        </label> </div><div style="padding-top: 7px;" ><b>
                            <a data-uk-modal="{target:'#modal11'}" class="uk-text-uppercase" href=" " >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA <?php echo $name; ?>  </a></b></div></div>



                <div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large uk-overflow-container ">
                        <a href="" class="uk-modal-close uk-close"></a>
          <?php  include ('it-IT_contrattoBancario.php');  ?>
 </div>
                    </div>
                </div>

                <br>


               <div class="uk-panel uk-panel-box  uk-container-left uk-width-medium-1-2">

                    Data: <?php echo $manualPayment['timeCreated']; ?> <br>

  Tour: <?php echo $manualPayment['trip']['name_ita'];?><br>
<?php echo (($manualPayment['order']['date_from']=='0000-00-00')?'':' Data di arrivo: '.date('d/m/Y',strtotime($manualPayment['order']['date_from'])).'<br/>') ;?>
<?php echo (($manualPayment['order']['date_to']=='0000-00-00')?'':'  Data di partenza: '.date('d/m/Y',strtotime($manualPayment['order']['date_to'])).'<br/>') ;?>

  Numero di persone:  <?php echo $manualPayment['order']['clientcount'];?><br>

                    Nome e cognome : <?php echo $manualPayment['fio']; ?> <br>
                    Telefono: <?php echo $manualPayment['telefon']; ?><br>
                    Email: <?php echo $manualPayment['email']; ?><br>
                    Causale del pagamento: <?php echo $manualPayment['order']['id']; ?> 
					<?php  echo   $manualPayment['clients'][0]['cognome'];  ?> 
					<?php  echo  $manualPayment['clients'][0]['nome'];  ?> </span> <?php// echo $manualPayment['description']; ?><br>
                    Importo:  <?php echo $manualPayment['amountEuro']; ?>  <i class="uk-icon-euro"></i><br>
<?php echo $manualPayment['pole1']; ?>
                </div>
                <div class=" uk-width-medium-1-1 uk-text-justify">
                Istruzioni per il pagamento: si tratta di un bonifico internazionale, non è un bonifico SEPA, quindi non c’è un codice IBAN ma un numero di conto corrente russo. In caso di richieste durante il pagamento sul vostro online banking selezionate “Pagamento internazionale” e come nazione ricevente “Russia”. Le commissioni devono essere condivise: selezionare “SHA”. Una volta effettuato il pagamento vi preghiamo di notificarcelo via mail, allegando una copia della transazione. La mancata notifica del pagamento potrebbe comportare ritardi nell’avvio della pratica o problemi nella conferma della prenotazione. <br>
</div>

                <button  data-uk-toggle="{target:'#my-id'}"   id="Inviare" disabled   class="uk-button uk-button-primary Inviare"    onclick="jQuery.ajax({
  url: '/?option=com_viaggio&task=bonifico2&paymentID=<?php echo $manualPayment['paymentID']; ?>',context: document.body}).done(function(data) {jQuery( '#payTime' ).html( data );});jQuery(this).hide();" id="Inviare" disabled>coordinate bancarie</button>

   <div  aria-hidden="true" class="<?php if ($manualPayment['payment_initiated'] != 2) echo 'uk-hidden'; ?>" id="my-id">

<div class="uk-panel uk-panel-box  uk-container-center uk-width-medium-1-2"><div class="uk-panel-badge uk-badge uk-badge-danger">pagamento </div><h3 class="uk-panel-title">COORDINATE BANCARIE</h3> <i class="uk-icon-university"></i>
<br/>
     <span id="payTime"><?php  if($manualPayment['payButton']) echo '  Data di accettazione del contratto-offerta: '.$manualPayment['payButton']; ?> </span><br/>
<?php echo $bank; ?>
  <span> <?php echo $manualPayment['order']['id']; ?> <?php  echo   $manualPayment['clients'][0]['cognome'];  ?> <?php  echo  $manualPayment['clients'][0]['nome'];  ?> </span>  <br>
  <b><span>Importo: <?php echo $manualPayment['amountEuro']; ?> </span> € </b>  <br>

</div> </div>


            </div>

        </div>
    </li>
