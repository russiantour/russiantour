<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$baseUrl = JUri::base();
$user   = JFactory::getUser();

$doc->addStyleSheet('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
?>
<script  type="text/javascript" src="/components/com_viaggio/media/js/disqusloader.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<form action="/index.php?option=com_viaggio&task=sendorder" method="post"  class="uk-form" id="viaggioOpenTourForm" >
<input type="hidden" name="tourid" value="<?php echo $this->item->id; ?>">
<div class="uk-spb-100 tm-opacity-90 uk-grid-margin-large   uk-panel-box item-page uk-padding-remove  " itemscope itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="it-IT" />
		
		<div class="uk-width-medium-1-1 uk-first-column uk-padding-remove "><?php echo $this->item->widgetkit; ?>
		

	<div class="uk-text-center uk-margin-large">
				<h1  class="uk-heading-large" itemprop="headline"><?php echo $this->item->name; ?></h1>
    
    <p class="uk-text-large uk-width-large-8-10 uk-text-justify uk-align-center"><?php echo $this->item->desc; ?></p>
</div>

		</div>
		
	
	 
			 

			<div class="uk-width-large-8-10 uk-text-justify uk-align-center" itemprop="articleBody">
				<h2 class="uk-heading-large uk-dropdown-top uk-text-center uk-article-title uk-heading-line"> <span id="cost"><?php echo ViaggioHelpersFrontend::getCost( $this->item->id, 0, 2 ); ?></span>€  </h2>
                <?php
                if (isset($_COOKIE['ViaggiobuyManualPaymentId']))
                {
                    echo '<a href="https://www.viaggio-russia.com/index.php?option=com_viaggio&task=buyManual&payment_id="'.$_COOKIE['ViaggiobuyManualPaymentId'].'>Оплатить</a>';
                }
                ?>
				<div class="uk-grid">
				<div class="uk-width-1-2 uk-text-center uk-text-large ">

			<?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_MOSCA'); ?><span class="uk-icon-plane rotate90 uk-icon-small"></span> <span id="tourItemfrom"><?php echo date('d/m/Y', strtotime($this->item->from)); ?></span> </div>
			<div class="uk-width-1-2 uk-text-center uk-text-large  ">
			<?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_ITALIA'); ?><span class="uk-icon-plane uk-icon-small"> </span>  <span id="tourItemTo"><?php echo date('d/m/Y', strtotime($this->item->to)); ?></span></div></div>	 
		 
		<?php if($this->item->russiandays){ ?>
			<div style="width:100%">
			<select name="transib" id="transib" class="calc">
				<option id="classic" value="classic"><?php echo JText::_('COM_VIAGGIO_CLASSIC'); ?></option>
				<option id="superior" value="superior"><?php echo JText::_('COM_VIAGGIO_SUPERIOR'); ?></option>
				<option id="superiorplus" value="superiorplus"><?php echo JText::_('COM_VIAGGIO_SUPERIOR_PLUS'); ?></option>
				<option id="nostalgic" value="nostalgic"><?php echo JText::_('COM_VIAGGIO_NOSTALGIC'); ?></option>
				<option id="bolshoy4" value="bolshoy4"><?php echo JText::_('COM_VIAGGIO_BOLSHOY_IV'); ?></option>
				<option id="platinum5" value="platinum5"><?php echo JText::_('COM_VIAGGIO_PLATINUM_5'); ?></option>
			</select>
			</div>
			<label> <input name="needvisamongolia" type="checkbox" id="visamongolia"  class="calc" <?php if(!$this->item->visamongolia){ echo 'disabled'; } ?>>   <?php echo JText::_('COM_VIAGGIO_VISA_MONGOLIA'); ?>  </label>
			<label> <input name="needvisachina" type="checkbox" id="visachina"  class="calc" <?php if(!$this->item->visachina){ echo 'disabled'; } ?>>  <?php echo JText::_('COM_VIAGGIO_VISA_CHINA'); ?>   </label>
		<?php } ?>
		
		<?php if(!$this->item->nostell1 && !$this->item->russiandays){ ?>
		<div class="uk-grid   uk-grid-small   uk-width-medium-1-1" style="margin-top: 43px;"> 
			<div class="uk-width-medium-1-2 uk-text-center" id="4steelButton"><a href="#">
				<span class="uk-icon-star"></span>
				<span class="uk-icon-star"></span>
				<span class="uk-icon-star"></span>
				<span class="uk-icon-star"></span> <br> <span class="uk-h4"><?php echo JText::_('COM_VIAGGIO_4_STELL'); ?></span></a>
			</div>
			<div class="uk-width-medium-1-2 uk-text-center" id="3steelButton"><a href="#">
				<span class="uk-icon-star"></span>
				<span class="uk-icon-star"></span>
				<span class="uk-icon-star"></span><br> 
				<span class="uk-h4"><?php echo JText::_('COM_VIAGGIO_3_STELL'); ?></span></a>
			</div> 
		</div>
		<div style="clear:both;"></div>
		<br/><br/>
		
		<div style="display: none;width:100%">
		<select name="stellcount" id="stellcount" class="calc">
			<?php if($this->item->inn3cost){ ?>
			<option id="stellcount3" value="3" <?php if(!$this->item->inn3cost){ echo 'disabled'; } ?>><?php echo JText::_('COM_VIAGGIO_3_STELL'); ?></option>
			<option id="stellcount4" value="4"><?php echo JText::_('COM_VIAGGIO_4_STELL'); ?></option>
			<?php }else{ ?>
			<option id="stellcount4" value="4"><?php echo JText::_('COM_VIAGGIO_4_STELL'); ?></option>
			<option id="stellcount3" value="3" <?php if(!$this->item->inn3cost){ echo 'disabled'; } ?>><?php echo JText::_('COM_VIAGGIO_3_STELL'); ?></option>
			<?php } ?>
		</select>
		</div>
		<?php } ?>
		<div style="width:100%">
		<?php if(!$this->item->nostell1 && !$this->item->russiandays){ ?>
		<label> <input name="insurance" type="checkbox" id="insurance"  class="calc" disabled>   <?php echo JText::_('COM_VIAGGIO_MEZZA_PENSIONE'); ?></label>
		<?php } ?>
		</div>
		<?php/*oninvalid="this.setCustomValidity('Feel it')"*/?>
		<div class="uk-grid" id="tourClientsBlock">
			<div class="uk-width-1-1 uk-form tourClientsElement">
				<input  name="cognome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top uk-width-medium-2-10 "> 
				<input name="nome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10 "> 
				<select name="sex[]" class="uk-margin-small-top  uk-width-medium-1-10" autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option>
				<option value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select> 
				<input name="nascita[]" style=" margin-top: 5px; " class=" uk-width-medium-1-6  participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" required placeholder="Data di nascita" data-uk-datepicker="{format:'DD/MM/YYYY'}"> 
				<select style="display: none;" name="nazionalita[]" class="uk-margin-small-top uk-width-medium-1-6  " autocomplete="off"><option name="nazionalita[]" value="">- <?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?> -</option></select>  	 <a href="#" style="display: none;" id="addTourClient" class="calc uk-button "><?php echo JText::_('COM_VIAGGIO_ADD'); ?></a> 
                          
			</div>
			<div class="uk-width-1-1  uk-form tourClientsElement">
				<input name="cognome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top  uk-width-medium-2-10 "> 
				<input name="nome[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10 ">
				<select name="sex[]" class="uk-margin-small-top uk-width-medium-1-10 " autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option>
				<option value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select> 
				<input name="nascita[]" style=" margin-top: 5px; " class=" uk-width-medium-1-6  participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" required placeholder="Data di nascita" data-uk-datepicker="{format:'DD/MM/YYYY'}"> 
				<select style="display: none;" name="nazionalita[]" class="uk-margin-small-top  uk-width-medium-1-6 " autocomplete="off"><option name="nazionalita[]" value="">- <?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?> -</option></select>     
				<button class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser deleteTourClient calc"><i class="uk-icon-remove"></i></button>                          
			</div>
		</div>
		
	
		
		
 
	 
 <div > 
 <?php if( $this->item->user->guest ){ ?>
  <div ><h3>
    <?php echo JText::_('COM_VIAGGIO_REGISTRATION'); ?>
    </h3> </div>
  <div> 
   <?php echo JText::_('COM_VIAGGIO_INDICARE_I_DATI_DELLA_PERSONA'); ?>
    </div>
 <?php  }  var_dump($this->item->user->guest,$this->item->user)?>
        <div>
            <?php if (!$this->item->user->guest) { $type='hidden'; ?>
                <?php echo JText::_('COM_VIAGGIO_NOMECOGNOME'); ?>: <?php echo $this->item->user->nomecgome; ?> |
                <?php echo JText::_('COM_VIAGGIO_TELEFONE'); ?>: <?php echo $this->item->user->telephone; ?> |
                E-mail: <?php echo $this->item->user->email; ?>
           <?php } else { $type = 'text';  }?>
            <input required  type="<?php echo $type; ?>" name="nomecgome" placeholder="<?php echo JText::_('COM_VIAGGIO_NOMECOGNOME'); ?>" value="<?php echo $this->item->user->nomecgome; ?>" class=" uk-margin-small-top  uk-width-medium-3-10">
            <input required  type="<?php echo $type; ?>" name="telefone" placeholder="<?php echo JText::_('COM_VIAGGIO_TELEFONE'); ?>" value="<?php echo $this->item->user->telephone; ?>" class=" uk-margin-small-top  uk-width-medium-3-10">
            <input required type="<?php echo $type; ?>" name="email" <?php if(!$this->item->user->guest){ echo 'value="'.$this->item->user->email.'" '; } ?> placeholder="E-mail" class="uk-margin-small-top uk-width-medium-3-10">
    </div>  
  
    </div>
	</div>
	
	<button type="submit" class="uk-button uk-hidden uk-margin-small-top uk-button-primary uk-text-small  "><?php echo JText::_('COM_VIAGGIO_PROSEGUIRE'); ?> </button>
<!--<input type="submit" value="<?php echo JText::_('COM_VIAGGIO_ORDER'); ?>" class=" uk-margin-small-top  uk-width-medium-4-10">-->
</form>
<div id="modal6" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
	<div class="uk-modal-dialog">
		<button type="button" class="uk-modal-close uk-close"></button>
		<form action="/index.php?option=com_viaggio&task=register" id="viaggioRegisterForm">
		<div class="uk-modal-header">
			<h2>Password</h2>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
		
			<input name="password1" required  type="text"  placeholder="Password"  class="edinfopass uk-margin-small-top  uk-width-medium-4-10">
			<input name="password2" required  type="text"  placeholder="Password"  class="edinfopass uk-margin-small-top  uk-width-medium-4-10">
			<span id="validpass"></span>
		</p>
		<div class="uk-modal-footer uk-text-right">
			<button type="button" class="uk-button uk-modal-close">Cancel</button>
			<button type="submit" id="viaggioRegisterSubmit" class="uk-button uk-button-primary" disabled>Save</button>
		</div>
		</form>
	</div>
</div>
 <div class=" uk-width-medium-9-10 uk-text-center ">
	<h3>
    <?php echo JText::_('COM_VIAGGIO_INORMATION'); ?> 
    </h3>
</div>
<?php
	//определяем идентификатор тура для disqusа
	$disqus_identifier = 0;
	if( $this->item->comment_id ){
		$disqus_identifier = $this->item->comment_id;
	}else{
		$disqus_identifier = $this->item->id;
	}
?>
<div style="background-color: #fff;">
<div class="uk-margin10  uk-text-center ">
	<div class="uk-width-medium-1-1 uk-row-first">
		<ul class="uk-tab" data-uk-tab="{connect:'#tab-content'}">
		<?php if($this->item->fulltext){ ?>
		<li class="uk-active"  ><a href="#">Programma</a></li>
		<?php } ?>
		<?php if($this->item->tab4){ ?>
		<li class=""><a href="#">Cosa visiterete</a></li>
		<?php } ?>
		<?php if($this->item->tab2){ ?>
		<li class=""><a href="#">Incluso/Non incluso</a></li>
		<?php } ?>
		<?php if($this->item->tab3){ ?>
		<li class=""><a href="#">Itinerario del viaggio</a></li>
		<?php } ?>
		<li class="disqus-comments-tab"><a href="#">Domande e Recensioni</a></li>
		<li class="uk-tab-responsive uk-active uk-hidden"><a>Tab</a>
		<div class="uk-dropdown uk-dropdown-small"> </div>
		</li>
		</ul>
		<ul id="tab-content" class="uk-switcher uk-margin">
		<?php if($this->item->fulltext){ ?>
		<li><?php echo $this->item->fulltext; ?></li>
		<?php } ?>
		<?php if($this->item->tab4){ ?>
		<li class="1"><?php echo $this->item->tab4; ?></li>
		<?php } ?>
		<?php if($this->item->tab2){ ?>
		<li class=""><div class="uk-panel-body"><?php echo $this->item->tab2; ?></div></li>
		<?php } ?>
		<?php if($this->item->tab3){ ?>
		<li class=""><?php echo $this->item->tab3; ?></li>
		<?php } ?>
		<li class="">
		<div id="disqus_thread<?php echo $this->item->id; ?>"></div>
<script>
//хак для виджекит, чтобы использовать несколько галереи на странице
jQuery(window).trigger('resize');
jQuery('.uk-tab').on('click', function(){
	jQuery(window).trigger('resize');
})

jQuery('.disqus-comments-tab').on('click', function(){
	disqusLoader( document.querySelector( '#disqus_thread<?php echo $this->item->id; ?>'),
	{
		scriptUrl:		'//viaggiorussia.disqus.com/embed.js',
		disqusConfig:	function()
		{
			this.page.identifier	= <?php echo $disqus_identifier; ?>;
			this.page.title			= '<?php echo addslashes( $this->item->name ); ?>';
		}
	});
});
</script>
<noscript>Please enable JavaScript to view the <a rel="nofollow"  href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
	
		</li>
		</ul>
	</div>
	
	
	
</div>
</div>  
 	</div>

 

 <?php //print_r($this->item->jsItem); ?>
<script>
jQuery(document).ready(function($) {
	$( ".datepicker-here" ).datepicker({
	  firstDay: 1,changeMonth: true,changeYear: true,dateFormat: "dd/mm/yy",yearRange:'c-117:c'
	});
	
	var passFields = $('.edinfopass')
    //validResult = $("#validpass");
	passFields.on('input', comparingPasswords)
	$('#viaggioRegisterForm').on('submit', comparingPasswords)
 
	function comparingPasswords (e){
		var output = 'Ok',
			err = false,
			p1 = $.trim(passFields.eq(0).val()),
			p2 = $.trim(passFields.eq(1).val());
		if(p1 == '' || p2 == '') {
			//output = 'Заполните поля!';
			err = true;
		} else {
			if(p1 != p2) {
				//output = 'Пароли не совпадают!'
				err = true;
			}
		}
		if(err){
			$('#viaggioRegisterSubmit').prop('disabled', true)
		}else{
			$('#viaggioRegisterSubmit').prop('disabled', false)
		}
		//validResult.text(output)  
		if(err)  e.preventDefault()
	}
	
	//clientform
	var html = `<div class="uk-width-1-1 uk-form tourClientsElement"><input required name="cognome[]" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top  uk-width-medium-2-10"> <input required name="nome[]" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10"> <select name="sex[]" class="uk-margin-small-top uk-width-medium-1-10" autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option><option value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select> <input required name="nascita[]" style=" margin-top: 5px; " class="uk-width-medium-1-6 participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_DATA_DI_NASCITA'); ?>" data-uk-datepicker="{format:'DD/MM/YYYY'}"> <select style="display: none;" name="nazionalita[]" class="uk-margin-small-top uk-width-medium-1-6" autocomplete="off"><option name="nazionalita[]" value=""><?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?>-  -</option></select>   <button class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser deleteTourClient calc"><i class="uk-icon-remove"></i></button></div>`;
	
	$( '#4steelButton' ).click(function() {
		$('#stellcount').val(4); calc();
		return false;
	});
	
	$( '#3steelButton' ).click(function() {
		$('#stellcount').val(3); calc();
		return false;
	});
	
	$( "#addTourClient" ).click(function() {
		$('#tourClientsBlock').append(html);
		$('.datepicker-here').datepicker({
	  firstDay: 1,changeMonth: true,changeYear: true,dateFormat: 'dd/mm/yy',yearRange:'c-117:c'
	});
		if( $('.tourClientsElement').length > 1 ) $( "#addTourClient" ).hide(); calc();
		return false;
	});
	
	$( '.deleteTourClient' ).live('click', function() {
		$(this).parent().remove()
		if( $('.tourClientsElement').length < 2 ) $( "#addTourClient" ).show(); calc();
		return false;
	});
	
	$('.participants-birthdate').live('change', function() {
		calc();
	});
				
	$( '.calc' ).click(function() {
		calc();
	});
	
	$( '.calc' ).live('change', function() {
		calc();
	});
	
	var item = {};
	
	item.inn4cost = <?php echo intval($this->item->inn4cost); ?>;
	item.inn3cost = <?php echo intval($this->item->inn3cost); ?>;
		
	item.halfboard4 = <?php echo intval($this->item->halfboard4); ?>;
	item.halfboard3 = <?php echo intval($this->item->halfboard3); ?>;
		
	item.singola4 = <?php echo intval($this->item->singola4); ?>;
	item.singola3 = <?php echo intval($this->item->singola3); ?>;
	
	item.classic = <?php echo intval($this->item->classic); ?>;
	item.classicsingola = <?php echo intval($this->item->classicsingola); ?>;
	item.superior = <?php echo intval($this->item->superior); ?>;
	item.superiorsingola = <?php echo intval($this->item->superiorsingola); ?>;
	item.superiorplus = <?php echo intval($this->item->superiorplus); ?>;
	item.superiorplussingola = <?php echo intval($this->item->superiorplussingola); ?>;
	item.nostalgic = <?php echo intval($this->item->nostalgic); ?>;
	item.nostalgicsingola = <?php echo intval($this->item->nostalgicsingola); ?>;
	item.bolshoy4 = <?php echo intval($this->item->bolshoy4); ?>;
	item.bolshoy4singola = <?php echo intval($this->item->bolshoy4singola); ?>;
	item.platinum5 = <?php echo intval($this->item->platinum5); ?>;
	item.platinum5singola = <?php echo intval($this->item->platinum5singola); ?>;
	item.russiandays = <?php echo intval($this->item->russiandays); ?>;
		
	item.statndarvisa = <?php echo intval($this->item->statndarvisa); ?>;
	item.urgentevisa = <?php echo intval($this->item->urgentevisa); ?>;
	item.sendvisa = <?php echo intval($this->item->sendvisa); ?>;
	item.visa7day = <?php echo intval($this->item->visa7day); ?>;
	item.visa1day = <?php echo intval($this->item->visa1day); ?>;
	
	checkSettings()
	
	function checkSettings()
	{
		var clientsCount = $('.tourClientsElement').length;
		if(clientsCount == 1)
		{
			if(!item.singola3)
			{
				$('#3steelButton').addClass('disabledstell');
				$('#stellcount3').attr('disabled','disabled');
			}else{
				$('#3steelButton').removeClass('disabledstell');
				$('#stellcount3').removeAttr('disabled');
			}
			if(!item.singola4)
			{
				$('#4steelButton').addClass('disabledstell');
				$('#stellcount4').attr('disabled','disabled');
			}else{
				$('#4steelButton').attr('disabled','disabled');
				$('#stellcount4').removeAttr('disabled');
			}
			
			
			if(!item.classicsingola)
			{
				$('#classic').attr('disabled','disabled');
			}else{
				$('#classic').removeAttr('disabled');
			}
			
			if(!item.superiorsingola)
			{
				$('#superior').attr('disabled','disabled');
			}else{
				$('#superior').removeAttr('disabled');
			}
			
			if(!item.superiorplussingola)
			{
				$('#superiorplus').attr('disabled','disabled');
			}else{
				$('#superiorplus').removeAttr('disabled');
			}
			
			if(!item.nostalgicsingola)
			{
				$('#nostalgic').attr('disabled','disabled');
			}else{
				$('#nostalgic').removeAttr('disabled');
			}
			
			if(!item.bolshoy4singola)
			{
				$('#bolshoy4').attr('disabled','disabled');
			}else{
				$('#bolshoy4').removeAttr('disabled');
			}
			
			if(!item.platinum5singola)
			{
				$('#platinum5').attr('disabled','disabled');
			}else{
				$('#platinum5').removeAttr('disabled');
			}
		}else if(clientsCount == 2)
		{
			if(!item.inn3cost)
			{
				$('#3steelButton').addClass('disabledstell');
				$('#stellcount3').attr('disabled','disabled');
			}else{
				$('#3steelButton').removeClass('disabledstell');
				$('#stellcount3').removeAttr('disabled');
			}
			if(!item.inn4cost)
			{
				$('#4steelButton').hide();
				$('#stellcount4').attr('disabled','disabled');
			}else{
				$('#4steelButton').show();
				$('#stellcount4').removeAttr('disabled');
			}
			
			
			if(!item.classic)
			{
				$('#classic').attr('disabled','disabled');
			}else{
				$('#classic').removeAttr('disabled');
			}
			
			if(!item.superior)
			{
				$('#superior').attr('disabled','disabled');
			}else{
				$('#superior').removeAttr('disabled');
			}
			
			if(!item.superiorplus)
			{
				$('#superiorplus').attr('disabled','disabled');
			}else{
				$('#superiorplus').removeAttr('disabled');
			}
			
			if(!item.nostalgic)
			{
				$('#nostalgic').attr('disabled','disabled');
			}else{
				$('#nostalgic').removeAttr('disabled');
			}
			
			if(!item.bolshoy4)
			{
				$('#bolshoy4').attr('disabled','disabled');
			}else{
				$('#bolshoy4').removeAttr('disabled');
			}
			
			if(!item.platinum5)
			{
				$('#platinum5').attr('disabled','disabled');
			}else{
				$('#platinum5').removeAttr('disabled');
			}
		}
		
		var stellcount = $('#stellcount').val();
		if(stellcount == 4)
		{
			if(item.halfboard4)
			{
				$('#insurance').removeAttr('disabled');
			}else{
				$('#insurance').attr('disabled','disabled');
			}
		}else if(stellcount == 3)
		{
			if(item.halfboard3)
			{
				$('#insurance').removeAttr('disabled');
			}else{
				$('#insurance').attr('disabled','disabled');
			}
		}else{
			$('#insurance').attr('disabled','disabled');
		}
	}
	
	function calc(){
		checkSettings()
		var clientsCount = $('.tourClientsElement').length;
		var insurance = 0;
		if( $('#insurance').is(':checked') ) insurance = 1;
		var stellcount = $('#stellcount').val();
		if(!stellcount) stellcount = 0;
		var birthday1 = 0;
		var birthday2 = 0;
		var transib =  $('#transib').val();
		if(!transib) transib = 0;
		var needvisamongolia = 0;
		if( $('#visamongolia').is(':checked') ) needvisamongolia = 1; 
		var needvisachina = 0;
		if( $('#visachina').is(':checked') ) needvisachina = 1;
		$('.participants-birthdate').each(function(index, element) {
			//alert(index)
			if(index == 0) birthday1 = $(this).val(); 
			if(index == 1) birthday2 = $(this).val();
		});
		if(!birthday1) birthday1 = 0;
		if(!birthday2) birthday2 = 0;
		$.post('/index.php?option=com_viaggio&task=calcTourCost&tour_id=<?php echo $this->item->id; ?>&stellcount='+stellcount+'&insurance='+insurance+'&count='+clientsCount+'&birthday2='+birthday2+'&birthday1='+birthday1+'&transib='+transib+'&needvisamongolia='+needvisamongolia+'&needvisachina='+needvisachina, function(data) {
		  $('#cost').html(data);
		});
	}
});
</script>