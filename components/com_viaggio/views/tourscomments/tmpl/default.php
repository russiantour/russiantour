<?php 
/**
 * @version    CVS: 1.0.0
 * @package    Com_Russiantour
 * @author     Тимур <timach-ufa@ya.ru>
 * @copyright  2017 Тимур
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');//

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
?>
<link rel="stylesheet" href="/templates/yoo_eat/css/bootstrap.css" type="text/css"/>
<div style="text-align: justify;">Siamo interessati a conoscere le vostre opinioni, al fine di ottimizzare i nostri servizi, migliorarci continuamente e garantire professionalità ai nostri clienti.</div>
<div style="text-align: justify;">Ora e` possibile inserire direttamente sul sito la vostra opinione, le vostre recensioni verranno pubblicate e se necessario riceverete una risposta direttamente su questa pagina.<hr id="system-readmore" />Pubblicheremo qui anche le nuove recensioni lasciateci su <a href="https://www.facebook.com/russiantour/reviews/" target="_blank" rel="noopener noreferrer">facebook</a>, <a href="https://it.tripadvisor.ch/Attraction_Review-g298507-d19317123-Reviews-Russian_Tour_International-St_Petersburg_Northwestern_District.html" target="_blank" rel="noopener noreferrer">tripadvisor</a> e <a href="https://www.google.it/?gws_rd=ssl#q=russian+tour+international&amp;lrd=0x4696316541e843a3:0x2301a20d3afa830e,1" target="_blank" rel="noopener noreferrer">google</a>, per cominciare ne inseriamo a seguire qualcuna degli ultimi anni.<br /><br /><a href="ita/recensioniform" rel="nofollow"><strong>COMPILA IL FORM PER ESPRIMERE LA TUA OPINIONE</strong></a></div>
<p> </p>
<div id="flexicontent">
    <div class="uk-grid">
        <form class="uk-form"  style="width: 95%;" action="" method="post"
              name="adminForm" id="adminForm">
                <?php foreach ($this->items as $i => $item) : ?>
                <?php
                    $date = explode('-',$item->date);
                    $date = $date[2].' '.month_translate($date[1]).' '.$date[0];
                    ?><br>
                    <article class="uk-comment">
                        <header class="uk-comment-header">
                            <img class="uk-comment-avatar" src="images/uikit_avatar.svg" alt="Avatar" width="50" height="50" />
                            <h4 class="uk-comment-title"><?php echo $item->author_name; ?></h4>
                            <p class="uk-comment-meta"><?php echo $date; ?></p>
                        </header>
                        <div class="uk-comment-body">
                            <p style="text-align: justify;">
                                <?php echo $item->text; ?>
                            </p>
                        </div>
                    </article>
                <?php endforeach; ?>
            <?php echo $this->pagination->getListFooter(); ?>
            <input type="hidden" name="task" value=""/>
            <input type="hidden" name="boxchecked" value="0"/>
            <?php echo JHtml::_('form.token'); ?>
        </form>
    </div>
</div>
<style>
    .pagination, .pagination ul {
        text-align: center;
    }
    .pagination ul>li {
        display: inline;
    }
    .pagination ul > li > a, .pagination ul>li>span {
        float: left;
        padding: 4px 12px;
        line-height: 24px;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #eaeaea;
        border-left-width: 0;
    }
</style>
<script>
    var Joomla = {};
    Joomla.submitform = function(){document.adminForm.submit()}
</script>
<?php
function month_translate($m)
{
    switch($m){
        case 1:
            $mru = 'января';
            $mit = 'gennaio';
            break;
        case 2:
            $mru = 'февраля';
            $mit = 'febbraio';
            break;
        case 3:
            $mru = 'марта';
            $mit = 'marzo';
            break;
        case 4:
            $mru = 'апреля';
            $mit = 'aprile';
            break;
        case 5:
            $mru = 'мая';
            $mit = 'maggio';
            break;
        case 6:
            $mru = 'июня';
            $mit = 'giugno';
            break;
        case 7:
            $mru = 'июля';
            $mit = 'luglio';
            break;
        case 8:
            $mru = 'августа';
            $mit = 'agosto';
            break;
        case 9:
            $mru = 'сентября';
            $mit = 'settembre';
            break;
        case 10:
            $mru = 'октября';
            $mit = 'ottobre';
            break;
        case 11:
            $mru = 'ноября';
            $mit = 'novembre';
            break;
        case 12:
            $mru = 'декабря';
            $mit = 'dicembre';
            break;
    }
    return $mit;
}

?>