
<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;
$subcat = explode('/',$_SERVER['REQUEST_URI'])[1];
$word = '';
switch ($subcat) {
    case 'mosca-sanpietroburgo':
        $word = 'stanza doppia';
        break;
    case 'crociera-sul-volga':
        $word = 'cabina doppia';
        break;
    case 'transiberiana':
        $word = 'cuccetta 2/4 posti';
        break;
    case 'anello-doro':
        $word = 'stanza doppia';
        break;
}
 ;
$doc = JFactory::getDocument();
$baseUrl = JUri::base();
$user   = JFactory::getUser();

//$doc->addStyleSheet($baseUrl.'templates/'.$this->template.'/css/style.css');
$doc->addStyleSheet('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
//print_r( $this->item ) 
if( $this->item->api->ErrorCode )
{
?>Возникла ошибка "<?php echo $this->item->api->ErrorMessage; ?>"<?php
}else{
?>Страница после оплаты, спасибо за покупку и т.д.<?php


?>
<script type="text/javascript">
setTimeout('location.replace("https://www.viaggio-russia.com/profile")', 3000);
</script>
<script  type="text/javascript" src="/components/com_viaggio/media/js/disqusloader.js"></script>
<script src="/images/js/jquery-ui.js"></script>
<form action="/index.php?option=com_viaggio&task=sendorder" method="post"  class="uk-form" id="1viaggioOpenTourForm" >
<input type="hidden" name="tourid" value="<?php echo $this->item->order->tour_id; ?>">
<div class="uk-spb-100 tm-opacity-90 uk-grid-margin-large   uk-panel-box item-page uk-padding-remove  " itemscope itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="it-IT" />
		<div class="uk-width-medium-1-1 uk-first-column uk-padding-remove "><?php echo $this->item->widgetkit; ?>
 <h3>Stiamo procedendo al pagamento, tra pochi secondi sarete reindirizzati al vostro profilo personale</h3>


 	</div>
 
 

 <?php //print_r($this->item->jsItem); ?>
<script>
jQuery(document).ready(function($) {
	$( ".datepicker-here" ).datepicker({
	  firstDay: 1,changeMonth: true,changeYear: true,dateFormat: "dd/mm/yy",yearRange:'c-117:c'
	});
	var passFields = $('.edinfopass')
    //validResult = $("#validpass");
	passFields.on('input', comparingPasswords)
	$('#viaggioRegisterForm').on('submit', comparingPasswords)
 
	function comparingPasswords (e){
		var output = 'Ok',
			err = false,
			p1 = $.trim(passFields.eq(0).val()),
			p2 = $.trim(passFields.eq(1).val());
		if(p1 == '' || p2 == '') {
			//output = 'Заполните поля!';
			err = true;
		} else {
			if(p1 != p2) {
				//output = 'Пароли не совпадают!'
				err = true;
			}
		}
		if(err){
			$('#viaggioRegisterSubmit').prop('disabled', true)
		}else{
			$('#viaggioRegisterSubmit').prop('disabled', false)
		}
		//validResult.text(output)  
		if(err)  e.preventDefault()
	}
	
	//clientform
	var html = '<div class="uk-width-1-1 uk-form tourClientsElement"><input required name="cognome[]" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_COGNOME'); ?>" class=" uk-margin-small-top  uk-width-medium-2-10"> <input required name="nome[]" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_NOME'); ?>" class="uk-margin-small-top uk-width-medium-2-10"><input  name="passport[]" required type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_PASSAPORTO'); ?>" class=" uk-margin-small-top uk-width-medium-2-10 "> <select name="sex[]" class="uk-margin-small-top uk-width-medium-1-10" autocomplete="off"><option value="m"><?php echo JText::_('COM_VIAGGIO_MASCHILE'); ?></option><option value="f"><?php echo JText::_('COM_VIAGGIO_FEMMINILE'); ?></option></select> <input required name="nascita[]" style=" margin-top: 5px; " class="uk-width-medium-1-6 participants-birthdate datepicker-here" data-date-format="dd/mm/yyyy" type="text" placeholder="<?php echo JText::_('COM_VIAGGIO_DATA_DI_NASCITA'); ?>" > <select style="display: none;" name="nazionalita[]" class="uk-margin-small-top uk-width-medium-1-6" autocomplete="off"><option name="nazionalita[]" value=""><?php echo JText::_('COM_VIAGGIO_NAZIONALITA'); ?>-  -</option></select>   <button class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser deleteTourClient calc"><i class="uk-icon-remove"></i></button></div>';
	
	$( '#4steelButton' ).click(function() {
		$('#stellcount').val(4); calc();
		return false;
	});
	
	$( '#3steelButton' ).click(function() {
		$('#stellcount').val(3); calc();
		return false;
	});
	
	$( "#addTourClient" ).click(function() {
		$('#tourClientsBlock').append(html);
		$('.datepicker-here').datepicker({
	  firstDay: 1,changeMonth: true,changeYear: true,dateFormat: 'dd/mm/yy',yearRange:'c-117:c'
	});
		if( $('.tourClientsElement').length > 1 ) $( "#addTourClient" ).hide(); calc();
		return false;
	});
	
	$( '.deleteTourClient' ).live('click', function() {
		$(this).parent().remove()
		if( $('.tourClientsElement').length < 2 ) $( "#addTourClient" ).show(); calc();
		return false;
	});
	
	$('.participants-birthdate').live('change', function() {
		calc();
	});
				
	$( '.calc' ).click(function() {
		calc();
	});
	
	$( '.calc' ).live('change', function() {
		calc();
	});
	
	var item = {};
	
	item.inn4cost = <?php echo intval($this->item->inn4cost); ?>;
	item.inn3cost = <?php echo intval($this->item->inn3cost); ?>;
		
	item.halfboard4 = <?php echo intval($this->item->halfboard4); ?>;
	item.halfboard3 = <?php echo intval($this->item->halfboard3); ?>;
		
	item.singola4 = <?php echo intval($this->item->singola4); ?>;
	item.singola3 = <?php echo intval($this->item->singola3); ?>;
	
	item.classic = <?php echo intval($this->item->classic); ?>;
	item.classicsingola = <?php echo intval($this->item->classicsingola); ?>;
	item.superior = <?php echo intval($this->item->superior); ?>;
	item.superiorsingola = <?php echo intval($this->item->superiorsingola); ?>;
	item.superiorplus = <?php echo intval($this->item->superiorplus); ?>;
	item.superiorplussingola = <?php echo intval($this->item->superiorplussingola); ?>;
	item.nostalgic = <?php echo intval($this->item->nostalgic); ?>;
	item.nostalgicsingola = <?php echo intval($this->item->nostalgicsingola); ?>;
	item.bolshoy4 = <?php echo intval($this->item->bolshoy4); ?>;
	item.bolshoy4singola = <?php echo intval($this->item->bolshoy4singola); ?>;
	item.platinum5 = <?php echo intval($this->item->platinum5); ?>;
	item.platinum5singola = <?php echo intval($this->item->platinum5singola); ?>;
	item.russiandays = <?php echo intval($this->item->russiandays); ?>;
		
	item.statndarvisa = <?php echo intval($this->item->statndarvisa); ?>;
	item.urgentevisa = <?php echo intval($this->item->urgentevisa); ?>;
	item.sendvisa = <?php echo intval($this->item->sendvisa); ?>;
	item.visa7day = <?php echo intval($this->item->visa7day); ?>;
	item.visa1day = <?php echo intval($this->item->visa1day); ?>;
	
	checkSettings()
	
	function checkSettings()
	{
		var clientsCount = $('.tourClientsElement').length;
		if(clientsCount == 1)
		{
			if(!item.singola3)
			{
				$('#3steelButton').addClass('disabledstell');
				$('#stellcount3').attr('disabled','disabled');
			}else{
				$('#3steelButton').removeClass('disabledstell');
				$('#stellcount3').removeAttr('disabled');
			}
			if(!item.singola4)
			{
				$('#4steelButton').addClass('disabledstell');
				$('#stellcount4').attr('disabled','disabled');
			}else{
				$('#4steelButton').attr('disabled','disabled');
				$('#stellcount4').removeAttr('disabled');
			}
			
			
			if(!item.classicsingola)
			{
				$('#classic').attr('disabled','disabled');
			}else{
				$('#classic').removeAttr('disabled');
			}
			
			if(!item.superiorsingola)
			{
				$('#superior').attr('disabled','disabled');
			}else{
				$('#superior').removeAttr('disabled');
			}
			
			if(!item.superiorplussingola)
			{
				$('#superiorplus').attr('disabled','disabled');
			}else{
				$('#superiorplus').removeAttr('disabled');
			}
			
			if(!item.nostalgicsingola)
			{
				$('#nostalgic').attr('disabled','disabled');
			}else{
				$('#nostalgic').removeAttr('disabled');
			}
			
			if(!item.bolshoy4singola)
			{
				$('#bolshoy4').attr('disabled','disabled');
			}else{
				$('#bolshoy4').removeAttr('disabled');
			}
			
			if(!item.platinum5singola)
			{
				$('#platinum5').attr('disabled','disabled');
			}else{
				$('#platinum5').removeAttr('disabled');
			}
		}else if(clientsCount == 2)
		{
			if(!item.inn3cost)
			{
				$('#3steelButton').addClass('disabledstell');
				$('#stellcount3').attr('disabled','disabled');
			}else{
				$('#3steelButton').removeClass('disabledstell');
				$('#stellcount3').removeAttr('disabled');
			}
			if(!item.inn4cost)
			{
				$('#4steelButton').hide();
				$('#stellcount4').attr('disabled','disabled');
			}else{
				$('#4steelButton').show();
				$('#stellcount4').removeAttr('disabled');
			}
			
			
			if(!item.classic)
			{
				$('#classic').attr('disabled','disabled');
			}else{
				$('#classic').removeAttr('disabled');
			}
			
			if(!item.superior)
			{
				$('#superior').attr('disabled','disabled');
			}else{
				$('#superior').removeAttr('disabled');
			}
			
			if(!item.superiorplus)
			{
				$('#superiorplus').attr('disabled','disabled');
			}else{
				$('#superiorplus').removeAttr('disabled');
			}
			
			if(!item.nostalgic)
			{
				$('#nostalgic').attr('disabled','disabled');
			}else{
				$('#nostalgic').removeAttr('disabled');
			}
			
			if(!item.bolshoy4)
			{
				$('#bolshoy4').attr('disabled','disabled');
			}else{
				$('#bolshoy4').removeAttr('disabled');
			}
			
			if(!item.platinum5)
			{
				$('#platinum5').attr('disabled','disabled');
			}else{
				$('#platinum5').removeAttr('disabled');
			}
		}
		
		var stellcount = $('#stellcount').val();
		if(stellcount == 4)
		{
			if(item.halfboard4)
			{
				$('#insurance').removeAttr('disabled');
			}else{
				$('#insurance').attr('disabled','disabled');
			}
		}else if(stellcount == 3)
		{
			if(item.halfboard3)
			{
				$('#insurance').removeAttr('disabled');
			}else{
				$('#insurance').attr('disabled','disabled');
			}
		}else{
			$('#insurance').attr('disabled','disabled');
		}
	}
	
	function calc(){
		checkSettings()
		var clientsCount = $('.tourClientsElement').length;
		var insurance = 0;
		if( $('#insurance').is(':checked') ) insurance = 1;
		var stellcount = $('#stellcount').val();
		if(!stellcount) stellcount = 0;
		var birthday1 = 0;
		var birthday2 = 0;
		var transib =  $('#transib').val();
		if(!transib) transib = 0;
		var needvisamongolia = 0;
		if( $('#visamongolia').is(':checked') ) needvisamongolia = 1; 
		var needvisachina = 0;
		if( $('#visachina').is(':checked') ) needvisachina = 1;
		$('.participants-birthdate').each(function(index, element) {
			//alert(index)
			if(index == 0) birthday1 = $(this).val(); 
			if(index == 1) birthday2 = $(this).val();
		});
		if(!birthday1) birthday1 = 0;
		if(!birthday2) birthday2 = 0;
		$.post('/index.php?option=com_viaggio&task=calcTourCost&tour_id=<?php echo $this->item->id; ?>&stellcount='+stellcount+'&insurance='+insurance+'&count='+clientsCount+'&birthday2='+birthday2+'&birthday1='+birthday1+'&transib='+transib+'&needvisamongolia='+needvisamongolia+'&needvisachina='+needvisachina, function(data) {
		  $('#cost').html(data);
		});
	}
});
</script>

<?php } ?>
