<ul id="tab-content" class="tm-tab-content uk-switcher">
    <li class="uk-active">

        <br><br>

        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1">
                <div class="uk-panel-box-secondary uk-text-center uk-text-bold uk-text-uppercase">Before paying, please read the general terms and conditions of the contract.</div>



                <div ><div class="uk-float-left">
               

   <label class="uk-form uk-margin-small-top">
                            <input  type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);">
                        </label> </div><div style="padding-top: 7px;" ><b>
                            <a data-uk-modal="{target:'#modal11'}" href=" " >&nbsp;&nbsp; I HAVE READ AND ACCEPT THE TERMS AND CONDITIONS OF SERVICE OF THE RUSSIAN TOUR INTERNATIONAL  <?php echo $text; ?>  </a></b></div></div>




                    <div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large">
                        <a href="" class="uk-modal-close uk-close"></a>
          <?php  include ('it_contrattoCard1.php');  ?>

                    </div>
                </div>

                <br>




                <div class="uk-panel uk-panel-box  uk-container-left uk-width-medium-1-2">

                    Data <?php echo $manualPayment['timeCreated']; ?> <br>

                    Name and surname  : <?php echo $manualPayment['fio']; ?> <br>
                    Phone: <?php echo $manualPayment['telefon']; ?><br>
                    Email: <?php echo $manualPayment['email']; ?><br>
                    Purpose of the payment: <?php echo $manualPayment['description']; ?><br>
                    Amount:  <?php echo $manualPayment['amountEuro']; ?>  <i class="uk-icon-euro"></i><br>

                </div>

                The payment link is active for 24 hours. If You don’t pay within 24 hours You need to ask the operator to provide You a new one.
                <br>
                <div class="uk-text-justify">
The payment system is secure, our website supports 256-bit encryption. The information You entered won’t be provided to third parties, except in the cases provided for by the legislation of the Russian Federation. Credit card transactions are conducted in strict accordance with the requirements of Visa Int. And MasterCard Europe Sprl international payment systems.
The charge is made in rubles based on the official exchange rate of the Central Bank of the Russian Federation in force at the time of payment. If the payment is canceled by the correspondent bank, Russian Tour International Ltd retains the right to cancel the services. By ticking the box "I have read and accept the terms and conditions of the Russian Tour International service" the holder of the credit card recognizes and authorizes the withdrawal from his card in accordance with the indicated conditions.
<br></div>
<button class="uk-button uk-button-primary Inviare"    onclick="location.href='<?php echo $link; ?>'" id="Inviare" disabled>PAYMENT BY CREDIT CARD</button>
            

            </div>

        </div>
    </li>