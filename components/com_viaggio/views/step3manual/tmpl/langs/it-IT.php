<?php
include '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';
$mp_user = JFactory::getUser($manualPayment['userId']);
$str = array(
    'paymentID='.$_GET['paymentID'],
    'mp_d='.$_GET['mp_d'],
    'mp_m='.$_GET['mp_m'],
    'mp_y='.$_GET['mp_y'],
    'order_id='.$_GET['order_id'],
    'amountEur='.$_GET['amountEur']
);
?>

<ul id="tab-content" class="tm-tab-content uk-switcher">
    <li class="uk-active">

        <br><br>

        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1">
                <div class="uk-panel-box-secondary uk-text-center   uk-text-bold uk-text-uppercase">  Prima di effettuare il pagamento leggere le condizioni generali di contratto.</div>



                <div ><div class="uk-float-left">
                        <label class="uk-form uk-margin-small-top">
                            <input  type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);" <?php if ($manualPayment['status'] == 1) echo 'checked="checked"'; ?>>
                        </label> </div><div style="padding-top: 7px;" ><b>
                            <a class="uk-text-uppercase" data-uk-modal="{target:'#modal11'}" href=" " >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA  <?php echo $name; ?>  </a></b></div></div>




                <div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large">
                        <a href="" class="uk-modal-close uk-close"></a>
          <?php  include ('it-IT_contrattoCart.php');  ?>

                    </div>
                </div>

                <br>



                <div class="uk-panel uk-panel-box  uk-container-left uk-width-medium-1-2">

                    Data <?php echo $manualPayment['timeCreated']; ?> <br>
					
					  Tour: <?php echo $manualPayment['trip']['name_ita'];?><br>
<?php echo (($manualPayment['order']['date_from']=='0000-00-00')?'':' Data di arrivo: '.date('d/m/Y',strtotime($manualPayment['order']['date_from'])).'<br/>') ;?>
<?php echo (($manualPayment['order']['date_to']=='0000-00-00')?'':'  Data di partenza: '.date('d/m/Y',strtotime($manualPayment['order']['date_to'])).'<br/>') ;?>
  Numero di persone:  <?php echo $manualPayment['order']['clientcount'];?><br>
                    Nome e cognome : <?php echo $manualPayment['fio']; ?> <br>
                    Telefono: <?php echo $manualPayment['telefon']; ?><br>
                    Email: <?php echo $manualPayment['email']; ?><br>
                    Causale del pagamento: <?php echo $manualPayment['description']; ?><br>
                    Importo:  <?php echo $manualPayment['amountEuro']; ?>  <i class="uk-icon-euro"></i><br>

                </div>

                Il link per il pagamento e` attivo 24 ore. Se non effettuate il pagamento entro 24 ore    dovrete richiedere all'operatore un nuovo link.
                <br>
                <div class="uk-text-justify uk-width-medium-1-1">
                    Il sistema di pagamento è sicuro, il nostro sito internet supporta la crittografia a 256-bit. Le informazioni inserite non verranno fornite a terzi, salvo nei casi previsti dalla legislazione della Federazione Russa. Le operazioni con carta di credito sono condotte in stretta conformità con i requisiti di sistemi di pagamento internazionali Visa Int. e MasterCard Europe Sprl. <br>
                    L’addebito viene effettuato in rubli in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi. Barrando la casella "Ho letto ed accetto i termini e le condizioni si servizio della Russian Tour International" l'intestatario della carta di credito riconosce ed autorizza il prelievo dalla sua carta alle condizioni indicate.<br></div>

                <button class="uk-button uk-button-primary Inviare"    onclick="location.href='<?php echo $link; ?>'" id="Inviare" disabled>PAGAMENTO CON CARTA DI CREDITO</button>
            </div>
            <div  aria-hidden="true" class="<?php if ($manualPayment['status'] != 1) echo 'uk-hidden'; ?>" id="my-id">

                <div class="uk-panel uk-panel-box  uk-container-center uk-width-medium-1-2"><div class="uk-panel-badge uk-badge uk-badge-danger">pagamento </div><h3 class="uk-panel-title">PAGAMENTO CON CARTA DI CREDITO</h3> <i class="uk-icon-university"></i>
                    <br/>
                    <span id="payTime"><?php  if($manualPayment['payButton']) echo '  Data di accettazione del contratto-offerta: '.$manualPayment['payButton']; ?> </span><br/>
                    <span> <?php echo $manualPayment['order']['id']; ?> <?php  echo   $manualPayment['clients'][0]['cognome'];  ?> <?php  echo  $manualPayment['clients'][0]['nome'];  ?> </span>  <br>
                    <b><span>Importo: <?php echo $manualPayment['amountEuro']; ?> </span> € </b>  <br>

                </div> </div>

        </div>
    </li>