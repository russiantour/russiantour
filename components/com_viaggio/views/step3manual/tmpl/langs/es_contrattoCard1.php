
<div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;"><div class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large"> <a href="/" class="uk-modal-close uk-close"></a>
<table>
<tbody>
<tr>
<td>
<p><strong>ДОГОВОР-ОФЕРТА<br /> о реализации туристского продукта №&nbsp;</strong><br /> </p>
</td>
<td>
<p><strong>CONTRATO-OFERTA</strong></p>
<p><strong>de realizaci&oacute;n del producto tur&iacute;stico N&ordm; ______</strong></p>
</td>
</tr>
<tr>
<td>
<p>Россия, Санкт-Петербург</p>
</td>
<td>
<p>Rusia, San Petersburgo</p>
</td>
</tr>
<tr>
<td>
<p>&laquo;____&raquo; __________ 2018 г.</p>
</td>
<td>
<p>&nbsp;&laquo;____&raquo; __________ del a&ntilde;o 2018</p>
</td>
</tr>
<tr>
</tr>
<tr>
<td>
<p>Общество с ограниченной ответственностью &laquo;Международная Компания &laquo;Русский Тур&raquo; (ООО &laquo;Международная Ком пания &laquo;Русский Тур&raquo;), Российская Федерация, именуемое в дальнейшем &laquo;Туроператор&raquo;, в лице Генерального директора Черемшенко Ольги Николаевны, действующей на основании Устава, с одной стороны</p>
</td>
<td>
<p>Sociedad de responsabilidad limitada "Compa&ntilde;&iacute;a internacional "Russki Tur" ("Compa&ntilde;&iacute;a internacional "Russki Tur", SRL), Federaci&oacute;n de Rusia, en lo sucesivo denominada "Operador tur&iacute;stico", representada por el Director General Olga Nikolayevna Cheremshenko, quien actua a base de los Estatutos, por una parte</p>
</td>
</tr>
<tr>
<td>
<p>и фамилию, имя, паспорт, страну, телефон, электронную почту именуемый (ая) в дальнейшем &laquo;Заказчик&raquo;, с другой стороны, вместе и по отдельности именуемые &laquo;Стороны&raquo;, заключили настоящий Договор о нижеследующем:</p>
</td>
<td>
<p>y nombre, apellido, pasaporte, pa&iacute;s, tel&eacute;fono, correo electr&oacute;nico, en lo sucesivo denominado(a) "El cliente", por otra parte, juntos e individualmente denominados "las Partes", han firmado este Contrato acerca de lo siguiente:</p>
</td>
</tr>
<tr>
<td>
<p>Настоящий Договор-оферта (далее &ndash; &laquo;Договор&raquo;, &laquo;Договор-оферта&raquo;) является письменным предложением (Офертой) Туроператора заключить Договор, направляемое Заказчику в соответствии со ст. 432-444 ГК РФ.</p>
<p>Договор заключается путем полного и безоговорочного принятия (акцепта) оферты Заказчиком в порядке, установленном п. 3 ст. 438 ГК РФ, и является подтверждением соблюдения письменной формы договора в соответствии с п. 3 ст. 434 ГК РФ.<br /> Текст настоящего Договора-оферты расположен по адресу: https://www.russiantour.com/profile?layout=pay&amp;order=id?&gt;</p>
</td>
<td>
<p>El presente Contrato-oferta (en adelante - "el Contrato", "el Contrato-oferta") es una propuesta (Oferta) escrita del Operador Tur&iacute;stico de concluir el Contrato enviada al Cliente de acuerdo con los Art. 432-444 del C&oacute;digo Civil de la Federaci&oacute;n de Rusia.</p>
<p>El Contrato se concluye mediante la aprobaci&oacute;n (aceptaci&oacute;n) total e incondicional de la oferta por parte del Cliente en el orden prescrito en el p&aacute;rrafo 3 del art. 438 del C&oacute;digo Civil de la Federaci&oacute;n de Rusia, y es una confirmaci&oacute;n del cumplimiento de la forma escrita del contrato de conformidad con el p&aacute;rrafo 3 del art. 434 del C&oacute;digo Civil de la Federaci&oacute;n de Rusia.</p>
<p>El texto del presente Contrato-propuesta se encuentra en: https: //www.russiantour.com/ita/contratto</p>
</td>
</tr>
<tr>
<td>
<p>Заказчик туристского продукта (Заказчик) - турист или иное лицо, заказывающее туристский продукт от имени туриста, в том числе законный представитель несовершеннолетнего туриста;<br /> Сайт &ndash; интернет-сайт www.russiantour.com, с помощью которого Заказчик осуществляет действия по заказу туристского продукта (действия, направленные на заключение настоящего Договора, в том числе действия по выбору услуг с помощью программных средств сайта и оформлению заявки);&nbsp;<br /> Оферта &ndash; письменное предложение Туроператора заключить Договор;<br /> Акцепт оферты - полное и безоговорочное принятие Заказчиком оферты путем осуществления действий, указанных в оферте;<br /> Туристская деятельность - туроператорская и турагентская деятельность, а также иная деятельность по организации путешествий;<br /> Туристский продукт - комплекс услуг по перевозке и размещению, оказываемых за общую цену (независимо от включения в общую цену стоимости экскурсионного обслуживания и (или) других услуг) по договору о реализации туристского продукта;<br /> Туроператорская деятельность - деятельность по формированию, продвижению и реализации туристского продукта, осуществляемая юридическим лицом (далее - Туроператор).</p>
</td>
<td>
<p><strong>Terminolog&iacute;a:</strong></p>
<p>El Cliente del producto tur&iacute;stico (Cliente): un turista u otra persona que solicita el producto tur&iacute;stico en nombre del turista, incluido el representante legal de un turista menor;</p>
<p><strong>Sitio web</strong>&nbsp;- el sitio web www.russiantour.com, con la ayuda del cual el Cliente lleva a cabo las acciones de solicitud del producto tur&iacute;stico (acciones dirigidas a la conclusi&oacute;n de este Contrato, incluidas las acciones de elecci&oacute;n de servicios con la ayuda de las herramientas de software del sitio web y la formalizaci&oacute;n de la aplicaci&oacute;n).</p>
<p>Oferta - una propuesta escrita del Operador tur&iacute;stico de concluir un Contrato;</p>
<p>Aceptaci&oacute;n de la oferta - aceptaci&oacute;n total e incondicional de la oferta por parte del Cliente a trav&eacute;s de la realizaci&oacute;n de las acciones especificadas en la oferta;</p>
<p>Actividad tur&iacute;stica - actividades de operadores tur&iacute;sticos y agencias de viajes, as&iacute; como otras actividades de organizaci&oacute;n de viajes;</p>
<p>Producto tur&iacute;stico - un conjunto de servicios de transporte y alojamiento proporcionados por el precio total (independientemente de la inclusi&oacute;n en el precio total de los servicios de excursiones y (u) otros servicios) en virtud del contrato de la realizaci&oacute;n del producto tur&iacute;stico.</p>
<p>La actividad del operador tur&iacute;stico - es una actividad que incluye la formaci&oacute;n, promoci&oacute;n y realizaci&oacute;n de un producto tur&iacute;stico, llevada a cabo por una entidad jur&iacute;dica (en adelante, el Operador tur&iacute;stico).</p>
</td>
</tr>
<tr>
<td>
<p>1.1. В соответствии с Договором Туроператор обязуется обеспечить Заказчику комплекс услуг, входящих в Туристский продукт, полный перечень которых указывается в Заявке на реализацию туристского продукта (Приложение № 1 к Договору) (далее - Туристский продукт), а Заказчик обязуется оплатить Туристский продукт на условиях настоящего Договора.<br /> 1.2. Сведения о Заказчике в объеме, необходимом для исполнения Договора, указаны в заявке на реализацию туристского продукта.<br /> 1.3. Туроператор предоставляет Заказчику следующие услуги:<br /> 1.3.1. Визовая поддержка;<br /> 1.3.2. Единые турпакеты (туры на несколько дней);<br /> 1.3.3. Приобретение авиабилетов;<br /> 1.4. Перечень услуг, указанных в п. 1.3. настоящего Договора не является исчерпывающим и может быть сторонами расширен.<br /> 1.5. Конкретный перечень услуг по Договору выбирается Заказчиком в Заявке о реализации туристского продукта (Приложение № 1 к Договору).<br /> 1.6. Настоящий Договор является публичной офертой.<br /> 1.7. Акцептом настоящего Договора-оферты со стороны Заказчика является проставление отметки &ldquo;V&rdquo; в графе &ldquo;HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL &ldquo;расположенной по адресу: https://www.russiantour.com/profile?layout=pay&amp;order=id?&gt;<br /> <strong>.&nbsp;</strong>Проставление Заказчиком отметки &ldquo;V&rdquo; в указанной графе обозначает согласие Заказчика с условиями Договора, принятие Заказчиком условий настоящего Договора.</p>
</td>
<td>
<p><strong>1. OBJETO DEL CONTRATO</strong></p>
<p>1.1. De acuerdo con el Contrato, el Operador Tur&iacute;stico se obliga a proporcionar al Cliente una gama de servicios incluidos en el Producto Tur&iacute;stico, cuya lista completa se indica en la Solicitud de realizaci&oacute;n del producto tur&iacute;stico (Anexo N&ordm; 1 del Contrato) (en adelante, el Producto Tur&iacute;stico), y el Cliente se obliga a pagar por el Producto Tur&iacute;stico en los t&eacute;rminos del presente Contrato.</p>
<p>1.2. La informaci&oacute;n sobre el Cliente en el volumen necesario para la ejecuci&oacute;n del Contrato se indica en la solicitud de realizaci&oacute;n del producto tur&iacute;stico.</p>
<p>1.3.El Operador tur&iacute;stico proporciona al Cliente los siguientes servicios:</p>
<p>1.3.1.Apoyo de visado</p>
<p>1.3.2.Paquetes tur&iacute;sticos unidos (viajes de varios d&iacute;as).</p>
<p>1.3.3.Compra de billetes de avi&oacute;n;</p>
<p>1.4.La lista de los servicios especificados en el p&aacute;rrafo 1.3. del presente Contrato no es exhaustiva y puede ser extendida por las partes.</p>
<p>1.5. La lista espec&iacute;fica de servicios seg&uacute;n el Contrato se elige por el Cliente en la Solicitud de realizaci&oacute;n del producto tur&iacute;stico (Anexo N&ordm; 1 del Contrato).</p>
<p>1.6. El presente Contrato es una oferta p&uacute;blica.</p>
<p>1.7. La aceptaci&oacute;n del presente Contrato-oferta por parte del Cliente es la inscripci&oacute;n del signo "V" en la columna "HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN INTERNATIONAL" ubicada en: https: //www.russiantour.com/ita/contratto. La inscripci&oacute;n por el Cliente del signo &ldquo;V&rdquo; en la columna indicada significa el acuerdo del Cliente con los t&eacute;rminos del Contrato, la aceptaci&oacute;n por parte del Cliente de las condiciones del presente Contrato.</p>
</td>
</tr>
<tr>
<td>
<p><strong>2. ЦЕНА ТУРИСТСКОГО ПРОДУКТА. ПОРЯДОК ОПЛАТЫ</strong></p>
<p>2.1. Общая цена Туристского продукта, приобретаемого по Договору (стоимость услуг), определяется на основании заявки на реализацию туристского продукта и указывается в инвойсе. Инвойс является неотъемлемой частью Договора.<br /> 2.2. Оплата осуществляется Заказчиком в следующем порядке:<br /> банковской картой с использованием услуги интернет-эквайринг, доступ к которой предоставляется на Сайте.<br /> 2.3. Заказчик обязан оплатить услуги Туроператора в течение 1 день с момента заключения Договора.<br /> 2.4. Расходы, связанные с использованием банковской карты, осуществлением оплаты в банке Заказчика относятся на счет Заказчика. 2.5. Стоимость услуг указывается в инвойсе в Евро.&nbsp;<br /> 2.6 Валюта платежа: российский рубль. Оплата производится в российских рублях по курсу ЦБ РФ на день платежа.<br /> 2.7. Датой оплаты Заказчиком услуг, оказываемых Туроператором по Договору, является дата зачисления денежных средств на банковский счет Туроператора.&nbsp;<br /> 2.8. Инвойс с момента его полной оплаты Заказчиком подтверждает оказание услуг Туроператором по соответствующей Заявке на реализацию туристского продукта и признается Сторонами в качестве акта выполненных работ.</p>
</td>
<td>
<p><strong>2. PRECIO DEL PRODUCTO TUR&Iacute;STICO. ORDEN DE PAGO</strong></p>
<p>2.1. El precio total del Producto tur&iacute;stico comprado seg&uacute;n el Contrato (costo de los servicios) se determina a base de la solicitud de realizaci&oacute;n del producto tur&iacute;stico y se indica en la factura. La factura es una parte integral del Contrato.</p>
<p>2.2. El pago se realiza por el Cliente en el siguiente orden:</p>
<p>a base de la factura extendida por transferencia bancaria.</p>
<p>2.3. El Cliente est&aacute; obligado a pagar por los servicios del Operador tur&iacute;stico dentro de los 14 d&iacute;as a partir del momento de la conclusi&oacute;n del Contrato, si no se solicit&oacute; el pago m&aacute;s urgente a causa de urgencia de la solicitud.</p>
<p>2.4. Las comisiones bancarias se distribuyen entre las Partes en el siguiente orden: el Cliente paga las comisiones del banco de servicio del Cliente, las comisiones de otros bancos se pagan por el Operador Tur&iacute;stico (SHA).</p>
<p>2.5. El precio de los servicios se indica en la factura en euros.</p>
<p>2.6. Moneda de pago: Euro</p>
<p>2.7. La fecha del pago por parte del Cliente por los servicios prestados por el Operador tur&iacute;stico en virtud del Contrato es la fecha de transferencia de fondos monetarios a la cuenta bancaria del Operador Tur&iacute;stico.</p>
<p>2.8. Desde el momento del pago total de la factura por parte del Cliente la factura confirma la prestaci&oacute;n de servicios por parte del Operador tur&iacute;stico seg&uacute;n la Solicitud correspondiente de realizaci&oacute;n del producto tur&iacute;stico y es reconocida por las Partes como un acto del trabajo realizado.</p>
</td>
</tr>
<tr>
</tr>
<tr>
<td>
<p>3.1. Туроператор обязан:<br /> - предоставить Заказчику достоверную информацию о потребительских свойствах Туристского продукта, а также информацию, предусмотренную Заявкой на реализацию туристского продукта;<br /> - передать Заказчику оформленные визовые документы по мере их готовности;<br /> - принять необходимые меры по обеспечению безопасности персональных данных Заказчика, в том числе при их обработке и использовании;<br /> - оказать Заказчику все услуги, входящие в Туристский продукт, самостоятельно или с привлечением третьих лиц, на которых Туроператором возлагается исполнение части или всех его обязательств перед Заказчиком и (или) туристом (в случае если Заказчик заказывает Туроператору туристский продукт от имени туриста, а также является законным представителем несовершеннолетнего туриста).<br /> 3.2. Туроператор вправе:<br /> - не приступать к оказанию услуг по Договору до полной оплаты Заказчиком заказанных услуг.<br /> 3.3. Заказчик обязан:<br /> - оплатить Туристский продукт в соответствии с Договором;<br /> - довести до туриста условия Договора, иную информацию, указанную в Договоре и приложениях к нему, а также передать ему документы, полученные от Туроператора для совершения туристом путешествия;<br /> - предоставить Туроператору свои контактные данные, а также контактные данные туриста, необходимые для оперативной связи, а также оформления Туристского продукта;<br /> - предоставить Туроператору документы и сведения, необходимые для исполнения Договора, в соответствии с требованиями, предъявленными Туроператором (необходимые требования к документам и сведениям размещены на Сайте либо доводятся до сведения Заказчика дополнительно);<br /> - проверить полученную от Туроператора визу;<br /> - приобретенный турпакет;<br /> - авиабилеты.<br /> 3.4. Заказчик вправе:<br /> - получить копию свидетельства о внесении сведений о Туроператоре в реестр;<br /> - получить оформленные визовые документы;<br /> - получить приобретенный турпакет;<br /> - получить приобретенные авиабилеты.<br /> 3.5. Стороны несут ответственность за неисполнение или ненадлежащее исполнение своих обязательств в соответствии с законодательством Российской Федерации.<br /> 3.6. Туроператор не несет ответственность:<br /> - за действия посольств (консульств) иностранных государств, иных организаций, за исключением организаций, которые привлечены Туроператором для оказания услуг, входящих в Туристский продукт, в том числе за отказ иностранного посольства (консульства) в выдаче (задержке) въездных виз туристам по маршруту путешествия, если в иностранное посольство (консульство) Туроператором либо непосредственно Заказчиком в установленные сроки были представлены все необходимые документы;<br /> - за отказ туристам в выезде/въезде при прохождении паспортного пограничного или таможенного контроля, либо применение к Заказчику органами, осуществляющими пограничный или таможенный контроль, штрафных санкций по причинам, не связанным с выполнением Туроператором своих обязательств по Договору;<br /> - за отмену или перенос авиарейсов;<br /> - изменение расписания движения железнодорожного транспорта, автобусного транспорта, в том числе отмену поездов/автобусов, изменение маршрута движения, изменение времени отправления или прибытия;<br /> - за отмену или изменение времени начала (окончания) услуг, включенных в турпакет.<br /> 3.7. Заказчик вправе предъявить претензии к полученным в результате оказания Туроператором визовым документам, авиабилетам, турпакету в день их получения. По истечении указанного срока претензии к указанным в настоящем пункте Договора документам Туроператором не принимаются.<br /> 3.8. Заключением настоящего Договора Заказчик подтверждает, что ознакомлен со всеми Правилами, размещенным на Сайте https://www.russiantour.com/profile?layout=pay&amp;order=id?&gt;</p>
</td>
<td>
<p><strong>3. INTERACCI&Oacute;N DE LAS PARTES</strong></p>
<p></p>
<p>3.1. El operador tur&iacute;stico se obliga a:</p>
<p>- proporcionar al Cliente informaci&oacute;n confiable sobre las propiedades al consumidor del Producto tur&iacute;stico, as&iacute; como informaci&oacute;n prevista por la Solicitud de realizaci&oacute;n del producto tur&iacute;stico;</p>
<p>- entregar al Cliente los documentos formalizados de visado tan pronto como est&eacute;n listos;</p>
<p>- tomar las medidas necesarias para garantizar la seguridad de los datos personales del Cliente, incluso durante su procesamiento y uso;</p>
<p>- prestar al Cliente todos los servicios incluidos en el Producto tur&iacute;stico, de forma independiente o con la participaci&oacute;n de terceros, encargados por el Operador tur&iacute;stico para cumplir alguna parte de obligaciones o todas sus obligaciones ante el Cliente y (o) el turista (si el Cliente solicita el Producto tur&iacute;stico en nombre del turista, as&iacute; como es representante legal de un turista menor).</p>
<p>3.2. El operador tur&iacute;stico tiene derecho de:</p>
<p>- no proceder a la prestaci&oacute;n de servicios en virtud del Contrato hasta que el Cliente realice el pago completo de los servicios solicitados.</p>
<p>3.3. El Cliente se obliga a:</p>
<p>- pagar por el Producto tur&iacute;stico de acuerdo con el Contrato;</p>
<p>- informar al turista acerca de las condiciones del Contrato, de otra informaci&oacute;n especificada en el Contrato y sus anexos, as&iacute; como entregarle los documentos recibidos del Operador tur&iacute;stico necesarios para realizaci&oacute;n del viaje por el turista;</p>
<p>- proporcionar al Operador tur&iacute;stico sus datos de contacto, as&iacute; como los datos de contacto del turista, necesarios para la comunicaci&oacute;n r&aacute;pida, as&iacute; como para la formalizaci&oacute;n del Producto tur&iacute;stico.</p>
<p>- proporcionar al Operador tur&iacute;stico los documentos e informaci&oacute;n necesarios para la ejecuci&oacute;n del Contrato, de acuerdo con los requisitos presentados por el Operador tur&iacute;stico (los requisitos necesarios para los documentos y la informaci&oacute;n se publican en el Sitio web o se comunican al Cliente adicionalmente);</p>
<p>- verificar el visado recibido del Operador tur&iacute;stico;</p>
<p>- paquete tur&iacute;stico comprado;</p>
<p>- billetes de avi&oacute;n.</p>
<p>3.4. El Cliente tiene derecho de:</p>
<p>- obtener una copia del certificado de inscripci&oacute;n de informaci&oacute;n sobre el Ooperador tur&iacute;stico en el registro;</p>
<p>- obtener los documentos del visado formalizados;</p>
<p>- obtener el paquete tur&iacute;stico comprado;</p>
<p>- obtener los billetes de avi&oacute;n comprados.</p>
<p>3.5. Las partes son responsables de incumplimiento o cumplimiento incorrecto de sus obligaciones de conformidad con la legislaci&oacute;n de la Federaci&oacute;n de Rusia.</p>
<p>3.6. El Operador tur&iacute;stico no es responsable de:</p>
<p>- las acciones de las embajadas (consulados) de estados extranjeros, otras organizaciones, excepto las organizaciones atra&iacute;das por el Operador tur&iacute;stico para proporcionar servicios incluidos en el Producto tur&iacute;stico, incluso la denegaci&oacute;n de la embajada extranjera (consulado) a emitir (retrasar) visados de entrada a turistas seg&uacute;n el itinerario del viaje, si el operador tur&iacute;stico o el cliente mismo present&oacute; todos los documentos necesarios a tiempo a la embajada extranjera (consulado);</p>
<p>- la denegaci&oacute;n a los turistas de salir / ingresar al pasar por el control de pasaportes o por la aduana, o aplicaci&oacute;n de las sanciones al cliente por los organismos que llevan a cabo el control de la frontera o de la aduana por motivos no relacionados con el cumplimiento de las obligaciones por el Operador tur&iacute;stico en virtud del Contrato;</p>
<p>- cancelaci&oacute;n o retraso de vuelos;</p>
<p>- cambio del horario del transporte ferroviario, autobuses, incluso la cancelaci&oacute;n de trenes / autobuses, cambio de la ruta, cambio de la hora de salida o llegada;</p>
<p>- cancelaci&oacute;n o cambio de la hora de inicio (fin) de los servicios incluidos en el paquete tur&iacute;stico</p>
<p>3.7. El cliente tiene el derecho de reclamar los documentos de visado, los billetes a&eacute;reos y el paquete tur&iacute;stico obtenidos como resultado de los servicios del Operador tur&iacute;stico, el d&iacute;a en que se reciben. Una vez finalizado el per&iacute;odo especificado, las reclamaciones a los documentos especificados en esta cl&aacute;usula del Contrato no ser&aacute;n aceptadas por el Operador tur&iacute;stico.</p>
<p>3.8. Al concluir este Acuerdo, el Cliente confirma que est&aacute; familiarizado con todas las Reglas publicadas en el sitio web www.russiantour.com</p>
</td>
</tr>
<tr>
<td>
<p><strong>4. ПРЕТЕНЗИИ. ПОРЯДОК РАЗРЕШЕНИЯ СПОРОВ</strong></p>
<p>4.1. Претензии в связи с нарушением условий Договора предъявляются Заказчиком Туроператору в порядке и на условиях, которые предусмотрены законодательством Российской Федерации.<br /> 4.2. Претензии к качеству Туристского продукта предъявляются Туроператору в письменной форме в течение 20 дней с даты окончания действия Договора и подлежат рассмотрению в течение 10 дней с даты получения претензий.<br /> 4.3. Споры, связанные с настоящим Договором, подлежат рассмотрению в суде в соответствии с положениями действующего законодательства РФ.</p>
</td>
<td>
<p><strong>4.&nbsp;</strong><strong>RECLAMACIONES</strong><strong>.&nbsp;</strong><strong>SOLUCION</strong>&nbsp;<strong>DE</strong>&nbsp;<strong>CONTROVERSIAS</strong></p>
<p>4.1. Las reclamaciones relacionadas con la violaci&oacute;n de los t&eacute;rminos del Contrato se presentan por el Cliente al Operador tur&iacute;stico en el orden y seg&uacute;n las condiciones previstas por la legislaci&oacute;n de la Federaci&oacute;n Rusa.</p>
<p>4.2. Las reclamaciones a la calidad del Producto tur&iacute;stico se presentan al Operador tur&iacute;stico por escrito dentro de los 20 d&iacute;as a partir de la fecha de terminaci&oacute;n del Contrato y est&aacute;n sujetas a revisi&oacute;n dentro de los 10 d&iacute;as a partir de la fecha de recepci&oacute;n de las reclamaciones.</p>
<p>4.3. Las disputas relacionadas con el presente Contrato se considerar&aacute;n en el tribunal de conformidad con las disposiciones de la legislaci&oacute;n vigente de la Federaci&oacute;n Rusa.</p>
</td>
</tr>
<tr>
<td>
<p><strong>5. ОТВЕТСТВЕННОСТЬ</strong></p>
<p>5.1. Туроператор не несет ответственность за следующие обстоятельства:<br /> - загранпаспорт Заказчика (туриста) не принимается консульским учреждением для оформления визы по причине его повреждения, изношенности, отсутствия необходимых печатей; а также по причине того, что до истечения срока действия загранпаспорта осталось менее 6 месяцев;<br /> - Заказчик предоставил неверные, недостоверные, ошибочные, неполные данные при заполнении Заявки на реализацию туристского продукта;<br /> - отказ в оформлении и выдаче визы по усмотрению консульского учреждения по причинам, не зависящим от Туроператора;<br /> - отказ в оформлении и выдаче визы по причине неправильного заполнения Заказчиком электронной анкеты, наличия ошибок в представленных Заказчиком документах, предоставлении неполного комплекта документов и др.;<br /> - задержка в выдаче визы по причине внепланового закрытия консульского учреждения для третьих лиц, в том числе представителей туристских, туроператорских компаний; внезапного изменения графика работы визового отдела консульского учреждения;<br /> - утрата документов по вине почтовых курьеров (служб); осуществление доставки по неверному адресу; задержка доставки по вине почтовых курьеров (служб);<br /> - выдача визы с ошибочной датой въезда, или датой, не совпадающей с датой, определенной консульским учреждением;<br /> - при прохождении пограничного контроля у Заказчика (туриста) возникли сложности с сотрудниками пограничной службы, в результате чего Заказчику (туристу) отказано во въезде на территорию Российской Федерации;<br /> - за невыполнение Заказчиком рекомендаций Туроператора, указанных в п. 7.7. настоящего Договора;<br /> - невозможность Заказчиком (туристом) воспользоваться оказанными Туроператором на настоящему Договору услугами в том случае, если такая невозможность возникла по обстоятельствам, за которые Туроператор не отвечает.<br /> 5.2. В случае отказа в выдаче визы, произошедшего по вине Туроператора, последний обязан возместить в полном объеме затраты Заказчика на услуги Туроператора по оказанию визовой поддержки в рамках настоящего Договора.<br /> 5.3. Стороны освобождаются от ответственности за частичное или полное невыполнение обязательств по Договору, если это неисполнение является следствием наступления обстоятельств непреодолимой силы, то есть возникших в результате чрезвычайных и непредотвратимых при данных условиях обстоятельств, которые Стороны не могли ни предвидеть, ни предотвратить разумными мерами.<br /> Если данные обстоятельства будут продолжаться более 14 (четырнадцати) календарных дней, каждая из Сторон вправе отказаться от исполнения обязательств по Договору, и в этом случае ни одна из Сторон не будет иметь права на возмещение другой Стороной возможных убытков по основаниям непреодолимой силы.</p>
</td>
<td>
<p><strong>5. RESPONSABILIDAD</strong></p>
<p>5.1. El operador tur&iacute;stico no es responsable de las siguientes circunstancias:</p>
<p>- el pasaporte para el extranjero del Cliente (turista) no se acepta por la oficina consular para emitir el visado a causa de su da&ntilde;o, deterioro, falta de sellos necesarios; y tambi&eacute;n a causa de que el plazo de vigencia del pasaporte se termina en menos de 6 meses;</p>
<p>- El cliente proporcion&oacute; datos incorrectos, inexactos, err&oacute;neos e incompletos al completar la Solicitud de realizaci&oacute;n del Producto tur&iacute;stico;</p>
<p>- denegaci&oacute;n de formalizaci&oacute;n y emisi&oacute;n del visado a discreci&oacute;n de la oficina consular por razones ajenas a la voluntad del Operador tur&iacute;stico;</p>
<p>- denegaci&oacute;n de formalizaci&oacute;n y emisi&oacute;n del visado a causa de llenado incorrecto de la solicitud electr&oacute;nica por parte del Cliente, errores en los documentos presentados por el Cliente, suministro de un conjunto incompleto de documentos, etc.;</p>
<p>- la demora en la emisi&oacute;n del visado a causa del cierre no planificado de la oficina consular para terceros, incluso representantes de empresas de turismo y operadores tur&iacute;sticos; cambios repentinos en el horario de trabajo del departamento de visados de la oficina consular;</p>
<p>- p&eacute;rdida de documentos por culpa de correos postales (servicios postales); entrega a la direcci&oacute;n incorrecta; retraso en la entrega debido a la culpa de los correos postales (servicios);</p>
<p>&nbsp;- emisi&oacute;n del visado con una fecha de ingreso err&oacute;nea o una fecha que no coincida con la fecha determinada por la oficina consular;</p>
<p>- al pasar el control fronterizo, el Cliente (turista) tuvo dificultades con el personal del Servicio Fronterizo, a causa de lo cual se le neg&oacute; la entrada al territorio de la Federaci&oacute;n Rusa al Cliente (turista);</p>
<p>- por incumplimiento por parte del Cliente de las recomendaciones del Operador tur&iacute;stico indicadas en el p&aacute;rrafo 7.7. del presente Contrato;</p>
<p>- imposibilidad por parte del Cliente (turista) de utilizar los servicios proporcionados por el Operador tur&iacute;stico seg&uacute;n el presente Contrato en el caso cuando tal imposibilidad surgi&oacute; a causa de las circunstancias de las cuales el Operador tur&iacute;stico no es responsable;</p>
<p>5.2. En el caso de denegaci&oacute;n a emitir el visado causada por el Operador tur&iacute;stico, este &uacute;ltimo est&aacute; obligado a reembolsar al Cliente el volumen total de los costos de los servicios del Operador tur&iacute;stico del apoyo de visado en virtud del presente Contrato.</p>
<p>5.3. Las Partes se liberan de responsabilidad del incumplimiento parcial o completo de las obligaciones seg&uacute;n el Contrato si este es consecuencia de una fuerza mayor, es decir, es resultado de circunstancias de emergencia inevitables en estas condiciones que las partes no pudieron prever y no pudieron evitar mediante medidas razonables.</p>
<p>Si estas circunstancias durar&aacute;n m&aacute;s de 14 (catorce) d&iacute;as calendarios, cada una de las Partes tendr&aacute; derecho a negarse a cumplir las obligaciones seg&uacute;n el Contrato, en este caso ninguna de las Partes tendr&aacute; derecho a una indemnizaci&oacute;n de posibles p&eacute;rdidas por la otra Parte por motivos de fuerza mayor.</p>
</td>
</tr>
<tr>
<td>
<p><strong>6. СРОК ДЕЙСТВИЯ ДОГОВОРА, ПОРЯДОК ИЗМЕНЕНИЯ И РАСТОРЖЕНИЯ</strong><strong>ДОГОВОРА</strong></p>
<p>6.1. Договор вступает в силу с даты акцепта Заказчиком его условий в соответствии с п. 1.7. Договора и действует до исполнения сторонами своих обязательств, но не позднее 31 декабря 2019 г.<br /> 6.2. Договор может быть изменен или расторгнут в случаях и порядке, предусмотренном законодательством Российской Федерации, в том числе по соглашению Сторон, оформленному в письменной форме.<br /> Любые изменения в Туристский продукт, иные условия Заявки на реализацию туристского продукта допускаются только по письменному дополнительному соглашению Сторон. Письменная форма изменений в Заявку на реализацию туристского продукта считается также соблюденной при согласовании таких изменений Сторонами по электронной почте. Для этих целей со стороны Туроператора используется следующей адрес электронной почты:&nbsp;<a href="mailto:info@russiantour.com">info@russiantour.com</a><br /> Со стороны Заказчика используется следующей адрес электронной почты&nbsp;</p>
</td>
<td>
<p><strong>6. PLAZO DE VIGENCIA DEL CONTRATO, PROCEDIMIENTO PARA CAMBIOS Y CANCELACI&Oacute;N DEL CONTRATO</strong></p>
<p>6.1. El Contrato entra en vigencia a partir de la fecha de aceptaci&oacute;n por parte del Cliente de sus t&eacute;rminos de acuerdo con la cl&aacute;usula 1.7. del Contrato y es v&aacute;lido hasta que las partes cumplan sus obligaciones, pero a m&aacute;s tardar el 31 de diciembre de 2018.</p>
<p>6.2. El Contrato puede ser modificado o cancelado en los casos y en el orden prescrito por la legislaci&oacute;n de la Federaci&oacute;n Rusa, incluso por acuerdo de las Partes, ejecutado por escrito.</p>
<p>Cualquier cambio del Producto tur&iacute;stico, otras condiciones de la Solicitud de realizaci&oacute;n del producto tur&iacute;stico se permiten solo mediante un acuerdo adicional por escrito de las Partes. La forma escrita de los cambios en la Solicitud de realizaci&oacute;n del producto tur&iacute;stico tambi&eacute;n se considera cumplida cuando las Partes acuerdan los cambios por correo electr&oacute;nico. Para estos fines, el Operador tur&iacute;stico utiliza la siguiente direcci&oacute;n de correo electr&oacute;nico: viaggio@russiantour.com</p>
<p>Por la parte del Cliente, se utiliza la siguiente direcci&oacute;n de correo electr&oacute;nico _______________.</p>
</td>
</tr>
<tr>
<td>
<p><strong>7. ЗАКЛЮЧИТЕЛЬНЫЕ ПОЛОЖЕНИЯ </strong></p>
<p>7.1. Сведения о заключении в пользу туристов договора добровольного страхования, условиями которого предусмотрена обязанность страховщика осуществить оплату и (или) возместить расходы на оплату медицинской помощи в экстренной и неотложной формах, оказанной туристу на территории страны временного пребывания при наступлении страхового случая в связи с получением травмы, отравлением, внезапным острым заболеванием или обострением хронического заболевания, включая медицинскую эвакуацию туриста в стране временного пребывания и из страны временного пребывания в страну постоянного проживания, и (или) возвращения тела (останков) туриста из страны временного пребывания в страну постоянного проживания в соответствии с требованиями законодательства Российской Федерации и страны временного пребывания указаны в Заявке на реализацию туристского продукта.<br /> 7.2. Заказчик предоставляет согласие, а также подтверждает, что в соответствии с требованиями Федерального закона РФ № 152-ФЗ от 27.07.2006 г. &laquo;О персональных данных&raquo; им получено согласие от всех туристов, указанных в Приложении № 1 к Договору, на обработку и передачу своих персональных данных и персональных данных лиц, указываемых в Заявке на реализацию туристского продукта, Туроператору и третьим лицам для исполнения Договора (в том числе для оформления виз, приобретения авиабилетов, турпакета).<br /> 7.3. В случае, если Заказчик осуществляет заказ Туристского продукта в интересах туриста, Заказчик подтверждает, что обладает необходимыми полномочиями для представления интересов туриста в отношениях с Туроператором.<br /> 7.4. Все приложения, а также изменения (дополнения) к Договору являются его неотъемлемой частью.<br /> 7.5. Во всем ином, что не урегулировано Договором, Стороны руководствуются правом Российской Федерации.<br /> 7.6. При обращении в Генеральное консульство Российской Федерации в Милане по вопросу получения визы в случае, если желаемый срок пребывания в стране превышает 14 дней, как правило, требуется предоставить дополнительные документы: документы, подтверждающие бронирование отелей, счета из отелей или&nbsp;систем бронирования, квитанции о 100% оплате услуг. Если Заказчиком не забронировано место пребывания заранее на весь срок пребывания в стране назначения и не собран самостоятельно весь пакет документов, не рекомендуется подавать документы в визовый отдел Генерального консульства Российской Федерации в Милане. Если Заказчиком будет принято решение о подаче заявления на выдачу визы в описываемой выше ситуации в отсутствие указанных документов, Туроператор не несет ответственности за возможные неблагоприятные последствия для Заказчика, в том числе за возможный отказ в выдаче визы. В данном случае услуги по Договору будут считаться выполненными Туроператором в полном объеме. Обязанности по выплате каких-либо компенсаций в пользу Заказчика не возникает, равно как не возникает у Туроператора обязанности повторной подачи документов в это же консульское учреждение либо любое иное.<br /> 7.7. Для несовершеннолетних, планирующих поездку в Российскую Федерацию, без сопровождения обоих родителей&nbsp;дополнительно необходимы следующие документы: свидетельство о рождении с данными о родителях, свидетельство о семейном положении, копия документа, удостоверяющего личность родителя или родителей, заявление о согласии родителя (родителей) несовершеннолетнего на посещение Российской Федерации без его (их) сопровождения.<br /> 7.8. В случае, если невозможно получить визу в стандартные сроки из-за итальянских или российских праздничных дней либо в случае, если географическое расположение Заказчика не позволяет доставить документы почтовым курьером (службой) до указанной в Заявке даты вылета, произведенная оплата подлежит возврату на банковскую карту/банковский счет Заказчика не позднее трех дней с даты оплаты.&nbsp;Заказчик вправе, изменив дату получения визы, заключить новый Договор на новых условиях. Заказчик также вправе воспользоваться возможностью срочного или срочного в течение дня оформления визы.<br /> 7.9. Заключением настоящего Договора Заказчик подтверждает, что он полностью ознакомлен Туроператором со всеми условиями и требованиями консульских учреждений, правилами въезда на территорию Российской Федерации, нахождения и выезда с ее территории, и они ему понятны. Риск совершения тех или действий, выбор того или иного варианта оказания услуги возлагается на Заказчика.<br /> 7.10. Со стороны Туроператора для исполнения настоящего Договора, в том числе для направления какой-либо информации в рамках Договора, используется исключительно адрес электронной почты:&nbsp;<a href="mailto:info@russiantour.com">info@russiantour.com</a><br /> 7.11. Со стороны Заказчика для исполнения настоящего Договора, в том числе для направления какой-либо информации, получения информации в рамках Договора, используется исключительно адрес электронной почты: , указанный Заказчиком при оформлении онлайн-запроса на Сайте.</p>
</td>
<td>
<p><strong>7.&nbsp;</strong><strong>DISPOSICIONES</strong>&nbsp;<strong>FINALES</strong></p>
<p>7.1. La informaci&oacute;n sobre la conclusi&oacute;n de un contrato de seguro voluntario para el beneficio de los turistas, cuyos t&eacute;rminos estipulan la obligaci&oacute;n del asegurador de pagar y (o) reembolsar los gastos de atenci&oacute;n m&eacute;dica de emergencia y urgente proporcionada al turista en el territorio del pa&iacute;s de residencia temporal en el caso de un evento de seguro relacionado con una lesi&oacute;n, intoxicaci&oacute;n, enfermedad aguda repentina o exacerbaci&oacute;n de una enfermedad cr&oacute;nica, incluida la evacuaci&oacute;n m&eacute;dica de un turista en el pa&iacute;s de residencia temporal y del pa&iacute;s de residencia temporal al pa&iacute;s de residencia, y (o) el retorno del cuerpo (restos) del turista del pa&iacute;s de residencia temporal al pa&iacute;s de residencia de conformidad con los requisitos de la legislaci&oacute;n de la Federaci&oacute;n Rusa y del pa&iacute;s de la estancia temporal se indica en la Solicitud de realizaci&oacute;n del producto tur&iacute;stico.</p>
<p>7.2. El cliente acepta y tambi&eacute;n confirma que, de conformidad con los requisitos de la Ley Federal de la Federaci&oacute;n Rusa No. 152-FZ, de la fecha de 27 de julio de 2006, "Sobre los datos personales", recibi&oacute; el consentimiento de todos los turistas que figuran en el Anexo No. 1 del Contrato para procesar y transferir sus datos personales y los datos personales de las personas indicadas en la Solicitud de realizaci&oacute;n del producto tur&iacute;stico, al Operador tur&iacute;stico y terceros para la ejecuci&oacute;n del Contrato (incluso para la emisi&oacute;n de visados, la compra de billetes a&eacute;reos, paquete tur&iacute;stico).</p>
<p>7.3. Si el Cliente hace un pedido del producto tur&iacute;stico en inter&eacute;s del turista, el Cliente confirma que tiene la autoridad necesaria para representar los intereses del turista en relaciones con el Operador tur&iacute;stico.</p>
<p>7.4. Todos los anexos, as&iacute; como las enmiendas (adiciones) al Contrato son su parte integral.</p>
<p>7.5. En todo lo que no est&aacute; regulado por el Contrato, las Partes se gu&iacute;an por la ley de la Federaci&oacute;n Rusa.</p>
<p>7.6. En el caso de solicitud al Consulado General de la Federaci&oacute;n Rusa en Mil&aacute;n acerca de la obtenci&oacute;n del visado para el per&iacute;odo de estad&iacute;a deseado en el pa&iacute;s que supera los 14 d&iacute;as, normalmente se requieren documentos adicionales: documentos que confirman las reservas de hotel, facturas de hoteles o sistemas de reserva, recibos de 100% pago por servicios. Si el Cliente no ha reservado por adelantado el lugar de estad&iacute;a para todo el per&iacute;odo de estad&iacute;a en el pa&iacute;s de destino y no ha preparado por si mismo el paquete completo de documentos, no se recomienda enviar los documentos al departamento de visados del Consulado General de la Federaci&oacute;n Rusa en Mil&aacute;n. Si el Cliente decide solicitar un visado en la situaci&oacute;n descrita anteriormente y no tiene estos documentos, el Operador tur&iacute;stico no es responsable de las posibles consecuencias adversas para el Cliente, incluida la posible denegaci&oacute;n a emitir el visado. En este caso, los servicios seg&uacute;n el Contrato se considerar&aacute;n prestados por el Operador tur&iacute;stico en su totalidad. No surge la obligaci&oacute;n de pagar cualquier compensaci&oacute;n a favor del Cliente, y el Operador tur&iacute;stico tampoco tiene la obligaci&oacute;n de presentar de nuevo los documentos a la misma oficina consular o a cualquiera otra.</p>
<p>7.7. Para los menores de edad que planean el viaje a la Federaci&oacute;n Rusa sin ambos padres, tambi&eacute;n son necesarios los siguientes documentos: un certificado de nacimiento con los datos de los padres, un certificado de estado civil, una copia del documento de identidad del padre o de los padres, una declaraci&oacute;n del consentimiento del padre (de los padres) del menor para viajar a la Federaci&oacute;n de Rusia sin su acompa&ntilde;amiento.</p>
<p>7.8. Si es imposible obtener el visado en plazo est&aacute;ndar debido a vacaciones en Italia o Rusia o si la ubicaci&oacute;n geogr&aacute;fica del Cliente no permite realizar la entrega de documentos por correo postal (servicio postal) antes de la fecha de salida indicada en la Solicitud, el pago realizado debe ser devuelto a la tarjeta bancaria/la cuenta bancaria del Cliente a m&aacute;s tardar tres d&iacute;as desde la fecha del pago. El Cliente tiene el derecho, al cambiar la fecha de recepci&oacute;n del visado, a celebrar un nuevo Contrato en nuevos t&eacute;rminos. El Cliente tambi&eacute;n tiene el derecho de utilizar la posibilidad de obtenci&oacute;n del visado urgente o urgente durante un d&iacute;a.</p>
<p>7.9. Al concluir este Contrato, el Cliente confirma que est&aacute; completamente familiarizado por el Operador tur&iacute;stico con todas las condiciones y requisitos de las instituciones consulares, las reglas de ingreso a la Federaci&oacute;n Rusa, estanc&iacute;a en su territorio y salida, y son comprensibles para &eacute;l. El riesgo de cometer tales o cuales acciones, la elecci&oacute;n de una u otra opci&oacute;n de servicio recae en el Cliente.</p>
<p>7.10. Por parte del Operador tur&iacute;stico, para la ejecuci&oacute;n del presente Contrato, incluso para enviar toda la informaci&oacute;n dentro del marco del Contrato, se utiliza exclusivamente esta direcci&oacute;n de correo electr&oacute;nico: viaggio@russiantour.com</p>
<p>7.11. Por parte del Cliente para la ejecuci&oacute;n del presente Contrato, incluso para enviar toda la informaci&oacute;n dentro del marco del Contrato, se utiliza exclusivamente esta direcci&oacute;n de correo electr&oacute;nico: __________________, especificada por el Cliente al realizar una solicitud en l&iacute;nea en el Sitio web.</p>
</td>
</tr>
<tr>
<td>
<p><strong>8. АДРЕСА И РЕКВИЗИТЫ СТОРОН. ИНФОРМАЦИЯ О ТУРОПЕРАТОРЕ И О ЛИЦЕ, ПРЕДОСТАВИВШЕМ ТУРОПЕРАТОРУ ФИНАНСОВОЕ ОБЕСПЕЧЕНИЕ ОТВЕТСТВЕННОСТИ</strong></p>
<p>1. Сведения о Туроператоре:</p>
<table>
<tbody>
<tr>
<td>
<p>Полное наименование</p>
</td>
<td>
<p>Общество с ограниченной ответственностью &laquo;Международная Компания &laquo;Русский Тур&raquo;</p>
</td>
</tr>
<tr>
<td>
<p>Сокращенное наименование</p>
</td>
<td>
<p><strong>ООО "\Международная Компания &laquo;Русский Тур&raquo;</strong></p>
</td>
</tr>
<tr>
<td>
<p>Адрес (место нахождения)</p>
</td>
<td>
<p>194044, Санкт-Петербург, Финляндский проспект, дом 4, лит. А, офис 424</p>
</td>
</tr>
<tr>
<td>
<p>Почтовый адрес</p>
</td>
<td>
<p>194044, Санкт-Петербург, Финляндский проспект, дом 4, лит. А, офис 717</p>
</td>
</tr>
<tr>
<td>
<p>Реестровый номер</p>
</td>
<td>
<p>MBT 012877</p>
</td>
</tr>
<tr>
<td>
<p>Телефон/факс</p>
</td>
<td>
<p>+7 (812) 6470690</p>
</td>
</tr>
<tr>
<td>
<p>Электронная почта/Сайт</p>
</td>
<td>
<p>infog<a href="mailto:visto@russiantour.com">@russiantour.com</a></p>
<p><a href="http://www.visto-russia.com">www.</a>russiantour.com</p>
</td>
</tr>
</tbody>
</table>
<p>2. Сведения об организации, предоставившей Туроператору финансовое</p>
<p>обеспечение ответственности Туроператора:</p>
<table>
<tbody>
<tr>
<td>
<p>Вид финансового обеспечения ответственности туроператора</p>
</td>
<td>
<p>Страхования гражданской ответственности туроператора - внутренний туризм<br /> международный въездной</p>
</td>
</tr>
<tr>
<td>
<p>Размер финансового обеспечения</p>
</td>
<td>
<p>500000 руб.</p>
</td>
</tr>
<tr>
<td>
<p>Дата и срок действия договора страхования ответственности туроператора или банковской гарантии</p>
</td>
<td>
<p>с 12/05/2017 по 11/05/2018</p>
</td>
</tr>
<tr>
<td>
<p>Наименование организации, предоставившей финансовое обеспечение ответственности туроператора</p>
</td>
<td>
<p>АО "Либерти Страхование"</p>
</td>
</tr>
<tr>
<td>
<p>Адрес (местонахождение)</p>
</td>
<td>
<p>196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А</p>
</td>
</tr>
<tr>
<td>
<p>Почтовый адрес</p>
</td>
<td>
<p>196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А</p>
</td>
</tr>
<tr>
<td>
<p>Сайт</p>
</td>
<td>
<p><a href="http://www.liberty24.ru">www.liberty24.ru</a></p>
</td>
</tr>
<tr>
<td>
<p>Электронная почта</p>
</td>
<td>
<p><a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a></p>
</td>
</tr>
</tbody>
</table>
</td>
<td>
<p><strong>8. DIRECCIONES Y REQUISITOS DE LAS PARTES. INFORMACI&Oacute;N SOBRE EL OPERADOR TUR&Iacute;STICO Y LA PERSONA QUE PROPORCION&Oacute; AL OPERADOR TUR&Iacute;STICO LA COBERTURA FINANCIERA DE LA RESPONSABILIDAD</strong></p>
<p>1. Informaci&oacute;n sobre el Operador tur&iacute;stico:</p>
<table>
<tbody>
<tr>
<td>
<p>Denominaci&oacute;n completa</p>
</td>
<td>
<p>Sociedad de responsabilidad limitada &laquo;Compa&ntilde;&iacute;a internacional &laquo;Russki Tur&raquo;</p>
</td>
</tr>
<tr>
<td>
<p>Denominaci&oacute;n corta</p>
</td>
<td>
<p>&nbsp;<strong>&laquo;Compa&ntilde;&iacute;a internacional &laquo;Russki Tur&raquo;, SRL</strong></p>
</td>
</tr>
<tr>
<td>
<p>Direcci&oacute;n (lugar de ubicaci&oacute;n)</p>
</td>
<td>
<p>194044, San Petersburgo, Avenida Finlyandski, 4, letra А, oficina 424</p>
</td>
</tr>
<tr>
<td>
<p>Direcci&oacute;n postal</p>
</td>
<td>
<p>194044, San Petersburgo, Avenida Finlyandski, 4, letra A, oficina 717</p>
</td>
</tr>
<tr>
<td>
<p>N&uacute;mero de registro</p>
</td>
<td>
<p>MBT 012877</p>
</td>
</tr>
<tr>
<td>
<p>Tel&eacute;fono/fax</p>
</td>
<td>
<p>+7 (812) 6470690</p>
</td>
</tr>
<tr>
<td>
<p>Correo electr&oacute;nico/Sitio web</p>
</td>
<td>
<p>info<a href="mailto:viaggio@russiantour.com">@russiantour.com</a></p>
<p><a href="http://www.russiantour.com/">www.</a><a href="mailto:viaggio@russiantour.com">russiantour.com</a></p>
</td>
</tr>
</tbody>
</table>
<p>2. Informaci&oacute;n sobre la organizaci&oacute;n que proporcion&oacute; al Operador tur&iacute;stico la cobertura financiera de la responsabilidad:</p>
<table>
<tbody>
<tr>
<td>
<table>
<tbody>
<tr>
<td>
<p>Tipo de cobertura financiera de la responsabilidad del operador tur&iacute;stico</p>
</td>
<td>
<p>Seguro de responsabilidad civil del operador tur&iacute;stico - turismo nacional</p>
<p>turismo de ingreso internacional</p>
</td>
</tr>
<tr>
<td>
<p>Volumen de cobertura financiera</p>
</td>
<td>
<p>500000 RUB.</p>
</td>
</tr>
<tr>
<td>
<p>Fecha y plazo de validez del contrato de seguro de responsabilidad del operador tur&iacute;stico o garant&iacute;a bancaria</p>
</td>
<td>
<p>De 12/05/2017 hasta 11/05/2020</p>
</td>
</tr>
<tr>
<td>
<p>Nombre de la organizaci&oacute;n que proporcion&oacute; cobertura financiera de la responsabilidad del operador tur&iacute;stico.</p>
</td>
<td>
<p>SA "Liberti Strakhovanie"</p>
</td>
</tr>
<tr>
<td>
<p>Direcci&oacute;n (lugar de ubicaci&oacute;n)</p>
</td>
<td>
<p>196044, ciudad de San Petersburgo, Avenida Moskovski, 79А, letra А</p>
</td>
</tr>
<tr>
<td>
<p>Direcci&oacute;n postal</p>
</td>
<td>
<p>196044, ciudad de San Petersburgo, Avenida Moskovski, 79А, letra А</p>
</td>
</tr>
<tr>
<td>
<p>Sitio web</p>
</td>
<td>
<p><a href="http://www.liberty24.ru/">www.liberty24.ru</a></p>
</td>
</tr>
<tr>
<td>
<p>Correo electr&oacute;nico</p>
</td>
<td>
<p><a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a></p>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
<td>
<p><strong>Туроператор</strong></p>
ООО "Международная Компания &laquo;Русский Тур&raquo;<br /> Юридический адрес: 194044, Санкт-Петербург,<br /> Финляндский проспект, дом 4, лит. А, офис 424<br /> ИНН 7802853888, КПП 780201001<br /> ОГРН 1147847089532, ОКПО 35460198<br /> Банк Филиал &laquo;Корпоративный&raquo; ПАО &laquo;Совкомбанк&raquo;<br /> р/сч 40702978200190742076<br /> SWIFT: SOMRRUMM<br /> Адрес банка: 119991,Россия, г. Москва,<br /> ул. Вавилова, д. 24<br /> Банк корреспондент:COMMERZBANK AG,<br /> Frankfurt am Main, Germany<br /> SWIFT: COBADEFFXXX
<p><br /> <strong>Генеральный директор </strong></p>
<p>Черемшенко Ольга Николаевна</p>
<p>_____________________________</p>
<p><strong>Телефон: </strong><br /> +7 (812) 6470690</p>
<p><strong>Электронная</strong><strong>почта:</strong></p>
<p><a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
</td>
<td>
<p><strong>Operador tur&iacute;stico</strong></p>
<p><strong>&laquo;Compa&ntilde;&iacute;a internacional &laquo;Russki Tur&raquo;, SRL</strong></p>
<p>Direcci&oacute;n jur&iacute;dica: 194044, San Petersburgo, Avenida Finlyandski, 4, letra А, oficina 424-tel./fax +7 (812) 6470690</p>
<p>CIF 7802853888, KPP (C&oacute;digo de concepto de alta fiscal) 780201001<br /> OGRN (n&uacute;mero b&aacute;sico de registro estatal) 1147847089532, OKPO (N&uacute;mero de Clasificaci&oacute;n General de Empresas y Organizaciones) 35460198</p>
<p>PJSC Sovcombank</p>
<p>c/c 40702978880000032988</p>
<p>SWIFT: SOMRRUMM</p>
<p>Banco corresponsal:</p>
<p>UBS Switzerland AG, Zurich, Switzerland</p>
<p>SWIFT: UBSWCHZH80A</p>
<p><br /> <strong>Director General</strong></p>
<p>Cheremshenko Olga Nikolaevna</p>
<p>_____________________________</p>
<p><strong>Tel&eacute;fono:&nbsp;</strong><br /> +7 (812) 6470690</p>
<p><strong>Correo electr&oacute;nico:</strong></p>
<p>info<a href="mailto:visto@russiantour.com">@russiantour.com</a></p>
</td>
</tr>
</tbody>
</table>
<table>
<tbody>
<tr>
</tr>
<tr>
<td>
<p>Приложение № 1 к договору-оферте</p>
<p>о реализации туристского продукта (визовой поддержки) № ______от &laquo;___&raquo; ___________ 20__г.</p>
<p>Россия, Санкт-Петербург</p>
<p><strong>Заявка на реализацию туристского продукта </strong></p>
<p>Дата заявки &laquo;____&raquo; ___________ 2018 г.</p>
<p>Приложение № 1 к договору-оферте о реализации туристского продукта<br /> №&nbsp;id?&gt; от . Россия, Санкт-Петербург</p>
Заявка на реализацию туристского продукта<br /> Дата заявки:<strong>&nbsp;г</strong><br /> Категория и вид визы:<strong> </strong><br /> Консульское учреждение:&nbsp;<br /> Услуга полного оформления :&nbsp;<br /> Необходимость оформления медицинского страховани:&nbsp;<br /> Консульский сбор:
<p>Турист № :</p>
<p>Данные о туристе:&nbsp;<br /> Фамилия, имя:&nbsp;<br /> Дата рождения:<br /> Пол:<br /> Паспорт:<br /> Гражданство/подданство:<strong> </strong><br /> Место жительства:<strong> </strong><br /> Курьерские услуги по забору документов у Заказчика и отправке готовых документов Заказчику<br /> Только обратно:<strong> </strong><br /> Турпакет :<br /> Наименование турпакета (тура):<br /> Дата начала тура:<br /> Дата завершения тура:<br /> Авиабилеты:&nbsp;</p>
</td>
<td>
<p>Anexo N&ordm;1 al contrato-oferta de realizaci&oacute;n del producto tur&iacute;stico N&ordm; ______de &laquo;___&raquo;de ___________ del a&ntilde;o 20__</p>
<p>Rusia, San Petersburgo</p>
<p><strong>Solicitud de realizaci&oacute;n del producto tur&iacute;stico</strong></p>
<p>Fecha de solicitud &laquo;____&raquo; ___________ 2018</p>
<p>Categor&iacute;a y tipo de visado</p>
<p>Entidad consular/</p>
<p>Servicio de formalizaci&oacute;n completa (s&iacute;/no)&nbsp;</p>
Necesidad de hacer el seguro m&eacute;dico (s&iacute;/no)
Arancel consular
Turista N&ordm; 1:
Datos del turista:
Apellido, nombre:
Fecha de nacimiento:
Sexo:
Pasaporte:
Nacionalidad/ciudadan&iacute;a:
Lugar de residencia:
Ciudades de visita:
Servicios de correos para recoger los documentos del Cliente y enviar los documentos listos al cliente
Ida-vuelta (s&iacute;/no):
Solamente ida (s&iacute;/no):
<p>Solamente vuelta (s&iacute;/no):</p>
<p>Paquete tur&iacute;stico (s&iacute;/no):&nbsp;</p>
<p>Denominaci&oacute;n del paquete tur&iacute;stico (del tour):&nbsp;</p>
<p>Fecha de inicio del tour:&nbsp;</p>
<p>Fecha de la terminaci&oacute;n del tour:&nbsp;</p>
<p>Billetes a&eacute;reos: (s&iacute;/no):&nbsp;</p>
<p>Itinerario:</p>
<p>Fecha de ida:&nbsp;</p>
<p>Fecha de vuelta:</p>
</td>
</tr>
<tr>
</tr>
</tbody>
</table>
</div></div>

		