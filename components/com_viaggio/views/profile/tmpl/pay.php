<?php
if (isset($_GET['order']))
{
    $orderId = intval($_GET['order']);
    if ($orderId)
    {
        $db = JFactory::getDbo();
        $query = "SELECT * FROM `#__viaggio_orders` WHERE id = $orderId";
        $db->setQuery($query);
        $order = $db->loadObject();

        $db = JFactory::getDbo();
        $query = "SELECT * FROM `#__viaggio_tours` WHERE id = $order->tour_id";
        $db->setQuery($query);
        $tour = $db->loadObject();

        $db = JFactory::getDbo();
        $query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = $order->id";
        $db->setQuery($query);
        $clients = $db->loadObjectList();
        ?>

		<?php include 'translit.php'; ?>
 <div class="uk-grid uk-panel-box">
	        <h2 class="uk-text-center uk-margin-top"><b>PAGAMENTO DEL TOUR  </b></h2>
        <div class="uk-width-medium-1-1">	
		
	 <b>	E` possibile procedere al pagamento della somma preventivata in una delle seguenti modalita`: </b> <br>
	 <h3 class="tm-article-subtitle"><b> Tramite bonifico bancario:<span id="cost" ><?php echo  round($order->valute_amount); ?></span> € </b> </h3>
	 Bonifico bancario europeo (SEPA), le spese bancarie non devono essere a carico del beneficiario, ma condivise (SHA). Copia del pagamento deve essere inviata scannerizzata all’indirizzo mail viaggio@russiantour.com - i pagamenti non notificati, non saranno presi in considerazione.
		   <div>  <div class="uk-form  uk-float-left">   <input   type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);"> </div>  
		   <div "><b> <a target="_blank"  data-uk-modal="{target:'#modal11'}" >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL </a></b></div></div>  
		<button class="uk-modal-close uk-button uk-button-primary Inviare uk-margin-top"   data-uk-toggle="{target:'#my-id'}" onclick="jQuery.get('?option=com_viaggio&task=print_pagare&number=<?=$order->id?>'); "   disabled="" id="Inviare1"  >COORDINATE BANCARIE</button>
		
		<div id="modal11" class="uk-modal " aria-hidden="true" style="display: none; overflow-y: scroll;"><div class="uk-modal-dialog uk-block-default uk-text-justify uk-modal-dialog-large"> <a href="/" class="uk-modal-close uk-close"></a> <div class="uk-overflow-container">  
<?php  include ('contrattoBancario.php');  ?>
		</div></div> </div>
		 
            <br> <!-- onclick="window.open('?option=com_viaggio&task=print_pagare&number=<?=$order->id?>'); " --> 
			<div aria-hidden="true" class="uk-hidden" id="my-id"><div class="uk-panel uk-panel-box   uk-block-default uk-container-center uk-width-medium-1-2"><div class="uk-panel-badge uk-badge uk-badge-danger">pagamento </div><h4 class="uk-panel-title">COORDINATE BANCARIE <br>(riceverete la fattura via mail)</h3>  <i class="uk-icon-university"></i> 
Russian Tour International Ltd.<br>
Numero di conto corrente: 40702978200190742076<br>
SWIFT: COMKRUMM<br>
Banca: SOVCOMBANK, CORPORATE BRANCH<br>
Banca corrispondente: COMMERZBANK AG, Frankfurt am Main, Germany<br>
SWIFT della banca corrispondente: COBADEFFXXX<br>

<br> Casuale pagamento:   ID: <?=$order->id?> <br>
              Importo: <b><span id="cost" ><?php echo  round($order->valute_amount); ?></span> € </div></b> </div>
         
            <br><div aria-hidden="true" class="uk-hidden" id="my-id"><div class="uk-panel uk-panel-box  uk-container-center uk-width-medium-1-2">  </div> </div> <br>
			
			<h3 class="tm-article-subtitle"><b>Con carta di credito: <span id="cost" ><?php echo round($order->valute_amount*1.02); ?></span>€ </b></h3>
			
			
			Il sistema di pagamento è sicuro, il nostro sito internet supporta la crittografia a 256-bit. Le informazioni inserite non verranno fornite a terzi, salvo nei casi previsti dalla legislazione della Federazione Russa. Le operazioni con carta di credito sono condotte in stretta conformità con i requisiti di sistemi di pagamento internazionali Visa Int. e MasterCard Europe Sprl. <br>
            Sul pagamento con carta di credito, viene applicata una commissione aggiuntiva del 2% sull’importo preventivato. L’addebito viene effettuato in rubli in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi.<br>
            <div>
		              <div class="uk-float-left  uk-form"> <label class="uk-margin-small-top">
                        <input type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare1').attr('disabled',false); else jQuery('.Inviare1').attr('disabled',true);"> </label>
                </div>
				
                <div >
                    <b>
                        <a    target="_blank" data-uk-modal="{target:'#modal12'}"   >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL </a>
                    </b>
                </div>
            </div>
					<div id="modal12" class="uk-modal " aria-hidden="true" style="display: none; overflow-y: scroll;"><div class="uk-modal-dialog uk-block-default uk-text-justify uk-modal-dialog-large"> <a href="/" class="uk-modal-close uk-close"></a>  <div class="uk-overflow-container">  
<?php  include ('contrattoCard.php');  ?>
		</div></div> </div>
			
			<!--onclick="window.open('?option=com_viaggio&task=buy&order_id=<?=$order->id?>')" -->
            <button class="uk-modal-close uk-button uk-button-primary Inviare1 uk-margin-top"
 onclick="UIkit.modal('#BAYWINDOW').show();jQuery.get('?option=com_viaggio&task=print_pagare_card&number=<?=$order->id?>',function(){document.location.href='?option=com_viaggio&task=buy&order_id=<?=$order->id?>'});"  target="_blank"
			 id="Inviare" disabled="">PAGAMENTO CON CARTA DI CREDITO</button>
        </div>
		
	                <div id="BAYWINDOW" class="uk-modal " aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class=" uk-block-default uk-modal-dialog">
                         
              
                        <div id="maggioriResult"></div>
                        <p  class="uk-text-center"><i class="uk-icon-spinner uk-icon-spin uk-icon-large"></i></p>
                       
                    </div>
                </div>	
		<script>
	//нажатие кнопки купить
	jQuery(document).ready(function($) {//waitBack
        $('#1viaggioOpenTourForm').submit(function(e){
            $('#Inviare').prop('disabled', true);
            UIkit.modal("#BAYWINDOW").show();
        });
		$('#viaggioOpenTourForm').submit(function(e){
			//отменяем стандартное действие при отправке формы
			e.preventDefault();
		
			//берем из формы метод передачи данных
			var m_method=$(this).attr('method');
		
			//получаем адрес скрипта на сервере, куда нужно отправить форму
			var m_action=$(this).attr('action');
			
			//получаем данные, введенные пользователем в формате input1=value1&input2=value2...,
			//то есть в стандартном формате передачи данных формы
			var m_data=$(this).serialize();
		
			$.ajax({
				type: m_method,
				url: m_action+'&ajax=1',
				data: m_data,
				success: function(result){
					$('#Inviare').prop('disabled', true);
					//$('#test_form').html(result);
					UIkit.modal('#modal-bay').show();
				}
			});
		});
	});
</script>
</div>
        <?php
    }
}
?>