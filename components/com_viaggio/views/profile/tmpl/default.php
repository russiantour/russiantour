<?php
$doc = JFactory::getDocument();
$user   = JFactory::getUser();
$doc->addStyleSheet('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
?>
<script  type="text/javascript" src="/components/com_viaggio/media/js/disqusloader.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
 <?php foreach($this->items->orders as $key=>$order){ ?>
<article class="uk-article" >

	<div class="uk-grid uk-panel-box" uk-grid="">
	
	
	
		<div class="uk-width-expand@m ">


<div class="uk-grid">
<div class="uk-width-medium-1-1">

 
 <div class="uk-grid-margin-small  item-page uk-padding-remove  " itemscope="" itemtype="https://schema.org/Article">
	<meta itemprop="inLanguage" content="it-IT">
	<div class="uk-width-medium-3-10 uk-padding-remove  ">
 
</div>
 <img class=" uk-align-left uk-border-rounded" src="<?=$order->tour->category_img?>" alt="<?=$order->tour->category_alt?>" title="<?=$order->tour->category_title?>">
</div>
		
		<div class="uk-width-medium-1-1 uk-first-column uk-padding-remove ">

<?php //echo '<pre>'.print_r($order,1).'</pre>'; ?>
	<div class="uk-text-center uk-margin-large">
				<h3  class="uk-h2 uk-text-bold" itemprop="headline"><?php echo $order->tour->name; ?></h3>
    <div class="uk-grid">
				<div class="uk-width-1-2 uk-text-center uk-text-large  uk-margin " ">

			Arrivo  : <span class="uk-icon-plane rotate90 uk-icon-small"></span> <span id="tourItemfrom"><?php echo date('d/m/Y', strtotime($order->tour->from)); ?></span> </div>
			<div class="uk-width-1-2 uk-text-center uk-text-large  ">
			Partenza : <span class="uk-icon-plane uk-icon-small"> </span>  <span id="tourItemTo"><?php echo date('d/m/Y', strtotime($order->tour->to)); ?></span></div></div>
 
   <div class="uk-text-justify  tm-block"><?php echo $order->tour->desc; ?></p>
</div>
         <!-- <b>                    							
							<div class=" <?php if($order->stell != 4){ echo ' uk-hidden '; } ?>" id="4steelButton" >
 
 	 <?php echo JText::_('COM_VIAGGIO_4_STELL'); ?>
  
			</div>
			<div class="  <?php if($order->stell != 3){ echo ' uk-hidden'; } ?>" id="3steelButton" >
 				 <?php echo JText::_('COM_VIAGGIO_3_STELL'); ?>
			</div> 	</b> -->
		</div>
		
	
 
		 
				
		
          
          <?php echo JText::_('COM_VIAGGIO_NOMECOGNOME'); ?>: <b><?=$order->nomecgome?></b><br>
              E-mail: <b><?=$order->email?></b><br> <?php echo JText::_('COM_VIAGGIO_TELEPHONE'); ?> <b><?=$order->telephone?></b> <br>
       		 
<div class="pay">
<?php
	//если оплачено
	if( $order->orderstatus == 2 OR $order->orderstatus == 9 )
	{
		?>
  <div class="   uk-alert uk-alert-danger" data-uk-alert="" > 
<a class="uk-alert-close uk-close"></a>
    <div class="uk-grid">
     
             <div class="uk-width-medium-1-1" >
			 Il pagamento è avvenuto con successo, la pratica è confermata. Riceverete maggiori informazioni al vostro indirizzo e-mail.
 

    
    </div>
    </div>
   </div>
		<?php
	}else{
		//если не оплачено
		?>
  <div class="   uk-alert uk-alert-danger" data-uk-alert="" > 
<a class="uk-alert-close uk-close"></a>
    <div class="uk-grid">
     
             <div class="uk-width-medium-1-1" >
 
Siete stati registrati automaticamente al nostro portale, riceverete i dettagli via mail. E possibile modificare la password generata automaticamente qui: <a href="#modal-pwd" data-uk-modal="">password</a>    

    
    </div>
    </div>
   </div>
		<?php
	}
?>
</div>
	

	

   <div style="color:#f7f7f7">
<?php 

if(count($this->items->orders)){
	//require_once "default-part1.php";

switch ($this->items->orders[0]->orderstatus) {
    case -1:
        echo "Заказ создан";
        break;
	case 0:
		echo "Ждем оплату по карте.";
		break;
    case 1:
        echo "Оплата по карте. Предавторизованная сумма захолдирована (для двухстадийных платежей).";
        break;
    case 2:
        echo "Оплачено по карте. ";
        break;
    case 3:
        echo "Оплата по карте. Авторизация отменена.";
        break;
    case 4:
        echo "Оплата по карте. По транзакции была проведена операция возврата.";
        break;
    case 5:
        echo "Оплата по карте. Инициирована авторизация через ACS банка-эмитента.";
        break;
    case 6:
        echo "Оплата по карте. Авторизация отклонена.";
        break;
	case 8:
        echo "Ждем оплаты по банк клиенту.";
        break;
    case 9:
        echo "Оплачено по банк клиенту.";
        break;
}
}
?>
</div>
   <div id="modal-pwd" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                                <div class="uk-modal-dialog uk-block-default">
                                    <button type="button" class="uk-modal-close uk-close"></button>
                                    <div class="uk-modal-header">
                                        <h2><?=JText::_('COM_VIAGGIO_CHANGING_PASSWORD')?></h2>
                                    </div>
                                    <div class="uk-width-1-1  uk-form ">
				<form action="/?option=com_viaggio&task=changepwd" method="post">
				<input name="pw1" type="password" placeholder="password" value="" required class=" uk-margin-small-top  uk-width-medium-2-10 "> 
				<input name="pw2" type="password" placeholder="password"  value="" required class="uk-margin-small-top uk-width-medium-2-10 ">
			       
			</div>
                                    <div class="uk-modal-footer uk-text-right">
                                        <button type="button" class="uk-button uk-modal-close"><?=JText::_('COM_VIAGGIO_CANCEL')?> </button>
                                        <button type="submit" class="uk-button uk-button-primary"><?=JText::_('COM_VIAGGIO_SAVE')?> </button>
										</form>
                                    </div>
                                </div>
                            </div>
 
  
  
			 
					<div class="uk-grid "> 
			
	  
           
             <div class="uk-width-medium-7-10" >
<?php foreach( $order->clients as $key=>$client){ ?>
	<div class="uk-grid "> 	
	<div class="uk-width-8-10">
	   <h3 class="uk-panel-title uk-text-large "><b><?php echo $client->cognome.' '.$client->nome; ?></b> </h3>   
	   </div>
	    <div class="uk-width-2-10">
		<?php if($order->orderstatus < 0){ ?>
		 <a data-uk-modal="{target:'#modal-edit-<?php echo $client->id; ?>'}"  class="uk-button uk-button-primary  " ><?=JText::_('COM_VIAGGIO_EDIT')?></a>
		 <?php } ?>
		</div>
		<div class="uk-width-2-10">
		 
    <?=JText::_('COM_VIAGGIO_DATA_DI_NASCITA')?>
		</div>
		<div class="uk-width-8-10">
		 <b><?php echo date('d/m/Y', strtotime($client->birthday)); ?></b> 
		</div>
		<?php /*
		<div class="uk-width-2-10">
		  Nazionalita
    
		</div>
		<div class="uk-width-8-10">
		  <b> <?php echo $client->nazionalita; if(!$client->nazionalita) echo 'Italiy'; ?></b> 
		</div>*/?>
				<div class="uk-width-2-10">
		  Sesso
    
		</div>
		<div class="uk-width-8-10">
		 <b><?php if($client->sex == 'm'){ echo 'Maschile';}else{ echo 'Femminile';}; ?></b> 
		</div>
		
		
		</div>
	   	
           
                          
                           

 
              <div id="modal-edit-<?=$client->id?>" class="uk-modal  " aria-hidden="true" style="display: none; overflow-y: scroll;">
                                <div class="uk-modal-dialog uk-block-default">
                                    <button type="button" class="uk-modal-close uk-close"></button>
                                    <div class="uk-modal-header">
                                        <h2><?=JText::_('COM_VIAGGIO_CHANGE')?></h2>
                                    </div>
                                   <div class="uk-grid" id="tourClientsBlock">
			 
			<div class="uk-width-1-1  uk-form tourClientsElement">
				<form action="/?option=com_viaggio&task=editclient" method="post">
				<input type="hidden" name="client_id" value="<?=$client->id?>">
				<input name="cognome" type="text" placeholder="<?php echo $client->cognome; ?>" value="<?php echo $client->cognome; ?>" class=" uk-margin-small-top  uk-width-medium-2-10 "> 
				<input name="nome" type="text" placeholder="<?php echo $client->nome; ?>" value="<?php echo $client->nome; ?>" class="uk-margin-small-top uk-width-medium-2-10 ">
				<select name="sex" class="uk-margin-small-top uk-width-medium-1-10 " autocomplete="off"><option value="m" <?php if($client->sex == 'm'){ echo 'checked';} ?>>Maschile</option>
				<option value="f" <?php if($client->sex == 'f'){ echo 'checked';} ?>>Femminile</option></select> 
				<input name="nascita" value="<?php echo date('d/m/Y', strtotime($client->birthday)); ?>" style=" margin-top: 5px; " class=" uk-width-medium-1-6  participants-birthdate hasDatepicker" type="text" required="" placeholder="<?php echo date('d/m/Y', strtotime($client->birthday)); ?>" data-uk-datepicker="{format:'DD/MM/YYYY'}"> 
				<button type="submit" name="delete" class="uk-button uk-margin-small-top uk-text-right uk-button-danger removeUser deleteTourClient calc"><i class="uk-icon-remove"></i></button>                          
			</div>
		</div>
                                  
                                    <div class="uk-modal-footer uk-text-right">
                                        <button type="button" class="uk-button uk-modal-close"><?=JText::_('COM_VIAGGIO_CLOSE')?> </button>
                                        <button type="submit" data-id="<?php echo $client->id; ?>" class="uk-button uk-button-primary"><?=JText::_('COM_VIAGGIO_SAVE')?> </button>
                                    </div>
									</form>
                                </div>
                            </div>
									 
	 
<?php } ?> 
				 

		 

		<div style="clear:both;"></div>
		 
		<div style="display: none;width:100%">
		<select name="stellcount" id="stellcount" class="calc">
						<option id="stellcount4" value="4">4 stell</option>
			<option id="stellcount3" value="3" disabled="disabled">3 stell</option>
					</select>
		</div>
 
			 </div>
			<div class="uk-width-medium-3-10 "  >
			 <h2 class="uk-h1 uk-text-center uk-margin-top"> <span id="cost" ><?php echo $order->valute_amount; ?></span>€  </h2>
<div class="pay">
<?php
	//если оплачено
	if( $order->orderstatus == 2 OR $order->orderstatus == 9 )
	{
		?>
		Numero d'ordine <?=$order->id?><br/>
		Pagato <?=$order->valute_amount?> € / <?=($order->amount/100)?> rubli / tasso di cambio  <?=($order->valute_rate*1.02)?> rubli/€
		
		
		
		<?php
	}else{
		//если не оплачено
		?>
		<form action="/index.php?option=com_viaggio&task=buy&order_id" method="post">
		 <input type="hidden" name="order_id" value="<?php echo $order->id; ?>">
		<!-- <input type="submit" class="uk-button uk-width-1-1 uk-button-primary " value="">-->
		</form>
		<a class="uk-button uk-width-1-1 uk-button-primary" href="/profile?layout=pay&order=<?=$order->id?>">Pagare</a>
		<?php
	}
?>
</div>	
			</div> 
		  </div>
<!--<input type="submit" value="Order" class=" uk-margin-small-top  uk-width-medium-4-10">-->

<div id="modal6" class="uk-modal" aria-hidden="false" style="display: none; overflow-y: scroll;">
	<div class="uk-modal-dialog">
		<button type="button" class="uk-modal-close uk-close"></button>
		<form action="/index.php?option=com_viaggio&amp;task=register" id="viaggioRegisterForm">
		<div class="uk-modal-header">
			<h2>Password</h2>
		</div>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
		
			<input name="password1" required="" type="text" placeholder="Password" class="edinfopass uk-margin-small-top  uk-width-medium-4-10">
			<input name="password2" required="" type="text" placeholder="Password" class="edinfopass uk-margin-small-top  uk-width-medium-4-10">
			<span id="validpass"></span>
		</p>
		<div class="uk-modal-footer uk-text-right">
			<button type="button" class="uk-button uk-modal-close">Cancel</button>
			<button type="submit" id="viaggioRegisterSubmit" class="uk-button uk-button-primary" disabled="">Save</button>
		</div>
		</form>
	</div>
</div>
 	 
 

 
<div class=" uk-width-medium-9-10 uk-text-center ">
 
</div>
<?php
	//определяем идентификатор тура для disqusа
	$disqus_identifier = 0;
	if( $order->tour->comment_id ){
		$disqus_identifier = $order->tour->comment_id;
	}else{
		$disqus_identifier = $order->tour->id;
	}
?>
<div   class="uk-margin10  uk-text-center ">
	<div class="uk-width-medium-1-1 uk-row-first" id="item<?=$order->id?>tab">
		<ul class="uk-tab" id="item<?=$order->id?>tab" data-uk-tab="{connect:'#tab-content<?=$order->id?>'}">
			<?php if($order->tour->fulltext){ ?>
				<li class="uk-active default"  ><a href="#">Programma</a></li>
			<?php } ?>
			<?php if($order->tour->tab4){ ?>
				<li class=""><a href="#">Cosa visiterete</a></li>
			<?php } ?>
			<?php if($order->tour->tab2){ ?>
				<li class=""><a href="#">Incluso/Non incluso</a></li>
			<?php } ?>
			<?php if($order->tour->tab3){ ?>
				<li class=""><a href="#">Itinerario del viaggio</a></li>
			<?php } ?>
				<li class="disqus-comments-tab" data-disqus-target="disqus_thread_tab<?php echo $order->id; ?>" data-disqus-id="<?php echo $disqus_identifier; ?>" data-disqus-title="<?php echo $order->tour->name; ?>"><a href="#">Domande e Recensioni</a></li>
				<li class="uk-tab-responsive uk-active uk-hidden"><a>Tab</a>
					<div class="uk-dropdown uk-dropdown-small"> </div>
				</li>
		</ul>
		<ul id="tab-content<?=$order->id?>" class="uk-switcher uk-margin">
		<?php if($order->tour->fulltext){ ?>
		<li><?php echo $order->tour->fulltext; ?></li>
		<?php } ?>
		<?php if($order->tour->tab4){ ?>
		<li class=""><?php echo $order->tour->tab4; ?></li>
		<?php } ?>
		<?php if($order->tour->tab2){ ?>
		<li class=""><?php echo $order->tour->tab2; ?></li>
		<?php } ?>
		<?php if($order->tour->tab3){ ?>
		<li class=""><?php echo $order->tour->tab3; ?></li>
		<?php } ?>
<div id="" class="disqus_thread_tab disqus_thread_tab<?php echo $order->id; ?>"></div>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>	
		</li>
		</ul>
	</div>	
</div>
</div> </article>
 <?php } ?>

<script type="text/javascript">
//хак для виджекит, чтобы использовать несколько галереи на странице
jQuery(window).trigger('resize');
jQuery('.uk-tab').on('click', function(){
	jQuery(window).trigger('resize');
})

jQuery( document ).ready(function($) {
	//если нажали вкладку с комментариями
	$('.disqus-comments-tab').on('click', function(){
		//получаем id тура
		var tour_id = $(this).attr('data-disqus-id');
		var order_id = $(this).parent().attr('id')
		var tour_title = $(this).attr('data-disqus-title');
		var disqus_target = $(this).attr('data-disqus-target');
		var disqus_el = $('.'+disqus_target);
		//переключаем вкладки в остальных турах на дефолтную
		$('.uk-tab').each(function(indx, element){
			$(this).children('.disqus-comments-tab').attr( 'data-disqus-id' );
			if( $(this).attr('id') != order_id ){
				$(this).children('.default').click();
			}else{
				//загружаем комментарии
				$.disqusLoader( disqus_el,
				{
					scriptUrl:		'//viaggiorussia.disqus.com/embed.js',
					disqusConfig:	function()
					{
						this.page.identifier	= tour_id;
						this.page.title			= tour_title;
					}
				});
			}
		});
	})
})
</script>