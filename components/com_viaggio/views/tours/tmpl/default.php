<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

// подключение класса 
//jimport('joomla.html.pagination');

$user       = JFactory::getUser();
$userId     = $user->get('id');
$listOrder  = $this->state->get('list.ordering');
$listDirn   = $this->state->get('list.direction');
$canCreate  = $user->authorise('core.create', 'com_viaggio') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tourform.xml');
$canEdit    = $user->authorise('core.edit', 'com_viaggio') && file_exists(JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR . 'forms' . DIRECTORY_SEPARATOR . 'tourform.xml');
$canCheckin = $user->authorise('core.manage', 'com_viaggio');
$canChange  = $user->authorise('core.edit.state', 'com_viaggio');
$canDelete  = $user->authorise('core.delete', 'com_viaggio'); 
?>
<div class="uk-grid tm-leading-article" data-uk-grid-match="" data-uk-grid-margin="">
<div class="uk-width-medium-1-1 uk-row-first">  
<?php foreach ($this->items as $i => $item) : ?>
<article class="uk-article" uk-grid="" data-permalink="<?php echo JRoute::_('index.php?view=showtour&id='.(int) $item->id); ?>">
	 <div  class="uk-grid  uk-panel  uk-panel-box uk-panel-hover "> 
	<div class="uk-width-medium-3-10 uk-padding-remove  ">
 <img class="uk-align-left uk-border-rounded"  src="<?php if($item->category_img){ echo $item->category_img; }else{
					echo '/components/com_viaggio/media/images/noimage.gif';
				} ?>" alt="<?php echo $item->category_img_alt; ?>" title="<?php echo $item->category_img_title; ?>">
</div>
 
			<div class="uk-width-medium-7-10  ">		 
 <h2 class=" uk-text-center"> 		 
 <a class="uk-h2 uk-text-bold" style="color: #3a3a32;" href="<?php echo JRoute::_('index.php?view=showtour&id='.(int) $item->id); ?>" title="<?php echo $item->name; ?>">
		 <?php echo $item->name; ?></h2> </a>
<?php //print_r($item); ?>
<div class="uk-grid "><div class="uk-width-medium-1-2  uk-h3  "> 
<?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_MOSCA'); ?> <span class="uk-icon-plane rotate90 uk-icon-small"></span> <span class=""></span> <?php echo date('d/m/Y', strtotime($item->from)); ?> </div>
<div class="uk-h3 uk-width-medium-1-2"><?php echo JText::_('COM_VIAGGIO_PARTENZA_DA_ITALIA'); ?> <span class="uk-icon-plane uk-icon-small"> </span>   <span class=""> </span><?php echo date('d/m/Y', strtotime($item->to)); ?> </div></div>

<!--
Duration:  <span class="uk-icon-sun-o"> </span>  7 days   <span class="uk-icon-moon-o"> </span>  8 notte  <br>
Hotels :            
         4 stell   ,  3 stell 
--><div class="uk-text-justify  tm-block">
  <?php echo $item->desc; ?> 
 </div>
			

	
		<!--<p><a href="/mosca-sanpietroburgo/viaggio-mosca/capodanno?task=article.edit&amp;a_id=53&amp;return=aHR0cHM6Ly93d3cudmlhZ2dpby1ydXNzaWEuY29tL21vc2NhLXNhbnBpZXRyb2J1cmdv" title="Modifica articolo"><span class="hasTooltip icon-edit tip" title="" data-original-title="<strong>Modifica articolo</strong><br />Pubblicato<br />Martedì, 29 Novembre 2016<br />Scritto da Super User"></span>Modifica</a></p>-->
	<div class="uk-grid   ">
	<div class="uk-width-medium-1-2  ">
	 
	
		 <a class="uk-h4 "  href="<?php
			$subcategory = ViaggioHelpersFrontend::getCategory($item->catid);
			echo $subcategory->path;
		?>"><?php
			echo $subcategory->title;
		?></a>
	 	<br>
 <span class="uk-text-bottom"> Durata: <?php echo ViaggioHelpersFrontend::detDuration($item->from,$item->to); ?> giorni </span>
	 </div>
	<div class="uk-width-medium-1-4 uk-text-center  ">
		 <span class="uk-text-muted ">prezzo</span> <h3  style="color: #0063AF;" class="uk-h1 uk-margin-remove  "> <?php echo ViaggioHelpersFrontend::getCost( $item->id, 0, 2 )/2; ?>€   </h3>
	 </div>
	 <div class="uk-width-medium-1-4 uk-text-right uk-text-bottom      "><br>
		<a class="uk-button  uk-text-right uk-text-bottom    uk-button-primary " href="<?php echo JRoute::_('index.php?view=showtour&id='.(int) $item->id); ?>" title="<?php echo $item->name; ?>"><?php echo JText::_('COM_VIAGGIO_LEGGI_TUTTO'); ?></a>
	 </div>
	 
	  
	 
	</div> </div>

</article>
<?php endforeach; ?>
</div>
 </div>
 
<?php //echo $this->pagination->getListFooter(); ?>
<div class="pagination">
	<p class="counter pull-right"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
	<?php echo $this->pagination->getPagesLinks(); ?>
</div>