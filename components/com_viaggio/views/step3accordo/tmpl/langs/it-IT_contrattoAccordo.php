<?php
include 'translit.php';

//include '/home/russiantour/web/russiantour.com/public_html/includes/firma.php';

?>
<div class="uk-grid">
 <div class="uk-width-1-2">
<p class="uk-text-center"><strong>Соглашение о расторжении
Договора оферты о реализации туристского продукта
№ </strong> C<?php echo $manualPayment['order']['id']; ?> от  <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?>
</p><p class="uk-text-center">
г. Санкт-Петербург<br> <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?> 
</p>
<b><?php echo $name_ru; ?> (ООО <?php echo $name_ru; ?>)</b>, далее именуемое «Туроператор», в лице генерального директора Черемшенко Ольги Николаевны, действующей на основании Устава, с одной стороны 
   и гражданин Итальянской Республики   <b><?php  echo   translitText($manualPayment['order']['nomecgome']);  ?>  </b>  , электронный адрес <?php  echo $manualPayment['order']['email']; ?> , далее именуемый «Заказчик», с другой стороны, действующие согласно Договора оферты о реализации туристского продукта №  C<?php echo $manualPayment['order']['id']; ?> от <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?>  (далее – Договор), далее совместно именуемые «Стороны», составили настоящее Соглашение о расторжении Договора оферты о реализации туристского продукта №  C<?php echo $manualPayment['order']['id']; ?> от <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?>   (далее – Соглашение) и договорились о нижеследующем:



<p class="uk-text-center uk-text-uppercase"><strong>1.	Преамбула</strong></p>
1.1.	Настоящее Соглашение, условия которого безоговорочно принимаются и подписываются Сторонами, вызвано обстоятельствами непреодолимой силы (форс-мажор) и ограничениями эпидемиологического характера, связанных с глобальным распространением COVID-19, что повлекло за собой:
— юридические, а затем и фактические ограничения деятельности Туроператора;<br>
— невозможность осуществления Туроператором на протяжении более 20 (Двадцати) месяцев экономической деятельности на территориях оказываемых услуг, предусмотренных Туристским продуктом;<br>
 — экономические споры и судебные разбирательства в рамках соответствующих юрисдикций с государственными органами, поставщиками услуг и иными контрагентами (сервисы, агрегаторы железнодорожных и авиабилетов, субъекты гостиничного отдыха), не исполнившими свои обязательства в рамках сформированных Туристских продуктов в интересах Заказчика; <br>
— неравномерное законодательное регулирование деятельности поставщиков услуг и иных контрагентов (сервисов, агрегаторов железнодорожных и авиабилетов, субъектов гостиничного отдыха), при котором юридическая и финансовая ответственность указанных субъектов ограничена нормами и правилами из обстоятельств непреодолимой силы (форс-мажор);<br>
— меры, предпринятые Туроператором во избежание последствий банкротства и обеспечения исполнения обязательств по Договору в интересах Заказчика.<br>
1.2.	Настоящее Соглашение следует основным нормам и принципам Договора и заключено Сторонами в соответствии с требованиями законодательства Российской Федерации.<br>
1.3.	Стороны в настоящем Соглашении используют термины и определения, предусмотренные Договором.<br>
1.4.	 Стороны при осуществлении своих прав и обязанностей по Договору принимают во внимание и действуют из обстоятельств непреодолимой силы (форс-мажор), ограничений эпидемиологического характера, возникших на территориях постоянного пребывания Сторон в связи распространением CIVID-19, а также и силу законодательства Российской Федерации, влияющего на права и обязанности каждой из Сторон, что находится в прямой причинно-следственной связи с невозможностью исполнения обязательств Сторонами по Договору ввиду названных обстоятельств.<br>
1.5.	Стороны, заключая настоящее Соглашение, руководствуются, в частности, следующими нормативными правовыми актами законодательства Российской Федерации, Постановлениями и Распоряжениями Правительства Российской Федерации, разъяснениями Верховного суда Российской Федерации, а также условиями Договора:<br>
1.5.1.	п. 5.3 Договора, в соответствии с которым Стороны освобождаются от ответственности за частичное или полное невыполнение обязательств по Договору, если это неисполнение является следствием наступления обстоятельств непреодолимой силы, то есть возникших в результате чрезвычайных и непредотвратимых при данных условиях обстоятельств, которые Стороны не могли не предвидеть, ни предотвратить разумными мерами. Если данные обстоятельства будут продолжаться более 14 (Четырнадцати) календарных дней, каждая из Сторон вправе отказаться от исполнения обязательств по Договору, и в этом случае ни одна из Сторон не будет иметь права на возмещение другой Стороной возможных убытков по основаниям непреодолимой силы;<br>
1.5.2.	п. 1 Распоряжение Правительства РФ от 16.03.2020 № 635-р, в соответствии с которым в целях обеспечения безопасности государства, защиты здоровья населения и нераспространения новой корона вирусной инфекции на территории Российской Федерации Федеральной службе безопасности Российской Федерации поручено с 00 часов 00 минут по местному времени 18 марта 2020 г. временно ограничить въезд в Российскую Федерацию иностранных граждан и лиц без гражданства;<br>
1.5.3.	п. 3 ст. 401 Гражданского кодекса Российской Федерации (далее – ГК РФ), в соответствии с которым лицо, не исполнившее или ненадлежащим образом исполнившее обязательство при осуществлении предпринимательской деятельности, несет ответственность, если не докажет, что надлежащее исполнение оказалось невозможным вследствие непреодолимой силы (форс-мажор), то есть чрезвычайных и непредотвратимых при данных условиях обстоятельств;<br>
1.5.4.	«Обзор по отдельным вопросам судебной практики, связанным с применением законодательства и мер по противодействию распространению на территории Российской Федерации новой корона вирусной инфекции (COVID-19) N 1» Верховного суда Российской Федерации от 21.04.2020, в соответствии с которым применительно к нормам статьи 401 ГК РФ обстоятельства, вызванные угрозой распространения новой корона вирусной инфекции, а также принимаемые органами государственной власти и местного самоуправления меры по ограничению ее распространения, в частности, установление обязательных правил поведения при введении режима повышенной готовности или чрезвычайной ситуации, запрет на передвижение транспортных средств, ограничение передвижения физических лиц, приостановление деятельности предприятий и учреждений, отмена и перенос массовых мероприятий, введение режима самоизоляции граждан и т.п., могут быть признаны обстоятельствами непреодолимой силы (форс-мажор), если будет установлено их соответствие названным выше критериям таких обстоятельств и причинная связь между этими обстоятельствами и неисполнением Сторонами обязательств по Договору.<br>
<p class="uk-text-center uk-text-uppercase"><strong> 2.	Условия расторжения Договора, права и обязанности Сторон</strong></p>
 
2.1.	Стороны пришли к соглашению расторгнуть Договор.<br>
2.2.	Договор считается расторгнутым с момента признания и подписания – полного и безоговорочного принятия (акцепта) оферты Заказчиком в порядке, установленном п. 3 ст. 438 ГК РФ – условий Соглашения, размещенных на сайте Туроператора https://www.russiantour.com/ita/accordo?payment_id=m<?php echo $manualPayment['order']['id']; ?> в информационно-телекоммуникационной системе «Интернет».<br>
2.3.	Стороны определили следующий порядок и сроки исполнения обязательств по Договору в связи с его расторжением:<br>
2.3.1.	Туроператор по письменному запросу Заказчика в течение 7 (Семи) рабочих дней с момента признания и подписания условий Соглашения перечисляет на счет Заказчика денежные средства, уплаченные ранее по Договору, в размере 40 (Сорок) % от цены, от суммы, уплаченной ранее по Договору Туристского продукта в виде аванса или иных согласованных Сторонами платежей и перечислений. В случае, если Туроператором ранее был осуществлен частичный возврат денежных средств, уплаченных Заказчиком за Туристский продукт, Туроператор обязуется возвратить Заказчику остаток денежных средств в той пропорции, при которой совокупный возврат составит 40 (Сорок) % от суммы, уплаченной ранее по Договору Туристского продукта в виде аванса или иных согласованных Сторонами платежей и перечислений.<br>
2.3.2.	Денежные средства в размере 60 (Шестьдесят) % от стоимости Туристского продукта, уплаченных Заказчиком по Договору, признаются Сторонами депозитом (бессрочным ваучером).<br>
2.3.3.	Депозит (бессрочный ваучер) может быть использован Заказчиком на следующих условиях:<br>
2.3.3.1.	Заказчик использует Депозит (бессрочный ваучер) в качестве предоплаты при приобретении иного Туристского продукта, предложенного Туроператором.<br>
2.3.3.2.	Заказчик истребует у Туроператора Депозит (бессрочный ваучер), а Туроператор осуществляет возврат Заказчику Депозита (бессрочного ваучера) в течение 7 (Семи) рабочих дней с момента направления соответствующего запроса Заказчика.<br>
2.3.4.	Условия п.п. 2.3.4.1-2.3.4.2 настоящего Соглашения подлежат применению при наступлении следующих обстоятельств в совокупности: <br>
— изменения в законодательстве стран пребывания Сторон и, в частности, в законодательстве Российской Федерации, в результате которых ограничения на деятельность Туроператора, вызванные эпидемиологической ситуацией в связи с распространением COVID-19, будут ослаблены в той мере, в которой исполнение обязательств Туроператора станет возможным, либо сняты в полном объеме;<br>
— нормализация эпидемиологической ситуации в странах пребывания Сторон и, в частности, в Российской Федерации, обуславливающая возобновление фактической экономической деятельности Туроператора; <br>
— открытие границ между странами пребывания Сторон, в результате которого для Сторон наступит фактическая возможность исполнения обязательств по настоящему Соглашению;<br>
— устранение иных законодательных и фактических ограничений, которые могут препятствовать исполнению Сторонами условий настоящего Соглашения.<br>
2.4.	С момента расторжения Договора обязательства Сторон по Договору прекращаются, за исключением обязательств, указанных в настоящем Соглашении.<br>
2.5.	Принятием и подписанием настоящего Соглашения Стороны подтверждают, что не имеют друг к другу претензий и иных притязаний по Договору.<br>
2.6.	Акцептом настоящего Соглашения со стороны Заказчика является проставление отметки “V” в графе “HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DELL’ACCORDO” размещенных на сайте Туроператора https://www.russiantour.com/ita/accordo?payment_id=m<?php echo $manualPayment['order']['id']; ?> в информационно-телекоммуникационной системе «Интернет». Проставление Заказчиком отметки “V” в указанной графе обозначает согласие Заказчика с условиями Соглашения, принятие Заказчиком условий настоящего Соглашения. <br>
<p class="uk-text-center uk-text-uppercase"><strong> 3.	Реквизиты Сторон Туроператор</strong></p>

<table>
<tbody>
<tr>
<td>
Полное наименование<br>
</td>
<td>
Общество с ограниченной ответственностью <?php echo $name_ru; ?><br>
</td>
</tr>
<tr>
<td>
Сокращенное наименование<br>
</td>
<td>
<strong>ООО "<?php echo $name_ru; ?>"</strong><br>
</td>
</tr>
<tr>
<td>
Адрес (место нахождения)<br>
</td>
<td>
<?php echo $adres_ru; ?><br>
</td>
</tr>
<tr>
<td>
Почтовый адрес<br>
</td>
<td>
<?php echo $adres_po_ru; ?><br>
</td>
</tr>
<tr>
<td>
Реестровый номер<br>
</td>
<td>
РТО <?php echo $mbt; ?><br>
</td>
</tr>
<tr>
<td>
Телефон/факс<br>
</td>
<td>
+7 (812) 6470690<br>
</td>
</tr>
<tr>
<td>
Электронная почта/Сайт<br>
</td>
<td>
<?php echo $email; ?> 
<a href="http://www.russiantour.com">www.russiantour.com</a><br>
</td>
</tr>
</tbody>
</table>
2. Сведения об организации, предоставившей Туроператору финансовое<br>
обеспечение ответственности Туроператора:<br>
<table>
<tbody>
<tr>
<td>
Вид финансового обеспечения ответственности туроператора<br>
</td>
<td>
Страхования гражданской ответственности туроператора - внутренний туризм<br> международный въездной<br>
</td>
</tr>
<tr>
<td>
Размер финансового обеспечения<br>
</td>
<td>
500000 руб.<br>
</td>
</tr>
<tr>
<td>
Дата и срок действия договора страхования ответственности туроператора или банковской гарантии<br>
</td>
<td>
<?php echo $mbt_data_ru; ?><br>
</td>
</tr>
<tr>
<td>
Наименование организации, предоставившей финансовое обеспечение ответственности туроператора<br>
</td>
<td>
АО "Либерти Страхование"<br>
</td>
</tr>
<tr>
<td>
Адрес (местонахождение)<br>
</td>
<td>
196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А<br>
</td>
</tr>
<tr>
<td>
Почтовый адрес<br>
</td>
<td>
196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А<br>
</td>
</tr>
<tr>
<td>
Сайт<br>
</td>
<td>
<a href="http://www.liberty24.ru">www.liberty24.ru</a><br>
</td>
</tr>
<tr>
<td>
Электронная почта<br>
</td>
<td>
<a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a><br>
</td>
</tr>
 <tr>
 <td colspan="2">

Туроператор: <?php echo $ooo_ru; ?>
<img src="<?php echo $pech_url; ?>" style="height: auto;width:400px;">



 
 
</td>
</tr>
 
<tr>
 <tr>
<td colspan="2">

<?php if (isset($manualPayment['clients'][1]) && $manualPayment['clients'][1]['nome'] == '')  { ?>
   <p class="uk-text-center "> Данные о туристах:</p>
   ГРУППА  <b>(<?php echo $manualPayment['order']['clientcount'];?>)</b> человек<br>
<?php } ?>
<?php  foreach ($manualPayment['clients'] as $n=>$client) {
    if ($n==1 && $client['nome'] == '')
        break;
    ?>
		 <p class="uk-text-center "> Турист № : <?php  echo ($n+1);?> </p>
Данные о туристе: <br>
Фамилия, имя: <b>  <?php  echo translitText($client['cognome']); ?>  <?php  echo translitText($client['nome']); ?> </b> <br>
 

Пол:<b>  <?php   if(($client['sex']) == 'm'){ echo 'Мужской';}else{ echo 'Женский';};  ?>   </b> <br>
<?php echo (($client['numero_di_passaporto']=='')?'':'Паспорт: '.$client['numero_di_passaporto'].'<br/>') ;?>
<?php echo (($client['birthday']=='1970-01-01')?'':'Дата рождения: '.date('d/m/Y',strtotime($client['birthday'])).'<br/>') ;?>

Гражданство/подданство:<b> Италия</b><br>
Турпакета (тур):<b>    <?php echo $manualPayment['trip']['name_rus'];?> </b> <br> 
Города посещения:<b> <?php echo $manualPayment['tour']->citys_rus ;?></b><br>
 
<?php echo (($manualPayment['order']['date_from']=='0000-00-00')?'':' Дата начала тура: '.date('d/m/Y',strtotime($manualPayment['order']['date_from'])).'<br/>') ;?>
<?php echo (($manualPayment['order']['date_to']=='0000-00-00')?'':'  Дата завершения тура: '.date('d/m/Y',strtotime($manualPayment['order']['date_to'])).'<br/>') ;?>
 
Авиабилеты:<b> Нет</b> <br>
Ж/Д билеты:<b> Нет</b> <br>
  <?php   } ?>
    <?php echo (($manualPayment['order']['cancellation']=='')?'':'<br>УСЛОВИЯ АННУЛЯЦИИ:<br> '.$manualPayment['order']['cancellation'].'<br/>') ;?>
 
</td>
</tr>

</tbody>
</table>
</td>
 </div>
  <div class="uk-width-1-2">
  
 

  <p class="uk-text-center"><strong>  Accordo di risoluzione 
del Сontratto-Offerta di realizzazione di un prodotto turistico
n. C<?php echo $manualPayment['order']['id']; ?> del <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?></strong><br>
San Pietroburgo          <br>           <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?>                                                                             
    </p>  
  <b> La società a responsabilità limitata <?php echo $name; ?> (ООО <?php echo $name; ?>)</b>, di seguito denominata «Operatore turistico», rappresentata dal direttore generale Cheremshenko Olga Nikolaevna, operante sulla base dello Statuto, da un lato, 
<br>
(1) e il cittadino della Repubblica Italiana <b> <?php  echo  $manualPayment['order']['nomecgome'];  ?> </b> ,   mail <?php  echo $manualPayment['order']['email']; ?> , di seguito chiamato «Cliente», d'altra parte, agiscono secondo il Сontratto-Offerta di realizzazione di un prodotto turistico n. C<?php echo $manualPayment['order']['id']; ?>  del <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?> (di seguito – Contratto), in seguito denominati congiuntamente le "Parti", hanno stipulato il presente Accordo di risoluzione del Contratto-Offerta di realizzazione di un prodotto turistico n. C<?php echo $manualPayment['order']['id']; ?> del <?php   echo date('d/m/Y',$manualPayment['order']['id']); ?> (di seguito – Accordo) e hanno concordato quanto segue:
<p class="uk-text-center uk-text-uppercase"><strong> 1.	Preambolo</strong></p>

1.1	Il presente Accordo, i cui termini sono incondizionatamente accettati e firmati dalle Parti, è causato da circostanze di forza maggiore, ed in particolar modo dalle restrizioni epidemiologiche relative alla diffusione globale del COVID-19, che ha comportato:<br>
—	Limitazioni legali, ed a seguire limitazioni di fatto, nello svolgimento delle attività dell’Operatore turistico;<br>
—	L’impossibilità di porre in atto, da parte dell’Operatore turistico, per più di 20 (venti) mesi le attività inerenti alla prestazione dei servizi previsti dal Prodotto turistico;<br>
—	Controversie commerciali e procedure giudiziarie, nell'ambito delle rispettive giurisdizioni, con le autorità pubbliche, i fornitori, e altri contraenti (realizzatori di servizi turistici, aggregatori di biglietti aerei e ferroviari, strutture alberghiere), che non hanno adempiuto i loro obblighi nell'ambito dei Prodotti turistici formati nell'interesse del Cliente;<br>
—	La regolamentazione legislativa non uniforme delle attività dei fornitori di servizi e di altre controparti (realizzatori di servizi turistici, aggregatori di biglietti aerei e ferroviari, strutture alberghiere), in cui la responsabilità legale e finanziaria di questi soggetti è limitata dalle norme e dai regolamenti delle circostanze di forza maggiore;<br>
—	L’adozione, da parte dell’Operatore turistico, di misure per evitare le conseguenze del fallimento e garantire l'esecuzione degli obblighi previsti dal Contratto a beneficio del Cliente.<br>
1.2	Il presente Accordo segue le regole e i principi di base del Contratto ed è concluso dalle Parti in conformità ai requisiti della legislazione della Federazione Russa.<br>
1.3	Le Parti nel presente Contratto utilizzano i termini e le definizioni previste dal Contratto.<br>
1.4.	Le parti nell'esercizio dei loro diritti e doveri ai sensi del Contratto prendono in considerazione e agiscono in base alle circostanze di forza maggiore, alle restrizioni di natura epidemiologica che si sono verificate nei territori di residenza delle Parti in relazione alla diffusione del COVID-19, così come per gli effetti della legislazione della Federazione Russa, che incide su diritti e doveri di ciascuna delle Parti, e che si trova in relazione causale diretta con l'impossibilità di adempimento degli obblighi dalle Parti nel Contratto a causa di dette circostanze.<br>
1.5.	Le Parti, concludendo l'Accordo, sono guidate dagli atti normativi della legislazione della Federazione Russa, dai Decreti e dagli Ordini del Governo della Federazione Russa, dai chiarimenti della Corte Suprema della Federazione Russa, cosi come dai termini del Contratto. In particolare:<br>
1.5.1.	Paragrafo 5.3 del Contratto, in base al quale le Parti sono esenti da responsabilità per il parziale o totale inadempimento degli obblighi derivanti dal Contratto, se l'inadempimento è la conseguenza di circostanze di forza maggiore, cioè derivanti dall'emergenza e circostanze non evitabili, che le Parti non potevano né prevedere né prevenire con precauzioni ragionevoli. Se tali circostanze hanno una durata superiore a 14 (Quattordici) giorni di calendario, ciascuna delle Parti ha il diritto di rifiutare l'esecuzione degli obblighi derivanti dal Contratto, ed in questo caso nessuna delle Parti avrà diritto al risarcimento dall’altra Parte di possibili perdite per motivi di forza maggiore;<br>
1.5.2.	Comma 1 del Decreto legislativo del Governo della Federazione Russa n. 635-p del 16.03.2020, in base al quale, al fine di garantire la sicurezza dello stato, la tutela della salute della popolazione e la non proliferazione della nuova infezione da coronavirus sul territorio della Federazione Russa, il Servizio Federale per la sicurezza della Federazione Russa è stato incaricato, dalle ore 00  minuti 00 ora locale del 18 marzo 2020, a limitare temporaneamente l'ingresso nella Federazione Russa di cittadini stranieri e apolidi;<br>
1.5.3.	Comma 3 dell’articolo 401 del Codice Civile della Federazione Russa (di seguito Codice Civile), in base al quale la persona che non ha adempiuto o ha adempiuto in modo improprio nell’esercizio di attività imprenditoriale, è responsabile, se non prova che la corretta esecuzione si è rivelata impossibile a causa di forza maggiore, cioè per una situazione di emergenza e per circostanze non evitabili;<br>
1.5.4.	"Rassegna su alcune questioni di prassi giudiziaria relative all'applicazione della legislazione e delle misure per contrastare la diffusione sul territorio della Federazione Russa della nuova infezione da coronavirus (COVID-19) n. 1” della Corte Suprema della Federazione Russa del 21.04.2020, in base alla quale, in relazione alle norme dell'articolo 401 del Codice Civile, le circostanze causate dal pericolo della diffusione di una nuova infezione di coronavirus, e anche le misure prese dalle autorità pubbliche e di governo locale per limitare la sua diffusione, ed in particolare, l'istituzione di una serie di norme di comportamento in seguito all’introduzione di modalità di allerta o di emergenza, quali il divieto alla circolazione dei veicoli, limiti della circolazione delle persone fisiche, la sospensione dell'attività delle imprese e delle istituzioni, la cancellazione e rinvio di eventi di massa, l'introduzione di modalità di autoisolamento dei cittadini, ecc., possono essere riconosciuti come cause di forza maggiore, se sarà individuata la loro conformità ai criteri sopra menzionati di tali circostanze e la relazione causale tra queste circostanze e il mancato adempimento degli obblighi contrattuali dalle Parti.<br>
<p class="uk-text-center uk-text-uppercase"><strong> 2.	Termini di risoluzione del Contratto, diritti e doveri delle parti</strong></p>


2.1. 	Le Parti hanno raggiunto un accordo per risolvere il Contratto.<br>
2.2.	Il contratto è considerato rescisso dalla data del riconoscimento e della stipulazione, cioè dall’accettazione completa e incondizionata dell'offerta da parte del Cliente secondo le modalità previste dalla comma 3 dell’articolo 438 del Codice Civile, ovvero i termini dell'Accordo, pubblicati sul sito web dell’Operatore turistico alla pagina https://www.russiantour.com/ita/accordo?payment_id=m<?php echo $manualPayment['order']['id']; ?> nel sistema di informazione e telecomunicazione «Internet».<br>
2.3.	Le Parti hanno determinato la seguente procedura ed il seguente periodo di adempimento degli obblighi ai sensi del Contratto in relazione alla sua risoluzione:<br>
2.3.1. L’Operatore turistico su richiesta scritta del Cliente entro 7 (Sette) giorni lavorativi dal momento di accettazione e della stipulazione dell’Accordo accredita sul conto del Cliente il denaro versato in precedenza in base al Contratto, per un importo pari al 40 (quaranta)% dell'importo pagato dal Cliente per il Prodotto turistico ai sensi del Contratto, in forma di anticipo o altre forme di pagamento accordate tra le Parti.<br>
2.3.2	Nel caso in cui l’Operatore turistico abbia in precedenza effettuato un rimborso parziale delle somme versate dal Cliente per il Prodotto turistico, l’Operatore turistico si impegna a restituire al Cliente il saldo nella stessa proporzione in cui il rimborso cumulativo sarà del 40 (quaranta) % dell'importo pagato dal Cliente per il Prodotto turistico ai sensi del Contratto, in forma di anticipo o altre forme di pagamento accordate tra le Parti.<br>
2.3.3	La restante parte, pari al 60 (sessanta) % del valore del Prodotto turistico pagato dal Cliente ai sensi del Contratto è riconosciuta dalle Parti come deposito (voucher a tempo indeterminato).<br>
2.3.4	Il deposito (voucher a tempo indeterminato) può essere utilizzato dal Cliente nelle seguenti modalità:<br>
2.3.4.1	Il Cliente utilizza il deposito (voucher a tempo indeterminato) come pagamento anticipato per l'acquisto di un altro Prodotto turistico offerto dall’Operatore turistico.<br>
2.3.4.2	Il Cliente richiede all’Operatore turistico il rimborso del deposito (voucher a tempo indeterminato) e l’Operatore turistico restituisce al Cliente l’importo monetario corrispondente al deposito (voucher a tempo indeterminato) entro 7 (sette) giorni lavorativi dalla data di invio della richiesta del Cliente.<br>
2.3.5	I termini dei paragrafi 2.3.4.1-2.3.4.2 del presente Accordo sono applicabili soltanto quando si verificano le seguenti circostanze in contemporanea:<br>
—	Cambiamenti nella legislazione dei paesi di residenza delle Parti e, in particolare, nella legislazione della Federazione Russa, in cui le limitazioni alle attività dell’Operatore turistico, causati dalla situazione epidemiologica in relazione con la diffusione COVID-19, saranno ritirate in pieno o facilitate nella misura in cui l'adempimento degli obblighi dell’Operatore turistico sarà possibile;<br>
—	Normalizzazione della situazione epidemiologica nei paesi di residenza delle Parti e, in particolare, nella Federazione Russa, che porti alla ripresa in pieno delle attività lavorative dell’Operatore turistico;<br>
—	La completa apertura delle frontiere tra i paesi di residenza delle Parti, a seguito della quale per le Parti sopraggiungerà l’effettiva capacità di adempimento degli obblighi ai sensi del presente Accordo;<br>
— eliminazione di altre restrizioni legali e di fatto, che potrebbero impedire alle Parti di rispettare i termini del presente Accordo.
2.4	Dal momento della risoluzione del Contratto, cesseranno gli obblighi delle Parti ai sensi del Contratto, ad eccezione degli obblighi indicati nel presente Accordo.<br>
2.5	Con l'accettazione e la stipulazione del presente Accordo, le Parti dichiarano di non avere pretese reciproche o altre rivendicazioni ai sensi del Contratto.<br>
       2.6        L’accettazione del presente Accordo da parte del Cliente è costituita dall’apposizione del segno di spunta “V” nel campo “HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DELL’ACCORDO”, sul sito web dell’Operatore turistico alla pagina: https://www.russiantour.com/ita/accordo?payment_id=m<?php echo $manualPayment['order']['id']; ?> nel sistema di informazione e telecomunicazione «Internet». L’apposizione da parte del Cliente del segno di spunta “V” nel campo indicato costituisce il consenso del Cliente alle condizioni del Contratto, l’accettazione da parte del Cliente delle condizioni del presente Accordo.<br>
 <p class="uk-text-center uk-text-uppercase"><strong> 3	Dati delle Parti Operatore turistico Cliente</strong></p>
  
 
<table>
<tbody>
<tr>
<td>
<p>Denominazione completa</p>
</td>
<td>
<p>Società a responsabilità limitata <?php echo $name; ?><?php echo $name; ?></p>
</td>
</tr>
<tr>
<td>
<p>Denominazione abbreviata</p>
</td>
<td>
<p><?php echo $name; ?></p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo (sede legale)</p>
</td>
<td>
<p><?php echo $adres; ?></p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo postale</p>
</td>
<td>
<p><?php echo $adres_po; ?></p>
</td>
</tr>
<tr>
<td>
<p>Numero di registro</p>
</td>
<td>
<p>MBT <?php echo $mbt; ?></p>
</td>
</tr>
<tr>
<td>
<p>Telefono / fax</p>
</td>
<td>
<p>+7 (812) 6470690</p>
</td>
</tr>
<tr>
<td>
<p>Posta elettronica / Sito</p>
</td>
<td>
<p><?php echo $email; ?> </p>
<p><a href="http://www.russiantour.com/">www.russiantour.com</a></p>
</td>
</tr>
</tbody>
</table>
<p>2. Informazioni sull’organizzazione che fornisce al Tour Operator la garanzia finanziaria della responsabilità del Tour Operator:</p>
<table>
<tbody>
<tr>
<td>
<p>Tipo di garanzia finanziaria della</p>
<p>Responsabilità del tour operator</p>
</td>
<td>
<p>Assicurazione di responsabilità civile dei tour operator - turismo interno</p>
<p>ed internazionale in ingresso</p>
</td>
</tr>
<tr>
<td>
<p>Importo della garanzia finanziaria</p>
</td>
<td>
<p>500000 rubli</p>
</td>
</tr>
<tr>
<td>
<p>Data e termine di validità del contratto di assicurazione della responsabilità del Tour operator o della garanzia bancaria</p>
</td>
<td>
<p><?php echo $mbt_data; ?></p>
</td>
</tr>
<tr>
<td>
<p>Denominazione dell’organizzazione che fornisce la garanzia finanziaria della responsabilità del tour operator</p>
</td>
<td>
<p>Liberty Insurance Ltd.</p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo (sede legale)</p>
</td>
<td>
<p>196044 San Pietroburgo, Moskovsky Pr. 79A</p>
</td>
</tr>
<tr>
<td>
<p>Indirizzo postale</p>
</td>
<td>
<p>196044 San Pietroburgo, Moskovsky Pr. 79A</p>
</td>
</tr>
<tr>
<td>
<p>Sito</p>
</td>
<td>
<p><a href="http://www.liberty24.ru/">www.liberty24.ru</a></p>
</td>
</tr>
<tr>
<td>
<p>E-mail</p>
</td>
<td>
<p><a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a></p>
</td>
</tr>

 <tr>
 <td colspan="2">
Tour Operator: <?php echo $ooo; ?>
<img src="<?php echo $pech_url; ?>" style="height: auto;width: 400px;" >
 


</td>
</tr>

 <tr>
<td colspan="2">


<?php if (isset($manualPayment['clients'][1]) && $manualPayment['clients'][1]['nome'] == '')  { ?>
   <p class="uk-text-center ">Dati dei turisti:</p>
   Gruppo  <b>(<?php echo $manualPayment['order']['clientcount'];?>)</b> persone<br>
<?php } ?>


       
<?php  foreach ($manualPayment['clients'] as $n=>$client) {
    if ($n==1 && $client['nome'] == '')
        break;
    ?>
		 <p class="uk-text-center "> Turista № : <?php  echo ($n+1);?> </p>
Dati del turista: <br>
Cognome, nome: <b> <?php  echo  $client['cognome']; ?>  <?php  echo  $client['nome']; ?>  </b> <br>
<?php echo (($client['birthday']=='1970-01-01')?'':'Data di nascita: '.date('d/m/Y',strtotime($client['birthday'])).'<br/>') ;?>
Sesso:<b>  <?php   if(($client['sex']) == 'm'){ echo 'Maschio';}else{ echo 'Femmina';};  ?>     </b> <br>
<?php echo (($client['numero_di_passaporto']=='')?'':'Passaporto: '.$client['numero_di_passaporto'].'<br/>') ;?>
Cittadinanza/nazionalità:<b> Italia</b><br>
Pacchetto turistico:<b>   <?php echo $manualPayment['trip']['name_ita'];?>   </b><br>
Citta da visitare:<b> <?php echo $manualPayment['tour']->citys ;?></b><br>

 <?php echo (($manualPayment['order']['date_from']=='0000-00-00')?'':' Data di inizio del tour: '.date('d/m/Y',strtotime($manualPayment['order']['date_from'])).'<br/>') ;?>
<?php echo (($manualPayment['order']['date_to']=='0000-00-00')?'':'  Data di termine del tour: '.date('d/m/Y',strtotime($manualPayment['order']['date_to'])).'<br/>') ;?>
Biglietti aerei  (Si/No): <b>No</b>  <br>
Biglietti ferroviari (Si/No):<b> No</b><br>
  
  <?php   } ?>
  
  <?php echo (($manualPayment['order']['cancellation_ita']=='')?'':'<br>CONDIZIONI DI CANCELLAZIONE:<br> '.$manualPayment['order']['cancellation_ita'].'<br/>') ;?>
 

</td>
</tr>
</tbody>
</table>
</td>
</tr>
</div>
</div>
</div>
 
 