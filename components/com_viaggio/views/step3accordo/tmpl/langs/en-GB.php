<ul id="tab-content" class="tm-tab-content uk-switcher">
    <li class="uk-active">

        <br><br>

        <div class="uk-grid uk-grid-collapse">
            <div class="uk-width-medium-1-1">
                <div class="uk-panel-box-secondary uk-text-center uk-text-bold uk-text-uppercase">  Prima di effettuare il pagamento leggere le condizioni generali di contratto.</div>



                <div ><div class="uk-float-left">
                        <label class="uk-margin-small-top">
                            <input  type="checkbox" id="ho_registrazione_1" onclick="if (jQuery('#ho_registrazione_1:checked').length) jQuery('.Inviare').attr('disabled',false); else jQuery('.Inviare').attr('disabled',true);">
                        </label> </div><div style="padding-top: 7px;" ><b>
                            <a data-uk-modal="{target:'#modal11'}" href=" " >&nbsp;&nbsp; HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL  </a></b></div></div>




                <div id="modal11" class="uk-modal" aria-hidden="true" style="display: none; overflow-y: scroll;">
                    <div class="uk-modal-dialog  uk-text-justify uk-modal-dialog-large">
                        <a href="" class="uk-modal-close uk-close"></a>
                        <table>
                            <tbody>
                            <tr>
                                <td >
                                    <p><strong>ДОГОВОР-ОФЕРТА</strong></p>
                                    <p><strong>о реализации туристского продукта (визовой поддержки) № ______</strong></p>
                                </td>
                                <td >
                                    <p><strong>CONTRATTO-OFFERTA</strong></p>
                                    <p><strong>Di realizzazione di un Prodotto turistico </strong></p>
                                    <p><strong>(Assistenza visto) </strong><strong>№ ______</strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p>Россия, Санкт-Петербург</p>
                                </td>
                                <td >
                                    <p>Russia, San Pietroburgo</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p>&laquo;____&raquo; __________ 2018 г.</p>
                                </td>
                                <td >
                                    <p>&nbsp;&ldquo;____&rdquo;__________ 2018</p>
                                </td>
                            </tr>
                            <tr>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td >
                                    <p>Общество с ограниченной ответственностью &laquo;Международная Компания &laquo;Русский Тур&raquo; (ООО &laquo;Международная Компания &laquo;Русский Тур &raquo;) , Российская Федерация, именуемое в дальнейшем &laquo;Туроператор&raquo;, в лице Генерального директора Черемшенко Ольги Николаевны, действующей на основании Устава, с одной стороны,</p>
                                </td>
                                <td >
                                    <p>La Societ&agrave; a responsabilit&agrave; limitata Russian Tour International Ltd. di seguito denominata &ldquo;Tour Operator&rdquo;, nella persona del Direttore Generale Cheremshenko Olga Nikolaevna, agente in base allo Statuto, da una parte,</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p>и фамилию, имя, паспорт, страну, телефон, электронную почту именуемый (ая) в дальнейшем &laquo;Заказчик&raquo;, с другой стороны, вместе и по отдельности именуемые &laquo;Стороны&raquo;, заключили настоящий Договор о нижеследующем:</p>
                                </td>
                                <td >
                                    <p>E cognome, nome, passaporto, paese, numero di telefono, indirizzo e-mail di seguito denominato(a) &ldquo;Committente&rdquo;, dall&rsquo;altra parte, unitamente e singolarmente denominati le &ldquo;Parti&rdquo;, hanno stipulato il presente Contratto in merito a quanto segue:</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p>Настоящий Договор-оферта (далее &ndash; &laquo;Договор&raquo;, &laquo;Договор-оферта&raquo;) является письменным предложением (Офертой) Туроператора заключить Договор, направляемое Заказчику в соответствии со ст. 432-444 ГК РФ.</p>
                                    <p>Договор заключается путем полного и безоговорочного принятия (акцепта) оферты Заказчиком в порядке, установленном п. 3 ст. 438 ГК РФ, и является подтверждением соблюдения письменной формы договора в соответствии с п. 3 ст. 434 ГК РФ.</p>
                                    <p>Текст настоящего Договора-оферты расположен по адресу: <a href="https://www.visto-russia.com/ita/contratto">https://www.visto-russia.com/ita/contratto</a>  </p>
                                </td>
                                <td >
                                    <p>Il presente Contratto-offerta (di seguito &ldquo;Contratto&rdquo;, &ldquo;Contratto-offerta&rdquo;) &egrave; una proposta scritta (Offerta) del Tour Operator di concludere un Contratto indirizzato al Committente ai sensi degli artt. 432-444 del Codice Civile della Federazione Russa.</p>
                                    <p>Il Contratto viene stipulato tramite accettazione totale e incondizionata dell&rsquo;offerta da parte del Committente nelle modalit&agrave; stabilite al c. 3 art. 438 del Codice Civile della Federazione Russa ed &egrave; conferma del rispetto della forma scritta del contratto ai sensi del c. 3 art. 434 del Codice Civile della Federazione Russa.</p>
                                    <p>Il testo del presente Contratto-offerta &egrave; disponibile all&rsquo;indirizzo: <a href="https://www.visto-russia.com/ita/contratto">https://www.visto-russia.com/ita/contratto</a> </p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>Терминология:</strong></p>
                                    <p>Заказчик туристского продукта (Заказчик) - турист или иное лицо, заказывающее туристский продукт от имени туриста, в том числе законный представитель несовершеннолетнего туриста;</p>
                                    <p><strong>Сайт</strong> &ndash; интернет-сайт <strong><a href="http://www.visto-russia.com">www.visto-russia.com</a></strong>, с помощью которого Заказчик осуществляет действия по заказу туристского продукта (действия, направленные на заключение настоящего Договора, в том числе действия по выбору услуг с помощью программных средств сайта и оформлению заявки);</p>
                                    <p>Оферта &ndash; письменное предложение Туроператора заключить Договор;</p>
                                    <p>Акцепт оферты - полное и безоговорочное принятие Заказчиком оферты путем осуществления действий, указанных в оферте;&nbsp;</p>
                                    <p>Туристская деятельность - туроператорская и турагентская деятельность, а также иная деятельность по организации путешествий;</p>
                                    <p>Туристский продукт - комплекс услуг по перевозке и размещению, оказываемых за общую цену (независимо от включения в общую цену стоимости экскурсионного обслуживания и (или) других услуг) по договору о реализации туристского продукта;</p>
                                    <p>Туроператорская деятельность - деятельность по формированию, продвижению и реализации туристского продукта, осуществляемая юридическим лицом (далее - Туроператор).</p>
                                </td>
                                <td >
                                    <p><strong>Terminologia:</strong></p>
                                    <p>Committente del Prodotto turistico (Committente): il turista o una terza persona che ha prenotato un Prodotto turistico a nome del turista, incluso il rappresentante legale di un turista minorenne;</p>
                                    <p><strong>Sito:</strong> sito internet <strong><a href="http://www.visto-russia.com">www.visto-russia.com</a></strong><strong>, </strong>con l&rsquo;aiuto del quale il Committente svolge azioni per l&rsquo;ordine di un prodotto turistico (azioni volte alla stipula del presente Contratto, incluse le azioni per la scelta dei servizi con l&rsquo;aiuto di software del sito e redazione della richiesta);</p>
                                    <p>Offerta: proposta scritta del Tour Operator di stipulare un Contratto;</p>
                                    <p>Accettazione dell&rsquo;offerta: accettazione totale e incondizionata da parte del Committente dell&rsquo;offerta tramite compimento delle azioni indicate nell&rsquo;offerta;</p>
                                    <p>Attivit&agrave; turistica: attivit&agrave; del Tour Operator e dell&rsquo;agenzia turistica, nonch&eacute; altra attivit&agrave; relativa all&rsquo;organizzazione di viaggi;</p>
                                    <p>Prodotto turistico: insieme di servizi per il trasporto e l&rsquo;alloggio, forniti a un prezzo complessivo (indipendentemente dall&rsquo;inserimento nel prezzo generale del costo di escursioni e/o altri servizi) su contratto per la realizzazione di un Prodotto turistico;</p>
                                    <p>Attivit&agrave; del Tour Operator: attivit&agrave; per la formazione, promozione e realizzazione di un Prodotto turistico svolta da una persona giuridica (di seguito: Tour Operator).</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>1. ПРЕДМЕТ ДОГОВОРА</strong></p>
                                    <p>1.1. В соответствии с Договором Туроператор обязуется обеспечить оказание Заказчику услуг визовой поддержки, входящей в Туристский продукт (Приложение № 1 к Договору) (далее - Туристский продукт), а Заказчик обязуется оплатить Туристский продукт.</p>
                                    <p>1.2. Сведения о Заказчике в объеме, необходимом для исполнения Договора, указаны в заявке на реализацию туристского продукта.</p>
                                    <p>1.3. Туроператор предоставляет Заказчику следующие услуги:</p>
                                    <p>1.3.1. Визовая поддержка;</p>
                                    <p>1.3.2. Оформление страхового медицинского полиса (в случае необходимости).</p>
                                    <p>1.4. Настоящий Договор является публичной офертой.</p>
                                    <p>1.5. Акцептом настоящего Договора-оферты со стороны Заказчика является проставление отметки &ldquo;V&rdquo; в графе &ldquo;HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL&rdquo; расположенной по адресу: <strong><a href="https://www.visto-russia.com/ita/contratto">https://www.visto-russia.com/ita/contratto</a></strong><strong>. </strong>Проставление Заказчиком отметки &ldquo;V&rdquo; в указанной графе обозначает согласие Заказчика с условиями Договора, принятие Заказчиком условий настоящего Договора.</p>
                                </td>
                                <td >
                                    <p><strong>1. OGGETTO DEL CONTRATTO</strong></p>
                                    <p>1.1. In accordo al Contratto, il Tour Operator &egrave; tenuto a garantire la prestazione al Committente dei servizi di assistenza per l&rsquo;ottenimento di visti consolari, facenti parte del Prodotto turistico (Allegato № 1 al Contratto) (di seguito: Prodotto turistico), e il Committente &egrave; tenuto a pagare il Prodotto turistico.</p>
                                    <p>1.2. Le informazioni sul Committente nella misura necessaria all&rsquo;esecuzione del Contratto sono indicate nella richiesta di realizzazione di un Prodotto turistico.</p>
                                    <p>1.3. Il Tour Operator fornisce al Committente i seguenti servizi:</p>
                                    <p>1.3.1. Assistenza per l&rsquo;ottenimento del visto consolare;</p>
                                    <p>1.3.2. Stipula della polizza medica assicurativa (se necessario).</p>
                                    <p>1.4. Il presente Contratto costituisce un&rsquo;offerta pubblica.</p>
                                    <p>1.5. L&rsquo;accettazione del presente Contratto-offerta da parte del Committente &egrave; costituita dall&rsquo;apposizione del segno di spunta &ldquo;V&rdquo; nel campo HO LETTO ED ACCETTO I TERMINI E LE CONDIZIONI DI SERVIZIO DELLA RUSSIAN TOUR INTERNATIONAL&rdquo;all&rsquo;indirizzo: <strong><a href="https://www.visto-russia.com/ita/contratto">https://www.visto-russia.com/ita/contratto</a></strong></p>
                                    <p>L&rsquo;apposizione da parte del Committente del segno di spunta &ldquo;V&rdquo; nel campo indicato costituisce il consenso del Committente alle condizioni del Contratto, l&rsquo;accettazione da parte del Committente delle condizioni del presente Contratto.</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>2. ЦЕНА ТУРИСТСКОГО ПРОДУКТА. ПОРЯДОК ОПЛАТЫ</strong></p>
                                    <p>2.1. Общая цена Туристского продукта, приобретаемого по Договору (стоимость услуг), определяется на основании заявки на реализацию туристского продукта и указывается в инвойсе. Инвойс является неотъемлемой частью Договора.</p>
                                    <p>2.2. Оплата осуществляется Заказчиком в следующем порядке:</p>
                                    <p>банковской картой с использованием услуги интернет-эквайринг, доступ к которой предоставляется на Сайте.</p>
                                    <p>2.3. Заказчик обязан оплатить услуги Туроператора в течение 1 день с момента заключения Договора.</p>
                                    <p>2.4. Расходы, связанные с использованием банковской карты, осуществлением оплаты в банке Заказчика относятся на счет Заказчика.</p>
                                    <p>2.5. Стоимость услуг указывается в инвойсе в Евро.</p>
                                    <p>2.6 Валюта платежа: российский рубль. Оплата производится в российских рублях по курсу ЦБ РФ на день платежа.</p>
                                    <p>2.7. Датой оплаты Заказчиком услуг, оказываемых Туроператором по Договору, является дата зачисления денежных средств на банковский счет Туроператора.</p>
                                    <p>2.8. Инвойс с момента его полной оплаты Заказчиком подтверждает оказание услуг Туроператором по соответствующей Заявке на реализацию туристского продукта и признается Сторонами в качестве акта выполненных работ.</p>
                                    <p>&nbsp;</p>
                                </td>
                                <td >
                                    <p><strong>2.</strong><strong>PREZZO DEL PRODOTTO TURISTICO. MODALIT&Agrave; DI PAGAMENTO</strong></p>
                                    <p>2.1. Il prezzo complessivo del Prodotto turistico acquistato con Contratto (costo del servizio) &egrave; determinato in base alla richiesta di realizzazione di un Prodotto turistico ed &egrave; indicato nella fattura. La fattura costituisce parte integrante del Contratto.</p>
                                    <p>2.2. Il pagamento deve essere effettuato dal Committente nelle seguenti modalit&agrave;:</p>
                                    <p>Con carta di credito/debito bancaria attraverso l&rsquo;utilizzo dei servizi di transazione internet-acquiring, il cui accesso &egrave; fornito sul Sito.</p>
                                    <p>2.3. Il Committente &egrave; tenuto a pagare i servizi del Tour Operator entro 1 giorno dalla stipula del Contratto.</p>
                                    <p>2.4. Le spese relative all&rsquo;utilizzo della carta bancaria per effettuare il pagamento nella banca del Committente sono a carico del Committente.</p>
                                    <p>2.5. Il costo dei servizi &egrave; indicato nella fattura in Euro.</p>
                                    <p>2.6. Valuta del pagamento: rublo russo.</p>
                                    <p>Il pagamento viene effettuato in rubli russi al tasso di cambio della Banca Centrale della Federazione Russa nel giorno del pagamento.</p>
                                    <p>2.7. La data del pagamento da parte del Committente dei servizi prestati dal Tour Operator su Contratto &egrave; la data di versamento del denaro sul conto corrente del Tour Operator.</p>
                                    <p>2.8. La fattura dal momento del suo pagamento totale da parte del Committente conferma la prestazione dei servizi da parte del Tour Operator secondo la relativa Richiesta per la realizzazione di un prodotto turistico ed &egrave; riconosciuta dalle Parti come atto di esecuzione dei lavori.</p>
                                </td>
                            </tr>
                            <tr>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>3. ВЗАИМОДЕЙСТВИЕ СТОРОН</strong></p>
                                    <p>3.1. Туроператор обязан:</p>
                                    <p>- предоставить Заказчику достоверную информацию о потребительских свойствах Туристского продукта, а также информацию, предусмотренную Заявкой на реализацию туристского продукта;</p>
                                    <p>- передать Заказчику оформленные визовые документы по мере их готовности;</p>
                                    <p>- принять необходимые меры по обеспечению безопасности персональных данных Заказчика, в том числе при их обработке и использовании;</p>
                                    <p>- оказать Заказчику все услуги, входящие в Туристский продукт, самостоятельно или с привлечением третьих лиц, на которых туроператором возлагается исполнение части или всех его обязательств перед Заказчиком и (или) туристом (в случае если Заказчик заказывает Туроператору туристский продукт от имени туриста, а также является законным представителем несовершеннолетнего туриста).</p>
                                    <p>3.2. Туроператор вправе:</p>
                                    <p>- не приступать к оказанию услуг по Договору до полной оплаты Заказчиком заказанных услуг.</p>
                                    <p>3.3. Заказчик обязан:</p>
                                    <p>- оплатить Туристский продукт в соответствии с Договором;</p>
                                    <p>- довести до туриста условия Договора, иную информацию, указанную в Договоре и приложениях к нему, а также передать ему документы, полученные от Туроператора для совершения туристом путешествия;</p>
                                    <p>- предоставить Туроператору свои контактные данные, а также контактные данные туриста, необходимые для оперативной связи, а также оформления Туристского продукта;</p>
                                    <p>- предоставить Туроператору документы и сведения, необходимые для исполнения Договора, в соответствии с требованиями, предъявленными Туроператором (необходимые требования к документам и сведениям размещены на Сайте либо доводятся до сведения Заказчика дополнительно);</p>
                                    <p>- проверить полученную от Туроператора визу.</p>
                                    <p>3.4. Заказчик вправе:</p>
                                    <p>- получить копию свидетельства о внесении сведений о Туроператоре в реестр;</p>
                                    <p>- получить оформленные визовые документы.</p>
                                    <p>3.5. Стороны несут ответственность за неисполнение или ненадлежащее исполнение своих обязательств в соответствии с законодательством Российской Федерации;</p>
                                    <p>3.6. Туроператор не несет ответственность:</p>
                                    <p>- за действия посольств (консульств) иностранных государств, иных организаций, за исключением организаций, которые привлечены Туроператором для оказания услуг, входящих в Туристский продукт, в том числе за отказ иностранного посольства (консульства) в выдаче (задержке) въездных виз туристам по маршруту путешествия, если в иностранное посольство (консульство) Туроператором либо непосредственно Заказчиком в установленные сроки были представлены все необходимые документы;</p>
                                    <p>- за отказ туристам в выезде/въезде при прохождении паспортного пограничного или таможенного контроля, либо применение к Заказчику органами, осуществляющими пограничный или таможенный контроль, штрафных санкций по причинам, не связанным с выполнением Туроператором своих обязательств по Договору.</p>
                                    <p>3.7. Заказчик вправе предъявить претензии к полученным в результате оказания Туроператором визовым документам в день их получения. По истечении указанного срока претензии к визовым документам Туроператором не принимаются.</p>
                                    <p>3.8. Заключением настоящего Договора Заказчик подтверждает, что ознакомлен со всеми Правилами, размещенным на Сайте <strong><a href="http://www.visto-russia.com">www.visto-russia.com</a></strong></p>
                                </td>
                                <td >
                                    <p><strong>3. INTERAZIONE TRA LE PARTI</strong></p>
                                    <p>3.1. Il Tour operator &egrave; tenuto a:</p>
                                    <p>- fornire al Committente informazioni attendibili sulle caratteristiche di consumo del Prodotto turistico, nonch&eacute; le informazioni previste dalla Richiesta di realizzazione di un Prodotto turistico;</p>
                                    <p>- consegnare al Committente i documenti per il visto man mano che sono pronti;</p>
                                    <p>- intraprendere le misure necessarie per garantire la sicurezza dei dati personali del Committente, anche durante il loro trattamento e utilizzo;</p>
                                    <p>- prestare al Committente tutti i servizi che fanno parte del Prodotto turistico, autonomamente o coinvolgendo terze persone alle quali il Tour operator affida l&rsquo;esecuzione di parte o di tutti gli obblighi nei confronti del Committente e/o del turista (nel caso in cui il Committente ordini al Tour Operator un Prodotto turistico a nome del turista, oppure sia il legale rappresentante di un turista minorenne).</p>
                                    <p>3.2. Il Tour Operator ha il diritto di:</p>
                                    <p>- non avviare la prestazione dei servizi secondo il Contratto prima del pagamento totale dei servizi prenotati da parte del Committente.</p>
                                    <p>3.3. Il Committente &egrave; tenuto a:</p>
                                    <p>- pagare il Prodotto turistico ai sensi del Contratto;</p>
                                    <p>- far comprendere al turista le condizioni del Contratto, altre informazioni indicate nel Contratto e relativi allegati, a trasmettergli i documenti ricevuti dal Tour Operator per il compimento del viaggio del turista;</p>
                                    <p>- fornire al Tour Operator i propri riferimenti, i riferimenti del turista, necessari per un contatto operativo e per la preparazione del Prodotto turistico;</p>
                                    <p>- fornire al Tour operator i documenti e le informazioni necessarie all&rsquo;esecuzione del Contratto, ai sensi delle richieste presentate dal Tour operator (richieste necessarie per i documenti e le informazioni pubblicate sul Sito oppure portate a conoscenza del Committente in altra sede).</p>
                                    <p>- controllare il visto ricevuto dal Tour Operator.</p>
                                    <p>3.4. Il Committente ha il diritto di:</p>
                                    <p>- ottenere una copia dell&rsquo;atto di inserimento delle informazioni del Tour Operator nel registro;</p>
                                    <p>- ottenere i documenti per il visto.</p>
                                    <p>3.5. Le Parti sono responsabili per la mancata esecuzione o l&rsquo;esecuzione inappropriata dei propri obblighi ai sensi della legislazione della Federazione Russa.</p>
                                    <p>3.5. Il Tour Operator non &egrave; responsabile per:</p>
                                    <p>- le azioni delle Ambasciate (Consolati) di stati esteri, di altre organizzazioni, ad eccezione delle organizzazioni coinvolte dal Tour Operator per la prestazione dei servizi che fanno parte del Prodotto turistico, incluso il rifiuto dell&rsquo;Ambasciata (Consolato) straniero al rilascio (ritardo) del visto di ingresso ai turisti per il loro percorso di viaggio qualora il Tour Operator o direttamente il Committente abbiano presentato nei tempi previsti all&rsquo;Ambasciata (Consolato) straniero tutti i documenti necessari.</p>
                                    <p>- per il rifiuto ai turisti di uscita/ingresso al controllo passaporti o al controllo doganale o per l&rsquo;applicazione di sanzioni al Committente da parte degli organi del controllo passaporti e del controllo doganale per motivi non relativi allo svolgimento da parte del Tour Operator dei propri obblighi in base al Contratto;</p>
                                    <p>3.7. Il Committente ha il diritto di avanzare reclami per i documenti del visto ricevuti in conseguenza delle azioni del Tour Operator nel giorno in cui li riceve. Al termine del periodo indicato, i reclami per i documenti del visto non saranno accettati dal Tour Operator.</p>
                                    <p>3.8. Con la firma del presente Contratto, il Committente conferma di aver preso visione di tutte le norme e le modalit&agrave; pubblicate sul Sito <strong><a href="http://www.visto-russia.com">www.visto-russia.com</a></strong></p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>4. ПРЕТЕНЗИИ. ПОРЯДОК РАЗРЕШЕНИЯ СПОРОВ</strong></p>
                                    <p>4.1. Претензии в связи с нарушением условий договора о реализации туристского продукта предъявляются Заказчиком Туроператору в порядке и на условиях, которые предусмотрены законодательством Российской Федерации.</p>
                                    <p>4.2. Претензии к качеству Туристского продукта предъявляются Туроператору в письменной форме в течение 20 дней с даты окончания действия договора о реализации туристского продукта и подлежат рассмотрению в течение 10 дней с даты получения претензий.</p>
                                    <p>4.3. Споры, связанные с настоящим Договором, подлежат рассмотрению в суде в соответствии с положениями действующего законодательства РФ.</p>
                                </td>
                                <td >
                                    <p><strong>4. RECLAMI. MODALIT&Agrave; DI RISOLUZIONE DELLE CONTROVERSIE</strong></p>
                                    <p>4.1. I reclami relativi alla violazione delle condizioni del contratto di realizzazione del Prodotto turistico sono da presentarsi dal Committente al Tour Operator nelle modalit&agrave; e alle condizioni previste dalla legislazione della Federazione Russa.</p>
                                    <p>4.2. I reclami relativi alla qualit&agrave; del Prodotto turistico fornito dal Tour Operator sono da presentarsi in forma scritta entro 20 giorni dalla data di termine della validit&agrave; del contratto di realizzazione del Prodotto turistico e sono soggetti all&rsquo;esame entro 10 giorni dalla data di ricevimento dei reclami.</p>
                                    <p>4.3. Le controversie relative al presente Contratto sono soggette all&rsquo;esame della corte ai sensi delle disposizioni della vigente legislazione della Federazione Russa.</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>5. ОТВЕТСТВЕННОСТЬ</strong></p>
                                    <p>5.1. Туроператор не несет ответственность за следующие обстоятельства:</p>
                                    <p>- загранпаспорт Заказчика (туриста) не принимается консульским учреждением для оформления визы по причине его повреждения, изношенности, отсутствия необходимых печатей; а также по причине того, что до истечения срока действия загранпаспорта осталось менее 6 месяцев;</p>
                                    <p>- Заказчик предоставил неверные, недостоверные, ошибочные, неполные данные при заполнении Заявки на реализацию туристского продукта;</p>
                                    <p>- отказ в оформлении и выдаче визы по усмотрению консульского учреждения по причинам, не зависящим от Туроператора;</p>
                                    <p>- отказ в оформлении и выдаче визы по причине неправильного заполнения Заказчиком электронной анкеты, наличия ошибок в представленных Заказчиком документах, предоставлении неполного комплекта документов и др.;</p>
                                    <p>- задержка в выдаче визы по причине внепланового закрытия консульского учреждения для третьих лиц, в том числе представителей туристских, туроператорских компаний; внезапного изменения графика работы визового отдела консульского учреждения;</p>
                                    <p>- утрата документов по вине почтовых курьеров (служб); осуществление доставки по неверному адресу; задержка доставки по вине почтовых курьеров (служб);</p>
                                    <p>&nbsp;- выдача визы с ошибочной датой въезда, или датой, не совпадающей с датой, определенной консульским учреждением;</p>
                                    <p>- при прохождении пограничного контроля у Заказчика (туриста) возникли сложности с сотрудниками пограничной службы, в результате чего Заказчику (туристу) отказано во въезде на территорию Российской Федерации;</p>
                                    <p>- за невыполнение Заказчиком рекомендаций Туроператора, указанных в п. 7.7. настоящего Договора.</p>
                                    <p>5.2. В случае отказа в выдаче визы, произошедшего по вине Туроператора, последний обязан возместить в полном объеме затраты Заказчика на услуги Туроператора по оказанию визовой поддержки в рамках настоящего Договора.</p>
                                    <p>5.3. Стороны освобождаются от ответственности за частичное или полное невыполнение обязательств по Договору, если это неисполнение является следствием наступления обстоятельств непреодолимой силы, то есть возникших в результате чрезвычайных и непредотвратимых при данных условиях обстоятельств, которые Стороны не могли ни предвидеть, ни предотвратить разумными мерами.</p>
                                    <p>Если данные обстоятельства будут продолжаться более 14 (четырнадцати) календарных дней, каждая из Сторон вправе отказаться от исполнения обязательств по Договору, и в этом случае ни одна из Сторон не будет иметь права на возмещение другой Стороной возможных убытков по основаниям непреодолимой силы.</p>
                                </td>
                                <td >
                                    <p><strong>5. RESPONSABILIT&Agrave;</strong></p>
                                    <p>5.1. Il Tour Operator non &egrave; responsabile per le seguenti circostanze:</p>
                                    <p>- il passaporto del Committente (turista) non viene accettato dagli enti consolari per il rilascio del visto perch&eacute; danneggiato, consumato, privo dei timbri necessari; nonch&eacute; perch&eacute; alla scadenza dello stesso mancano meno di 6 mesi;</p>
                                    <p>- il Committente ha fornito dati falsi, incorretti, errati, incompleti nella compilazione della Richiesta per la realizzazione del prodotto turistico;</p>
                                    <p>- rifiuto di redazione e rilascio del visto a discrezione dell&rsquo;ente consolare per motivi indipendenti dal Tour Operator;</p>
                                    <p>- rifiuto di redazione e rilascio del visto per motivi di scorretta compilazione da parte del Committente del modulo elettronico, presenza di errori nei documenti forniti dal Committente, fornitura di un set di documenti incompleto, etc.;</p>
                                    <p>- ritardo nel rilascio del visto per motivi di chiusura improvvisa dell&rsquo;ente consolare a terzi, inclusi i rappresentanti delle societ&agrave; turistiche, dei tour operator; modifiche improvvise all&rsquo;orario di apertura dell&rsquo;ufficio visti dell&rsquo;ente consolare;</p>
                                    <p>- perdita di documenti per colpa dei corrieri (servizi) postali; consegna a un indirizzo errato; ritardo della consegna per colpa dei corrieri (servizi) postali;</p>
                                    <p>- rilascio del visto con data di ingresso errata o con data non corrispondente alla data indicata, da parte dell&rsquo;ente consolare;</p>
                                    <p>- durante il controllo di frontiera il Committente (turista) ha difficolt&agrave; con i dipendenti del servizio di frontiera, in conseguenza delle quali al Committente (turista) viene rifiutato l&rsquo;ingresso nella Federazione Russa;</p>
                                    <p>- per il mancato rispetto da parte del Committente delle raccomandazioni del Tour Operator indicate al punto 7.7 del Contratto.</p>
                                    <p>5.2. In caso di rifiuto al rilascio del visto per colpa di errori imputabili al Tour Operator, quest&rsquo;ultimo &egrave; tenuto a rimborsare completamente al Committente le spese sostenute per i servizi prestati da parte del Tour Operator per l&rsquo;ottenimento del visto nell&rsquo;ambito del presente Contratto.</p>
                                    <p>5.3. Le Parti sono esonerate dalla responsabilit&agrave; per il mancato adempimento parziale o totale degli obblighi del Contratto, se tale mancato adempimento &egrave; conseguenza di circostanze di forza maggiore, ovvero di circostanze sorte per condizioni d&rsquo;emergenza e imprevedibili, che le Parti non avrebbero potuto prevedere, n&eacute; prevenire con misure ragionevoli.</p>
                                    <p>Se tali circostanze dovessero prolungarsi per pi&ugrave; di 14 (quattordici) giorni di calendario, ognuna delle Parti avr&agrave; il diritto di rifiutare l&rsquo;adempimento degli obblighi del Contratto e in tal caso nessuna delle Parti avr&agrave; il diritto alla compensazione dall&rsquo;altra Parte delle possibili perdite motivate da cause di forza maggiore.</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>6. СРОК ДЕЙСТВИЯ ДОГОВОРА, ПОРЯДОК ИЗМЕНЕНИЯ И РАСТОРЖЕНИЯ</strong><strong>ДОГОВОРА</strong></p>
                                    <p>6.1. Договор вступает в силу с даты акцепта Заказчиком его условий в соответствии с п. 1.5. Договора и действует до исполнения сторонами своих обязательств, но не позднее 31 декабря 2018 г.</p>
                                    <p>6.2. Договор может быть изменен или расторгнут в случаях и порядке, предусмотренном законодательством Российской Федерации, в том числе по соглашению Сторон, оформленному в письменной форме.</p>
                                    <p>Любые изменения в Туристский продукт, иные условия Заявки на реализацию туристского продукта допускаются только по письменному дополнительному соглашению Сторон. Письменная форма изменений в Заявку на реализацию туристского продукта считается также соблюденной при согласовании таких изменений Сторонами по электронной почте. Для этих целей со стороны Туроператора используется следующей адрес электронной почты: <a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
                                    <p>Со стороны Заказчика используется следующей адрес электронной почты _______________.</p>
                                </td>
                                <td >
                                    <p><strong>6. TEMPISTICHE DI VALIDIT&Agrave; DEL CONTRATTO, MODALIT&Agrave; DI MODIFICHE E SCIOGLIMENTO DEL CONTRATTO</strong></p>
                                    <p>6.1. Il Contratto entra in vigore dalla data di accettazione da parte del Committente delle sue condizioni ai sensi del p.1.7 del Contratto ed &egrave; valido fino all&rsquo;esecuzione degli obblighi delle Parti, ma non oltre il 31 dicembre 2018.</p>
                                    <p>6.2. Il Contratto pu&ograve; essere modificato o sciolto nei casi e nelle modalit&agrave; previste dalla legislazione della Federazione Russa &egrave; su accordo delle Parti, se tale accordo viene redatto in forma scritta.</p>
                                    <p>Qualsiasi modifica al Prodotto turistico, altre condizioni della Richiesta per la realizzazione di un prodotto turistico sono ammesse solo su accordo scritto aggiuntivo delle Parti.</p>
                                    <p>La forma scritta delle modifiche della Richiesta per la realizzazione di un prodotto turistico &egrave; considerata rispettata anche in caso di accordo di tali modifiche tra le Parti per posta elettronica. A questo scopo da parte del Tour Operatoк &egrave; utilizzato il seguente indirizzo di posta elettronica: <a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
                                    <p>Da parte del Committente viene utilizzato il seguente indirizzo di posta elettronica _____________.</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>7. ЗАКЛЮЧИТЕЛЬНЫЕ ПОЛОЖЕНИЯ </strong></p>
                                    <p>7.1. Сведения о заключении в пользу туристов договора добровольного страхования, условиями которого предусмотрена обязанность страховщика осуществить оплату и (или) возместить расходы на оплату медицинской помощи в экстренной и неотложной формах, оказанной туристу на территории страны временного пребывания при наступлении страхового случая в связи с получением травмы, отравлением, внезапным острым заболеванием или обострением хронического заболевания, включая медицинскую эвакуацию туриста в стране временного пребывания и из страны временного пребывания в страну постоянного проживания, и (или) возвращения тела (останков) туриста из страны временного пребывания в страну постоянного проживания в соответствии с требованиями законодательства Российской Федерации и страны временного пребывания указаны в Заявке на реализацию туристского продукта.</p>
                                    <p>7.2. Заказчик предоставляет согласие, а также подтверждает, что в соответствии с требованиями Федерального закона РФ № 152-ФЗ от 27.07.2006 г. &laquo;О персональных данных&raquo; им получено согласие от всех туристов, указанных в <a href="consultantplus://offline/ref=4CB53F90A2C485ED2D567EFA69F11E0C645E22A260FB1F24ADFF65DB0D21E95A24974CFAEDF19AF5H0jEN">Приложении № 1</a> к Договору, на обработку и передачу своих персональных данных и персональных данных лиц, указываемых в заявке на реализацию туристского продукта, Туроператору и третьим лицам для исполнения Договора (в том числе для оформления виз).</p>
                                    <p>7.3. В случае, если Заказчик осуществляет заказ Туристского продукта в интересах туриста, Заказчик подтверждает, что обладает необходимыми полномочиями для представления интересов туриста в отношениях с Туроператором.</p>
                                    <p>7.4. Все приложения, а также изменения (дополнения) к Договору являются его неотъемлемой частью.</p>
                                    <p>7.5. Во всем ином, что не урегулировано Договором, Стороны руководствуются правом Российской Федерации.</p>
                                    <p>7.6. Местом оказании услуги является территория Итальянской Республики.</p>
                                    <p>7.7. При обращении в Генеральное консульство Российской Федерации в Милане по вопросу получения визы в случае, если желаемый срок пребывания в стране превышает 14 дней, как правило, требуется предоставить дополнительные документы: документы, подтверждающие бронирование отелей, счета из отелей или&nbsp;систем бронирования, квитанции о 100% оплате услуг. Если Заказчиком не забронировано место пребывания заранее на весь срок пребывания в стране назначения и не собран самостоятельно весь пакет документов, не рекомендуется подавать документы в визовый отдел Генерального консульства Российской Федерации в Милане. Если Заказчиком будет принято решение о подаче заявления на выдачу визы в описываемой выше ситуации в отсутствие указанных документов, Туроператор не несет ответственности за возможные неблагоприятные последствия для Заказчика, в том числе за возможный отказ в выдаче визы. В данном случае услуги по Договору будут считаться выполненными Туроператором в полном объеме. Обязанности по выплате каких-либо компенсаций в пользу Заказчика не возникает, равно как не возникает у Туроператора обязанности повторной подачи документов в это же консульское учреждение либо любое иное.</p>
                                    <p>7.8. Для несовершеннолетних, планирующих поездку в Российскую Федерацию, без сопровождения обоих родителей&nbsp;дополнительно необходимы следующие документы: свидетельство о рождении с данными о родителях, свидетельство о семейном положении, копия документа, удостоверяющего личность родителя или родителей, заявление о согласии родителя (родителей) несовершеннолетнего на посещение Российской Федерации без его (их) сопровождения.</p>
                                    <p>7.9. В случае, если невозможно получить визу в стандартные сроки из-за итальянских или российских праздничных дней либо в случае, если географическое расположение Заказчика не позволяет доставить документы почтовым курьером (службой) до указанной в Заявке даты вылета, произведенная оплата подлежит возврату на банковскую карту/банковский счет Заказчика не позднее трех дней с даты оплаты.&nbsp;Заказчик вправе, изменив дату получения визы, заключить новый Договор на новых условиях. Заказчик также вправе воспользоваться возможностью срочного или срочного в течение дня оформления визы.</p>
                                    <p>7.10. В случае, если Заказчик в Заявке на реализацию туристского продукта откажется от Полного оформления путем проставления в соответствующем в графе &laquo;Нет&raquo; соответствующей метки, для получения визы Заказчик должен лично явиться в одно из консульских учреждений. Прием производится по предварительной записи с согласованием времени и способа подачи заявки на получение визы. Въезд на территорию Российской Федерации только при наличии приглашения и страховки не допускается. Письмо-приглашение должно содержать информацию о бронировании отеля, если бронирование было выполнено, также необходимо будет указать информацию о лице, осуществившем бронирование и быть готовым, что подобная информация будет проверяться.&nbsp;</p>
                                    <p>В Консульском отделе Посольства Российской Федерации в Итальянской Республике в Риме и в Генеральном консульстве Российской Федерации в Милане для оформления визовых документов принимаются только оригиналы страховых полисов. Для посещения указанных консульских учреждений Заказчику будет необходимо иметь при себе оригинал страхового полиса.&nbsp;</p>
                                    <p>В Генеральном консульстве Российской Федерации в Генуе и в Генеральном консульстве Российской Федерации в Палермо копии страховых полисов принимаются. Копии страховых полисов направляются Заказчику по электронной почте не позднее дня, следующего за днем оплаты услуг Туроператора по Договору.</p>
                                    <p>7.11. Заключением настоящего Договора Заказчик подтверждает, что он полностью ознакомлен Туроператором со всеми условиями и требованиями консульских учреждений, правилами въезда на территорию Российской Федерации, нахождения и выезда с ее территории, и они ему понятны. Риск совершения тех или действий, выбор того или иного варианта оказания услуги возлагается на Заказчика.</p>
                                    <p>7.12. Со стороны Туроператора для исполнения настоящего Договора, в том числе для направления какой-либо информации в рамках Договора, используется исключительно адрес электронной почты: <a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
                                    <p>7.13. Со стороны Заказчика для исполнения настоящего Договора, в том числе для направления какой-либо информации, получения информации в рамках Договора, используется исключительно адрес электронной почты: <a href="mailto:visto@russiantour.com">__________________,</a> указанный Заказчиком при оформлении онлайн-запроса на Сайте.</p>
                                </td>
                                <td >
                                    <p><strong>7. DISPOSIZIONI CONCLUSIVE</strong></p>
                                    <p>7.1. Le informazioni sulla eventuale stipula a favore dei turisti del contratto di assicurazione volontaria, tra le cui condizioni vi &egrave; l&rsquo;obbligo dell&rsquo;assicuratore di pagare e/o rimborsare le spese di pagamento dell&rsquo;assistenza medica urgente e d&rsquo;emergenza prestata al turista nel territorio del paese di soggiorno temporaneo al momento dell&rsquo;evento assicurato relativamente alla ricezione di un trauma, all&rsquo;intossicazione, a una improvvisa malattia grave o al peggioramento di una malattia cronica, inclusa l&rsquo;evacuazione medica del turista nel paese di soggiorno temporaneo e dal paese di soggiorno temporaneo nel paese di residenza e/o il rimpatrio della salma (resti) del turista dal paese di soggiorno temporaneo nel paese di residenza ai sensi dei dettami della legislazione della Federazione Russa e del paese di soggiorno temporaneo, sono indicate nella Richiesta di realizzazione del Prodotto turistico.</p>
                                    <p>7.2. Il Committente fornisce il consenso e conferma altres&igrave; che, ai sensi dei requisiti della Legge Federale della Federazione Russa N&deg;152-FZ del 27.07.2006 &ldquo;Sui dati personali&rdquo;, ha ottenuto, da parte di tutti i turisti indicati nell&rsquo;Allegato № 1 al Contratto, il consenso al trattamento e alla trasmissione dei propri dati personali e dei dati personali delle persone indicate nella richiesta di realizzazione di un prodotto turistico, al Tour Operator e a terze persone per l&rsquo;esecuzione del Contratto (incluso l&rsquo;ottenimento del visto).</p>
                                    <p>7.3. Nel caso in cui il Committente effettui l&rsquo;ordine di un Prodotto turistico negli interessi di un turista, il Committente conferma di possedere i potere necessari alla rappresentanza degli interessi del turista nei confronti del Tour Operator.</p>
                                    <p>7.4. Tutti gli allegati, nonch&eacute; le modifiche (integrazioni) al Contratto ne costituiscono parte integrante.</p>
                                    <p>7.5. Per tutto quanto non regolamentato dal Contratto, le Parti si atterranno al diritto della Federazione Russa.</p>
                                    <p>7.6. Il luogo di prestazione dei servizi &egrave; il territorio della Repubblica Italiana.</p>
                                    <p>7.7. In caso di richiesta al Consolato Generale della Federazione Russa a Milano per questioni di ottenimento del visto nel caso in cui il periodo di soggiorno nel paese superi i 14 giorni, di norma, occorre presentare ulteriori documenti a conferma della prenotazione alberghiera, le fatture degli hotel o dei sistemi di prenotazione, ricevute di pagamento del 100% dei servizi. Se il Committente non ha prenotato per tempo un alloggio per tutta la durata del soggiorno nel paese di destinazione e non ha raccolto autonomamente tutto il set di documenti necessari, si sconsiglia di presentare i documenti all&rsquo;ufficio visti del Consolato Generale della Federazione Russa a Milano. Se il Committente prender&agrave; la decisione di presentare la richiesta di rilascio del visto nel caso sopra descritto in assenza dei documenti indicati, il Tour Operator non &egrave; responsabile delle possibili negative conseguenze per il Committente, incluso il possibile rifiuto di rilascio del visto. In tal caso i servizi del Contratto saranno considerati forniti completamente da parte del Tour Operator. L&rsquo;obbligo per il pagamento di qualsiasi rimborso a favore del Committente non sorge, cos&igrave; come non sorge l&rsquo;obbligo per il Tour Operator di ripresentare i documenti allo stesso ente consolare o ad un altro.</p>
                                    <p>7.8. Per i minorenni che prevedono un viaggio nella Federazione Russa non accompagnati da entrambi i genitori, sono altres&igrave; necessari i seguenti documenti: atto di nascita riportante i dati dei genitori, certificato dello stato di famiglia, copia di un documento di identit&agrave; del genitore o dei genitori, dichiarazione di consenso del genitore (dei genitori) del minorenne per la visita della Federazione Russa senza la sua (loro) presenza.</p>
                                    <p>7.9. Nel caso in cui sia impossibile ottenere il visto nei tempi standard a causa di festivit&agrave; italiane o russe o nel caso in cui la posizione geografica del Committente non permetta la consegna dei documenti con corriere (servizio) postale entro la data del viaggio indicata nella Richiesta, il pagamento effettuato &egrave; soggetto a rimborso sulla carta bancaria /sul conto corrente bancario del Committente entro tre giorni dalla data del pagamento. Il Committente ha il diritto, cambiando la data di ottenimento del visto, di stipulare un nuovo Contratto a nuove condizioni. Il Committente ha altres&igrave; il diritto di utilizzare la possibilit&agrave; di rilascio del visto urgente o urgente in giornata.</p>
                                    <p>7.10. Nel caso in cui il Committente nella Richiesta di realizzazione di un prodotto turistico rifiuti la Redazione completa tramite apposizione del segno di spunta alla voce relativa alla parola &ldquo;No&rdquo;, per l&rsquo;ottenimento del visto il Committente dovr&agrave; recarsi personalmente presso uno degli enti consolari. Il ricevimento avverr&agrave; su appuntamento con scelta dell&rsquo;orario e delle modalit&agrave; di consegna della richiesta di visto. L&rsquo;ingresso nel territorio della Federazione Russa con possesso solo dell&rsquo;invito e dell&rsquo;assicurazione non &egrave; concesso. La lettera d&rsquo;invito deve contenere le informazioni sulla prenotazione dell&rsquo;hotel, se la prenotazione &egrave; stata effettuata, nonch&eacute; sar&agrave; necessario indicare le informazioni sulla persona che ha effettuato la prenotazione ed essere consci del fatto che tali informazioni verranno verificate.</p>
                                    <p>Nell&rsquo;ufficio consolare dell&rsquo;Ambasciata della Federazione Russa nella Repubblica Italiana a Roma e nel Consolato Generale della Federazione Russa a Milano per la redazione dei documenti per il visto sono accettati solo gli originali delle polizze assicurative. Per accedere agli enti consolari indicati, il Committente necessiter&agrave; di avere con s&egrave; l&rsquo;originale della polizza assicurativa.</p>
                                    <p>Presso il Consolato generale della Federazione Russa a Genova e presso il Consolato Generale della Federazione Russa a Palermo sono accettate le copie delle polizze assicurative. Le copie delle polizze assicurative saranno spedite al Committente per posta elettronica entro il giorno successivo al pagamento dei servizi del Tour Operator in base al Contratto.&nbsp;</p>
                                    <p>7.11. Con la stipula del presente Contratto il Committente conferma di aver preso visione di tutte le condizioni e richieste degli enti consolari delle norme di ingresso nella Federazione Russa, di soggiorno e di uscita dalla stessa, e che le stesse sono chiare. Il rischio conseguente a certe azioni, di scelta di una opzione piuttosto di un&rsquo;altra per la prestazione dei servizi &egrave; a carico del Committente.</p>
                                    <p>7.12 Da parte del Tour Operator per l&rsquo;esecuzione del presente Contratto, incluso l&rsquo;invio di qualsivoglia informazione nell&rsquo;ambito del Contratto, utilizzare esclusivamente l&rsquo;indirizzo: <a href="mailto:info@russiantour.com">info@russiantour.com</a></p>
                                    <p>7.13 Da parte del Committente per l&rsquo;esecuzione del presente Contratto, incluso l&rsquo;invio di qualsivoglia informazione, il ricevimento di informazioni nell&rsquo;ambito del Contratto, utilizzare esclusivamente l&rsquo;indirizzo ______________ indicato dal Committente durante la compilazione della richiesta online sul Sito.</p>
                                </td>
                            </tr>
                            <tr>
                                <td >
                                    <p><strong>8. АДРЕСА И РЕКВИЗИТЫ СТОРОН. ИНФОРМАЦИЯ О ТУРОПЕРАТОРЕ И О ЛИЦЕ, ПРЕДОСТАВИВШЕМ ТУРОПЕРАТОРУ ФИНАНСОВОЕ ОБЕСПЕЧЕНИЕ ОТВЕТСТВЕННОСТИ</strong></p>
                                    <p>1. Сведения о Туроператоре:</p>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td  >
                                                <p>Полное наименование</p>
                                            </td>
                                            <td  >
                                                <p>Общество с ограниченной ответственностью &laquo;Международная Компания &laquo;Русский Тур&raquo;</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Сокращенное наименование</p>
                                            </td>
                                            <td  >
                                                <p><strong>ООО "Международная Компания &laquo;Русский Тур&raquo;</strong></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Адрес (место нахождения)</p>
                                            </td>
                                            <td  >
                                                <p>194044, Санкт-Петербург, Финляндский проспект, дом 4, лит. А, офис 424</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Почтовый адрес</p>
                                            </td>
                                            <td  >
                                                <p>194044, Санкт-Петербург, Финляндский проспект, дом 4, лит. А, офис 717</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Реестровый номер</p>
                                            </td>
                                            <td  >
                                                <p>MBT 012877</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Телефон/факс</p>
                                            </td>
                                            <td  >
                                                <p>+7 (812) 6470690</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Электронная почта/Сайт</p>
                                            </td>
                                            <td  >
                                                <p><a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
                                                <p><a href="http://www.visto-russia.com">www.visto-russia.com</a></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p>2. Сведения об организации, предоставившей Туроператору финансовое</p>
                                    <p>обеспечение ответственности Туроператора:</p>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td >
                                                <p>Вид финансового обеспечения ответственности туроператора</p>
                                            </td>
                                            <td  >
                                                <p>Страхования гражданской ответственности туроператора - внутренний туризм<br /> международный въездной</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Размер финансового обеспечения</p>
                                            </td>
                                            <td  >
                                                <p>500000 руб.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Дата и срок действия договора страхования ответственности туроператора или банковской гарантии</p>
                                            </td>
                                            <td  >
                                                <p>с 12/05/2017 по 11/05/2018</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Наименование организации, предоставившей финансовое обеспечение ответственности туроператора</p>
                                            </td>
                                            <td  >
                                                <p>АО "Либерти Страхование"</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Адрес (местонахождение)</p>
                                            </td>
                                            <td  >
                                                <p>196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Почтовый адрес</p>
                                            </td>
                                            <td  >
                                                <p>196044, г. Санкт-Петербург, Московский пр., д. 79А, лит. А</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Сайт</p>
                                            </td>
                                            <td  >
                                                <p><a href="http://www.liberty24.ru">www.liberty24.ru</a></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >
                                                <p>Электронная почта</p>
                                            </td>
                                            <td  >
                                                <p><a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td >
                                    <p><strong>8. INDIRIZZI E COORDINATE DELLE PARTI. INFORMAZIONI SUL TOUR OPERATOR E SOCIETA` CHE FORNISCE AL TOUR OPERATOR LA GARANZIA FINANZIARIA DELLA RESPONSABILIT&Agrave;</strong></p>
                                    <p>1. Informazioni sul Tour Operator:</p>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td  >
                                                <p>Denominazione completa</p>
                                            </td>
                                            <td >
                                                <p>Societ&agrave; a responsabilit&agrave; limitata Russian Tour International</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Denominazione abbreviata</p>
                                            </td>
                                            <td >
                                                <p>Russian Tour International Ltd.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Indirizzo (sede legale)</p>
                                            </td>
                                            <td >
                                                <p>194044, San Pietroburgo, prospekt Finljandskij, 4, edificio A, interno 424</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Indirizzo postale</p>
                                            </td>
                                            <td >
                                                <p>194044, San Pietroburgo, prospekt Finljandskij, 4, edificio A, interno 717</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Numero di registro</p>
                                            </td>
                                            <td >
                                                <p>MBT 012877</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Telefono / fax</p>
                                            </td>
                                            <td >
                                                <p>+7 (812) 6470690</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Posta elettronica / Sito</p>
                                            </td>
                                            <td >
                                                <p><a href="mailto:info@russiantour.com">visto@russiantour.com</a></p>
                                                <p><a href="http://www.visto-russia.com">www.visto-russia.com</a></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p>2. Informazioni sull&rsquo;organizzazione che fornisce al Tour Operator la garanzia finanziaria della responsabilit&agrave; del Tour Operator:</p>
                                    <table>
                                        <tbody>
                                        <tr>
                                            <td  >
                                                <p>Tipo di garanzia finanziaria della</p>
                                                <p>Responsabilit&agrave; del tour operator</p>
                                            </td>
                                            <td  >
                                                <p>Assicurazione di responsabilit&agrave; civile dei tour operator - turismo interno</p>
                                                <p>ed internazionale in ingresso</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Importo della garanzia finanziaria</p>
                                            </td>
                                            <td  >
                                                <p>500000 rubli</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Data e termine di validit&agrave; del contratto di assicurazione della responsabilit&agrave; del Tour operator o della garanzia bancaria</p>
                                            </td>
                                            <td  >
                                                <p>dal 12/05/2017 al 11/05/2018</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Denominazione dell&rsquo;organizzazione che fornisce la garanzia finanziaria della responsabilit&agrave; del tour operator</p>
                                            </td>
                                            <td  >
                                                <p>Liberty Insurance Ltd.</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Indirizzo (sede legale)</p>
                                            </td>
                                            <td  >
                                                <p>196044 San Pietroburgo, Moskovsky Pr. 79A</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Indirizzo postale</p>
                                            </td>
                                            <td  >
                                                <p>196044 San Pietroburgo, Moskovsky Pr. 79A</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>Sito</p>
                                            </td>
                                            <td  >
                                                <p><a href="http://www.liberty24.ru">www.liberty24.ru</a></p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td  >
                                                <p>E-mail</p>
                                            </td>
                                            <td  >
                                                <p><a href="mailto:cs@libertyrus.ru">cs@libertyrus.ru</a></p>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                        <table width="0">
                            <tbody>
                            <tr>
                                <td  >
                                    <p><strong>Туроператор</strong></p>
                                    <?php include ('rti_ru1.php'); ; ?>

                                    <p><strong><br /> Генеральный директор </strong></p>
                                    <p>Черемшенко Ольга Николаевна</p>
                                    <p>_____________________________</p>
                                    <p><strong>Телефон: <br /></strong>+7 (812) 6470690</p>
                                    <p><strong>Электронная</strong><strong>почта:</strong></p>
                                    <p><a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
                                </td>
                                <td >
                                    <p><strong>Tour Operator: </strong></p>
                                    <?php include ('rti_it1.php'); ; ?>
                                    <p><strong><br /> Il Direttore generale</strong></p>
                                    <p>Cheremshenko Olga Nikolaevna</p>
                                    <p>_____________________________</p>
                                    <p><strong>Telefono: </strong></p>
                                    <p>+7 (812) 6470690</p>
                                    <p><strong>E-mail:</strong></p>
                                    <p><a href="mailto:visto@russiantour.com">visto@russiantour.com</a></p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table width="0">
                            <tbody>
                            <tr>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                            </tr>
                            <tr>
                                <td  >
                                    <p>Приложение № 1 к договору-оферте</p>
                                    <p>о реализации туристского продукта (визовой поддержки) № ______от &laquo;___&raquo; ___________ 20__г.</p>
                                    <p>Россия, Санкт-Петербург</p>
                                    <p><strong>Заявка на реализацию туристского продукта </strong></p>
                                    <p>Дата заявки &laquo;____&raquo; ___________ 2018 г.</p>
                                </td>
                                <td >
                                    <p>Allegato № 1 al contratto-offerta di realizzazione di un Prodotto turistico (assistenza visto) № ______del &ldquo;___&rdquo; ___________ 20__</p>
                                    <p>Russia, San Pietroburgo</p>
                                    <p><strong>Richiesta di realizzazione di un Prodotto turistico</strong></p>
                                    <p>Data della Richiesta &ldquo;____&rdquo; ___________ 2018</p>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2"
                                <p>Категория и вид визы/ Categoria e tipo di visto</p>
                                <p>Консульское учреждение/ Ente consolare</p>
                                <p>Услуга полного оформления (да/нет) / Procedura completa (s&igrave;/no)</p>
                                Необходимость оформления медицинского страхования
                                <p>&nbsp;(да/нет) / Necessit&agrave; di richiedere l&rsquo;assicurazione medica (s&igrave;/no)</p>
                                <p>Консульский сбор/ Diritti consolari</p>
                                <p>Турист № 1: Turista № 1:</p>
                                <p>Данные о туристе: Dati del turista:</p>
                                <p>Фамилия, имя: Cognome, nome:</p>
                                <p><br /> Дата рождения: Data di nascita:</p>
                                <p><br /> Пол: Sesso:</p>
                                <p>Паспорт: Passaporto:</p>
                                <p>Гражданство/подданство: Cittadinanza/nazionalit&agrave;&agrave;:</p>
                                <p>Место жительства: Residenza:</p>
                                <p>Города посещения: Citt&agrave; da visitare:</p>
                                <p>Курьерские услуги по забору документов у Заказчика и отправке готовых документов Заказчику/ Servizi del corriere per il ritiro dei documenti presso il Committente e la spedizione dei documenti pronti al Committente</p>
                                <p>Туда-обратно (да/нет): Andata-ritorno (s&igrave;/no)</p>
                                <p>Только туда (да/нет): Solo andata (s&igrave;/no)</p>
                                <p>Только обратно (да/нет): Solo rientro (s&igrave;/no)</p>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <br>




                <div class="uk-panel uk-panel-box  uk-container-left uk-width-medium-1-2">

                    Data <?php echo $manualPayment['timeCreated']; ?> <br>

                    Nome e cognome : <?php echo $manualPayment['fio']; ?> <br>
                    Telefono: <?php echo $manualPayment['telefon']; ?><br>
                    Email: <?php echo $manualPayment['email']; ?><br>
                    Causale del pagamento: <?php echo $manualPayment['description']; ?><br>
                    Importo:  <?php echo $manualPayment['amountEuro']; ?>  <i class="uk-icon-euro"></i><br>

                </div>

                Il link per il pagamento e` attivo 24 ore. Se non effettuate il pagamento entro 24 ore    dovrete richiedere all'operatore un nuovo link.
                <br>
                <div class="uk-text-justify">
                    Il sistema di pagamento è sicuro, il nostro sito internet supporta la crittografia a 256-bit. Le informazioni inserite non verranno fornite a terzi, salvo nei casi previsti dalla legislazione della Federazione Russa. Le operazioni con carta di credito sono condotte in stretta conformità con i requisiti di sistemi di pagamento internazionali Visa Int. e MasterCard Europe Sprl. <br>
                    L’addebito viene effettuato in rubli in base al tasso di cambio ufficiale della Banca Centrale della Federazione Russa in vigore al momento del pagamento. Nel caso in cui il pagamento venga annullato dalla banca corrispondente, Russian Tour International Ltd si riserva il diritto di annullare i servizi. Barrando la casella "Ho letto ed accetto i termini e le condizioni si servizio della Russian Tour International" l'intestatario della carta di credito riconosce ed autorizza il prelievo dalla sua carta alle condizioni indicate.<br></div>

                <button class="uk-button Inviare"    onclick="location.href='<?php echo $link; ?>'" id="Inviare" disabled>PAGAMENTO CON CARTA DI CREDITO</button>

            </div>

        </div>
    </li>