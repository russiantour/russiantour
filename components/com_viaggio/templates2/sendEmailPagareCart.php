<?php
$send = true;//false

$subject = "Pagamento con carta di credito del tour prenotato con viaggio-russia";

$body = '';
include 'top.php';

$body .= '

Numero ordine '.$item->id.'<br>
<b>Tour: '.$item->tour->name.'<br></b>
Partenza: '.$item->tour->from.'<br>
Ritorno: '.$item->tour->to.'<br>
Nome e cognome:'.$item->nomecgome.'  <br/>
Telephone: '.$item->telephone.' <br/>
Importo in euro:  '.$item->valute_amount.' euro <br/>
Importo in rubli (per pagamenti con carta di credito): '.($item->amount/100).' rubli   <br><br/>

Può procedere al pagamento dell’ordine entro una giornata lavorativa, cliccando sul link a seguire:
<a href="https://www.viaggio-russia.com/profile?layout=pay&order='.$item->id.'">PAGAMENTO CON CARTA DI CREDITO</a><br><br>

Per qualsiasi chiarimento o delucidazione, è possibile contattarci all’indirizzo viaggio@russiantour.com o ai recapiti telefonici che trovate a fondo pagina.<br><br>
Cordiali saluti,<br>
Lo Staff di Viaggio-Russia 
<span style=" color: #fff; ">sendEmailPagareCart.php</span>



';

include 'bottom.php';

/*
stdClass Object
(
    [id] => 1499031986
    [subid] => 6
    [order_id] => 0c4a1302-2b46-4cd8-abfc-4ecb9737bc09
    [asset_id] => 0
    [created_by] => 748
    [currency] => 
    [orderstatus] => 
    [errorcode] => 0
    [errormessage] => 
    [amount] => 48829321
    [valute_amount] => 7060
    [valute_rate] => 67.8072
    [tour_id] => 44
    [clientcount] => 1
    [insurance] => 0
    [stell] => 3
    [nomecgome] => miska mishak
    [telephone] => 79050077888
    [email] => timach-ufa@yandex.ru
    [errorCode] => 0
    [errorMessage] => 
    [orderStatus] => 
))
*/
?>