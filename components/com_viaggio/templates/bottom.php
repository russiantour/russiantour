<?php
$body .= '</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <!-- 1 Column Text + Button : END -->
              <!-- Clear Spacer : BEGIN -->
                <tr>
                    <td height="40" style="font-size: 0; line-height: 0;">
                        &nbsp;
                    </td>
                </tr>
                <!-- Clear Spacer : END -->

             <!-- 3 Even Columns : BEGIN -->
                <tr>
                    <td bgcolor="#ffffff" align="center" height="100%" valign="top" width="100%" style="padding: 10px 0;">
                        <!--[if mso]>
                        <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                        <tr>
                        <td align="center" valign="top" width="660">
                        <![endif]-->
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" align="center" width="100%" style="max-width:660px;">
                            <tr>
                                <td align="center" valign="top" style="font-size:0;">
                                    <!--[if mso]>
                                    <table role="presentation" border="0" cellspacing="0" cellpadding="0" align="center" width="660">
                                    <tr>
                                    <td align="left" valign="top" width="220">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:33.33%; min-width:220px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 10px 10px;">
                                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                                        <tr>
                                                            <td align="center">
                                                                  <a href="https://www.russiantour.com"><img src="https://russiantour.com/images/banners/a1/p1.jpg" width="200" height="" border="0" alt="alt_text" class="center-on-narrow"   max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr >
                                                            <td  align="center"  style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-top: 10px;" class="stack-column-center">
                                                               <a href="https://www.russiantour.com/ita/tour-mosca">Tour Mosca San Pietroburgo</a><br>
                                                                <a href="https://www.russiantour.com/ita/anellodoro">Tour Anello d\'oro</a><br>
                                                                <a href="https://www.russiantour.com/ita/crocieravolga">Crociera sul Volga</a><br>
                                                                <a href="https://www.russiantour.com/ita/transiberiana">Transiberiana</a><br>
                                                                <a href="https://www.russiantour.com/ita/marnero">Tour Crimea e Mar Nero</a><br>


                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    <td align="left" valign="top" width="220">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:33.33%; min-width:220px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 10px 10px;">
                                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                                        <tr>
                                                            <td align="center" >
                                                                  <a href="https://www.russiantour.com"><img src="https://russiantour.com/images/banners/a1/p2.jpg" width="200" height="" border="0" alt="alt_text" class="center-on-narrow"   max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td  align="center"  style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-top: 10px;" class="stack-column-center">
                                                                <a href="https://www.russiantour.com/ita/soggiorno/hotel-mosca">Hotel Mosca </a><br>
                                                                <a href="https://www.russiantour.com/ita/soggiorno/hotel-sanpietroburgo">Hotel San Pietroburgo</a><br>
                                                                <a href="https://www.russiantour.com/ita/soggiorno/appartamento-mosca">Appartamento Mosca</a><br>
                                                                <a href="https://www.russiantour.com/ita/soggiorno/appartamento-sanpietroburgo">Affitto San Pietroburgo</a><br>
                                                                <a href="https://www.russiantour.com/ita/soggiorno/hotel-russia">Hotel Russia</a><br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    <td align="left" valign="top" width="220">
                                    <![endif]-->
                                    <div style="display:inline-block; margin: 0 -2px; max-width:33.33%; min-width:220px; vertical-align:top; width:100%;" class="stack-column">
                                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                                            <tr>
                                                <td style="padding: 10px 10px;">
                                                    <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="font-size: 14px;text-align: left;">
                                                        <tr>
                                                            <td align="center" >
                                                                <a href="https://www.russiantour.com"><img src="https://russiantour.com/images/banners/a1/p3.jpg" width="200" height="" border="0" alt="alt_text" class="center-on-narrow"   max-width: 200px; height: auto; background: #dddddd; font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555;"></a>
                                                            </td>
                                                        </tr>
                                                        <tr  >
                                                            <td align="center"  style="font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px; color: #555555; padding-top: 10px;" class="stack-column-center">
                                                                 <a href="https://www.visto-russia.com/ita/visto-business">Visto business Russia</a><br>
                                                                <a href="https://www.russiantour.com/ita/escursioni-mosca">Escursioni Mosca</a><br>
                                                                <a href="https://www.russiantour.com/ita/escursioni-sanpietroburgo">Escursioni S. Pietroburgo</a><br>
                                                                <a href="https://www.russiantour.com/ita/treni-russia">Biglietteria ferroviaria</a><br>
                                                                <a href="https://www.russiantour.com/ita/volo-russia">Biglietteria aerea</a><br>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!--[if mso]>
                                    </td>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                </td>
                            </tr>
                        </table>
                        <!--[if mso]>
                        </td>
                        </tr>
                        </table>
                        <![endif]-->
                    </td>
                </tr>
                <!-- 3 Even Columns : END -->




            </table>
            <!-- Email Body : END -->

            <!-- Email Footer : BEGIN -->
            <table role="presentation" cellspacing="0" cellpadding="0" border="0" align="center" width="100%" style="background-color: #fff;max-width: 680px;">
                <tr>
                                                                                            <tr>
                                                            <td style=" text-align: center;">
                                                                <a href="https://www.viaggio-russia.com"><img src="https://www.viaggio-russia.com"><img src="https://www.viaggio-russia.com/images/homepage/logo_viaggio_600.png" width="289px" height="" border="0" alt="alt_text" class="center-on-narrow" style="max-width: 289px; height: auto;   font-family: sans-serif; font-size: 15px; mso-height-rule: exactly; line-height: 20px;  "></a>
                                                            </td>
                                                        </tr>

					<td style="padding: 10px 10px;width: 100%;font-size: 12px; font-family: sans-serif; mso-height-rule: exactly; line-height:18px; text-align: center; color: #888888;">
<br>
                     <span class="mobile-link--footer">Russian Tour International Ltd. viaggio@russiantour.com<br>Finlyandskiy Pr. 4a, off.717, 194044 San Pietroburgo <br>

Tel/fax +78126470690 - Numero Verde: 800404000  <br>
Roma +390690289660  -  Palermo +390917481654 <br>
Milano +390245074837  -  Genova +390108935089<br>


</span>

                        <br><br>
                  
                    </td>
                </tr>
            </table>
            <!-- Email Footer : END -->

            <!--[if mso]>
            </td>
            </tr>
            </table>
            <![endif]-->
        </div>
    </center>
</body>
</html>
'; ?>