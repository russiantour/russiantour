<?php

$send = true;//false

$subject = "Pago recibido";

$body = '';
include 'es-ES_top.php';

$body .= '
Hemos recibido su pago de euro '.$payment->amountEuro.'  € ('.($payment->amountRub/100).' rubli), gracias! <br>
Para cualquier información adicional o aclaración, puede contactarnos por correo electrónico a info@russiantour.com<br>

Saludos cordiales
<span style=" color: #fff; ">es-ES_sendEmailAfterPayment.php</span>
';

include 'es-ES_bottom.php';

/*
stdClass Object
(
    [id] => 1499031339
    [subid] => 1
    [order_id] => b4254739-8dc2-47cd-b93b-257449385e9f
    [asset_id] => 0
    [created_by] => 747
    [currency] => 643
    [orderstatus] => 2
    [errorcode] => 0
    [errormessage] => Успешно
    [amount] => 6155538
    [valute_amount] => 890
    [valute_rate] => 67.8072
    [tour_id] => 11
    [clientcount] => 1
    [insurance] => 0
    [stell] => 3
    [nomecgome] => Kryska Lariska
    [telephone] => 79050077888
    [email] => timach-ufa@ya.ru
)
*/

?>