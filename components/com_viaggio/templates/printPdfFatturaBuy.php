<?php

require_once('/home/viaggio/web/viaggio-russia.com/public_html/tcpdf/config/tcpdf_config2.php');

require_once('/home/viaggio/web/viaggio-russia.com/public_html/tcpdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Viaggio Russia');
$pdf->SetTitle('Fattura ');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('arial', '', 10, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));



$name = 'fattura'.$item->number;

/*
stdClass Object
(
    [id] => 1496292383
    [subid] => 0
    [order_id] => 
    [asset_id] => 0
    [created_by] => 735
    [currency] => 
    [orderstatus] => created
    [errorcode] => 0
    [errormessage] => 
    [amount] => 7017662
    [valute_amount] => 1085
    [valute_rate] => 63.4107
    [tour_id] => 8
    [clientcount] => 1
    [insurance] => on
    [stell] => 3
    [telephone] => 89050077888
    [email] => timach-ufa@ya.ru
    [number] => 1496292383
	[nomecgome] =>
)
*/

$dru = '«'.date('j').'»';
$dit = '"'.date('j').'"';
$m = date('n');

//перевод месяца
switch($m){
    case 1:
        $mru = 'января';
		$mit = 'gennaio';
        break;
	case 2:
        $mru = 'февраля';
		$mit = 'febbraio';
        break;
	case 3:
        $mru = 'марта';
		$mit = 'marzo';
        break;
	case 4:
        $mru = 'апреля';
		$mit = 'aprile';
        break;
	case 5:
        $mru = 'мая';
		$mit = 'maggio';
        break;
    case 6:
        $mru = 'июня';
		$mit = 'giugno'; 
        break;
	case 7:
        $mru = 'июля';
		$mit = 'luglio';
        break;
	case 8:
        $mru = 'августа';
		$mit = 'agosto';
        break;
	case 9:
        $mru = 'сентября';
		$mit = 'settembre';
        break;
	case 10:
        $mru = 'октября';
		$mit = 'ottobre';
        break;
	case 11:
        $mru = 'ноября';
		$mit = 'novembre';
        break;
    case 12:
        $mru = 'декабря';
		$mit = 'dicembre'; 
        break;
}

$y = date('Y');

$amount = ($item->amount/100);
$rate = $item->valute_rate;//*1.02;

if($item->orderstatus > -1 AND $item->orderstatus <8){
// Set some content to print
$html = <<<EOD
<table border="1" style="padding: 3px;">
	<tr>
		<td>
			<p><b>Инвойс № $item->number от $dru $mru $y г.</b></p>

<p>по договору-оферте о реализации туристского продукта (визовой поддержки) № <b>$item->number</b> от $dru $mru $y г. (далее – Договор)</p>

<p><b>Туроператор:</b> ООО «Международная Компания «Русский Тур»</p>

<p><b>Заказчик:</b> $item->nomecgome<br>

В соответствии с Договором оплате подлежат следующие услуги</p>

<p>
<table border="1" style="padding: 5px;">
<tr>
<td>
№ п/п
</td>
<td>
Наименование услуги
</td>
<td>
Цена, Евро
</td>
<td>
Курс ЦБ РФ, Евро к рублю
</td>
<td>
Цена, руб.
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Визовая поддержка
</td>
<td>
$item->valute_amount
</td>
<td>
$rate
</td>
<td>
$amount
</td>
</tr>
<tr>
<td colspan="2">
ИТОГО к оплате
</td>
<td>
$item->valute_amount
</td>
<td>
$rate
</td>
<td>
$amount
</td>
</tr>
</table>
</p>

<p>Курс Евро к рублю устанавливается по курсу ЦБ РФ на день платежа.</p>

<p>Срок оплаты: в соответствии с условиями Договора.</p>

<p>В назначении платежа должен быть указан № инвойса и его дата.</p>

<p><b>Настоящий инвойс с момента его полной оплаты Заказчиком подтверждает оказание услуг Туроператором и признается Сторонами в качестве акта выполненных работ.</b></p>

<p>Генеральный директор ООО Международная Компания Русский Тур</p>

<p>О. Н. Черемшенко</p>
<img width="250"  src="https://www.visto-russia.com/images/pech_po.png">
		</td>
		<td>
		
		
		<p><b>Fattura № $item->number del $dit $mit $y</b></p>

<p>in base al contratto di realizzazione di un prodotto turistico (assistenza visto)  № <b>$item->number</b> del $dit $mit $y (di seguito: Contratto).</p>

<p><b>Tour operator: Russian Tour International Ltd.</b></p>

<p><b>Committente:</b> $item->nomecgome</p>

<p>Ai sensi del Contratto sono soggetti a pagamento i seguenti servizi</p>

<p>
<table border="1" style="padding: 5px;">
<tr>
<td>
№
</td>
<td>
Denominazione del servizio
</td>
<td>
Prezzo, Euro
</td>
<td>
Tasso di cambio ZB Euro/Rublo
</td>
<td>
Prezzo, Rubli
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Assistenza visto
</td>
<td>
$item->valute_amount
</td>
<td>
$rate
</td>
<td>
$amount
</td>
</tr>
<tr>
<td colspan="2">
TOTALE da pagare
</td>
<td>
$item->valute_amount
</td>
<td>
$rate
</td>
<td>
$amount
</td>
</tr>
</table>
</p>

<p>Il tasso di cambio dell'euro nei confronti del rublo russo è fissato in base al tasso ufficiale della banca centrale della Federazione Russa al giorno del pagamento.</p>

<p>Termine di pagamento: in ottemperanza alle condizioni del Contratto.</p>

<p>Nella causale del pagamento deve essere indicato il № della fattura e la data.</p>

<p><b>
La presente fattura dal momento del suo pagamento totale da parte del Committente conferma la prestazione dei servizi da parte del Tour Operator ed e riconosciuta dalle Parti come atto di esecuzione dei lavori.</b></p>

<p>Il Direttore Generale di Russian Tour International Ltd.</p>

<p>O. N. Cheremshenko</p>
<img width="250"  src="https://www.visto-russia.com/images/pech_po.png">
		
		
		
		</td>
	</tr>
</table>
EOD;
}else{
	$html = <<<EOD
<table border="1" style="padding: 3px;">
	<tr>
		<td>
			<p><b>Инвойс № $item->number от $dru $mru $y г.</b></p>

<p>по договору-оферте о реализации туристского продукта (визовой поддержки) № <b>$item->number</b> от $dru $mru $y г. (далее – Договор)</p>

<p><b>Туроператор:</b> ООО «Международная Компания «Русский Тур»</p>

<p><b>Заказчик:</b> $item->nomecgome<br>

В соответствии с Договором оплате подлежат следующие услуги</p>

<p>
<table border="1" style="padding: 5px;">
<tr>
<td>
№ п/п
</td>
<td>
Наименование услуги
</td>
<td>
Цена, Евро
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Визовая поддержка
</td>
<td>
$item->valute_amount
</td>
</tr>
<tr>
<td colspan="2">
ИТОГО к оплате
</td>
<td>
$item->valute_amount
</td>
</tr>
</table>
</p>

<p>Срок оплаты: в соответствии с условиями Договора.</p>

<p><b>Банковские реквизиты:</b></p>

<p><b>Russian Tour International Ltd.</b><br/>
Account: 40702978880000032988<br/>
JSB «ROSEVROBANK» (JSC), Moscow, Russia<br/>
IN FAVOUR OF ST. PETERSBURG PJSC Sovcombank; SWIFT: SOMRRUMM
Correspondent Bank: UBS Switzerland AG, Zurich, Switzerland; SWIFT: UBSWCHZH80A</p>

<p>В назначении платежа должен быть указан № инвойса и его дата.</p>

<p><b>Настоящий инвойс с момента его полной оплаты Заказчиком подтверждает оказание услуг Туроператором и признается Сторонами в качестве акта выполненных работ.</b></p>

<p>Генеральный директор ООО Международная Компания Русский Тур</p>

<p>О. Н. Черемшенко</p>
<img width="250"  src="https://www.visto-russia.com/images/pech_po.png">
		</td>
		<td>
		
		
		<p><b>Fattura № $item->number del $dit $mit $y</b></p>

<p>in base al contratto di realizzazione di un prodotto turistico (assistenza visto)  № <b>$item->number</b> del $dit $mit $y (di seguito: Contratto).</p>

<p><b>Tour operator: Russian Tour International Ltd.</b></p>

<p><b>Committente:</b> $item->nomecgome</p>

<p>Ai sensi del Contratto sono soggetti a pagamento i seguenti servizi</p>

<p>
<table border="1" style="padding: 5px;">
<tr>
<td>
№
</td>
<td>
Denominazione del servizio
</td>
<td>
Prezzo, Euro
</td>
</tr>
<tr>
<td>
1.
</td>
<td>
Assistenza visto
</td>
<td>
$item->valute_amount
</td>
</tr>
<tr>
<td colspan="2">
TOTALE da pagare
</td>
<td>
$item->valute_amount
</td>
</tr>
</table>
</p>

<p>Termine di pagamento: in ottemperanza alle condizioni del Contratto.</p>

<p><b>Coordinate bancarie:</b></p>

<p><b>Russian Tour International Ltd.</b><br/>
Account : 40702978880000032988<br/>
JSB «ROSEVROBANK» (JSC), Moscow, Russia
IN FAVOUR OF ST. PETERSBURG BRANCH OF ROSEVROBANK; SWIFT: COMKRUMM080
Correspondent Bank: UBS Switzerland AG, Zurich, Switzerland; SWIFT: UBSWCHZH80A</p>

<p>Nella causale del pagamento deve essere indicato il № della fattura e la data.</p>

<p><b>
La presente fattura dal momento del suo pagamento totale da parte del Committente conferma la prestazione dei servizi da parte del Tour Operator ed e riconosciuta dalle Parti come atto di esecuzione dei lavori.</b></p>

<p>Il Direttore Generale di Russian Tour International Ltd.</p>

<p>O. N. Cheremshenko</p>
<img width="250"  src="https://www.visto-russia.com/images/pech_po.png">
		
		
		
		</td>
	</tr>
</table>
EOD;
}

$pdf->writeHTML($html, true, false, true, false, '');
?>