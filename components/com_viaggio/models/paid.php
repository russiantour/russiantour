<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\Utilities\ArrayHelper;
/**
 * Viaggio model.
 *
 * @since  1.6
 */
class ViaggioModelPaid extends JModelItem
{
	public function getData()
	{
		include(JPATH_COMPONENT.'/helpers/settings.php');
		
		$item = new stdClass();
		$app = JFactory::getApplication();
		$item->order_number = $app->input->getInt('order_number', 0);
		//var_dump($item->order_number);
		$item->orderId = 0;
		
		//get order
		$item->order = ViaggioHelpersFrontend::getOrderObject($item->order_number);
		
		//get data from payment system by api
		$item->api = ViaggioHelpersFrontend::paymentApiGetOrderStatus($item->order->order_id, $settings);
		
		//echo '<pre>'.print_r($item->order ,1).'</pre>';
		//echo '<pre>'.print_r($item->api,1).'</pre>';
		
		$update = new stdClass();
		$update->id = $item->order->id;
		if( ! $item->api->ErrorCode ){
			$update->currency = $item->api->currency;
			$update->orderstatus = $item->api->OrderStatus;
		}
		$update->errorcode = $item->api->ErrorCode;
		$update->errormessage = $item->api->ErrorMessage;
		
		ViaggioHelpersFrontend::saveOrderArray($update);
		
		//уведомления
		if( ! $item->api->ErrorCode ){
            //ViaggioHelpersFrontend::makeCheck($item->order->id,$item->order->email,'+'.$item->order->telephone,$item->order->amount/100);
			//уведомление о заказе по email
            $pdf = ViaggioHelpersFrontend::sendPdf('printPdfContratto',$item);
            $lang = JFactory::getLanguage();
			ViaggioHelpersFrontend::sendEmailByTemplate($lang->getTag().'_sendEmailAfterPayment',$item->order->email,$settings, $item->order,$pdf);
				
			//уведомление о заказе по смс
			ViaggioHelpersFrontend::sendSmsByTemplate('sendSmsAfterPayment', $item->order->telephone, $settings, $item->order);
		}
		
		return $item;
	}
}