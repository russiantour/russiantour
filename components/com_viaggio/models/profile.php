<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

/**
 * Methods supporting a list of Viaggio records.
 *
 * @since  1.6
 */
class ViaggioModelProfile extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see        JController
	 * @since      1.6
	 */
	public function __construct()
	{
		parent::__construct();
	}


	/**
	 * Method to get an array of data items
	 *
	 * @return  mixed An array of data on success, false on failure.
	 */
	public function getItems()
	{
		$items = new stdClass();
		$items->user = JFactory::getUser();
		
		if($items->user->guest){
			$app = JFactory::getApplication();
			$app->redirect(JRoute::_(JURI::root().'index.php?option=com_users&view=login&return='.base64_encode($_SERVER['REQUEST_URI'])));
		}
		$db = JFactory::getDbo();
		
		//получаем список заказов
		$user_id = $items->user->id;
		$query = "SELECT * FROM `#__viaggio_orders` WHERE created_by = $user_id ORDER BY id DESC";
		$db->setQuery($query);
		$items->orders = $db->loadObjectList();
		
		//получаем списки клиентов
		foreach($items->orders as $key=>$order)
		{
			$order_id = $order->id;
			$query = "SELECT * FROM `#__viaggio_clients` WHERE order_id = $order_id";
			$db->setQuery($query);
			$items->orders[$key]->clients = $db->loadObjectList();
		}
		
		//включаем события для плагинов контента
		//Получаем параметры
		$params = JFactory::getApplication( 'site' )->getParams();	
		//Подключаем обработчик событий
		$dispatcher = JDispatcher::getInstance();
		//Подключаем плагины из группы контента
		JPluginHelper::importPlugin( 'content' );
		$article = new stdClass();
		
		//получаем описание туров
		foreach($items->orders as $key=>$order)
		{
			$item = null;
			$tour_id = $order->tour_id;
			$query = "SELECT * FROM `#__viaggio_tours` WHERE id = $tour_id";
			$db->setQuery($query);
			$item = $db->loadObject();
			
			//пропускаем через события контента onContentPrepare
			$article->text = $item->fulltext;
			//Вызываем событие onContentPrepare (подготовка контента)
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.categories', &$article, &$params ) );
			//сохраняем результат
			$item->fulltext = $article->text;
			/*
			$article->text = $item->widgetkit;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.article', &$article, &$params ) );
			$item->widgetkit = $article->text;
			*/
			$article->text = $item->tab4;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.categories', &$article, &$params ) );
			$item->tab4 = $article->text;
			
			$article->text = $item->tab2;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.categories', &$article, &$params ) );
			$item->tab2 = $article->text;
			
			$article->text = $item->tab3;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.categories', &$article, &$params ) );
			$item->tab3 = $article->text;
			
			$items->orders[$key]->tour = $item;
		}
		//ViaggioModelProfile::l();
		return $items;
	}
	
	function l($i){
		echo '<pre>'.print_r($i,1).'</pre>';
	}

}
