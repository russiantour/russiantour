<?php

/**
 * @version     1.0.0
 * @package     com_viaggio
 * @copyright   © 2020. Все права защищены.
 * @license     GNU General Public License версии 2 или более поздней; Смотрите LICENSE.txt
 * @author      Oleg <kaktus.mov@gmail.com> -
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class ViaggioModelTourscomments extends JModelList {

    /**
     * Build an SQL query to load the list data.
     *
     * @return	JDatabaseQuery
     * @since	1.6
     */
    protected function getListQuery() {
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        // Select the required fields from the table.
        $query->select(
            $this->getState(
                'list.select', 'a.*'
            )
        );
        $query->from('`#__viaggio_tours_comments` AS a');
        if ($tour_id = $this->state->get('tour_id',false))
            $query->where('a.tour_id = '.$tour_id);

        $query->where('a.state = 1');
        $query->order('a.date DESC');
        return $query;
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @param   string  $ordering   Elements order
     * @param   string  $direction  Order direction
     *
     * @return void
     *
     * @throws Exception
     *
     * @since    1.6
     */
    protected function populateState($ordering = null, $direction = null)
    {
        $app  = JFactory::getApplication();
        $list = $app->getUserState($this->context . '.list');

        $ordering  = isset($list['filter_order'])     ? $list['filter_order']     : null;
        $direction = isset($list['filter_order_Dir']) ? $list['filter_order_Dir'] : null;

        $list['limit']     = (int) JFactory::getConfig()->get('list_limit', 20);
        $list['start']     = $app->input->getInt('start', 0);
        $list['ordering']  = $ordering;
        $list['direction'] = $direction;

        $app->setUserState($this->context . '.list', $list);
        $app->input->set('list', null);

        // List state information.
        parent::populateState($ordering, $direction);

        $app = JFactory::getApplication();

        $ordering  = $app->getUserStateFromRequest($this->context . '.ordercol', 'filter_order', $ordering);
        $direction = $app->getUserStateFromRequest($this->context . '.orderdirn', 'filter_order_Dir', $ordering);

        $this->setState('list.ordering', $ordering);
        $this->setState('list.direction', $direction);

        $start = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'int');
        //$start = $app->getUserStateFromRequest($this->context . '.start', 'start', 0, 'int');
        $limit = $app->getUserStateFromRequest($this->context . '.limit', 'limit', 20, 'int');

        $this->setState('list.limit', $limit);
        $this->setState('list.start', $start);
    }
}
