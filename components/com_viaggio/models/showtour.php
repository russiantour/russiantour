<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Viaggio
 * @author     Timur Khamitov <timach-ufa@ya.ru>
 * @copyright  
 * @license    
 */
// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelitem');
jimport('joomla.event.dispatcher');

use Joomla\Utilities\ArrayHelper;
/**
 * Viaggio model.
 *
 * @since  1.6
 */
class ViaggioModelShowtour extends JModelItem
{
	public function getData()
	{

		$app = JFactory::getApplication();
		$params = $app->getParams();


		
		$id = $params->get('tour_id');
		if(!$id) $id = intval( $_REQUEST['id'] );
		
		
		
		$query = "SELECT * FROM `#__viaggio_tours` WHERE id = '$id';";
		
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$item = $db->loadObject();
		
		//get user object
		$item->user = JFactory::getUser();
		
		//set meta variables
		if(!isset($item->metadesc)) $item->metadesc = '';
		if(!isset($item->metakey)) $item->metakey = '';
		if(!isset($item->robots)) $item->robots = '';
		
		//get settings to item
		$item->statndarvisa = $params->get('statndarvisa');
		$item->urgentevisa = $params->get('urgentevisa');
		$item->sendvisa = $params->get('sendvisa');
		$item->visa7day = $params->get('visa7day');
		$item->visa1day = $params->get('visa1day');
		$item->servicecost = $params->get('servicecost');
		$item->visamongolia = $params->get('visamongolia');
		$item->visachina = $params->get('visachina');
		
		//get category params
		$query2 = "SELECT * FROM `#__categories` where id = $item->category";
		$db->setQuery($query2);
		$item2 = $db->loadObject();
		$categoryParams = json_decode($item2->params);
		$item->viaggiocalccost = $categoryParams->viaggiocalccost;
		$item->viaggio3stars = $categoryParams->viaggio3stars;
		$item->viaggio4stars = $categoryParams->viaggio4stars;
		//prepare data to js
		$search = array("result", "statndarvisa", "urgentevisa", "sendvisa", "visa7day", "visa1day", "halfboard3", "halfboard4", "inn3cost", "inn4cost", "inn3count", "inn4count", "singola3", "singola4","servicecost", "days", "nostell1", "nostell2");
		$replace   = array('$result', $item->statndarvisa, $item->urgentevisa, $item->sendvisa, $item->visa7day, $item->visa1day);
		$replaceJS   = array('result', 'item.statndarvisa', "item.urgentevisa", "item.sendvisa", "item.visa7day", "item.visa1day", "item.halfboard3", "item.halfboard4", "item.inn3cost", "item.inn4cost", "item.inn3count", "item.inn4count", "item.singola3", "item.singola4","item.servicecost", "item.days", "item.nostell1", "item.nostell2");
		$item->viaggiocalccostJS = str_replace($search, $replaceJS, $categoryParams->viaggiocalccost);
		
		//get days
		$item->days = (strtotime($item->to) - strtotime($item->from))/3600/24+1;
		
		//generate object for client js
		$item->jsItem = new stdClass();
		$item->jsItem->servicecost = $item->servicecost;
		$item->jsItem->visa7day = $item->visa7day;
		$item->jsItem->visa1day = $item->visa1day;
		$item->jsItem->statndarvisa = $item->statndarvisa;
		$item->jsItem->urgentevisa = $item->urgentevisa;
		$item->jsItem->sendvisa = $item->sendvisa;
		$item->jsItem->halfboard3 = $item->halfboard3;
		$item->jsItem->halfboard4 = $item->halfboard4;
		$item->jsItem->inn3cost = $item->inn3cost;
		$item->jsItem->inn4cost = $item->inn4cost;
		$item->jsItem->inn3count = $item->inn3count;
		$item->jsItem->inn4count = $item->inn4count;
		$item->jsItem->singola3 = $item->singola3;
		$item->jsItem->singola4 = $item->singola4;
		$item->jsItem->days = $item->days;
		$item->jsItem->nostell1 = $item->nostell1;
		$item->jsItem->nostell2 = $item->nostell2;
		
		if(true){
			//Получаем параметры
			$params = JFactory::getApplication( 'site' )->getParams();
			
			//enable content plugins
			$article = new stdClass();
			//Подключаем обработчик событий
			$dispatcher = JDispatcher::getInstance();
			//Подключаем плагины из группы контента
			JPluginHelper::importPlugin( 'content' );			
			
			$article->text = $item->fulltext;
			//Вызываем событие onContentPrepare (подготовка контента)
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.article', &$article, &$params ) );
			$item->fulltext = $article->text;
			
			$article->text = $item->widgetkit;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.article', &$article, &$params ) );
			$item->widgetkit = $article->text;
			
			$article->text = $item->tab4;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.article', &$article, &$params ) );
			$item->tab4 = $article->text;
			
			$article->text = $item->tab2;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.article', &$article, &$params ) );
			$item->tab2 = $article->text;
			
			$article->text = $item->tab3;
			$dispatcher->trigger( 'onContentPrepare', array( 'com_content.article', &$article, &$params ) );
			$item->tab3 = $article->text;
		}
		
		$item->telephone = null;
		if($item->user->id){
			//получаем номер телефона
			$item->telephone = JUserHelper::getProfile( $item->user->id )->profile['phone'];
		}
		
		
		//echo '<pre>'; var_dump( $item->viaggiocalccost ); echo '</pre>';
		
		/*
		$table = $this->getTable('tour');
		if (!$table->load($id))
		{
			// обработчик загрузки
			// используем $table->getError() для ловли ошибок
		}
		
		echo '<pre>';
		print_r($table);
		echo '</pre>';
		*/
		
		//$db->setQuery('ALTER TABLE `#__viaggio_tours` CHANGE `nostell` `nostell1` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL;');
        $app     = JFactory::getApplication();
        $pathway = $app->getPathway();
        $items   = $pathway->getPathWay();
        $item->pathway = $items;
 		return $item;
	}
}