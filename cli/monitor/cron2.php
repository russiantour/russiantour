<?php
// Локаль.
setlocale(LC_ALL, 'ru_RU.utf8');
date_default_timezone_set('Europe/Moscow');
header('Content-type: text/html; charset=utf-8');
mb_internal_encoding('UTF-8');
mb_regex_encoding('UTF-8');
mb_http_output('UTF-8');
mb_language('uni');

$email = 'admin@mail.russiantour.com';

$domains = [
'rusalia.it',
'marcotogni.it',
'dove-e-quando.it',
'expedia.it',
'alpitour.it',
'evaneos.it',
'sanpietroburgo.it',

];
$chache = array();
$mail_text = array();

function script($domain) {
    // За сколько отправлять уведомление.
    $warn = 259200; // 3 дня

foreach ($domains as $domain) {
    $date = 0;
    $zone = explode('.', $domain);
    $zone = end($zone);

    switch ($zone) {
        case 'it': $server = 'whois.nic.it'; break;
        case 'ru':
        case 'su':
        case 'рф': $server = 'whois.tcinet.ru'; break;
        case 'com':
        case 'net': $server = 'whois.verisign-grs.com'; break;
        case 'org': $server = 'whois.pir.org'; break;
    }

    $socket = fsockopen($server, 43);
    if ($socket) {
        fputs($socket, $domain . PHP_EOL);
        while (!feof($socket)) {
            $res = fgets($socket, 128);
            if (mb_stripos($res, 'Expire Date:') !== false) {
                $date = explode('Expire Date:', $res);
                $date = strtotime(trim($date[1]));
                break;
            }
            if (mb_stripos($res, 'Registry Expiry Date:') !== false) {
                $date = explode('Registry Expiry Date:', $res);
                $date = strtotime(trim($date[1]));
                break;
            }
        }
        fclose($socket);
    }

    if (!empty($date) && time() + $warn > $date) {
        $mail_text[] = $domain . ' - заканчивается ' . date('d.m.Y H:i', $date);
    } elseif (empty($date)) {
        $mail_text[] = $domain . ' - не удалось получить whois';
    }

    $chache['domains'][$domain] = $date;
}
}
$counter = 3;

do {
    $elem = array_shift($domains);
    script($elem);
    $counter--;
    if (!$counter) {
        $counter = 3;
        sleep(5);
    }
} while (count($domains));
  
// Сохранение в файл.
file_put_contents(__DIR__ . '/chache.json', json_encode($chache));
 
// Вывод данных в браузер.
echo '<pre>' . print_r($chache, true) . '</pre>';
 
// Отправка уведомления.
if (!empty($mail_text)) {
    mail(
	$email,
	'Мониторинг срока действия доменов и SSL-сертификатов', 
	implode('<br>', $mail_text), 
	"MIME-Version: 1.0\r\nContent-Type: text/html;"
    );
}




?>