<?php
/**
 * @package    Joomla.Cli
 *
 * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

/**
 * This is a CRON script which should be called from the command-line, not the
 * web. For example something like:
 * /usr/bin/php /path/to/site/cli/update_cron.php
 */
require '/home/russiantour/web/russiantour.com/public_html/cli/excel/vendor/autoload.php';

use Joomla\CMS\User\User;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
// Set flag that this is a parent file.
const _JEXEC = 1;

error_reporting(E_ALL | E_NOTICE);
ini_set('display_errors', 1);

// Load system defines
if (file_exists(dirname(__DIR__) . '/defines.php'))
{
	require_once dirname(__DIR__) . '/defines.php';
}

if (!defined('_JDEFINES'))
{
	define('JPATH_BASE', dirname(__DIR__));
	require_once JPATH_BASE . '/includes/defines.php';
}

require_once JPATH_LIBRARIES . '/import.legacy.php';
require_once JPATH_LIBRARIES . '/cms.php';

// Load the configuration
require_once JPATH_CONFIGURATION . '/configuration.php';

/**
 * This script will fetch the update information for all extensions and store
 * them in the database, speeding up your administrator.
 *
 * @since  2.5
 */
class ViaggioCron extends JApplicationCli
{
	/**
	 * Entry point for the script
	 *
	 * @return  void
	 *
	 * @since   2.5
	 */
	public function doExecute()
	{
        $prev_date = strtotime(date('Y-m-01',strtotime('+1 month',strtotime(date('Y-m-15')))));
        do {
            $prev_date = $prev_date-3600*24;
        } while (in_array(date('D',$prev_date),['Sat','Sun']));

        $isLastMonthWorkDay = (date('Y-m-d',$prev_date) == date('Y-m-d'));

        if (((isset($_SERVER['argv'][1]) && $_SERVER['argv'][1]=='noDB')) || date('D')=='Fri' || $isLastMonthWorkDay)
        {
            $users = [429,508,510,594,663,673,712,723,725];

            $mailer = JFactory::getMailer();
            $config = JFactory::getConfig();
            $sender = array(
                $config->get( 'mailfrom' ),
                $config->get( 'fromname' )
            );
            $mailer->setSender($sender);
            $mailer->isHTML(true);

            $mailer->addRecipient('admin@russiantour.com');
            $mailer->addRecipient('kaktus.mov@gmail.com');
            $mailer->addRecipient('marco@russiantour.com');

            $mailer->setSubject('Excel '.date('Y-m-d'));

            $mailer->setBody('Excel за '.date('Y-m-d'));
            foreach ($users as $user) {
                $excelPath = $this->makeExcel($user);

                $mailer->addAttachment($excelPath);
            }
            $mailer->Send();

            sendCommission();
        }

        if (!$isLastMonthWorkDay)
            return true;

	    echo "CRON TASK START ";
        echo "\n"; // Use \n is you are executing the cron from the terminal.

        $db = JFactory::getDbo();
        $db->setQuery("SELECT o.*, t.name_rus, t.name_ita FROM #__viaggio_orders o JOIN #__viaggio_trips t ON o.trip_id = t.id ORDER BY o.created_at DESC");
        $orders = $db->loadObjectList();

        $db->setQuery("SELECT * FROM #__viaggio_course WHERE date = '".date('Y-m-01')."' LIMIT 1");
        $course = $db->loadObject();
        if (isset($course->amount))
            $course = $course->amount/100;
        else
            $course = 1;

        $sum_paid_this_month = 0;
        foreach ($orders as $order)
        {
            $db->setQuery("SELECT * FROM #__viaggio_month_statistic WHERE order_id = ".$order->id." ORDER BY manualpaymant_id ASC");
            $month_statistic = $db->loadObjectList();

            $lastMonthStatisticDate = false;
            $month_statistic_array = array();
            foreach ($month_statistic as $row){
                $month_statistic_array[] = $row->manualpaymant_id;
                $lastMonthStatisticDate = $row->timeCreated;
            }

            $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = ".$order->id." and status > 0 and hidden <> 1 ORDER BY id ASC");
            $payments = $db->loadObjectList();

            if (count($payments)) {
                if ($payments[count($payments)-1]->amountEuro == 0)
                    continue;

                $total_euro = 0;
                $total_rub = 0;

                $query = "SELECT p.name_rus pname, s.name_rus sname, c.amount_euro, c.amount_rub FROM #__viaggio_orders_relations_postchanges c
                    LEFT JOIN #__viaggio_partners p on p.id = c.partner_id
                    LEFT JOIN #__viaggio_services s ON s.id = c.service_id
                    WHERE c.order_id = ".$order->id.
                    ($lastMonthStatisticDate?" AND c.created_at > '".$lastMonthStatisticDate."' ":"").
                    " ORDER BY c.id ASC";
                $db->setQuery($query);
                $changes_table_data = $db->loadObjectList();

                if (count($changes_table_data) == 0)
                {
                    $db->setQuery("SELECT p.name_rus pname, s.name_rus sname, r.amount_euro, r.amount_rub FROM #__viaggio_orders_relations r
                    JOIN #__viaggio_services s ON s.id = r.service_id
                    JOIN #__viaggio_partners p ON p.id = r.partner_id
                    WHERE r.order_id = " . $order->id . " ORDER BY p.name_rus ASC, s.name_rus ASC");
                    $partners = $db->loadObjectList();
                }
                else
                {
                    $partners = $changes_table_data;
                }

                foreach ($partners as $k=>$partner) {
                    if (count($changes_table_data) > 0 && $partner->amount_euro != 0 && $partner->amount_rub != 0)
                    {
                        $partners[$k]->amount_euro += ($partner->amount_rub/($course*100))*100;
                        $partners[$k]->amount_rub = 0;
                    }
                    $total_euro += $partner->amount_euro / 100;
                    $total_rub += $partner->amount_rub / 100;
                }

                $euroAmount = 0;
                foreach ($payments as $payment)
                {
                    $euroAmount += $payment->amountEuro;
                    if (!in_array($payment->id,$month_statistic_array))
                        $sum_paid_this_month += $payment->amountEuro;
                }

                //$acconto = ($order->valute_amount?round(($payments[count($payments)-1]->amountEuro/$order->valute_amount*100),2):0);
                //$acconto = ($order->valute_amount?round(($euroAmount/$order->valute_amount*100),2):0);
                $acconto = ($order->valute_amount?round(($sum_paid_this_month/$order->valute_amount*100),2):0);

                $paymentsForPartnersSumm = round($total_euro+$total_rub/$course,2);
                $howMuchPaidNotThisMonth = 0;
                $full_payment_costoParziale = 0;
                if (count($month_statistic))
                    foreach ($month_statistic as $row)
                    {
                        echo ''.($row->costo_parziale/100).'-----';
                        $full_payment_costoParziale += $row->costo_parziale/100;
                        $howMuchPaidNotThisMonth += $row->how_much_paid/100;
                    }

                if ($payments[count($payments)-1]->full_payment==1 && count($month_statistic) > 0)
                {
                    $full_payment_costoParziale = $row->costo_parziale/100;
                    $costoParziale = $paymentsForPartnersSumm-$full_payment_costoParziale;
                }
                else
                    $costoParziale = ($order->valute_amount?round($acconto*($total_euro+$total_rub/$course)/100,2):0);

                $total_profit = ($order->valute_amount-round($total_euro+$total_rub/$course,2));

                //$guadagno_parziale = ($euroAmount-$costoParziale-$howMuchPaidNotThisMonth);
                $guadagno_parziale = ($sum_paid_this_month-$costoParziale);

                /*$paidThisMonth = $euroAmount-$howMuchPaidNotThisMonth;
                if ($paidThisMonth != 0)
                    $profit = $guadagno_parziale/$paidThisMonth;
                else
                    $profit = 0;*/
                if ($sum_paid_this_month != 0)
                    $profit = $guadagno_parziale/$sum_paid_this_month;
                else
                    $profit = 0;

                $payment = $payments[count($payments)-1];

                $data = array();
                $data['order_id'] = $order->id;
                $data['manualpayment_id'] = $payment->id;
                $data['full_summ'] = $order->valute_amount*100;
                $data['acconto'] = $acconto*100;
                $data['curs'] = $course*100;
                $data['how_much_paid'] = $euroAmount*100;
                $data['payments_for_partners_summ'] = $paymentsForPartnersSumm*100;
                $data['costo_parziale'] = $costoParziale*100;
                $data['total_profit'] = $total_profit*100;
                $data['guadagno_parziale'] = $guadagno_parziale*100;
                $data['profit'] = $profit*100;

                /*$db->setQuery('SELECT count(*) count FROM #__viaggio_month_statistic WHERE order_id = ' . $data['order_id'] . ' AND manualpaymant_id = ' . $data['manualpayment_id']);
                $count = $db->loadObject();

                if ($count->count == 0) {*/
                    $query = "INSERT INTO #__viaggio_month_statistic (`order_id`,`manualpaymant_id`,`full_summ`,`acconto`,`curs`,`how_much_paid`,`payments_for_partners_summ`,`costo_parziale`,`total_profit`,`guadagno_parziale`,`profit`,`payments_count`) VALUES ('" . $data['order_id'] . "','" . $data['manualpayment_id'] . "','" . $data['full_summ'] . "','" . $data['acconto'] . "','" . $data['curs'] . "','" . $data['how_much_paid'] . "','" . $data['payments_for_partners_summ'] . "','" . $data['costo_parziale'] . "','" . $data['total_profit'] . "','" . $data['guadagno_parziale'] . "','" . $data['profit'] . "','".count($payments)."')";
                    $db->setQuery($query);
                    $db->execute();
                    echo $query."\n\n";
                //}
            }
        }

        $db->setQuery("UPDATE #__viaggio_month_statistic SET skip = null WHERE skip = 1");
        $db->execute();

        echo "CRON TASK END ";
        echo "\n";
	}

	public function makeExcel($user_id){
        /*$spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Hello');
        $sheet->setCellValue('A4', '123');
        $sheet->setCellValue('A3', "hello\nworld\n!!!");
        $sheet->mergeCells('A1:A2');

        $sheet->setCellValue('A355',"hello\nworld\n!!!");
        $sheet->getStyle('A355')->getAlignment()->setWrapText(true);
        $sheet->setCellValue('A356',"hello\nworld\n!!!");

        $writer = new Xlsx($spreadsheet);
        $writer->save('/home/russiantour/web/russiantour.com/public_html/hello world.xlsx');return;*/

        //return array('countRows'=>$countRows,'total_summm'=>$total_summm,'total_costo_paraliziale'=>$total_costo_paraliziale);
        $return = $this->makeTable($user_id);
echo 0;
        $rowsNum = $return['countRows'];
        $total_summm = $return['total_summm'];
        $total_costo_paraliziale = $return['total_costo_paraliziale'];
        $profito = $return['profito'];
        $zp = $return['zp'];
        $zp2 = $return['zp2'];
echo 1;
        $user = User::getInstance($user_id);
        echo '_1';
        $user_name = $user->get('name');
        echo '_2';
        $user_name = str_replace(' ','_',$user_name);
        echo '_4';
        $user_name = str_replace('\'','_',$user_name);
        echo '_5';
        $user_name = str_replace('.','_',$user_name);
        echo '_6';

        $q = new \PhpOffice\PhpSpreadsheet\Reader\Html();
        $spreadsheet = $q->load('/home/russiantour/web/russiantour.com/public_html/images/nalog/user/'.date('mY').'_'.$user_id.'.html');
echo 2;
        //$spreadsheet->getActiveSheet()->getStyle('A2')->getAlignment()->setWrapText(true);
        //$spreadsheet->getActiveSheet()->getStyle('A5')->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('A2:A8'.$spreadsheet->getActiveSheet()->getHighestRow()) ->getAlignment()->setWrapText(true);
        $spreadsheet->getActiveSheet()->getStyle('E2:E8'.$spreadsheet->getActiveSheet()->getHighestRow()) ->getAlignment()->setWrapText(true);


        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(40);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(32);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('L')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(16);
        $spreadsheet->getActiveSheet()->getColumnDimension('O')->setWidth(25);
echo 3;

        /*$spreadsheet->getActiveSheet()->getStyle('B3:B7')->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFFF0000');*/



        $styleArray =    [
            'font' => [
                'bold' => false,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],



            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],

            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFF0F8FF',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
echo 4;
        $spreadsheet->getActiveSheet()->getStyle('F2:F'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('H2:H'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('I2:I'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('J2:J'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('K2:K'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('L2:L'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('M2:M'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('G2:G'.$rowsNum)->applyFromArray($styleArray);
echo 5;
        $styleArray = [
            'font' => [
                'bold' => false,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],



            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],

            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFFA8072',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
echo 6;
        $spreadsheet->getActiveSheet()->getStyle('E2:E'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O2')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O4')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O6')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O8')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O10')->applyFromArray($styleArray);
echo 7;
        $styleArray = [
            'font' => [
                'bold' => false,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],



            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],

            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FF98FB98',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
echo 8;
        $spreadsheet->getActiveSheet()->getStyle('A2:A'.$rowsNum)->applyFromArray($styleArray);
echo 9;
        $styleArray = [
            'font' => [
                'bold' => false,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
                 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],



            'borders' => [
                'horizontal' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],

            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFFDF5E6',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
echo 10;
        $spreadsheet->getActiveSheet()->getStyle('B2:B'.$rowsNum)->applyFromArray($styleArray);
echo 11;
        $styleArray = [
            'font' => [
                'bold' => false,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],



            'borders' => [
                'horizontal' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,

                ],

            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
                'rotation' => 90,
                'startColor' => [
                    'argb' => 'FFFFDEAD',
                ],
                'endColor' => [
                    'argb' => 'FFFFFFFF',
                ],
            ],
        ];
echo 12;
        $spreadsheet->getActiveSheet()->getStyle('D2:D'.$rowsNum)->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('C2:C'.$rowsNum)->applyFromArray($styleArray);

        $spreadsheet->getActiveSheet()->getStyle('O1')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O3')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O5')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O7')->applyFromArray($styleArray);
        $spreadsheet->getActiveSheet()->getStyle('O9')->applyFromArray($styleArray);
echo 13;

        $spreadsheet->getActiveSheet()->getStyle('K2:K'.$rowsNum)->getAlignment()->applyFromArray( [ 'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 'textRotation' => 0, 'wrapText' => TRUE ] );

        $spreadsheet->getActiveSheet()->getStyle('J2:J'.$rowsNum)->getAlignment()->applyFromArray( [ 'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 'textRotation' => 0, 'wrapText' => TRUE ] );

        $spreadsheet->getActiveSheet()->getStyle('I2:I'.$rowsNum)->getAlignment()->applyFromArray( [ 'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER, 'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER, 'textRotation' => 0, 'wrapText' => TRUE ] );
echo 14;

        //$spreadsheet->getActiveSheet()->getStyle('C2')->getNumberFormat()->setFormatCode('#,##0.00');
        $spreadsheet->getActiveSheet()
            ->getStyle('E2:E'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('C2:C'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('H2:H'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('I2:I'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('J2:J'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('F2:F'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('G2:G'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);

        $spreadsheet->getActiveSheet()
            ->getStyle('M2:M'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_PERCENTAGE_00);
        $spreadsheet->getActiveSheet()
            ->getStyle('L2:L'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
        $spreadsheet->getActiveSheet()
            ->getStyle('K2:K'.$rowsNum)
            ->getNumberFormat()
            ->setFormatCode(PhpOffice\PhpSpreadsheet\Style\NumberFormat::FORMAT_CURRENCY_EUR_SIMPLE);
echo 15;
        ////return array('countRows'=>$countRows,'total_summm'=>$total_summm,'total_costo_paraliziale'=>$total_costo_paraliziale);
        $spreadsheet->getActiveSheet()->setCellValue('O1','TOTALE VENDITE MENSILE');
        $spreadsheet->getActiveSheet()->setCellValue('O2',$total_summm);
        $spreadsheet->getActiveSheet()->setCellValue('O3','TOTALE COSTI MENSILI');
        $spreadsheet->getActiveSheet()->setCellValue('O4',$total_costo_paraliziale);
        $spreadsheet->getActiveSheet()->setCellValue('O5','TOTALE GUADAGNI');
        $spreadsheet->getActiveSheet()->setCellValue('O6',$total_summm-$total_costo_paraliziale);
        $spreadsheet->getActiveSheet()->setCellValue('O7','% PROFITTO');
        $spreadsheet->getActiveSheet()->setCellValue('O8',$profito);
        $spreadsheet->getActiveSheet()->setCellValue('O9','ZP');
        $spreadsheet->getActiveSheet()->setCellValue('O10',$zp2);
        /*header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=$filename.xls");
        header("Content-Transfer-Encoding: binary ");*/
echo 16;
        $writer = new Xlsx($spreadsheet);
echo 17;
	    $writer->setOffice2003Compatibility(TRUE);
echo 18;
        $excelPath = '/home/russiantour/web/russiantour.com/public_html/images/nalog/user/'.date('mY').'_'.$user_name.'.xlsx';
        $writer->save($excelPath);
echo 19;
        return $excelPath;
    }

    private function makeTable($user_id)
    {
$db = JFactory::getDbo();
$created_by = '';
/*$user = JFactory::getUser();
$user_groups = JAccess::getGroupsByUser($user->get('id'));
if (in_array(8,$user_groups))
{
    if (isset($_POST['filter_user']) && intval($_POST['filter_user']))
        $created_by = " WHERE created_by = ".intval($_POST['filter_user']);
}
else
    $created_by = " WHERE created_by = ".$user->get('id');*/
$created_by = " WHERE created_by = ".$user_id;
$db->setQuery("SELECT o.*, t.name_rus, t.name_ita FROM #__viaggio_orders o JOIN #__viaggio_trips t ON o.trip_id = t.id ".$created_by." ORDER BY o.created_at DESC");
$orders = $db->loadObjectList();
$countRows = 1;
$html = '<table class="uk-table " border="1">
            <thead>
            <tr>
                <th>название тура'.date('Y-m-d H:i:s').'</th>
                <th>парнтеры </th>
                <th>оплата партнёрам</th>
                <th>в рублях</th>
                <th>курс</th>
                <th>полная сумма</th>
                <th>% ACCONTO</th>
                <th>Сколько оплатили</th>
                <th>Сумма оплат парнерам</th>
                <th>COSTO PARZIALE</th>
                <th>сколько заработали всего</th>
                <th>GUADAGNO PARZIALE</th>
                <th>Profit</th>
            </tr>
            </thead>
            <tbody>';
            $total_costo_paraliziale = 0;
            $db->setQuery("SELECT * FROM #__viaggio_course WHERE date = '".date('Y-m-01')."' LIMIT 1");
            $course = $db->loadObject();
            if (isset($course->amount))
                $course = $course->amount/100;
            else
                $course = 1;
            $total_summm = 0;
            $totale_guadagni = 0;

            foreach ($orders as $order)
            {
                $db->setQuery("SELECT * FROM #__viaggio_month_statistic WHERE order_id = ".$order->id." ORDER BY id ASC");
                $month_statistic = $db->loadObjectList();
                $month_statistic_array = array();
                $lastMonthStatisticDate = false;
                foreach ($month_statistic as $row){
                    $month_statistic_array[] = $row->manualpaymant_id;
                    $lastMonthStatisticDate = $row->timeCreated;
                }
                $where = '';
                $db->setQuery("SELECT count(*) count FROM #__viaggio_manualpayments WHERE order_id = ".$order->id." and status > 0 and hidden <> 1 ".$where." ORDER BY id ASC");
                $paymentsCount = $db->loadObject();
                if ($paymentsCount->count > 0)
                {
                    $db->setQuery("SELECT * FROM #__viaggio_manualpayments WHERE order_id = ".$order->id." and status > 0 and hidden <> 1 ORDER BY id ASC");
                    $payments = $db->loadObjectList();

                    $db->setQuery("SELECT count(*) count, sum(payments_count) pcs FROM #__viaggio_month_statistic WHERE skip is null AND manualpaymant_id <> 0 AND order_id = ".$order->id);
                    $monthStatisticCount = $db->loadObject();
                }

                $query = "SELECT p.name_rus pname, s.name_rus sname, c.amount_euro, c.amount_rub FROM #__viaggio_orders_relations_postchanges c
                    LEFT JOIN #__viaggio_partners p on p.id = c.partner_id
                    LEFT JOIN #__viaggio_services s ON s.id = c.service_id
                    WHERE c.order_id = ".$order->id.
                    ($lastMonthStatisticDate?" AND c.created_at > '".$lastMonthStatisticDate."' ":"").
                    " ORDER BY c.id ASC";
                $db->setQuery($query);
                $changes_table_data = $db->loadObjectList();

                if (($paymentsCount->count > 0 && $paymentsCount->count > $monthStatisticCount->pcs/*$monthStatisticCount->count*/) || count($changes_table_data) > 0)
                {
                    $total_euro = 0;
                    $total_rub = 0;
                    $partially_skip = false;
                    if (count($changes_table_data) == 0)
                    {
                        $db->setQuery("SELECT p.name_rus pname, s.name_rus sname, r.amount_euro, r.amount_rub FROM #__viaggio_orders_relations r
                    JOIN #__viaggio_services s ON s.id = r.service_id
                    JOIN #__viaggio_partners p ON p.id = r.partner_id
                    WHERE r.order_id = ".$order->id." ORDER BY p.name_rus ASC, s.name_rus ASC");
                        $partners = $db->loadObjectList();
                        $pcount = count($partners)+1;
                    }
                    else
                    {
                        $partially_skip = true;
                        $month_statistic = array();
                        $month_statistic_array = array();

                        $order->valute_amount = 0;

                        $partners = $changes_table_data;
                        $pcount = count($changes_table_data)+1;
                    }

                    if ($pcount < 6)
                        $pcount = 6;
                    foreach ($partners as $k=>$partner)
                    {
                        if ($partially_skip && count($changes_table_data) > 0 && $partner->amount_euro != 0 && $partner->amount_rub != 0)
                        {
                            $partners[$k]->amount_euro += ($partner->amount_rub/($course*100))*100;
                            $partners[$k]->amount_rub = 0;
                        }
                        $total_euro += $partner->amount_euro/100;
                        $total_rub += $partner->amount_rub/100;
                    }

                    $html .= '<tr>';
                    $countRows++;
                    $html .= '<td rowspan="'.$pcount.'">';
                    foreach ($payments as $payment)
                    {
                        if ($payment->full_payment == 1)
                        {
                            $html .= "<b>Это полный платёж!<b><br style=\'mso-data-placement:same-cell;\' />\n";
                        }

                    }

                    $html .= $order->id."<br style=\'mso-data-placement:same-cell;\' />\n".$order->name_rus."<br style=\'mso-data-placement:same-cell;\' />\n".$order->nomecgome."<br style=\'mso-data-placement:same-cell;\' />\n".$order->email."<br style=\'mso-data-placement:same-cell;\' />\n".$order->created_at."\n <br style=\'mso-data-placement:same-cell;\' />".$order->clientcount.' PAX ';
                    $html .= '
					   </td>';

                    $howMuchPaidText =  '<td rowspan="'.$pcount.'">';
                    $euroAmount = 0;
                    $euroAmountTotal = 0;
                    $sum_paid_this_month = 0;
                    foreach ($payments as $payment)
                    {

                        if (!in_array($payment->id,$month_statistic_array)
                            && (
                                count($month_statistic_array) < 1 ||
                                $payment->id > $month_statistic_array[count($month_statistic_array)-1]
                            ))
                        {
                            if (!$partially_skip)
                                $total_summm += $payment->amountEuro;
                            $euroAmount += $payment->amountEuro;

                            $sum_paid_this_month += $payment->amountEuro;
                        }
                        $euroAmountTotal += $payment->amountEuro;
                        /*$howMuchPaidText .='<div  data-uk-tooltip title="'.$payment->paymentID."\n";

                        $howMuchPaidText .= convertTimeCreated($payment->timeCreated)."\n";

                        if ($payment->status ==1)
                        {
                            $howMuchPaidText .= ' ' .($payment->amountRub/100).' ₽ \n';
                            $howMuchPaidText .= 'Курс: '.$payment->curs."\n";
                        }
                        $howMuchPaidText .= $payment->amountEuro   .' €</div> ';*/
                        $howMuchPaidText .= ' ';

                    }
                    if (!$partially_skip)
                        $howMuchPaidText .= $euroAmount;
                    $howMuchPaidText .= '</td>';

                    if (isset($partners[0]))
                    {
                        $html .= '<td style="background: #cccccc;"> '.$partners[0]->pname.' </td>';
                        $html .= '<td style="background: #cccccc;" > '.($partners[0]->amount_euro?($partners[0]->amount_euro/100).'':'').'</td>';
                        $html .= '<td style="background: #cccccc;">'.($partners[0]->amount_rub?($partners[0]->amount_rub/100).'':'').'</td>';

                    }
                    else
                    {
                        $html .= '<td></td>';
                        $html .= '<td></td>';
                        $html .= '<td></td>';
                    }

                    $html .= '<td rowspan="'.$pcount.'">';
                    /*if (count($month_statistic))
                        foreach ($month_statistic as $row)
                        {
                            $html .= ($row->curs/100).'<br style=\'mso-data-placement:same-cell;\' />';
                        }*/
                    $html .= $course;
                    $html .= '</td>';

                    $html .= '<td rowspan="'.$pcount.'">';
                   /* if (count($month_statistic))
                        foreach ($month_statistic as $row)
                            $html .= ($row->full_summ/100).' €<br style=\'mso-data-placement:same-cell;\' />';*/
                    $html .= $order->valute_amount.'';
                    $html .= '</td>';

                    //$acconto = ($order->valute_amount?round(($payments[count($payments)-1]->amountEuro/$order->valute_amount*100),2):0);
                    //$acconto = ($order->valute_amount?round(($euroAmount/$order->valute_amount*100),2):0);
                    $acconto = ($order->valute_amount?round(($sum_paid_this_month/$order->valute_amount*100),2):0);
                    $html .= '<td rowspan="'.$pcount.'">';
                    /*if (count($month_statistic))
                        foreach ($month_statistic as $row)
                            $html .= ''.($row->acconto/100).' <br style=\'mso-data-placement:same-cell;\' />';*/
                    $html .= ''.($acconto/100).' ';
                    $html .= '</td>';
                    $html .= $howMuchPaidText;

                    $html .= '<td rowspan="'.$pcount.'">';
                    $paymentsForPartnersSumm = round($total_euro+$total_rub/$course,2);
/*
                    if (count($month_statistic))
                        foreach ($month_statistic as $row)
                            $html .= ($row->payments_for_partners_summ/100).' €<br style=\'mso-data-placement:same-cell;\' />';*/

                    if (!$partially_skip)
                        $html .= $paymentsForPartnersSumm.'';

                    $html .= '</td>';

                    $html .= '<td rowspan="'.$pcount.'">';

                    $full_payment_costoParziale = 0;
                    if (count($month_statistic))
                        foreach ($month_statistic as $row)
                        {
                            /*$html .= ($row->costo_parziale/100).' € <br style=\'mso-data-placement:same-cell;\' />';*/
                            $full_payment_costoParziale += $row->costo_parziale/100;
                        }

                    if ($payments[count($payments)-1]->full_payment==1 && count($month_statistic) > 0)
                    {
                        $full_payment_costoParziale = $row->costo_parziale/100;
                        $costoParziale = $paymentsForPartnersSumm-$full_payment_costoParziale;
                    }
                    else
                    {
                        $costoParziale = ($order->valute_amount?round($acconto*($total_euro+$total_rub/$course)/100,2):0);

                    }
                    $total_costo_paraliziale += $costoParziale;
                    if (!$partially_skip)
                        $html .= $costoParziale.'';
                    if ($payments[count($payments)-1]->full_payment==1)
                    {
                        /*$html .= "<br style=\'mso-data-placement:same-cell;\' /><br style=\'mso-data-placement:same-cell;\' />Полный платёж "*/$full_payment_costoParziale.'';
                    }

                    $html .= ' </td>';

                    $html .= '<td rowspan="'.$pcount.'">';
/*
                    if (count($month_statistic))
                        foreach ($month_statistic as $row)
                            $html .= ($row->total_profit/100).' € <br style=\'mso-data-placement:same-cell;\' />';*/
                    $total_profit = ($order->valute_amount-round($total_euro+$total_rub/$course,2));
                    if ($partially_skip)
                        $totale_guadagni += $total_profit;
                    $html .= $total_profit. '' ;

                    $html .= '</td>';

                    $html .= '<td rowspan="'.$pcount.'">';

                    $howMuchPaidNotThisMonth = 0;
                    if (count($month_statistic))
                        foreach ($month_statistic as $row)
                        {
                            $howMuchPaidNotThisMonth += $row->how_much_paid/100;
                        }
                    $guadagno_parziale = ($sum_paid_this_month-$costoParziale);

                    if (!$partially_skip)
                        $html .= $guadagno_parziale. '';

                    $html .= '</td>';

                    $html .= '<td rowspan="'.$pcount.'">';
                   /* if (count($month_statistic))
                        foreach ($month_statistic as $row)
                            $html .= '   '.($row->profit).'  1111 <br style=\'mso-data-placement:same-cell;\' />';
                    $profit = $guadagno_parziale/$payments[count($payments)-1]->amountEuro;*/
                    $howMuchPaidNotThisMonth = 0;
                    if (count($month_statistic))
                        foreach ($month_statistic as $row)
                        {
                            //echo '<span class="uk-num">'.($row->payments_for_partners_summ/100).' €</span><hr/>';
                            $howMuchPaidNotThisMonth += $row->how_much_paid/100;
                        }

                    /*$paidThisMonth = $euroAmountTotal-$howMuchPaidNotThisMonth;

                    if ($paidThisMonth != 0)
                    {
                        $profit = $guadagno_parziale/$paidThisMonth;

                    }
                    else
                        $profit = 0;*/
                    if ($sum_paid_this_month != 0)
                    {
                        $profit = $guadagno_parziale/$sum_paid_this_month;

                    }
                    else
                        $profit = 0;

                    if (!$partially_skip)
                        $html .= round($profit,4).'  ';
                    $html .= '</td>';

                    $payment = $payments[count($payments)-1];

                    $total_profit = ($order->valute_amount-round($total_euro+$total_rub/$course,2));
                    $html .= '</tr>';
                    if ($pcount > 1)
                    {
                        $total_euro = 0;
                        $total_rub = 0;
                        foreach ($partners as $k=>$partner)
                        {
                            $total_euro += $partner->amount_euro/100;
                            $total_rub += $partner->amount_rub/100;
                            if ($k>0)
                            {
                                $html .= '<tr>';
                                $countRows++;
                                $html .= '<td> '.$partner->pname.' </td>';
                                $html .= '<td>'.($partner->amount_euro?($partner->amount_euro/100).'  ':'').'</td>';
                                $html .= '<td> '.($partner->amount_rub?($partner->amount_rub/100).'  ':'').'</td>';

                                $html .= '</tr>';
                            }
                        }
                        $countRows++;
                        $diff = (5 - count($partners));
                        if ($diff > 0)
                        {
                            for ($i=0;$i<$diff;$i++)
                            {
                                $countRows++;
                                $html .= '<tr>';
                                $html .= '<td></td>';
                                $html .= '<td></td>';
                                $html .= '<td></td>';
                                $html .= '</tr>';
                            }
                        }
                        $html .= '<tr>';
                        $html .= '<td>общее сумма</td>';
                        $html .= '<td> '.$total_euro.'    </td>';
                        $html .= '<td>  '.$total_rub.' </td>';
                        $html .= '</tr>';
                    }
                    else
                    {
                        for ($i=0;$i<6;$i++)
                        {
                            $countRows++;
                            $html .= '<tr>';
                            $html .= '<td></td>';
                            $html .= '<td></td>';
                            $html .= '<td></td>';
                            $html .= '</tr>';
                        }
                    }
                }
            }
            /*$html .= '<tr><td>Сумма всех сумм:</td><td>'.$total_summm.'</td></tr>';
            $html .= '<tr><td>Сумма costo paraliziale:</td><td>'.$total_costo_paraliziale.'</td></tr>';*/
$html .= '</tbody>
        </table>';

        echo '1-';
        $forZP = ($total_summm-$total_costo_paraliziale+$totale_guadagni)/10*$course+10000;
        if ($forZP < 50000)
            $forZP = 50000;
        echo '2-';

        $forZP2 = ($total_summm-$total_costo_paraliziale+$totale_guadagni)/10*$course+10000;
        if ($forZP2 < 50000)
            $forZP2 = 50000;
        else
            $forZP2 = 50000 + 0.87*($forZP2-50000);

        $outputArray = array(
            'countRows'=>$countRows,
            'total_summm'=>$total_summm,
            'total_costo_paraliziale'=>$total_costo_paraliziale,
            'profito'=>(($total_summm!=0)?(round(($total_summm-$total_costo_paraliziale+$totale_guadagni)/$total_summm*100,2)):0),
            'zp' => $forZP,
            'zp2' => $forZP2
        );

        /*$html = '<table class="uk-table " border="1"><tbody><tr><td>Сумма всех сумм: '.$total_summm.'</td></tr>
        <tr><td>Сумма costo paraliziale: '.$total_costo_paraliziale.'</td></tr>
        <tr><td>TOTALE GUADAGNI: '.($total_summm-$total_costo_paraliziale+$totale_guadagni).'</td></tr>
        <tr><td>% PROFITTO: '.$outputArray['profito'].'</td></tr>
        <tr><td>ЗП: '.$forZP.'</td></tr>
        <tr><td>ЗП2: '.$forZP2.'</td></tr></tbody></table>'.$html;*/

        file_put_contents('/home/russiantour/web/russiantour.com/public_html/images/nalog/user/'.date('mY').'_'.$user_id.'.html',$html);

        return $outputArray;
    }
}
function convertTimeCreated($timeCreated)
{
    $timeCreated = explode(' ',$timeCreated);
    $timeCreatedDate = explode('-',$timeCreated[0]);
    return $timeCreated[1].' '.$timeCreatedDate[2].'/'.$timeCreatedDate[1].'/'.$timeCreatedDate[0];
}
function sendCommission(){
    $data = JFactory::getDbo()->setQuery(
        "SELECT u.name, sum(mp.comissionEuro) comissionEuro  
FROM #__viaggio_manualpayments mp 
JOIN #__users u on u.id = mp.userId 
WHERE mp.payTime >= '".date('Y-m-01')." 00:00:00' 
AND mp.payTime < '".date('Y-m-01',strtotime('+1 month',strtotime(date('Y-m-15'))))."' 
GROUP BY mp.userId"
    )->loadObjectList();
    $body = '';
    foreach ($data as $row){
        $body .= $row->name.": ".$row->comissionEuro."<br/>";
    }
///////////////////////
    $mailer = JFactory::getMailer();
    $config = JFactory::getConfig();
    $sender = array(
        $config->get( 'mailfrom' ),
        $config->get( 'fromname' )
    );
    $mailer->setSender($sender);
    $mailer->isHTML(true);

    $mailer->addRecipient('admin@russiantour.com');
    $mailer->addRecipient('kaktus.mov@gmail.com');
    $mailer->addRecipient('marco@russiantour.com');

    $mailer->setSubject('Commission report '.date('Y-m-d'));

    $mailer->setBody($body);
    $mailer->Send();
}
JApplicationCli::getInstance('ViaggioCron')->execute();
