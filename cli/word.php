<?php
require '/home/russiantour/web/russiantour.com/public_html/cli/word/vendor/autoload.php';

// Creating the new document...
//$phpWord = new \PhpOffice\PhpWord\PhpWord();

$phpWord = new \PhpOffice\PhpWord\PhpWord();
/* Note: any element you append to a document must reside inside of a Section. */


use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Settings;
$document = $phpWord->loadTemplate('Template.docx'); //шаблон

// Make sure you have `dompdf/dompdf` in your composer dependencies.
Settings::setPdfRendererName(Settings::PDF_RENDERER_DOMPDF);
// Any writable directory here. It will be ignored.
Settings::setPdfRendererPath('.');

// Adding an empty Section to the document...
$section = $phpWord->addSection();
// Adding Text element to the Section having font styled by default...
$section->addText(
    '"Learn from yesterday, live for today, hope for tomorrow. '
        . 'The important thing is not to stop questioning." '

        . '(Albert Einstein)'
);



/*
 * Note: it's possible to customize font style of the Text element you add in three ways:
 * - inline;
 * - using named font style (new font style object will be implicitly created);
 * - using explicitly created font style object.
 */

// Adding Text element with font customized inline...
$section->addText(
    '"Great achievement is usually born of great sacrifice, '
        . 'and is never the result of selfishness.qwrwrqwreqwer" '
        . '(Napoleon Hill)',
    array('name' => 'Tahoma', 'size' => 10)
);

// Adding Text element with font customized using named font style...
$fontStyleName = 'oneUserDefinedStyle';
$phpWord->addFontStyle(
    $fontStyleName,
    array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
);
$section->addText(
    '"The greatest accomplishment is not in never falling, '
        . 'but in rising again after you fall." '
        . '(Vince Lombardi)',
    $fontStyleName
);

// for example manipulating simple text information ($element1 is instance of Text object)


 
$section->addTextBreak(1); // перенос строки
      $section->addText("Table with colspan and rowspan");
       
      $styleTable = array('borderSize' => 6, 'borderColor' => '999999');
      $cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center');
      $cellRowContinue = array('vMerge' => 'continue');
      $cellColSpan2 = array('gridSpan' => 2, 'valign' => 'center');
      $cellColSpan3 = array('gridSpan' => 3, 'valign' => 'center');
       
      $cellHCentered = array('align' => 'center');
      $cellVCentered = array('valign' => 'center');
 
      $phpWord->addTableStyle('Colspan Rowspan', $styleTable);
      $table = $section->addTable('Colspan Rowspan');
      $table->addRow(null, array('tblHeader' => true));
      $table->addCell(2000, $cellVCentered)->addText('A', array('bold' => true), $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('B', array('bold' => true), $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('C', array('bold' => true), $cellHCentered);
      $table->addCell(2000, $cellColSpan2)->addText('D', array('bold' => true), $cellHCentered);
       
      $table->addRow();
      $table->addCell(2000, $cellColSpan3)->addText(' colspan=3 '
              . '(need enough columns under -- one diff from html)', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('E', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('F', null, $cellHCentered);
       
      $table->addRow();
      $table->addCell(2000, $cellRowSpan)->addText('rowspan=2 '
              . '(need one null cell under)', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellRowSpan)->addText('rowspan=3 '
              . '(nedd 2 null celss under)', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
       
      $table->addRow();
      $table->addCell(null, $cellRowContinue); // 1 пустая в колонке 1
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(null, $cellRowContinue); // 1 пустая в колонке 3
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
       
       
      $table->addRow();     
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(null, $cellRowContinue);  // 2 пустая в колонке 3
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
 
       
      $table->addRow();
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
      $table->addCell(2000, $cellVCentered)->addText('Т', null, $cellHCentered);
       

// Adding Text element with font customized using explicitly created font style object...

// Saving the document as OOXML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
$objWriter->save('helloWorld.docx');

// Saving the document as ODF file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'ODText');
$objWriter->save('helloWorld.odt');

// Saving the document as HTML file...
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'HTML');
$objWriter->save('helloWorld.html');

