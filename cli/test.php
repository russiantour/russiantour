<?php

$arr = [1,2,3,4,5,6,7,8,9,10];

function script($elem) {
    echo $elem. "\n";
}

$counter = 3;

do {
    $elem = array_shift($arr);
    script($elem);
    $counter--;
    if (!$counter) {
        $counter = 3;
        sleep(5);
    }
} while (count($arr));
?>