$(function(){
    var time_slide = '100';
    /*
      $('.tablo_schedule .direction').hover(function(){
        if ($('.active', $(this)).next().hasClass('not_active')){
            $('.slide_box', $(this)).animate({
                'bottom': '-39px'
            }, time_slide, "linear", function(){
                var box = $(this);
                $('.not_active', box).clone().prependTo(box);
                $('.not_active:last', box).remove();
                box.css({'bottom': '0px'});
            });
        }
    }, function(){
        if ($('.not_active', $(this)).next().hasClass('active')){
            $('.slide_box', $(this)).animate({
                'bottom': '-39px'
            }, time_slide, "linear", function(){
                var box = $(this);
                $('.active', box).clone().prependTo(box);
                $('.active:last', box).remove();
                box.css({'bottom': '0px'});
            });
        }
    });
     */


/*
 Вариант с реализованной задержкой перед реакцией
     $('.tablo_schedule .direction').hover(function(){
        var element = $(this);
        element.addClass('hover');
        setTimeout(function(){
            var slide_box = $('.slide_box', element);
            if (element.hasClass('hover')){
                slide_box.stop().animate({
                    'bottom': '-39px'
                }, time_slide, "linear", function(){
                    slide_box.prep
                });
            }
        }, time_delay);
    }, function(){
        $(this).removeClass('hover');
        $('.slide_box', $(this)).stop().animate({
            'bottom': '0px'
        }, time_slide, "linear");
    });
 **/
    $('.tablo_schedule .direction').hover(function(){
        $('.slide_box', $(this)).stop().animate({
            'bottom': '-39px'
        }, time_slide, "linear");
    }, function(){
        $('.slide_box', $(this)).stop().animate({
            'bottom': '0px'
        }, time_slide, "linear");
    });
});