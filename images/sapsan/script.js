$(document).ready(function(){
/**
 *@see http://webactives.ru 
 **/
	//меняем назначение ссылки
	$('#registration_link').click(function(){
		lps.auth(function(){
			lps.console('login: ok', "debug");
		});
		return false;
	});
	
	$('.top_login_form .enter').click(function(event) {
  		var $form = $(event.target).closest('FORM');
		$form.submit();
		return true;
	});
	
	$('.top_login_form .enter').mouseenter(function(){
		$(this).addClass("mouseover");
	}).mouseleave(function(){
		$(this).removeClass("mouseover");
	});
	//Авто добавление класса hover
	$('.hoverable').hover(
        function(){
            $(this).addClass('hover');
            $('a',this).addClass('hover');
        },function(){
            $(this).removeClass('hover');
            $('a',this).removeClass('hover');
        }
    );
    //Нажатие на области транслируется в нажатие по ссылке
	$('.link_wrap').click(function(){
        window.location = $('a',this).attr('href');
    });
    //Нажатие интерпретируется как отправка формы
    $('.submit').click(function(){
        $(this).closest('FORM').submit();
    });

   //контакты на верхней плашке
   $('.page_top .contacts .button').not('.online').click(function(){

        $('.page_top .contacts .active .wrap').animate({
            'width': '17px'
        }, 300, "linear");

        $('.wrap', $(this)).stop().animate({
            'width': $('.inner', $(this)).width()
        }, 300, "linear");

        $('.page_top .contacts .button.active').removeClass('active');

        $(this).addClass('active');
   });

   //top_menu
   $('.page_top .top_menu LI').hover(function(){
        $(this).addClass('bordered').next().addClass('bordered');
        if ($(this).hasClass('last')) $(this).closest('UL').addClass('bordered');
   },function(){
       if ( ! $(this).hasClass('active'))
            $(this).removeClass('bordered').next().removeClass('bordered');
        if ($(this).hasClass('last')) $(this).closest('UL').removeClass('bordered');
   });

   //картинки в постах
   $('.pages_start_post #post_content IMG').not('.mceTransport IMG, .not_fancybox').each(function(){
      var img_width = parseInt($(this).attr('width'));
      var img_height = parseInt($(this).attr('height'));
          if (img_width){
            if (!img_height) img_height = img_width;
            var box_width = img_width+parseInt($(this).css('padding-left'))+parseInt($(this).css('padding-right')) + 2;
            $(this).closest('.mceImage_with_description').find('.image_description').css('width', box_width);
            var box_hight = img_height+parseInt($(this).css('padding-top'))+parseInt($(this).css('padding-bottom')) + 2 + parseInt($(this).closest('.mceImage_with_description').find('.image_description').height()) + 5;
            $(this).closest('.mceImage_with_description').width(box_width).height(box_hight);
            $(this).wrap('<a href="'+ $(this).attr('src').replace(/thumbs\/w[0-9]*h[0-9]*q[0-9]*/, 'images/') +'" rel="gallery" />');
            $(this).closest('A').append('<img class="bordered_image thumb" src="'+$(this).attr('src').replace('images/', 'thumbs/w'+img_width+'h'+img_height+'q90/')+'" />');
            $(this).hide();
          }
   });
   $('.pages_start_post #post_content A[rel="gallery"]').fancybox({
       'titlePosition' : 'inside'
   });
   $('A[rel="gallery"]').not('.pages_start_post #post_content').fancybox({
       'titlePosition' : 'inside'
   });
   

   //капча
   $('#page_footer .change_captcha').click(function(){
       $('.captcha', $(this).closest('.kod')).attr('src', '/captcha.php?'+Math.random());
	   $('#page_footer .kod INPUT').attr('value', '');
       return false;
   });

   $('#question_form .send .button').click(function(){
       var form = $(this).closest('FORM');
       $.post(
            '/main/sendData/',
            {
                name: $('INPUT[name="name"]', form).val(), 
                mail: $('INPUT[name="mail"]', form).val(), 
                text: $('TEXTAREA[name="text"]', form).val(),
                captcha: $('INPUT[name="captcha"]', form).val()
            }, 
            function(result){
                $('#page_footer .right .appear_box').html(result);
				$('#page_footer .change_captcha').click(); //резет капчи
				if (!$('#page_footer .right .appear_box .error').length){
					$('#mailSendFromSite')[0].reset();
				}
            }
        );
   });

   $('#page_footer').click(function(){
       $('.right .appear_box', $(this)).empty();
   });
   
   //Закидывание анонса в верх страницы, если есть.
   var $announces = $('.articles .announce');
   if ($announces.length == 1 && $('.page_announce').length){
	   $('.page_announce').text($announces.text());
	   $announces.hide();
   }

   //добавление класса элементу UL, если не правильно создался список
   $('UL:has(LI.blue_pin_menu)').addClass('blue_pin_menu');
});