var marva_alertFallback = false;
if (typeof console === "undefined" || typeof console.log === "undefined") {
    console = {};
    if (marva_alertFallback) {
        console.log = function(msg) {
        alert(msg);
    };
    } else {
        console.log = function(msg) {};
    }
}

(function(){

  var DomReady = window.DomReady = {};

  // Everything that has to do with properly supporting our document ready event. Brought over from the most awesome jQuery. 

  var userAgent = navigator.userAgent.toLowerCase();

  // Figure out what browser is being used
  var browser = {
      version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [])[1],
      safari: /webkit/.test(userAgent),
      opera: /opera/.test(userAgent),
      msie: (/msie/.test(userAgent)) && (!/opera/.test( userAgent )),
      mozilla: (/mozilla/.test(userAgent)) && (!/(compatible|webkit)/.test(userAgent))
  };

  var readyBound = false;
  var isReady = false;
  var readyList = [];

  // Handle when the DOM is ready
  function domReady() {
      // Make sure that the DOM is not already loaded
      if(!isReady) {
          // Remember that the DOM is ready
          isReady = true;
          if(readyList) {
              for(var fn = 0; fn < readyList.length; fn++) {
                  readyList[fn].call(window, []);
              }
              readyList = [];
          }
      }
  };

  // From Simon Willison. A safe way to fire onload w/o screwing up everyone else.
  function addLoadEvent(func) {
    var oldonload = window.onload;
    if (typeof window.onload != 'function') {
      window.onload = func;
    } else {
      window.onload = function() {
        if (oldonload) {
          oldonload();
        }
        func();
      }
    }
  };

  // does the heavy work of working through the browsers idiosyncracies (let's call them that) to hook onload.
  function bindReady() {
      if(readyBound) {
          return;
      }

      readyBound = true;

      // Mozilla, Opera (see further below for it) and webkit nightlies currently support this event
      if (document.addEventListener && !browser.opera) {
          // Use the handy event callback
          document.addEventListener("DOMContentLoaded", domReady, false);
      }

      // If IE is used and is not in a frame
      // Continually check to see if the document is ready
      if (browser.msie && window == top) (function(){
          if (isReady) return;
          try {
              // If IE is used, use the trick by Diego Perini
              // http://javascript.nwbox.com/IEContentLoaded/
              document.documentElement.doScroll("left");
          } catch(error) {
              setTimeout(arguments.callee, 0);
              return;
          }
          // and execute any waiting functions
          domReady();
      })();

      if(browser.opera) {
          document.addEventListener( "DOMContentLoaded", function () {
              if (isReady) return;
              for (var i = 0; i < document.styleSheets.length; i++)
                  if (document.styleSheets[i].disabled) {
                      setTimeout( arguments.callee, 0 );
                      return;
                  }
              // and execute any waiting functions
              domReady();
          }, false);
      }

      if(browser.safari) {
          var numStyles;
          (function(){
              if (isReady) return;
              if (document.readyState != "loaded" && document.readyState != "complete") {
                  setTimeout( arguments.callee, 0 );
                  return;
              }
              if (numStyles === undefined) {
                  var links = document.getElementsByTagName("link");
                  for (var i=0; i < links.length; i++) {
                      if(links[i].getAttribute('rel') == 'stylesheet') {
                          numStyles++;
                      }
                  }
                  var styles = document.getElementsByTagName("style");
                  numStyles += styles.length;
              }
              if (document.styleSheets.length != numStyles) {
                  setTimeout( arguments.callee, 0 );
                  return;
              }
              // and execute any waiting functions
              domReady();
          })();
      }

      // A fallback to window.onload, that will always work
      addLoadEvent(domReady);
  };

  // This is the public function that people can use to hook up ready.
  DomReady.ready = function(fn, args) {
      // Attach the listeners
      bindReady();

      // If the DOM is already ready
      if (isReady) {
          // Execute the function immediately
          fn.call(window, []);
      } else {
          // Add the function to the wait list
          readyList.push( function() { return fn.call(window, []); } );
      }
  };

  bindReady();

})();

if (typeof(marva) == 'undefined' || marva == null) {
  marva = {};
}

marva.autoload = {};
marvaCONFIG = {};

DomReady.ready(function() {
  if (true) { //typeof(marva.autoload) == 'undefined' || marva.autoload == null || marva.autoload != {}) {
    marva.autoload = new function() {
      function getUniq() {
        return new Date().getTime();
      }
      var uniq = getUniq();
      var do_tracker_flag = 1;

      getReferrer = function() {
        return escape(document.referrer);
      }

      function getLocation() {
        return escape(location.toString());
      }

      var CONFIG = {
        aspl: "rustur1",
        aspID: 47482,
        deptID: 25971,
        button_text: "",
        BASE_URL: "https://account.marva.ru",
        resource: "b7dfe284cae3b56ff7b494e45835cc10",
        THEME_USER_INITIATE_BUTTON: "https://account.marva.ru/themes/gc_client/images/initiate7button.gif",
        THEME_USER_INITIATE_BUTTON_CLOSE: "https://account.marva.ru/themes/gc_client/images/initiate_close.gif",
        max_requset_message_length: 500,
        widthWindowChat: "450",
        heightWindowChat: "360",
        enter_comment: "������� ���� ��� ������",
        tracker_refresh: 20000
      };
      marvaCONFIG = CONFIG;
      CONFIG.tracker_image = CONFIG.BASE_URL + "/image_no.php?l=" + CONFIG.aspl + "&x=" + CONFIG.aspID + "&deptid=" + CONFIG.deptID + "&unique=" + uniq + "&refer=" + getReferrer() + "&ps=" + CONFIG.resource + "&text=" + CONFIG.button_text + "&page=" + getLocation();
      CONFIG.ns = (document.layers);
      CONFIG.ie = (document.all);
      CONFIG.w3 = (document.getElementById && !ie);

      CONFIG.backtrack = 0; /* ����������� �������� */
      CONFIG.isclosed = 0;
      CONFIG.repeat = 1;
      CONFIG.timer = 20; /* �������� �������� */
      CONFIG.halt = 0;

      var ProactiveDiv; /* document.write DIV style */
      var el_main_div2; /* DOM DIV style */
      var FistStepInput; /* ������ ���� ����� ������� � ���� ������� ��� ������ */
      var FistStepDiv; /* ������ ���� ������� ��� ������ */
      var unique;
      var start_tracker = unique = uniq;
      var time_elapsed;

      var do_tracker_flag = 1;
      var refer = escape(document.referrer);
      var chat_opened;
      var initiate = chat_opened = 0;

      var ns = (document.layers);
      var ie = (document.all);
      var w3 = (document.getElementById && !ie);

      var pullimage = new Image;


      this.toggleMotion = function(flag) {
        if (flag) {
          CONFIG.halt = 1;
        } else {
          CONFIG.halt = 0;
        }
      }

      /* ������� �������� ����

      document.body.style.background = 'url(/;-)/n.gif) no-repeat';
      document.body.style.backgroundAttachment = 'scroll';
      */

      /* ����������� ��������-������������ ���������� */

      var browser_ua = navigator.userAgent.toLowerCase();
      var browser_type, tempdata;

      /* write external style here.. it won't work if we put it directly in the DIV */
      var stylesheet = document.createElement("link");
      stylesheet.setAttribute("type", "text/css");
      stylesheet.setAttribute("rel", "stylesheet");
      stylesheet.setAttribute("media", "screen");
      stylesheet.setAttribute("href", CONFIG.BASE_URL + "/css/rh.css?rnd=" + Math.floor(Math.random() * 1000000000));
      (document.getElementsByTagName("head")[0] || document.getElementsByTagName("body")[0]).appendChild(stylesheet);


      /* ***************************************** */

      this.noemptybr = function (e) {
        var key;
        var keychar;

        if (window.event) {
          key = window.event.keyCode;
        } else if (e) {
          key = e.which;
        } else {
          return true;
        }

        if (document.getElementById("premes_text").value != "") {
          if (document.getElementById("premes_text").value.length >= CONFIG.max_requset_message_length) {
            if ((key == 8) || (key == 0)) {
              return true;
            } else {
              return false;
            }
          } else {
            return true;
          }
        } else {
          if ((key == 32) || (key == 13) || (key == 10)) {
            return false;
          } else {
            return true;
          }
        }
      };

      this.check_question = function() {
        if (document.getElementById("premes_text").value == CONFIG.enter_comment){
          document.getElementById("premes_text").value = "";
        }
        var question_temp;
        if (question_temp = document.getElementById("premes_text")) {
          var question_value = question_temp.value;
          var max_rmesslength = CONFIG.max_requset_message_length;

          if (question_value.length > max_rmesslength) {
            document.getElementById("premes_text").value = question_value.substr(0, max_rmesslength);
            alert("������������ ���������� ����� ������ " + max_rmesslength + " ��������.\n��� ����� ��� �������� �� ����� �������.\n��� ����������� ��������� ������ �� ������� ����.");
          }
        }
      };

      /* ***************************************** */


                show_ad_block_init = "<div style=\"position:fixed;z-index:9999;top:0px;left:0px;\" >";
          show_ad_block_init += "<a href=\"http://www.marva.ru\"><img src=\"http://www.marva.ru/img/ad.gif\" alt=\"www.marva.ru\" border=\"0\"></a>";
          show_ad_block_init += "</div>";

          var dLink = document.getElementsByTagName("body")[0];
          var el_main_div = document.createElement("div");
          dLink.appendChild(el_main_div);
          el_main_div.className = 'class_el_main_div';
          el_main_div.setAttribute("align", "left");
          el_main_div.setAttribute("id", "ProactiveSupport4545");
          el_main_div.innerHTML = show_ad_block_init;
      
      var Browser = {
        Version: function() {
          var version = 999; // we assume a sane browser
          if (navigator.appVersion.indexOf("MSIE") != -1) {
            // bah, IE again, lets downgrade version number
            version = parseFloat(navigator.appVersion.split("MSIE")[1]);
          }
          return version;
        }
      }

      function initializeProactive() {
        if(!document.getElementById("ProactiveSupport4545")) {
          var output_init;
          var shadow_param;
          var scroll_image = new Image;
          scroll_image.onload = function(){

          };
          scroll_image.src = CONFIG.BASE_URL + "/scroll_image.php?x=" + CONFIG.aspID + "&l=" + CONFIG.aspl + "&ps=" + CONFIG.resource + "&" + uniq;

          if (CONFIG.aspID == 48033)
          {
            shadow_param = "box-shadow: 0 0 20px rgba(0,0,0,0.5); border-radius: 10px; margin-top: 27px;";
          }
          output_init = "<a href=\"JavaScript:marva.autoload.launch_support()\" OnMouseOver=\"marva.autoload.toggleMotion(1);marva.autoload.change_on(1)\" OnMouseOut=\"marva.autoload.toggleMotion(0);marva.autoload.change_on(0)\">";
          if (Browser.Version() < 7) {
            output_init += '<img src="' + CONFIG.BASE_URL + '/Utils/pngfix/blank.gif" width="368" height="217" style="z-index:9991" border="0" style="filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src=\'' + scroll_image.src + '\', sizingMethod=\'scale\')" /></a>';
          } else {
            output_init += "<img src=\"" + scroll_image.src + "\" border=\"0\" style=\"z-index:9991;"+shadow_param+"\" /></a>";
          }
          output_init += "<div align=\"left\" style=\"font-size:0;border:0px; align:left !important; position:absolute;z-index:9998;top:155px;left:24px;width:295px;margin:0;padding:0;\">\
            <textarea id=\"premes_text\" maxlength=\"200\"\
              onKeyPress=\"return marva.autoload.noemptybr(event)\"\
              onfocus=\"this.innerHTML == '" + CONFIG.enter_comment + "' ? this.value = '' : void(0); marva.autoload.check_question();\"\
              onBlur=\"this.innerHTML == '' ? this.value = '" + CONFIG.enter_comment + "' : void(0); marva.autoload.check_question();\"\
              onChange=\"marva.autoload.check_question();\"\
              onKeyDown=\"marva.autoload.check_question();\"\
              onKeyUp=\"marva.autoload.check_question();\"\
              style=\"background-color:#fff;border:0;overflow:hidden;margin:0;padding:3px;align:left !important;line-height:12px;font-family:Tahoma,Verdana;color:#292929;font-size:11px;text-decoration:none;width:295px;height:40px;\">" + CONFIG.enter_comment + "</textarea>\
            </div>\
            <div align=\"left\" style=\"font-size:0;border:0px; position:absolute;z-index:9998;top:151px;left:321px;height:53px;width:29px;margin:0;padding:0;\">\
              <a style=\"height:53px;width:29px;margin:0;padding:0;font-size:0;\"\
              href=\"JavaScript:marva.autoload.launch_support()\"\
              OnMouseOver=\"marva.autoload.toggleMotion(1);marva.autoload.change_on(1)\"\
              OnMouseOut=\"marva.autoload.toggleMotion(0);marva.autoload.change_on(0)\"><img width=\"29\" height=\"53\" hspace=\"0\" vspace=\"0\" style=\"margin:0;padding:0;\" id=\"init_on_id\" src=\"" + CONFIG.BASE_URL + "/images/initiate7buttno.gif\" alt=\"������ ���\" border=0></a>\
            </div>\
            <div style=\"position:absolute;z-index:9999;top:62px;left:20px;\" >\
              <a href=\"JavaScript:marva.autoload.launch_support()\"\
                 OnMouseOver=\"marva.autoload.toggleMotion(1);marva.autoload.change_on(1)\"\
                 OnMouseOut=\"marva.autoload.toggleMotion(0);marva.autoload.change_on(0)\"><img id=\"init_pic_name\" height=\"75px\" width=\"75px\" src=\"" + CONFIG.BASE_URL + "/initphoto_tracker.php?x=" + CONFIG.aspID + "\" alt=\"����\" border=0></a>\
            </div>\
            <div style=\"position:absolute;z-index:9999;top:33px;left:346px;\" >\
              <a href=\"JavaScript:marva.autoload.rejectInitiate()\"\
              OnMouseOver=\"marva.autoload.toggleMotion(1)\"\
              OnMouseOut=\"marva.autoload.toggleMotion(0)\"><img id=\"init_close_id2\" src=\"" + CONFIG.THEME_USER_INITIATE_BUTTON_CLOSE + "\" alt=\"������� ����\" border=0></a>\
            </div>";

          var dLink = document.getElementsByTagName("body")[0];
          var el_main_div = document.createElement("div");


          el_main_div.className = "class_el_main_div";
          el_main_div.setAttribute("align", "left");
          el_main_div.setAttribute("id", "ProactiveSupport4545");
          el_main_div.innerHTML = output_init;

          /*if (CONFIG.aspID == 48033)
          {
            dLink = document.getElementById("maincontainer");
            el_main_div.style.left = "auto";
            el_main_div.style.marginLeft = "612px";

          }*/

          dLink.appendChild(el_main_div);


          /* SRC ������ ����������� �-�� checkinitiate */

          el_frame = document.createElement("iframe");
          el_main_div.appendChild(el_frame);
          el_frame.allowTransparency = 1;
          el_frame.className = 'class_el_frame';
          el_frame.frameBorder = 0;
         // el_frame.src = CONFIG.BASE_URL + "/initext_tracker.php?x=" + CONFIG.aspID + "&page=" + getLocation() + "&resource" + CONFIG.resource;
          el_frame.src = CONFIG.BASE_URL + "/initext_tracker.php?x=" + CONFIG.aspID + "&resource" + CONFIG.resource + "&page=" + getLocation();

          el_main_div2 = document.getElementById("ProactiveSupport4545").style;

          if (!ns && !ie && !w3) {
            return;
          }
          ProactiveDiv = document.getElementById("ProactiveSupport").style;
        }
        if (ie || w3) {
          el_main_div2.visibility = "visible";
        } else {
          el_main_div2.visibility = "show";
        }
        backtrack = 0;
        isclosed = 0;
        repeat = 1;
        moveIt(-177);
      }

      function moveIt(h) {
        var what_should_i_move = document.getElementById("ProactiveSupport4545").style;

        what_should_i_move.left = 7;
        what_should_i_move.top = h + "px";

        /* ������� �������� � ���������� DOCTYPE ������� �������� */

        if (document.compatMode == "BackCompat") {
          var h_screen_coord = document.getElementsByTagName('body')[0].scrollTop;
        } else {
          var h_screen_coord = document.documentElement.scrollTop || document.body.scrollTop || 0;
        }

        if (h < (h_screen_coord)) {
          h += 10;
          backtrack = 1;
          // document.getElementById('premes_text').focus();
        } else {
          h = h_screen_coord;
        }
        if (CONFIG.halt) {
          setTimeout(function() {
            moveIt(h);
          }, CONFIG.timer);
        } else if ((!backtrack || (backtrack && (h >= 20))) && ((what_should_i_move.visibility == "visible") || (what_should_i_move.visibility == "show")) && repeat && !isclosed) {
          setTimeout(function() {
            moveIt(h);
            }, CONFIG.timer);
        } else if (!isclosed) {
          backtrack = 0;
          repeat = 0;
          setTimeout(function() {
            moveIt(h);
          }, CONFIG.timer);
        } else {
          what_should_i_move.left = 7;
        }
      }

      function DoClose() {
        if (ie||w3) {
          if(el_main_div2) el_main_div2.visibility = "hidden";
          ProactiveDiv.visibility = "hidden";
        } else {
          if(el_main_div2) el_main_div2.visibility = "hide";
          ProactiveDiv.visibility = "hide";
        }
        isclosed = 1;
        CONFIG.halt = 0;
      }

      function checkinitiate() {
        initiate = pullimage.width;
        if ((initiate == 2) && !chat_opened) {
          chat_opened = 1;
          launch_support();
        } else if ((initiate == 3) && !chat_opened) {
          // ���� ������ ����������� ����������� � ��� ��� ���� ��� �� ������
          chat_opened = 1;
          initializeProactive();
          var unique = getUniq();
        //  el_frame.src = CONFIG.BASE_URL + "/initext_tracker.php?x=" + CONFIG.aspID + "&page=" + getLocation() + "&unique=" + unique;
          el_frame.src = CONFIG.BASE_URL + "/initext_tracker.php?x=" + CONFIG.aspID + "&unique=" + unique + "&page=" + getLocation();
          document.getElementById('init_pic_name').src = CONFIG.BASE_URL + "/initphoto_tracker.php?x=" + CONFIG.aspID + "&ps=" + CONFIG.resource + "&unique=" + unique;
        } else if (initiate == 100) {
          do_tracker_flag = 0;
        }

        if ((initiate == 1) && chat_opened) {
          chat_opened = 0;
        }
      };

      do_tracker = function() {
        var title_page;
        /* check to make sure they are not idle for more then 1 hour... if so, then
        they left window open and let's stop the tracker to save server load time.
        (1000 = 1 second) */

        var unique = getUniq();
        time_elapsed = unique - start_tracker;
        if (time_elapsed > 3600000) {
          do_tracker_flag = 0;
        }

        //��������� �������� ���� title �� ��������, ��� � ������ ������ ��������� ������������
        if(document.title) {
          title_page = document.title;
        } else {
          title_page = '';
        }

        //pullimage.src = CONFIG.BASE_URL + "/image_tracker.php?l=" + CONFIG.aspl + "&x=" + CONFIG.aspID + "&deptid=" + CONFIG.deptID + "&page=" + getLocation() + "&title_page=" + title_page + "&ps=" + CONFIG.resource + "&unique=" + unique;
        pullimage.src = CONFIG.BASE_URL + "/image_tracker.php?l=" + CONFIG.aspl + "&x=" + CONFIG.aspID + "&deptid=" + CONFIG.deptID + "&title_page=" + title_page + "&ps=" + CONFIG.resource + "&unique=" + unique + "&page=" + getLocation();
        pullimage.onload = checkinitiate;
        if (do_tracker_flag == 1) {
          setTimeout(do_tracker, CONFIG.tracker_refresh);

        }
      };

      /* ����� ��������� �� ������ �� ������ */

      this.launch_support = function() {
        launch_support.call(this);
      }

      function launch_support() {
        var premes_filtered = "";
        if(tempopremes = document.getElementById('premes_text')) {
          premes_filtered = tempopremes.value;
          if (premes_filtered == CONFIG.enter_comment) {
            premes_filtered = "";
          }
          if (premes_filtered == "") return;
          
          premes_filtered = premes_filtered.replace(/>/g, "&gt;");
          premes_filtered = premes_filtered.replace(/</g, "&lt;");
          premes_filtered = premes_filtered.replace(/^(\s)+/, "");
          premes_filtered = premes_filtered.replace(/(\r\n)/gi, "--rn--");
          premes_filtered = premes_filtered.replace(/^(--rn--)+/, "");
          premes_filtered = premes_filtered.replace(/(--rn--){3,}/gi, "--rn----rn--");
          premes_filtered = premes_filtered.replace(/\n/gi, "--n--"); // firefox double check
          premes_filtered = premes_filtered.replace(/^(--n--)+/, "");
          premes_filtered = premes_filtered.replace(/(--n--){3,}/gi, "--n----n--");
          premes_filtered = premes_filtered.replace(/(--rn--)/gi, "\r\n");
          premes_filtered = premes_filtered.replace(/(--n--)/gi, "\n");
          premes_filtered = encodeURIComponent(premes_filtered);
        }
        var data = '';
        if (typeof(marvaData) != 'undefined') {
          data = '&data=' + marvaData;
        }
        //var request_url = CONFIG.BASE_URL + "/request.php?l=" + CONFIG.aspl + "&x=" + CONFIG.aspID + "&deptid=" + CONFIG.deptID + "&page=" + getLocation() + "&premes=" + premes_filtered + "&resource=" + CONFIG.resource + "&firstmes=" + data;
        var request_url = CONFIG.BASE_URL + "/request.php?l=" + CONFIG.aspl + "&x=" + CONFIG.aspID + "&deptid=" + CONFIG.deptID + "&premes=" + premes_filtered + "&resource=" + CONFIG.resource + "&firstmes=" + data + "&page=" + getLocation();
        var newwin = window.open(request_url, unique, 'menubar=no,resizable=1,directories=no,location=no,toolbar=no,status=no,scrollbars=0,screenX=50,screenY=100,width=' + CONFIG.widthWindowChat + ',height=' + CONFIG.heightWindowChat);
        if (newwin != null && newwin != undefined) {
          newwin.focus();
        }
        DoClose();
      }

      this.change_on = function(var_and) {
        if(var_and) {
          document.getElementById('init_on_id').src = CONFIG.THEME_USER_INITIATE_BUTTON;
        } else {
          document.getElementById('init_on_id').src = CONFIG.BASE_URL + '/images/initiate7buttno.gif';
        }
      }

      /* ������ ���� ����������� */

      function WriteChatDiv() {
        var output_inold;
        var scroll_image = new Image;
        scroll_image.src = CONFIG.BASE_URL + "/scroll_image.php?x=" + CONFIG.aspID + "&l=" + CONFIG.aspl + "&resource=" + CONFIG.resource + "&" + unique;

        output_inold = document.createElement("div");
        output_inold.setAttribute("id", "ProactiveSupport");
        output_inold.setAttribute("class", "ProactiveSupport");
        output_inold.setAttribute("style", "align:left;position:absolute;z-index:9997;");

        document.getElementsByTagName("body")[0].appendChild(output_inold);

        if (ie||w3) {
          output_inold.style.visibility = "hidden";
        } else {
          output_inold.style.visibility = "hide";
        }
      }

      // Reject operator`s invitation
      this.rejectInitiate = function() {
        var rejectimage = new Image;
        rejectimage.src = CONFIG.BASE_URL + "/image_tracker.php?l=" + CONFIG.aspl + "&x=" + CONFIG.aspID + "&deptid=" + CONFIG.deptID + "&unique=" + unique + "&ps=" + CONFIG.resource + "&action=reject";
        DoClose();
        chat_opened = 0;
      };

      var status_image = document.createElement("img");
      status_image.src = CONFIG.tracker_image;
      status_image.setAttribute("style", "margin:0;padding:0;font-size:0;border:0;line-height:0;border:0;position:absolute;left:-9000px");
      document.getElementsByTagName("body")[0].appendChild(status_image);

      if (!marva_rh_loaded_v090131) {
        WriteChatDiv();
        /* �������� ��� �����������, ������������ �� ��� ������ �� �������� */
        window.setTimeout(do_tracker, 5000);
      }
      var marva_rh_loaded_v090131 = 1;
    }
  }
});
