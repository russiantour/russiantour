function reloadImg(img) {
    if (img == null) return;
    var time = 60000;
           var imgSrc = img.src;
           var paramArray = imgSrc.split('&');
    if (typeof(marva) == 'undefined' || marva == null) {
      marva = {};
    }
    if (typeof(marva.load_async) == 'undefined' || marva.load_async == null) {
      // to support the package without asynchronous download
           setTimeout(function() {
        img.src = paramArray[0] + '&' + paramArray[1] + '&' + paramArray[2] + '&rand=' + new Date().getTime();
      }, time);
    } else {
      setInterval(function() {
        img.src = paramArray[0] + '&' + paramArray[1] + '&' + paramArray[2] + '&rand=' + new Date().getTime();
      }, time);
    }
       }