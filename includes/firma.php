<?php
if (isset($payment->order_id))
{
    $the_sql = "SELECT * FROM #__viaggio_orders WHERE id = ".$payment->order_id;
    $db = JFactory::getDbo();
    $db->setQuery($the_sql);
    $the_order = $db->loadObject();
}
elseif (isset($item))
{
    $the_order = $item;
}
if (!isset($email))
    $email = '';
else
    $email .= ',';
if (
    (is_array($manualPayment) && isset($manualPayment['check_type']) && $manualPayment['check_type']==1) ||
    (isset($manualPayment->check_type) && $manualPayment->check_type==1) ||
    (isset($payment->check_type) && $payment->check_type==1) ||
    (isset($the_order->check_type) && $the_order->check_type==1) /*|| (isset($payment->check_type) && $payment->check_type==1)*/)

{

    $name = 'Russian Tour International ';
    $name_ru = '«Международная Компания «Русский Тур»';
    $pech = '/home/visto/web/visto-russia.com/public_html/images/pech_po.png';
    $pech_url = 'https://www.visto-russia.com/images/pech_po.png';
   $logo = '/home/russiantour/web/russiantour.com/public_html/images/yootheme/demo/default/logo.png';
    $mbt = '012877';
    $mbt_data = 'dal 12/05/2021 al 11/05/2022';
    $mbt_data_ru = 'с 12/05/2021 по 11/05/2022';
    $email .= 'info@russiantour.com';
    $adres = '191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 1-3-4';
    $adres_ru = '191123 г. Санкт-Петербург, ул. Шпалерная, д. 22, лит. А, пом. 9-Н, оф. 1,3,4';
    $adres_po = '191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 1-3-4';
    $adres_po_ru = '191123 г. Санкт-Петербург, ул. Шпалерная, д. 22, лит. А, пом. 9-Н, оф. 1,3,4';
    $s1 = 'Russian Tour International Ltd.<br>191123, San Pietroburgo,191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 1-3-4 <br>INN 7802853888, KPP 780201001 OGRN 1147847089532, OKPO 35460198<br>Bank:PJSC Sovcombank<br>Account number: 40702978200190742076 SWIFT: SOMRRUMM<br>info@russiantour.com www.russiantour.com';
    $s1_ru = 'Общество с ограниченной ответственностью Международная Компания «Русский Тур» ИНН  7802853888 КПП 780201001 ОГРН 1147847089532 <br>Юр. Адрес 191123 г. Санкт-Петербург, ул. Шпалерная, д. 22, лит. А, пом. 9-Н, оф. 1,3,4/сч 30101810445250000360 БИК 044525360<br>info@russiantour.com www.russiantour.com ';
$strah = 'АО "Совкомбанк страхование"';
$strah_it ='Sovcombank insurance Ltd';
$strah_www = 'Sovcomins.ru';
$strah_email = 'cs@sovcomins.ru';


    if (
        (is_array($manualPayment) && isset($manualPayment['bank']) && $manualPayment['bank']=='tinkoff') ||
        (isset($manualPayment->bank) && $manualPayment->bank=='tinkoff') ||
        (isset($payment->bank) && $payment->bank=='tinkoff') ||
        (isset($the_order->bank) && $the_order->bank=='tinkoff') /*|| (isset($payment->bank) && $payment->bank=='tinkoff')*/)
    {
$ooo = 'Russian Tour International Ltd.<br>
191123, San Pietroburgo, Uliza Shpalernaya 22,<br>
lit. A, pom. 9 N, uff. 1-3-4<br>
INN (Codice individuale del contribuente) 7802853888,<br>
KPP (Codice causale di registrazione)780201001<br>
OGRN (Numero unico di registrazione stataleb) 1147847089532<br>
Beneficiary Bank: Tinkoff Bank <br>
Account number: 40702978120000003618<br>
SWIFT: TICSRUMMXXX <br>
Address Beneficiary Bank: 1st Volokolamsky pr.,<br> 10, bld. 1, Moscow, Russia<br>
Correspondent Bank: J.P.MORGAN AG<br> FRANKFURT AM MAIN, DE <br>
SWIFT: CHASDEFXXXX<br>
';


$ooo_ru ='ООО "Международная Компания «Русский Тур»<br>
Юридический адрес: 191123 г. Санкт-Петербург,<br> 
ул. Шпалерная, д. 22, лит. А, пом. 9-Н, офис 1,3,4<br>
ИНН 7802853888, КПП 780201001<br>
ОГРН 1147847089532, ОКПО 35460198<br>
Банк АО «Тинькофф Банк» <br>
р/сч 40702978120000003618<br>
SWIFT: TICSRUMMXXX<br>
Адрес банка: 1-ый Волоколамский проезд, <br>10, стр. 1, Москва, Россия<br>
Банк корреспондент: J.P.MORGAN AG<br> FRANKFURT AM MAIN, DE <br>
SWIFT: CHASDEFXXXX <br>';

$strah = 'АО "Совкомбанк страхование"';
$strah_it ='Sovcombank insurance Ltd';
$bank ='Beneficiario: Russian Tour International Ltd.<br>
Indirizzo: 191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 1-3-4<br>
Numero di conto corrente:  40702978120000003618 <br>
SWIFT: TICSRUMMXXX <br>
Banca: Tinkoff Bank <br>
Indirizzo della banca: 1st Volokolamsky pr., 10, bld. 1, Moscow, Russia<br>
Causale pagamento: ';
    }


    if (
        (is_array($manualPayment) && isset($manualPayment['bank']) && in_array($manualPayment['bank'],['payler','СовкомБанк'])) ||
        (isset($manualPayment->bank) && in_array($manualPayment->bank,['payler','СовкомБанк'])) ||
        (isset($payment->bank) && in_array($payment->bank,['payler','СовкомБанк'])) ||
        (isset($the_order->bank) && in_array($the_order->bank,['payler','СовкомБанк'])))
    {
$ooo = 'Russian Tour International Ltd.<br>
191123, San Pietroburgo, Uliza Shpalernaya 22,<br>
lit. A, pom. 9 N, uff. 1-3-4<br>
INN (Codice individuale del contribuente) 7802853888,<br>
KPP (Codice causale di registrazione)780201001<br>
OGRN (Numero unico di registrazione stataleb) 1147847089532<br>
Account number: 40702978200190742076 <br>
SWIFT: SOMRRUMM <br>
Bank: PJSC Sovcombank<br>
Bank address: 24 Vavilova Street ,119991 Moscow, Russia<br>
Correspondent Bank: COMMERZBANK AG,<br>
Frankfurt am Main, Germany<br>
Correspondent Bank SWIFT: COBADEFFXXX<br>';


$ooo_ru ='ООО "Международная Компания «Русский Тур»<br>
Юридический адрес: 191123 г. Санкт-Петербург,<br> 
ул. Шпалерная, д. 22, лит. А, пом. 9-Н, офис 1,3,4<br>
ИНН 7802853888, КПП 780201001<br>
ОГРН 1147847089532, ОКПО 35460198<br>
Банк Филиал «Корпоративный» ПАО «Совкомбанк»<br>
Р/с 40702810500190730765 <br>
к/сч 30101810445250000360 БИК 044525360<br>
Адрес банка: 119991,Россия, г. Москва,<br>
ул. Вавилова, д. 24<br>
';

$bank ='Beneficiario: Russian Tour International Ltd.<br>
Indirizzo: 191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 1-3-4<br>
Numero di conto corrente:  40702978200190742076 <br>
SWIFT: SOMRRUMM <br>
Banca: PJSC Sovcombank<br>
Indirizzo della banca: Vavilova Street 24, 119991 Moscow, Russia<br>
Causale pagamento: ';

    }
}
elseif (
    (is_array($manualPayment) && isset($manualPayment['check_type']) && $manualPayment['check_type']==2) ||
    (isset($manualPayment->check_type) && $manualPayment->check_type==2) ||
    (isset($payment->check_type) && $payment->check_type==2) ||
    (isset($the_order->check_type) && $the_order->check_type==2) /*|| (isset($payment->check_type) && $payment->check_type==2)*/)
{
    $name = 'Visto Russia ';
    $name_ntd = 'Visto Russia Ltd';

    $name_ru = '«Висто Руссиа»';

    $pech = '/home/visto/web/visto-russia.com/public_html/images/pech_visto.png';
    $pech_url = 'https://www.visto-russia.com/images/pech_visto.png';

    $logo = '/home/visto/web/visto-russia.com/public_html/images/logo_vi.png';

    $s1 = 'Visto Russia Ltd.<br>191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 2-5<br>INN 7802681004, KPP 780201001 OGRN 1187847346136, OKPO 34622262<br>Bank: SOVCOMBANK, CORPORATE BRANCH<br>Account number: 40702978400190802046<br>info@russiantour.com www.russiantour.com';

    $mbt = '020790';
    $mbt_data = 'dal 01/02/2020 al 31/01/2021';
    $mbt_data_ru = 'c 01/02/2020 по 31/01/2021';
    $email .='info@visto-russia.com';
    $adres = '191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 2-5';
    $adres_ru = '191123, Санкт-Петербург, ул. Шпалерная, д. 22, лит. А, пом. 9-Н, офис  2-5';
    $adres_po = '191123, San Pietroburgo, Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 2-5';
    $adres_po_ru = '191123, г, Cанкт-Петербург ул. Шпалерная, д. 22, лит. А, пом. 9-Н, офис  2-5';

    if (
        (is_array($manualPayment) && isset($manualPayment['bank']) && in_array($manualPayment['bank'],['payler','СовкомБанк'])) ||
        (isset($manualPayment->bank) && in_array($manualPayment->bank,['payler','СовкомБанк'])) ||
        (isset($payment->bank) && in_array($payment->bank,['payler','СовкомБанк'])) ||
        (isset($the_order->bank) && in_array($the_order->bank,['payler','СовкомБанк'])))
    {
$ooo = 'Visto Russia Ltd.<br>
191123, San Pietroburgo, prospekt<br>
Uliza Shpalernaya 22, lit. A, pom. 9 N,
uff. 2-5<br>
INN (Codice individuale del contribuente) 7802681004,<br>
KPP (Codice causale di registrazione)780201001<br>
OGRN (Numero unico di registrazione stataleb) 1187847346136<br>
Account number: 40702978400190802046<br>
SWIFT: SOMRRUMM <br>
Bank: PJSC Sovcombank<br>
Bank address: 24 Vavilova Street ,119991 Moscow, Russia<br>
Correspondent Bank: COMMERZBANK AG,<br>
Frankfurt am Main, Germany<br>
Correspondent Bank SWIFT: COBADEFFXXX<br>';

$strah = 'АО "Совкомбанк страхование"';
$strah_it ='Sovcombank insurance Ltd';
$strah_www = 'Sovcomins.ru';
$strah_email = 'cs@sovcomins.ru';

$ooo_ru = 'ООО "Висто Руссиа»<br>
Юридический адрес: 191123, Санкт-Петербург,<br>
ул. Шпалерная, д. 22, лит. А, пом. 9-Н, <br>
офис 2,5<br>
ИНН 7802681004, КПП 780201001<br>
ОГРН 1187847346136, ОКПО 34622262<br>
Банк Филиал «Корпоративный» ПАО «Совкомбанк»<br>
р/сч 40702978400190802046<br>
SWIFT: SOMRRUMM<br>
Адрес банка: 119991,Россия, г. Москва,<br>
ул. Вавилова, д. 24<br>
Банк корреспондент:COMMERZBANK AG,<br>
Frankfurt am Main, Germany<br>
SWIFT: COBADEFFXXX<br>';

$bank = 'Beneficiario: Visto Russia Ltd.<br>
Indirizzo: Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 2-5, 191123 St. Petersburg, Russia<br>
Numero di conto corrente: 40702978400190802046<br>
SWIFT: SOMRRUMM<br>
Banca: PJSC Sovcombank<br>
Indirizzo della banca: Vavilova Street 24, 119991 Moscow, Russia<br>
Causale pagamento: ';

    }

    if (
        (is_array($manualPayment) && isset($manualPayment['bank']) && $manualPayment['bank']=='tinkoff') ||
        (isset($manualPayment->bank) && $manualPayment->bank=='tinkoff') ||
        (isset($payment->bank) && $payment->bank=='tinkoff') ||
        (isset($the_order->bank) && $the_order->bank=='tinkoff') /*|| (isset($payment->bank) && $payment->bank=='tinkoff')*/)
    {
$ooo = 'Visto Russia Ltd.<br>
191123, San Pietroburgo, prospekt<br>
Uliza Shpalernaya 22, lit. A, pom. 9 N,
uff. 2-5<br>
INN (Codice individuale del contribuente) 7802681004,<br>
KPP (Codice causale di registrazione)780201001<br>
OGRN (Numero unico di registrazione stataleb) 1187847346136<br>
Account number: 40702978020000003860<br>
SWIFT: TICSRUMMXXX<br>
Bank: Tinkoff Bank<br>
Bank address: 1st Volokolamsky pr., 10, bld. 1, Moscow, Russia<br>
Correspondent Bank:  J.P.MORGAN AG<br>
FRANKFURT AM MAIN, DE<br>
Correspondent Bank SWIFT: CHASDEFXXXX<br>';

$ooo_ru = 'ООО "Висто Руссиа»<br>
Юридический адрес: 191123, Санкт-Петербург,<br>
ул. Шпалерная, д. 22, лит. А, пом. 9-Н,<br>
офис 2,5<br>
ИНН 7802681004, КПП 780201001<br>
ОГРН 1187847346136, ОКПО 34622262<br>
Банк Tinkoff Bank <br>
р/сч 40702978020000003860<br>
SWIFT: TICSRUMMXXX<br>
Адрес банка: : 1-ый Волоколамский проезд, 10,<br> стр. 1, Москва, Россия <br>
Банк корреспондент::J.P.MORGAN AG<br>
FRANKFURT AM MAIN, DE<br>
SWIFT: CHASDEFXXXX<br>';

$bank = 'Beneficiario: Visto Russia Ltd. <br>
Indirizzo: Uliza Shpalernaya 22, lit. A, pom. 9 N, uff. 2-5, 191123 St. Petersburg, Russia<br>
Numero di conto corrente: 40702978020000003860<br>
SWIFT: TICSRUMMXXX<br>
Banca: Tinkoff Bank<br>
Indirizzo della banca: 1st Volokolamsky pr., 10, bld. 1, Moscow, Russia <br>
Causale pagamento: ';
    }
}
else
{
    $name = 'Visto Russia Int.';
}


?>