var myGeocoder = null;
var googleaddress = null;
var geodata = null;

function findAddressGeo(eventObj, submit) {
	geodata = new Array();
	$googleaddress = jQuery("#googleaddress");
	if ($googleaddress.length > 0) {
		googleaddress = $googleaddress.val()
	}

	if (!googleaddress || googleaddress.length == 0) {
		// uncomment to make compulsary
		// alert("Please enter an address and/or zip code");
		return false;
	}
	if (myGeocoder) {
		var myTimer = trapSlowGoogle.delay(3000);

		myGeocoder.geocode({
			'address': googleaddress,
			'region': jQuery('#gregion').val()
		},
		function (results, status) {
			var $gaddressmatches = jQuery("#gaddressmatches");
			$gaddressmatches.html("");
			$gaddressmatches.css('display', "none");

			if (status == google.maps.GeocoderStatus.OK) {
				if (results.length > 1) {
					submit = false;
					var countmatches = 0;
					for (var r = 0; r < results.length; r++) {
						match = results[r];
						geodata.push(match);
						$gaddressmatches.html($gaddressmatches.html()+ "<a href='#select' onclick='setGeoData(" + countmatches + ")'>" + match.formatted_address + "</a><br/>");
						$gaddressmatches.css('display', "block");
						countmatches++;
					}
					;
				}
				else {
					jQuery("#googleaddress").val(results[0].formatted_address);
				}
				var $gvelem = jQuery("#geosearch_fv");
				var point = results[0].geometry.location;
				//gvelem.value = point.lng()+","+point.lat();
				$gvelem.val(point.lat() + "," + point.lng());

				clearTimeout(myTimer);
				// since we stopped the event we must now allow the form to submit !!!
				if (submit) {
					$myform = jQuery("#jeventspost");
					$myform.submit();
				}
			} else {
				alert("Address/Zip Code " + googleaddress + " not found.");
			}
		});
	}
}

function setGeoData(idx) {
	jQuery("#googleaddress").val(geodata[idx].formatted_address);
	var $gvelem = jQuery("#geosearch_fv");
	var point = geodata[idx].geometry.location;
	$gvelem.val(point.lat() + "," + point.lng());

	var $gaddressmatches = jQuery("#gaddressmatches");
	$gaddressmatches.html("");
	$gaddressmatches.css('display', "none");

	$myform = jQuery("#jeventspost");
	$myform.submit();
}

function findEditAddressGeo(eventObj, submit) {
	var $gvelem = jQuery("#geosearch_fv");
	if ($gvelem.length > 0)
	{
		//Do not get the geo location if already have it !!!
		return;
	}

	geodata = new Array();
	googleeditaddress = document.getElementById("evlocation");
	if (googleeditaddress) {
		googleeditaddress = googleeditaddress.value
	}
	if (!googleeditaddress || googleeditaddress.length == 0) {
		// uncomment to make compulsary
		// alert("Please enter an address and/or zip code");
		return false;
	}
	if (myGeocoder) {
		var myTimer = trapSlowGoogle.delay(3000);

		myGeocoder.geocode({
			'address': googleeditaddress,
			'region': jQuery('#gregion').val()
		},
		function (results, status) {
			var gaddressmatches = jQuery("#geditaddressmatches");
			gaddressmatches.html("");
			gaddressmatches.css('display', "none");
			if (status == google.maps.GeocoderStatus.OK) {
				if (results.length > 1) {
					submit = false;
					var countmatches = 0;
					for (var r = 0; r < results.length; r++) {
						match = results[r];
						geodata.push(match);
						gaddressmatches.html(gaddressmatches.html() + "<a href='#select' onclick='setEditGeoData(" + countmatches + ")'>" + match.formatted_address + "</a><br/>");
						gaddressmatches.css('display', "block");
						countmatches++;
					}
					;
				}
				else {
					jQuery("#evlocation").val(results[0].formatted_address);
				}

				clearTimeout(myTimer);

			} else {
				alert("Address/Zip Code " + googleeditaddress + " not found.");
			}
		});

	}
}

function setEditGeoData(idx) {
	jQuery("#googleeditaddress").val(geodata[idx].formatted_address);
	var gaddressmatches = jQuery("#geditaddressmatches");
	gaddressmatches.html("");
	gaddressmatches.css("display", "none");
}

function trapSlowGoogle() {
	alert("The address lookup is taking too long and so the address filter has been ignored");
	$myform = jQuery("#jeventspost");
	$myform.submit();
}

function clearlonlat() {
	var $gvelem = jQuery("#geosearch_fv");
	if ($gvelem.length > 0)
	{
		$gvelem.val("");
	}
}


function successLocation(position)
{
	var googleaddress = document.getElementById("googleaddress");
	if (!googleaddress || googleaddress.value != "") {
		return;
	}

	var $gvelem = jQuery("#geosearch_fv");
	if ($gvelem.length == 0) {
		return;
	}
	$gvelem.val(position.coords.latitude + "," + position.coords.longitude);

	if (myGeocoder) {
		var myTimer = trapSlowGoogle.delay(30000);

		var input = $gvelem.val();
		var latlngStr = input.split(",", 2);
		var lat = parseFloat(latlngStr[0]);
		var lng = parseFloat(latlngStr[1]);

		var latlng = new google.maps.LatLng(lat, lng);

		myGeocoder.geocode(
				{
					'latLng': latlng
				},
		function (results, status) {
			var $gaddressmatches = jQuery("#gaddressmatches");
			$gaddressmatches.html("");
			$gaddressmatches.css('display', "none");
			if (status == google.maps.GeocoderStatus.OK) {
				if (results.length > 0) {
					jQuery("#googleaddress").val(results[0].formatted_address);
					var point = results[0].geometry.location;
					$gvelem.val(point.lat() + "," + point.lng());
				}

				clearTimeout(myTimer);
				$myform = jQuery("#jeventspost");
				$myform.submit();
			} else {
				alert("Geocoder failed due to: " + status);
				$myform = jQuery("#jeventspost");
				myform.submit();
			}
		}
		);
	}
}

function findEventNearby(autogeolocate) {
	// Touch devices only
	if (autogeolocate == 2) {
		try {
			document.createEvent("TouchEvent");
		}
		catch (e) {
			return false;
		}
	}
	if (navigator.geolocation) {
		/* geolocation is available */
		navigator.geolocation.getCurrentPosition(successLocation);
	}
}


var geolocationSupport = {
	setup: function () {
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(
					function (position) {

						// Did we get the position correctly?
						alert(position.coords.latitude);

						// To see everything available in the position.coords array:
						for (key in position.coords) {
							alert(key)
						}

						mapServiceProvider(position.coords.latitude, position.coords.longitude);

					},
					// next function is the error callback
							function (error)
							{
								switch (error.code)
								{
									case error.TIMEOUT:
										alert('Timeout');
										break;
									case error.POSITION_UNAVAILABLE:
										alert('Position unavailable');
										break;
									case error.PERMISSION_DENIED:
										alert('Permission denied');
										break;
									case error.UNKNOWN_ERROR:
										alert('Unknown error');
										break;
								}
							}

					);
				}
	}

}


jQuery(window).load(function () {
	try {
		myGeocoder = new google.maps.Geocoder();
		// set the form onsubmit event
		var myform = jQuery("#jeventspost");
		if (myform.length > 0) {
			myform.on("submit", function (event) {
				var $gvelem = jQuery("#geosearch_fv");
				// if auto geolocating then we have the address and longitude/latitude already
				if ($gvelem.length > 0 && $gvelem.val()!=""){
					return true;
				}

				var $googleaddress = jQuery("#googleaddress");
				if ($googleaddress.length > 0) {
					googleaddress = $googleaddress.val();
				}
				// force submit to wait for the address to be checked
				if (googleaddress && googleaddress.length > 0) {
					var eventObj = new DOMEvent(event);
					try {
						eventObj.stop();
					}
					catch (e) {
						eventObj.stopImmediatePropagation();
					}
					findAddressGeo(eventObj, 1);
				}
			});

		}
		;
	}
	catch (e) {

	}
});