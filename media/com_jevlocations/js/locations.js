var myMap = false;
var myMarker = false;
var myGeocoder = null;
var disableautopan = false;

function myMapload(){
	if (typeof google == 'undefined' || !jQuery("#gmap").length) return;
	var point =  new google.maps.LatLng(globallat,globallong);

	var myOptions = {
		center: point,
		zoom: globalzoom,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	myMap = new google.maps.Map(document.getElementById("gmap"), myOptions);
	//myMap.addControl( new GSmallMapControl() );
	//myMap.addControl( new GMapTypeControl()) ;
	//myMap.addControl( new GOverviewMapControl(new GSize(60,60)) );

	/*
		// Create our "tiny" marker icon
		var blueIcon = new GIcon(G_DEFAULT_ICON);
		blueIcon.image = "http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png";

		// Set up our GMarkerOptions object
		markerOptions = { icon:blueIcon, draggable:true };
		*/
	var markerOptions = {
		position:point,
		map:myMap,
		disableAutoPan:disableautopan,
		animation: google.maps.Animation.DROP,
		draggable:true
	};

	myMarker = new google.maps.Marker( markerOptions);

	google.maps.event.addListener(myMap, 'click',  function(e) {
		if (e.latLng) {
			document.getElementById('geolat').value=e.latLng.lat();
			document.getElementById('geolon').value=e.latLng.lng();
			document.getElementById('geozoom').value=myMap.getZoom();

			myMarker.setPosition(e.latLng);
			myMap.setCenter(e.latLng);
		}
	});

	google.maps.event.addListener(myMarker, "dragend", function(e) {
		if (e.latLng) {
			document.getElementById('geolat').value=e.latLng.lat();
			document.getElementById('geolon').value=e.latLng.lng();
			document.getElementById('geozoom').value=myMap.getZoom();
			myMap.setCenter(e.latLng);
		}
	});

	google.maps.event.addListener(myMap,  "zoom_changed", function() {
		document.getElementById('geozoom').value=myMap.getZoom();
	});

	myGeocoder = new google.maps.Geocoder();
};

jQuery(window).on('load', myMapload);

function addressSelected() {
	var street = document.getElementById("street").value;
	var city = document.getElementById("catid").value;
}

// directcall is true if function is called from clicking a button
function findAddress(directcall){
	var address = [];
	var searchAddress = '';
	if(jQuery('#googleaddress').length)
	{
		if(jQuery("#googleaddress").val().length)
		{
			address[0] = jQuery("#googleaddress").val();
		}
		if(jQuery("#googlecountry").val().length)
		{
			address[4] = jQuery("#googlecountry").val();
		}
		if (address.length == 0)
		{
			address = getStreetData(address);

			if(jQuery("#catid").val() != 0)
			{
				address[1] = jQuery( "#catid option:selected").text();
			}
		}
		if (address.length)
		{
			searchAddress = address.join();
		}
	}
	else {
		try {

			address = getStreetData(address);

			if(jQuery("#city").val().length)
			{
				address[2] = jQuery("#city").val();
			}
			if(jQuery("#state").val().length)
			{
				address[3] = jQuery("#state").val();
			}
			if(jQuery("#country").val().length)
			{
				address[4] = jQuery("#country").val();
			}

			if (address.length)
			{
				searchAddress = address.join();
			}
		}
		catch (e){}
	}

	if (myGeocoder) {
		if(searchAddress.length)
		{
			myGeocoder.geocode( { 'address': searchAddress}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					point = results[0].geometry.location;
					myMap.setCenter(point);
					myMarker.setPosition(point);
					document.getElementById('geolat').value=point.lat();
					document.getElementById('geolon').value=point.lng();
					document.getElementById('geozoom').value=myMap.getZoom();
                    jQuery('.jev_loc_notice').hide();
				}
				else {
                    jQuery('.jev_loc_notice').show();
                    jQuery('.jev_loc_notice').text(searchAddress + ', ' + jev_str_notfound);

                }
			});
		}
                // no need to call alert from onblur otherwise we get in a loop
		else if (directcall)
		{
            jQuery('.jev_loc_notice').show();
            jQuery('.jev_loc_notice').text(jev_str_introduceaddress);
		}
	}
}

function getStreetData(address)
{
	if(jQuery("#street").val().length)
	{
		address[0] = jQuery("#street").val();
	}
	if(jQuery("#postcode").val().length)
	{
		address[1] = jQuery("#postcode").val();
	}

	return address;
}

function selectLocation(url)
{
	jevModalPopup('jevLocationModal',url,"");
}

function selectThisLocation(locid, elem){

	var title = elem.innerHTML;

	var locn = jQuery('#locn',  window.parent.document);
	var evlocation = jQuery('#evlocation',  window.parent.document);
	if (locn.length && evlocation.length)
	{//If we are editing an event
		locn.val(locid);
		evlocation.val(title);
		window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
		showRemoveButton();
	}
	else
	{// If actually selecting a location for a menu item we do something different:
		var menu_location = jQuery('#menuloc',  window.parent.document);
		if (menu_location.length)
		{
			menu_location.val("jevl:"+locid);
			var $result = window.parent.sortableLocations.selectThisLocation(locid, elem);
			window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
			return $result;
		}
		// in this case editing a location detail menu item
		var selectedlocationtitle = jQuery('#selectedlocationtitle',  window.parent.document);
		var selectedlocation = jQuery('#selectedlocation',  window.parent.document);
		if (selectedlocationtitle.length && selectedlocation.length)
		{
			selectedlocationtitle.val(title);
			selectedlocation.val(locid);
			try {
				SqueezeBox.close();
				return false;
			}
			catch (e) {
			}
			try {
				window.parent.SqueezeBox.close();
			}
			catch (e) {
				window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
				showRemoveButton();
			}
		}
		return false;

	}

	return false;
}

function selectLocationEditEvent(locid, elem){
	var title = elem.innerHTML;
	var locn = jQuery('#locn',  window.parent.document);
	if (locn.length){
		locn.val(locid);
	}
	var evlocation = jQuery('#evlocation',  window.parent.document);
	if (evlocation.length){
		evlocation.val(title);
		// If actually selecting a location for a menu item we do something different:
		var menu_location = jQuery('#menu_location');
		if (menu_location.length){
			menu_location.val("jevl:"+locid);
		}
	}
	// else this is a menu so do something different
	else {
		return window.parent.sortableLocations.selectThisLocation(locid, elem);
	}

	showRemoveButton();
	
	window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');

	return false;
}

function removeLocation(){
	jQuery('#locn').val('');
	jQuery('#evlocation').val('');
	showRemoveButton();
	return;
}

var sortableLocations = {
	setup:function (){
		var uls = jQuery('#sortableLocations');
		uls.sortable({
			placeholder: 'ui-state-highlight',
			update: sortableLocations.fieldsHaveReordered
		});

		var lis = uls.children();
		lis.each(function(i, li){
			sortableLocations.copyTrash(li);
		});
	},
	copyTrash:function(li){
		var trashimage = jQuery('#trashimageloc');
		var trashCopy = trashimage.clone();
		trashCopy.attr('id','');
		trashCopy.show();

		li = jQuery(li);
		$moveIcon = jQuery('<i/>').addClass('icon-menu').css('cursor','move');
		$moveIcon.prependTo(li);
		trashCopy.appendTo(li);
		sortableLocations.setupTrashImage(trashCopy);
	},
	fieldsHaveReordered:function(targetNode){
		// Now rebuild the select list items
		var menuloc = jQuery("#menuloc");
		if (menuloc.length) {
			menuloc.val('');
			var uls = jQuery('#sortableLocations');
			var lis = jQuery('#sortableLocations li');

			var newOrder = []

			lis.each(function(i, item){
				var id = jQuery(item).attr('id');
				id = id.replace("sortableloc","");
				newOrder[i] = 'jevl:' + id;
				});

			menuloc.val(newOrder.join());
		}


	},
	setupTrashImage:function(item){
		item.click(function(){
			item.parent().remove();
			sortableLocations.fieldsHaveReordered(item);
		})
	},
	selectThisLocation:function (locid, elem){
		var duplicateTest = jQuery("#sortableloc"+locid);
		if (duplicateTest.length) {
			alert(jev_str_duplicatedlocation);
			window.parent.jQuery('#jevLocationModal', window.parent.document).modal('hide');
			return;
		}
		var title = elem.innerHTML;

		var li = jQuery('<li/>',{id: 'sortableloc'+locid})

		li.html(title);
		if (jQuery('#sortableLocations').length)
		{
			jQuery('#sortableLocations').append(li);
			sortableLocations.copyTrash(li);

			var togdown = document.getElement('h3.jpane-toggler-down');
			if (togdown ){
				var tab = togdown.getNext();
				var tabtable = tab.getElement('table.paramlist');
				tab.setStyle('height',tabtable.offsetHeight);
			}

			// reset the sortable list
		/*	new Sortables('sortableLocations',{
				"onComplete":sortableLocations.fieldsHaveReordered
			});*/
		}

		// If actually selecting a location for a menu item we do something different:
		var menuloc = jQuery('#menuloc');
		if (menuloc.length)
		{
			sortableLocations.fieldsHaveReordered(menuloc);
		}

		return false;
	},
	selectLocation:function (url,title){
		jevModalPopup('jevLocationModal',url,title);
	}
}

var JevrRequiredFields = false;

jQuery(window).on ('load',  function() {	
    // for custom field handling - defined required fields if not defined in the custom fields plugin already
    if (JevrRequiredFields == false){
        JevrRequiredFields = {
            fields: new Array(),
            verify:function (form){
                    var messages =  new Array();
                    valid = true;
                    JevrRequiredFields.fields.forEach(function (item,i) {
                            name = item.name;

                        // should we skip this test because of category restrictions?
                            if (JevrCategoryFields.skipVerify(name))  return;

                            var matches = new Array();
                               jQuery(form.elements).each (function (testi, testitem) {
                                    if(testitem.name == name || "custom_"+testitem.name == name || testitem.id == name  || testitem.id.indexOf(name+"_")==0  || ("#"+testitem.id) == name  || $(testitem).hasClass(name.substr(1))){
                                            matches.push(testitem);					
                                    };
                            });
                            var value = "";
                            if(matches.length==1){
                                    value = matches[0].value;
                            }
                            // A set of radio checkboxes
                            else if (matches.length>1){
                                    matches.each (function (match, index){
                                            if (match.checked) value = match.value; 
                                    });				
                            }
                            //if (elem) elem.value = item.value;
                            if (value == item['default'] || value == ""){
                                    valid = false;
                                    // TODO add message together 
                                    if(item.reqmsg!=""){
                                            messages.push(item.reqmsg);
                                    }
                            }
                    });
                    if (!valid){
                            message = "";
                            messages.each (function (msg, index){message += msg+"\\n";});
                            alert(message); 
                    }
                    return valid;
            }
        }
        var form =document.adminForm;
        if (form){		
           jQuery(form).on('submit',function(event){if (!JevrRequiredFields.verify(form)) {event.preventDefault();event.stopImmediatePropagation();}});
        };
    }

});


// for location editing page only
function setupNewLocation(){
	if (jQuery("#title").length && jQuery("#title").value=="" && window.parent && window.parent != window && jQuery("#evlocation",window.parent.document) && jQuery("#evlocation",window.parent.document).val()!=" -- "  && jQuery("#evlocation", window.parent.document).val()!=""){
		jQuery("#title").val(jQuery("#evlocation",window.parent.document).val());
	}
}

function noMatchingLocations() {
	if (jQuery("#createlocationbutton").length){
		jQuery("#createlocationbutton").css('display',"inline-block");
	}
	if (jQuery("#selectlocationbutton").length){
		jQuery("#selectlocationbutton").css('display',"inline-block");
	}
}

function showRemoveButton(){
	var value = window.parent.jQuery('#locn').val();
	var $removeButton = window.parent.jQuery('#removelocationbutton');
	if(value === '')
	{
		$removeButton.css('display','none');
	}
	else
	{
		$removeButton.css('display','inline-block');
	}
}

jQuery(window).on('load', function(){
		myMapload();
		setupNewLocation();
});
