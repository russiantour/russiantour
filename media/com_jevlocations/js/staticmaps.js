/* 
 *  @author    Carlos Cámara - JEvents
 *  @copyright Copyright (C) GWE Systems Ltd.
 *  @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
 *  @link http://www.jevents.net
 */


function setupStaticMap(){
	var $gmap = jQuery('#gmap');
	if ($gmap.length > 0)
	{
		$gmap.click(function () {
			loadScript();
		});

		$gmap.css('cursor','pointer');

		var iconUrl = encodeURI( gmapConf.siteRoot + 'media/com_jevlocations/images/' + gmapConf.mapicon );

		var urlString = [gmapConf.googleurl + '/maps/api/staticmap?markers='];
		//urlString.push('icon:' + iconUrl + '|' + gmapConf.latitude + ',' + gmapConf.longitude);
		urlString.push('color:red|' + gmapConf.latitude + ',' + gmapConf.longitude);
		urlString.push('&size=' + $gmap.width() + 'x' + $gmap.height() );
		urlString.push('&zoom=' + gmapConf.zoom );
		urlString.push('&region=' + gmapConf.gregion );
		urlString.push('&maptype=' + gmapConf.maptype );
		urlString.push(gmapConf.key);
		$gmap.css('background', 'url(\'' + urlString.join('') + '\')');
	}
}

/**
 * Loads in the Maps API script. This is called after some sort of user interaction.
 * The script loads asynchronously and calls loadMap once it's in.
*/
function loadScript() {
  if (!isLoaded) {
    isLoaded = true;
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = gmapConf.googlescript +   '&async=2&callback=myMapload';
    document.body.appendChild(script);
  }
}