<?php
include 'city.php';

$xw = xmlwriter_open_memory();
xmlwriter_set_indent($xw, 1);
xmlwriter_set_indent_string($xw, ' ');

xmlwriter_start_document($xw, '1.0', 'UTF-8');

xmlwriter_start_element($xw, 'tours');

xmlwriter_start_attribute($xw, 'type');
xmlwriter_text($xw, 'array');
xmlwriter_end_attribute($xw);

    foreach ($cities as $city)
    {
        xmlwriter_start_element($xw, 'tour');
        xmlwriter_start_element($xw, 'name');
        xmlwriter_text($xw, 'www.russiantour.com Международной компании «Русский тур»');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'placeName');
        xmlwriter_text($xw, 'Встреча Нового года и Рождества в Москве.  Гостиницы: “Парк Инн” 4*');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'stars');
        xmlwriter_text($xw, '4');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'apartmentTypes');
        xmlwriter_start_attribute($xw, 'type');
        xmlwriter_text($xw, 'array');
        xmlwriter_end_attribute($xw);

        xmlwriter_start_element($xw, 'apartmentType');
        xmlwriter_text($xw, 'Отель');
        xmlwriter_end_element($xw);
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'minPrice');
        xmlwriter_text($xw, '13500');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'tourTypes');
        xmlwriter_start_attribute($xw, 'type');
        xmlwriter_text($xw, 'array');
        xmlwriter_end_attribute($xw);

        xmlwriter_start_element($xw, 'tourType');
        xmlwriter_text($xw, 'NEW_YEAR');
        xmlwriter_end_element($xw);
 
		xmlwriter_start_element($xw, 'tourType');
        xmlwriter_text($xw, 'EXCURSION');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'tourType');
        xmlwriter_text($xw, 'HOLIDAYS');
        xmlwriter_end_element($xw);
        xmlwriter_end_element($xw);


        xmlwriter_start_element($xw, 'transportType');
        xmlwriter_text($xw, 'BUS');
        xmlwriter_end_element($xw);

 

        xmlwriter_start_element($xw, 'photos');
        xmlwriter_start_attribute($xw, 'type');
        xmlwriter_text($xw, 'array');
        xmlwriter_end_attribute($xw);

        xmlwriter_start_element($xw, 'photo');
        xmlwriter_text($xw, 'https://www.russiantour.com/images/mir/tour/nov_msc.jpg');
        xmlwriter_end_element($xw);
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'tourThemes');
        xmlwriter_start_attribute($xw, 'type');
        xmlwriter_text($xw, 'array');
        xmlwriter_end_attribute($xw);

        xmlwriter_start_element($xw, 'tourTheme');
        xmlwriter_text($xw, 'Авторские');
        xmlwriter_end_element($xw);
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'tourKidsTheme');
        xmlwriter_text($xw, 'Экскурсионный');
        xmlwriter_end_element($xw);



        xmlwriter_start_element($xw, 'description');
        xmlwriter_text($xw, 'Это хорошая возможность получить яркие впечатления, завести новые знакомства и прекрасно провести время в Москве. Обзорная экскурсия по Москве, Храм Христа Спасителя и Московский Кремль.  тел.88002018744 info@russiantour.com');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'partner_url');
        xmlwriter_text($xw, 'https://www.russiantour.com/rus/novyj-god');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'confirmation_phone');
        xmlwriter_text($xw, '89112262465');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'fromRegion');
        xmlwriter_text($xw, $regions[$city->regionId]);
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'fromCity');
        xmlwriter_text($xw, $city->label);
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'toRegion');
        xmlwriter_text($xw, 'Москва');
        xmlwriter_end_element($xw);

        xmlwriter_start_element($xw, 'toCity');
        xmlwriter_text($xw, 'Москва');
        xmlwriter_end_element($xw);

        xmlwriter_end_element($xw);
    }

xmlwriter_end_element($xw);

xmlwriter_end_document($xw);

$data = xmlwriter_output_memory($xw);

echo $data;

file_put_contents('nov_msc.xml', $data);